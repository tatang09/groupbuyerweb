<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>GroupBuyer</title>

        <!-- Fonts -->
        <link rel="dns-prefetch" href="//fonts.gstatic.com">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
        <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet" type="text/css">

        <!-- Styles -->
        <style>
            html, body {
                background-color: #fff;
                /* color: #636b6f; */
                font-family: -apple-system,BlinkMacSystemFont,"Segoe UI",Roboto,"Helvetica Neue",Arial,"Noto Sans",sans-serif,"Apple Color Emoji","Segoe UI Emoji","Segoe UI Symbol","Noto Color Emoji";
                font-weight: 100;
                height: 100vh;
                margin: 0;
            }

            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .content {
                text-align: center;
            }

            .title {
                font-size: 36px;
                padding: 20px;
            }
        </style>
    </head>
    <body>
        <div class="d-flex min-vh-100 align-items-center container" style="min-height: 100vh;">
            <div class="px-lg-4 px-3 custom-font-content text-center container-fluid">
                <div class="justify-content-center pt-5 pb-3 row">
                    <h1 class="custom-font-title"><strong>We will be back soon!</strong></h1>
                </div><div class="row">
                    <div class="mb-3 col-lg-12 col-md-12 col-sm-12 col-12">
                        <img src="/assets/svg/gbuyer_construction.svg" class="my-lg-4 img-fluid" style="height: 300px;">
                        <h5 class="custom-font-title pt-3 pb-3 px-lg-5 mx-lg-5">
                            The website is currently under maintenance.
                        </h5>
                        <p>We apologize for the inconvenience, but we are performing some maintenance to the site. You can still contact us at <b>info@groupbuyer.com.au</b>.</p>
                    </div>
                </div>
                <div class="justify-content-center my-2 row">
                    <img src="/assets/svg/updated_logo.svg" width="350px" class="img-fluid">
                </div>
            </div>
        </div>
    </body>
</html>
