<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{csrf_token()}}" />

        <link rel="apple-touch-icon" sizes="180x180" href={{ URL::asset("/favicon/favicon_new.svg") }}>
        <link rel="icon" type="image/png" sizes="32x32" href={{ URL::asset("/favicon/favicon_new.svg") }}>
        <link rel="icon" type="image/png" sizes="16x16" href={{ URL::asset("/favicon/favicon_new.svg") }}>
        <link rel="manifest" href={{ URL::asset("/favicon/site.webmanifest") }}>

        <title>Group Buyer - Admin Dashboard</title>

        <link rel="stylesheet" type="text/css" href="{{mix('/css/app.css')}}">
    </head>
    <body>
        <div id="app"></div>
        <script type="text/javascript" src="{{mix('/js/manifest.js')}}"></script>
        <script type="text/javascript" src="{{mix('/js/vendor.js')}}"></script>
        <script type="text/javascript" src="{{mix('/js/admin/app.js')}}"></script>
    </body>
</html>
