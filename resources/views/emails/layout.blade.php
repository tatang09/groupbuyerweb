<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    {{-- <link rel="stylesheet" type="text/css" href="{{ asset('css/email.css') }}"> --}}

    <style>
        /* Base */

body, body *:not(html):not(style):not(br):not(tr):not(code) {
    font-family: Avenir, Helvetica, sans-serif;
    box-sizing: border-box;
    outline: none;
}

body {
    background-color: #000033;
    color: #000000;
    height: 100%;
    hyphens: auto;
    line-height: 1.4;
    margin: 0;
    -moz-hyphens: auto;
    -ms-word-break: break-all;
    width: 100% !important;
    -webkit-hyphens: auto;
    -webkit-text-size-adjust: none;
    word-break: break-all;
    word-break: break-word;
}

p,
ul,
ol,
blockquote {
    line-height: 1.4;
    text-align: left;
}

a {
    color: #3869D4;
}

a img {
    border: none;
}

/* Typography */

h1 {
    color: #2F3133;
    font-size: 28px;
    font-weight: bold;
    margin-top: 0;
    text-align: left;
    letter-spacing: -0.5px;
    margin-bottom: 12px;
}

h2 {
    color: #2F3133;
    font-size: 24px;
    font-weight: bold;
    margin-top: 0;
    text-align: left;
}

h3 {
    color: #2F3133;
    font-size: 20px;
    font-weight: bold;
    margin-top: 0;
    text-align: left;
}

p, li {
    color: #74787E;
    font-size: 14px;
    line-height: 1.5em;
    margin-top: 0;
    text-align: left;
}

p.sub {
    font-size: 12px;
}

img {
    max-width: 100%;
}

hr {
    margin: 24px 0;
    opacity: 0.2;
}

/* Buttons */

.action {
    margin: 30px auto;
    padding: 0;
    text-align: center;
    width: 100%;
    -premailer-cellpadding: 0;
    -premailer-cellspacing: 0;
    -premailer-width: 100%;
}

.button {
    border-radius: 3px;
    border: none;
    padding: 10px 30px;
    /* box-shadow: 0 2px 3px rgba(0, 0, 0, 0.16); */
    cursor: pointer;
    color: #FFF;
    font-size: 16px;
    font-weight: 500;
    letter-spacing: 0.5px;
    display: inline-block;
    text-decoration: none;
    -webkit-text-size-adjust: none;
}

.button-blue,
.button-primary {
    background-color: #3097D1;
}

.button-green,
.button-success {
    background-color: #b3e19f;
}

.button-red,
.button-error {
    background-color: #bf5329;
}

/* Body */

.content {
    width: 500px;
    margin: 60px auto;
}

/* Footer */

.footer {
    text-align: center;
    margin-top: 12px;
    padding: 12px;
    font-size: 12px;
    color: #FFFFFF;
}

/* Custom */
.content .body {
    background: #FFFFFF;
    padding: 24px;
}

.property-banner {
    height: 250px;
    background-size: cover;
    background-repeat: no-repeat;
    padding: 15px 24px;
    display: table;
    width: 100%;
}

.property-banner div {
    display: table-cell;
    vertical-align: bottom;
}

.property-banner h1,
.property-banner h2 {
    color: #FFFFFF;
    margin: 0;
}

.property-banner h1{
    font-size: 42px;
    line-height: 1em;
}

.property-banner h2{
    font-size: 20px;
}

.content .body h2 {
    text-align: center;
    font-size: 28px;
    font-weight: 600;
    color: #000;
    line-height: 1.2em;
}

    </style>
</head>
<body>
    <table class="wrapper" width="100%" cellpadding="0" cellspacing="0">
        <tr>
            <td>
                <table class="content" width="100%" cellpadding="0" cellspacing="0">
                    <tr>
                        <td class="header">
                            @yield('header')
                        </td>
                    </tr>

                    <!-- Email Body -->
                    <tr>
                        <td class="body" width="100%" cellpadding="0" cellspacing="0">
                            <table class="inner-body" width="100%" cellpadding="0" cellspacing="0">
                                <!-- Body content -->
                                <tr>
                                    <td class="content-cell">
                                        @yield('content')
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>

                    <tr>
                        <td>
                            <table class="footer" width="100%" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td class="content-cell">
                                        @section('footer')
                                            Copyright © {{ date('Y') }}, {{ config('app.name') }}
                                        @show
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</body>
</html>
