<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
        <meta name="csrf-token" content="{{csrf_token()}}" />
        <meta name="title" content="GroupBuyer">
        <meta name="description" content="Brand new homes at significantly reduced prices." />

        <link rel="apple-touch-icon" sizes="180x180" href={{ URL::asset("/favicon/favicon_new.svg") }}>
        <link rel="icon" type="image/png" sizes="32x32" href={{ URL::asset("/favicon/favicon_new.svg") }}>
        <link rel="icon" type="image/png" sizes="16x16" href={{ URL::asset("/favicon/favicon_new.svg") }}>
        <link rel="manifest" href={{ URL::asset("/favicon/site.webmanifest") }}>

        <title>GroupBuyer</title>

        <!-- Google Tag Manager -->
        <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
          new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
          j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
          'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
          })(window,document,'script','dataLayer','GTM-WWCNNNH');</script>
        <!-- End Google Tag Manager -->

        @if(isset($items))

            <meta itemprop="name" content="{{$items['title']}}">
            <meta itemprop="description" property="og:description" content="{{$items['description']}}">
            <meta itemprop="image" content="{{$items['image']}}">

            <meta property="og:title" content="{{$items['title']}}">
            <meta property="og:description" content="{{$items['description']}}">
            <meta property="og:image" content="{{$items['image']}}">
            <meta property="og:url" content="{{$items['url']}}">

            <meta name="twitter:card" content="summary_large_image">
            <meta name="twitter:title" content="{{$items['title']}}">
            <meta name="twitter:description" content="{{$items['description']}}">
            <meta name="twitter:image" content="{{$items['image']}}">

        @endif


        <link rel="stylesheet" type="text/css" href="{{mix('/css/app.css')}}">

    </head>
    <body>
        <!-- Google Tag Manager (noscript) -->
        <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-WWCNNNH"
          height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
        <!-- End Google Tag Manager (noscript) -->

        <div id="app" style="height: 100%;max-width: 1920px;margin: auto;overflow-x:hidden;"></div>

        <script type="text/javascript" src="{{mix('/js/manifest.js')}}"></script>
        <script type="text/javascript" src="{{mix('/js/vendor.js')}}"></script>
        <script type="text/javascript" src="{{mix('/js/app.js')}}"></script>


        {{-- @if(url()->current() != url('under-construction'))
        <script type="text/javascript">
          var div = document.createElement('div');
          div.className = 'fb-customerchat';
          div.setAttribute('page_id', '339692443417448');
          div.setAttribute('ref', '');
          div.setAttribute('theme_color','#E91AFC');
          // div.setAttribute('color','white');
          document.body.appendChild(div);
          window.fbMessengerPlugins = window.fbMessengerPlugins || {
            init: function () {
              FB.init({
                appId            : '1678638095724206',
                autoLogAppEvents : true,
                xfbml            : true,
                version          : 'v3.3'
              });
            }, callable: []
          };

          window.fbAsyncInit = window.fbAsyncInit || function () {
            window.fbMessengerPlugins.callable.forEach(function (item) { item(); });
            window.fbMessengerPlugins.init();
          };

        setTimeout(function () {
          (function (d, s, id) {
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) { return; }
            js = d.createElement(s);
            js.id = id;
            js.src = "//connect.facebook.net/en_US/sdk/xfbml.customerchat.js";
            fjs.parentNode.insertBefore(js, fjs);
          }(document, 'script', 'facebook-jssdk'));
        }, 0);
        </script>
        @endif --}}
    </body>
</html>
