import React from "react";
import Loadable from "react-loadable";
import LoaderComponent from "./components/LoaderComponent";

import { isLoggedIn } from "~/services/auth";
import UnderConstruction from "./pages/UnderConstruction";

const Home = Loadable({
  loader: () => import(/* webpackChunkName: "Home" */ "./pages/Home"),
  loading: LoaderComponent
});

const NewHome = Loadable({
  loader: () => import(/* webpackChunkName: "Home" */ "./pages/NewHome"),
  loading: LoaderComponent
});


const SiteUnderConstruction = Loadable({
  loader: () => import(/* webpackChunkName: "Home" */ "./pages/SiteUnderConstruction"),
  loading: LoaderComponent
});

const TermsAndConditions = Loadable({
  loader: () =>
    import(
      /* webpackChunkName: "TermsAndConditions" */ "./pages/TermsAndConditions"
    ),
  loading: LoaderComponent
});

const PrivacyPolicy = Loadable({
  loader: () =>
    import(/* webpackChunkName: "PrivacyPolicy" */ "./pages/PrivacyPolicy"),
  loading: LoaderComponent
});

const AboutPage = Loadable({
  loader: () => import(/* webpackChunkName: "AboutPage" */ "./pages/AboutPage"),
  loading: LoaderComponent
});

const ContactPage = Loadable({
  loader: () =>
    import(/* webpackChunkName: "ContactPage" */ "./pages/ContactPage"),
  loading: LoaderComponent
});

const SellingPage = Loadable({
  loader: () =>
    import(/* webpackChunkName: "SellingPage" */ "./pages/SellingPage"),
  loading: LoaderComponent
});

const HowItWorksPage = Loadable({
  loader: () =>
    import(/* webpackChunkName: "HowItWorksPage" */ "./pages/HowItWorksPage"),
  loading: LoaderComponent
});

const BuyingListPage = Loadable({
  loader: () =>
    import(/* webpackChunkName: "BuyingListPage" */ "./pages/BuyingListPage"),
  loading: LoaderComponent
});

// const BuyingProjectDetailsPage = Loadable({
//   loader: () =>
//     import(
//       /* webpackChunkName: "BuyingProjectDetailsPage" */ "./pages/BuyingProjectDetailsPage"
//     ),
//   loading: LoaderComponent
// });

const BuyingProductPage = Loadable({
  loader: () =>
    import(
      /* webpackChunkName: "BuyingProductPage" */ "./pages/BuyingProductPage"
    ),
  loading: LoaderComponent
});

const BuyingApartmentDetailsPage = Loadable({
  loader: () =>
    import(
      /* webpackChunkName: "BuyingApartmentDetailsPage" */ "./pages/BuyingApartmentDetailsPage"
    ),
  loading: LoaderComponent
});

const BuyingConfirmationPage = Loadable({
  loader: () =>
    import(
      /* webpackChunkName: "BuyingConfirmationPage" */ "./pages/BuyingConfirmationPage"
    ),
  loading: LoaderComponent
});

const SecuredDealsListPage = Loadable({
  loader: () =>
    import(
      /* webpackChunkName: "SecuredDealsListPage" */ "./pages/SecuredDealsListPage"
    ),
  loading: LoaderComponent
});

const UserAccountSettings = Loadable({
  loader: () =>
    import(
      /* webpackChunkName: "UserAccountSettings" */ "./pages/UserAccountSettings"
    ),
  loading: LoaderComponent
});

const UserWishListPage = Loadable({
  loader: () =>
    import(
      /* webpackChunkName: "UserWishListPage" */ "./pages/UserWishListPage"
    ),
  loading: LoaderComponent
});

const FrequentlyAskedQuestions = Loadable({
  loader: () =>
    import(
      /* webpackChunkName: "FrequentlyAskedQuestions" */ "./pages/FrequentlyAskedQuestions"
    ),
  loading: LoaderComponent
});

const ManageProjectsPage = Loadable({
  loader: () =>
    import(
      /* webpackChunkName: "ManageProjectsPage" */ "./pages/ManageProjectsPage"
    ),
  loading: LoaderComponent
});

const ManagePropertiesPage = Loadable({
  loader: () =>
    import(
      /* webpackChunkName: "ManagePropertiesPage" */ "./pages/ManagePropertiesPage"
    ),
  loading: LoaderComponent
});

const CarsListPage = Loadable({
  loader: () =>
    import(/* webpackChunkName: "CarsListPage" */ "./pages/CarsListPage"),
  loading: LoaderComponent
});

const ContactThankYouPage = Loadable({
  loader: () =>
    import(
      /* webpackChunkName: "ContactThankYouPage" */ "./pages/ContactThankYouPage"
    ),
  loading: LoaderComponent
});

const ForgotPassword = Loadable({
  loader: () =>
    import(/* webpackChunkName: "ForgotPassword" */ "./pages/ForgotPassword"),
  loading: LoaderComponent
});

const ResetPassword = Loadable({
  loader: () =>
    import(/* webpackChunkName: "ResetPassword" */ "./pages/ResetPassword"),
  loading: LoaderComponent
});

const UserLayout = Loadable({
  loader: () =>
    isLoggedIn()
      ? import(
          /* webpackChunkName: "UserLayout" */ "./layouts/users/UserLayout"
        )
      : (window.location.href = "/"),
  loading: LoaderComponent
});

const Developers = Loadable({
  loader: () =>
    import(/* webpackChunkName: "DeveloperPage" */ "./pages/DeveloperPage"),
  loading: LoaderComponent
});

const NewBuyingApartmentDetailsPage = Loadable({
  loader: () =>
    import(/* webpackChunkName: "DeveloperPage" */ "./pages/NewBuyingApartmentDetailsPage"),
  loading: LoaderComponent
});

const routes = {
  "/": () => <SiteUnderConstruction />,
  // "/under-construction": () => <SiteUnderConstruction />,
  // "/how-it-works": () => <HowItWorksPage />,
  // "/about": () => <AboutPage />,
  // "/developers": () => <Developers />,
  // "/weekly-deals": () => <BuyingListPage />,
  // "/cars-inventory": () => <CarsListPage />,
  // "/weekly-deals/:dealId": ({ dealId }) => (
  //   <BuyingProductPage dealId={parseInt(dealId)}/>
  // ),
  // "/weekly-deals/:dealId/apartment/:propertyId": ({ propertyId, dealId }) => 
  // <NewBuyingApartmentDetailsPage  propertyId={parseInt(propertyId)} dealId={parseInt(dealId)}/>,
  // "/weekly-deals/:dealId/apartment/:propertyId/": ({
  //   dealId,
  //   propertyId
  // }) => (
  //   <BuyingApartmentDetailsPage
  //     dealId={parseInt(dealId)}
  //     propertyId={parseInt(propertyId)}
  //   />
  // ),
  // "/weekly-deals/:dealId/apartment/:propertyId/confirm": ({
  //   dealId,
  //   propertyId
  // }) => (
  //   <BuyingConfirmationPage
  //     dealId={parseInt(dealId)}
  //     propertyId={parseInt(propertyId)}
  //   />
  // ),
  // "/contact": () => <ContactPage />,
  // "/selling": () => <SellingPage />,
  // "/terms-and-conditions": () => <TermsAndConditions />,
  // "/frequently-asked-questions": () => <FrequentlyAskedQuestions />,
  // "/contact-thank-you": () => <ContactThankYouPage />,
  // "/privacy-policy": () => <PrivacyPolicy />,
  // "/profile/account-settings": () => (
  //   <UserLayout>
  //     <UserAccountSettings />
  //   </UserLayout>
  // ),
  // "/profile/secured-deals": () => <SecuredDealsListPage isAdmin={false} />,
  // "/profile/wish-list": () => <UserWishListPage />,
  // "/profile/manage-projects": () => <ManageProjectsPage />,
  // "/profile/manage-properties/:dealId/:dealSubPropertyId": ({
  //   dealId,
  //   dealSubPropertyId
  // }) => (
  //   <ManagePropertiesPage
  //     dealId={parseInt(dealId)}
  //     dealSubPropertyId={parseInt(dealSubPropertyId)}
  //   />
  // ),
  // "/request-password-reset": () => <ForgotPassword />,
  // "/reset-password": () => <ResetPassword />,
 
};

export default routes;
