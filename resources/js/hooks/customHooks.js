import React, { useEffect, useState } from 'react';


//for normal input feilds 
export const useForm = initialValues => {
    const [state, setValues] = useState(initialValues);

    return[
        state, e => {
            setValues({
                ...state, 
                [e.target.name]: e.target.value,
            })
        }
    ];
}