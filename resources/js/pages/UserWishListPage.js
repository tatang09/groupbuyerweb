import React, { useState, useEffect, createRef } from "react";
import { debounce } from "lodash";

import UserLayout from "~/layouts/users/UserLayout";
import { Table, TextInput } from "~/components/_base";

import CurrencyFormat from "react-currency-format";
import moment from "moment";
import { isMobile } from "react-device-detect";

import { Helmet } from "react-helmet";

import { metaHelper } from "../helpers/metaHelper/metaHelper";

const UserWishListPage = () => {
  const [toggleFetch, setToggleFetch] = useState(false);
  const [keyword, setKeyword] = useState("");
  const [deals, setDeals] = useState([]);
  const [loading, setLoading] = useState(true);

  const tableHeaders = [
    {
      label: "Project Name",
      width: 230
    },
    {
      label: "Property Address",
      width: 150
    },
    {
      label: "Deal Price",
      width: 150
    },
    {
      label: "Savings",
      width: 150
    },
    // {
    //   label: "Status",
    //   width: 150
    // },
    {
      label: "Properties Left",
      width: 150
    },
    // {
    //   label: "Secured Date",
    //   width: 150
    // },
    {
      label: "View Deal",
      width: 80
    }
  ];

  const discountedPrice = ({
    discount_is_percentage,
    discount,
    property_price
  }) => {
    const newPrice = discount_is_percentage
      ? property_price - property_price * discount
      : property_price - discount;

    return (
      <CurrencyFormat
        value={newPrice}
        displayType={"text"}
        thousandSeparator={true}
        prefix={"$"}
      />
    );
  };

  const renderWishlist = () => {
    return (
      <>
        <tbody>
          {deals &&
            deals.map((deal, index) => {
              return !isMobile ? (
                <tr
                  key={index}
                  className={`hover:bg-gray-100 border-b text-base`}
                >
                  <td className="p-4">
                    {/* <a
                      href={`/weekly-deals/${deal.deal_id}/apartment/${deal.property_id}`}> */}
                    <div className={`flex items-center`}>
                      <img
                        src={`${deal.featured_images[0]}`}
                        className={` w-12 h-12 mr-3`}
                      />
                      <div>
                        <div className={`font-bold text-palette-purple`}>
                          {deal.deal_name}
                        </div>
                        <div className={`opacity-50 text-sm`}>
                          {"Apartment " + deal.property_unit_name}
                        </div>
                      </div>
                    </div>
                    {/* </a> */}
                  </td>
                  <td className="p-4">
                    {deal.address.line_1 ? deal.address.line_1 + ", " : ""}
                    {deal.address.line_2 ? deal.address.line_2 + ", " : ""}
                    {deal.address.suburb ? deal.address.suburb + ", " : ""}
                    {deal.address.suburbs ? deal.address.suburbs + ", " : ""}
                    {deal.address.city ? deal.address.city + ", " : ""}
                    {deal.address.state ? deal.address.state + ", " : ""}
                    {deal.address.postal ? deal.address.postal : ""}
                  </td>
                  <td className="p-4">
                    <CurrencyFormat
                      value={deal.property_price || 0}
                      displayType={`text`}
                      thousandSeparator={true}
                      prefix={`$`}
                    />
                  </td>
                  <td className="p-4 text-palette-blue-light">
                    <CurrencyFormat
                      value={(deal.property_price * deal.discount) / 100 || 0}
                      displayType={`text`}
                      thousandSeparator={true}
                      prefix={`$`}
                    />
                  </td>
                  <td className="pl-10">{`${deal.property_available}/5`}</td>
                  {/* <td className="p-4">
                  {moment(deal.created_at).format("MMM DD, YYYY")}
                </td> */}
                  <td className="p-6">
                    <a
                      href={`/weekly-deals/${deal.deal_id}/apartment/${deal.property_id}/false`}
                    >
                      <div
                        className={`cursor-pointer bg-white border border-gray-400 duration-300 hover:bg-palette-purple hover:border-palette-purple hover:text-white inline-flex px-3 py-1 rounded transition-all`}
                      >
                        <svg className={`feather-icon h-5 w-5 opacity-50`}>
                          <use
                            xlinkHref={`/assets/svg/feather-sprite.svg#eye`}
                          />
                        </svg>
                      </div>
                    </a>
                  </td>
                </tr>
              ) : (
                <tr key={index} className={`flex flex-col`}>
                  <td
                    className={`p-4 rounded`}
                    style={{ boxShadow: "0 5px 28px 4px rgba(0,0,0,0.1)" }}
                  >
                    <div className={`flex items-center justify-between`}>
                      <div className={`flex items-center`}>
                        <img
                          src={`${deal.featured_images[0]}`}
                          className={` w-12 h-12 mr-3`}
                        />
                        <div>
                          <div
                            className={`font-bold text-base text-palette-purple`}
                          >
                            {deal.deal_name}
                          </div>
                          <div className={`opacity-50 text-sm`}>
                            {"Apartment " + deal.property_unit_name}
                          </div>
                        </div>
                      </div>
                      <a
                        href={`/weekly-deals/${deal.deal_id}/apartment/${deal.property_id}`}
                      >
                        <div
                          className={`cursor-pointer bg-white border border-gray-400 duration-300 hover:bg-palette-purple hover:border-palette-purple hover:text-white inline-flex px-3 py-1 rounded transition-all`}
                        >
                          <svg className={`feather-icon h-5 w-5 opacity-50`}>
                            <use
                              xlinkHref={`/assets/svg/feather-sprite.svg#eye`}
                            />
                          </svg>
                        </div>
                      </a>
                    </div>
                    <hr className="block border border-gray-200 my-2" />
                    <div>
                      {deal.address.line_1 ? deal.address.line_1 + ", " : ""}
                      {deal.address.line_2 ? deal.address.line_2 + ", " : ""}
                      {deal.address.suburb ? deal.address.suburb + ", " : ""}
                      {deal.address.suburbs ? deal.address.suburbs + ", " : ""}
                      {deal.address.city ? deal.address.city + ", " : ""}
                      {deal.address.state ? deal.address.state + ", " : ""}
                      {deal.address.postal ? deal.address.postal : ""}
                    </div>
                    <div className={`flex items-center justify-between mt-2`}>
                      <div>
                        <span className={`uppercase font-bold`}>
                          Deal Price:{" "}
                        </span>
                        {discountedPrice(deal)}
                      </div>
                      <div>
                        <span className={`uppercase font-bold`}>Savings: </span>
                        <CurrencyFormat
                          value={
                            deal.discount_is_percentage
                              ? deal.property_price * deal.discount
                              : deal.property_price - deal.discount || 0
                          }
                          displayType={`text`}
                          thousandSeparator={true}
                          prefix={`$`}
                          className={`text-palette-blue-light`}
                        />
                      </div>
                    </div>
                  </td>
                  <td className={`p-3`}></td>
                </tr>
              );
            })}
          {/* {!isMobile && (
            <tr>
              <td colSpan={7}>
                <div className={`text-right mt-3`}>
                  <span className={`font-bold uppercase text-base mr-3 opacity-50`}>
                    Total savings:
                  </span>
                  <CurrencyFormat
                    value={deals.reduce(
                      (a, b) =>
                        b["discount_is_percentage"]
                          ? (b["property_price"] * b["discount"]) + a
                          : (b["property_price"] - b["discount"]) + a,
                        0
                    )}
                    displayType={`text`}
                    thousandSeparator={true}
                    prefix={`$`}
                    className={`text-lg font-bold text-palette-blue-light`}
                  />
                </div>
              </td>
            </tr>
          )} */}
        </tbody>
      </>
    );
  };

  const renderHeaders = () => {
    return (
      <thead>
        <tr>
          {tableHeaders.map((th, index) => {
            return (
              <th
                style={{ width: th.width }}
                className={`border-b font-bold px-4 py-2 text-palette-gray px-4 py-2 ${
                  th.label === "View Deal" ? "text-left" : "text-left"
                }`}
                key={index}
              >
                {th.label}
              </th>
            );
          })}
        </tr>
      </thead>
    );
  };

  const handleSearch = debounce(value => {
    setKeyword(value);
  }, 800);

  return (
    <UserLayout>
      <Helmet>
        <meta name="description" content={metaHelper.desc} />
      </Helmet>
      <section
        className={`bg-palette-blue-dark`}
        style={{ marginTop: -130, paddingTop: 160 }}
      >
        <section className={`relative bg-white`}>
          <div
            className={`pb-16 px-6 lg:px-20 mx-auto`}
            style={{ maxWidth: 1366 }}
          >
            <h1
              className={`font-bold leading-tight text-4xl pt-10 pb-8 lg:pb-16 text-center`}
            >
              Wish list
            </h1>

            <div className={`flex justify-center lg:justify-end mb-8`}>
              <div className={`w-64 relative `}>
                <svg
                  className={`absolute feather-icon h-full left-0 mx-3 text-gray-500`}
                >
                  <use xlinkHref={`/assets/svg/feather-sprite.svg#search`} />
                </svg>
                <TextInput
                  className={`pl-8`}
                  type={`text`}
                  placeholder={`Search`}
                  border={false}
                  appearance={true}
                  onChange={e => handleSearch(e.target.value)}
                />
              </div>
            </div>
            <Table
              query={`/api/wish-list`}
              toggleFetch={toggleFetch}
              keyword={keyword}
              getData={setDeals}
              content={renderWishlist()}
              header={isMobile ? null : renderHeaders()}
            />
          </div>
        </section>
      </section>
    </UserLayout>
  );
};

export default UserWishListPage;
