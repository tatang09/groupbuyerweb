import React, { useEffect, useState } from "react";
import { isMobile } from "react-device-detect";
import UserLayout from "~/layouts/users/UserLayout";
import { Button } from '~/components/_base';
import { AgentDetails, Testimonials } from "~/components";

const DeveloperPage = () => {


    const [width, setWidth] = useState(window.innerWidth);


    useEffect(() => {
        const handleWindowResize = () => setWidth(window.innerWidth)
        window.addEventListener("resize", handleWindowResize);
        return () => window.removeEventListener("resize", handleWindowResize);
    }, []);

    const devBenefitsArr = [
        { title: "Display current stock", paragh: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi ut turpis in velit ultricies dignissim. Morbi viverra." },
        { title: "Raise brand awareness", paragh: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi ut turpis in velit ultricies dignissim. Morbi viverra." },
        { title: "Promote opportunities", paragh: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi ut turpis in velit ultricies dignissim. Morbi viverra." },
        { title: "Secure cash flow", paragh: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi ut turpis in velit ultricies dignissim. Morbi viverra." }
    ];

    const buyerBenefitsArr = [
        { title: "Exclusive properties", paragh: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi ut turpis in velit ultricies dignissim. Morbi viverra." },
        { title: "Unbeatable price", paragh: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi ut turpis in velit ultricies dignissim. Morbi viverra." },
        { title: "Full transparency", paragh: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi ut turpis in velit ultricies dignissim. Morbi viverra." },
        { title: "Power of knowledge", paragh: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi ut turpis in velit ultricies dignissim. Morbi viverra." }
    ];

    const logoPathArr1 = [
        "/assets/images/static_logos/CompanyLogos1@2x.png",
        "/assets/images/static_logos/SH_Official_Logo_BOX_RGB@2x.png",
        "/assets/images/static_logos/CompanyLogos2@2x.png",
        "/assets/images/static_logos/CompanyLogos4@2x.png",
    ];

    const logoPathArr2 = [
        "/assets/images/static_logos/CompanyLogos6@2x.png",
        "/assets/images/static_logos/CompanyLogos7@2x.png",
        "/assets/images/static_logos/CompanyLogos8@2x.png",
        "/assets/images/static_logos/CompanyLogos5@2x.png",
    ];


    const displayBenefits = (arr, titleColor, pColor) => {
        return arr.map((item, key) => {
            return <div key={key}>
                <span className={`font-bold leading-7 sm:text-2xl text-base ${titleColor}`}>{item.title}</span>
                <p className={`pt-2 ${pColor}`}>{item.paragh}</p>
            </div>
        });
    }

    const displayLogos = (arr) => {
        return arr.map((item, key) => {
            return <div key={key} className={`sm:flex sm:flex-1 sm:items-center sm:justify-center`}><img src={`${item}`} className={`h-24`} /></div>
        });
    };



    return (
        <UserLayout>
            <section className={`text-white`}>
                <section className={`flex bg-palette-black-main flex-col lg:flex-row`}>
                    <div className={`flex lg:w-2/5`} style={rowStyles.developerBanner}>
                        <div className={`flex hidden items-center justify-center lg:block`}>
                            <img src={`/assets/images/_directors/Ben_Daniel_large.png`} className={`flex lg:ml-12 mt-10 py-16 xl:ml-24`} />
                        </div>
                    </div>

                    <div className={`flex flex-col justify-center lg:w-3/4 xl:pl-40 lg:pl-16 p-8 pt-10 ${width >= 1024 ? "2xl:pl-32" : ""}`}>
                        <div className={`lg:pr-20 lg:pt-0 pt-10 xl:pr-32`}>
                            <div className={`lg:mt-8 w-auto xl:my-8 mt-4`}>
                                <p className={`font-bold leading-7 lg:w-auto text-4xl text-white w-2/3 xl:text-5xl`}>Why we exist</p>
                                <p className={` pt-1 text-white xl:text-xs`}>
                                    We believe… dolor sit amet, consectetur adipiscing elit. Morbi ut turpis in velit ultricies dignissim. Morbi viverra libero quis est porttitor tincidunt. Nunc in metus sit amet felis feugiat tincidunt sit amet quis lectus.
                                    </p>
                            </div>
                            <div className={`lg:mb-3 w-auto xl:mb-8`}>
                                <p className={`leading-6 lg:w-2/5 mb-0 text-base text-palette-purple-main w-3/5 xl:text-2xl xl:w-3/5`}>Twenty years of experience in property</p>
                                <p className={`lg:w-34 mb-0 mb-2 pt-1 sm:mb-0 text-white text-xs`}>
                                    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi ut turpis in velit ultricies dignissim. Morbi viverra libero quis est porttitor tincidunt. Nunc in metus sit amet felis feugiat tincidunt sit amet
                                    </p>
                            </div>
                            <div className={`lg:mb-3 w-auto 2xl:mb-8`}>
                                <p className={`leading-6 mb-0 text-base text-palette-purple-main w-10/12 xl:text-2xl`}>Making great business with both agents and developers</p>
                                <p className={`lg:w-34 pt-1 text-white text-xs mb-0`}>
                                    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi ut turpis in velit ultricies dignissim. Morbi viverra libero quis est porttitor tincidunt. Nunc in metus sit amet felis feugiat tincidunt sit amet
                                    </p>
                            </div>
                        </div>
                    </div>
                    <div style={rowStyles.founder} className={`lg:w-2/6 rounded-lg lg:hidden block`}>
                        <div className={`flex flex-col items-center p-8`}>
                            <div className={`flex sm:justify-center w-full`}><p className={`mb-2 text-palette-purple-main text-xs`}>BEN DANIEL - FOUNDER</p></div>
                            <img src={"/assets/images/_directors/Ben_Daniel_founder.png"} className={`h-64 my-3 rounded sm:w-1/2 w-full`} />
                        </div>
                    </div>
                </section>
                <section className={`relative bg-cover`}
                    style={rowStyles.developerBenefits}>
                    <div className={`p-8`}>
                        <div className={`sm:flex sm:flex-col sm:items-center sm:justify-center`}>
                            <div className={`flex flex-col hidden items-center justify-center sm:block sm:mb-12 lg:mt-10`}>
                                <span className={`font-bold leading-7 text-5xl text-gray-900`}>Benefits for Developers</span>
                                <p className={`leading-3 m-0 pt-2 text-base text-center text-palette-purple-main`}>
                                    An alternative to the existing property flatforms.
                                </p>
                            </div>
                            <div className={`block sm:hidden w-10/12`}>
                                <span className={`font-bold leading-7 sm:text-6xl text-4xl text-gray-800`}>Benefits</span>
                            </div>
                            <div className={`block sm:hidden w-10/12`}>
                                <span className={`font-bold leading-7 sm:text-6xl text-4xl text-gray-800`}>for Developers</span>
                                <p className={`pt-2 text-palette-purple-main whitespace-no-wrap`}>
                                    An alternative to the existing property flatforms.
                                </p>
                            </div>
                        </div>
                        <div className={`sm:flex md:mb-16 xl:px-48`}>
                            <div className={`flex flex-1 hidden sm:block`}>
                                <img src={`/assets/images/benefits_for_developers2.png`} className={`h-auto rounded-lg w-11/12`} />
                            </div>
                            <div className={`flex flex-1 flex-col md:justify-center sm:mt-4`}>
                                <div className={`flex flex-col items-center sm:justify-center `}>{displayBenefits(devBenefitsArr, "text-purple-800", "text-gray-800")}</div>
                                <Button className={`bg-palette-purple-main button button-primary font-sans md:w-1/2 rounded undefined w-full md:w-3/4 xl:w-1/2`}>Join Us Now</Button>
                            </div>
                        </div>
                    </div>
                </section>
                <section className={`relative bg-cover`}
                    style={rowStyles.buyerBenefits}>
                    <div className={`p-8`}>
                        <div className={`sm:mt-10 sm:flex sm:flex-col sm:justify-center sm:items-center`}>
                            <div className={`flex flex-col hidden items-center justify-center sm:block sm:mb-12`}>
                                <span className={`font-bold leading-7 sm:text-6xl text-4xl text-white`}>Benefits for Buyers</span>
                                <p className={`leading-3 m-0 pt-4 text-center text-palette-purple-main text-xs`}>
                                    A new and innovative platform to access properties from trusted developers.
                                </p>
                            </div>
                            <div className={`w-10/12 block sm:hidden`}>
                                <span className={`font-bold leading-7 text-4xl text-white`}>Benefits</span>
                            </div>
                            <div className={`w-10/12 block sm:hidden`}>
                                <span className={`font-bold leading-7 text-4xl text-white`}>for Buyers</span>
                                <p className={`pt-2 text-palette-purple-main`}>
                                    A new and innovative platform to access properties from trusted developers.
                                </p>
                            </div>
                        </div>
                        <div className={`sm:flex md:mb-16 xl:px-48`}>
                            <div className={`flex-1 flex`}>
                                <div className={`flex flex-col items-center justify-end sm:pb-8`}>{displayBenefits(buyerBenefitsArr, "text-palette-purple-main", "text-white")}</div>
                            </div>
                            <div className={`flex flex-1 hidden sm:block`}>
                                <img src={`/assets/images/benefits_for_buyers2.png`} className={`h-auto rounded-lg w-10/11`} />
                            </div>
                        </div>
                    </div>
                </section>
                <section>
                    <div className={`flex flex-col items-center justify-center p-8 w-full sm:py-16`}>
                        <div className={`sm:flex sm:flex-col sm:items-center`}>
                            <div className={`flex items-center justify-center mb-4 w-full`}>
                                <span className={`leading-7 leading-none text-base text-center text-gray-800 w-1/2 mb-3`}>People we are working with</span>
                            </div>
                            <p className={`sm:w-2/5 pt-2 text-center text-gray-800`}>
                                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi ut turpis in velit ultricies dignissim. Morbi viverra libero quis est porttitor tincidunt. Nunc in
                            </p>
                        </div>
                        <div className={`flex sm:flex-col sm:w-3/4 w-full`}>
                            <div className={`sm:flex-row flex flex-1 flex-col items-center justify-center w-full sm:mb-12`}>
                                {displayLogos(logoPathArr1)}
                            </div>
                            <div className={`sm:flex-row flex flex-1 flex-col items-center justify-center w-full`}>
                                {displayLogos(logoPathArr2)}
                            </div>
                        </div>
                    </div>
                </section>
               <Testimonials />
                <section style={rowStyles.letsWorkTogether}>
                    <div className={`p-8 pb-0`}>
                        <div className={`text-center w-full`}>
                            <span className={`font-bold leading-7 text-palette-purple-main md:text-base text-xxs`}>LET'S WORK TOGETHER</span>
                        </div>
                        <div className={`flex items-center justify-center mb-4 w-full`}>
                            <span className={`leading-7 leading-none mb-3 md:text-4xl md:w-1/2 text-base text-center text-white w-7/12 xl:w-1/3`}>Join and let's discuss on how we can collaborate</span>
                        </div>
                    </div>
                    <div className={`md:flex md:flex-col md:px-48 p-8 pt-0 pb-2 sm:px-16 items-center`}>
                        <AgentDetails />
                    </div>
                    <div className={`flex md:py-12 px-8 py-8 sm:items-center sm:justify-center`}>
                        <Button className={`bg-palette-purple-main button button-primary font-sans lg:w-1/5 rounded sm:w-1/2 text-xs undefined w-full`}>Let's work together</Button>
                    </div>
                </section>
               
            </section>
        </UserLayout>
    )
}

const rowStyles = {
    developerBanner: {
        background:
            "url('/assets/images/developers_banner_3.png')",
        // marginTop: isMobile ? -162 : -130,
        minHeight: "100%",
        backgroundBlendMode: "multiply",
        backgroundSize: "cover"
    },
    buyerBenefits: {
        background:
            "url('/assets/images/buyer_benefits.png')",
        backgroundSize: "100%",
        backgroundPosition: "center",
        backgroundRepeat: "no-repeat",
        minHeight: "100%",
        backgroundSize: "contain"
    },
    founder: {
        background:
            "url('/assets/images/developers_banner_3.png')",
        backgroundSize: "cover",
        backgroundPosition: "center",
        backgroundRepeat: "no-repeat"
    },
    letsWorkTogether: {
        background:
            "url('/assets/images/lets_work_together.png')",
        minHeight: "100%",
        backgroundBlendMode: "multiply",
        backgroundSize: "cover",
    },
    developerBenefits: {
        background:
            "url('/assets/images/developer_benefits.png')",
        // marginTop: isMobile ? -162 : -130,
        minHeight: "100%",
        backgroundBlendMode: "multiply",
        backgroundSize: "cover",
        backgroundRepeat: "no-repeat"
    },
    developerBenefits2: {
        background:
            "url('/assets/images/benefits_for_developers2.png')",
        // marginTop: isMobile ? -162 : -130,
        minHeight: "100%",
        backgroundSize: "cover",
        backgroundRepeat: "no-repeat"
    }
}

export default DeveloperPage;