import React, { useState, useEffect, useRef } from "react";
import { debounce } from "lodash";

import UserLayout from "~/layouts/users/UserLayout";
import UserGlobal from "~/states/userGlobal";
import { BuyingProgress, SocialMediaShareModal } from "~/components";
import { Table } from "~/components/_base";
import { isLoggedIn } from "~/services/auth";

import {
  ClickAwayListener,
  Collapse,
  Popper,
  Grow,
  MenuList,
  MenuItem,
  Paper
} from "@material-ui/core";
import { isMobile } from "react-device-detect";
import CurrencyFormat from "react-currency-format";
import Select from "react-select";
import { Helmet } from "react-helmet";
import { metaHelper } from "../helpers/metaHelper/metaHelper";
 
const CarsListPage = () => {
  const [userState, userAction] = UserGlobal();
  const [keyword, setKeyword] = useState("");
  const [popper, setPopper] = useState(null);
  const [openSearch, setOpenSearch] = useState(false);
  const [toggleFetch, setToggleFetch] = useState(false);
  const [priceDropdown, setPriceDropdown] = useState(false);
  const [projectData, setProjectData] = useState([]);
  const [options, setOptions] = useState([]);
  const [priceOptions, setPriceOptions] = useState([]);
  const [filters, setFilters] = useState({
    sortBy: { value: "deals.created_at", order: "desc", label: "Most Recent" },
    minPrice: { value: 0, label: "Any" },
    maxPrice: { value: 15000000, label: "Any" },
    types: "All",
    selectedTypes: { value: "All", label: "All" },
    locations: "All",
    selectedLocations: { value: "All", label: "All" }
  });

  const anchorRef = useRef(null);

  const prices = [
    50000,
    100000,
    150000,
    200000,
    250000,
    300000,
    350000,
    400000,
    450000,
    500000,
    550000,
    600000,
    650000,
    700000,
    750000,
    800000,
    850000,
    900000,
    950000,
    1000000,
    1100000,
    1200000,
    1300000,
    1400000,
    1500000,
    1600000,
    1700000,
    1800000,
    1900000,
    2000000,
    2100000,
    2200000,
    2300000,
    2400000,
    2500000,
    2600000,
    2700000,
    2800000,
    2900000,
    3000000,
    4000000,
    5000000,
    6000000,
    7000000,
    8000000,
    9000000,
    10000000,
    11000000,
    12000000,
    13000000,
    14000000,
    15000000
  ];

  useEffect(() => {
    if (localStorage.getItem("__use_projects_filters")) {
      let savedFilters = JSON.parse(localStorage.getItem("__projects_filters"));

      setFilters(savedFilters);
      setToggleFetch(!toggleFetch);
      localStorage.setItem("__use_projects_filters", "");
    }

    const getType = async () => {
      let { data } = await axios.get("/api/sub-property-type");

      let $typeOptions = data.map(type => ({
        value: type.id,
        label: type.name
      }));
      $typeOptions.unshift({ value: "All", label: "All" });
      setOptions($typeOptions);
    };

    const getPriceOptions = () => {
      let options = [];

      prices.map(price => {
        options.push({
          value: price,
          label: new Intl.NumberFormat("en-AU", {
            style: "currency",
            currency: "AUD",
            minimumFractionDigits: 0
          }).format(price || 0)
        });
      });

      setPriceOptions(options);
    };

    getType();
    getPriceOptions();
  }, []);

  const handleSortBy = selectedItem => {
    setFilters({ ...filters, sortBy: selectedItem });
    setToggleFetch(!toggleFetch);
  };

  const handleMinPrice = selectedItem => {
    if (!selectedItem) {
      setFilters({ ...filters, minPrice: { value: 0, label: "Any" } });
      setToggleFetch(!toggleFetch);
      return;
    }

    setFilters({ ...filters, minPrice: selectedItem });
    setToggleFetch(!toggleFetch);
  };

  const handleMaxPrice = selectedItem => {
    if (!selectedItem) {
      setFilters({ ...filters, maxPrice: { value: 15000000, label: "Any" } });
      setToggleFetch(!toggleFetch);
      return;
    }

    setFilters({ ...filters, maxPrice: selectedItem });
    setToggleFetch(!toggleFetch);
  };

  const handleSelectType = selectedItem => {
    if (!selectedItem || selectedItem.length === 0) {
      setFilters({
        ...filters,
        types: "All",
        selectedTypes: { value: "All", label: "All" }
      });
      setToggleFetch(!toggleFetch);
      return;
    }

    let types = [];
    let lastSelected = selectedItem[selectedItem.length - 1];
    let filteredTypes = selectedItem.filter(item => item.value !== "All");

    if (lastSelected.value === "All") {
      setFilters({
        ...filters,
        types: lastSelected.value,
        selectedTypes: lastSelected
      });
      setToggleFetch(!toggleFetch);
      return;
    }

    filteredTypes.map(type => {
      types.push(type.value);
    });

    setFilters({
      ...filters,
      types: types,
      selectedTypes: filteredTypes
    });
    setToggleFetch(!toggleFetch);
  };

  const handleSelectLocation = selectedItem => {
   
    if (!selectedItem || selectedItem.length === 0) {
      setFilters({
        ...filters,
        locations: "All",
        selectedLocations: { value: "All", label: "All" }
      });
      setToggleFetch(!toggleFetch);
      return;
    }

    let locations = [];
    let lastSelected = selectedItem[selectedItem.length - 1];
    let filteredLocations = selectedItem.filter(item => item.value !== "All");

    if (lastSelected.value === "All") {
      setFilters({
        ...filters,
        locations: lastSelected.value,
        selectedLocations: lastSelected
      });
      setToggleFetch(!toggleFetch);
      return;
    }

    filteredLocations.map(city => {
      locations.push(city.value);
    });

    setFilters({
      ...filters,
      locations: locations,
      selectedLocations: filteredLocations
    });
    setToggleFetch(!toggleFetch);
  };

  const handleSearch = debounce(value => {
    setKeyword(value);
  }, 800);

  const handleSelectProject = url => {
    if (!isLoggedIn()) {
      userAction.setState({ showSignIn: true });
      return;
    }
    localStorage.setItem("__projects_filters", JSON.stringify(filters));
    window.location = url;
  };

  const renderProjects = () => {
    return (
      <tbody className={`flex flex-wrap`}>
        {projectData &&
          projectData.map((project, key) => {
            return (
              <tr key={key} className={`lg:w-4/12 mb-8 lg:mb-4 lg:px-3 w-full`}>
                <td
                  style={{ boxShadow: "0 5px 28px 4px rgba(0,0,0,0.1)" }}
                  className={`block bg-white rounded-md lg:hover:scale-105 duration-300 transform transition-all p-0`}
                >
                  <div
                    className={`cursor-pointer`}
                    onClick={() =>
                      handleSelectProject(`/weekly-deals/${project.id}`)
                    }
                  >
                    <div
                      className={`bg-cover bg-center rounded-t-md relative text-white`}
                      style={{
                        minHeight: isMobile ? 250 : 300,
                        background: `linear-gradient(rgba(0, 0, 0, 0.1) 0%, rgba(0, 0, 0, 0.2) 80%, rgb(0, 0, 0) 100%), url("/storage/projects/${project.featured_images[0]}")`
                      }}
                    >
                      <div className={`absolute top-0 left-0 mt-6`}>
                        <div className={`bg-red-700 flex items-center px-5`}>
                          <span className={`font-semibold text-2xl mr-3`}>
                          {`${project.discount}%`}
                          </span>
                          <span className={`font-semibold text-base uppercase`}>
                            Discount
                          </span>
                        </div>
                      </div>

                      <div
                        className={`text-2xl absolute bottom-0 left-0 mb-3 ml-4 font-bold w-56 leading-none`}
                      >
                        {project.name}
                      </div>

                      <div
                        className={`absolute bottom-0 right-0 mb-4 mr-4 flex items-center`}
                      >
                        <svg
                          className={`feather-icon text-palette-purple mr-1`}
                        >
                          <use
                            xlinkHref={`/assets/svg/feather-sprite.svg#map-pin`}
                          />
                        </svg>
                        {project.address.suburbs + " " + project.address.state}
                      </div>
                    </div>
                  </div>
                  <div className={`pt-3 pb-4 px-5`}>
                    <div
                      className={`flex items-center justify-between border-b pb-2 mb-2`}
                    >
                      <span className={`text-base font-semibold`}>
                        <CurrencyFormat
                          value={project.properties.reduce(
                            (a, b) => (b["price"] < a ? b["price"] : a),
                            project.properties[0].price
                          )}
                          displayType={`text`}
                          thousandSeparator={true}
                          prefix={`$`}
                        />
                        <span> - </span>
                        <CurrencyFormat
                          value={project.properties.reduce(
                            (a, b) => (b["price"] > a ? b["price"] : a),
                            project.properties[0].price
                          )}
                          displayType={`text`}
                          thousandSeparator={true}
                          prefix={`$`}
                        />
                      </span>
                      <span
                        className={`
                      text-base font-semibold
                      ${
                        project.properties.filter(
                          property => property.is_secured === false
                        ).length < 2
                          ? "text-red-700"
                          : ""
                      }
                    `}
                      >
                        {project.properties.filter(
                          property => property.is_secured === false
                        ).length !== 0
                          ? project.properties.filter(
                              property => property.is_secured === false
                            ).length
                          : ""}
                        {project.properties.filter(
                          property => property.is_secured === false
                        ).length > 1
                          ? " Available"
                          : project.properties.filter(
                              property => property.is_secured === false
                            ).length === 0
                          ? "Sold out"
                          : " Remaining"}
                      </span>
                    </div>
                    <div className={`flex items-center justify-between`}>
                      <span className={`text-base`}>{project.deal_type}</span>
                      <div className={`text-base flex`}>
                        <span
                          className={`cursor-pointer`}
                          onClick={() =>
                            userAction.setState({ showSocialMedia: true })
                          }
                        >
                          <svg className={`feather-icon h-5 w-5`}>
                            <use
                              xlinkHref={`/assets/svg/feather-sprite.svg#share`}
                            />
                          </svg>
                        </span>
                        {/* <svg className={`feather-icon h-5 w-5 cursor-pointer`}>
                        <use xlinkHref={`/assets/svg/feather-sprite.svg#heart`}/>
                      </svg> */}
                      </div>
                    </div>
                  </div>
                </td>
              </tr>
            );
          })}
      </tbody>
    );
  };

  const renderFilters = () => {
    return (
      <div
        className={`
        lg:flex-row lg:px-16 lg:m-0
        flex-col flex mt-8 mb-10 text-gray-600 px-8 items-center justify-center relative
      `}
      >
        {/* <div>
          <div
            className={`uppercase font-bold px-3 text-xs absolute`}
            style={{top: "-24px"}}
          >
            Property Name
          </div>
          <div className={`border-l-2 border-r-2 p-3 w-full h-full flex items-center`}>
            <TextInput
              border={false}
              placeholder={`Search Properties`}
              type={`text`}
              prefix={`search`}
              onChange={(e) => handleSearch(e.target.value)}
            />
          </div>
        </div> */}

        <div
          className={`flex-1 mb-4 lg:mb-0 w-full`}
          style={{ maxWidth: isMobile ? "100%" : "25%" }}
        >
          <div
            className={`lg:block uppercase font-bold px-3 text-xs lg:absolute`}
            style={{ top: "-24px" }}
          >
            Location
          </div>
          <div className={`border-2 w-full h-full flex items-center`}>
            <Select
              isMulti
              isClearable={false}
              closeMenuOnSelect={false}
              hideSelectedOptions={false}
              isSearchable={false}
              options={[
                { value: "All", label: "All" },
                { value: "Adelaide", label: "Adelaide" },
                { value: "Brisbane", label: "Brisbane" },
                { value: "Gold Coast", label: "Gold Coast" },
                { value: "Melbourne", label: "Melbourne" },
                { value: "Newcastle", label: "Newcastle" },
                { value: "Sydney", label: "Sydney" }
              ]}
              classNamePrefix={`input-select`}
              className={`gb-multi-select h-full w-full`}
              placeholder={`Select Location`}
              value={filters.selectedLocations}
              onChange={handleSelectLocation}
            />
          </div>
        </div>

        <div
          className={`flex-1 mb-4 lg:mb-0 w-full`}
          style={{ maxWidth: isMobile ? "100%" : "25%" }}
        >
          <div
            className={`lg:block uppercase font-bold px-3 text-xs lg:absolute`}
            style={{ top: "-24px" }}
          >
            Type
          </div>
          <div
            className={`border-2 lg:border-l-0 w-full h-full flex items-center`}
          >
            <Select
              isMulti
              isClearable={false}
              closeMenuOnSelect={false}
              hideSelectedOptions={false}
              isSearchable={false}
              options={options}
              classNamePrefix={`input-select`}
              className={`gb-multi-select h-full w-full`}
              placeholder={`Select Type`}
              value={filters.selectedTypes}
              onChange={handleSelectType}
            />
          </div>
        </div>

        <div
          className={`relative flex-1 mb-4 lg:mb-0 w-full`}
          style={{ maxWidth: isMobile ? "100%" : "25%" }}
        >
          <div
            className={`lg:block uppercase font-bold px-3 text-xs lg:absolute`}
            style={{ top: "-24px" }}
          >
            Price
          </div>
          <div
            className={`border-2 lg:border-l-0 w-full h-full flex items-center cursor-pointer relative`}
            onClick={() => setPriceDropdown(!priceDropdown)}
          >
            <div
              className={`px-3 py-2 text-base text-gray-500 w-full flex items-center justify-between`}
            >
              <div className={`flex items-center`} style={{ height: 36 }}>
                {filters.minPrice.label} ~ {filters.maxPrice.label}
              </div>
              <svg
                className={`feather-icon opacity-50 hover:opacity-100 ${
                  priceDropdown ? "opacity-100" : ""
                }`}
                style={{
                  color: "hsl(0, 0%, 60%)",
                  strokeWidth: "3px",
                  width: 18,
                  height: 18
                }}
              >
                <use
                  xlinkHref={`/assets/svg/feather-sprite.svg#chevron-down`}
                />
              </svg>
            </div>
          </div>
          {priceDropdown && (
            <ClickAwayListener
              onClickAway={() => setPriceDropdown(!priceDropdown)}
            >
              <div
                className={`absolute bg-white border-2 p-2 z-50`}
                style={{
                  top: "calc(100% - 2px)",
                  width: isMobile ? "100%" : "calc(100% + 2px)",
                  left: isMobile ? 0 : "-2px"
                }}
              >
                <div className={`flex items-center mb-2`}>
                  <span className={`font-bold mr-1 w-16`}>Min Price</span>
                  <Select
                    hideSelectedOptions={false}
                    isSearchable={false}
                    isClearable={true}
                    options={priceOptions}
                    classNamePrefix={`input-select`}
                    className={`gb-price-select flex-1 ml-2`}
                    placeholder={`Min Price`}
                    defaultValue={filters.minPrice}
                    onChange={handleMinPrice}
                  />
                </div>
                <div className={`flex items-center`}>
                  <span className={`font-bold mr-1 w-16`}>Max Price</span>
                  <Select
                    hideSelectedOptions={false}
                    isSearchable={false}
                    isClearable={true}
                    options={priceOptions}
                    classNamePrefix={`input-select`}
                    className={`gb-price-select flex-1 ml-2`}
                    placeholder={`Max Price`}
                    defaultValue={filters.maxPrice}
                    onChange={handleMaxPrice}
                  />
                </div>
              </div>
            </ClickAwayListener>
          )}
        </div>

        {!isMobile && (
          <div
            className={`flex-1 w-full`}
            style={{ maxWidth: isMobile ? "100%" : "25%" }}
          >
            <div
              className={`hidden lg:block uppercase font-bold px-3 text-xs absolute`}
              style={{ top: "-24px" }}
            >
              Sort By
            </div>
            <div
              className={`border-2 lg:border-l-0 w-full h-full flex items-center`}
            >
              <Select
                hideSelectedOptions={false}
                isSearchable={false}
                options={[
                  {
                    value: "deals.created_at",
                    order: "desc",
                    label: "Most Recent"
                  },
                  {
                    value: "incentives.discount",
                    order: "desc",
                    label: "Biggest Discount"
                  },
                  {
                    value: "deals.expires_at",
                    order: "asc",
                    label: "Closing Soon"
                  },
                  {
                    value: "asc_properties_total_price",
                    order: "asc",
                    label: "Price (low-high)"
                  },
                  {
                    value: "desc_properties_total_price",
                    order: "desc",
                    label: "Price (high-low)"
                  }
                ]}
                classNamePrefix={`input-select`}
                className={`gb-multi-select h-full w-full`}
                placeholder={`Sort By`}
                value={filters.sortBy}
                onChange={handleSortBy}
              />
            </div>
          </div>
        )}

        {/* { isMobile && (
          <Button
            onClick={handleClearFilters}
            className={`mt-3 border-palette-purple text-palette-purple font-bold`}
            type={`secondary`}
          >
            Clear filters
          </Button>
        )} */}
      </div>
    );
  };

  return (
    <UserLayout>
      <Helmet>
        <meta name="description" content={metaHelper.desc} />
      </Helmet>
      <section
        className={`bg-palette-blue-dark`}
        style={{ marginTop: -130, paddingTop: 160 }}
      >
        <section className={`relative bg-white`}>
          <BuyingProgress />

          <div
            className={`pb-16 px-6 lg:px-0 mx-auto`}
            style={{ maxWidth: 1366 }}
          >
            <h1
              className={`text-left lg:text-center font-bold leading-tight text-4xl mt-10 mb-8 lg:mb-16`}
            >
              Cars Inventory List
            </h1>

            {isMobile && (
              <div className={`flex items-center justify-between`}>
                <div
                  onClick={() => setOpenSearch(!openSearch)}
                  className={`uppercase font-bold text-palette-gray flex items-center`}
                >
                  Refine Search
                  <svg
                    className={`
                    ml-1 transition-all duration-300 feather-icon text-palette-gray transform
                    ${openSearch ? "rotate-45" : ""}
                  `}
                  >
                    <use xlinkHref={`/assets/svg/feather-sprite.svg#plus`} />
                  </svg>
                </div>
                <div
                  ref={anchorRef}
                  onClick={e => setPopper(popper ? null : e.currentTarget)}
                  className={`uppercase font-bold text-palette-gray flex items-center`}
                >
                  {filters.sortBy.label}
                  <svg
                    className={`
                    ml-1 transition-all duration-300 feather-icon text-palette-gray transform
                    ${popper ? "rotate-180" : ""}
                  `}
                  >
                    <use
                      xlinkHref={`/assets/svg/feather-sprite.svg#chevron-down`}
                    />
                  </svg>
                </div>
                <Popper
                  open={Boolean(popper)}
                  anchorEl={popper}
                  placement={`bottom-end`}
                  transition
                  disablePortal
                  className={`z-50 menu-list`}
                >
                  {({ TransitionProps }) => (
                    <Grow
                      {...TransitionProps}
                      style={{ transformOrigin: "right top" }}
                    >
                      <Paper elevation={2} className={`relative`}>
                        <MenuList>
                          <MenuItem
                            onClick={() => {
                              setFilters({
                                ...filters,
                                sortBy: {
                                  value: "deals.created_at",
                                  order: "desc",
                                  label: "Most Recent"
                                }
                              });
                              setToggleFetch(!toggleFetch);
                              setPopper(null);
                            }}
                          >
                            Most Recent
                          </MenuItem>
                          <MenuItem
                            onClick={() => {
                              setFilters({
                                ...filters,
                                sortBy: {
                                  value: "incentives.discount",
                                  order: "desc",
                                  label: "Biggest Discount"
                                }
                              });
                              setToggleFetch(!toggleFetch);
                              setPopper(null);
                            }}
                          >
                            Biggest Discount
                          </MenuItem>
                          <MenuItem
                            onClick={() => {
                              setFilters({
                                ...filters,
                                sortBy: {
                                  value: "deals.expires_at",
                                  order: "asc",
                                  label: "Closing Soon"
                                }
                              });
                              setToggleFetch(!toggleFetch);
                              setPopper(null);
                            }}
                          >
                            Closing Soon
                          </MenuItem>
                          <MenuItem
                            onClick={() => {
                              setFilters({
                                ...filters,
                                sortBy: {
                                  value: "properties_total_price",
                                  order: "asc",
                                  label: "Price (low-high)"
                                }
                              });
                              setToggleFetch(!toggleFetch);
                              setPopper(null);
                            }}
                          >
                            Price (low-high)
                          </MenuItem>
                          <MenuItem
                            onClick={() => {
                              setFilters({
                                ...filters,
                                sortBy: {
                                  value: "properties_total_price",
                                  order: "desc",
                                  label: "Price (high-low)"
                                }
                              });
                              setToggleFetch(!toggleFetch);
                              setPopper(null);
                            }}
                          >
                            Price (high-low)
                          </MenuItem>
                        </MenuList>
                      </Paper>
                    </Grow>
                  )}
                </Popper>
              </div>
            )}

            <Collapse in={isMobile ? openSearch : true}>
              {renderFilters()}
            </Collapse>

            <div className={`lg:px-16 mt-4 lg:mt-24`}>
              <Table
                query={`/api/deal`}
                queryParams={`&projects=true&types=${filters.types}&locations=${filters.locations}&min_price=${filters.minPrice.value}&max_price=${filters.maxPrice.value}`}
                // queryParams={`&projects=true&types=${filters.types}`}
                toggleFetch={toggleFetch}
                toggleFetch={toggleFetch}
                keyword={keyword}
                getData={setProjectData}
                content={renderProjects()}
                sort={filters.sortBy.value.replace(/asc_|desc_/g, "") || ""}
                order={filters.sortBy.order || ""}
              />
            </div>
          </div>
        </section>
      </section>
      <SocialMediaShareModal />
    </UserLayout>
  );
};

export default CarsListPage;
