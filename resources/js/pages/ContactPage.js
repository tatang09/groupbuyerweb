import React from "react";
import { A } from "hookrouter";
import UserLayout from "~/layouts/users/UserLayout";
import { ContactForm, SocialMedia } from "~/components";
import { Helmet } from "react-helmet";
import { metaHelper } from "../helpers/metaHelper/metaHelper";

const ContactPage = () => {
  return (
    <UserLayout topFooter={false}>
      <Helmet>
      
        <meta name="description" content={metaHelper.desc} />
     
      </Helmet>
      <section
        className="text-white bg-palette-blue-dark"
        style={{ marginTop: -130, paddingTop: 160 }}
      >
        <section
          className="relative text-center bg-center bg-cover"
          style={rowStyles.contact}
        >
          <div className={`mx-auto`} style={{ maxWidth: 1366 }}>
            <h1 className={`font-bold leading-10 pb-20 pt-24 text-4xl`}>
              Get in touch.
            </h1>
            <div className={`lg:mx-auto mx-4 relative`}>
              <ContactForm backgroundColor={`rgba(0, 25, 84, 0.90)`} />
            </div>
            <div className={`flex flex-col items-center justify-center py-12`}>
              <SocialMedia className={`mb-4`} size={`2x`} />
              <div className={`font-semibold lg:font-bold text-2xl`}>
                Phone
                <a
                  href={`tel:1300031835`}
                  className={`hover:text-palette-teal text-palette-blue-dark`}
                >
                  &nbsp;1300 031 835
                </a>
              </div>
              <div className={`font-semibold lg:font-bold text-2xl`}>
                Email
                <a
                  href={`mailto:info@groupbuyer.com.au`}
                  className={`hover:text-palette-teal text-palette-blue-dark`}
                >
                  &nbsp;info@groupbuyer.com.au
                </a>
              </div>
            </div>
          </div>
        </section>
      </section>
    </UserLayout>
  );
};

const rowStyles = {
  contact: {
    background:
      "linear-gradient(to left,rgb(30, 136, 225) 0%, rgb(30, 136, 225, 0.5) 0%), url('/assets/images/contact_page.jpg')"
  }
};

export default ContactPage;
