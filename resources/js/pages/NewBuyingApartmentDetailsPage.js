import React, { useState, useEffect } from "react";

import UserLayout from "~/layouts/users/UserLayout";
import UserGlobal from "~/states/userGlobal";

import {
  SocialMediaShareModal,
  TheMore,
  InspectProperty,
  TalkExpert,
  JoinUs,
  GoogleMapsNew
} from "~/components";
import { isLoggedIn } from "~/services/auth";
import { isEmpty } from "lodash";

import { Helmet } from "react-helmet";
import { metaHelper } from "../helpers/metaHelper/metaHelper";
import CurrencyFormat from "react-currency-format";
import { extracDescription, extractFeatureOrInclusion} from '../helpers/html/htmlHelper';

const NewBuyingApartmentDetailsPage = ({ dealId, propertyId }) => {
  const [loading, setLoading] = useState(true);
  const [userState, userAction] = UserGlobal();
  const [apartment, setApartment] = useState();
  const [showBookingModal, setShowBookingModal] = useState(false);
  const [project, setProject] = useState();

  const getDeal = async () => {
    const deal = await axios.get(`/api/deal/${dealId}`);

    if (deal.data.sub_property_id == 4) {
      setIsProjectLand(true);
    }

    if (deal.data && deal.data.expires_at) {
      let exp = JSON.parse(deal.data.expires_at);
      setTimeLeft(exp.time_limit);
    }

    let newApartment = null;

    if (isLoggedIn()) {
      const { data } = await axios.get(`/api/wish-list/${dealId}`, {
        headers: {
          Authorization: "Bearer " + isLoggedIn()
        }
      });

      let newProject = Object.assign({}, deal.data);

      if (data && !isEmpty(newProject)) {
        data.map(d => {
          newProject.properties.map(p => {
            if (p.id == d.property_id) {
              p.hasWishlist = true;
              p.wishListId = d.id;
            }
          });
        });
      }

      newApartment = newProject.properties.find(p => p.id == propertyId);

      if (!isEmpty(newApartment)) {
        setApartment(newApartment);
      }
      setProject(newProject);
      setLoading(false);
    } else {
      newApartment = deal.data.properties.find(p => p.id == propertyId);
      setApartment(newApartment);
      setProject(deal.data);
      setLoading(false);
    }
  };


  const handleWishlist = async property => {
    if (!isLoggedIn()) {
      userAction.setState({ showSignIn: true });
      return;
    }

    let formData = new FormData();

    formData.append("property_id", property.id);
    formData.append("deal_id", dealId);

    try {
      if (!property.hasWishlist) {
        await axios.post("/api/wish-list", formData, {
          headers: {
            Authorization: "Bearer " + isLoggedIn()
          }
        });
      } else {
        await axios.delete(`/api/wish-list/${property.wishListId}`, {
          headers: {
            Authorization: "Bearer " + isLoggedIn()
          }
        });
      }

      getDeal();
    } catch (error) { }
  };

  useEffect(() => {
    getDeal();
  }, [userState.user]);

  const handleClose = () => {
    setShow(false);
  };

  const featuredImagesMapper = () => {
    if(loading) return;

  }

  return (
    <UserLayout>
      <Helmet>
        <meta name="description" content={metaHelper.desc} />
      </Helmet>
      <section className={"text-black bg-white"}> {/* Hero image  */}
        <div className={"block"}>
          <div
            style={{
                backgroundImage: "linear-gradient(rgba(0, 0, 0, 0), rgba(0, 0, 0, 0.7)), url('/assets/images/Interior 4.jpg')",
            }}
            className={"flex items-end w-full text-white bg-left bg-no-repeat bg-cover lg:h-full xs:h-screen"}
          >
            <div className={"block w-full"}>
              <div className={"w-full block lg:pt-56"}>
                <div className={"w-full flex justify-between lg:pt-32 lg:pb-24 xs:pt-16"}>
                  <button className={"rounded w-auto py-5 lg:px-12 xs:px-6"}>
                      <span>
                        <img src="/assets/images/left-chevron-white.svg" className={"transform -rotate-90 lg:w-10 xs:w-6"} />
                      </span>
                  </button>
                  <button className={"rounded w-auto py-5 lg:px-12 xs:px-6"}>
                      <span>
                        <img src="/assets/images/right-chevron-white.svg" className={"transform -rotate-90 lg:w-10 xs:w-6"} />
                      </span>
                  </button>
                </div>
              </div>
              <div className={"w-full lg:px-16 lg:pt-32 xs:pt-56 lg:pb-2 xs:px-8 xs:pb-8"}>
                <span className={"font-bold leading-none text-6xl xs:text-5xl"}>
                  Save $111,111
                </span>
                <p className={"font-semibold leading-none text-palette-purple-main text-3xl xs:text-2xl"}>
                  {!loading && project.discount}% off
                </p>
              </div>
              <div className={"xs:hidden lg:block w-full block pb-6"}>
                <div className={"w-full flex justify-center space-x-2"}>
                  <button className={"rounded w-auto p-2"}>
                      <span>
                        <img src="/assets/images/checkbox-circle-purple.svg" className={"transform -rotate-90 lg:w-4"} />
                      </span>
                  </button>
                  <button className={"rounded w-auto p-2"}>
                      <span>
                        <img src="/assets/images/checkbox-circle-gray.svg" className={"transform -rotate-90 lg:w-4"} />
                      </span>
                  </button>
                  <button className={"rounded w-auto p-2"}>
                      <span>
                        <img src="/assets/images/checkbox-circle-gray.svg" className={"transform -rotate-90 lg:w-4"} />
                      </span>
                  </button>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
      <section className={"text-black bg-white"}> {/* Apartment details */}
        <div className={"block lg:pt-24 lg:pb-8 lg:w-1/2 lg:mx-auto xs:w-full xs:px-12 xs:pt-8 xs:pb-2 space-y-8"}>
          <div className={"flex w-full"}> {/* Apartment name */}
            <div className={"block w-3/4 space-y-1"}>
              <span className={"font-bold leading-none text-palette-purple-main lg:text-4xl xs:text-xl"}>
                {!loading && project.name} / Unit {!loading && apartment.unit_name}
              </span>
              <p className={"italic font-light leading-none lg:text-lg xs:text-sm text-gray-500"}>
                Estimated completion: November 2021
              </p>
            </div>
            <div className={"block w-1/4"}>
              <div className={"flex w-full justify-end items-center space-x-3"}>
                <div className="flex items-center text-3xl font-semibold">
                  <span>
                    <img src="/assets/images/heart.svg" className={"lg:w-10 xs:w-6"} />
                  </span>
                </div>
                <div className="flex items-center text-3xl font-semibold">
                  <span>
                    <img src="/assets/images/share.svg" className={"lg:w-10 xs:w-6"} />
                  </span>
                </div>
              </div>
            </div>
          </div>
          <div className={"flex w-full"}> {/* Apartment price */}
            <div className={"block w-2/5"}>
              <span className={"font-bold leading-none lg:text-5xl xs:text-2xl"}>
                {!loading && <CurrencyFormat
                  value={
                    apartment.price
                  }
                  displayType={`text`}
                  thousandSeparator={true}
                  prefix={`$`}
                />}
              </span>
            </div>
            <div className={"block w-3/5"}>
              <div className={"flex w-full justify-end items-center"}>
                <div className="flex items-center font-bold border-r lg:pr-6 lg:text-3xl xs:pr-2 xs:text-lg border-palette-purple-main">
                  {!loading && apartment.no_of_bedrooms}
                  <span>
                    <img src="/assets/images/bed.svg" className={"lg:w-10 xs:w-6 lg:ml-4 xs:ml-2"} />
                  </span>
                </div>
                <div className="flex items-center font-bold border-r lg:px-6 lg:text-3xl xs:px-2 xs:text-lg border-palette-purple-main">
                  {!loading && apartment.no_of_bathrooms}
                  <span>
                    <img src="/assets/images/shower.svg" className={"lg:w-10 xs:w-6 lg:ml-4 xs:ml-2"} />
                  </span>
                </div>
                <div className="flex items-center font-bold lg:pl-6 lg:text-3xl xs:pl-2 xs:text-lg">
                  {!loading && apartment.no_of_garages}
                  <span>
                    <img src="/assets/images/car.svg" className={"lg:w-10 xs:w-6 lg:ml-4 xs:ml-2"} />
                  </span>
                </div>
              </div>
            </div>
          </div>
          <div className={"block w-full space-y-4"}> {/* Apartment summary */}
            <div className={"flex w-auto space-x-4"}>
              <div className={"font-normal leading-none text-base"}>
                <span className={"font-bold"}>Internal:</span>{" "}50m<span className="align-top text-xxs">2</span>
              </div>
              <div className={"font-normal leading-none text-base"}>
                <span className={"font-bold"}>External:</span>{" "}10m<span className="align-top text-xxs">2</span>
              </div>
            </div>
            <div className={"block w-full space-y-4"}>
             
                {!loading &&
                  <div
                    className={`text-base description-list pb-4`}
                    dangerouslySetInnerHTML={{
                      __html: !loading && extracDescription(apartment.description)
                    }}
                  />
                }
            
            </div>
          </div>
          <div className={"lg:flex xs:block w-full lg:space-x-4 xs:space-y-8"}> {/* Features & inclusions */}
            <div className={"block lg:w-1/2 xs:w-full lg:space-y-3 xs:space-y-2"}>
              <p className={"font-bold lg:pb-2 leading-none lg:text-4xl xs:text-2xl"}>
                Features
              </p>
              {!loading && extractFeatureOrInclusion(apartment.description, true)}
            </div>
            <div className={"block lg:w-1/2 xs:w-full lg:space-y-3 xs:space-y-2"}>
              <p className={"font-bold lg:pb-2 leading-none lg:text-4xl xs:text-2xl"}>
                Inclusions
              </p>
              {!loading && extractFeatureOrInclusion(apartment.description, false)}
            </div>
          </div>
          <div className={"block w-full space-y-4"}> {/* Apartment interior pictures */}
            <div className={"block w-full space-y-4"}>
              <div className={"lg:flex xs:block w-full lg:space-x-4 xs:space-y-4"}>
                <div className={"h-auto lg:w-1/2 xs:w-full"}>
                  <div 
                    style={{
                        backgroundImage: "url('/assets/images/Interior 1.jpg')",
                    }}
                    className={"items-end bg-cover bg-center bg-no-repeat"}
                  >
                    <div className={"lg:py-32 xs:py-24"}>
                    </div>
                  </div>
                </div>
                <div className={"h-auto lg:w-1/2 xs:w-full"}>
                  <div 
                    style={{
                        backgroundImage: "url('/assets/images/Interior 2.jpg')",
                    }}
                    className={"items-end bg-cover bg-center bg-no-repeat"}
                  >
                    <div className={"lg:py-32 xs:py-24"}>
                    </div>
                  </div>
                </div>
              </div>
              <div className={"lg:flex xs:block w-full lg:space-x-4 xs:space-y-4"}>
                <div className={"h-auto lg:w-1/2 xs:w-full"}>
                  <div 
                    style={{
                        backgroundImage: "url('/assets/images/Interior 3.jpg')",
                    }}
                    className={"items-end bg-cover bg-center bg-no-repeat"}
                  >
                    <div className={"lg:py-32 xs:py-24"}>
                    </div>
                  </div>
                </div>
                <div className={"h-auto lg:w-1/2 xs:w-full"}>
                  <div 
                    style={{
                        backgroundImage: "url('/assets/images/Interior 4.jpg')",
                    }}
                    className={"items-end bg-cover bg-center bg-no-repeat"}
                  >
                    <div className={"lg:py-32 xs:py-24"}>
                    </div>
                  </div>
                </div>
              </div>
              <div className={"lg:flex xs:block w-full lg:space-x-4 xs:space-y-4"}>
                <div className={"h-auto lg:w-1/2 xs:w-full"}>
                  <div 
                    style={{
                        backgroundImage: "url('/assets/images/Interior 5.jpg')",
                    }}
                    className={"items-end bg-cover bg-center bg-no-repeat"}
                  >
                    <div className={"lg:py-32 xs:py-24"}>
                    </div>
                  </div>
                </div>
                <div className={"h-auto lg:w-1/2 xs:w-full"}>
                  <div 
                    style={{
                        backgroundImage: "url('/assets/images/Interior 6.jpg')",
                    }}
                    className={"items-end bg-cover bg-center bg-no-repeat"}
                  >
                    <div className={"lg:py-32 xs:py-24"}>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div className={"block w-full"}>
              <p className={"italic font-light lg:text-base xs:text-xs text-gray-700"}>
                *Property images are for illustration purposes only and may not be specific to your particular property. Property sizes and outdoing costs are estimates only.
              </p>
            </div>
          </div>
          <div className={"block w-full space-y-2"}> {/* Buttons */}
            <div className={"block w-full"}>
              <button className={"rounded text-white bg-palette-purple-main lg:text-2xl xs:text-sm w-full font-bold py-5 px-3"}>Secure this deal now for only $1,000</button>
            </div>
            <div className={"block w-full"}>
              <button className={"rounded text-palette-purple-main lg:text-2xl xs:text-sm w-full font-bold py-5 px-3"}>Back to project details</button>
            </div>
          </div>
        </div>
      </section>
      {!loading && <GoogleMapsNew project={project} />}
      <section className={"text-black bg-white"}> {/* Apartment details */}
        <div className={"lg:flex xs:block w-full h-auto"}>
          <InspectProperty />
          <TalkExpert />
        </div>
      </section>
      <TheMore bgColor={`#4D0B98`} />
      <JoinUs />
      <SocialMediaShareModal />
    </UserLayout>
  );
};

export default NewBuyingApartmentDetailsPage;
