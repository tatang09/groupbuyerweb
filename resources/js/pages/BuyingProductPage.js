import React, { useState, useEffect, useRef } from "react";

import UserLayout from "~/layouts/users/UserLayout";
import UserGlobal from "~/states/userGlobal";
import {
    SocialMediaShareModal,
    TheMore,
    RegistrationForm,
    PieceMeal,
    InspectProperty,
    TalkExpert,
    GoogleMapsNew,
    JoinUs,
    NeighbourhoodInsights,
    LocalSchoolCatchments
} from "~/components";
import { isEmpty } from "lodash";
import { isLoggedIn } from "~/services/auth";
import { Helmet } from "react-helmet";
import { metaHelper } from "../helpers/metaHelper/metaHelper";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { isMobile } from "react-device-detect";
import CurrencyFormat from "react-currency-format";
import { discountedPrice } from "../helpers/pricingHelper/discountHelper";

const BuyingProductPage = ({ dealId }) => {
    const [userState, userAction] = UserGlobal();
    const [loading, setLoading] = useState(true);
    const [loadSummary, setLoadSummary] = useState(false);
    const [project, setProject] = useState();


    const anchorRef = useRef(null);

    useEffect(() => {

    });

    const truncateString = (string, limit) => {
        if (string.length > limit) {
            return string.substring(0, limit) + "..."
        } else {
            return string
        }
    }

    const getDeal = async () => {
        let deal = await axios.get(`/api/deal/${dealId}`);

        if (deal.data && deal.data.expires_at) {
            let exp = JSON.parse(deal.data.expires_at);
            setTimeLeft(exp.time_limit);
        }

        // getDeveloperById(deal.data.user_id).then(res => {
        //     if (res.data.company_logo_path) {
        //         setDeveloperLogo(res.data.company_logo_path);
        //         setDeveloperURl(res.data.company_url);
        //     }
        // });

        if (isLoggedIn() && !isEmpty(userState.user)) {
            const { data } = await axios.get(`/api/wish-list/${dealId}`, {
                headers: {
                    Authorization: "Bearer " + isLoggedIn()
                }
            });

            let newProject = Object.assign({}, deal.data);

            if (data && !isEmpty(newProject)) {
                data.map(d => {
                    newProject.properties.map(p => {
                        if (p.id == d.property_id) {
                            p.hasWishlist = true;
                            p.wishListId = d.id;
                        }
                    });
                });
            }
            setProject(newProject);
        } else {
            setProject(deal.data);
        }
        setLoading(false);
    };

    useEffect(() => {
        getDeal();
    }, [userState.user]);

    const handleWishlist = async property => {
        if (!isLoggedIn()) {
            userAction.setState({ showSignIn: true });
            return;
        }

        let formData = new FormData();

        formData.append("property_id", property.id);
        formData.append("deal_id", dealId);

        try {
            if (!property.hasWishlist) {
                await axios.post("/api/wish-list", formData, {
                    headers: {
                        Authorization: "Bearer " + isLoggedIn()
                    }
                });
            } else {
                await axios.delete(`/api/wish-list/${property.wishListId}`, {
                    headers: {
                        Authorization: "Bearer " + isLoggedIn()
                    }
                });
            }
            getDeal();
        } catch (error) { }
    };

    const renderApartments = () => {
        return (
            !loading &&
            Object.keys(project).length !== 0 &&
            project.properties.map((property, key) => {
                return (
                    <li key={key} className={`lg:px-3 lg:w-4/12 mb-6 relative w-full`}>
                        <div
                            className={`bg-white lg:px-5 px-3 py-4 relative rounded-md`}
                            style={{ boxShadow: "0 5px 28px 4px rgba(0,0,0,0.1)" }}
                        >
                            {property.is_secured && (
                                <div
                                    className={`rounded-md absolute inset-0 z-20 bg-gray-900 bg-opacity-75 flex items-center justify-center`}
                                >
                                    <div
                                        className={`uppercase font-bold text-4xl text-white flex items-center`}
                                    >
                                        <span
                                            className={`w-12 h-12 rounded-full mr-4 flex items-center justify-center`}
                                            style={{ background: "#89b036" }}
                                        >
                                            <svg
                                                className={`feather-icon h-8 w-8`}
                                                style={{ strokeWidth: 4 }}
                                            >
                                                <use
                                                    xlinkHref={`/assets/svg/feather-sprite.svg#check`}
                                                />
                                            </svg>
                                        </span>
                                    Deal Secured
                                </div>
                                </div>
                            )}
                            <div

                            >
                                <div
                                    className={`flex items-center justify-between px-2 py-2`}
                                >
                                    <span className={`text-2xl text-palette-purple-main`}>
                                        Unit {property.unit_name}
                                    </span>
                                </div>
                                <div
                                    className={`bg-center bg-cover relative rounded text-white`}
                                    style={{
                                        minHeight: isMobile ? 250 : 300,
                                        background: `linear-gradient(rgba(0, 0, 0, 0.1) 0%, rgba(0, 0, 0, 0.2) 80%, rgb(0, 0, 0) 100%), url("${property.featured_images[0]}")`
                                    }}
                                >
                                    <div className={`absolute flex px-5 py-3 right-0 top-0`}>
                                        <div className={`rounded-full items-center justify-center flex mr-3 text-gray-500`}>
                                            <img src={`/assets/images/icons/purple_heart.png`} />
                                        </div>
                                        <div className={`rounded-full items-center justify-center flex text-gray-500`}>
                                            <img src={`/assets/images/icons/share.png`} />
                                        </div>
                                    </div>
                                    <div className={`absolute bottom-0 flex py-3 px-5 w-full`}>
                                        <div className={`flex-1 flex items-end text-red-700 line-through`}>
                                            <CurrencyFormat
                                                value={
                                                    property.price
                                                }
                                                displayType={`text`}
                                                thousandSeparator={true}
                                                prefix={`$`}
                                            />
                                        </div>
                                        <div className={`flex flex-1 font-bold items-center justify-end text-2xl`}>
                                            <CurrencyFormat
                                                value={
                                                    property.discounted_price || 0
                                                }
                                                displayType={`text`}
                                                thousandSeparator={true}
                                                prefix={`$`}
                                            />
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className={`py-4`}>
                                <div className={`px-2 flex items-center text-gray-900 text-justify`}>
                                    <div className={`text-base`} >
                                        <p>
                                            Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                                            Morbi ut turpis in velit ultricies dignissim.
                                            Morbi viverra libero quis est porttitor tincidunt.
                                        </p>
                                    </div>
                                </div>
                                <div className={` pb-4 px-2 flex items-center text-gray-900 text-justify`}>
                                    <div
                                        className={`lg:w-1/3`}
                                    >
                                        <span className={`mr-3`}>
                                            <span className={`font-semibold text-sm`}>Internal:{" "}</span>50m2
                                        </span>
                                    </div>
                                    <div
                                        className={`lg:w-1/3`}
                                    >
                                        <span className={`mr-3`}>
                                            <span className={`font-semibold text-sm`}>External:{" "}</span>10m2
                                        </span>
                                    </div>
                                </div>

                                <div className={` pb-4 px-2 flex items-center text-gray-900 text-justify`}>
                                    <div className="flex items-center font-bold border-r lg:pr-6 lg:text-3xl xs:pr-2 xs:text-lg border-palette-purple-main">
                                        {property.no_of_bedrooms}{" "}
                                        <span>
                                            <img src="/assets/images/bed.svg" className={"lg:w-10 xs:w-6 lg:ml-4 xs:ml-2"} />
                                        </span>
                                    </div>
                                    <div className="flex items-center font-bold border-r lg:px-6 lg:text-3xl xs:px-2 xs:text-lg border-palette-purple-main">
                                        {property.no_of_bathrooms}{" "}
                                        <span>
                                            <img src="/assets/images/shower.svg" className={"lg:w-10 xs:w-6 lg:ml-4 xs:ml-2"} />
                                        </span>
                                    </div>
                                    <div className="flex items-center font-bold lg:pl-6 lg:text-3xl xs:pl-2 xs:text-lg">
                                        {property.no_of_garages}{" "}
                                        <span>
                                            <img src="/assets/images/car.svg" className={"lg:w-10 xs:w-6 lg:ml-4 xs:ml-2"} />
                                        </span>
                                    </div>
                                </div>

                                <div className={`w-full px-2`}>
                                    <button
                                        className={`bg-palette-purple-main lg:py-2 lg:rounded py-3 rounded-sm text-white w-full`}
                                        onClick={() => {
                                            window.location = `/weekly-deals/${dealId}/apartment/${property.id}`;
                                        }}
                                    >
                                        See More
                                    </button>
                                </div>
                            </div>
                        </div>
                    </li>
                );
            })
        );
    };



    return (
        <UserLayout>
            <Helmet>
                <meta name="description" content={metaHelper.desc} />
            </Helmet>
            <section className="text-black bg-white">
                <section>
                    <div className="lg:grid lg:grid-cols-3 xs:block">
                        <div className="lg:col-span-2 xs:block">
                            <div
                                style={{
                                    backgroundImage: "linear-gradient(rgba(0, 0, 0, 0), rgba(0, 0, 0, 0.8)), url('/assets/images/homepage_carrousel_b_4.jpg')",
                                }}
                                className="flex items-end w-full text-white bg-left bg-no-repeat bg-cover lg:h-full xs:h-screen"
                            >
                                <div className="w-full px-8 pb-8 lg:space-y-4 xs:space-y-3 xs:pt-56 lg:pb-12 lg:px-16 lg:pt-0">
                                    <p className={"font-bold leading-none lg:text-6xl xs:text-5xl"}>
                                        Sunny Cove
                                    </p>
                                    <p className={"font-bold leading-none lg:text-3xl xs:text-2xl"}>
                                        13% off
                                    </p>
                                    <div className="flex items-center w-full">
                                        <div className="items-center block border-r lg:text-base xs:text-sm lg:pr-6 xs:pr-4 border-palette-purple-main">
                                            <div className="font-bold text-palette-purple-main">
                                                Goal
                                            </div>
                                            <div className="font-normal text-white">
                                                20 investors
                                            </div>
                                        </div>
                                        <div className="items-center block border-r lg:text-base xs:text-sm lg:px-6 xs:px-4 border-palette-purple-main">
                                            <div className="font-bold text-palette-purple-main">
                                                Current
                                            </div>
                                            <div className="font-normal text-white">
                                                12 investors
                                            </div>
                                        </div>
                                        <div className="items-center block lg:text-base xs:text-sm lg:pl-6 xs:pl-4">
                                            <div className="font-bold text-palette-purple-main">
                                                To Go
                                            </div>
                                            <div className="font-normal text-white">
                                                8 investors
                                            </div>
                                        </div>
                                    </div>
                                    {loadSummary && (
                                        <p className="font-normal lg:text-base xs:text-sm">
                                            Dolor sit amet, consectetur adipiscing elit. Morbi ut turpis in velit ultricies dignissim. Morbi viverra libero quis est porttitor tincidunt. Nunc in metus sit amet felis feugiat tincidunt sit amet quis lectus. Vivamus tincidunt a ligula a malesuada. Aenean viverra lacus nulla, sit amet bibendum ipsum suscipit sit amet.
                                        </p>
                                    )}
                                    <div className="flex items-center justify-between w-full pt-2">
                                        <button className="py-3 text-xs font-semibold text-white whitespace-pre rounded lg:px-12 xs:px-6" style={{ backgroundColor: '#E91AFC' }}
                                            onClick={() => {
                                                setLoadSummary(!loadSummary)
                                            }}
                                        >
                                            {loadSummary ? "Read less" : "Read more"}
                                        </button>
                                        <div className="flex">
                                            <button className={"rounded w-auto py-4 lg:px-6 xs:px-4"}>
                                                <span>
                                                <img src="/assets/images/left-chevron-white.svg" className={"transform -rotate-90 lg:w-5 xs:w-4"} />
                                                </span>
                                            </button>
                                            <button className={"rounded w-auto py-4 lg:px-6 xs:px-4"}>
                                                <span>
                                                <img src="/assets/images/right-chevron-white.svg" className={"transform -rotate-90 lg:w-5 xs:w-4"} />
                                                </span>
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div className="lg:col-span-1 xs:block">
                            <div className={"h-full items-end bg-cover text-white bg-left bg-no-repeat flex w-full bg-palette-violet-main-dark"}>
                                <div className={"block lg:pr-20 lg:pl-16 lg:pt-48 lg:pb-32 xs:px-8 xs:pt-12 xs:pb-8 space-y-4"}>
                                    <div className={"lg:block xs:flex xs:flex-row-reverse lg:space-y-3 xs:justify-between text-xs font-normal"}>
                                        <div className={"lg:w-full xs:w-1/3"}>
                                            <div className={"flex xs:justify-end"}>
                                                <div className={"flex items-center font-semibold text-xs px-3 py-1 bg-palette-purple-main rounded"}>
                                                    Express
                                                    <span>
                                                        <img src={"/assets/images/running.svg"} className={"w-5 ml-2"} />
                                                    </span>
                                                </div>
                                            </div>
                                        </div>
                                        <div className={"lg:w-full xs:w-2/3"}>
                                            <div className={"font-bold leading-none lg:text-5xl xs:text-3xl"}>
                                                Property Features
                                            </div>
                                        </div>
                                    </div>
                                    <p className={"font-semibold leading-none text-xs text-palette-purple-main uppercase xs:w-3/4"}>
                                        Spectacular brand new contemporary home
                                    </p>
                                    <div className={"block space-y-3 text-xs font-normal"}>
                                        <div className={"flex"}>
                                            <span>
                                                <img src={"/assets/images/chevron-right.svg"} className={"w-2 mr-2"} />
                                            </span>
                                            SMEG appliances throughout
                                        </div>
                                        <div className={"flex"}>
                                            <span>
                                                <img src={"/assets/images/chevron-right.svg"} className={"w-2 mr-2"} />
                                            </span>
                                            European inspired timber floors
                                        </div>
                                        <div className={"flex"}>
                                            <span>
                                                <img src={"/assets/images/chevron-right.svg"} className={"w-2 mr-2"} />
                                            </span>
                                            Architecturally designed open spaces
                                        </div>
                                        <div className={"flex"}>
                                            <span>
                                                <img src={"/assets/images/chevron-right.svg"} className={"w-2 mr-2"} />
                                            </span>
                                            Ducted day-night air-condition
                                        </div>
                                        <div className={"flex"}>
                                            <span>
                                                <img src={"/assets/images/chevron-right.svg"} className={"w-2 mr-2"} />
                                            </span>
                                            Spacious entertainers` balcony
                                        </div>
                                        <div className={"flex"}>
                                            <span>
                                                <img src={"/assets/images/chevron-right.svg"} className={"w-2 mr-2"} />
                                            </span>
                                            1 car space and private storage
                                        </div>
                                        <div className={"flex"}>
                                            <span>
                                                <img src={"/assets/images/chevron-right.svg"} className={"w-2 mr-2"} />
                                            </span>
                                            Access to the rooftop terrace and BBQ
                                        </div>
                                        <div className={"flex"}>
                                            <span>
                                                <img src={"/assets/images/chevron-right.svg"} className={"w-2 mr-2"} />
                                            </span>
                                            Access to pool and residence lounge
                                        </div>
                                        <div className={"flex"}>
                                            <span>
                                                <img src={"/assets/images/chevron-right.svg"} className={"w-2 mr-2"} />
                                            </span>
                                            Great connectivity with local amenities
                                        </div>
                                        <div className={"flex"}>
                                            <span>
                                                <img src={"/assets/images/chevron-right.svg"} className={"w-2 mr-2"} />
                                            </span>
                                            Designed by award-winning architects
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <section className="lg:flex xs:block">
                    <div className="lg:w-1/2 xs:w-full">
                        {!loading && <GoogleMapsNew project={project} />}
                    </div>
                    <NeighbourhoodInsights />
                </section>
                <section className="lg:flex xs:block">
                    <div className="lg:w-1/2 xs:w-full xs:hidden lg:flex lg:items-center" style={{backgroundColor: "#4D0B98"}}>
                        <RegistrationForm
                            subHeaderColor="text-white"
                            headerColor="text-palette-purple-main"
                            isPopup={false}
                            opacity={"100%"}
                            containerBgColor={"#4D0B98"}
                        />
                    </div>
                    <LocalSchoolCatchments />
                </section>
                <section className={"text-black bg-white"}> {/* Apartment details */}
                    <div className={"lg:flex xs:block w-full h-auto"}>
                        <InspectProperty />
                        <TalkExpert />
                    </div>
                </section>
                <TheMore bgColor={`#4D0B98`} />
                <section className={`relative bg-white`}>
                    <div
                        className={`pb-4 px-6 lg:px-0 mx-auto`}
                        style={{ maxWidth: 1366 }}
                    >
                        <h4 className={`text-center my-8 text-palette-purple-main text-base font-extrabold`}>{!loading && project.name}</h4>
                        <h1
                            className={`text-left text-center leading-none font-bold text-4xl lg:text-5xl px-8 mb-4 lg:mb-10`}
                        >
                            Available properties
                        </h1>
                        <p className={`text-justify mb-5  lg:px-32 lg:mb-16`}>
                            Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
                            eiusmod tempor incididunt ut labore et dolore magna aliqua. Mi
                            proin sed libero enim sed faucibus turpis. Amet consectetur
                            adipiscing elit ut aliquam purus sit.
                        </p>

                        <ul className={`flex flex-wrap mt-6`}>{renderApartments()}</ul>
                    </div>
                </section>
                <PieceMeal />
                <JoinUs />
            </section>
            <SocialMediaShareModal />
        </UserLayout>
    );
};

const apartmentSettings = {
    dots: false,
    arrows: false,
    infinite: true,
    autoplay: true,
    autoplaySpeed: 5000,
    slidesToShow: 1,
    responsive: [
        {
            breakpoint: 1024,
            settings: {
                slidesToShow: 3,
                slidesToScroll: 3,
                infinite: true,
                dots: true
            }
        },
        {
            breakpoint: 600,
            settings: {
                slidesToShow: 2,
                slidesToScroll: 2
            }
        },
        {
            breakpoint: 480,
            settings: {
                slidesToShow: 1,
                slidesToScroll: 1
            }
        }
        // You can unslick at a given breakpoint now by adding:
        // settings: "unslick"
        // instead of a settings object
    ]
};

export default BuyingProductPage;
