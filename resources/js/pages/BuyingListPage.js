import React, { useState, useEffect, useRef } from "react";

import UserLayout from "~/layouts/users/UserLayout";
import UserGlobal from "~/states/userGlobal";
import {
  BuyingProgress,
  SocialMediaShareModal,
  Testimonials,
  TheMore,
  BenefitsForBuyers,
  RegistrationForm
} from "~/components";
import { Table } from "~/components/_base";

import {
  discountedPriceRange,
  prices
} from "../helpers/pricingHelper/discountHelper";

import {
  ClickAwayListener,
  Collapse,
  Popper,
  Grow,
  MenuList,
  MenuItem,
  Paper
} from "@material-ui/core";
import { isMobile } from "react-device-detect";
import CurrencyFormat from "react-currency-format";
import Select from "react-select";

import { Helmet } from "react-helmet";
import { metaHelper } from "../helpers/metaHelper/metaHelper";

const BuyingListPage = () => {
  const [userState, userAction] = UserGlobal();
  const [keyword, setKeyword] = useState("");
  const [popper, setPopper] = useState(null);
  const [openSearch, setOpenSearch] = useState(false);
  const [toggleFetch, setToggleFetch] = useState(false);
  const [priceDropdown, setPriceDropdown] = useState(false);
  const [projectData, setProjectData] = useState([]);
  const [options, setOptions] = useState([]);
  const [priceOptions, setPriceOptions] = useState([]);
  const [filters, setFilters] = useState({
    sortBy: { value: "deals.created_at", order: "desc", label: "Most Recent" },
    minPrice: { value: 0, label: "Any" },
    maxPrice: { value: 15000000, label: "Any" },
    types: "All",
    selectedTypes: { value: "All", label: "All" },
    locations: "All",
    selectedLocations: { value: "All", label: "All" }
  });

  const anchorRef = useRef(null);

  useEffect(() => {
    if (localStorage.getItem("__use_projects_filters")) {
      let savedFilters = JSON.parse(localStorage.getItem("__projects_filters"));

      setFilters(savedFilters);
      setToggleFetch(!toggleFetch);
      localStorage.setItem("__use_projects_filters", "");
    }

    const getType = async () => {
      let { data } = await axios.get("/api/sub-property-type");

      let $typeOptions = data.map(type => ({
        value: type.id,
        label: type.name
      }));
      $typeOptions.unshift({ value: "All", label: "All" });
      setOptions($typeOptions);
    };

    const getPriceOptions = () => {
      let options = [];

      prices.map(price => {
        options.push({
          value: price,
          label: new Intl.NumberFormat("en-AU", {
            style: "currency",
            currency: "AUD",
            minimumFractionDigits: 0
          }).format(price || 0)
        });
      });

      setPriceOptions(options);
    };

    getType();
    getPriceOptions();
  }, []);

  const handleSortBy = selectedItem => {
    setFilters({ ...filters, sortBy: selectedItem });
    setToggleFetch(!toggleFetch);
  };

  const handleMinPrice = selectedItem => {
    if (!selectedItem) {
      setFilters({ ...filters, minPrice: { value: 0, label: "Any" } });
      setToggleFetch(!toggleFetch);
      return;
    }

    setFilters({ ...filters, minPrice: selectedItem });
    setToggleFetch(!toggleFetch);
  };

  const handleMaxPrice = selectedItem => {
    if (!selectedItem) {
      setFilters({ ...filters, maxPrice: { value: 15000000, label: "Any" } });
      setToggleFetch(!toggleFetch);
      return;
    }

    setFilters({ ...filters, maxPrice: selectedItem });
    setToggleFetch(!toggleFetch);
  };

  const handleSelectType = selectedItem => {
    if (!selectedItem || selectedItem.length === 0) {
      setFilters({
        ...filters,
        types: "All",
        selectedTypes: { value: "All", label: "All" }
      });
      setToggleFetch(!toggleFetch);
      return;
    }

    let types = [];
    let lastSelected = selectedItem[selectedItem.length - 1];
    let filteredTypes = selectedItem.filter(item => item.value !== "All");

    if (lastSelected.value === "All") {
      setFilters({
        ...filters,
        types: lastSelected.value,
        selectedTypes: lastSelected
      });
      setToggleFetch(!toggleFetch);
      return;
    }

    filteredTypes.map(type => {
      types.push(type.value);
    });

    setFilters({
      ...filters,
      types: types,
      selectedTypes: filteredTypes
    });
    setToggleFetch(!toggleFetch);
  };

  const handleSelectLocation = selectedItem => {
    if (!selectedItem || selectedItem.length === 0) {
      setFilters({
        ...filters,
        locations: "All",
        selectedLocations: { value: "All", label: "All" }
      });
      setToggleFetch(!toggleFetch);
      return;
    }

    let locations = [];
    let lastSelected = selectedItem[selectedItem.length - 1];
    let filteredLocations = selectedItem.filter(item => item.value !== "All");

    if (lastSelected.value === "All") {
      setFilters({
        ...filters,
        locations: lastSelected.value,
        selectedLocations: lastSelected
      });
      setToggleFetch(!toggleFetch);
      return;
    }

    filteredLocations.map(city => {
      locations.push(city.value);
    });

    setFilters({
      ...filters,
      locations: locations,
      selectedLocations: filteredLocations
    });
    setToggleFetch(!toggleFetch);
  };

  const handleSelectProject = url => {
    // if (!isLoggedIn()) {
    //   userAction.setState({ showSignIn: true });
    //   return;
    // }
    localStorage.setItem("__projects_filters", JSON.stringify(filters));
    window.location = url;
  };

  const renderProjects = () => {
    return (
      <tbody className={`flex flex-wrap`}>
        {projectData &&
          projectData.map((project, key) => {
            return (
              <tr key={key} className={`lg:w-4/12 mb-4 lg:mb-4 lg:px-3 w-full`}>
                <>
                  <td
                    style={{ boxShadow: "2px 2px 5px 0px rgb(0 0 0 / 10%), 2px 2px 5px 0px rgb(0 0 0 / 6%)" }}
                    className={`p-5 block bg-white rounded-lg lg:hover:scale-105 p-0`}
                  //className={`p-4 block bg-white rounded-md lg:hover:scale-105 duration-300 transform transition-all p-0`}
                  >
                    <div
                      className={`${key % 5 === 0 && key !== 0
                        ? "text-palette-gray-main"
                        : "text-palette-violet-main-dark"
                        } text-2xl font-bold mb-3 leading-none py-3`}
                    >
                      {project.name}
                    </div>
                    <div
                      className={`cursor-pointer`}
                      onClick={() =>
                        handleSelectProject(
                          `/weekly-deals/${project.id}`
                        )
                      }
                    >
                      <div
                        className={`bg-center bg-cover relative text-white`}
                        style={{
                          minHeight: isMobile ? 225 : 180,
                          background: `linear-gradient(rgba(0, 0, 0, 0.9) 0%,rgba(0, 0, 0, 0.1) 25%,rgba(0, 0, 0, 0.1) 0%, rgba(0, 0, 0, 0.2) 82%, rgba(0, 0, 0,0.8) 100%), url("${project.featured_images[0]}")`
                        }}
                      >
                        {key % 5 === 0 && key !== 0 && (
                          <div
                            className={`absolute left-0 mt-3 overflow-hidden rounded-sm top-0`}
                          >
                            <div className={`flex items-center ml-3`}>
                              <img
                                src={`assets/svg/sold_out.svg`}
                                className={`w-2/3`}
                              />
                            </div>
                          </div>
                        )}

                        {key % 3 === 0 && (
                          <div
                            className={`absolute left-0 mt-3 overflow-hidden rounded-sm top-0`}
                          >
                            <div className={`flex items-center ml-3`}>
                              <img
                                src={`assets/svg/premium.svg`}
                                className={`w-3/4`}
                              />
                            </div>
                          </div>
                        )}

                        {project.discount && (
                          <div
                            className={`absolute top-0 right-0 mt-2 text-xl top-0`}
                          >
                            <div
                              className={`bg-transparent flex items-center mr-3`}
                            >
                              <span className={`text-base font-semibold`}>
                                {`${project.discount}% `}
                              </span>
                              <span className={`text-base font-semibold `}>
                                &nbsp;off
                              </span>
                            </div>
                          </div>
                        )}

                        <div
                          className={`absolute bottom-0 right-0 mb-2 mr-4 flex items-center text-sm font-bold`}
                        >
                          <span>From&nbsp;</span>
                          <CurrencyFormat
                            value={
                              project && discountedPriceRange(project, false)
                            }
                            displayType={`text`}
                            thousandSeparator={true}
                            prefix={`$`}
                          />
                        </div>
                      </div>
                    </div>
                    <div className={`pt-5 pb-0 px-2`}>
                      <p className={`text-xs text-gray-900 font-light text-justify text-justify`}>
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit,
                        sed do eiusmod tempor incididunt ut labore et dolore
                        magna aliqua.
                      </p>
                    </div>
                    <div
                      className={`text-sm pb-4 px-2 flex justify-center items-center text-gray-900 text-justify`}
                    >
                      <div
                        className={`w-1/3`}
                      >
                        <h4
                          className={`text-palette-purple-main font-extrabold`}
                        >
                          Goal
                        </h4>
                        <h5 className={`font-light`}>15 Investors</h5>
                      </div>
                      <div
                        className={`w-1/3 flex justify-center border-r border-l border-palette-purple-main`}
                      >
                        <div>
                          <h4
                            className={`text-palette-purple-main font-extrabold`}
                          >
                            Current
                          </h4>{" "}
                          <h5 className={`font-light`}>8 Investors </h5>
                        </div>
                      </div>
                      <div className={`w-1/3 flex justify-end`}>
                        <div>
                          <h4
                            className={`text-palette-purple-main font-extrabold`}
                          >
                            To Go
                          </h4>{" "}
                          <h5 className={`font-light`}>8 Investors </h5>
                        </div>
                      </div>
                    </div>

                    <div className={`w-full px-2`}>
                      <button
                        className={`bg-palette-violet-main-dark lg:py-2 lg:rounded py-3 rounded-sm text-white w-full`}
                        onClick={() => {
                          window.location = `/weekly-deals/${project.id}`;
                        }}
                      >
                        Explore
                      </button>
                    </div>
                    <div className={`pt-3 pb-4 px-5 hidden`}>
                      <div
                        className={`flex items-center justify-between border-b pb-2 mb-2`}
                      >
                        <span
                          className={`text-base font-semibold text-gray-900`}
                        >
                          <CurrencyFormat
                            value={
                              project && discountedPriceRange(project, false)
                            }
                            displayType={`text`}
                            thousandSeparator={true}
                            prefix={`$`}
                          />
                          <span> - </span>
                          <CurrencyFormat
                            value={
                              project && discountedPriceRange(project, true)
                            }
                            displayType={`text`}
                            thousandSeparator={true}
                            prefix={`$`}
                          />
                        </span>
                        <span
                          className={`text-red-600 text-base font-semibold ${project.properties.filter(
                            property => property.is_secured === false
                          ).length < 2
                            ? "text-red-700"
                            : ""
                            }`}
                        >
                          {project.properties.filter(
                            property => property.is_secured === false
                          ).length !== 0
                            ? project.properties.filter(
                              property => property.is_secured === false
                            ).length
                            : ""}

                          {project.properties.filter(
                            property => property.is_secured === false
                          ).length > 1
                            ? " Available"
                            : project.properties.filter(
                              property => property.is_secured === false
                            ).length === 0
                              ? "Sold out"
                              : " Remaining"}
                        </span>
                      </div>
                      <div
                        className={`flex items-center justify-between text-gray-900 font-black`}
                      >
                        <span className={`text-base`}>{project.deal_type}</span>
                        <div className={`text-base flex`}>
                          <span
                            className={`cursor-pointer`}
                            onClick={() =>
                              userAction.setState({ showSocialMedia: true })
                            }
                          >
                            <svg className={`feather-icon h-5 w-5`}>
                              <use
                                xlinkHref={`/assets/svg/feather-sprite.svg#share`}
                              />
                            </svg>
                          </span>
                        </div>
                      </div>
                    </div>
                  </td>
                </>
              </tr>
            );
          })}
      </tbody>
    );
  };

  const renderFilters = () => {
    return (
      <div
        className={`
        lg:flex-row lg:px-16 lg:m-0
        flex-col flex mt-8 mb-10 text-gray-600 px-8 items-center justify-center relative
      `}
      >
        <div
          className={`flex-1 mb-4 lg:mb-0 w-full`}
          style={{ maxWidth: isMobile ? "100%" : "25%" }}
        >
          <div
            className={`lg:block uppercase font-bold px-3 text-xs lg:absolute`}
            style={{ top: "-24px" }}
          >
            Location
          </div>
          <div className={`border-2 w-full h-full flex items-center`}>
            <Select
              isMulti
              isClearable={false}
              closeMenuOnSelect={false}
              hideSelectedOptions={false}
              isSearchable={false}
              options={[
                { value: "All", label: "All" },
                { value: "Adelaide", label: "Adelaide" },
                { value: "Brisbane", label: "Brisbane" },
                { value: "Gold Coast", label: "Gold Coast" },
                { value: "Melbourne", label: "Melbourne" },
                { value: "Newcastle", label: "Newcastle" },
                { value: "Sydney", label: "Sydney" }
              ]}
              classNamePrefix={`input-select`}
              className={`gb-multi-select h-full w-full`}
              placeholder={`Select Location`}
              value={filters.selectedLocations}
              onChange={handleSelectLocation}
            />
          </div>
        </div>

        <div
          className={`flex-1 mb-4 lg:mb-0 w-full`}
          style={{ maxWidth: isMobile ? "100%" : "25%" }}
        >
          <div
            className={`lg:block uppercase font-bold px-3 text-xs lg:absolute`}
            style={{ top: "-24px" }}
          >
            Type
          </div>
          <div
            className={`border-2 lg:border-l-0 w-full h-full flex items-center`}
          >
            <Select
              isMulti
              isClearable={false}
              closeMenuOnSelect={false}
              hideSelectedOptions={false}
              isSearchable={false}
              options={options}
              classNamePrefix={`input-select`}
              className={`gb-multi-select h-full w-full`}
              placeholder={`Select Type`}
              value={filters.selectedTypes}
              onChange={handleSelectType}
            />
          </div>
        </div>

        <div
          className={`relative flex-1 mb-4 lg:mb-0 w-full`}
          style={{ maxWidth: isMobile ? "100%" : "25%" }}
        >
          <div
            className={`lg:block uppercase font-bold px-3 text-xs lg:absolute`}
            style={{ top: "-24px" }}
          >
            Price
          </div>
          <div
            className={`border-2 lg:border-l-0 w-full h-full flex items-center cursor-pointer relative`}
            onClick={() => setPriceDropdown(!priceDropdown)}
          >
            <div
              className={`px-3 py-2 text-base text-gray-500 w-full flex items-center justify-between`}
            >
              <div className={`flex items-center`} style={{ height: 36 }}>
                {filters.minPrice.label} ~ {filters.maxPrice.label}
              </div>
              <svg
                className={`feather-icon opacity-50 hover:opacity-100 ${priceDropdown ? "opacity-100" : ""
                  }`}
                style={{
                  color: "hsl(0, 0%, 60%)",
                  strokeWidth: "3px",
                  width: 18,
                  height: 18
                }}
              >
                <use
                  xlinkHref={`/assets/svg/feather-sprite.svg#chevron-down`}
                />
              </svg>
            </div>
          </div>
          {priceDropdown && (
            <ClickAwayListener
              onClickAway={() => setPriceDropdown(!priceDropdown)}
            >
              <div
                className={`absolute bg-white border-2 p-2 z-50`}
                style={{
                  top: "calc(100% - 2px)",
                  width: isMobile ? "100%" : "calc(100% + 2px)",
                  left: isMobile ? 0 : "-2px"
                }}
              >
                <div className={`flex items-center mb-2`}>
                  <span className={`font-bold mr-1 w-16`}>Min Price</span>
                  <Select
                    hideSelectedOptions={false}
                    isSearchable={false}
                    isClearable={true}
                    options={priceOptions}
                    classNamePrefix={`input-select`}
                    className={`gb-price-select flex-1 ml-2`}
                    placeholder={`Min Price`}
                    defaultValue={filters.minPrice}
                    onChange={handleMinPrice}
                  />
                </div>
                <div className={`flex items-center`}>
                  <span className={`font-bold mr-1 w-16`}>Max Price</span>
                  <Select
                    hideSelectedOptions={false}
                    isSearchable={false}
                    isClearable={true}
                    options={priceOptions}
                    classNamePrefix={`input-select`}
                    className={`gb-price-select flex-1 ml-2`}
                    placeholder={`Max Price`}
                    defaultValue={filters.maxPrice}
                    onChange={handleMaxPrice}
                  />
                </div>
              </div>
            </ClickAwayListener>
          )}
        </div>

        {!isMobile && (
          <div
            className={`flex-1 w-full`}
            style={{ maxWidth: isMobile ? "100%" : "25%" }}
          >
            <div
              className={`hidden lg:block uppercase font-bold px-3 text-xs absolute`}
              style={{ top: "-24px" }}
            >
              Sort By
            </div>
            <div
              className={`border-2 lg:border-l-0 w-full h-full flex items-center`}
            >
              <Select
                hideSelectedOptions={false}
                isSearchable={false}
                options={[
                  {
                    value: "deals.created_at",
                    order: "desc",
                    label: "Most Recent"
                  },
                  {
                    value: "deals.discount",
                    order: "desc",
                    label: "Biggest Discount"
                  },
                  {
                    value: "deals.expires_at",
                    order: "asc",
                    label: "Closing Soon"
                  },
                  {
                    value: "asc_properties_total_price",
                    order: "asc",
                    label: "Price (low-high)"
                  },
                  {
                    value: "desc_properties_total_price",
                    order: "desc",
                    label: "Price (high-low)"
                  }
                ]}
                classNamePrefix={`input-select`}
                className={`gb-multi-select h-full w-full`}
                placeholder={`Sort By`}
                value={filters.sortBy}
                onChange={handleSortBy}
              />
            </div>
          </div>
        )}
      </div>
    );
  };

  return (
    <UserLayout>
      <Helmet>
        <meta name="description" content={metaHelper.desc} />
      </Helmet>
      <section className={`text-black bg-white`}>
        <section>
          <div className="lg:grid lg:grid-cols-8 xs:block">
            <div className="lg:col-span-5 xs:block">
              <div
                style={{
                    backgroundImage: "linear-gradient(rgba(0, 0, 0, 0), rgba(0, 0, 0, 0.8)), url('/assets/images/homepage_carrousel_b_4.jpg')",
                }}
                className={"flex items-end w-full text-white bg-left bg-no-repeat bg-cover lg:h-full xs:h-screen"}
              >
                <div className={"lg:pb-12 lg:pl-16 lg:pr-56 xs:pt-56 xs:px-8 xs:pb-8 space-y-4"}>
                  <span className={"font-bold leading-none lg:text-6xl xs:text-5xl"}>
                      Sanctuary
                  </span>
                  <p className={"text-base font-light"}>
                      Dolor sit amet, consectetur adipiscing elit. Morbi ut turpis in velit ultricies dignissim. Morbi viverra libero quis est porttitor tincidunt. Nunc in metus sit amet felis feugiat tincidunt sit amet quis lectus. Vivamus tincidunt a ligula a malesuada. Aenean viverra lacus nulla, sit amet bibendum ipsum suscipit sit amet.
                  </p>
                  <p className={"font-bold leading-none lg:text-3xl xs:text-2xl"}>
                    13% off
                  </p>
                  <button className={"rounded-sm text-white text-xs font-semibold py-3 px-12"} style={{backgroundColor:'#E91AFC'}}>Explore</button>
                </div>
              </div>
            </div>

            <div className="lg:col-span-3 xs:block">
              <div className={"h-full items-end bg-cover text-white bg-left bg-no-repeat flex w-full bg-palette-violet-main-dark"}>
                <div className={"lg:pr-20 lg:pl-16 lg:pt-48 lg:pb-32 xs:px-8 xs:pt-12 xs:pb-8 space-y-4"}>
                  <span className={"font-bold leading-none lg:text-6xl xs:text-5xl"}>
                    Deal of the Week
                  </span>
                  <p className={"text-base font-light"}>
                    We believe... dolor sit amet, consectetur adipiscing elit. Morbi ut turpis in velit ultricies dignissim. Morbi viverra libero quis est porttitor tincidunt. Nunc in metus sit amet felis feugiat tincidunt sit amet quis lectus. Vivamus tincidunt a ligula a malesuada. Aenean viverra lacus nulla, sit amet bibendum ipsum suscipit sit amet.
                  </p>
                  <button className={"rounded-sm text-white text-xs font-semibold py-3 px-8"} style={{backgroundColor:'#E91AFC'}}>See the Properties</button>
                </div>
              </div>
            </div>
          </div>
        </section>
        <TheMore bgColor={`#211B24`} />
        <section className={`relative bg-white lg:px-24`}>
          {/* <BuyingProgress /> */}

          <div
            className={`pb-4 px-6 lg:px-0 mx-auto`}
            style={{ maxWidth: 1366 }}
          >
            <h4
              className={`w-full text-center mt-10 text-palette-purple-main font-semibold tracking-wider text-xs`}
            >
              LOREM IPSUM
            </h4>
            <h1
              className={`text-left text-center leading-none font-bold text-4xl lg:text-5xl px-8 mb-4 lg:mb-10`}
            >
              Current Popular Deals
            </h1>
            <p className={`text-justify mb-5  lg:px-32 lg:mb-16`}>
              Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
              eiusmod tempor incididunt ut labore et dolore magna aliqua. Mi
              proin sed libero enim sed faucibus turpis. Amet consectetur
              adipiscing elit ut aliquam purus sit.
            </p>

            {isMobile && (
              <div className={`flex items-center justify-between`}>
                <div
                  onClick={() => setOpenSearch(!openSearch)}
                  className={`uppercase font-bold text-palette-gray flex items-center`}
                >
                  Refine Search
                  <svg
                    className={`
                    ml-1 transition-all duration-300 feather-icon text-palette-gray transform
                    ${openSearch ? "rotate-45" : ""}
                  `}
                  >
                    <use xlinkHref={`/assets/svg/feather-sprite.svg#plus`} />
                  </svg>
                </div>
                <div
                  ref={anchorRef}
                  onClick={e => setPopper(popper ? null : e.currentTarget)}
                  className={`uppercase font-bold text-palette-gray flex items-center`}
                >
                  {filters.sortBy.label}
                  <svg
                    className={`
                    ml-1 transition-all duration-300 feather-icon text-palette-gray transform
                    ${popper ? "rotate-180" : ""}
                  `}
                  >
                    <use
                      xlinkHref={`/assets/svg/feather-sprite.svg#chevron-down`}
                    />
                  </svg>
                </div>
                <Popper
                  open={Boolean(popper)}
                  anchorEl={popper}
                  placement={`bottom-end`}
                  transition
                  disablePortal
                  className={`z-50 menu-list`}
                >
                  {({ TransitionProps }) => (
                    <Grow
                      {...TransitionProps}
                      style={{ transformOrigin: "right top" }}
                    >
                      <Paper elevation={2} className={`relative`}>
                        <MenuList>
                          <MenuItem
                            onClick={() => {
                              setFilters({
                                ...filters,
                                sortBy: {
                                  value: "deals.created_at",
                                  order: "desc",
                                  label: "Most Recent"
                                }
                              });
                              setToggleFetch(!toggleFetch);
                              setPopper(null);
                            }}
                          >
                            Most Recent
                          </MenuItem>
                          <MenuItem
                            onClick={() => {
                              setFilters({
                                ...filters,
                                sortBy: {
                                  value: "deals.discount",
                                  order: "desc",
                                  label: "Biggest Discount"
                                }
                              });
                              setToggleFetch(!toggleFetch);
                              setPopper(null);
                            }}
                          >
                            Biggest Discount
                          </MenuItem>
                          <MenuItem
                            onClick={() => {
                              setFilters({
                                ...filters,
                                sortBy: {
                                  value: "deals.expires_at",
                                  order: "asc",
                                  label: "Closing Soon"
                                }
                              });
                              setToggleFetch(!toggleFetch);
                              setPopper(null);
                            }}
                          >
                            Closing Soon
                          </MenuItem>
                          <MenuItem
                            onClick={() => {
                              setFilters({
                                ...filters,
                                sortBy: {
                                  value: "properties_total_price",
                                  order: "asc",
                                  label: "Price (low-high)"
                                }
                              });
                              setToggleFetch(!toggleFetch);
                              setPopper(null);
                            }}
                          >
                            Price (low-high)
                          </MenuItem>
                          <MenuItem
                            onClick={() => {
                              setFilters({
                                ...filters,
                                sortBy: {
                                  value: "properties_total_price",
                                  order: "desc",
                                  label: "Price (high-low)"
                                }
                              });
                              setToggleFetch(!toggleFetch);
                              setPopper(null);
                            }}
                          >
                            Price (high-low)
                          </MenuItem>
                        </MenuList>
                      </Paper>
                    </Grow>
                  )}
                </Popper>
              </div>
            )}

            <Collapse in={isMobile ? openSearch : true}>
              {renderFilters()}
            </Collapse>

            <div className={`lg:px-20 mt-4 lg:mt-24 text-white m-auto`} style={{maxWidth:1336}}>

              <Table
                query={`/api/deal?approved=true&`}
                queryParams={`&projects=true&types=${filters.types}&locations=${filters.locations}&min_price=${filters.minPrice.value}&max_price=${filters.maxPrice.value}`}
                // queryParams={`&projects=true&types=${filters.types}`}
                toggleFetch={toggleFetch}
                toggleFetch={toggleFetch}
                keyword={keyword}
                getData={setProjectData}
                content={renderProjects()}
                sort={filters.sortBy.value.replace(/asc_|desc_/g, "") || ""}
                order={filters.sortBy.order || ""}
              />

            </div>
          </div>
        </section>
        <section className="lg:flex xs:block">
            <div className="lg:w-1/2 xs:w-full" style={{backgroundColor: "#4D0B98"}}>
                <RegistrationForm
                  subHeaderColor="text-white"
                  headerColor="text-palette-purple-main"
                  isPopup={false}
                  opacity={"100%"}
                  containerBgColor={"#4D0B98"}
                />
            </div>
            <BenefitsForBuyers />
        </section>
        <Testimonials />
      </section>
      <SocialMediaShareModal />
    </UserLayout>
  );
};

export default BuyingListPage;
