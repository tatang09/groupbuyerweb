import React, { useState, useEffect, useRef } from "react";
import UserLayout from "~/layouts/users/UserLayout";
import { Button, Modal, TextInput } from "~/components/_base";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { sendResetPasswordLink } from "../data/resetPassword/resetPassword";
import { faSpinner } from "@fortawesome/free-solid-svg-icons";
import { sweetAlert } from "../components/_base/alerts/sweetAlert";
import { isEmpty } from "lodash";

const ForgotPassword = () => {
  const [errors, setErrors] = useState({});
  const [loading, setLoading] = useState(false);
  const [email, setEmail] = useState("");

  const sendResetPassword = e => {
    e.preventDefault();
    setLoading(true);
    let url = "/api/password/email";

    sendResetPasswordLink({ url, email })
      .then(res => {
        if (res.status == 200) {
          setLoading(false);
          sweetAlert("success", res.data.message);
        }
      })
      .catch(error => {
        setErrors(error.response.data.errors);
        setLoading(false);
      });
  };

  const handleInputChange = e => {
    setErrors({});
    setEmail(e.target.value);
  };

  useEffect(() => {

  }, []);

  return (
    <UserLayout>
      <section
        className="text-white bg-palette-blue-dark"
        style={{ marginTop: -130, paddingTop: 160 }}
      >
        <div className={`mx-auto`} style={{ maxWidth: 1366 }}>
          <div className="bg-white flex flex-col items-center lg:p-16 mx-auto pb-10 pt-8 px-4 text-white">
            <h1
              className={
                "bg-white font-bold leading-tight lg:w-3/4 px-1 py-8 rounded-t text-2xl text-center text-gray-900 w-full"
              }
            >
              Send reset password link
            </h1>
            <div className={`mb-5 w-1/2`}>
              <TextInput
                className={`${
                  !isEmpty(errors) ? "border-red-300" : "border-gray-400"
                } border pl-2 py-2 rounded text-base text-gray-900`}
                border={false}
                name="email"
                placeholder={`Email`}
                autoComplete={`off`}
                onChange={e => handleInputChange(e)}
              />
              {errors["email"] && (
                <span className="px-1 text-red-400 tracking-widest text-xs ">
                  {errors["email"][0]}
                </span>
              )}
            </div>

            <div className={`flex items-center justify-center mt-3 mb-5 w-1/2`}>
              <Button
                className={`font-bold rounded`}
                disabled={loading}
                onClick={e => sendResetPassword(e)}
              >
                {loading && (
                  <FontAwesomeIcon
                    icon={faSpinner}
                    className={`fa-spin mr-2`}
                  />
                )}
                Send Reset Link
              </Button>
            </div>
          </div>
        </div>
      </section>
    </UserLayout>
  );
};

export default ForgotPassword;
