import React, { useEffect, useState, createRef } from "react";

import UserLayout from "~/layouts/users/UserLayout";
import UserGlobal from "~/states/userGlobal";
import {
  BuyingProgress,
  AgentDetailsSection,
  SocialMediaShareModal,
  PurchaserDetails,
  SolicitorDetails,
  AdditionalInfos,
  SalesAdvicePropertyDetails,
  ResourcesComponent
} from "~/components";
import { SocialMedia } from "~/components";
import { isLoggedIn } from "~/services/auth";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import BaseAlert from "../components/_base/alerts/BaseAlert";
import CurrencyFormat from "react-currency-format";
import { isMobile } from "react-device-detect";
import swal from "sweetalert2";
import { Button } from "~/components/_base";
import { isEmpty } from "lodash";
import {
  getDealByID,
  salesAdviceAdditionalInfo,
  salesAdviceSolicitorInfo
} from "../data/index";

import * as deviceSize from "../helpers/deviceSizeHelper";
 
import {
  fieldValidator,
  defaultPurchasers,
  purchasersMapper
} from "~/helpers/salesAdviceHelper/salesAdviceHelper";

import { sweetAlert } from "../components/_base/alerts/sweetAlert";
import { fromPairs } from "lodash";

const BuyingConfirmationPage = ({ dealId, propertyId }) => {
  const [userState, userAction] = UserGlobal();
  const [loading, setLoading] = useState(false);
  const [purchaserErrors, setPurchaserErrors] = useState([]);
  const [solicitorErrors, setSolicitorErrors] = useState({});
  const [additionalInfoErrors, setAddtionalInfoErrors] = useState({});
  const [purchasers, setPurchasers] = useState(defaultPurchasers);

  const solicitorForm = createRef();
  const additionalInfoForm = createRef();
  const [currentProject, setProject] = useState({});
  const [apartment, setApartment] = useState({});

  useEffect(() => {
    if (!isLoggedIn()) {
      window.location = `/weekly-deals`;
    }
    getDeal();
  }, []);

  const getDeal = async () => {
    setLoading(true);
    let url = `/api/deal/${dealId}`;
    getDealByID({ url }).then(res => {
      setProject(res.data);
      let property = res.data.properties.filter(
        property => property.id === propertyId
      )[0];
      setApartment(property || {});
      setLoading(false);
    });
  };

  const handleSubmit = isSubmit => {
    let solicitorFormData = new FormData(solicitorForm.current);
    let additionalInfoFormData = new FormData(additionalInfoForm.current);

    swal
      .fire({
        title: `${isSubmit ? "Submit" : "Save"} sales advice`,
        text: `Are you sure you want to ${
          isSubmit ? "Submit" : "Save"
        } sales advice?`,
        type: "info",
        showCancelButton: true,
        confirmButtonText: "Yes!",
        cancelButtonText: "No"
      })
      .then(result => {
        if (result.value) {
          setLoading(true);

          let purchaserArr = [];

          let solicitorAddress = {
            suburb: solicitorFormData.get("suburb"),
            state: solicitorFormData.get("state"),
            country: solicitorFormData.get("country"),
            postcode: solicitorFormData.get("postcode")
          };

          solicitorFormData.append("propertyId", propertyId);
          solicitorFormData.append("address", JSON.stringify(solicitorAddress));
          let phone = solicitorFormData.get("phone")
            ? "+61" + solicitorFormData.get("phone")
            : "";
          solicitorFormData.set("phone", phone);

          purchasers.forEach(purchaser => {
            let data = {};

            data.address = {
              address_line_1: purchaser.address_line_1,
              suburb: purchaser.suburb,
              country: purchaser.country,
              state: purchaser.state,
              postcode: purchaser.postcode
            };

            data.address_line_1 = purchaser.address_line_1;
            data.suburb = purchaser.suburb;
            data.country = purchaser.country;
            data.state = purchaser.state;
            data.title = purchaser.title;
            data.postcode = purchaser.postcode;

            data.phone = purchaser["phone"] ? "+61" + purchaser["phone"] : "";

            data.alternate_phone = purchaser["alternate_phone"]
              ? "+61" + purchaser["alternate_phone"]
              : "";

            data.email = purchaser["email"];
            data.first_name = purchaser["first_name"];
            data.last_name = purchaser["last_name"];
            data.propertyId = purchaser["propertyId"];
            data.id = purchaser["id"];

            purchaserArr.push(data);
          });

          let param = {
            propertyId: propertyId,
            purchasers: purchaserArr,
            solicitor: fromPairs(Array.from(solicitorFormData.entries())),
            additionalInfo: fromPairs(
              Array.from(additionalInfoFormData.entries())
            ),
            isSubmit: isSubmit
          };

          try {
            axios.post("/api/sales-advice", param).then(res => {
              setLoading(false);
              if (res.status === 200 && !res.data.errorCode) {
                setPurchasers(
                  purchasersMapper(res.data[0].original.purchasers)
                );
                setSolicitorErrors([]);
                setPurchaserErrors([]);
                sweetAlert("success", "Sales advice successfully saved.");
              }
              if (res.data.errorCode === 422) {
                setLoading(false);
                if (!isEmpty(res.data.solicitorValidator)) {
                  setSolicitorErrors(res.data.solicitorValidator);
                }
                if (!isEmpty(res.data.purchaserValidator)) {
                  setPurchaserErrors(res.data.purchaserValidator);
                }
              }
            });
          } catch (error) {
            setLoading(false);
          }
        }
      });
  };

  return (
    <UserLayout>
     
      <section
        className={`bg-palette-blue-dark`}
        style={{ marginTop: -130, paddingTop: 160 }}
      >
        <section className={`relative bg-white text-gray-800`}>
          <BuyingProgress current={5} isPreview={false} />
          <div
            className={`flex flex-col lg:flex-row lg:px-10 mt-8 pb-16 mx-auto`}
            style={{ maxWidth: 1366 }}
          >
            {!isMobile && (
              <div className="lg:w-1/5 py-2 text-center text-gray-700">
                <div className={`relative`}>
                  <a
                    href={`/weekly-deals/${dealId}/false`}
                    className={`bg-white cursor-pointer duration-300 font-bold hover:text-palette-blue inline-flex items-center pr-3 relative text-palette-blue-light text-sm transition-all z-10`}
                  >
                    <svg className={`feather-icon h-4 w-4 mr-1 inline`}>
                      <use
                        xlinkHref={`/assets/svg/feather-sprite.svg#chevron-left`}
                      />
                    </svg>
                    Back to project details
                  </a>
                  <div
                    className={`absolute border-gray-200 border-t w-full`}
                    style={{ top: 10 }}
                  />
                </div>
                <div className="flex flex-col items-center">
                  <div className="text-center mt-5">
                    <h2 className="font-bold font-serif leading-6 mb-2 text-3xl">
                      The
                      <br />
                      Orchards
                    </h2>
                    <span className="font-black text-base">Northwest</span>
                  </div>
                  <div className="mt-2 text-center">
                    <img
                      src="/assets/images/_directors/luke-hayes.jpg"
                      className={`mx-auto rounded-full`}
                      style={{ width: 120 }}
                    />
                  </div>
                  <div className="text-center mt-2 text-center">
                    <div className="text-base text-gray-900 mt-2 font-bold">
                      {!isEmpty(currentProject) && currentProject.agent_name}
                    </div>
                    <div className="border rounded my-1 border-gray-700 font-normal text-base text-center text-gray-900 inline-block">
                      {!isEmpty(currentProject) && (
                        <a
                          className={`px-4`}
                          href={`tel:${currentProject.agent_phone}`}
                        >
                          {currentProject && currentProject.agent_phone}
                        </a>
                      )}
                    </div>
                    <div className="font-bold text-base text-gray-900 truncate w-48 hover:text-palette-purple duration-300 transition-all">
                      {!isEmpty(currentProject) && (
                        <a href={`mailto:${currentProject.agent_email}`}>
                          {currentProject && currentProject.agent_email}
                        </a>
                      )}
                    </div>
                  </div>
                </div>
              </div>
            )}

            <div className="lg:ml-16 px-6 lg:px-0">
              <div className="flex flex-col lg:w-9/12">
                <BaseAlert
                  message={"Payment successful"}
                  icon={"check"}
                  type={"success"}
                />
                <h1 className="font-bold flex flex-col lg:flex-row text-4xl mb-4 lg:mb-2">
                  <span>Congratulations!&nbsp;</span>
                  <span className={`text-2xl lg:text-4xl`}>
                    You just saved&nbsp;
                    <CurrencyFormat
                      value={apartment.price * (currentProject.discount / 100)}
                      displayType={`text`}
                      thousandSeparator={true}
                      prefix={`$`}
                    />
                  </span>
                </h1>
                <div className="text-lg mb-4 lg:mb-2">
                  You have just secured{" "}
                  <span className="font-bold">
                    Apartment {apartment && apartment.unit_name} at{" "}
                    {currentProject.name}
                  </span>
                </div>
              </div>

              <hr className="hidden lg:block border border-gray-300 mb-3" />

              <div className={`flex flex-col lg:flex-row`}>
                <div className={`lg:w-9/12 lg:mr-10`}>
                  <p className="text-base font-bold lg:pr-24">
                    An email has now been sent to you with the following
                    details. Your project agent{" "}
                    {` ${currentProject.agent_name &&
                      currentProject.agent_name.split(" ")[0]} `}{" "}
                    will be in contact shortly.
                  </p>
                  <div className="mt-6 mb-12">
                    <p>
                      <span className="text-base font-bold">
                        Your agent's details:
                      </span>
                      <br />
                      <span className="text-base">
                        {currentProject.agent_name} |{" "}
                        {currentProject.agent_phone} |{" "}
                      </span>
                      <a
                        href={`mailto:${currentProject.agent_email}`}
                        className="text-palette-blue-light text-base"
                      >
                        {currentProject.agent_email}
                      </a>
                    </p>
                  </div>

                  <div className="flex flex-row">
                    <div className="bg-blue-100 leading-6 lg:p-10 p-6 rounded-lg">
                      <h2 className="font-black font-bold text-2xl mb-4">
                        Next steps
                      </h2>
                      <p className="text-base font-bold">
                        1. In the next 48 hours submit your Sales Advice with
                        the details of your purchase.
                      </p>
                      <p className="text-base font-bold">
                        2. Once all 5 apartments are sold within your project
                        your GroupBuyer deal is valid.
                      </p>
                      <p className="text-base font-bold">
                        3. The Vendor's Solicitor will issue your individual
                        contract for sale (to your Solicitor).
                      </p>
                      <p className="text-base font-bold">
                        4. Sign your contract and pay your property deposit.
                      </p>
                      <p className="text-base font-bold">
                        5. Exchange contracts via your solicitor and/or
                        GroupBuyer agent.
                      </p>

                      <div className="text-base">
                        Your GroupBuyer agent will be in contact shortly to
                        assist you every step of the way. If you need to come
                        back to your Sales Advice form later you can find it in
                        your <span className="font-bold">Profile</span> under{" "}
                        <span className="font-bold">&nbsp;'Secured deals'</span>
                        .
                      </div>
                    </div>
                  </div>
                </div>

                {/* {isMobile && (
                  <>
                    <div className="lg:w-1/5 py-2 text-center my-3 text-gray-700">
                      <div className="flex flex-col items-center">
                        <div className="text-center mt-5">
                          <h2 className="font-bold font-serif leading-6 mb-2 text-3xl">
                            The
                            <br />
                            Orchards
                          </h2>
                          <span className="font-black text-base">
                            Northwest
                          </span>
                        </div>
                        <div className="mt-2 text-center">
                          <img
                            src="/assets/images/_directors/luke-hayes.jpg"
                            className={`mx-auto rounded-full`}
                            style={{ width: 120 }}
                          />
                        </div>
                        <div className="text-center mt-2 text-center">
                          <div className="text-base text-gray-900 mt-2 font-bold">
                            {currentProject && currentProject.agent_name}
                          </div>
                          <div className="border rounded my-1 border-gray-700 font-normal text-base text-center text-gray-900 inline-block">
                            <a
                              className={`px-4`}
                              href={`tel:${currentProject.agent_phone}`}
                            >
                              {currentProject && currentProject.agent_phone}
                            </a>
                          </div>
                          <div className="font-bold text-base text-gray-900 truncate w-48 hover:text-palette-purple duration-300 transition-all">
                            <a href={`mailto:${currentProject.agent_email}`}>
                              {currentProject && currentProject.agent_email}
                            </a>
                          </div>
                        </div>
                      </div>
                    </div>
                    <hr className="block border border-gray-300 mb-3" />
                  </>
                )} */}

                <div className={`lg:w-3/12 px-12 lg:px-0 relative`}>
                  <div>
                    <div
                      className={`font-bold my-3 text-base text-palette-gray text-center lg:text-left uppercase`}
                    >
                      Resources
                    </div>
                    <ul>
                      {!isEmpty(currentProject) &&
                        currentProject.resources.length > 0 && (
                          <ResourcesComponent project={currentProject} />
                        )}
                      <span
                        onClick={() =>
                          userAction.setState({ showSocialMedia: true })
                        }
                        className={`mb-3 cursor-pointer flex items-center bg-gray-200 block font-bold px-3 py-1 leading-relaxed text-base rounded text-palette-blue-light`}
                      >
                        Share this project
                        <svg className={`feather-icon h-4 w-4 ml-2`}>
                          <use
                            xlinkHref={`/assets/svg/feather-sprite.svg#share`}
                          />
                        </svg>
                      </span>
                    </ul>
                  </div>

                  <div className="bg-gray-200 bottom-0 leading-6 px-4 py-6 rounded self-end">
                    <div>
                      <h2 className="font-black font-bold  text-lg  ">
                        Connect with us...
                      </h2>
                      <p className="font-hairline text-base text-sm">
                        Check our reviews, project updates, latest projects and
                        more
                      </p>
                    </div>
                    <div className="flex justify-center lg:justify-start text-white">
                      <SocialMedia size={`2x`} />
                    </div>
                  </div>
                </div>
              </div>

              <PurchaserDetails
                setPurchasers={setPurchasers}
                purchasers={purchasers}
                errors={purchaserErrors}
                loading={loading}
                width={`lg:w-9/12`}
              />

              {/* {!isEmpty(currentProject) && !isEmpty(apartment) && (
                <SalesAdvicePropertyDetails
                  project={currentProject}
                  apartment={apartment}
                  deal={{
                    discount_is_percentage:
                      currentProject.discount_is_percentage,
                    discount: currentProject.discount,
                    property_price: apartment.price
                  }}
                />
              )} */}

              {/* {!isEmpty(currentProject) && !isEmpty(apartment) && (
                <SolicitorDetails
                  solicitorErrors={solicitorErrors}
                  solicitorForm={solicitorForm}
                  width={`lg:w-9/12`}
                />
              )}

              {!isEmpty(currentProject) && !isEmpty(apartment) && (
                <AdditionalInfos
                  additionalInfoErrors={additionalInfoErrors}
                  additionalInfoForm={additionalInfoForm}
                  width={`lg:w-9/12`}
                />
              )} */}

              <div
                className={`flex justify-center mt-8 ${
                  userState.windowSize <= deviceSize.xtraSmall
                    ? "flex-1 flex-col"
                    : ""
                }`}
              >
                <Button
                  onClick={() => handleSubmit(false)}
                  className={`${
                    loading ? "bg-gray-500" : "bg-palette-blue-light"
                  } text-white mr-2 block button button-primary rounded-full`}
                  disabled={loading}
                >
                  {loading && (
                    <FontAwesomeIcon
                      icon={["fas", "spinner"]}
                      className={`fa-spin mr-2`}
                    />
                  )}
                  Save
                </Button>
                <Button
                  className={`bg-red-600 button button-primary font-bold mr-2 rounded-full h-10
                  ${
                    userState.windowSize <= deviceSize.xtraSmall
                      ? "mt-2"
                      : "mt-0"
                  }`}
                  disabled={loading}
                >
                  Cancel
                </Button>
                <Button
                  onClick={() => handleSubmit(true)}
                  className={`block button button-primary rounded-full
                  ${
                    userState.windowSize <= deviceSize.xtraSmall
                      ? "mt-2"
                      : "mt-0"
                  }`}
                  disabled={loading}
                >
                  {loading && (
                    <FontAwesomeIcon
                      icon={["fas", "spinner"]}
                      className={`fa-spin mr-2`}
                    />
                  )}
                  Submit
                </Button>
              </div>
            </div>
          </div>

          {!isEmpty(currentProject) && (
            <AgentDetailsSection project={currentProject} />
          )}

          <SocialMediaShareModal />
        </section>
      </section>
    </UserLayout>
  );
};

export default BuyingConfirmationPage;
