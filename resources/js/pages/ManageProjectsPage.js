import React, { useState, useEffect } from "react";
import UserLayout from "~/layouts/users/UserLayout";
import { Table, TextInput, Tooltip, Modal, Button } from "~/components/_base";
import UserGlobal from "~/states/userGlobal";
import SequenceAddingModal from "../components/SequenceAddingModal";
import SequenceEditingModal from "../components/SequenceEditingModal";

import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { debounce } from "lodash";
import { isMobile } from "react-device-detect";
import moment from "moment";
import { faPlus } from "@fortawesome/free-solid-svg-icons";

import { addressHelper } from "../helpers/addressHelper";

import * as deviceSizes from "../helpers/deviceSizeHelper";

import { Helmet } from "react-helmet";
import { metaHelper } from "../helpers/metaHelper/metaHelper";
import { sweetAlert } from "../components/_base/alerts/sweetAlert";

const ManageProjects = () => {
  const [userState, userAction] = UserGlobal();
  const [keyWord, setKeyWord] = useState("");
  const [toggleFetch, setToggleFetch] = useState(false);
  const [projects, setProjects] = useState([]);
  const [filters, setFilters] = useState({
    sortBy: { value: "deals.created_at", order: "desc" }
  });
  const [toggleOrder, setToggleOrder] = useState(false);
  const [toggleEditProject, setToggleEditProject] = useState(false);
  const [toggleAddProject, setToggleAddProject] = useState(false);
  const [toggleSequenceAdding, setToggleSequenceAdding] = useState(false);
  const [toggleSequenceEdit, setToggleSequenceEdit] = useState(false);
  const [project, setProject] = useState([]);
  const [toggleInfoModal, setToggleInfoModal] = useState(false);

  useEffect(() => {
    if (!toggleAddProject) {
      setToggleFetch(!toggleFetch);
    }
    return () => {};
  }, [toggleAddProject]);

  useEffect(() => {
    if (!toggleEditProject) {
      setToggleFetch(!toggleFetch);
    }
    return () => {};
  }, [toggleEditProject]);

  const formatFilter = column => {
    if (column) {
      setToggleOrder(!toggleOrder);

      setFilters({
        sortBy: { value: column, order: toggleOrder ? "desc" : "asc" }
      });

      setToggleFetch(!toggleFetch);
    }
  };

  const tableHeaders = [
    {
      label: "Deal Id",
      width: 80,
      show: isMobile ? false : true
    },
    {
      label: "Project Name",
      column: "deals.name",
      sortable: true,
      style: "",
      width: 100,
      show: isMobile ? true : false
    },
    {
      label: "Property Address",
      column: "deals.address",
      sortable: true,
      style: "",
      width: 200,
      show: isMobile ? false : true
    },
    {
      label: "Property Type",
      column: "deals.sub_property_id",
      sortable: true,
      width: isMobile ? 120 : 150,
      show: isMobile ? true : false
    },
    {
      label: "Proposed Settlement",
      column: "deals.proposed_settlement",
      sortable: true,
      width: 150,
      show: isMobile ? false : true
    },
    {
      label: "Project Time Limit",
      column: "deals.deal_time_limit",
      width: 150,
      show: isMobile ? false : true
    },
    {
      label: "Property Count",
      column: "",
      sortable: false,
      style: "text-center",
      width: 150,
      show: true
    },
    {
      label: "Approved",
      column: "deals.is_deal_approved",
      sortable: true,
      style: "text-center",
      width: 60,
      show: isMobile ? false : true
    },
    {
      label: isMobile ? "View" : "Edit",
      column: "",
      sortable: false,
      width: 60,
      show: true
    }
  ];

  const handleSearch = debounce(text => {
    setKeyWord(text);
  }, 800);

  const toggleSequenceAddingModal = () => {
    setToggleSequenceAdding(!toggleSequenceAdding);
  };

  const toggleSequenceEditModal = () => {
    setToggleSequenceEdit(!toggleSequenceEdit);
  };

  const renderHeaders = () => {
    if (!isMobile) {
      return (
        <thead>
          <tr>
            {tableHeaders.map((th, index) => {
              return (
                <th
                  onClick={() => formatFilter(th.column)}
                  style={{ width: th.width }}
                  className={`${
                    th.style
                  } border-b font-bold px-4 py-2 text-palette-gray px-4 py-2  ${
                    th.sortable
                      ? "cursor-pointer hover:text-palette-gray-dark"
                      : " "
                  }
              ${th.label === "Approved" ? "text-left" : "text-left"}

              `}
                  key={index}
                >
                  {th.label}
                </th>
              );
            })}
          </tr>
        </thead>
      );
    } else {
      return (
        <thead>
          <tr className={`bg-gray-200`}>
            {tableHeaders.map((th, index) => {
              if (th.show) {
                return (
                  <th
                    onClick={() => formatFilter(th.column)}
                    style={{ width: th.width }}
                    className={`${
                      th.style
                    } border-b font-bold px-4 py-2 text-palette-gray px-4 py-2  ${
                      th.sortable
                        ? "cursor-pointer hover:text-palette-gray-dark"
                        : " "
                    }
              ${th.label === "View" ? "text-center" : "text-left"}
              ${
                th.label === "Property Count" &&
                userState.windowSize <= deviceSizes.baseSmall
                  ? "hidden"
                  : ""
              }
              `}
                    key={index}
                  >
                    {th.label}
                  </th>
                );
              }
            })}
          </tr>
        </thead>
      );
    }
  };

  const projectsContent = () => {
    if (!isMobile) {
      return (
        <>
          <tbody>
            {projects.map(project => {
              let timeLimit = project.expires_at
                ? JSON.parse(project.expires_at)
                : project.created_at;

              return (
                <tr
                  key={project.id}
                  className={`hover:bg-gray-100 border-b text-base text-center`}
                >
                  <td className={`pl-4 text-left`}>{project.id}</td>
                  <td
                    onClick={() => {
                      window.location = `/profile/manage-properties/${project.id}/${project.sub_property_id}`;
                    }}
                    className={`pl-4 text-left transform font-bold hover:text-palette-blue-light cursor-pointer text-palette-purple`}
                  >
                    {project.name}
                  </td>
                  <td className={`p-4 text-left`}>
                    {addressHelper(project.address)}
                  </td>
                  <td className={`p-4 text-left text-left`}>
                    {project.deal_type}
                  </td>
                  <td className={`p-4 text-left text-left`}>
                    {" "}
                    {project.sub_property_id != 4
                      ? moment(project.proposed_settlement).format("DD-MM-YYYY")
                      : moment(project.land_registration).format("DD-MM-YYYY")}
                  </td>
                  <td className={`p-4 text-left`}>
                    {project.expires_at
                      ? moment(timeLimit.time_limit).format("DD-MM-YYYY")
                      : moment(project.deal_time_limit).format("DD-MM-YYYY")}
                    {/* {moment(project.deal_time_limit).format("LL")} */}
                  </td>
                  <td className={`p-4 text-center`}>
                    {project.properties
                      ? `${project.properties.length} / 5`
                      : "0 / 5"}
                  </td>
                  <td className={`p-4 text-center`}>
                    {/* {project.is_deal_approved ? "Yes" : "No"} */}
                    <svg
                      className={`feather-icon ml-4 ${
                        project.is_deal_approved
                          ? "text-palette-purple h-6 w-6"
                          : "text-gray-500 h-5 w-5"
                      }`}
                    >
                      <use xlinkHref={`/assets/svg/feather-sprite.svg#check`} />
                    </svg>
                  </td>

                  <td className={`p-4`}>
                    <Tooltip title={`Edit Project`}>
                      <div
                        onClick={() => {
                          toggleSequenceEditModal();
                          setProject(project);
                        }}
                        className={`cursor-pointer bg-white border border-gray-400 duration-300 hover:bg-palette-purple hover:border-palette-purple hover:text-white inline-flex px-3 py-1 rounded transition-all`}
                      >
                        <svg className={`feather-icon h-5 w-5 opacity-50`}>
                          <use
                            xlinkHref={`/assets/svg/feather-sprite.svg#edit`}
                          />
                        </svg>
                      </div>
                    </Tooltip>
                  </td>
                </tr>
              );
            })}
          </tbody>
        </>
      );
    } else {
      return (
        <>
          <tbody>
            {projects.map(project => {
              return (
                <tr
                  key={project.id}
                  className={`hover:bg-gray-100 border-b text-base text-center`}
                >
                  <td
                    onClick={() => {
                      window.location = `/profile/manage-properties/${project.id}`;
                    }}
                    className={`pl-4 text-left transform font-bold hover:text-palette-blue-light cursor-pointer text-palette-purple`}
                  >
                    {" "}
                    {project.name}
                  </td>
                  <td className={`p-4 text-left text-left`}>
                    {" "}
                    {project.deal_type}{" "}
                  </td>
                  <td
                    className={`p-4 text-center  ${
                      userState.windowSize <= deviceSizes.baseSmall
                        ? "hidden"
                        : ""
                    }`}
                  >
                    {project.properties
                      ? `${project.properties.length} / 5`
                      : "0 / 5"}
                  </td>
                  <td className={`p-4`}>
                    <Tooltip title={`Edit Project`}>
                      <div
                        onClick={() => {
                          setProject(project);
                          toggleSequenceEditModal();
                        }}
                        className={`cursor-pointer bg-white border border-gray-400 duration-300 hover:bg-palette-purple hover:border-palette-purple hover:text-white inline-flex px-3 py-1 rounded transition-all`}
                      >
                        <svg className={`feather-icon h-5 w-5 opacity-50`}>
                          <use
                            xlinkHref={`/assets/svg/feather-sprite.svg#eye`}
                          />
                        </svg>
                      </div>
                    </Tooltip>
                  </td>
                </tr>
              );
            })}
          </tbody>
        </>
      );
    }
  };

  const dislayText = () => {
    return (
      <div className={"my-10"} key={1}>
        <span className="block font-extrabold mt-2 text-2xl text-center text-gray-500 mb-5">
          Welcome to GroupBuyer
        </span>
        <span className="block mt-2 text-base text-gray-500 text-left">
          When adding a project ensure you upload all 5 properties/lots and fill
          in all required fields. Your project will be approved by a GroupBuyer
          Admin Agent prior to being displayed online. Should your project not
          meet the requirements of GroupBuyer, a team member will be in contact
          with you. Click the 'View Agency Agreement' button to learn more about
          our terms and conditions.
        </span>
      </div>
    );
  };

  const callEditToggleFetch = () => {
    setToggleFetch(!toggleFetch);
  };

  const callAddToggleFetch = () => {
    setToggleFetch(!toggleFetch);
  };

  const addProject = () => {
    return isMobile
      ? sweetAlert(
          "error",
          "In order to upload a project this must be done via desktop."
        )
      : toggleSequenceAddingModal();
  };

  return (
    <>
      <UserLayout>
        <Helmet>
          <meta name="description" content={metaHelper.desc} />
        </Helmet>
        {toggleSequenceEdit && (
          <SequenceEditingModal
            project={project}
            editToggleFetch={callEditToggleFetch}
            toggleSequenceEditModal={() => toggleSequenceEditModal()}
          ></SequenceEditingModal>
        )}
        {toggleSequenceAdding && (
          <SequenceAddingModal
            addToggleFetch={callAddToggleFetch}
            toggleSequenceAddingModal={() => toggleSequenceAddingModal()}
          ></SequenceAddingModal>
        )}
        <section
          className={`bg-palette-blue-dark`}
          style={{ marginTop: -130, paddingTop: 160 }}
        >
          <section className={`relative bg-white`}>
            <div
              className={`pb-16 px-6 lg:px-20 mx-auto`}
              style={{ maxWidth: 1600 }}
            >
              <h1
                className={`font-bold leading-tight text-4xl pt-10 pb-8 lg:pb-16 text-center`}
              >
                Manage projects
              </h1>
              {!isMobile && (
                <div className="flex justify-between mb-8">
                  <div className={`flex justify-center lg:justify-end`}>
                    <div className={`w-64 relative `}>
                      <svg
                        className={`absolute feather-icon h-8 left-0 mx-3 text-gray-500`}
                      >
                        <use
                          xlinkHref={`/assets/svg/feather-sprite.svg#search`}
                        />
                      </svg>
                      <TextInput
                        className={`pl-8`}
                        type={`text`}
                        placeholder={`Search`}
                        border={false}
                        appearance={true}
                        onChange={e => handleSearch(e.target.value)}
                      />
                    </div>
                  </div>
                  <div className={`flex justify-center lg:justify-end`}>
                    <a
                      href={
                        "/assets/pdf/group-buyer-agency-agreement-2020-10-08-draft.pdf"
                      }
                      target={`_blank`}
                      className={`border border-gray-400 button flex items-center justify-around mr-2`}
                    >
                      <span className={`text-base text-gray-600`}>
                        View Agency Agreement
                      </span>
                    </a>

                    <Tooltip title={`Click to add a Project`}>
                      <div className={`w-auto`}>
                        <Button
                          className={`flex justify-around items-center`}
                          onClick={() => {
                            toggleSequenceAddingModal();
                          }}
                        >
                          Add Project
                        </Button>
                      </div>
                    </Tooltip>
                  </div>
                </div>
              )}

              {isMobile && (
                <div className="flex justify-between mb-8">
                  <div
                    className={`flex justify-center md:flex-1 md:justify-start xs:w-48`}
                  >
                    <div className={`w-64 relative `}>
                      <svg
                        className={`absolute feather-icon h-8 left-0 mx-3 text-gray-500`}
                      >
                        <use
                          xlinkHref={`/assets/svg/feather-sprite.svg#search`}
                        />
                      </svg>
                      <TextInput
                        className={`pl-8`}
                        type={`text`}
                        placeholder={`Search`}
                        border={false}
                        appearance={true}
                        onChange={e => handleSearch(e.target.value)}
                      />
                    </div>
                  </div>

                  <div
                    className={`flex items-center justify-center md:flex-1 md:justify-end md:mr-12`}
                  >
                    <div className="flex justify-end ml-10">
                      <a
                        href={
                          "/assets/pdf/group-buyer-agency-agreement-2020-10-08-draft.pdf"
                        }
                        target={`_blank`}
                        className={`border border-gray-400 button flex items-center justify-around mr-2`}
                      >
                        <span className={`text-base text-gray-600`}>
                          View Agency Agreement
                        </span>
                      </a>

                      <span
                        className={`font-semibold text-palette-purple cursor-pointer`}
                        onClick={() => addProject()}
                      >
                        <FontAwesomeIcon
                          icon={faPlus}
                          className={`fa-plus mr-2`}
                        />
                        Add Project
                      </span>
                    </div>
                  </div>
                </div>
              )}
              <div className={`lg:mb-12`}>
                <span className={`font-bold text-lg`}>Note:&nbsp;</span>
                <span className={`font-semibold text-red-600 text-sm`}>
                  A project cannot go live unless all 5 properties are approved.
                </span>
              </div>

              {/* {isEmpty(projects) && displayText()} */}

              <Table
                query={`/api/project`}
                toggleFetch={toggleFetch}
                keyword={keyWord}
                getData={setProjects}
                content={projectsContent()}
                sort={filters.sortBy.value.replace(/asc_|desc_/g, "") || ""}
                order={filters.sortBy.order || ""}
                header={renderHeaders()}
                emptyComponent={dislayText()}
              />
            </div>
          </section>
        </section>
      </UserLayout>
    </>
  );
};

export default ManageProjects;
