import React, { useState } from "react";

import UserLayout from "~/layouts/users/UserLayout";
import UserGlobal from "~/states/userGlobal";
import { Button, Modal } from "~/components/_base";
import { isLoggedIn } from "~/services/auth";

import ReactPlayer from "react-player";
import { isMobile } from "react-device-detect";
import { Helmet } from "react-helmet";
import { metaHelper } from "../helpers/metaHelper/metaHelper";
import MediaPlayer from '../components/MediaPlayer';

const HowItWorksPage = () => {
  const [userState, userAction] = UserGlobal();
  const [showWork, setShowWork] = useState(false);

  const handleClose = () => {
    setShowWork(false);
  };

  return (
    <UserLayout>
      <Helmet>
    
        <meta name="description" content={metaHelper.desc} />
       
      </Helmet>
      <section className={`text-white bg-palette-blue-dark lg:pb-20`}>
        <section
          className={`mx-auto`}
          style={{ marginTop: -130, paddingTop: 160 }}
        >
          <div
            style={{
              background:
                "linear-gradient(to left, rgb(30, 136, 225) 0%, rgba(30, 136, 255, 0.5) 0%), url('/assets/images/landing_page_how_it_works.jpg')"
            }}
          >
            <div
              className={`flex flex-col-reverse lg:flex-row lg:px-16 mx-auto pt-12 px-6`}
              style={{ maxWidth: 1366 }}
            >
              {/* flex flex-col-reverse lg:flex-row lg:mb-16 lg:px-16 mx-auto pt-12 px-6 */}
              {/* flex flex-col-reverse lg:flex-row lg:pt-16 mb-8 lg:px-16 mx-auto px-6 */}
              <div className={`w-full text-center`}>
                <div className={`font-bold leading-tight lg:px-40 text-2xl`}>
                  GroupBuyer provides the platform for buyers to join
                  <span className={`text-palette-teal`}> 'buyer groups' </span>
                  to gain a bulk buy discount from a property developer.
                </div>

                {/* <div className={`leading-loose mt-12 pr-64 text-lg`}>
                <p>
                  Not only do Buyers save minimum 10% on the purchase price of a new home, they also have the opportunity to receive thousands of dollars in rebates.
                </p>
                <p>
                  For every buyer that joins a project deal, a $2,000 rebate is applied. The more buyers that join the deal, the greater the amount of rebates given to each buyer.
                </p>
              </div> */}
              </div>
              {/* How it works original layout */}
              {/* <div className={`bg-center bg-cover flex flex-col items-center justify-center lg:w-4/12 rounded-lg shadow-lg`} style={rowStyles.see}>
              <span className={`text-2xl font-bold mb-6`}>See how it works</span>
              <PlayButton onClick={() => setShowWork(!showWork)}/>
            </div> */}
            </div>

            {/* <div className={`mb-6 mt-8`}>
            <svg className={`feather-icon text-white w-10 h-10 mx-auto`}>
              <use xlinkHref={`/assets/svg/feather-sprite.svg#chevron-down`} />
            </svg>
          </div> */}

            {/* <div className={`mx-auto rounded-lg shadow-lg lg:w-8/12 w-full px-6 lg:p-0`}>
            <ReactPlayer
              className={`mb-4`}
              url={`https://www.youtube.com/watch?v=ZccHRQCJJ8Y`}
              width={`100%`}
              height={isMobile ? 300 : 600}
            />
          </div> */}

            <section
              className={`px-6 lg:p-0 relative z-10 text-center flex items-center justify-center`}
            >
              <div className={`mx-auto w-full`} style={{ maxWidth: 1366 }}>
                <div
                  className={`my-16 lg:p-0 lg:w-9/12 mx-auto px-6 rounded-lg shadow-lg w-full`}
                >
                  <MediaPlayer url={`https://www.youtube.com/watch?v=_2EC8cfrFBI`} height={isMobile ? 300 : 600} />
                  {/* <ReactPlayer
                    url={`https://www.youtube.com/watch?v=m8kBcCIQvx8`}
                    width={`100%`}
                    height={isMobile ? 300 : 600}
                  /> */}
                </div>
              </div>
            </section>
          </div>
          {/* <div className={`bg-cover bg-center pt-10 lg:pt-20`} style={rowStyles.discount}>
            <div className={`lg:px-16 px-8 relative`}> */}
          {/* <div className={`bg-palette-teal shadow-lg text-palette-blue-dark text-2xl font-bold py-2 pr-16 pl-4 absolute right-0 top-0 mt-4`}>
                First, the discount you receive
              </div> */}

          {/* <div className={`font-bold lg:text-5xl mb-4 lg:mb-8 text-3xl`}>Buyer group's discount</div>
              <div className={`text-xl mb-10`}>Example 'buyer group' purchasing 5 x $800,000 properties</div>
              <div className={`lg:px-20 pb-16 pt-6`}>
                <img src={`/assets/images/how_it_works_discount.png`} />
                <div className={`text-base text-center lg:text-right`}>
                    <span
                      className={`lg:mr-10 opacity-50 underline cursor-pointer`}
                      onClick={() => userAction.setState({showTermsAndConditions: true})}
                    >
                      Terms &amp; Conditions
                    </span>
                </div>
              </div>
            </div> */}

          {/* <div className={`mb-6 mt-6`}>
              <svg className={`feather-icon text-white w-10 h-10 mx-auto`}>
                <use xlinkHref={`/assets/svg/feather-sprite.svg#chevron-down`} />
              </svg>
            </div> */}

          {/* <div className={`px-16 bg-black bg-opacity-50 pt-20 pb-20 relative`}>
              <div className={`bg-palette-teal shadow-lg text-palette-blue-dark text-2xl font-bold py-2 pr-16 pl-4 absolute right-0 top-0 mt-24`}>
                Plus, you also receive this rebate
              </div>

              <div className={`mb-8 text-5xl font-bold`}>Individual buyer's rebate</div>
              <div className={`flex`}>
                <div className={`w-7/12 leading-loose pr-20 text-lg`}>
                  <p>
                    Upon any project deal being finalised, a $2,000 rebate multiplied by the number of buyers in your group os credited to each individual buyer.
                  </p>
                  <p>
                    With a minimum of 5 properties in each project deal, it pays to share each project to your social network to attract more buyer demand.
                  </p>
                  <div className={`items-center mt-20`} style={{ width: "fit-content" }}>
                    <Button className={`px-24`}>Show me the discounts</Button>
                    <div className={`text-center opacity-50 mt-4 text-base`}>It's free to join!</div>
                  </div>
                </div>
                <div className={`w-5/12`}>
                  <img src={`/assets/images/how_it_works_room.jpg`} />
                  <div className={`text-base mt-4`}>
                    <span
                      className={`mr-10 opacity-50 underline cursor-pointer`}
                      onClick={() => userAction.setState({showTermsAndConditions: true})}
                    >
                      Terms &amp; Conditions
                    </span>
                  </div>
                </div>
              </div>
            </div> */}
          {/* </div> */}

          <div className={`lg:px-0 mt-10 pb-2 px-4 text-center`}>
            <div className={`lg:px-0 mt-10 pb-2 px-4 text-center`}>
              <div className={`lg:text-4xl text-3xl`}>Access our projects</div>
            </div>
            <p className={`text-xl py-3 mb-2`}>
              and see all the benefits with GroupBuyer.
            </p>
            {!isLoggedIn() ? (
              <Button onClick={() => userAction.setState({ showSignUp: true })}>
                Sign up for free
              </Button>
            ) : (
              <a href={`/weekly-deals`}>
                <Button type={`secondary`}>View all projects</Button>
              </a>
            )}
          </div>
        </section>
      </section>

      <Modal
        show={showWork}
        title={`How does it work?`}
        maxWidth={`md`}
        onClose={() => handleClose()}
      >
        {/* <ReactPlayer
          className={`mb-4`}
          url={`https://www.youtube.com/watch?v=m8kBcCIQvx8`}
          width={`100%`}
          height={isMobile ? `300px` : `520px`}
        /> */}
        <MediaPlayer url={`https://www.youtube.com/watch?v=m8kBcCIQvx8`} className={`mb-4`} height={isMobile ? `300px` : `520px`}/>
      </Modal>
    </UserLayout>
  );
};

const rowStyles = {
  discount: {
    background:
      "linear-gradient(to right, rgba(0, 255, 0, 0.6) 0%, rgba(0, 159, 255, 0.9) 100%), url('/assets/images/landing_page_sell_projects.jpg')",
    minHeight: isMobile ? 400 : 500
  }
};

export default HowItWorksPage;
