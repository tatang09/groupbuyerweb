import React, { useState, useEffect, createRef } from "react";
import { debounce } from "lodash";

import UserGlobal from "~/states/userGlobal";
import UserLayout from "~/layouts/users/UserLayout";
import { Table, TextInput, Tooltip } from "~/components/_base";
import { isEmpty } from "lodash";

import CurrencyFormat from "react-currency-format";
import moment from "moment";
import { isMobile } from "react-device-detect";
import { SalesAdviceModal } from "~/components";
import {
  purchasersMapper,
  solicitorDetailsMapper,
  additionalInfoMapper
} from "~/helpers/salesAdviceHelper/salesAdviceHelper";

import { addressHelper } from "~/helpers/addressHelper";
import { Helmet } from "react-helmet";
import { metaHelper } from "../helpers/metaHelper/metaHelper";

const SecuredDealsList = ({ isAdmin }) => {
  const [userState, userAction] = UserGlobal();
  const [toggleFetch, setToggleFetch] = useState(false);
  const [keyword, setKeyword] = useState("");
  const [deals, setDeals] = useState([]);
  const [selectedDeal, setSelectedDeal] = useState({});
  const [loading, setLoading] = useState(true);
  const [selectedDealProperty, setSelectedDealProperty] = useState({});
  const [purchasers, setPurchasers] = useState([]);
  const [addtionalInfo, setAdditionalInfo] = useState({});
  const [solicitorDetails, setSolicitorDetail] = useState({});

  const tableHeaders = [
    {
      label: "Project Name",
      width: 230
    },
    {
      label: "Address",
      width: 150
    },
    {
      label: "Developer Price",
      width: 150
    },
    {
      label: "Discount",
      width: 60
    },
    {
      label: "GroupBuyer Price",
      width: 150
    },
    {
      label: "Savings",
      width: 150
    },
    // {
    //   label: "Status",
    //   width: 150
    // },
    {
      label: "Properties Left",
      width: 120
    },
    {
      label: "Secured Date",
      width: 150
    },
    {
      label: "Proposed Settlement",
      width: 150
    },
    {
      label: "Sales Advice",
      width: 30
    },
    {
      label: "View Deal",
      width: 80
    }
  ];

  const handleSalesAdviceModal = async deal => {
    setLoading(true);

    await axios.get(`/api/property/${deal.property_id}`).then(res => {
      if (res.status == 200) {
        setSelectedDealProperty(res.data);
      }
    });

    await axios.get(`/api/sales-advice/${deal.secured_deals_id}`).then(res => {
      if (res.status == 200) {
        setPurchasers(purchasersMapper(res.data.purchasers));
        setAdditionalInfo(additionalInfoMapper(res.data.additionalDetails));
        setSolicitorDetail(solicitorDetailsMapper(res.data.solicitorDetails));
      }
    });

    setSelectedDeal(deal);
    setLoading(false);
    userAction.setState({ showSalesAdviceModal: true });
  };

  const onClose = () => {
    userAction.setState({ showSalesAdviceModal: false });
  };

  const discountedPrice = deal => {
    if (isEmpty(deal)) return;
    return deal.property_price - deal.property_price * (deal.discount / 100);
  };

  const renderSecuredDeals = () => {
    return (
      <>
        <tbody>
          {deals &&
            deals.map((deal, index) => {
              let originalPrice = parseInt(deal.property_price || 0);
              let address = addressHelper(deal.deal_address);
              return !isMobile ? (
                <tr
                  key={index}
                  className={`hover:bg-gray-100 border-b text-base`}
                >
                  <td className="pl-2">
                    <div className={`my-3`}>
                      <div className={`font-bold text-palette-purple`}>
                        {deal.deal_name}
                      </div>
                      <div className={`opacity-50 text-sm`}>
                        {"Apartment " + deal.property_unit_name}
                      </div>
                    </div>
                  </td>
                  <td className="pl-2">{address}</td>
                  <td className="pl-4">
                    <CurrencyFormat
                      value={originalPrice}
                      displayType={`text`}
                      thousandSeparator={true}
                      prefix={`$`}
                    />
                  </td>
                  <td className="pl-4">{`${
                    deal.discount ? deal.discount + "%" : "N/A"
                  }`}</td>
                  <td className="pl-4 font-semibold text-red-700">
                    <Tooltip title={`Price discount: ${deal.discount}%`}>
                      <CurrencyFormat
                        value={
                          deal.property_price -
                          deal.property_price * (deal.discount / 100)
                        }
                        displayType={`text`}
                        thousandSeparator={true}
                        prefix={`$`}
                      />
                    </Tooltip>
                  </td>

                  <td className="pl-2 text-palette-blue-light">
                    <CurrencyFormat
                      value={(deal.property_price * deal.discount) / 100 || 0}
                      displayType={`text`}
                      thousandSeparator={true}
                      prefix={`$`}
                    />
                  </td>
                  <td className="pl-12">{`${deal.property_available}/5`}</td>
                  <td className="pl-2">
                    {moment(deal.created_at).format("DD-MM-YYYY")}
                  </td>
                  <td className="pl-8">
                    {moment(deal.proposed_settlement).format("DD-MM-YYYY")}
                  </td>
                  <td
                    className={` ${
                      userState.adminDrawerOpen ? "pl-2" : "pl-8"
                    }`}
                  >
                    <div
                      onClick={() => handleSalesAdviceModal(deal)}
                      className={`cursor-pointer bg-white border border-gray-400 duration-300 hover:bg-palette-purple hover:border-palette-purple hover:text-white inline-flex px-3 py-1 rounded transition-all`}
                    >
                      <svg className={`feather-icon h-5 w-5 opacity-50`}>
                        <use
                          xlinkHref={`/assets/svg/feather-sprite.svg#edit`}
                        />
                      </svg>
                    </div>
                  </td>
                  <td className="lg:pl-4">
                    <a
                      href={`/weekly-deals/${deal.deal_id}/apartment/${deal.property_id}/false`}
                    >
                      <div
                        className={`cursor-pointer bg-white border border-gray-400 duration-300 hover:bg-palette-purple hover:border-palette-purple hover:text-white inline-flex px-3 py-1 rounded transition-all`}
                      >
                        <svg className={`feather-icon h-5 w-5 opacity-50`}>
                          <use
                            xlinkHref={`/assets/svg/feather-sprite.svg#eye`}
                          />
                        </svg>
                      </div>
                    </a>
                  </td>
                </tr>
              ) : (
                <tr key={index} className={`flex flex-col`}>
                  <td
                    className={`pl-2 rounded`}
                    style={{ boxShadow: "0 5px 28px 4px rgba(0,0,0,0.1)" }}
                  >
                    <div className={`flex items-center justify-between`}>
                      <div className={`flex items-center`}>
                        <img
                          src={`${deal.project_images[0]}`}
                          className={`rounded-full w-12 h-12 mr-3`}
                        />
                        <div>
                          <div
                            className={`font-bold text-base text-palette-purple`}
                          >
                            {deal.deal_name}
                          </div>
                          <div className={`opacity-50 text-sm`}>
                            {"Apartment " + deal.property_unit_name}
                          </div>
                        </div>
                      </div>
                      <div
                        onClick={() => handleSalesAdviceModal(deal)}
                        className={`cursor-pointer bg-white border border-gray-400 duration-300 hover:bg-palette-purple hover:border-palette-purple hover:text-white inline-flex px-3 py-1 rounded transition-all`}
                      >
                        <svg className={`feather-icon h-5 w-5 opacity-50`}>
                          <use
                            xlinkHref={`/assets/svg/feather-sprite.svg#edit`}
                          />
                        </svg>
                      </div>
                      <a
                        href={`/weekly-deals/${deal.deal_id}/apartment/${deal.property_id}/false`}
                      >
                        <div
                          className={`cursor-pointer bg-white border border-gray-400 duration-300 hover:bg-palette-purple hover:border-palette-purple hover:text-white inline-flex px-3 py-1 rounded transition-all`}
                        >
                          <svg className={`feather-icon h-5 w-5 opacity-50`}>
                            <use
                              xlinkHref={`/assets/svg/feather-sprite.svg#eye`}
                            />
                          </svg>
                        </div>
                      </a>
                    </div>
                    <hr className="block my-2 border border-gray-200" />
                    <div>{address}</div>
                    <div className={`flex items-center justify-between mt-2`}>
                      <div>
                        <span className={`uppercase font-bold`}>
                          Deal Price:{" "}
                        </span>
                        {discountedPrice(
                          deal.discount_is_percentage,
                          deal.discount,
                          deal.property_price
                        )}
                      </div>
                      <div>
                        <span className={`uppercase font-bold`}>Savings: </span>
                        <CurrencyFormat
                          value={
                            deal.discount_is_percentage
                              ? deal.property_price * deal.discount
                              : deal.property_price - deal.discount || 0
                          }
                          displayType={`text`}
                          thousandSeparator={true}
                          prefix={`$`}
                          className={`text-palette-blue-light`}
                        />
                      </div>
                    </div>
                  </td>
                  <td className={`p-3`}></td>
                </tr>
              );
            })}
          {!isMobile && (
            <tr>
              <td colSpan={6}>
                <div className={`text-right mt-3`}>
                  <span
                    className={`font-bold uppercase text-base mr-3 opacity-50`}
                  >
                    Total savings:
                  </span>
                  <CurrencyFormat
                    value={deals.reduce(
                      (a, b) => b["property_price"] * (b["discount"] / 100) + a,
                      0
                    )}
                    displayType={`text`}
                    thousandSeparator={true}
                    prefix={`$`}
                    className={`text-lg font-bold text-palette-blue-light`}
                  />
                </div>
              </td>
            </tr>
          )}
        </tbody>
      </>
    );
  };

  const renderHeaders = () => {
    return (
      <thead>
        <tr>
          {tableHeaders.map((th, index) => {
            return (
              <th
                style={{ width: th.width }}
                className={`border-b font-bold text-palette-gray pl-2 text-left`}
                key={index}
              >
                {th.label}
              </th>
            );
          })}
        </tr>
      </thead>
    );
  };

  const handleSearch = debounce(value => {
    setKeyword(value);
  }, 800);

  const renderMainChildComponent = () => {
    return (
      <>
        <h1
          className={`font-bold leading-tight text-4xl pt-10 pb-8 lg:pb-16 text-center`}
        >
          Secured deals
        </h1>

        <div className={`flex justify-center lg:justify-end mb-8`}>
          <div className={`w-64 relative `}>
            <svg
              className={`absolute feather-icon h-full left-0 mx-3 text-gray-500`}
            >
              <use xlinkHref={`/assets/svg/feather-sprite.svg#search`} />
            </svg>
            <TextInput
              className={`pl-8`}
              type={`text`}
              placeholder={`Search`}
              border={false}
              appearance={true}
              onChange={e => handleSearch(e.target.value)}
            />
          </div>
        </div>
        <Table
          query={`/api/secure-deal`}
          toggleFetch={toggleFetch}
          keyword={keyword}
          getData={setDeals}
          content={renderSecuredDeals()}
          header={isMobile ? null : renderHeaders()}
        />
      </>
      //   </div>
      // </section>
    );
  };

  if (isAdmin) {
    return (
      <div className={`bg-white mt-10`}>
        <div className={`pb-16 px-6 lg:px-10 mx-auto`}>
          {renderMainChildComponent()}
          {!loading && (
            <SalesAdviceModal
              show={userState.showSalesAdviceModal}
              onClose={() => onClose()}
              deal={selectedDeal}
              apartment={selectedDealProperty}
              purchasersParam={purchasers}
            />
          )}
        </div>
      </div>
    );
  } else {
    return (
      <UserLayout>
        <Helmet>
          
          <meta name="description" content={metaHelper.desc} />
       
        </Helmet>
        <section
          className={`bg-palette-blue-dark`}
          style={{ marginTop: -130, paddingTop: 160 }}
        >
          <section className={`relative bg-white`}>
            <div className={`pb-16 px-6 lg:px-20`}>
              {renderMainChildComponent()}
              {!loading && (
                <SalesAdviceModal
                  show={userState.showSalesAdviceModal}
                  onClose={() => onClose()}
                  deal={selectedDeal}
                  apartment={selectedDealProperty}
                  purchasersParam={purchasers}
                />
              )}
            </div>
          </section>
        </section>
      </UserLayout>
    );
  }
};

export default SecuredDealsList;
