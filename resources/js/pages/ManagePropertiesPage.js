import React, { useState, useEffect } from "react";
import UserLayout from "~/layouts/users/UserLayout";
import { Table, Tooltip, Button } from "~/components/_base";
import EditPropertyModal from "../components/EditPropertyModal";
import AddPropertyModal from "../components/AddPropertyModal";
import AddLandProperty from "../components/AddLandProperty";
import EditLandPropertyModal from "../components/EditLandPropertyModal";
 
import CurrencyFormat from "react-currency-format";

import { debounce } from "lodash";
import { isMobile } from "react-device-detect";
 
import { addressHelper } from "../helpers/addressHelper";

import { faPlusCircle } from "@fortawesome/free-solid-svg-icons";

import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

import { Helmet } from "react-helmet";
import { metaHelper } from "../helpers/metaHelper/metaHelper";

const ManageProperties = ({ dealId, dealSubPropertyId }) => {
  
  const [keyWord, setKeyWord] = useState("");
  const [toggleFetch, setToggleFetch] = useState(false);
  const [currentProject, setCurrentProject] = useState([]);
  const [filters, setFilters] = useState({
    sortBy: { value: "deals.name", order: "desc" }
  });
  const [toggleOrder, setToggleOrder] = useState(false);
  const [toggleEditProperty, setToggleEditProperty] = useState(false);
  const [toggleAddProperty, setToggleAddProperty] = useState(false);
  const [toogleLandProperty, setToogleLandProperty] = useState(false);
  const [toogleEditLandProperty, setToogleEditLandProperty] = useState(false);
  const [property, setProperty] = useState([]);
  const [isProjectLand, setIsProjectLand] = useState(dealSubPropertyId == 4 ? true : false);
 
  const formatFilter = column => {
    if (column) {
      setToggleOrder(!toggleOrder);

      setFilters({
        sortBy: { value: column, order: toggleOrder ? "desc" : "asc" }
      });

      setToggleFetch(!toggleFetch);
    }
  };

  useEffect(() => {
    if (!toggleAddProperty) {
      setToggleFetch(!toggleFetch);
    }
    return () => {};
  }, [toggleAddProperty]);

  useEffect(() => {
    if (!toggleEditProperty) {
      setToggleFetch(!toggleFetch);
    }
    return () => {};
  }, [toggleEditProperty]);

  const tableHeaders = [
    {
      label: "Property Id",
      width: 80,
      show: isMobile ? false : true
    },
    {
      label: isMobile ? "Property#" : "Property Number",
      column: "properties.unit_name",
      width: isMobile ? 70 : 100,
      show: true
    },
    {
      label: "Property Address",
      column: "property.address",
      width: 200,
      show: isMobile ? false : true
    },
    {
      label: "Property Price",
      column: "property.price",
      width: isMobile ? 100 : 150,
      show: true
    },
    {
      label: "Approved",
      column: "property.is_property_approved",
      width: isMobile ? 80 : 150,
      show: true
    },

    {
      label: "Edit",
      column: "",
      width: 40,
      show: true
    }
  ];

  const handleSearch = debounce(text => {
    setKeyWord(text);
  }, 800);

  const renderHeaders = () => {
    return (
      <thead>
        <tr>
          {tableHeaders.map((th, index) => {
            if (th.show) {
              return (
                <th
                  style={{ width: th.width }}
                  className={`sm:text-base border-b font-bold md:px-4 md:py-2 text-palette-gray`}
                  key={index}
                >
                  {th.label}
                </th>
              );
            }
          })}
        </tr>
      </thead>
    );
  };

  const getProperty = async propId => {
    const url = `/api/property/${propId}`;
    await axios.get(url).then(res => {
      if (res.data.property_type_id == 4) {
        setIsProjectLand(true);
      } else {
        setIsProjectLand(false);
      }
      setProperty(res.data);
      showModal(res.data.property_type_id == 4);
    });
  };

  const showModal = (show) => {
    if (!show) {
      setToggleEditProperty(!toggleEditProperty);
    } else {
      setToogleEditLandProperty(!toogleEditLandProperty);
    }
  };

  const toggleEditPropertyModal = () => {
    if (!isProjectLand) {
      setToggleEditProperty(!toggleEditProperty);
    } else {
      setToogleEditLandProperty(!toogleEditLandProperty);
    }
  };

  const toggleAddPropertyModal = () => {
    if (!isProjectLand) {
      setToggleAddProperty(!toggleAddProperty);
    } else {
      setToogleLandProperty(!toogleLandProperty);
    }
  };
 

  const propertiesContent = () => {
    return (
      <>
        <tbody>
          {currentProject.properties &&
            currentProject.properties.length > 0 &&
            currentProject.properties.map(property => {
              return (
                <tr
                  key={property.id}
                  className={`hover:bg-gray-100 border-b text-base`}
                >
                  {!isMobile && (
                    <td className={`pl-10 text-left`}>{property.id}</td>
                  )}
                  <td
                    className={`cursor-pointer font-bold lg:pl-10 pl-5 text-left text-palette-purple transform`}
                  >
                    {property.unit_name}
                  </td>

                  <td className={`p-4`}>{addressHelper(property.address)}</td>

                  {/* <td className={`p-4`}> {property.floor_area} </td> */}
                  <td className={`lg:p-4 py-4`}>
                    <CurrencyFormat
                      value={property.price}
                      displayType={"text"}
                      thousandSeparator={true}
                      prefix={"$"}
                    />
                  </td>
                  <td className={`lg:p-4 py-4`}>
                    <svg
                      className={`feather-icon ml-4 ${
                        property.is_property_approved
                          ? "text-palette-purple h-6 w-6"
                          : "text-gray-500 h-5 w-5"
                      }`}
                    >
                      <use xlinkHref={`/assets/svg/feather-sprite.svg#check`} />
                    </svg>
                  </td>
                  <td className={`flex md:p-4 py-4`}>
                    {!property.is_property_approved ? (
                      <Tooltip title={`Edit Property`}>
                        <button
                          onClick={() => getProperty(property.id)}
                          className={`bg-white border-gray-400 cursor-pointer duration-300 hover:bg-palette-purple hover:border-palette-purple hover:text-white inline-flex md:border md:px-4 pr-4 py-1 rounded transition-all`}
                        >
                          <svg className={`feather-icon h-5 w-5 opacity-50`}>
                            <use
                              xlinkHref={`/assets/svg/feather-sprite.svg#edit`}
                            />
                          </svg>
                        </button>
                      </Tooltip>
                    ) : (
                      <Tooltip
                        title={`Property is already approved. Changes are not allowed.`}
                      >
                        <svg className={`feather-icon h-5 w-5 opacity-50`}>
                          <use
                            xlinkHref={`/assets/svg/feather-sprite.svg#lock`}
                          />
                        </svg>
                      </Tooltip>
                    )}
                  </td>
                </tr>
              );
            })}
        </tbody>
      </>
    );
  };

  return (
    <>
      <UserLayout>
        <Helmet>
       
          <meta name="description" content={metaHelper.desc} />
     
        </Helmet>
        {toggleEditProperty && (
          <EditPropertyModal
            project={currentProject}
            projectName={currentProject.name}
            projectId={currentProject.id}
            property={property}
            toggleEditPropertyModal={() => toggleEditPropertyModal()}
            editToggleFetch={() => {
              setToggleFetch(!toggleFetch);
            }}
          ></EditPropertyModal>
        )}
        {toggleAddProperty && (
          <AddPropertyModal
            projectName={currentProject.name}
            projectId={currentProject.id}
            toggleAddPropertyModal={() => toggleAddPropertyModal()}
            addToggleFetch={() => {
              setToggleFetch(!toggleFetch);
            }}
          ></AddPropertyModal>
        )}
        {toogleLandProperty && (
          <AddLandProperty
            projectName={currentProject.name}
            projectId={currentProject.id}
            toggleAddPropertyModal={() => toggleAddPropertyModal()}
            addToggleFetch={toggleFetch}
          ></AddLandProperty>
        )}
        {toogleEditLandProperty && (
          <EditLandPropertyModal
            projectName={currentProject.name}
            projectId={currentProject.id}
            property={property}
            toggleEditPropertyModal={() => toggleEditPropertyModal()}
            editToggleFetch={toggleFetch}
          ></EditLandPropertyModal>
        )}
        <section
          className={`bg-palette-blue-dark`}
          style={{ marginTop: -130, paddingTop: 160 }}
        >
          <section className={`relative bg-white`}>
            <div
              className={`pb-16 px-6 lg:px-20 mx-auto`}
              style={{ maxWidth: 1600 }}
            >
              <div
                className={`flex items-center flex items-center lg:pb-16 pb-10 pt-10`}
              >
                <span
                  className={`bg-white cursor-pointer duration-300 font-bold hover:text-palette-blue inline-flex items-center pr-3 relative text-palette-blue-light text-sm transition-all z-10`}
                  onClick={() => (window.location = "/profile/manage-projects")}
                >
                  <svg className={`feather-icon h-4 w-4 mr-1 inline`}>
                    <use
                      xlinkHref={`/assets/svg/feather-sprite.svg#chevron-left`}
                    />
                  </svg>
                  Back to Projects page
                </span>
                <h1
                  className={`flex-1 font-bold leading-tight text-4xl text-center`}
                >
                  <span className="text-center xs:text-3xl">
                    {" "}
                    Manage properties
                  </span>
                </h1>
              </div>

              <div className="flex justify-between mb-8">
                <div className="ml-5 rounded-lg text-base">
                  <h1>
                    Project:
                    <span className={`font-semibold`}>
                      &nbsp;{currentProject.name && currentProject.name}
                    </span>
                  </h1>
                  <h2>
                    Property Count:&nbsp;
                    <span className={`font-semibold`}>
                      {currentProject.properties
                        ? `${currentProject.properties.length} / 5`
                        : 0 / 5}
                    </span>
                  </h2>
                </div>
                <div className={`flex justify-center lg:justify-end`}>
                  {currentProject.properties &&
                  currentProject.properties.length < 5 ? (
                    <Tooltip title={`Click to add a Project`}>
                      <div className={`w-auto`}>
                        <Button
                          className={`flex justify-around items-center`}
                          onClick={() => toggleAddPropertyModal()}
                        >
                          <FontAwesomeIcon
                            className={`mr-2 `}
                            icon={faPlusCircle}
                          />
                          Add Property
                        </Button>
                      </div>
                    </Tooltip>
                  ) : (
                    <Tooltip
                      title={`You have reached the maximum 5 properties per project. Once all 5 properties are sold you can upload another 5 properties..`}
                    >
                      <div className={`w-64 relative `}>
                        <svg
                          className={`absolute feather-icon h-full left-0 ml-5 text-gray-500`}
                        >
                          <use
                            xlinkHref={`/assets/svg/feather-sprite.svg#plus-circle`}
                          />
                        </svg>
                        <Button
                          disabled={true}
                          onClick={() => toggleAddPropertyModal()}
                        >
                          Add Property
                        </Button>
                      </div>
                    </Tooltip>
                  )}
                </div>
              </div>
              <Table
                query={`/api/deal/${dealId}?paginated=true`}
                toggleFetch={toggleFetch}
                keyword={keyWord}
                getData={data => setCurrentProject(data[0])}
                content={propertiesContent()}
                sort={filters.sortBy.value.replace(/asc_|desc_/g, "") || ""}
                order={filters.sortBy.order || ""}
                header={renderHeaders()}
              />
            </div>
          </section>
        </section>
      </UserLayout>
    </>
  );
};

export default ManageProperties;
