import React from "react";
import UserLayout from "~/layouts/users/UserLayout";
import { Helmet } from "react-helmet";
import { metaHelper } from "../helpers/metaHelper/metaHelper";
 
const SellingPage = () => {
  return (
    <UserLayout>
      <Helmet>
       
        <meta name="description" content={metaHelper.desc} />
    
      </Helmet>
      <section
        className="text-white bg-palette-blue-dark pb-20"
        style={{ marginTop: -125, paddingTop: 160 }}
      >
        <div className="bg-cover bg-center relative" style={rowStyles.selling}>
          <div className="mx-auto pb-16" style={{ maxWidth: 1280 }}>
            <div className="md:flex md:w-full pt-10 md:p-10 lg:pt-10">
              <div className="flex-1 m-2 px-4 py-2 text-gray-700">
                <h3 className="font-bold leading-10 text-4xl text-gray-100 text-left">
                  With an over-supply of property on the market in Australia and
                  so many competing developers Group Buyer is your solution to
                  making bulk sales faster than ever before.
                </h3>
                <ul className="font-hairline mt-10 pl-8 text-base text-white">
                  <li className="list-disc p-2">
                    Provides a platform for bulk-sales
                  </li>
                  <li className="list-disc p-2">Access to a larger audience</li>
                  <li className="list-disc p-2">Faster sales made 24/7</li>
                  <li className="list-disc p-2">No marketing costs</li>
                  <li className="list-disc p-2">
                    Sell through projects faster
                  </li>
                </ul>
                <p className="mt-6 font-hairline text-base text-white">
                  Apply to become a partner. We'll get back in touch with you to
                  discuss your project requirements.
                </p>
              </div>
              <div className="flex-1 text-gray-700 text-center">
                <div>
                  <img src="/assets/images/landing_page_sell_image.jpg" />
                </div>
              </div>
            </div>
            <div className="flex flex-col pl-16">
              <p className="font-hairline text-base text-white">
                You can also{" "}
                <a href="tel:1300">
                  <span className="hover:text-gray-600 text-sub-500">Call</span>
                </a>{" "}
                or Send us a{" "}
                <a href="">
                  <span className="hover:text-gray-600 text-sub-500">
                    Message
                  </span>
                </a>{" "}
                to get in touch.
              </p>
              <button className="border border-gray-100 rounded-full text-white w-1/6 h-12 hover:bg-yellow-200 hover:text-primary-900">
                View Projects
              </button>
              <h3 className="font-bold text-4xl text-white pt-6">
                Our Partners
              </h3>
            </div>
          </div>
        </div>
      </section>
    </UserLayout>
  );
};

const rowStyles = {
  selling: {
    background: `linear-gradient(to left, blue 0%, rgba(0, 0, 255, 0.8) 0%, rgba(255, 0, 255, 0.8) 100%), url('/assets/images/landing_page_sell_projects.jpg')`
  }
};

export default SellingPage;
