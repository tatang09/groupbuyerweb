import React, { useState } from "react";

import UserGlobal from "~/states/userGlobal";
import { isMobile } from "react-device-detect";

import UserLayout from "~/layouts/users/UserLayout";
import Collapse from "@material-ui/core/Collapse";

import { dummyFAQ } from "../helpers/faqMockData";

import { makeStyles } from "@material-ui/core/styles";
import Pagination from "@material-ui/lab/Pagination";
import { Helmet } from "react-helmet";
import { metaHelper } from "../helpers/metaHelper/metaHelper";

const useStyles = makeStyles(theme => ({
  root: {
    "& > *": {
      marginTop: theme.spacing(2)
    }
  }
}));

const FrequentlyAskedQuestions = () => {
  const classes = useStyles();

  const [userState] = UserGlobal();
  const [faq, setFAQ] = useState(dummyFAQ);
  const [total, setTotal] = useState(faq.length);
  const [initialCount, setInitialCount] = useState(10);

  const handleClick = id => {
    let data = Object.assign([], faq);

    data.map(f => {
      if (f.id == id) {
        f.isOpen = !f.isOpen;
      } else {
        f.isOpen = false;
      }
    });

    setFAQ(data);
  };

  const renderFAQ = () => {
    return (
      <table className={`bg-white rounded-b table-fixed text-left lg:w-3/4`}>
        <tbody className={`flex flex-wrap mb-6 px-4 lg:px-0`}>
          {faq.map(data => {
            return (
              <tr
                key={data.id}
                className={`lg:mb-4 lg:px-8 mb-8 w-full`}
                onClick={() => {
                  handleClick(data.id);
                }}
              >
                <td className={`bg-white block border-b`}>
                  <div className={`pt-3`}>
                    <div
                      className={`flex items-center justify-between pb-1 mb-2`}
                    >
                      <span className={`text-base font-bold text-gray-900`}>
                        {data.question}
                      </span>
                      <span
                        className={`cursor-pointer hover:text-purple-800 lg:ml-0 ml-2 text-gray-900`}
                      >
                        <svg className={`feather-icon`}>
                          <use
                            xlinkHref={`/assets/svg/feather-sprite.svg#${
                              data.isOpen ? "chevron-up" : "chevron-down"
                            }`}
                          />
                        </svg>
                      </span>
                    </div>
                  </div>

                  <Collapse in={data.isOpen}>
                    <div className={`flex flex-col justify-between mb-3`}>
                      <span className={`text-base text-gray-900 italic`}>
                        {`${data.answer}`}
                      </span>
                      <span
                        className={`font-bold italic mr-16 mt-2 pr-4 text-base text-gray-900 text-right w-full`}
                      >
                        - GroupBuyer
                      </span>
                    </div>
                  </Collapse>
                </td>
              </tr>
            );
          })}
        </tbody>
      </table>
    );
  };

  return (
    <UserLayout>
      <Helmet>
      
        <meta name="description" content={metaHelper.desc} />
      
      </Helmet>
      <section
        className="text-white bg-palette-blue-dark"
        style={{ marginTop: -130, paddingTop: 160 }}
      >
        <div className="bg-gray-200 flex flex-col items-center lg:p-16 mx-auto pb-10 pt-8 px-4 text-white">
          <h1
            className={
              "bg-white font-bold leading-tight lg:w-3/4 w-full px-1 py-8 rounded-t text-3xl text-4xl text-center text-gray-900"
            }
          >
            Have questions? We're here to help.
          </h1>
          <div className={`flex justify-center`}>{renderFAQ()}</div>
        </div>
      </section>
    </UserLayout>
  );
};

export default FrequentlyAskedQuestions;
