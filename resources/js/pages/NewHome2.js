import React, { useState, createRef } from "react";

// JSX
import HeroSlider, { Slide, Nav, SideNav, OverlayContainer } from "hero-slider";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { Checkbox } from "pretty-checkbox-react";
import Wrapper from "../ui/Wrapper/Wrapper";
import Title from "../ui/Title/Title";
import Button from '@material-ui/core/Button';
import Modal from '@material-ui/core/Modal';

import UserGlobal from "~/states/userGlobal";
import { axios } from "~/helpers/bootstrap";
import { setToken } from "~/services/auth";
import { useForm } from "../hooks/customHooks";
import { CurrentPopularDeals, TheMore, PieceMeal, RegistrationForm, LoginForm } from "~/components";

import { sweetAlert } from "../components/_base/alerts/sweetAlert";
// Images
const carrousel_1 = "/assets/images/homepage_carrousel_1.jpg";
const carrousel_2 = "/assets/images/homepage_carrousel_2.jpg";
const carrousel_3 = "/assets/images/homepage_carrousel_3.jpg";
// const carrousel_4 = "/assets/images/homepage_carrousel_4.jpg";
const carrousel_b_1 = "/assets/images/homepage_carrousel_b_1.jpg";
const carrousel_b_2 = "/assets/images/homepage_carrousel_b_2.jpg";
const carrousel_b_3 = "/assets/images/homepage_carrousel_b_3.jpg";
const carrousel_b_4 = "/assets/images/homepage_carrousel_b_4.jpg";
const chevron_right = "/assets/images/chevron-right.svg";

const triple_chevron_down_purple =
  "/assets/images/triple-chevron-down-purple.svg";
const easy_step_bg = "/assets/images/easy_steps_bg.jpg";
const commonwealth_bank = "/assets/svg/commbank.svg";
import UserLayout from "~/layouts/users/UserLayout";
import { Helmet } from "react-helmet";
import { metaHelper } from "../helpers/metaHelper/metaHelper";

const NewHome = () => {
  const [userState, userAction] = UserGlobal();
  const [modal, setModal] = useState(false);
  const [modalLogin, setModalLogin] = useState(false);
  const [state, formChange] = useForm({
    first_name: "",
    last_name: "",
    email: "",
    password: "",
    user_type: "customer"
  });
  const [errors, setErrors] = useState({});
  const [loading, setLoading] = useState(false);
  const form = createRef();

  const handleLoginModal = () => {
    setModal(false);
    setModalLogin(true);
  }
  const handleLoginModalToSignUp = () => {
    setModalLogin(false);
    setModal(true);
  }
  const handleOpenModal = () => {
    setModal(true);
  };

  const handleCloseModal = () => {
    setModal(false);
    setModalLogin(false);
  };

  const handleSubmit = async e => {
    e.preventDefault();
    let errors = {};
    let formData = { ...state };
    console.log(formData);
    setLoading(true);

    try {
      let { data } = await axios.post("/api/register", formData);

      setToken(data.access_token);

      setLoading(false);

      userAction.setState({ fetch: !userState.fetch });

      userAction.setState({ userCount: userState.userCount + 1 });

      handleClose();

      setTimeout(() => {
        sweetAlert(
          "success",
          "Thank you for registering and welcome to GroupBuyer."
        );
      }, 2000);
    } catch (error) {
      let { data } = error.response;

      errors = data.errors;
      setLoading(false);
    }
    setErrors(errors || {});
  };
  return (
    <UserLayout>
      <Helmet>
        <meta name="description" content={metaHelper.desc} />
      </Helmet>
      <section id="headPart">
        <HeroSlider
          slidingAnimation="left_to_right"
          orientation="horizontal"
          initialSlide={1}
          style={
            {
              // backgroundColor: "linear-gradient(to top, rgba(255, 0, 255, 1) 0%, rgba(47, 54, 86, 1) 100%)"
              // background: "transparent linear-gradient(180deg, #4705B4 0%, #171617 100%) 0% 0% no-repeat padding-box"
            }
          }
          settings={{
            slidingDuration: 250,
            slidingDelay: 100,
            shouldAutoplay: false,
            shouldDisplayButtons: false,
            autoplayDuration: 5000,
            height: "120vh",
            backgroundColor: "blue"
          }}
        >
          <OverlayContainer>
            <div
              className="absolute w-screen h-full"
              style={{
                background:
                  "linear-gradient(359deg, rgb(71 5 180) 0%, rgb(23, 22, 23) 80%, rgb(23, 22, 23) 100%) 0% 0% no-repeat padding-box padding-box transparent",
                opacity: 0.65,
                backgroundBlendMode: "multiply"
              }}
            ></div>
            <div
              className={`flex flex-col relative z-10 items-center justify-center h-full`}
            >
              <span
                className={`text-white text-center leading-none font-bold mb-8 md:mt-32 xs:mt-32 xs:text-5xl banner-text-landing`}
                style={{ fontSize: "5.5rem" }}
              >
                Welcome to <br />
                GroupBuyer
              </span>
              <span
                className={`text-white text-center text-base leading-normal font-light px-8`}
              >
                Together, we are stronger: Via our exceptional{" "}
                <b className="text-palette-purple-main">power of purchase</b>,
                we give access to <br />
                exclusive opportunities to{" "}
                <b className="text-palette-purple-main">buy new homes</b> from
                trusted developers at{" "}
                <b className="text-palette-purple-main">unbeatable prices.</b>
              </span>

              <button
                className={
                  "rounded-sm text-white border border-white text-base font-light py-2 text-center w-full max-w-xs mt-8 mb-4"
                }
                onClick={handleOpenModal}
              >
                Join us for free
              </button>
              <button
                className={
                  "rounded-sm text-white text-base font-bold py-2 text-center mb-6 w-full max-w-xs"
                }
                style={{ backgroundColor: "#E91AFC" }}
              >
                Browse the properties
              </button>

              <h2 className="text-white text-base font-light mb-3 xs:mt-16 lg:mt-8 learn-more-landing">
                Learn more
              </h2>
              <a href="#four-easy-step">
                <img src={triple_chevron_down_purple} className={"w-6"} />
              </a>
            </div>
          </OverlayContainer>

          {/* <Slide
            background={{
              backgroundImage: carrousel_1,
              backgroundAttachment: "fixed",
              shouldLazyLoad: false
            }}
          />
          {/* <Nav /> */}
        </HeroSlider>
      </section>
      <TheMore bgColor={"#4D0B98"} />
      <PieceMeal breakpoint={`md`} />

      <section
        id="access_opportunities_section"
        className="access_opportunities_section"
      >
        <HeroSlider
          slidingAnimation="left_to_right"
          orientation="horizontal"
          initialSlide={1}
          style={
            {
              // backgroundColor: "linear-gradient(to top, rgba(255, 0, 255, 1) 0%, rgba(47, 54, 86, 1) 100%)"
              // background: "transparent linear-gradient(180deg, #4705B4 0%, #171617 100%) 0% 0% no-repeat padding-box"
            }
          }
          className={"access_opportunities"}
          settings={{
            slidingDuration: 250,
            slidingDelay: 100,
            shouldAutoplay: true,
            shouldDisplayButtons: true,
            autoplayDuration: 5000,
            height: "118vh",
            backgroundColor: "blue"
          }}
        >
          <OverlayContainer>
            <div
              className="absolute w-screen h-full"
              style={{
                background:
                  "linear-gradient(359deg,#171617 0%, #171617 85%, #171617 100%) 0% 0% no-repeat padding-box padding-box black",
                opacity: 0.65
              }}
            ></div>
            <div
              className={`flex flex-col relative z-10 items-center justify-center h-full`}
            >
              <span
                className={`text-white text-center leading-none font-bold mb-8 xs:text-4xl`}
                style={{ fontSize: "6rem" }}
              >
                Access to{" "}
                <span className={"text-palette-purple-main"}>Exclusive</span>{" "}
                <br />
                Opportunities
              </span>
            </div>
          </OverlayContainer>

          <Slide
            background={{
              backgroundImage: carrousel_b_1,
              backgroundAttachment: "fixed",
              shouldLazyLoad: false
            }}
            // style={{
            //   background: `linear-gradient(180deg, #4705B4 0%, #171617 100%), url('${carrousel_1}') 0% 0% no-repeat padding-box padding-box rgba(255, 255, 255, 0)`
            // }}
          />

          <Slide
            background={{
              backgroundImage: carrousel_b_2,
              backgroundAttachment: "fixed",
              shouldLazyLoad: false
            }}
          />

          <Slide
            background={{
              backgroundImage: carrousel_b_3,
              backgroundAttachment: "fixed",
              shouldLazyLoad: false
            }}
          />

          {/* <Slide
            background={{
              backgroundImage: carrousel_b_4,
              backgroundAttachment: "fixed",
              shouldLazyLoad: false
            }}
          /> */}

          <Nav />
        </HeroSlider>
      </section>
      <section id="be-in-the-loop">
        <form
          ref={form}
          // className={`flex-row lg:p-6 text-white`}
          onSubmit={handleSubmit}
        >
          <div className={"flex xs:flex-col-reverse w-full"}>
            <div
              className={"w-full lg:w-1/2"}
              style={{ backgroundColor: "#4D0B98" }}
            >
              <RegistrationForm/>
            </div>
            <div className={"w-full lg:w-1/2 flex flex-col"}>
              <div className={"py-8 px-12 w-full"}>
                <h3 className="mb-2 text-palette-violet-main-dark font-bold xs:mb-0">
                  BE IN THE KNOW
                </h3>
                <h2
                  className={
                    "text-palette-purple-main font-bold xs:text-3xl xs:mb-4 xs:leading-tight"
                  }
                  style={{ fontSize: "3rem" }}
                >
                  What is GroupBuyer?
                </h2>
                <p className={"font-light text-base xs:text-justify"}>
                  Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi
                  ut turpis in velit ultricies dignissim. Morbi viverra libero
                  quis est porttitor tincidunt. Nunc in metus sit amet felis
                  feugiat tincidunt sit amet quis lectus. Vivamus tincidunt a
                  ligula a malesuada. Aenean viverra lacus nulla, sit amet
                  bibendum ipsum suscipit sit amet. Proin enim purus, mattis et
                  ex non, finibus sollicitudin orci.{" "}
                </p>
                <a className={"text-palette-purple-main text-base font-medium"}>
                  Learn More
                </a>
              </div>
              <div
                className={
                  "py-8 pl-12 pr-24 w-full flex-1 xs:pt-10 xs:px-10 xs:pb-10"
                }
                style={{ backgroundColor: "#E91AFC" }}
              >
                <h3 className="mb-2 text-palette-violet-main-dark font-bold">
                  MANAGE OPPORTUNITIES
                </h3>
                <h2
                  className={"text-white font-bold xs:text-3xl"}
                  style={{ fontSize: "2rem" }}
                >
                  Find a property
                </h2>
                <div className={"flex text-base mt-2 mb-4"}>
                  <input
                    className={
                      "rounded-sm placeholder-white py-2 px-4 mr-2 flex-1 xs:text-xs xs:px-3"
                    }
                    placeholder={"Enter development location or name"}
                    style={{ backgroundColor: "#FFFFFF5C" }}
                  ></input>
                  <button
                    className={"rounded-sm text-white text-base py-2 px-6"}
                    style={{ backgroundColor: "#4D0B98" }}
                  >
                    Search
                  </button>
                </div>
                {/* <h2 className={"text-white font-bold xs:text-3xl"} style={{fontSize:"2rem"}}>Track a specific deal</h2>
            <div className={"flex text-base mt-2 mb-4"}>
              <input className={"rounded-sm placeholder-white py-2 px-4 mr-2 flex-1"} placeholder={"Enter deal name or number"} style={{backgroundColor:'#FFFFFF5C'}}></input>
              <button className={"rounded-sm text-white text-base py-2 px-6"} style={{backgroundColor:'#4D0B98'}}>Search</button>
            </div> */}
              </div>
            </div>
          </div>
        </form>
      </section>
      <section id="why-groupbuyer">
        <div
          className={
            "flex flex-col pt-20 pb-32 px-32 xs:pt-10 xs:px-10 xs:pb-10 xs:bg-center"
          }
          style={{
            background:
              "url('/assets/images/why_groupbuyer_1.jpg') no-repeat padding-box padding-box rgba(255, 255, 255, 0)",
            backgroundSize: "cover",
            backgroundPositionX: "right",
            backgroundPositionY: "bottom"
          }}
        >
          <span
            className={
              "font-bold text-white text-6xl mb-12 text-center xs:text-4xl xs:leading-tight"
            }
          >
            Why GroupBuyer?
          </span>
          {/* <span className={"font-bold text-white text-6xl mb-12 text-center xs:text-4xl xs:leading-tight"}>Why <br className={"lg:hidden md:hidden"}/> GroupBuyer?</span> */}
          <div className={"md:flex lg:flex m-auto"} style={{maxWidth:1336}}>
            <div className={"flex-1 p-6 xs:p-1"}>
              <div
                className={
                  "flex justify-between items-center text-white leading-tight font-bold text-3xl"
                }
              >
                <p>
                  Power of <br className={"xs:hidden"} />
                  Community
                </p>
                <FontAwesomeIcon
                  icon={["fas", "plus"]}
                  size={`sm`}
                  className="xs:hidden"
                />
              </div>
              <p className={"text-base font-light text-white xs:leading-tight"}>
                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi
                ut turpis in velit ultricies dignissim. Morbi
              </p>
              <ol
                className={
                  "why-groupbuyer-ol text-base text-white font-light xs:leading-tight"
                }
              >
                <li>
                  <b className={"text-palette-purple-main font-bold text-2xl"}>
                    1.
                  </b>{" "}
                  Lorem ipsum dolor sit amet
                </li>
                <li>
                  <b className={"text-palette-purple-main font-bold text-2xl"}>
                    2.
                  </b>{" "}
                  consectetur adipiscing elit
                </li>
                <li>
                  <b className={"text-palette-purple-main font-bold text-2xl"}>
                    3.
                  </b>{" "}
                  ut turpis in velit ultricies
                </li>
                <li>
                  <b className={"text-palette-purple-main font-bold text-2xl"}>
                    4.
                  </b>{" "}
                  Morbi viverra libero quis
                </li>
              </ol>
              <div className={"text-center text-white pt-5 text-3xl"}>
                <FontAwesomeIcon
                  icon={["fas", "plus"]}
                  size={`sm`}
                  className="lg:hidden md:hidden"
                />
              </div>
            </div>

            <div className={"flex-1 p-6 xs:p-1"}>
              <div
                className={
                  "flex justify-between items-center text-white leading-tight font-bold text-3xl"
                }
              >
                <p>
                  Power of <br className={"xs:hidden"} />
                  Purchase
                </p>
                <FontAwesomeIcon
                  icon={["fas", "plus"]}
                  size={`sm`}
                  className="xs:hidden"
                />
              </div>
              <p className={"text-base font-light text-white xs:leading-tight"}>
                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi
                ut turpis in velit ultricies dignissim. Morbi
              </p>
              <ol
                className={
                  "why-groupbuyer-ol text-base text-white font-light xs:leading-tight"
                }
              >
                <li>
                  <b className={"text-palette-purple-main font-bold text-2xl"}>
                    1.
                  </b>{" "}
                  Lorem ipsum dolor sit amet
                </li>
                <li>
                  <b className={"text-palette-purple-main font-bold text-2xl"}>
                    2.
                  </b>{" "}
                  consectetur adipiscing elit
                </li>
                <li>
                  <b className={"text-palette-purple-main font-bold text-2xl"}>
                    3.
                  </b>{" "}
                  ut turpis in velit ultricies
                </li>
                <li>
                  <b className={"text-palette-purple-main font-bold text-2xl"}>
                    4.
                  </b>{" "}
                  Morbi viverra libero quis
                </li>
              </ol>
              <div className={"text-center text-white pt-5 text-3xl"}>
                <FontAwesomeIcon
                  icon={["fas", "plus"]}
                  size={`sm`}
                  className="lg:hidden md:hidden"
                />
              </div>
            </div>

            <div className={"flex-1 p-6 xs:p-1"}>
              <div
                className={
                  "flex justify-between items-center text-white leading-tight font-bold text-3xl"
                }
              >
                <p>
                  Power of <br className={"xs:hidden"} />
                  Knowledge
                </p>
                <FontAwesomeIcon
                  icon={["fas", "equals"]}
                  size={`sm`}
                  className="xs:hidden"
                />
              </div>
              <p className={"text-base font-light text-white xs:leading-tight"}>
                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi
                ut turpis in velit ultricies dignissim. Morbi
              </p>
              <ol
                className={
                  "why-groupbuyer-ol text-base text-white font-light xs:leading-tight"
                }
              >
                <li>
                  <b className={"text-palette-purple-main font-bold text-2xl"}>
                    1.
                  </b>{" "}
                  Lorem ipsum dolor sit amet
                </li>
                <li>
                  <b className={"text-palette-purple-main font-bold text-2xl"}>
                    2.
                  </b>{" "}
                  consectetur adipiscing elit
                </li>
                <li>
                  <b className={"text-palette-purple-main font-bold text-2xl"}>
                    3.
                  </b>{" "}
                  ut turpis in velit ultricies
                </li>
                <li>
                  <b className={"text-palette-purple-main font-bold text-2xl"}>
                    4.
                  </b>{" "}
                  Morbi viverra libero quis
                </li>
              </ol>
              <div className={"text-center text-white pt-5 text-3xl"}>
                <FontAwesomeIcon
                  icon={["fas", "equals"]}
                  size={`sm`}
                  className="lg:hidden md:hidden"
                />
              </div>
            </div>
            <div className={"flex-1 p-6 xs:p-1"}>
              <div
                className={
                  "flex justify-between items-center text-palette-purple-main leading-tight font-bold text-3xl"
                }
              >
                <p>
                  The One Stop
                  <br className={"xs:hidden"} />
                  Shop Solution
                </p>
              </div>
              <p className={"text-base font-light text-white xs:leading-tight"}>
                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi
                ut turpis in velit ultricies dignissim. Morbi
              </p>
              <ol
                className={
                  "why-groupbuyer-ol text-base text-white font-light xs:leading-tight"
                }
              >
                <li>
                  <b className={"text-palette-purple-main font-bold text-2xl"}>
                    1.
                  </b>{" "}
                  Lorem ipsum dolor sit amet
                </li>
                <li>
                  <b className={"text-palette-purple-main font-bold text-2xl"}>
                    2.
                  </b>{" "}
                  consectetur adipiscing elit
                </li>
                <li>
                  <b className={"text-palette-purple-main font-bold text-2xl"}>
                    3.
                  </b>{" "}
                  ut turpis in velit ultricies
                </li>
                <li>
                  <b className={"text-palette-purple-main font-bold text-2xl"}>
                    4.
                  </b>{" "}
                  Morbi viverra libero quis
                </li>
              </ol>
            </div>
          </div>
          <div className={"flex justify-center mt-16  xs:hidden"}>
            <div
              className={
                "flex flex-col items-center py-12 px-24 rounded-md xs:pt-10 xs:px-10 xs:pb-10"
              }
              style={{
                background:
                  "linear-gradient(45deg, rgb(255 255 255 / 30%), rgb(255 255 255 / 30%))"
              }}
            >
              <p className={"text-4xl text-white"}>Join us for free</p>
              <p
                className={
                  "text-base text-white max-w-lg text-center font-light"
                }
              >
                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi
                ut turpis in velit ultricies dignissim. Morbi viverra libero
                quis est porttitor tincidunt. Nunc in metus sit amet felis
                feugiat tincidunt sit amet quis lectus. Vivamus tincidunt a
                ligula a malesuada. Aenean viverra lacus nulla, sit amet
                bibendum ipsum suscipit sit{" "}
              </p>
              <img src={triple_chevron_down_purple} className={"w-16 my-4"} />

              <p className={"text-4xl text-white"}>Access to deals</p>
              <p
                className={
                  "text-base text-white max-w-lg text-center font-light"
                }
              >
                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi
                ut turpis in velit ultricies dignissim. Morbi viverra libero
                quis est porttitor tincidunt. Nunc in metus sit amet felis
                feugiat tincidunt sit amet quis lectus. Vivamus tincidunt a
                ligula a malesuada. Aenean viverra lacus nulla, sit amet
                bibendum ipsum suscipit sit{" "}
              </p>
              <img src={triple_chevron_down_purple} className={"w-16 my-4"} />

              <p className={"text-4xl text-white"}>Secured your deals</p>
              {/* <p
                className={
                  "text-base text-white max-w-lg text-center font-light mb-12"
                }
              >
                Register your interest for a deal for only $1,000: it is easy,
                completely secure! GroupBuyer will support you on each step of
                the acquisition process to make it simple and stress-free.
              </p>

              <button
                className={
                  "rounded-sm text-white text-base font-bold py-2 text-center mb-6 w-full max-w-xs"
                }
                style={{ backgroundColor: "#E91AFC" }}
              >
                Find your next deal
              </button>
              <button
                onClick={handleOpenModal}
                className={
                  "rounded-sm text-palette-purple-main border text-base font-light py-2 text-center w-full max-w-xs mb-8"
                }
                style={{ borderColor: "#E91AFC" }}
              >

                Join now for free
              </button> */}
              <p className={"text-base text-white max-w-lg text-center font-light mb-6"}>Register your interest for a deal for only $1,000: it is easy, completely secure! GroupBuyer will support you on each step of the acquisition process to make it simple and stress-free.</p>
              <div className={"flex justify-center items-center mb-8"}>
                <img
                  src={commonwealth_bank}
                  style={{width:"85%"}}
                  className={""}
                />
                {/* <p className={"text-1xl text-white pt-5 text-right"}>
                  Payment gateway powered by
                </p> */}
              </div>
              <button className={"rounded-sm text-white text-base font-bold py-2 text-center mb-6 w-full max-w-xs"} style={{ backgroundColor: '#E91AFC' }}>Find your next deal</button>
              <button  onClick={handleOpenModal} className={"rounded-sm text-palette-purple-main border text-base font-light py-2 text-center w-full max-w-xs mb-8"} style={{ borderColor: '#E91AFC' }}>Join now for free</button>
            </div>
          </div>
        </div>
      </section>
      <section id="join-us">
        <div
          className={
            "flex flex-col items-center w-full xs:pt-10 xs:px-10 xs:pb-10 lg:hidden md:hidden"
          }
          style={{ backgroundColor: "#4D0B98" }}
        >
          <p className={"text-3xl text-white"}>Join us for free</p>
          <p className={"text-base text-white max-w-lg text-center font-light"}>
            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi ut
            turpis in velit ultricies dignissim. Morbi viverra libero quis est
            porttitor tincidunt. Nunc in metus sit amet felis feugiat tincidunt
            sit amet quis lectus. Vivamus tincidunt a ligula a malesuada. Aenean
            viverra lacus nulla, sit amet bibendum ipsum suscipit sit{" "}
          </p>
          <img src={triple_chevron_down_purple} className={"w-16 my-4"} />

          <p className={"text-3xl text-white"}>Access to deals</p>
          <p className={"text-base text-white max-w-lg text-center font-light"}>
            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi ut
            turpis in velit ultricies dignissim. Morbi viverra libero quis est
            porttitor tincidunt. Nunc in metus sit amet felis feugiat tincidunt
            sit amet quis lectus. Vivamus tincidunt a ligula a malesuada. Aenean
            viverra lacus nulla, sit amet bibendum ipsum suscipit sit{" "}
          </p>
          <img src={triple_chevron_down_purple} className={"w-16 my-4"} />

          <p className={"text-3xl text-white"}>Secured your deals</p>
          <p
            className={
              "text-base text-white max-w-lg text-center font-light mb-2"
            }
          >
            Register your interest for a deal for only $1,000: it is easy,
            completely secure! GroupBuyer will support you on each step of the
            acquisition process to make it simple and stress-free.
          </p>
          <div className={"flex justify-center item-center px-8 my-8"}>
            <img
              src={commonwealth_bank}
              className={"w-full"}
            />
            {/* <p className={"text-1xl text-white pt-5 text-right"}>
              Payment gateway powered by
            </p> */}
          </div>
          {/* <button
            className={
              "rounded-sm text-white text-base font-bold py-2 text-center mb-6 w-full max-w-xs"
            }
            style={{ backgroundColor: "#E91AFC" }}
          >
            Find your next deal
          </button>
          <button
            className={
              "rounded-sm text-white border text-base font-light py-2 text-center w-full max-w-xs mb-8"
            }
            style={{ borderColor: "white" }}
          >
            Join now for free
          </button> */}
          <button className={"rounded-sm text-white text-base font-bold py-2 text-center mb-6 w-full max-w-xs"} style={{ backgroundColor: '#E91AFC' }}>Find your next deal</button>
          <button className={"rounded-sm text-white border text-base font-light py-2 text-center w-full max-w-xs mb-8"} style={{ borderColor: 'white' }}><a href="#be-in-the-loop">Join now for free</a></button>
        </div>
      </section>
      <CurrentPopularDeals />
    {/* <button onClick={handleOpenModal}>Open Modal</button> */}
    <Modal
      aria-labelledby="simple-modal-title"
      aria-describedby="simple-modal-description"
      open={modal}
      // centered
      style={{display:'flex',alignItems:'center',justifyContent:'center'}}
      onClose={handleCloseModal}
    >
      <div style={{zoom: '0.85'}}>
        <RegistrationForm
          containerBgColor="#fff"
          containerRounded={true}
          displayCloseButton={true}
          rightPadded={false}
          subHeader="JOIN US FOR FREE"
          textColorTheme="dark"
          inputBgColor="#dadada5c"
          headerTop="Create"
          headerBottom="account"
          handleCloseModal={handleCloseModal}
          handleLoginModal={handleLoginModal}
          isPopup={true}
        />
      </div>
    </Modal>

    <Modal
      aria-labelledby="simple-modal-title"
      aria-describedby="simple-modal-description"
      open={modalLogin}
      style={{display:'flex',alignItems:'center',justifyContent:'center'}}
      // onClose={handleCloseModal}
    >
      <div style={{zoom: '0.85'}}>
      <LoginForm
        containerBgColor="#fff"
        containerRounded={true}
        displayCloseButton={true}
        rightPadded={false}
        subHeader="JOIN US FOR FREE"
        textColorTheme="dark"
        inputBgColor="#dadada5c"
        headerTop="Login to"
        headerBottom="account"
        handleCloseModal={handleCloseModal}
        hangleSignUpModal={handleLoginModalToSignUp}
      />
      </div>
    </Modal>
    </UserLayout>
  );
};

export default NewHome;
