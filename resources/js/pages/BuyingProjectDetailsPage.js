import React, { useState, useEffect } from "react";
import { isEmpty } from "lodash";

import UserLayout from "~/layouts/users/UserLayout";
import UserGlobal from "~/states/userGlobal";

import {
  BuyingProgress,
  AgentDetailsSection,
  SocialMediaShareModal,
  ResourcesComponent,
  Deposit,
  EstimatedCompletionDate,
  ReadyForOccupancy,
  BookInspection
} from "~/components";
import { isLoggedIn } from "~/services/auth";

import AdContainerComponent from "../components/adComponents/AdContainerComponent";

import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { isMobile } from "react-device-detect";
import CurrencyFormat from "react-currency-format";
import DateCountdown from "react-date-countdown-timer";
import Slider from "react-slick";

import { Tooltip } from "~/components/_base";
import { discountedPriceRange } from "../helpers/pricingHelper/discountHelper";
import { getDeveloperById } from "../data/developersData/developersData";

const BuyingProjectDetailsPage = ({ dealId, isPreview }) => {
  const [userState, userAction] = UserGlobal();
  const [loading, setLoading] = useState(true);
  const [project, setProject] = useState();
  const [timeleft, setTimeLeft] = useState("");
  const [preview, setPreview] = useState(isPreview === "true" ? true : false);
  const [developerLogo, setDeveloperLogo] = useState("");
  const [developerURL, setDeveloperURl] = useState("");
  const [isProjectLand, setIsProjectLand] = useState(false);

  const [showBookingModal, setShowBookingModal] = useState(false);

  const toogleShowBookingModal = () => {
    setShowBookingModal(!showBookingModal);
  };

  const getDeal = async () => {
    let deal = await axios.get(`/api/deal/${dealId}`);

    if (deal.data.sub_property_id == 4) {
      setIsProjectLand(true);
    }

    if (deal.data && deal.data.expires_at) {
      let exp = JSON.parse(deal.data.expires_at);
      setTimeLeft(exp.time_limit);
    }

    getDeveloperById(deal.data.user_id).then(res => {
      if (res.data.company_logo_path) {
        setDeveloperLogo(res.data.company_logo_path);
        setDeveloperURl(res.data.company_url);
      }
    });

    if (isLoggedIn() && !isEmpty(userState.user)) {
      const { data } = await axios.get(`/api/wish-list/${dealId}`, {
        headers: {
          Authorization: "Bearer " + isLoggedIn()
        }
      });

      let newProject = Object.assign({}, deal.data);

      if (data && !isEmpty(newProject)) {
        data.map(d => {
          newProject.properties.map(p => {
            if (p.id == d.property_id) {
              p.hasWishlist = true;
              p.wishListId = d.id;
            }
          });
        });
      }
      setProject(newProject);
    } else {
      setProject(deal.data);
    }
    // setProject(deal.data);
    setLoading(false);
  };

  useEffect(() => {
    // if (!isLoggedIn()) {
    //   window.location = `/weekly-deals`;
    // }
    getDeal();
  }, [userState.user]);

  const handleWishlist = async property => {
    if (!isLoggedIn()) {
      userAction.setState({ showSignIn: true });
      return;
    }

    let formData = new FormData();

    formData.append("property_id", property.id);
    formData.append("deal_id", dealId);

    try {
      if (!property.hasWishlist) {
        await axios.post("/api/wish-list", formData, {
          headers: {
            Authorization: "Bearer " + isLoggedIn()
          }
        });
      } else {
        await axios.delete(`/api/wish-list/${property.wishListId}`, {
          headers: {
            Authorization: "Bearer " + isLoggedIn()
          }
        });
      }

      getDeal();
    } catch (error) {}
  };

  const renderApartments = () => {
    return (
      !loading &&
      Object.keys(project).length !== 0 &&
      project.properties.map((property, key) => {
        // let images = property.featured_images.length > 0 ?  JSON.parse(property.featured_images) : [];
        return (
          <li key={key} className={`lg:px-3 lg:w-4/12 mb-6 relative w-full`}>
            <div
              className={`relative bg-white rounded-md duration-300 transform transition-all ${
                !property.is_secured ? "lg:hover:scale-105" : ""
              }`}
              style={{ boxShadow: "0 5px 28px 4px rgba(0,0,0,0.1)" }}
            >
              {property.is_secured && (
                <div
                  className={`rounded-md absolute inset-0 z-20 bg-gray-900 bg-opacity-75 flex items-center justify-center`}
                >
                  <div
                    className={`uppercase font-bold text-4xl text-white flex items-center`}
                  >
                    <span
                      className={`w-12 h-12 rounded-full mr-4 flex items-center justify-center`}
                      style={{ background: "#89b036" }}
                    >
                      <svg
                        className={`feather-icon h-8 w-8`}
                        style={{ strokeWidth: 4 }}
                      >
                        <use
                          xlinkHref={`/assets/svg/feather-sprite.svg#check`}
                        />
                      </svg>
                    </span>
                    Deal Secured
                  </div>
                </div>
              )}
              <a
                href={`/weekly-deals/${dealId}/apartment/${
                  property.id
                }/${isPreview || false}`}
              >
                <div
                  className={`flex items-center justify-between lg:px-4 px-2 py-2`}
                >
                  <span className={`text-2xl font-bold`}>
                    {isProjectLand ? "Lot No." : " Apartment"}{" "}
                    {property.unit_name}
                  </span>
                  {!isProjectLand && (
                    <div
                      className={`flex font-semibold items-center lg:font-bold my-3`}
                    >
                      <span className={`mr-3 text-lg`}>
                        {property.no_of_bedrooms}{" "}
                        <FontAwesomeIcon icon={["fas", "bed"]} />
                      </span>
                      <span className={`mr-3 text-lg`}>
                        {property.no_of_bathrooms}{" "}
                        <FontAwesomeIcon icon={["fas", "bath"]} />
                      </span>
                      <span className={`text-lg`}>
                        {property.no_of_garages}{" "}
                        <FontAwesomeIcon icon={["fas", "car"]} />
                      </span>
                    </div>
                  )}
                </div>
                <div className={`relative`}>
                  <Slider {...apartmentSettings}>
                    {!loading &&
                      property.featured_images.map((image, key) => {
                        return (
                          <img
                            key={key}
                            src={image}
                            className={`object-cover carousel-apartment`}
                          />
                        );
                      })}
                  </Slider>
                </div>
              </a>
              <div className={`p-4`}>
                <div className={`flex items-center justify-between`}>
                  <div
                    className={`lg:text-3xl text-2xl font-bold py-1 px-6 bg-red-700 text-white -ml-4`}
                  >
                    Save{" "}
                    <CurrencyFormat
                      value={(property.price * project.discount) / 100}
                      displayType={`text`}
                      thousandSeparator={true}
                      prefix={`$`}
                    />
                  </div>
                  <div
                    className={`text-base flex`}
                    style={{ position: "relative" }}
                  >
                    {!preview && (
                      <Tooltip title={`Share`} placement={`top`}>
                        <svg
                          className={`feather-icon h-5 w-5 cursor-pointer mr-3`}
                          onClick={() =>
                            userAction.setState({ showSocialMedia: true })
                          }
                        >
                          <use
                            xlinkHref={`/assets/svg/feather-sprite.svg#share`}
                          />
                        </svg>
                      </Tooltip>
                    )}

                    {!preview && (
                      <Tooltip
                        title={`${
                          property.hasWishlist
                            ? "Already on wishlist"
                            : "Add to wishlist"
                        }`}
                        placement={`top`}
                      >
                        <svg
                          className={`${
                            property.hasWishlist ? "text-red-600" : "text-black"
                          } feather-icon h-5 w-5 cursor-pointer`}
                          onClick={() => handleWishlist(property)}
                        >
                          <use
                            xlinkHref={`/assets/svg/feather-sprite.svg#heart`}
                            className={`${
                              property.hasWishlist ? "fill-current" : null
                            }`}
                          />
                        </svg>
                      </Tooltip>
                    )}
                  </div>
                </div>

                <div className={`flex items-center justify-between mt-3`}>
                  <span
                    className={`opacity-75 line-through font-bold text-xl lg:mr-6`}
                  >
                    WAS{" "}
                    <CurrencyFormat
                      value={property.price || 0}
                      displayType={`text`}
                      thousandSeparator={true}
                      prefix={`$`}
                    />
                  </span>
                  {!isProjectLand && (
                    <span className={`opacity-75`}>
                      Internal size: {property.internal_size}sqm
                    </span>
                  )}
                </div>

                <div className={`flex items-center justify-between`}>
                  <span
                    className={`text-red-700 font-bold text-xl leading-tight`}
                  >
                    NOW{" "}
                    <CurrencyFormat
                      value={
                        property.price -
                        property.price * (project.discount / 100)
                      }
                      displayType={`text`}
                      thousandSeparator={true}
                      prefix={`$`}
                    />
                  </span>
                  {!isProjectLand && (
                    <span className={`text-palette-blue-light font-bold`}>
                      <a
                        href={property.floor_plan.file}
                        className={`flex items-center ${
                          property.floor_plan.file ? "" : "cursor-not-allowed"
                        }`}
                        target={`_blank`}
                      >
                        <img
                          src={`/assets/svg/floor-plan.svg`}
                          className={`mr-2`}
                        />
                        Floor plan
                      </a>
                    </span>
                  )}
                </div>
              </div>
            </div>
          </li>
        );
      })
    );
  };

  const handleBackToSearch = () => {
    localStorage.setItem("__use_projects_filters", true);
    if (isPreview) {
      window.location = "/admin/projects";
    } else {
      window.location = `/weekly-deals/${isPreview}`;
    }
  };

  return (
    <UserLayout>
      <section
        className={`bg-palette-blue-dark`}
        style={{ marginTop: -130, paddingTop: 160 }}
      >
        <section className={`relative bg-white`}>
          {!preview && <BuyingProgress current={2} isPreview={preview} />}

          {preview && (
            <div
              className={`w-full bg-palette-purple text-white font-bold text-center p-4 text-xl`}
            >
              You are viewing a preview of the project.
            </div>
          )}

          {!isMobile && !loading && (
            <section className={`lg:mt-0 mt-16 relative z-20`}>
              <Slider {...featuredSettings}>
                {!loading &&
                  project.featured_images.length > 0 &&
                  project.featured_images.map((image, key) => {
                    return (
                      <img
                        key={key}
                        src={image}
                        className={`object-cover carousel-featured`}
                      />
                    );
                  })}
              </Slider>
            </section>
          )}

          {!isMobile && !loading && project.discount && (
            <section
              className={`mt-8 lg:-mt-8 relative text-white z-20 inline-block w-full lg:w-auto`}
            >
              <div className={`flex lg:inline-flex shadow-lg`}>
                <div
                  className={`w-6/12 lg:w-auto bg-red-700 flex items-center lg:pl-12 lg:pr-24 px-3 py-1`}
                  style={{
                    clipPath: "polygon(0 0, 100% 0, 80% 100%, 0 100%)"
                  }}
                >
                  <span className={`font-bold lg:text-4xl mr-4 text-2xl`}>
                    {`${project.discount}%`}
                  </span>
                  <span className={`font-bold lg:text-2xl text-lg uppercase`}>
                    Discount
                  </span>
                </div>
                <div
                  className={`w-6/12 lg:w-auto lg:-ml-20 bg-red-800 flex items-center lg:pl-20 px-3 py-1`}
                  style={{
                    clipPath: "polygon(18% 0, 100% 0, 100% 100%, 0 100%)"
                  }}
                >
                  <div
                    className={`font-bold leading-none lg:mr-4 mr-1 text-center uppercasee`}
                  >
                    <div className={`text-sm lg:text-2xl`}>SAVE</div>
                    <div className={`text-sm lg:text-2xl`}>UP TO</div>
                  </div>
                  <div className={`ml-2`}>
                    <CurrencyFormat
                      value={
                        (project.properties.length &&
                          Math.max.apply(
                            Math,
                            project.properties.map(max => {
                              return (max.price * project.discount) / 100;
                            })
                          )) ||
                        0
                      }
                      displayType={`text`}
                      thousandSeparator={true}
                      prefix={`$`}
                      className={`font-bold lg:text-4xl text-lg`}
                    />
                  </div>
                </div>
              </div>
            </section>
          )}

          {isMobile && !loading && project.discount && (
            <section
              className={`mt-8 lg:-mt-8 relative text-white z-20 inline-block w-full lg:w-auto`}
            >
              <div className={`flex lg:inline-flex shadow-lg`}>
                <div
                  id={`discountBanner`}
                  className={`lg:-mr-20 items-center sm:justify-center -mr-16 sm:-mr-32 md:justify-center bg-red-700 flex lg:pl-12 lg:pr-24 lg:w-auto w-full xs:pl-8`}
                  style={{
                    clipPath: "polygon(0 0, 100% 0, 80% 100%, 0 100%)"
                  }}
                >
                  <span className={`font-bold lg:text-4xl mr-4 text-base`}>
                    {`${project.discount}%`}
                  </span>
                  <span className={`font-bold lg:text-2xl text-base uppercase`}>
                    Discount
                  </span>
                </div>

                <div
                  className={`bg-red-800 flex items-center lg:-ml-20 lg:pl-20 lg:w-auto pl-12 px-3 py-1 w-full justify-center`}
                  style={{
                    clipPath: "polygon(18% 0, 100% 0, 100% 100%, 0 100%)"
                  }}
                >
                  <div
                    className={`font-bold leading-none lg:mr-4 mr-1 text-center uppercasee`}
                  >
                    <div className={`text-sm lg:text-2xl`}>SAVE</div>
                    <div className={`text-sm lg:text-2xl`}>UP TO</div>
                  </div>
                  <div className={`ml-2`}>
                    <CurrencyFormat
                      value={
                        (project.properties.length &&
                          Math.max.apply(
                            Math,
                            project.properties.map(max => {
                              return (max.price * project.discount) / 100;
                            })
                          )) ||
                        0
                      }
                      displayType={`text`}
                      thousandSeparator={true}
                      prefix={`$`}
                      className={`font-bold lg:text-4xl text-lg`}
                    />
                  </div>
                </div>
              </div>
            </section>
          )}

          <section
            className={`lg:px-10 mt-6 pb-16 lg:pb-24 mx-auto relative`}
            style={{ maxWidth: 1600 }}
          >
             
            <section
              className={`flex flex-col lg:flex-row`}
              style={{ minHeight: 440 }}
            >
              <div className={`lg:w-3/12 px-4 lg:px-0`}>
                {preview && (
                  <div className={`relative`}>
                    <span
                      className={`bg-white cursor-pointer duration-300 font-bold hover:text-palette-blue inline-flex items-center pr-3 relative text-palette-blue-light text-sm transition-all z-10`}
                      onClick={handleBackToSearch}
                    >
                      <svg className={`feather-icon h-4 w-4 mr-1 inline`}>
                        <use
                          xlinkHref={`/assets/svg/feather-sprite.svg#chevron-left`}
                        />
                      </svg>
                      {isPreview
                        ? "Back to Admin Projects"
                        : "Back to search results"}
                    </span>
                    <div
                      className={`absolute border-gray-200 border-t w-full`}
                      style={{ top: 10 }}
                    />
                  </div>
                )}

                <h1
                  className={`font-bold lg:mb-2 lg:text-5xl mb-3 mt-2 text-4xl leading-none`}
                >
                  {!loading && project.name}
                </h1>

                <div className={`lg:font-bold text-base`}>
                  {!loading && project.address && (
                    <>
                      {project.address.line_1
                        ? project.address.line_1 + ", "
                        : ""}
                      {project.address.line_2
                        ? project.address.line_2 + ", "
                        : ""}
                      {project.address.suburbs
                        ? project.address.suburbs + ", "
                        : ""}
                      {project.address.suburb
                        ? project.address.suburb + ", "
                        : ""}
                      {project.address.state ? project.address.state + " " : ""}
                      {project.address.postal ? project.address.postal : ""}
                    </>
                  )}
                </div>

                {!loading && !isProjectLand && (
                  <div
                    className={`px-4 py-1 shadow-lg mt-4 bg-red-700 font-bold text-white rounded-lg`}
                    style={{ width: isMobile ? "fit-content" : "100%" }}
                  >
                    <span className={`text-white`}>Time left: </span>

                    {(!loading && timeleft && (
                      <DateCountdown
                        dateTo={timeleft ? timeleft : project.created_at}
                        locales={[
                          "Year, ",
                          "Month, ",
                          "Day, ",
                          "hr, ",
                          "min, ",
                          "sec"
                        ]}
                        locales_plural={[
                          "Years, ",
                          "Months, ",
                          "Days, ",
                          "hrs, ",
                          "min, ",
                          "sec"
                        ]}
                      />
                    )) ||
                      "-"}
                  </div>
                )}

                {!isMobile && (
                  <div className={`flex`}>
                    {!isEmpty(project) &&
                      !isProjectLand &&
                      !project.is_completed && (
                        <EstimatedCompletionDate
                          loading={loading}
                          project={project}
                        />
                      )}

                    {!isEmpty(project) &&
                      !isProjectLand &&
                      project.is_completed && <ReadyForOccupancy />}

                    {!isEmpty(project) && project.deposit > 0 && (
                      <Deposit deposit={project.deposit} />
                    )}
                  </div>
                )}

                {isMobile && (
                  <div className={`flex xs:flex-col`}>
                    {!isEmpty(project) &&
                      isProjectLand &&
                      !project.is_completed && (
                        <EstimatedCompletionDate
                          loading={loading}
                          project={project}
                        />
                      )}

                    {!isEmpty(project) && project.is_completed && (
                      <ReadyForOccupancy />
                    )}

                    {!isEmpty(project) && project.deposit > 0 && (
                      <Deposit deposit={project.deposit} />
                    )}
                  </div>
                )}

                <div className={`lg:mt-6`}>
                  <div className={`flex flex-col items-center lg:block`}>
                    <span
                      className={`text-gray-600 font-bold text-base w-full`}
                    >
                      DEVELOPER
                    </span>
                    {developerURL && (
                      <a href={developerURL} target="_blank">
                        <img className="my-3" src={developerLogo} />
                      </a>
                    )}
                    {!developerURL && (
                      <img className="my-3" src={developerLogo} />
                    )}
                  </div>
                  {!loading && (
                    <div className={`my-4 p-1 rounded text-center`}>
                      <button
                        disabled={isPreview === "true" ? true : false}
                        onClick={() => setShowBookingModal(true)}
                        className={`rounded text-base font-bold p-2 ${
                          !isPreview ? "text-white" : "text-gray-400"
                        } w-1/2 text-center hover:bg-gray-900 bg-gray-700 transition-all duration-300`}
                      >
                        Book Inspection
                      </button>
                    </div>
                  )}
 
                </div>
              </div>

              {isMobile && (
                <section className={`lg:mt-0 mt-8 relative z-20`}>
                  <Slider {...featuredSettings}>
                    {!loading &&
                      project.featured_images.length > 0 &&
                      project.featured_images.map((image, key) => {
                        return (
                          <div key={key} className={`px-1`}>
                            <img
                              src={image}
                              className={`object-cover carousel-featured rounded-lg`}
                            />
                          </div>
                        );
                      })}
                  </Slider>
                </section>
              )}

              <div className={`px-6 mt-10 lg:mt-0 lg:px-16 lg:w-7/12`}>
                <div>
                  {!loading && !isEmpty(project) && (
                    <>
                    
                      <div
                        className={`border-b-2 pb-4 mb-4 flex items-center justify-between`}
                      >
                        <div
                          className={`font-bold text-xl lg:text-3xl opacity-50`}
                        >
                          <span className="mr-2">From</span>
                          <CurrencyFormat
                            value={
                              project && discountedPriceRange(project, false)
                            }
                            displayType={`text`}
                            thousandSeparator={true}
                            prefix={`$`}
                          />
                          <span> - </span>
                          <CurrencyFormat
                            value={
                              project && discountedPriceRange(project, true)
                            }
                            displayType={`text`}
                            thousandSeparator={true}
                            prefix={`$`}
                          />
                        </div>
                        {!isMobile && !preview && (
                          <div className={`text-base flex`}>
                            <Tooltip title={`Share`} placement={`top`}>
                              <svg
                                className={`feather-icon h-5 w-5 cursor-pointer mr-5`}
                                onClick={() =>
                                  userAction.setState({ showSocialMedia: true })
                                }
                              >
                                <use
                                  xlinkHref={`/assets/svg/feather-sprite.svg#share`}
                                />
                              </svg>
                            </Tooltip>
                            
                          </div>
                        )}
                      </div>
                    </>
                  )}
                </div>

                {!loading && (
                  <>
                    <div
                      className={`text-base`}
                      dangerouslySetInnerHTML={{ __html: project.description }}
                    />
                    {!isProjectLand && (
                      <p className="italic text-sm mt-10">
                        *Property images are for illustration purposes only and
                        may not be specific to your particular property.
                        Property sizes and outgoing costs are estimates only.
                      </p>
                    )}
                  </>
                )}
                
              </div>

              {!isMobile && (
                <div className={`lg:w-2/12`}>
                  <div
                    className={`text-palette-gray uppercase font-bold text-base mb-4`}
                  >
                    Resources
                  </div>
                  <ul>
                    {!loading && <ResourcesComponent project={project} />}

                    <span
                      className={`cursor-pointer flex items-center bg-gray-200 block font-bold px-3 py-1 leading-relaxed text-base rounded text-palette-blue-light mt-3`}
                      onClick={() =>
                        userAction.setState({ showSocialMedia: true })
                      }
                    >
                      Share this project
                      <svg className={`feather-icon h-4 w-4 ml-2`}>
                        <use
                          xlinkHref={`/assets/svg/feather-sprite.svg#share`}
                        />
                      </svg>
                    </span>
                  </ul>
                </div>
              )}
            </section>

            <section className={`border-t-2 lg:mt-8 lg:px-0 mt-4 pt-4 px-6`}>
              

              <div className={`text-3xl font-bold mt-6`}>
                Available properties
              </div>

              <ul className={`flex flex-wrap mt-6`}>{renderApartments()}</ul>

              {isMobile && (
                <div className={`px-16 mt-6`}>
                  <div
                    className={`text-palette-gray uppercase font-bold text-center text-base mb-4`}
                  >
                    Resources
                  </div>
                  <ul>
                    {!isEmpty(project) && project.resources.length > 0 && (
                      <ResourcesComponent project={project} />
                    )}
                    <div>
                      <span
                        className={`${
                          isMobile ? "justify-center" : ""
                        } mt-3 cursor-pointer flex items-center bg-gray-200 block font-bold px-3 py-1 leading-relaxed text-base rounded text-palette-blue-light`}
                        onClick={() =>
                          userAction.setState({ showSocialMedia: true })
                        }
                      >
                        Share this project
                        <svg className={`feather-icon h-4 w-4 ml-2`}>
                          <use
                            xlinkHref={`/assets/svg/feather-sprite.svg#share`}
                          />
                        </svg>
                      </span>
                    </div>
                  </ul>
                </div>
              )}
 
            </section>
          </section>
          {!loading && (
            <AgentDetailsSection
              showAdd={true}
              project={project}
              isProjectLand={isProjectLand}
            />
          )}
          {!loading && (
            <BookInspection
              show={showBookingModal}
              toogleShowBookingModal={toogleShowBookingModal}
              dealId={dealId}
            />
          )}
          <SocialMediaShareModal />
        </section>
      </section>
    </UserLayout>
  );
};

const featuredSettings = {
  dots: true,
  arrows: false,
  infinite: true,
  autoplay: true,
  autoplaySpeed: 5000,
  slidesToShow: 3,
  responsive: [
    {
      breakpoint: 1024,
      settings: {
        slidesToShow: 3,
        slidesToScroll: 3,
        infinite: true,
        dots: true
      }
    },
    {
      breakpoint: 600,
      settings: {
        slidesToShow: 2,
        slidesToScroll: 2
      }
    },
    {
      breakpoint: 480,
      settings: {
        centerMode: true,
        slidesToShow: 1,
        slidesToScroll: 1
      }
    }
  ]
};

const apartmentSettings = {
  dots: false,
  arrows: false,
  infinite: true,
  autoplay: true,
  autoplaySpeed: 5000,
  slidesToShow: 1,
  responsive: [
    {
      breakpoint: 1024,
      settings: {
        slidesToShow: 3,
        slidesToScroll: 3,
        infinite: true,
        dots: true
      }
    },
    {
      breakpoint: 600,
      settings: {
        slidesToShow: 2,
        slidesToScroll: 2
      }
    },
    {
      breakpoint: 480,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1
      }
    }
    // You can unslick at a given breakpoint now by adding:
    // settings: "unslick"
    // instead of a settings object
  ]
};

export default BuyingProjectDetailsPage;
