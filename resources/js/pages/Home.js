import React, { useEffect, useState } from "react";

import UserLayout from "~/layouts/users/UserLayout";
import UserGlobal from "~/states/userGlobal";
import { isLoggedIn } from "~/services/auth";
import { Button, Modal } from "~/components/_base";
import { PlayButton, MemberCount, Message } from "~/components";

import { isMobile } from "react-device-detect";
import CurrencyFormat from "react-currency-format";
import Slider from "react-slick";
import ReactPlayer from "react-player";

import * as deviceSizes from "../helpers/deviceSizeHelper";

import { stateAbbreviation } from "../helpers/stateAbbreviation";

import { getAverageSavings } from '../data/dealsData/dealsData';

import { Helmet } from "react-helmet";
import { metaHelper } from "../helpers/metaHelper/metaHelper";


const Home = () => {
  const [userState, userAction] = UserGlobal();
  const [showAbout, setShowAbout] = useState(false);
  const [showWork, setShowWork] = useState(false);

  const handleClose = () => {
    setShowAbout(false);
    setShowWork(false);
  };

  useEffect(() => {
    averageSavings();
  }, []);

  const handleScrollBottom = () => {
    window.scrollTo({ top: document.body.scrollHeight, behavior: "smooth" });
  };

  
  const averageSavings = () => {
    getAverageSavings().then(res => {
      userAction.setState({ aveSavings: res.data });
    });
  };

  const handleSelectProject = url => {
    // if (!isLoggedIn()) {
    //   userAction.setState({ showSignIn: true });
    //   return;
    // }
    window.location = url;
  };

  const buyerBenefits = () => {
    let array = [
      "Biggest discounts online",
      "Australia's best projects",
      "Top-shelf developers",
      "Faster and easier buying process"
    ];

    return array.map((item, i) => {
      return (
        <li key={i} className={`flex flex-1 items-center mb-3`}>
          <svg
            className={`feather-icon h-6 w-6 mr-2`}
            style={{ color: "#ffffff" }}
          >
            <use xlinkHref={`/assets/svg/feather-sprite.svg#check`} />
          </svg>
          <span className={`flex-1 text-left`}>{item}</span>
        </li>
      );
    });
  };

  const listYourProject = () => {
    let array = [
      "Upload properties for free",
      "No advertising or hidden costs",
      "Sell your properties around the clock",
      "Log in to see real-time sale activity",
      "Online support team"
    ];

    return array.map((item, i) => {
      return (
        <li key={i} className={`flex items-center`}>
          <svg
            className={`feather-icon h-6 w-6 mr-2`}
            style={{ color: "#ffffff" }}
          >
            <use xlinkHref={`/assets/svg/feather-sprite.svg#check`} />
          </svg>
          {item}
        </li>
      );
    });
  };

  const renderFeatured = () => {
    let slides = userState.featuredDeals && userState.featuredDeals.length;
    let newFeatured = [...userState.featuredDeals];

    if (!isMobile) {
      if (slides > 1 && slides < 4) {
        userState.featuredDeals.map(featured => {
          newFeatured.push(featured);
        });
      }

      if (slides === 1) {
        userState.featuredDeals.map(featured => {
          newFeatured.push(featured);
          newFeatured.push(featured);
          newFeatured.push(featured);
        });
      }
    }

    return newFeatured.map((featured, key) => {
      return (
        <div
          key={key}
          className={`item px-1 lg:px-0 rounded-lg lg:rounded-none cursor-pointer`}
          onClick={
            () => handleSelectProject(`/weekly-deals/${featured.id}/false`)
            // handleSelectProject(`/weekly-deals`)
          }
        >
          <div
            className={`bg-cover bg-center relative text-white`}
            style={{
              minHeight: 320,
              background: `linear-gradient(rgba(0, 0, 0, 0.1) 0%, rgba(0, 0, 0, 0.2) 80%, rgb(0, 0, 0) 100%), url("${featured.featured_images[0]}")`
            }}
          >
            {featured.discount && (
              <div className={`absolute top-0 left-0 mt-6`}>
                <div className={`bg-red-700 flex items-center pr-5 pl-12`}>
                  <span className={`font-bold text-2xl mr-3`}>
                    {`${featured.discount}%`}
                  </span>
                  <span className={`font-bold text-base uppercase`}>
                    Discount
                  </span>
                </div>
              </div>
            )}

            <div
              className={`absolute bottom-0 font-bold leading-none left-0 mb-16 lg:mb-6 lg:ml-10 text-2xl lg:text-3xl w-full lg:w-64 px-10 lg:p-0 text-center lg:text-left`}
            >
              {featured.name}
            </div>

            <div
              className={`absolute bottom-0 right-0 mb-6 lg:mb-8 lg:mr-6 flex items-center justify-center text-xs font-bold w-full lg:w-auto`}
            >
              <svg className={`feather-icon text-palette-purple h-6 w-6 mr-1`}>
                <use xlinkHref={`/assets/svg/feather-sprite.svg#map-pin`} />
              </svg>
              <span className={`opacity-75`}>
                {featured.address.suburb +
                  ", " +
                  stateAbbreviation(featured.address.state)}
              </span>
            </div>
          </div>
        </div>
      );
    });
  };

  return (
    <UserLayout>
      <Helmet>
        <meta name="description" content={metaHelper.desc} />
      </Helmet>
      <section className={`text-white`}>
        {/* Banner Row */}
        <section
          className={`text-center relative bg-cover pb-20`}
          style={rowStyles.banner}
        >
          <div
            className={`flex flex-col items-center justify-center lg:h-full ${
              isMobile ? "lg:pt-60" : "lg:pt-0"
            }  pt-40 `}
          >
            {isMobile && <MemberCount append={`pb-3`} />}

            <div
              className={`font-bold leading-tight test ${
                isMobile ? "pt-8" : "pt-0"
              }`}
              style={{ fontSize: !isMobile ? "2.5rem" : "1.75rem" }}
            >
              {!isMobile && (
                <div>
                  <span>
                    Brand new homes at <br /> significantly reduced prices.
                  </span>
                </div>
              )}

              {isMobile && userState.windowSize <= deviceSizes.baseSmall && (
                <div>
                  <span>Brand new homes at</span>
                  <br />
                  <span>significantly reduced prices.</span>
                </div>
              )}
 
              {isMobile && userState.windowSize > deviceSizes.baseSmall && (
                <div>
                  <span>
                    Brand new homes at <br /> significantly reduced prices.
                  </span>
                </div>
              )}
            </div>
            {!isMobile && (
              <div
                className={`font-bold leading-tight lg:mt-3 lg:p-0 lg:text-2xl mt-6 px-6 text-xl`}
              >
                Average saving of{" "}
                <b className="text-palette-teal">
                  <CurrencyFormat
                    value={userState.aveSavings}
                    displayType={`text`}
                    thousandSeparator={true}
                    prefix={`$`}
                  />
                </b>{" "}
                across our listed properties.
              </div>
            )}

            {isMobile && (
              <div
                className={`font-bold leading-tight lg:mt-3 lg:p-0 lg:text-2xl mt-6 px-6 text-xl`}
              >
                <span>
                  Average saving of{" "}
                  <b className="text-palette-teal">
                    {" "}
                    <CurrencyFormat
                      value={userState.aveSavings}
                      displayType={`text`}
                      thousandSeparator={true}
                      prefix={`$`}
                    />
                  </b>
                </span>
                <br />
                <span>across our listed properties.</span>
              </div>
            )}

            {!isLoggedIn() && (
              <div
                className={`mt-12 ${
                  userState.windowSize <= deviceSizes.small
                    ? "flex flex-col"
                    : "flex flex-row"
                }`}
              >
                <Button
                  onClick={() => userAction.setState({ showSignIn: true })}
                  type={`secondary`}
                  className={`${!isMobile ? "mr-2" : ""} px-16 ${
                    isMobile && userState.windowSize <= deviceSizes.small
                      ? "mb-3"
                      : "mr-2"
                  }`}
                >
                  Log in
                </Button>
                <Button
                  onClick={() => userAction.setState({ showSignUp: true })}
                  className={`${!isMobile ? "ml-2" : ""} px-16 ${
                    isMobile && userState.windowSize <= deviceSizes.small
                      ? ""
                      : "ml-2"
                  }`}
                >
                  Sign up
                </Button>
              </div>
            )}
          </div>
        </section>

        {isMobile && userState.featuredDeals.length >= 3 && (
          <section className={`text-center -mt-8`} style={rowStyles.sliders}>
            <section className={`relative`}>
              <Slider {...settings}>{renderFeatured()}</Slider>
            </section>
          </section>
        )}
        {!isMobile && userState.featuredDeals.length >= 3 && (
          <section
            className={`absolute home-deals left-0 right-0 z-20 mx-auto`}
            style={{
              padding: "75px 0px",
              overflow: "hidden",
              maxWidth: 1366,
              top: 530
            }}
          >
            {/* <Message /> */}
            <Slider {...settings}>{renderFeatured()}</Slider>
          </section>
        )}
        {/* What is Group Buyer Row */}
        <section
          className={`px-6 lg:p-0 relative z-10 bg-cover text-center flex flex-col items-center justify-center`}
          style={rowStyles.about}
        >
          <div className={`mx-auto`} style={{ maxWidth: 1366 }}>
            <PlayButton
              className={`mb-12 lg:mt-40`}
              onClick={() => setShowAbout(!showAbout)}
            />
            <div className={`font-bold leading-tight lg:text-5xl text-3xl`}>
              <div>What is GroupBuyer?</div>
            </div>
            <div className={`font-bold mt-6 lg:px-10 text-xl lg:text-2xl`}>
              <span>
                GroupBuyer is an Australian-first online platform that allows
                buyers to
              </span>
              {!isMobile && <br />}
              <span className={`text-palette-teal`}>
                {" "}
                purchase quality new properties at significantly reduced prices{" "}
              </span>
              from top-shelf developers.
            </div>
          </div>
        </section>
        {/* How It Works Row */}
        <section
          className={`px-6 lg:p-0 bg-cover text-center bg-blend-overlay bg-center flex flex-col items-center justify-center`}
          style={rowStyles.howWorks}
        >
          <div className={`mx-auto`} style={{ maxWidth: 1366 }}>
            <PlayButton
              className={`mb-12`}
              onClick={() => setShowWork(!showWork)}
            />
            <div className={`font-bold leading-tight lg:text-5xl text-3xl`}>
              <div>How does it work?</div>
            </div>
            <div
              className={`font-bold lg:text-center mt-6 lg:px-10 text-xl lg:text-2xl`}
            >
              <span>GroupBuyer provides the platform for buyers&nbsp;</span>
              {!isMobile && <br />}
              <span>
                to join
                <span className={`text-palette-teal`}> 'buyer groups' </span>to
                gain a
                <span className={`text-palette-teal`}> bulk-buy discount </span>
                from a developer.
              </span>
            </div>
          </div>
        </section>
        <section className={`bg-palette-blue-dark pb-12 pt-10 text-center`}>
          {/* Access Projects Row */}
          <div className={`mx-auto`} style={{ maxWidth: 1366 }}>
            <div>
              <div className={`font-bold leading-tight lg:text-5xl text-3xl`}>
                <div>Access our projects</div>
              </div>
              <p className={`lg:text-xl mb-2 py-3 text-base`}>
                and see all the benefits with GroupBuyer.
              </p>
              <a href={`/weekly-deals`}>
                <Button type={`secondary`}>View all projects</Button>
              </a>
              {/* {!isLoggedIn() ? (
                <Button
                  onClick={() => userAction.setState({ showSignUp: true })}
                >
                  Sign up for free
                </Button>
              ) : (
                <a href={`/weekly-deals`}>
                  <Button type={`secondary`}>View all projects</Button>
                </a>
              )} */}
            </div>
          </div>

          {/* Deal of the Week Row */}
          {/* {userState.weeklyDeal && (
            <div style={{ minHeight: 600 }} className={`mx-auto flex mt-12`}>
              <div
                className={`w-7/12 bg-cover bg-center relative`}
                style={{
                  background: `linear-gradient(transparent 50%, black 104%), url("${userState.weeklyDeal.featured_images[0]}")`
                }}
              >
                <div className={`absolute top-0 left-0 mt-10 flex`}>
                  <div
                    className={`bg-palette-teal flex items-center pl-5 pr-16 py-1 text-palette-blue-dark`}
                    style={{
                      clipPath: "polygon(0 0, 100% 0, 80% 100%, 0 100%)"
                    }}
                  >
                    <span className={`font-bold text-4xl mr-4`}>
                      {userState.weeklyDeal.discount_is_percentage
                        ? `${userState.weeklyDeal.discount * 100}%`
                        : userState.weeklyDeal.discount}
                    </span>
                    <span className={`font-bold text-2xl uppercase`}>
                      Discount
                    </span>
                  </div>
                  <div
                    className={`-ml-16 bg-palette-blue-dark flex items-center pl-16 pr-6 py-1`}
                    style={{
                      clipPath: "polygon(18% 0, 100% 0, 100% 100%, 0 100%)"
                    }}
                  >
                    <div
                      className={`leading-none text-2xl uppercase mr-4 font-bold`}
                    >
                      <div>Save</div>
                      <div>up to</div>
                    </div>
                    <div className={`font-bold text-4xl`}>
                      <CurrencyFormat
                        value={
                          userState.weeklyDeal.properties.length &&
                          Math.max.apply(
                            Math,
                            userState.weeklyDeal.properties.map(max => {
                              return userState.weeklyDeal.discount_is_percentage
                                ? max.price * userState.weeklyDeal.discount
                                : userState.weeklyDeal.discount;
                            })
                          )
                        }
                        displayType={`text`}
                        thousandSeparator={true}
                        prefix={`$`}
                      />
                    </div>
                  </div>
                </div>

                <div
                  className={`font-bold text-5xl absolute bottom-0 left-0 mb-12 ml-12`}
                >
                  {userState.weeklyDeal.name}
                </div>

                <div
                  className={`font-bold text-base absolute bottom-0 right-0 mb-16 mr-12 flex items-center`}
                >
                  <svg className={`feather-icon text-palette-purple mr-1`}>
                    <use xlinkHref={`/assets/svg/feather-sprite.svg#map-pin`} />
                  </svg>
                  <span className={`opacity-75`}>
                    {userState.weeklyDeal.address.suburb +
                      " " +
                      userState.weeklyDeal.address.state}
                  </span>
                </div>
              </div>

              <div
                className={`w-5/12 bg-cover bg-center text-left flex flex-col justify-around`}
                style={rowStyles.dealDescription}
              >
                <div
                  className={`font-bold leading-tight text-5xl text-palette-blue-dark`}
                >
                  <div>Deal of the week</div>
                </div>

                <div className={`font-bold leading-tight text-4xl mb-4`}>
                  {userState.weeklyDeal.properties.length}
                  {userState.weeklyDeal.properties.length > 1
                    ? " apartments "
                    : " apartment "}
                  in {userState.weeklyDeal.address.suburb}
                </div>

                <div className={`overflow-hidden text-base leading-normal`}>
                  <span
                    dangerouslySetInnerHTML={{
                      __html:
                        userState.weeklyDeal.description.substring(0, 500) +
                        "... "
                    }}
                  />
                  <a
                    href={`/weekly-deals/${userState.weeklyDeal.id}`}
                    className={`text-base text-palette-purple hover:text-tertiary-600 transition-all duration-300`}
                  >
                    more
                  </a>
                </div>

                <div>
                  <a href={`/weekly-deals/${userState.weeklyDeal.id}`}>
                    <Button>View project</Button>
                  </a>
                  <a href={`/weekly-deals`}>
                    <Button type={`secondary`} className={`ml-4`}>
                      View all projects
                    </Button>
                  </a>
                </div>
              </div>
            </div>
          )} */}
        </section>
        {/* Buyer Benefits Row */}
        {/* <section
          className={`bg-cover bg-center py-10`}
          style={rowStyles.benefits}
        >
          <div
            // className={`flex h-auto items-center justify-center mx-auto`}
            className={`mx-auto h-full`}
            style={{ maxWidth: 1366 }}
          >
            <div  className={`px-6 lg:px-0 h-auto text-center`}>  
          
              <div
                className={`font-bold leading-tight lg:text-5xl text-4xl mb-6 lg:mt-0 lg:mb-12`}
              >
                <div>Buyer benefits</div>
              </div>
              <p className={`font-bold lg:px-64 lg:text-2xl text-lg`}>
                Why pay full price for property when you can purchase from the
                same developer right here at GroupBuyer?
              </p>

              {!isMobile && <ul
                className={`leading-10 my-3 text-lg mx-auto`}
                style={{ width: "fit-content" }}
              >
                {buyerBenefits()}
              </ul>} 

              {isMobile && <ul
                className={`leading-10 phone`}
                
              >
                {buyerBenefits()}
              </ul>} 

              {!isLoggedIn() ? (
                <div>
                  <Button
                    onClick={() => userAction.setState({ showSignUp: true })}
                  >
                    Sign up and view our projects
                  </Button>
                  <div className={`text-center mt-4 text-base`}>
                    It's free to join!
                  </div>
                </div>
              ) : (
                <div className={``}>
                  <a href={`/weekly-deals`}>
                    <Button className={``}>View our projects</Button>
                  </a>
                </div>
              )}
            
           </div> 
          </div>
        </section> */}
        {/* Sell Your Project Row */}
        <section
          className={`bg-cover bg-center py-10`}
          style={rowStyles.benefits}
        >
          <div
            // className={`mx-auto h-auto`}
            className={`mx-auto h-full`}
            style={{ maxWidth: 1366 }}
          >
            <div
              // className={`flex h-auto items-center justify-center lg:text-center mx-auto text-justify`}
              className={`flex h-full items-center justify-center lg:text-center mx-auto text-justify`}
            >
              <div className={`text-center`}>
                <div
                  className={`font-bold leading-tight lg:text-5xl text-center mb-6 text-3xl`}
                >
                  <div>Buyer Benefits</div>
                </div>

                <p
                  className={`font-bold lg:px-64 lg:text-2xl text-lg mt-6 mx-6 lg:mx-0`}
                >
                  Why pay full price for property when you can purchase from the
                  same developer right here at GroupBuyer?
                </p>

                <ul
                  className={`mb-3 my-3 text-lg mx-auto`}
                  style={{ width: "fit-content" }}
                >
                  {buyerBenefits()}
                </ul>

                <div className={`mt-8`}>
                  <a href={`/weekly-deals`}>
                    <Button className={``}>View our projects</Button>
                  </a>
                </div>

                {/* {!isLoggedIn() ? (
                  <div>
                    <Button
                      onClick={() => userAction.setState({ showSignUp: true })}
                    >
                      Sign up and view our projects
                    </Button>
                    <div className={`text-center mt-4 text-base`}>
                      It's free to join!
                    </div>
                  </div>
                ) : (
                 
                )} */}
              </div>
            </div>
          </div>
        </section>

        <section className={`bg-cover bg-center py-10`} style={rowStyles.sell}>
          <div
            // className={`mx-auto h-auto`}
            className={`mx-auto h-full`}
            style={{ maxWidth: 1366 }}
          >
            <div
              // className={`flex h-auto items-center justify-center lg:text-center mx-auto text-justify`}
              className={`flex h-full items-center justify-center lg:text-center mx-auto text-justify`}
            >
              <div className={`text-center`}>
                <div
                  className={`font-bold leading-tight lg:text-5xl text-center mb-6 text-3xl`}
                >
                  <div>List your project</div>
                </div>

                <p
                  className={`font-bold lg:px-64 lg:text-2xl text-lg mt-6 mx-6 lg:mx-0`}
                >
                  GroupBuyer partners with Australia's most successful and
                  well-regarded project agents and property developers.
                </p>

                <ul
                  className={`leading-10 my-3 text-lg mx-auto`}
                  style={{ width: "fit-content" }}
                >
                  {listYourProject()}
                </ul>

                <p className={`text-lg mt-8`}>
                  <a href={`tel:1300031835`} className={`text-palette-teal`}>
                    Call{" "}
                  </a>
                  or send us a
                  <span
                    className={`text-palette-teal cursor-pointer`}
                    onClick={handleScrollBottom}
                  >
                    {" "}
                    Message{" "}
                  </span>
                  to get in touch.
                </p>

                <div className={`mt-8 flex justify-center`}>
                  <Button
                    onClick={handleScrollBottom}
                    // onClick={() =>
                    //   userAction.setState({ showSellerApply: true })
                    // }
                    // className={`bg-palette-violet hover:bg-palette-blue-light`}
                  >
                    Enquire
                  </Button>
                  {/* <a href={`/`}>
                    <Button type={`secondary`} className={`ml-2`}>View benefits</Button>
                  </a> */}
                </div>
              </div>

              {/* <div className={`w-5/12`}>
                <img
                  src={`/assets/images/landing_page_sell_image.jpg`}
                  className={`object-cover`}
                  style={{ minHeight: 400 }}
                />
              </div> */}
            </div>

            {/* <div style={{maxWidth: 1280}} className={`mx-auto flex text-left mt-10 pl-16`}>
              <div className={`font-bold leading-tight text-2xl mb-12`}>
                <div>Our Partners</div>
              </div>
            </div> */}
          </div>
        </section>
        <Modal
          show={showAbout}
          title={`What is GroupBuyer?`}
          maxWidth={`md`}
          onClose={() => handleClose()}
        >
          <ReactPlayer
            className={`mb-4`}
            url={`https://www.youtube.com/watch?v=Vpxopv8gr8M`}
            width={`100%`}
            height={isMobile ? `300px` : `520px`}
          />
        </Modal>
        <Modal
          show={showWork}
          title={`How does it work?`}
          maxWidth={`md`}
          onClose={() => handleClose()}
        >
          <ReactPlayer
            className={`mb-4`}
            // url={`https://www.youtube.com/watch?v=m8kBcCIQvx8`}
            url={`https://www.youtube.com/watch?v=_2EC8cfrFBI`}
            width={`100%`}
            height={isMobile ? `300px` : `520px`}
          />
        </Modal>
      
      </section>
    </UserLayout>
  );
};

const settings = {
  dots: false,
  arrows: false,
  infinite: true,
  autoplay: true,
  autoplaySpeed: 5000,
  slidesToShow: 3,
  responsive: [
    {
      breakpoint: 1024,
      settings: {
        slidesToShow: 3,
        slidesToScroll: 3,
        infinite: true,
        dots: true
      }
    },
    {
      breakpoint: 600,
      settings: {
        infinite: true,
        slidesToShow: 2,
        slidesToScroll: 2
      }
    },
    {
      breakpoint: 480,
      settings: {
        infinite: true,
        centerMode: true,
        slidesToShow: 1,
        slidesToScroll: 1
      }
    }
  ]
};

const rowStyles = {
  banner: {
    background:
      "linear-gradient(rgba(47, 54, 86, 0.4) 0%, rgba(47, 54, 86, 0.7) 85%, rgba(47, 54, 86, 1) 100%), url('/assets/images/landing_page_banner_1.jpg')",
    marginTop: isMobile ? -162 : -130,
    height: isMobile ? "auto" : 800,
    backgroundSize: "cover",
    backgroundPosition: "center",
    backgroundRepeat: "no-repeat"
  },
  sliders: {
    background:
      "linear-gradient(to top, rgba(255, 0, 255, 1) 0%, rgba(47, 54, 86, 1) 100%), url('/assets/images/landing_page_what_is_group_buyer.jpg')"
  },
  about: {
    background:
      "linear-gradient(to left, blue 0%, rgba(0, 0, 255, 0.5) 0%, rgba(255, 0, 255, 0.7) 100%), url('/assets/images/landing_page_what_is_group_buyer.jpg')",
    height: isMobile ? 550 : 800,
    marginTop: "-5px"
  },
  howWorks: {
    background:
      "url('/assets/images/landing_page_how_it_works.jpg'), rgba(7, 14, 51, 0.8)",
    height: isMobile ? 550 : 650
  },
  dealDescription: {
    background:
      "linear-gradient(to left, rgba(0, 159, 255, 0.9) 0%, rgba(34, 222, 222, 0.9) 100%), url('/assets/images/landing_page_deal_descriptions.jpg')",
    padding: "20px 60px 20px 30px"
  },
  benefits: {
    background:
      "linear-gradient(to left, rgb(30, 136, 225) 0%, rgba(30, 136, 255, 0.5) 0%), url('/assets/images/landing_page_buyer_benefits2.png')",
    // padding: "80px 0",
    height: isMobile ? "auto" : 650,
    backgroundSize: "cover",
    backgroundPosition: "center",
    backgroundRepeat: "no-repeat"
  },
  sell: {
    background:
      "linear-gradient(to left, blue 0%, rgba(0, 0, 255, 0.5) 0%, rgba(255, 0, 255, 0.7) 100%), url('/assets/images/landing_page_sell_your_project.jpg')",
    // padding: "80px 0",
    height: isMobile ? "auto" : 650
  }
};

export default React.memo(Home);
