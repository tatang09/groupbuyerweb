import React, { useState, useEffect } from "react";

import UserLayout from "~/layouts/users/UserLayout";
import UserGlobal from "~/states/userGlobal";

import {
  BuyingProgress,
  SecureDeal,
  AgentDetailsSection,
  SocialMediaShareModal,
  ResourcesComponent,
  Deposit,
  EstimatedCompletionDate,
  ReadyForOccupancy,
  BookInspection
} from "~/components";
import { isLoggedIn } from "~/services/auth";

import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { isMobile } from "react-device-detect";
import CurrencyFormat from "react-currency-format";
import DateCountdown from "react-date-countdown-timer";
import Slider from "react-slick";

import { Tooltip } from "~/components/_base";
import { isEmpty } from "lodash";

import AdContainerComponent from "../components/adComponents/AdContainerComponent";
import OutgoingCosts from "../components/OutgoingCosts";
import TotalSize from "../components/TotalSize";
import LandProjectSize from "../components/LandProjectSize";

const BuyingApartmentDetailsPage = ({ dealId, propertyId, isPreview }) => {
  const [userState, userAction] = UserGlobal();
  const [show, setShow] = useState(false);
  const [loading, setLoading] = useState(true);
  const [project, setProject] = useState();
  const [apartment, setApartment] = useState();
  const [isPrev, seIsPrev] = useState(isPreview === "true");
  const [timeleft, setTimeLeft] = useState("");
  const [showBookingModal, setShowBookingModal] = useState(false);
  const [isProjectLand, setIsProjectLand] = useState(false);

  const toogleShowBookingModal = () => {
    setShowBookingModal(!showBookingModal);
  };

  const getDeal = async () => {
    const deal = await axios.get(`/api/deal/${dealId}`);

    if (deal.data.sub_property_id == 4) {
      setIsProjectLand(true);
    }

    if (deal.data && deal.data.expires_at) {
      let exp = JSON.parse(deal.data.expires_at);
      setTimeLeft(exp.time_limit);
    }

    let newApartment = null;

    if (isLoggedIn() && !isEmpty(userState.user)) {
      const { data } = await axios.get(`/api/wish-list/${dealId}`, {
        headers: {
          Authorization: "Bearer " + isLoggedIn()
        }
      });

      let newProject = Object.assign({}, deal.data);

      if (data && !isEmpty(newProject)) {
        data.map(d => {
          newProject.properties.map(p => {
            if (p.id == d.property_id) {
              p.hasWishlist = true;
              p.wishListId = d.id;
            }
          });
        });
      }

      newApartment = newProject.properties.find(p => p.id == propertyId);

      if (!isEmpty(newApartment)) {
        setApartment(newApartment);
      }
      setProject(newProject);
      setLoading(false);
    } else {
      newApartment = deal.data.properties.find(p => p.id == propertyId);
      setApartment(newApartment);
      setProject(deal.data);
      setLoading(false);
    }
  };

  const handleWishlist = async property => {
    if (!isLoggedIn()) {
      userAction.setState({ showSignIn: true });
      return;
    }

    let formData = new FormData();

    formData.append("property_id", property.id);
    formData.append("deal_id", dealId);

    try {
      if (!property.hasWishlist) {
        await axios.post("/api/wish-list", formData, {
          headers: {
            Authorization: "Bearer " + isLoggedIn()
          }
        });
      } else {
        await axios.delete(`/api/wish-list/${property.wishListId}`, {
          headers: {
            Authorization: "Bearer " + isLoggedIn()
          }
        });
      }

      getDeal();
    } catch (error) {}
  };

  const getDescription = node => {
    let nodes = node;
    let htmlObject = document.createElement("div");
    htmlObject.innerHTML = nodes;
    let features = htmlObject.getElementsByTagName("ul")[0];
    let inclusions = htmlObject.getElementsByTagName("ul")[1];
    let featureHeader = htmlObject.getElementsByTagName("p");

    for (let index = 0; index <= featureHeader.length; index++) {
      let p = featureHeader[index];
      if (
        p &&
        (p.innerHTML === "<strong>Features:</strong>" ||
          p.innerHTML === "<strong>Inclusions:</strong>")
      ) {
        p.setAttribute("hidden", "true");
      }
    }

    features.remove();
    inclusions.remove();

    return htmlObject.innerHTML;
  };

  const getFeatures = node => {
    let nodes = node;
    let htmlObject = document.createElement("div");
    htmlObject.innerHTML = nodes;
    let features = htmlObject.getElementsByTagName("ul")[0].innerHTML;
    let inclusions = htmlObject.getElementsByTagName("ul")[1].innerHTML;
    return { features, inclusions };
  };

  useEffect(() => {
    // if (!isLoggedIn()) {
    //   window.location = `/weekly-deals`;
    // }
    getDeal();
  }, [userState.user]);

  const handleClose = () => {
    setShow(false);
  };

  return (
    <UserLayout>
      <section
        className={`bg-palette-blue-dark`}
        style={{ marginTop: -130, paddingTop: 160 }}
      >
        <section className={`relative bg-white`}>
          {!loading && apartment && !apartment.is_secured && (
            <BuyingProgress
              current={3}
              project_id={dealId}
              isPreview={isPrev}
            />
          )}

          {loading && (
            <div
              className={`w-full bg-palette-purple text-white font-bold text-center p-4 text-xl`}
            >
              Loading property details...
            </div>
          )}

          {!loading && apartment && apartment.is_secured && (
            <div
              className={`w-full bg-palette-purple text-white font-bold text-center p-4 text-xl`}
            >
              This deal has already been secured.
            </div>
          )}

          {!isMobile && (
            <section className={`relative z-20`}>
              <Slider {...featuredSettings}>
                {!loading &&
                  apartment.featured_images &&
                  apartment.featured_images.map((image, key) => {
                    return (
                      <img
                        key={key}
                        src={`${image}`}
                        className={`object-cover carousel-featured`}
                      />
                    );
                  })}
              </Slider>
            </section>
          )}

          {!isMobile && !loading && project.discount && (
            <section
              className={`mt-8 lg:-mt-8 relative text-white z-20 inline-block w-full lg:w-auto`}
            >
              <div className={`flex lg:inline-flex shadow-lg`}>
                <div
                  className={`w-1/2 lg:w-auto bg-red-700 flex items-center lg:pl-12 lg:pr-24 px-3 py-1`}
                  style={{
                    clipPath: "polygon(0 0, 100% 0, 80% 100%, 0 100%)"
                  }}
                >
                  <span className={`font-bold lg:text-4xl mr-4 text-2xl`}>
                    {!loading && `${project.discount}%`}
                  </span>
                  <span className={`font-bold lg:text-2xl text-lg uppercase`}>
                    Discount
                  </span>
                </div>

                <div
                  className={`w-6/12 lg:w-auto lg:-ml-20 bg-red-800 flex items-center lg:pl-16 px-3 py-1`}
                  style={{
                    clipPath: "polygon(18% 0, 100% 0, 100% 100%, 0 100%)"
                  }}
                >
                  <div
                    className={`font-bold leading-none lg:text-4xl mr-4 text-center text-lg uppercase`}
                  >
                    <div>Save</div>
                  </div>
                  <div className={`font-bold lg:text-4xl text-2xl`}>
                    {!loading && apartment && (
                      <CurrencyFormat
                        value={(apartment.price * project.discount) / 100}
                        displayType={`text`}
                        thousandSeparator={true}
                        prefix={`$`}
                      />
                    )}
                  </div>
                </div>
              </div>
            </section>
          )}

          {isMobile && !loading && project.discount && (
            <section
              className={`mt-8 lg:-mt-8 relative text-white z-20 inline-block w-full lg:w-auto`}
            >
              <div className={`flex lg:inline-flex shadow-lg`}>
                <div
                  id={`discountBanner`}
                  className={`lg:-mr-20 items-center sm:justify-center -mr-16 sm:-mr-32 md:justify-center bg-red-700 flex lg:pl-12 lg:pr-24 lg:w-auto w-full xs:pl-8`}
                  style={{
                    clipPath: "polygon(0 0, 100% 0, 80% 100%, 0 100%)"
                  }}
                >
                  <span className={`font-bold lg:text-4xl mr-4 text-base`}>
                    {`${project.discount}%`}
                  </span>
                  <span className={`font-bold lg:text-2xl text-base uppercase`}>
                    Discount
                  </span>
                </div>

                <div
                  className={`bg-red-800 flex items-center lg:-ml-20 lg:pl-20 lg:w-auto pl-12 px-3 py-1 w-full justify-center`}
                  style={{
                    clipPath: "polygon(18% 0, 100% 0, 100% 100%, 0 100%)"
                  }}
                >
                  <div
                    className={`font-bold leading-none lg:mr-4 mr-1 text-center uppercasee`}
                  >
                    <div className={`text-sm lg:text-2xl`}>SAVE</div>
                    <div className={`text-sm lg:text-2xl`}>UP TO</div>
                  </div>
                  <div className={`ml-2`}>
                    {!loading && apartment && (
                      <CurrencyFormat
                        value={(apartment.price * project.discount) / 100}
                        displayType={`text`}
                        thousandSeparator={true}
                        prefix={`$`}
                        className={`font-bold lg:text-4xl text-lg`}
                      />
                    )}
                  </div>
                </div>
              </div>
            </section>
          )}

          <section
            className={`flex flex-col lg:flex-row lg:px-10 mt-6 pb-16 lg:pb-24 mx-auto relative`}
            style={{ maxWidth: 1600 }}
          >
            {/* <div
              className={`absolute h-auto left-0 w-1/4 p-12 pt-4`}
              style={{ marginLeft: "-320px" }}
            >
              {!loading && <LargeScreenAdComponent />}
            </div>
            <div
              className={`absolute h-auto right-0 w-1/4 p-12 pt-4`}
              style={{ marginRight: "-320px" }}
            >
              {!loading && <LargeScreenAdComponent />}
            </div> */}
            {!isMobile && (
              <div className={`lg:w-3/12 w-full`}>
                <div className={`relative`}>
                  <a
                    href={`/weekly-deals/${dealId}/${isPreview}`}
                    className={`bg-white cursor-pointer duration-300 font-bold hover:text-palette-blue inline-flex items-center pr-3 relative text-palette-blue-light text-sm transition-all z-10`}
                  >
                    <svg className={`feather-icon h-4 w-4 mr-1 inline`}>
                      <use
                        xlinkHref={`/assets/svg/feather-sprite.svg#chevron-left`}
                      />
                    </svg>
                    Back to project details
                  </a>
                  <div
                    className={`absolute border-gray-200 border-t w-full`}
                    style={{ top: 10 }}
                  />
                </div>

                <h1
                  className={`font-bold lg:mb-2 lg:text-5xl mb-3 mt-2 text-4xl leading-none`}
                >
                  {!loading && project.name}
                </h1>
                <div className={`font-bold text-base`}>
                  {!loading && project.address && (
                    <>
                      {project.address.line_1
                        ? project.address.line_1 + ", "
                        : ""}
                      {project.address.line_2
                        ? project.address.line_2 + ", "
                        : ""}
                      {project.address.suburbs
                        ? project.address.suburbs + ", "
                        : ""}
                      {project.address.suburb
                        ? project.address.suburb + ", "
                        : ""}
                      {project.address.state ? project.address.state + " " : ""}
                      {project.address.postal ? project.address.postal : ""}
                    </>
                  )}
                </div>

                {!loading && !isProjectLand && (
                  <div
                    className={`px-4 py-1 shadow-lg mt-4 bg-red-700 font-bold text-white rounded-lg`}
                  >
                    <span>Time left: </span>

                    {(!loading && timeleft && (
                      <DateCountdown
                        dateTo={timeleft ? timeleft : project.created_at}
                        // dateTo={project.expires_at}
                        locales={[
                          "Year, ",
                          "Month, ",
                          "Day, ",
                          "hr, ",
                          "min, ",
                          "sec"
                        ]}
                        locales_plural={[
                          "Years, ",
                          "Months, ",
                          "Days, ",
                          "hrs, ",
                          "min, ",
                          "sec"
                        ]}
                      />
                    )) ||
                      "-"}
                  </div>
                )}

                <div className={`flex`}>
                  {!isEmpty(project) &&
                    !isProjectLand &&
                    !project.is_completed && (
                      <EstimatedCompletionDate
                        loading={loading}
                        project={project}
                      />
                    )}

                  {!isEmpty(project) && project.is_completed && (
                    <ReadyForOccupancy />
                  )}

                  {!isEmpty(project) && project.deposit > 0 && (
                    <Deposit deposit={project.deposit} />
                  )}
                </div>

                {!isLoggedIn() ? (
                  <>
                    <div className={`rounded bg-blue-100 p-5 text-center my-4`}>
                      <div className={`mb-3 text-base`}>
                        <span
                          onClick={() =>
                            userAction.setState({ showSignIn: true })
                          }
                          className={`cursor-pointer font-bold text-palette-purple hover:text-palette-violet`}
                        >
                          Sign in
                        </span>
                        &nbsp;or&nbsp;
                        <span
                          onClick={() =>
                            userAction.setState({ showSignUp: true })
                          }
                          className={`cursor-pointer font-bold text-palette-purple hover:text-palette-violet`}
                        >
                          Sign up
                        </span>
                        &nbsp;to secure this deal
                      </div>
                      <span className={`lg:text-base`}>
                        Secure this deal for only $1,000
                      </span>
                    </div>
                  </>
                ) : (
                  <>
                    {apartment && !apartment.is_secured && (
                      <>
                        <div
                          className={`rounded ${
                            isPrev ? "bg-gray-400" : "bg-blue-100"
                          } p-5 text-center my-4`}
                        >
                          <div className={`font-bold text-base`}>
                            Yes! I want to
                          </div>
                          <button
                            disabled={isPrev}
                            onClick={() => setShow(true)}
                            className={`rounded text-base font-bold p-4 ${
                              !isPrev ? "text-white" : "text-gray-400"
                            } w-full text-center my-3 ${
                              isPrev ? "bg-palette-violet" : "bg-palette-purple"
                            } hover:bg-palette-violet transition-all duration-300`}
                          >
                            Secure this deal
                          </button>
                          <span className={`lg:text-base`}>
                            Secure this deal for only $1,000
                          </span>
                        </div>
                      </>
                    )}
                  </>
                )}
                <div className={`my-4 p-1 rounded text-center`}>
                  <button
                    disabled={isPrev}
                    onClick={() => setShowBookingModal(true)}
                    className={`rounded text-base font-bold p-2 ${
                      !isPrev ? "text-white" : "text-gray-400"
                    } w-1/2 text-center hover:bg-gray-900 bg-gray-700 transition-all duration-300`}
                  >
                    Book Inspection
                  </button>
                </div>
              </div>
            )}

            <div
              className={`relative px-6 lg:px-16 lg:w-7/12`}
              style={{ minHeight: isMobile ? null : 440 }}
            >
              {isMobile && (
                <>
                  <div className={`relative mb-3`}>
                    <a
                      href={`/weekly-deals/${dealId}/${isPreview}`}
                      className={`bg-white cursor-pointer duration-300 font-bold hover:text-palette-blue inline-flex items-center pr-3 relative text-palette-blue-light text-sm transition-all z-10`}
                    >
                      <svg className={`feather-icon h-4 w-4 mr-1 inline`}>
                        <use
                          xlinkHref={`/assets/svg/feather-sprite.svg#chevron-left`}
                        />
                      </svg>
                      Back to project details
                    </a>
                    <div
                      className={`absolute border-gray-200 border-t w-full`}
                      style={{ top: 10 }}
                    />
                  </div>

                  {!isPrev && (
                    <div className={`flex`}>
                      <div className={`flex flex-col`}>
                        <div>
                          <div>
                            <a
                              href={`/weekly-deals/${dealId}/${isPreview}`}
                              className={`font-bold text-2xl opacity-25 underline`}
                            >
                              {!loading && project.name}
                            </a>
                            <span className={`font-bold text-2xl opacity-25`}>
                              /{" "}
                              {(!loading &&
                                apartment &&
                                "Lot " +
                                  (!isProjectLand
                                    ? apartment.unit_no
                                    : apartment.unit_name)) ||
                                ""}
                            </span>
                          </div>
                          <div>
                            <div
                              className={`font-bold text-2xl lg:text-4xl leading-tight italic`}
                            >
                              {isProjectLand ? "Lot " : "Apartment"}{" "}
                              {(!loading && apartment && apartment.unit_name) ||
                                ""}
                            </div>
                            {!isProjectLand && (
                              <div
                                className={`font-bold text-base flex items-center my-3`}
                              >
                                <span className={`mr-4 text-lg`}>
                                  {(!loading && apartment.no_of_bedrooms) || ""}
                                  <FontAwesomeIcon
                                    icon={["fas", "bed"]}
                                    className={`text-2xl ml-1`}
                                  />
                                </span>
                                <span className={`mr-4 text-lg`}>
                                  {(!loading && apartment.no_of_bathrooms) ||
                                    ""}
                                  <FontAwesomeIcon
                                    icon={["fas", "bath"]}
                                    className={`text-2xl fa-w-20`}
                                  />
                                </span>
                                <span className={`mr-4 text-lg`}>
                                  {(!loading && apartment.no_of_garages) || ""}
                                  <FontAwesomeIcon
                                    icon={["fas", "car"]}
                                    className={`text-2xl fa-w-20`}
                                  />
                                </span>
                              </div>
                            )}
                          </div>
                        </div>
                      </div>

                      <div className={`flex flex-1 justify-end mr-4`}>
                        <Tooltip
                          title={`Share`}
                          placement={`top`}
                        >
                          <svg
                            className={`feather-icon h-5 w-5 cursor-pointer mr-3`}
                            onClick={() =>
                              userAction.setState({ showSocialMedia: true })
                            }
                          >
                            <use
                              xlinkHref={`/assets/svg/feather-sprite.svg#share`}
                            />
                          </svg>
                        </Tooltip>

                        <Tooltip
                          title={`${
                            !loading && apartment.hasWishlist
                              ? "Already on wishlist"
                              : "Add to wishlist"
                          }`}
                          placement={`top`}
                        >
                          <svg
                            className={`${
                              !loading && apartment.hasWishlist
                                ? "text-red-600"
                                : "text-black"
                            } feather-icon h-5 w-5 cursor-pointer`}
                            onClick={() => handleWishlist(apartment)}
                          >
                            <use
                              xlinkHref={`/assets/svg/feather-sprite.svg#heart`}
                              className={`${
                                !loading && apartment.hasWishlist
                                  ? "fill-current"
                                  : null
                              }`}
                            />
                          </svg>
                        </Tooltip>
                      </div>
                    </div>
                  )}
                </>
              )}

              <div
                className={`lg:border-b-2 lg:pb-4 lg:mb-4 flex items-center justify-between`}
              >
                <div className={`w-full`}>
                  <div>
                    {!isMobile && (
                      <div className={`flex flex-col`}>
                        <div className={`flex`}>
                          <div className={`flex flex-1`}>
                            <a
                              href={`/weekly-deals/${dealId}/${isPrev}`}
                              className={`font-bold text-2xl opacity-25 underline`}
                            >
                              {!loading && project.name}
                            </a>
                            <span className={`font-bold text-2xl opacity-25`}>
                              /{" "}
                              {(!loading &&
                                !isProjectLand &&
                                apartment &&
                                "Lot " + apartment.unit_no) ||
                                ""}
                              {(!loading &&
                                apartment &&
                                isProjectLand &&
                                "Lot " + apartment.unit_name) ||
                                ""}
                            </span>
                          </div>
                          <div className={`flex flex-1 justify-end mr-4`}>
                            {!isMobile && !isPrev && (
                              <div className={`text-base flex mt-2`}>
                                <Tooltip
                                  title={`Share`}
                                  placement={`top`}
                                >
                                  <svg
                                    className={`feather-icon h-5 w-5 cursor-pointer mr-3`}
                                    onClick={() =>
                                      userAction.setState({
                                        showSocialMedia: true
                                      })
                                    }
                                  >
                                    <use
                                      xlinkHref={`/assets/svg/feather-sprite.svg#share`}
                                    />
                                  </svg>
                                </Tooltip>
                                <Tooltip
                                  title={`${
                                    !loading && apartment.hasWishlist
                                      ? "Already on wishlist"
                                      : "Add to wishlist"
                                  }`}
                                  placement={`top`}
                                >
                                  <svg
                                    className={`${
                                      !loading && apartment.hasWishlist
                                        ? "text-red-600"
                                        : "text-black"
                                    } feather-icon h-5 w-5 cursor-pointer`}
                                    onClick={() => handleWishlist(apartment)}
                                  >
                                    <use
                                      xlinkHref={`/assets/svg/feather-sprite.svg#heart`}
                                      className={`${
                                        !loading && apartment.hasWishlist
                                          ? "fill-current"
                                          : null
                                      }`}
                                    />
                                  </svg>
                                </Tooltip>
                              </div>
                            )}
                          </div>
                        </div>

                        <div className={`flex`}>
                          <div
                            className={`flex flex-1 font-bold italic leading-tight lg:text-4xl mt-2 text-2xl`}
                          >
                            {isProjectLand ? "Lot " : "Apartment"}{" "}
                            {(!loading && apartment && apartment.unit_name) ||
                              ""}
                          </div>
                          {!isProjectLand && (
                            <div
                              className={`flex flex-1 font-bold items-center justify-end my-3 text-base`}
                            >
                              <span className={`mr-4 text-2xl`}>
                                {(!loading && apartment.no_of_bedrooms) || ""}
                                <FontAwesomeIcon
                                  icon={["fas", "bed"]}
                                  className={`text-2xl ml-1`}
                                />
                              </span>
                              <span className={` mr-4 text-2xl`}>
                                {(!loading && apartment.no_of_bathrooms) || ""}
                                <FontAwesomeIcon
                                  icon={["fas", "bath"]}
                                  className={`text-2xl fa-w-20`}
                                />
                              </span>
                              <span className={`mr-4 text-2xl`}>
                                {(!loading && apartment.no_of_garages) || ""}
                                <FontAwesomeIcon
                                  icon={["fas", "car"]}
                                  className={`text-2xl fa-w-20`}
                                />
                              </span>
                            </div>
                          )}
                        </div>
                      </div>
                    )}

                    <div className={`flex`}>
                      <span
                        className={`opacity-75 line-through font-bold text-xl lg:text-2xl mr-3`}
                      >
                        {!loading && apartment && (
                          <>
                            WAS{" "}
                            <CurrencyFormat
                              value={apartment.price || 0}
                              displayType={`text`}
                              thousandSeparator={true}
                              prefix={`$`}
                            />
                          </>
                        )}
                      </span>

                      <span
                        className={`text-red-700 font-bold text-xl lg:text-2xl`}
                      >
                        {!loading && apartment && (
                          <>
                            NOW{" "}
                            <CurrencyFormat
                              value={
                                apartment.price -
                                apartment.price * (project.discount / 100)
                              }
                              displayType={`text`}
                              thousandSeparator={true}
                              prefix={`$`}
                            />
                          </>
                        )}
                      </span>
                    </div>
                  </div>

                  {isMobile && (
                    <div className={`flex xs:flex-col`}>
                      {!isEmpty(project) &&
                        !isProjectLand &&
                        !project.is_completed && (
                          <EstimatedCompletionDate
                            loading={loading}
                            project={project}
                          />
                        )}

                      {!isEmpty(project) && project.is_completed && (
                        <ReadyForOccupancy />
                      )}

                      {!isEmpty(project) && project.deposit > 0 && (
                        <Deposit deposit={project.deposit} />
                      )}
                    </div>
                  )}
                </div>
              </div>

              {!isMobile && (
                <>
                  <div
                    className={`text-base description-list border-gray-500 border-b-2 pb-4`}
                    dangerouslySetInnerHTML={{
                      __html: !loading && apartment.description
                    }}
                  />

                  {!loading && !isProjectLand && (
                    <TotalSize apartment={apartment} />
                  )}

                  {!loading && isProjectLand && (
                    <LandProjectSize apartment={apartment} />
                  )}

                  {!loading &&
                    !isProjectLand &&
                    apartment.strata_cost > 0 &&
                    apartment.water_cost > 0 &&
                    apartment.council_cost > 0 && (
                      <OutgoingCosts apartment={apartment} />
                    )}

                  {!isProjectLand && (
                    <p className="text-sm italic mt-10 ">
                      *Property images are for illustration purposes only and
                      may not be specific to your particular property. Property
                      sizes and outgoing costs are estimates only.
                    </p>
                  )}
                </>
              )}
            </div>

            {isMobile && (
              <>
                <section className={`lg:mt-0 mt-8 relative z-20`}>
                  <Slider {...featuredSettings}>
                    {!loading &&
                      apartment.featured_images &&
                      apartment.featured_images.map((image, key) => {
                        return (
                          <div key={key} className={`px-1`}>
                            <img
                              src={`${image}`}
                              className={`object-cover carousel-featured rounded-lg`}
                            />
                          </div>
                        );
                      })}
                  </Slider>
                </section>

                {isMobile && (
                  <>
                    <div className={`lg:w-3/12 w-full px-16 mt-8`}>
                      <div
                        className={`rounded bg-blue-100 p-5 text-center my-4`}
                      >
                        {!isLoggedIn() ? (
                          <>
                            <div className={`mb-3 text-base`}>
                              <span
                                onClick={() =>
                                  userAction.setState({ showSignIn: true })
                                }
                                className={`cursor-pointer font-bold text-palette-purple hover:text-palette-violet`}
                              >
                                Sign in
                              </span>
                              &nbsp;or&nbsp;
                              <span
                                onClick={() =>
                                  userAction.setState({ showSignUp: true })
                                }
                                className={`cursor-pointer font-bold text-palette-purple hover:text-palette-violet`}
                              >
                                Sign up
                              </span>
                              &nbsp;to secure this deal
                            </div>
                            <span className={`lg:text-base`}>
                              Secure this deal for only $1,000
                            </span>
                          </>
                        ) : (
                          <>
                            {!loading && apartment && apartment.is_secured ? (
                              <>
                                <div
                                  className={`font-bold text-base text-palette-gray`}
                                >
                                  Deal already secured for this apartment.
                                </div>
                              </>
                            ) : (
                              <>
                                <div className={`font-bold text-base`}>
                                  Yes! I want to
                                </div>
                                <button
                                  onClick={() => setShow(true)}
                                  className={`rounded text-base font-bold p-4 text-white w-full text-center my-3 bg-palette-purple hover:bg-palette-violet transition-all duration-300`}
                                >
                                  Secure this deal
                                </button>
                                <span className={`lg:text-base`}>
                                  Secure this deal for only $1,000
                                </span>
                              </>
                            )}
                          </>
                        )}
                      </div>
                      <div className={`my-4 p-1 rounded text-center`}>
                        <button
                          disabled={isPrev}
                          onClick={() => setShowBookingModal(true)}
                          className={`rounded text-base font-bold p-2 ${
                            !isPrev ? "text-white" : "text-gray-400"
                          }  text-center hover:bg-gray-900 bg-gray-700 transition-all duration-300`}
                        >
                          Book Inspection
                        </button>
                      </div>
                    </div>
                    <div
                      className={`px-6 mt-8 text-base description-list`}
                      dangerouslySetInnerHTML={{
                        __html: !loading && apartment.description
                      }}
                    />

                    {!loading && (
                      <div className={`mx-8`}>
                        <TotalSize apartment={apartment} />
                      </div>
                    )}

                    {!loading &&
                      apartment.strata_cost > 0 &&
                      apartment.water_cost > 0 &&
                      apartment.council_cost > 0 && (
                        <div className={`mx-8`}>
                          <OutgoingCosts apartment={apartment} />
                        </div>
                      )}
                    {!isProjectLand && (
                      <p className="italic px-8 text-sm mt-10">
                        *Property images are for illustration purposes only and
                        may not be specific to your particular property.
                        Property sizes and outgoing costs are estimates only.
                      </p>
                    )}
                  </>
                )}
              </>
            )}

            <div className={`lg:w-2/12 px-16 mt-6 lg:mt-0 lg:p-0`}>
              <div
                className={`text-palette-gray uppercase font-bold text-center lg:text-left text-base mb-4`}
              >
                Resources
              </div>
              <ul>
                {!loading && (
                  <ResourcesComponent
                    project={project}
                    floorPlan={apartment.floor_plan}
                  />
                )}
                <span
                  onClick={() => userAction.setState({ showSocialMedia: true })}
                  className={`${
                    isMobile ? "justify-center" : ""
                  } mt-3 cursor-pointer flex items-center bg-gray-200 block font-bold px-3 py-1 leading-relaxed text-base rounded text-palette-blue-light`}
                >
                  Share this project
                  <svg className={`feather-icon h-4 w-4 ml-2`}>
                    <use xlinkHref={`/assets/svg/feather-sprite.svg#share`} />
                  </svg>
                </span>
              </ul>
            </div>
          </section>

          <div className={`lg:mb-24 items-center justify-center flex`}>
            <section
              className={`flex-1 pt-4 mt-8 px-6 lg:px-0 h-full`}
              style={{ maxWidth: 1366 }}
            ></section>
          </div>

          {!loading && project && (
            <AgentDetailsSection
              showAdd={true}
              project={project}
              isProjectLand={isProjectLand}
            />
          )}

          {!loading && (
            <BookInspection
              show={showBookingModal}
              toogleShowBookingModal={toogleShowBookingModal}
              propertyId={propertyId}
              dealId={dealId}
            />
          )}
        </section>
      </section>

      {!loading && apartment && (
        <SecureDeal
          show={show}
          handleClose={() => handleClose()}
          apartment={apartment}
          project={project}
        />
      )}

      <SocialMediaShareModal />
    </UserLayout>
  );
};

const featuredSettings = {
  dots: true,
  arrows: false,
  infinite: true,
  autoplay: true,
  autoplaySpeed: 5000,
  slidesToShow: 3,
  responsive: [
    {
      breakpoint: 1024,
      settings: {
        slidesToShow: 3,
        slidesToScroll: 3,
        infinite: true,
        dots: true
      }
    },
    {
      breakpoint: 600,
      settings: {
        slidesToShow: 2,
        slidesToScroll: 2
      }
    },
    {
      breakpoint: 480,
      settings: {
        centerMode: true,
        slidesToShow: 1,
        slidesToScroll: 1
      }
    }
  ]
};

export default BuyingApartmentDetailsPage;
