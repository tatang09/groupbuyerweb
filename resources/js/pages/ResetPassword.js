import React, { useState, useEffect, useRef } from "react";
import UserLayout from "~/layouts/users/UserLayout";
import { Button, Modal, TextInput } from "~/components/_base";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { resetPassword } from "../data/resetPassword/resetPassword";
import { faSpinner } from "@fortawesome/free-solid-svg-icons";
import { sweetAlert } from "../components/_base/alerts/sweetAlert";
import { isEmpty } from "lodash";

const ResetPassword = () => {
  const [errors, setErrors] = useState({});
  const [loading, setLoading] = useState(false);
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [confirmPassword, setPasswordConfirm] = useState("");
  const [showPassword, setShowPassword] = useState(false);
  const [showConfirmPassword, setShowConfirmPassword] = useState(false);
  const [paramToken, setToken] = useState("");

  const requestPasswordReset = e => {
    e.preventDefault();

    if (password !== confirmPassword) {
      sweetAlert("error", "Password did not match!");
      return;
    }

    setLoading(true);
    let url = "/api/password/reset";
    let param = {
      email: email,
      password: password,
      password_confirmation: confirmPassword,
      token: paramToken
    };

    resetPassword({ url, param })
      .then(res => {        
        if (res.status == 200) {
          setLoading(false);
          sweetAlert("success", res.data.message);
          window.location = "/"
        }
      })
      .catch(error => {     
        setErrors(error.response.data.errors);
        setLoading(false);
      });
  };

  const handleInputChange = (field, e) => {
    switch (field) {
      case "password":
        setPassword(e.target.value);
        break;
      case "confirmPassword":
        setPasswordConfirm(e.target.value);
        break;
      default:
        setEmail(e.target.value);
        break;
    }
  };

  const backToReset = e => {
    e.preventDefault();
    window.location = "/request-password-reset";
  };

  useEffect(() => {
    let token = new URLSearchParams(window.location.search).get("token");
    setToken(token);
  }, []);

  return (
    <UserLayout>
      <section
        className="text-white bg-palette-blue-dark"
        style={{ marginTop: -130, paddingTop: 160 }}
      >
        <div className={`mx-auto`} style={{ maxWidth: 1366 }}>
          <div className="bg-white flex flex-col items-center lg:p-16 mx-auto pb-10 pt-8 px-4 text-white">
            <h1
              className={
                "bg-white font-bold leading-tight lg:w-3/4 px-1 py-8 rounded-t text-2xl text-center text-gray-900 w-full"
              }
            >
              Reset Password
            </h1>
            <div className={`mb-5 w-1/2`}>
              <TextInput
                className={`${
                   errors["email"] &&  errors["email"][0]
                    ? "border-red-300"
                    : "border-gray-400"
                } border-gray-400 border pl-2 py-2 rounded text-base text-gray-900`}
                border={false}
                name="email"
                placeholder={`Email`}
                autoComplete={`off`}
                onChange={e => handleInputChange("email", e)}
              />
              {errors["email"] && (
                <span className="px-1 text-red-400 tracking-widest text-xs ">
                  {errors["email"][0]}
                </span>
              )}
            </div>
            <div
              className={`${
                errors["password"] && errors["password"][0]
                  ? "border-red-300"
                  : "border-gray-400 mb-5"
              } w-1/2 border-2 rounded`}
            >
              <TextInput
                className={`appearance-none bg-transparent border-gray-400 border-r-0 font-hairline leading-relaxed pl-2 py-2 text-base text-gray-900 w-full`}
                border={false}
                name="password"
                type={`${showPassword ? "text" : "password"}`}
                placeholder={`Password`}
                autoComplete={`off`}
                suffix={
                  <span
                    className={`cursor-pointer mr-2`}
                    onClick={() => setShowPassword(!showPassword)}
                  >
                    <svg
                      className={`feather-icon h-6 opacity-50 text-gray-800 w-6 mt-2`}
                    >
                      <use
                        xlinkHref={`/assets/svg/feather-sprite.svg#${
                          showPassword ? "eye-off" : "eye"
                        }`}
                      />
                    </svg>
                  </span>
                }
                onChange={e => handleInputChange("password", e)}
              />
            </div>

            {errors["password"] && (
              <div className={`mb-5 w-1/2`}>
                <span className="px-1 text-red-400 tracking-widest text-xs">
                  {errors["password"][0]}
                </span>
              </div>
            )}

            <div
              className={`${
                errors["password_confirmation"] && errors["password_confirmation"][0]
                  ? "border-red-300"
                  : "border-gray-400 mb-5"
              } w-1/2 border-2 border-gray-400 rounded`}
            >
              <TextInput
                className={`appearance-none bg-transparent  border-gray-400 border-r-0 font-hairline leading-relaxed pl-2 py-2 text-base text-gray-900 w-full`}
                border={false}
                name="confirmPassword"
                type={`${showConfirmPassword ? "text" : "password"}`}
                placeholder={`Confirm Password`}
                autoComplete={`off`}
                suffix={
                  <span
                    className={`cursor-pointer mr-2`}
                    onClick={() => setShowConfirmPassword(!showConfirmPassword)}
                  >
                    <svg
                      className={`feather-icon h-6 opacity-50 text-gray-800 w-6 mt-2`}
                    >
                      <use
                        xlinkHref={`/assets/svg/feather-sprite.svg#${
                          showConfirmPassword ? "eye-off" : "eye"
                        }`}
                      />
                    </svg>
                  </span>
                }
                onChange={e => handleInputChange("confirmPassword", e)}
              />
            </div>
            {errors["password_confirmation"] && (
              <div className={`mb-5 w-1/2`}>
                <span className="px-1 text-red-400 tracking-widest text-xs">
                  {errors["password_confirmation"][0]}
                </span>
              </div>
            )}

            <div className={`flex items-center justify-end mb-5 w-1/2`}>
              <Button
                className={`font-bold rounded mr-3 bg-red-600`}
                disabled={loading}
                onClick={e => backToReset(e)}
              >
                Cancel
              </Button>
              <Button
                className={`font-bold rounded`}
                disabled={loading}
                onClick={e => requestPasswordReset(e)}
              >
                {loading && (
                  <FontAwesomeIcon
                    icon={faSpinner}
                    className={`fa-spin mr-2`}
                  />
                )}
                Reset Password
              </Button>
            </div>
          </div>
        </div>
      </section>
    </UserLayout>
  );
};

export default ResetPassword;
