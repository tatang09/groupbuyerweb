import React, { createRef, useState, useRef, useEffect } from "react";

import UserGlobal from "~/states/userGlobal";
import { axios } from "~/helpers/bootstrap";
import { Button, Form, TextInput } from "~/components/_base";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import Collapse from "@material-ui/core/Collapse";
// import swal from "sweetalert2";
import { sweetAlert } from "../components/_base/alerts/sweetAlert";
import Select from "react-select";
import { isEmpty } from "lodash";
import {
  fields,
  developerFields,
  descriptions,
  locations,
  prop_types,
  prices
} from "~/helpers/userAccountSettingsHelper/userAccountHelper";

import { fetchBanks } from "../data/index";

import { countries } from "~/helpers/countries";
import { states } from "~/helpers/propertyHelper/propertyHelper";

import { Helmet } from "react-helmet";
import { metaHelper } from "../helpers/metaHelper/metaHelper";
import { userRole } from "~/services/auth";

const UserAccountSettings = () => {
  const [userState, userAction] = UserGlobal();
  const [loading, setLoading] = useState(false);
  const [errors, setErrors] = useState({});
  const [isOpen, setIsOpen] = useState(false);
  const [propertyTypes, setPropertyTypes] = useState([]);
  const [devId, setDevId] = useState("");
  const [selectedState, setSelectedState] = useState([]);
  const [selectedCountry, setSelectedCountry] = useState([]);
  const [showOtherBankText, setShowOtherBankText] = useState(false);
  const [bankAccountName, setBankAccountName] = useState("");
  const [bankBSB, setBankBSB] = useState("");
  const [bankAccountNumber, setBankAccountNumber] = useState("");

  const [userPhone, setUserPhone] = useState("");
  const [devPhone, setDevPhone] = useState("");

  const [selectedLocation, setSelectedLocation] = useState([
    { value: "All", label: "All" }
  ]);
  const [selectedMaxPrice, setMaxPrice] = useState([
    { value: 50000, label: 50000 }
  ]);
  const [selectedMinPrice, setMinPrice] = useState([
    { value: 50000, label: 50000 }
  ]);
  const [selectedPropertyType, setSelectedPropertyType] = useState([
    { value: "All", label: "All" }
  ]);
  const [selectedDescription, setSelectedDescription] = useState([
    { value: "All", label: "All" }
  ]);
  const [propertyTypeArr, setPropertyTypeArr] = useState([]);

  const [profilePic, setProfilePic] = useState(
    "/assets/images/profile_default.jpg"
  );

  const [companyPic, setCompanyPic] = useState("/assets/images/no_logo.png");

  const form = createRef();

  const profileInput = useRef(null);
  const companyInput = useRef(null);

  const [selectedBank, setSelectedBank] = useState("");
  const [bankList, setBankList] = useState([]);

  const mapData = () => {
    if (userState.user && userState.user.location) {
      handleDataChange(JSON.parse(userState.user.location), "location");
    }

    if (userState.user && userState.user.min_price) {
      handleDataChange(JSON.parse(userState.user.min_price), "min_price");
    }

    if (userState.user && userState.user.max_price) {
      handleDataChange(JSON.parse(userState.user.max_price), "max_price");
    }

    if (userState.user && userState.user.best_describe) {
      handleDataChange(
        JSON.parse(userState.user.best_describe),
        "best_describe"
      );
    }

    if (userState.user && userState.user.propertyType) {
      handleDataChange(JSON.parse(userState.user.propertyType), "propertyType");
    }
  };

  useEffect(() => {
    getBanks();
  }, []);

  useEffect(() => {
    if (userState.user && userState.user.id) {
      getDeveloperAccount();
    }
    if (userState.user && userState.user.avatar_path) {
      setProfilePic(`${userState.user.avatar_path}`);
    }
    if (userState.user && userState.user.phone) {
      setUserPhone(userState.user["phone"].replace("+61", ""));
    }

    mapData();
  }, [userState.user]);

  const handleClick = isProfile => {
    if (isProfile) {
      profileInput.current.click();
    } else {
      companyInput.current.click();
    }
  };

  const handleFileChange = (e, isProfile) => {
    const imageFile = e.target.files[0];

    if (!imageFile.name.match(/\.(jpg|jpeg|png|gif)$/)) {
      sweetAlert("error", "Please select valid image.");
      return;
    }

    if (isProfile) {
      setProfilePic(URL.createObjectURL(imageFile));
    } else {
      setCompanyPic(URL.createObjectURL(imageFile));
    }
  };

  const getDeveloperAccount = async () => {
    const { data } = await axios.get(`/api/developer/${userState.user.id}`);
    mapDeveloperFields(data);
    setDevId(data.id || 0);
  };

  const getBanks = async () => {
    let url = "/api/bank";
    await fetchBanks({ url }).then(res => {
      let newBankList = [
        ...res.data.map(bank => {
          return {
            label: `${bank.name}`,
            value: String(bank.id)
          };
        })
      ];
      newBankList.push({ label: "Other", value: "Other" });
      setBankList(newBankList);
    });
  };

  const mapDeveloperFields = data => {
    if (isEmpty(data)) return;

    if (data.country) {
      setSelectedCountry({ value: data.country, label: data.country });
    }

    if (data.bank_id) {
      let bank = bankList.filter(bank => bank.value == data.bank_id.toString());
      setSelectedBank(bank[0]);
    }

    if (data.state) {
      setSelectedState({ value: data.state, label: data.state });
    }

    if (data.company_logo_path) {
      setCompanyPic(data.company_logo_path);
    }

    if (data.bank_account_name) {
      setBankAccountName(data.bank_account_name);
    }

    if (data.bank_bsb) {
      setBankBSB(data.bank_bsb);
    }

    if (data.bank_account_number) {
      setBankAccountNumber(data.bank_account_number);
    }

    Object.keys(developerFields).forEach(key => {
      let k = key.replace("dev_", "");

      if (data.hasOwnProperty(k)) {
        if (k === "phone") {
          developerFields[key].value = data[k]
            ? data[k].replace("+61", "")
            : "";
        } else {
          developerFields[key].value = data[k];
        }
      }
    });
  };

  const handleSubmit = async e => {
    e.preventDefault();
 
    if (
      companyPic.substring(0, 8) === "/assets/" &&
      userRole() !== "customer"
    ) {
      return sweetAlert("warning", "Company profile picture is required.");
    }

    setLoading(true);

    let errors = {};

    let formData = new FormData(form.current);

    let phone = userPhone ? "+61" + userPhone : "";

    formData.append("_method", "PATCH");

    formData.append("best_describe", JSON.stringify(selectedDescription));

    formData.append("location", JSON.stringify(selectedLocation));

    formData.append("max_price", JSON.stringify(selectedMaxPrice));

    formData.append("min_price", JSON.stringify(selectedMinPrice));

    formData.append("propertyType", JSON.stringify(selectedPropertyType));

    formData.append("user_id", userState.user.id);

    formData.append("user_type", userState.user.user_role);

    formData.set("phone", phone);

    formData.append("dev_state", selectedState.value || "");

    formData.append("dev_country", selectedCountry.value || "");

    let dev_phone = formData.get("dev_phone")
      ? "+61" + formData.get("dev_phone")
      : "";

    formData.set("dev_phone", dev_phone);

    if (!isEmpty(selectedBank) && selectedBank.value !== "Other") {
      formData.set("bank_id", selectedBank.value);
    }

    formData.append("devId", devId);

    try {
      let url = `/api/user/${userState.user.id}`;

      await axios.post(url, formData);

      sweetAlert("success", "Profile successfully updated.");

      userAction.setState({ fetch: !userState.fetch });

      setLoading(false);
    } catch (error) {
      let { data } = error.response;

      errors = data.errors;

      setLoading(false);

      if (errors && errors.avatar_path) {
        sweetAlert("error", errors.avatar_path[0]);

        userState.user.avatar_path
          ? setProfilePic(`${userState.user.avatar_path}`)
          : setProfilePic("/assets/images/profile_default.jpg");
      }
    }

    setErrors(errors || {});
  };

  if (userState.user) {
    Object.keys(fields).forEach(key => {
      if (key === "phone") {
        fields[key].value = userState.user[key]
          ? userState.user[key].replace("+61", "")
          : "";
      } else {
        if (key === "userEmail") {
          fields[key].value = userState.user["email"];
        } else {
          fields[key].value = userState.user[key];
        }
      }
      fields[key].id = userState.user[key];
    });
  }

  const userPreferenceDownFields = [
    {
      key: "best_describe",
      placeholder: "Select Buying Purpose",
      label: "Buying Purpose",
      value: selectedDescription,
      options: descriptions
    },
    {
      label: "Location",
      placeholder: "Select Location",
      key: "location",
      value: selectedLocation,
      options: locations
    },
    {
      label: "Type",
      placeholder: "Select Property Type",
      key: "type",
      value: selectedPropertyType,
      options: prop_types
    },
    {
      label: "Min Price",
      placeholder: "Select Min Price",
      key: "min_price",
      value: selectedMinPrice,
      options: prices
    },
    {
      label: "Max Price",
      placeholder: "Select Max Price",
      key: "max_price",
      value: selectedMaxPrice,
      options: prices
    }
  ];

  const handleDataChange = (selectedItems, key) => {
    switch (key) {
      case "location":
        if (
          !selectedItems ||
          selectedItems.length == 0 ||
          selectedItems[selectedItems.length - 1].value === "All"
        ) {
          setSelectedLocation([{ value: "All", label: "All" }]);
        } else {
          setSelectedLocation(
            selectedItems.filter(item => item.value !== "All")
          );
        }
        break;
      case "best_describe":
        if (
          !selectedItems ||
          selectedItems.length == 0 ||
          selectedItems[selectedItems.length - 1].value === "All"
        ) {
          setSelectedDescription([{ value: "All", label: "All" }]);
        } else {
          setSelectedDescription(
            selectedItems.filter(item => item.value !== "All")
          );
        }
        break;
      case "max_price":
        if (!selectedItems || selectedItems.length == 0) {
          setMaxPrice([{ value: 50000, label: 50000 }]);
        } else {
          setMaxPrice(selectedItems);
        }
        break;
      case "min_price":
        if (!selectedItems || selectedItems.length == 0) {
          setMinPrice([{ value: 50000, label: 50000 }]);
        } else {
          setMinPrice(selectedItems);
        }
        break;
      default:
        if (
          !selectedItems ||
          selectedItems.length == 0 ||
          selectedItems[selectedItems.length - 1].value === "All"
        ) {
          setSelectedPropertyType([{ value: "All", label: "All" }]);
        } else {
          setSelectedPropertyType(
            selectedItems.filter(item => item.value !== "All")
          );
        }
        break;
    }
  };

  const handleChange = (key, e) => {
    if (key === "dev_country") {
      setSelectedCountry(e);
    } else if (key === "dev_state") {
      setSelectedState(e);
    } else {
      if (e.value === "Other") {
        setShowOtherBankText(true);
      } else {
        setShowOtherBankText(false);
      }
      setSelectedBank(e);
    }
  };

  const dropDownComponent = (options, value, placeholder, key) => {
    return (
      <Select
        isMulti={key === "min_price" || key === "max_price" ? false : true}
        isClearable={false}
        closeMenuOnSelect={false}
        hideSelectedOptions={false}
        isSearchable={false}
        options={options}
        classNamePrefix={`input-select`}
        className={`gb-multi-select h-full w-full`}
        placeholder={placeholder}
        value={value}
        onChange={e => handleDataChange(e, key)}
      />
    );
  };

  const renderDropDown = () => {
    return userPreferenceDownFields.map(item => {
      return (
        <div
          className={`flex lg:flex-row flex-row mb-5 rounded text-base`}
          key={item.key}
        >
          <label className="capitalize mb-2 mr-4 mt-4 pr-3 w-40 lg:mb-0">
            <span className="font-bold">{item.label}</span>
          </label>
          <div className={`border-2 w-full flex items-center`}>
            {dropDownComponent(
              item.options,
              item.value,
              item.placeholder,
              item.key
            )}
          </div>
        </div>
      );
    });
  };

  return (
    <div>
      <Helmet>
        <meta name="description" content={metaHelper.desc} />
      </Helmet>
      <section
        className={`bg-palette-blue-dark`}
        style={{ marginTop: -130, paddingTop: 160 }}
      >
        <section className={`relative bg-white`}>
          <div
            className={`pb-16 px-6 lg:px-10 mx-auto`}
            style={{ maxWidth: 1366 }}
          >
            <h1
              className={`font-bold leading-tight text-4xl pt-10 pb-8 lg:pb-16 text-center`}
            >
              My Profile
            </h1>

            <form ref={form} onSubmit={handleSubmit}>
              <div className={`flex flex-col`}>
                <div
                  className={`w-full flex flex-col lg:flex-row lg:pr-16 mt-8 lg:mt-0`}
                >
                  <div className={`w-full mb-8 lg:mb-0 lg:w-1/2`}>
                    <div
                      className={`flex justify-center`}
                      //className={`lg:w-4/12`}
                    >
                      <input
                        type={`file`}
                        name={`avatar_path`}
                        className={`hidden`}
                        ref={profileInput}
                        onChange={e => handleFileChange(e, true)}
                      />
                      <div className="relative flex justify-center">
                        <div
                          className={`mx-auto flex items-center p-0 overflow-hidden border-4 border-palette-gray h-48 lg:h-56 rounded-full shadow-lg w-48 lg:w-56 `}
                          style={{ width: "fit-content" }}
                        >
                          <img
                            className={`object-cover h-48 w-48 lg:w-56 lg:h-56`}
                            src={profilePic}
                          />
                        </div>
                        <span
                          className={`
                          absolute bg-white border border-gray-300 bottom-0 cursor-pointer flex h-10 items-center justify-center mb-3 mr-3 right-0
                          rounded-full shadow-md w-10 transition-all duration-300 hover:bg-palette-purple hover:text-white hover:border-palette-purple
                        `}
                          onClick={() => handleClick(true)}
                        >
                          <svg className={`feather-icon`}>
                            <use
                              xlinkHref={`/assets/svg/feather-sprite.svg#edit-2`}
                            />
                          </svg>
                        </span>
                      </div>
                    </div>
                  </div>
                  <div className={`w-full lg:w-1/2`}>
                    <Form errors={errors} formFields={fields} />

                    {/* <div className={`mb-5 text-base flex`}>
                      <label className="mr-3 w-32 mt-2 capitalize pr-3">
                        <span className="font-bold">Phone</span>
                      </label>

                      <div className={`w-full flex flex-row w-full`}>
                        <div
                          className={`px-4 bg-gray-200 flex items-center justify-center rounded-l border-l border-t border-b border-gray-400`}
                        >
                          +61
                        </div>
                        <TextInput
                          placeholder={`Enter Phone`}
                          name={`phone`}
                          border={false}
                          appearance={true}
                          className={`rounded-l-none border-l-0`}
                          maxLength={`9`}
                          value={userPhone}
                          onChange={e => textHandleChange("userPhone", e)}
                        />
                      </div>
                    </div> */}

                    <div className={`mb-5 text-base flex`}>
                      <label className="mr-3 w-40 mt-2 capitalize pr-3">
                        <span className="font-bold">Password</span>
                      </label>

                      <div className={`w-full`}>
                        <div
                          onClick={() => setIsOpen(!isOpen)}
                          className={`
                            border border-palette-gray text-palette-gray hover:bg-palette-gray hover:text-white transition-all duration-300
                            cursor-pointer font-bold inline-block px-6 text-sm py-2 rounded shadow
                          `}
                        >
                          Change Password
                        </div>

                        <Collapse in={isOpen}>
                          {isOpen && (
                            <div>
                              <div className={`mt-5 text-sm`}>Old Password</div>
                              <TextInput
                                type={`password`}
                                placeholder={`Old Password`}
                                name={`old_password`}
                                border={false}
                                appearance={true}
                              />
                              {errors.old_password && (
                                <span className="text-red-500 text-xs">
                                  {errors.old_password[0]}
                                </span>
                              )}

                              <div className={`mt-5 text-sm`}>New Password</div>
                              <TextInput
                                type={`password`}
                                placeholder={`New Password`}
                                name={`password`}
                                border={false}
                                appearance={true}
                              />
                              {errors.password && (
                                <span className="text-red-500 text-xs">
                                  {errors.password[0]}
                                </span>
                              )}

                              <div className={`mt-5 text-sm`}>
                                Confirm Password
                              </div>
                              <TextInput
                                type={`password`}
                                placeholder={`Confirm Password`}
                                name={`password_confirmation`}
                                border={false}
                                appearance={true}
                              />
                            </div>
                          )}
                        </Collapse>
                      </div>
                    </div>

                    {!isEmpty(userState.user) &&
                      userState.user.user_role !== "project_developer" && (
                        <div className={`mx-auto`} style={{ maxWidth: 1366 }}>
                          <h1
                            className={`font-bold leading-tight lg:pb-16 ml-3 pb-8 pt-10 text-3xl text-center`}
                          >
                            My Preferences
                          </h1>

                          {/* <div className={`lg:mb-5 mb-2 text-base rounded flex`}>
                            <label className="capitalize flex-1 mt-4 w-32">
                              <span className="font-bold">
                                Buying Purpose?
                              </span>
                            </label>
                          </div> */}

                          {renderDropDown()}
                        </div>
                      )}
                  </div>
                </div>

                {userState.user && userState.user.user_role !== "customer" && (
                  <div className="w-full flex flex-col lg:flex-row">
                    <div className="w-full lg:w-1/2">
                      <h1
                        className={`font-bold leading-tight lg:pb-16 ml-3 pb-8 pt-10 text-3xl text-center`}
                      >
                        Seller Profile
                      </h1>
                    </div>
                  </div>
                )}

                <div
                  className={`w-full flex flex-col lg:flex-row lg:pr-16 mt-8 lg:mt-0`}
                >
                  <div className={`w-full mb-8 lg:mb-0 lg:w-1/2`}>
                    {userState.user && userState.user.user_role !== "customer" && (
                      <div
                        className={`flex justify-center`}
                        //className={`lg:w-4/12`}
                      >
                        <input
                          type={`file`}
                          name={`company_logo_path`}
                          className={`hidden`}
                          ref={companyInput}
                          onChange={e => handleFileChange(e, false)}
                        />
                        <div className="relative flex justify-center">
                          <div
                            className={`relative mx-auto flex items-center p-0 overflow-hidden border-4 border-palette-gray h-48 lg:h-56 shadow-lg w-48 lg:w-56 `}
                            style={{ width: "fit-content" }}
                          >
                            <img
                              className={`object-cover h-48 w-48 lg:w-56 lg:h-56`}
                              src={companyPic}
                            />
                          </div>
                          <span
                            className={`
                              absolute bg-white border border-gray-300 bottom-0 cursor-pointer flex h-10 items-center justify-center mb-3 mr-3 right-0
                              rounded-full shadow-md w-10 transition-all duration-300 hover:bg-palette-purple hover:text-white hover:border-palette-purple
                            `}
                            onClick={() => handleClick(false)}
                          >
                            <svg className={`feather-icon`}>
                              <use
                                xlinkHref={`/assets/svg/feather-sprite.svg#edit-2`}
                              />
                            </svg>
                          </span>
                        </div>
                      </div>
                    )}
                  </div>
                  <div className={`w-full lg:w-1/2`}>
                    {!isEmpty(userState.user) &&
                      userState.user.user_role !== "customer" && (
                        <>
                          <Form errors={errors} formFields={developerFields} />

                          <div className={`mb-5 text-base flex`}>
                            <label className="mr-3 w-40 mt-2 capitalize pr-3">
                              <span className="font-bold">Bank</span>
                            </label>

                            <div className={`w-full`}>
                              <div className={`flex`}>
                                <Select
                                  isOptionSelected
                                  name={`bank`}
                                  classNamePrefix={`input-select`}
                                  onChange={e => handleChange("bank", e)}
                                  className={`w-full`}
                                  placeholder={`Select Bank`}
                                  value={selectedBank}
                                  options={bankList}
                                />
                              </div>
                            </div>
                          </div>

                          <Collapse in={showOtherBankText}>
                            <div className={`mb-5 text-base flex`}>
                              <label className="mr-3 w-40 mt-2 capitalize pr-3">
                                <span className="font-bold"></span>
                              </label>

                              <div className={`w-full`}>
                                <div className={`flex`}>
                                  <TextInput
                                    type={`text`}
                                    placeholder={`Enter Bank Name`}
                                    name={`new_bank_name`}
                                    border={false}
                                    appearance={true}
                                  />
                                </div>
                              </div>
                            </div>
                          </Collapse>

                          <div className={`mb-5 text-base flex`}>
                            <label className="mr-3 w-40 mt-2 capitalize pr-3">
                              <span className="font-bold">Acct. Name</span>
                            </label>

                            <div className={`w-full`}>
                              <div className={`flex`}>
                                <TextInput
                                  type={`text`}
                                  placeholder={`Account Name`}
                                  name={`bank_account_name`}
                                  border={false}
                                  defaultValue={bankAccountName}
                                  appearance={true}
                                />
                                {errors.bank_account_name && (
                                  <span className="text-red-500 text-xs">
                                    {errors.bank_account_name[0]}
                                  </span>
                                )}
                              </div>
                            </div>
                          </div>

                          <div className={`mb-5 text-base flex`}>
                            <label className="mr-3 w-40 mt-2 capitalize pr-3">
                              <span className="font-bold">BSB</span>
                            </label>

                            <div className={`w-full`}>
                              <div className={`flex`}>
                                <TextInput
                                  type={`text`}
                                  placeholder={`Enter BSB`}
                                  name={`bank_bsb`}
                                  defaultValue={bankBSB}
                                  border={false}
                                  appearance={true}
                                />
                                {errors.bank_bsb && (
                                  <span className="text-red-500 text-xs">
                                    {errors.bank_bsb[0]}
                                  </span>
                                )}
                              </div>
                            </div>
                          </div>

                          <div className={`mb-5 text-base flex`}>
                            <label className="mr-3 w-40 mt-2 capitalize pr-3">
                              <span className="font-bold">Acct. No.</span>
                            </label>

                            <div className={`w-full`}>
                              <div className={`flex`}>
                                <TextInput
                                  type={`text`}
                                  placeholder={`Enter Account Number`}
                                  name={`bank_account_number`}
                                  defaultValue={bankAccountNumber}
                                  border={false}
                                  appearance={true}
                                />
                                {errors.bank_account_number && (
                                  <span className="text-red-500 text-xs">
                                    {errors.bank_account_number[0]}
                                  </span>
                                )}
                              </div>
                            </div>
                          </div>

                          <div className={`mb-5 text-base flex`}>
                            <label className="mr-3 w-40 mt-2 capitalize pr-3">
                              <span className="font-bold">State</span>
                            </label>

                            <div className={`w-full`}>
                              <div className={`flex`}>
                                <Select
                                  isOptionSelected
                                  name={`dev_state`}
                                  classNamePrefix={`input-select`}
                                  onChange={e => handleChange("dev_state", e)}
                                  className={`w-full`}
                                  placeholder={`Select State`}
                                  value={selectedState}
                                  options={states}
                                />
                              </div>
                            </div>
                          </div>

                          <div className={`mb-5 text-base flex`}>
                            <label className="mr-3 w-40 mt-2 capitalize pr-3">
                              <span className="font-bold">Country</span>
                            </label>

                            <div className={`w-full`}>
                              <div className={`flex`}>
                                <Select
                                  isOptionSelected
                                  name={`dev_country`}
                                  classNamePrefix={`input-select`}
                                  onChange={e => handleChange("dev_country", e)}
                                  className={`w-full`}
                                  placeholder={`Select Country`}
                                  value={selectedCountry}
                                  options={countries}
                                />
                              </div>
                            </div>
                          </div>
                        </>
                      )}
                  </div>
                </div>
              </div>

              <div
                className={`lg:px-16 flex justify-center lg:justify-end mt-8`}
              >
                <Button className={`font-bold rounded`} disabled={loading}>
                  {loading && (
                    <FontAwesomeIcon
                      icon={["fas", "spinner"]}
                      className={`fa-spin mr-2`}
                    />
                  )}
                  Save
                </Button>
              </div>
            </form>
          </div>
        </section>
      </section>
    </div>
  );
};

export default UserAccountSettings;
