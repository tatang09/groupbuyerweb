import React from "react";
import { Modal } from "~/components/_base";
import UserGlobal from "~/states/userGlobal";
import UserLayout from "~/layouts/users/UserLayout";
import { Helmet } from "react-helmet";
import { metaHelper } from "../helpers/metaHelper/metaHelper";

const PrivacyPolicy = ({ handleClose }) => {
  const [userState] = UserGlobal();

  return (
    // <Modal
    //   show={userState.showPrivacyPolicy}
    //   title={`Privacy Policy`}
    //   onClose={() => handleClose()}
    //   transition={`grow`}
    //   maxWidth={`md`}
    // >
    <UserLayout>
      <Helmet>
    
        <meta name="description" content={metaHelper.desc} />
   
      </Helmet>
      <section
        className="text-white bg-palette-blue-dark"
        style={{ marginTop: -130, paddingTop: 160 }}
      >
        <div className={`mx-auto`} style={{ maxWidth: 1366 }}>
          <div className="bg-palette-blue-dark lg:px-16 mx-auto pb-10 lg:pt-16 px-4 text-white">
            <h1
              className={"font-extrabold lg:text-5xl mb-5 text-3xl text-center"}
            >
              Privacy Policy
            </h1>
            <p className={`text-base`}>
              This Privacy Policy explains how GroupBuyer Pty Ltd ACN 641 556
              470 (GroupBuyer) deals with privacy issues relating to personal
              information which is provided by a person or entity (User) who
              uses the GroupBuyer website (Website).
            </p>
            <p className={`text-base`}>
              Please email GroupBuyer at info@groupbuyer.com.au if you have any
              queries about your Personal Information, how we deal with your
              Personal Information or would like to amend or otherwise delete
              your Personal Information.
            </p>
            <p className={`text-base`}>This Privacy Policy will describe:</p>
            <ul className={`list-disc pl-10`}>
              <li>
                <p className={`text-base`}>
                  How GroupBuyer will collect and handle a User’s personal
                  information;
                </p>
              </li>
              <li>
                <p className={`text-base`}>
                  The kinds of personal information that GroupBuyer will collect
                  and store;
                </p>
              </li>
              <li>
                <p className={`text-base`}>
                  The purpose for which GroupBuyer collects personal
                  information;
                </p>
              </li>
              <li>
                <p className={`text-base`}>
                  How the User can contact the data controller;
                </p>
              </li>
              <li>
                <p className={`text-base`}>
                  The use of cookies and third-party access on the GroupBuyer
                  Website.
                </p>
              </li>
            </ul>
            <p>
              <span className="mb-5 text-base">
                <strong>
                  <span className="underline">
                    The User’s Agreement to this Privacy Policy
                  </span>
                </strong>
                <p className="mt-5 text-base">
                  By using the GroupBuyer Website a User consents to the privacy
                  practices as disclosed in this Privacy Policy noting that
                  personal information about the User may be collected by
                  GroupBuyer or its Associates and relevant third-parties in
                  accordance with this Privacy Policy:
                </p>
              </span>
            </p>
            <ul className={`list-disc pl-10`}>
              <li>
                <p className={`text-base`}>
                  To contact the User in response to a query or feedback from
                  the User or due to the information provided by the User;
                </p>
              </li>
              <li>
                <p className={`text-base`}>
                  To enable GroupBuyer or an Associate to provide the User with
                  information;
                </p>
              </li>
              <li>
                <p className={`text-base`}>
                  To provide to GroupBuyer Associates and relevant third-parties
                  to facilitate an enquiry or submission regarding lost
                  property;
                </p>
              </li>
              <li>
                <p className={`text-base`}>
                  For marketing or promotional purposes;
                </p>
              </li>
              <li>
                <p className={`text-base`}>
                  To provide the User with quotes in relation to goods or
                  services;
                </p>
              </li>
              <li>
                <p className={`text-base`}>To verify a User’s identity;</p>
              </li>
              <li>
                <p className={`text-base`}>
                  For any other purpose at GroupBuyer’s sole discretion.
                </p>
              </li>
            </ul>
            <p>
              <span className="underline text-base">
                <strong>Personal Information</strong>
              </span>
            </p>
            <p className={`text-base`}>
              For the purposes of this Privacy Policy personal information
              includes the information that the User submits to GroupBuyer when
              the User creates a GroupBuyer account, enquiries and submissions
              regarding lost property, visits the GroupBuyer Website or contacts
              GroupBuyer by telephone, via mail, email or online, which includes
              but is not limited to the following information:
            </p>
            <ul className={`list-disc pl-10`}>
              <li>
                <p className={`text-base`}>The User’s full name;</p>
              </li>
              <li>
                <p className={`text-base`}>
                  The User’s payment details including banking or credit card
                  information;
                </p>
              </li>
              <li>
                <p className={`text-base`}>
                  The User’s telephone numbers and email address;
                </p>
              </li>
              <li>
                <p className={`text-base`}>The User’s home address;</p>
              </li>
              <li>
                <p className={`text-base`}>
                  Information which the User enters into the GroupBuyer Website
                  including but not limited to photographs, images or written
                  content.
                </p>
              </li>
            </ul>
            <p className={`text-base`}>
              <span className="underline">
                <strong>Collecting the User’s Personal Information</strong>
              </span>
            </p>
            <p className={`text-base`}>
              GroupBuyer will not collect personal information about a User
              without that User’s consent and will only collect that personal
              information which a User voluntarily provides to GroupBuyer.
            </p>
            <p className={`text-base`}>
              <span className={`text-base`}>
                <strong>Collecting the User’s Personal Information</strong>
              </span>
            </p>
            <p className={`text-base`}>
              GroupBuyer will take all reasonable steps to protect the User’s
              personal information from loss, misuse, and unauthorized access.
            </p>
            <p className={`text-base`}>
              GroupBuyer will also destroy or de-identify any personal
              information which belongs to a User if that User requests that
              GroupBuyer do so.
            </p>
            <p className={`text-base`}>
              <span className="underline">
                <strong>Use of Personal Information</strong>
              </span>
            </p>
            <p className={`text-base`}>
              GroupBuyer will use the User’s personal information for the
              primary purpose of providing the User with the service or offers
              in relation to the service.
            </p>
            <p className={`text-base`}>
              GroupBuyer will receive remuneration for the sale of User’s
              personal information and data to relevant third party businesses
              or participating suppliers.
            </p>
            <p className={`text-base`}>
              Other purposes for which the User’s personal information may be
              used include, but are not limited to:
            </p>
            <ul className={`list-disc pl-10`}>
              <li>
                <p className={`text-base`}>
                  Enhancing the security of both GroupBuyer and the User;
                </p>
              </li>
              <li>
                <p className={`text-base`}>
                  Providing GroupBuyer business partners or participating
                  suppliers with the User’s personal information so that they
                  may provide the User with information about their products and
                  services;
                </p>
              </li>
              <li>
                <p className={`text-base`}>Processing payments;</p>
              </li>
              <li>
                <p className={`text-base`}>
                  Contacting the User in respect of any queries or complaints;
                </p>
              </li>
              <li>
                <p className={`text-base`}>
                  For internal business management including accounting;
                </p>
              </li>
              <li>
                <p className={`text-base`}>
                  To advise the User of new product offering or promotions;
                </p>
              </li>
              <li>
                <p className={`text-base`}>
                  To advise the User about changes to the GroupBuyer Website’s
                  terms and conditions;
                </p>
              </li>
              <li>
                <p className={`text-base`}>
                  For legal proceedings if a dispute or claim arises as a result
                  of the User’s use of the GroupBuyer’s Website.
                </p>
              </li>
            </ul>
            <p className={`text-base`}>
              User’s personal information may be used by GroupBuyer and the
              GroupBuyer Website to make automated decisions.
            </p>
            <p className={`text-base`}>
              The User’s personal information is hosted by{" "}
              <strong>GroupBuyer</strong>
              <strong></strong> on servers located in Australia.{" "}
              <strong></strong>
            </p>
            <p className={`text-base`}>
              The Data Controller is GroupBuyer. The Data Controller can be
              contacted via email at <em>info@groupbuyer.com.au.</em>
            </p>
            <p className={`text-base`}>
              <span className="underline">
                <strong>
                  Use of personal information for research purposes
                </strong>
              </span>
            </p>
            <p className={`text-base`}>
              GroupBuyer may use a User’s de-identified personal information for
              research and development purposes without obtaining a User’s
              consent.
            </p>
            <p className={`text-base`}>
              <span className="underline">
                <strong>Individuals Right of Access</strong>
              </span>
            </p>
            <p className={`text-base`}>
              The User’s personal information is collected in accordance with
              the <em>Privacy Act 1988 </em>(Cth) and is held for two (2) years.
            </p>
            <p className={`text-base`}>
              A User can request to delete or amend any personal information
              stored by GroupBuyer or its Associates, and/or request a Subject
              Access Request (“SAR”) by contacting GroupBuyer via email at
              <a href="mailto:info@groupbuyer.com.au">
                <span className="text-sub-300"> info@groupbuyer.com.au</span>
              </a>
            </p>
            <p className={`text-base`}>
              GroupBuyer will take all reasonable steps to allow a User to
              access the User’s personal information unless such access would
              have an unreasonable impact on the privacy of other individuals
              and other Users.
            </p>
            <p className={`text-base`}>
              GroupBuyer reserves the right to amend and update this Privacy
              Policy at any time.
            </p>
            <p className={`text-base`}>
              A User can contact GroupBuyer at any time on{" "}
              <em>info@groupbuyer.com.au</em> to opt out of direct marketing or
              go to unsubscribe&nbsp;
              <a
                href="/"
                className={`hover:text-palette-teal text-bold underline`}
              >
                link
              </a>
            </p>
            <p className={`text-base`}>
              <span className="underline">
                <strong>Cookies and Usage Tracking</strong>
              </span>
              <span></span>
            </p>
            <p className={`text-base`}>
              GroupBuyer and any Associate or relevant third party may use small
              text files called “cookies”:
            </p>
            <ul className={`list-disc pl-10`}>
              <li>
                <p className={`text-base`}>
                  To enable more efficient browsing of the GroupBuyer Website;
                </p>
              </li>
              <li>
                <p className={`text-base`}>
                  To keep the User logged in to the website as they move from
                  page to page;
                </p>
              </li>
              <li>
                <p className={`text-base`}>
                  For statistical purposes, such as to determine which pages
                  visitors view most often, and to provide a better browsing
                  experience.
                </p>
              </li>
            </ul>
            <p className={`text-base`}>
              Cookies are stored on the User’s computer until they are deleted.
              User browser settings may be adjusted to refuse cookies from the
              website. However, please note that this may affect your ability to
              use the GroupBuyer Website.
            </p>
            <p className={`text-base`}>
              USE OF THIS SITE MEANS THAT YOU ACCEPT THE PRACTICES SET FORTH IN
              THIS POLICY. YOUR CONTINUED USE INDICATES YOUR AGREEMENT TO THE
              CHANGES.
            </p>
          </div>
        </div>
      </section>
    </UserLayout>
    // </Modal>
  );
};

export default PrivacyPolicy;
