import React from "react";
import UserLayout from "~/layouts/users/UserLayout";
import { Button } from "~/components/_base";
import { metaHelper } from "../helpers/metaHelper/metaHelper";
 
const AboutPage = () => {
  return (
    <UserLayout>
      <Helmet>
        <meta name="description" content={metaHelper.desc}/>
      </Helmet>
      <section
        className="text-white bg-palette-blue-dark pb-20"
        style={{ marginTop: -130, paddingTop: 160 }}
      >
        <div className="bg-cover bg-center relative" style={rowStyles.about}>
          <div className="mx-auto pb-16" style={{ maxWidth: 1280 }}>
            <div className="py-8">
              <h2 className="font-bold text-4xl text-center text-white">
                Helping everyday Aussies make home affordability more achievable
              </h2>
            </div>
            <div className="md:flex">
              <div className="flex-1">
                <img src="/assets/template/salaset.jpg" />
              </div>
              <div className="flex-1">
                <p className="font-bold leading-relaxed lg:px-16 md: py-6 text-left text-white text-xl">
                  We set out in 2019 to create a world-first discount
                  marketplace for property listings. We knew all too well the
                  gap between many Aussies wanting to buy a new home and that
                  dream being financially achievable.
                </p>
              </div>
            </div>
            <div className="md:flex">
              <div className="flex-1 my-10 lg:my-5">
                <p className="font-bold leading-relaxed lg:pb-10 lg:px-16 lg:py-6 text-white text-xl text-left">
                  Four founding partners with a combined 50 years experience
                  working with buyers and developers came together to design a
                  solution to help developers sell through their stock faster,
                  and buyers leverage the power of bulk buying for their
                  individual purchases.
                </p>
              </div>
              <div className="flex-1">
                <img src="/assets/template/people.jpg" />
              </div>
            </div>
            <div className="flex-row"></div>
          </div>
        </div>

        <div className={`text-center mt-10`}>
          <div className={`font-bold leading-tight text-4xl`}>
            <div>Access our projects</div>
          </div>
          <p className={`text-base py-3 mb-2`}>
            and see all the benefits with Group Buyer
          </p>
          <Button>Sign up for free</Button>
        </div>
      </section>
    </UserLayout>
  );
};

const rowStyles = {
  about: {
    background: `linear-gradient(to left, rgba(119, 0, 198, 0.9) 0%, rgba(0, 255, 255, 0.7) 100%), url('/assets/images/landing_page_how_it_works.jpg')`
  }
};

export default AboutPage;
