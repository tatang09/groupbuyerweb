import React, { useState } from "react";

import UserGlobal from "~/states/userGlobal";
import { isMobile } from "react-device-detect";

import UserLayout from "~/layouts/users/UserLayout";

import { makeStyles } from "@material-ui/core/styles";
import { Helmet } from "react-helmet";
import { metaHelper } from "../helpers/metaHelper/metaHelper";

const useStyles = makeStyles(theme => ({
  root: {
    "& > *": {
      marginTop: theme.spacing(2)
    }
  }
}));

const ContactThankYouPage = () => {
  window.setTimeout(function() {
    window.location = "/";
  }, 5000);

  return (
    <UserLayout topFooter={false}>
      <Helmet>
      
        <meta name="description" content={metaHelper.desc} />
     
      </Helmet>
      <section
        className="text-white bg-palette-blue-dark"
        style={{ marginTop: -130, paddingTop: 160 }}
      >
        <div className="bg-gray-200 flex flex-col items-center lg:p-16 mx-auto pb-10 pt-8 px-4 text-white">
          <img
            src="/assets/images/undraw_message_sent_1030.svg"
            height="250px"
            width="420px"
            style={{
              marginTop: "24px"
            }}
          />

          <h1
            className={
              "font-bold lg:w-3/4 w-full px-1 rounded-t text-3xl text-4xl text-center text-gray-900"
            }
          >
            Thank you for contacting GroupBuyer.
          </h1>
          <h1
            className={
              "font-bold lg:w-3/4 w-full px-1 rounded-t text-3xl text-4xl text-center text-gray-900"
            }
          >
            We will respond as soon as possible.
          </h1>

          <h4
            className={
              "lg:w-3/4 w-full py-8 rounded-t text-2xl text-center text-gray-900"
            }
          >
            If page does not redirect you back to the homepage in 5 seconds,{" "}
            <u
              onClick={() => (window.location = "/")}
              style={{ cursor: "pointer" }}
            >
              click here.
            </u>
          </h4>
        </div>
      </section>
    </UserLayout>
  );
};

export default ContactThankYouPage;
