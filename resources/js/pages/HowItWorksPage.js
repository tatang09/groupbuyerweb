import React, { useState } from "react";

import UserLayout from "~/layouts/users/UserLayout";
import UserGlobal from "~/states/userGlobal";
import HeroSlider, { Slide, Nav, SideNav, OverlayContainer } from "hero-slider";
import { Helmet } from "react-helmet";
import { metaHelper } from "../helpers/metaHelper/metaHelper";
import Button from '@material-ui/core/Button';
import { CurrentPopularDeals, TheMore, HowItWorksComponent } from "~/components";
import { JoinUs } from "~/components";


const HowItWorksPage = () => {
  const [userState, userAction] = UserGlobal();
  const [showWork, setShowWork] = useState(false);

  const nextSlideHandler = React.useRef();
  const previousSlideHandler = React.useRef();

  const handleClose = () => {
    setShowWork(false);
  };

  return (
    <UserLayout>
      <Helmet>

        <meta name="description" content={metaHelper.desc} />

      </Helmet>

      <section>
        <div className="lg:inline-flex" id="HowItWorksHero">
          <div
            className={`bg-cover lg:bg-center md:bg-center bg-no-repeat relative lg:w-full`}
            style={{
              background:
                "url('/assets/images/howitworks-hero.png')",
            }}
            id="imageSize-HIW-main"
          >
          </div>

          <div>
            <div className={"flex-1 flex text-white flex-col bg-palette-violet-main-dark"} id="heroSectionContent">
              <div className={"lg:ml-24 lg:mt-48 lg:mr-48 lg:mb-32 xs:mt-8 xs:ml-8 xs:mr-8 xs:mb-12 2xl:ml-32"} id="heroSectionPosition">
                <p className="font-bold tracking-wider uppercase xs:text-sm text-palette-purple-main">Ben Daniel's Vision</p>
                <span className="pb-4 mb-4 font-extrabold leading-none lg:align-bottom lg:text-6xl xs:text-5xl xs:mr-4" id="heroSectionMainHeading">
                  We believe in <br />a fair-go for Australians
                </span>
                <p className={"mt-6 text-base font-light 2xl:font-hairline"} id="heroSectionSubHeading">
                  We believe everyone deserves to own their own home and benefit from it. We wanted property to be more affordable and accessible to everyday Aussies, not just those with higher incomes. GroupBuyer is a win/win solution where vendors can sell faster in bulk and buyers can purchase below market value.
                </p>
              </div>
            </div>
          </div>
        </div>

      </section>

      <TheMore bgColor={`#ffffff`} txtColor={`text-black`} />
      <section>
        <HowItWorksComponent />
      </section>

        <section>
          <div className={"xs:hidden py-10"} style={{background:"linear-gradient(rgb(255 255 255 / 98%), rgb(255 255 255 / 98%)) 0% 0% / cover, url(/assets/images/benefits_for_buyer.png) 0% 0% no-repeat padding-box padding-box rgba(255, 255, 255, 0)", backgroundSize:"cover", backgroundPosition:"center"}}>
            <div className={"w-100 text-center flex flex-col w-full items-center mt-4"}>
              <span className={"font-bold text-palette-purple-main lg:text-7xl"}>How we proceed</span>
              <p className={"text-base xs:pt-4 xs:pb-4 text-palette-violet-main-light"}>We are transparent with our process because we believe great relationships are formed on the foundation of trust.</p>
              <img src="assets/images/how-it-works_how-we-proceed.png" className="px-48 py-16"/>
            </div>
          </div>
      </section>

        <section id="howitworks-how-we-proceed" className={"lg:hidden md:hidden"} style={{background:"linear-gradient(rgb(255 255 255 / 98%), rgb(255 255 255 / 98%)) 0% 0% / cover, url(/assets/images/benefits_for_buyer.png) 0% 0% no-repeat padding-box padding-box rgba(255, 255, 255, 0)", backgroundSize:"cover", backgroundPosition:"center"}}>
            <div className={"w-100 text-center flex flex-col w-full items-center xs:py-6 w-100 xs:px-8 xs:pb-4"}>
              <h2 className="text-base font-bold leading-none text-palette-purple-main xs:text-5xl">How we proceed</h2>
              <p className={"text-base xs:pt-4 xs:pb-4 text-palette-violet-main-light"}>We are transparent with our process because we believe great relationships are formed on the foundation of trust.</p>
            </div>
            <HeroSlider
              nextSlide={nextSlideHandler}
              orientation="horizontal"
              initialSlide={1}
              style={{}}
              settings={{
                shouldAutoplay: false,
                shouldDisplayButtons: false,
                height: "70vh",
                slidingDuration: 250,
                slidingDelay: 100,
              }}
            >
              <Slide>
                <div className={"text-white ml-10 mr-10 mb-10 rounded-md"} style={{backgroundColor: '#443466'}}>
                  <h2 className={"font-extrabold text-base xs:text-4xl text-center pt-4"}>Stage 1</h2>
                  <div className={"text-white pb-2 ml-8 mr-4 justify-center"}>
                    <p className={"font-normal pt-4 text-palette-purple-main text-base"}>GroupBuyer selects properties</p>
                    <span className={"font-extralight"}>
                      GroupBuyer does its due diligence and considers the following factors before listing a property online:
                    </span>
                  </div>
                  <div className="justify-center pt-2 ml-4 text-base">
                    <ul>
                      <li><span className="px-2 font-light xs:ml-2 text-palette-purple-main">-</span>Developes</li>
                      <li><span className="px-2 font-light xs:ml-2 text-palette-purple-main" >-</span> Architect/Builder</li>
                      <li><span className="px-2 font-light xs:ml-2 text-palette-purple-main" >-</span>Location/Neighbourhood</li>
                      <li><span className="px-2 font-light xs:ml-2 text-palette-purple-main" >-</span>Local Council</li>
                      <li><span className="px-2 font-light xs:ml-2 text-palette-purple-main" >-</span>Infrastructure (eg. Nearby Transport, Schools, CBD, etc)</li>
                    </ul>
                  </div>
                  <div className={"py-4"}>
                      <Button
                        style={{
                        marginLeft: "13.5rem",
                      }}
                        onClick={() => nextSlideHandler.current()}
                      >
                        <img src={"assets/svg/Group 179.svg"} style={{width: "60%"}}/>
                      </Button>
                  </div>
                </div>
              </Slide>

              <Slide>
                <div className={"text-white ml-10 mr-10 mb-10 rounded-md"} style={{backgroundColor: '#443466'}}>
                  <h2 className={"font-extrabold text-base xs:text-4xl text-center pt-4"}>Stage 2</h2>
                  <div className={"text-white pb-2 ml-8 mr-4 justify-center"}>
                    <p className={"font-normal pt-4 text-palette-purple-main text-base"}>GroupBuyer set-up Deals</p>
                    <span className={"font-extralight"}>
                      Each property is qualified by price, size & future demand. We tick all the boxes to ensure you're buying a quality property that will increase in value over time.
                    </span>
                  </div>
                  <div className="justify-center pt-2 ml-4 text-base">
                    <ul>
                      <li><span className="px-2 font-light xs:ml-2 text-palette-purple-main">-</span>Developes</li>
                      <li><span className="px-2 font-light xs:ml-2 text-palette-purple-main" >-</span> Architect/Builder</li>
                      <li><span className="px-2 font-light xs:ml-2 text-palette-purple-main" >-</span>Location/Neighbourhood</li>
                      <li><span className="px-2 font-light xs:ml-2 text-palette-purple-main" >-</span>Local Council</li>
                      <li><span className="px-2 font-light xs:ml-2 text-palette-purple-main" >-</span>Infrastructure (eg. Nearby Transport, Schools, CBD, etc)</li>
                    </ul>
                  </div>
                  <div className={"py-4"}>
                      <Button
                        style={{
                        marginLeft: "13.5rem",
                      }}
                        onClick={() => nextSlideHandler.current()}
                      >
                        <img src={"assets/svg/Group 179.svg"} style={{width: "60%"}}/>
                      </Button>
                  </div>
                </div>
              </Slide>

              <Slide>
                <div className={"text-white ml-10 mr-10 mb-10 rounded-md"} style={{backgroundColor: '#443466'}}>
                  <h2 className={"font-extrabold text-base xs:text-4xl text-center pt-4"}>Stage 3</h2>
                  <div className={"text-white pb-2 ml-8 mr-4 justify-center"}>
                    <p className={"font-normal pt-4 text-palette-purple-main text-base"}>GroupBuyer gives you access to deals</p>
                    <span className={"font-extralight"}>
                      Once a project is live online it's over to you. Available property deals are on a "first-in-best-dressed" basis and can not be held without a $1000 deposit.
                    </span>
                  </div>
                  <div className="justify-center pt-2 ml-4 text-base">
                    <ul>
                      <li><span className="px-2 font-light xs:ml-2 text-palette-purple-main">-</span>Developes</li>
                      <li><span className="px-2 font-light xs:ml-2 text-palette-purple-main" >-</span> Architect/Builder</li>
                      <li><span className="px-2 font-light xs:ml-2 text-palette-purple-main" >-</span>Location/Neighbourhood</li>
                      <li><span className="px-2 font-light xs:ml-2 text-palette-purple-main" >-</span>Local Council</li>
                      <li><span className="px-2 font-light xs:ml-2 text-palette-purple-main" >-</span>Infrastructure (eg. Nearby Transport, Schools, CBD, etc)</li>
                    </ul>
                  </div>
                  <div className={"py-4"}>
                      <Button
                        style={{
                        marginLeft: "13.5rem",
                      }}
                        onClick={() => nextSlideHandler.current()}
                      >
                        <img src={"assets/svg/Group 179.svg"} style={{width: "60%"}}/>
                      </Button>
                  </div>
                </div>
              </Slide>


            </HeroSlider>
        </section>

      <JoinUs />

      <CurrentPopularDeals />

    </UserLayout>
  );
};



export default HowItWorksPage;
