import React, { useEffect, useState } from 'react';
import UserLayout from "~/layouts/users/UserLayout";
import { TheMore, PieceMeal, RegistrationForm, HowItWorksComponent } from '~/components';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { isMobile } from 'react-device-detect';
import Modal from '@material-ui/core/Modal';

const triple_chevron_down_purple =
    "/assets/images/triple-chevron-down-purple.svg";

const SiteUnderConstruction = () => {

    const [modal, setModal] = useState(false);
    const [showButton, setShowButton] = useState(isMobile ? true : false)

    useEffect(() => {
        window.addEventListener('scroll', function (e) {
            let mobileJoinUsFooter = document.getElementById('footerBottom');

            if (e.target.scrollTop > (e.target.scrollHeight - (e.target.clientHeight + 200))) {
                mobileJoinUsFooter.classList.remove('block')
                mobileJoinUsFooter.classList.add('hidden')
            } else {
                mobileJoinUsFooter.classList.remove('hidden')
                mobileJoinUsFooter.classList.add('block')
            }
        }, true);
    }, []);

    const handleCloseModal = () => {
        setModal(false);
    };

    const handleOpenModal = () => {
        console.log("clicked");
        setModal(true);
    };

    const scrollToBottom = () => {
        let div = document.getElementById("be-in-the-loop");
        div.scrollIntoView({
            behavior: 'smooth'
        });
    }

    const handlePageScroll = (event) => {
        console.log(event)
    }

    document.onscroll = function (event) {
        console.log(event)
        if (window.innerHeight + window.scrollY > document.body.clientHeight) {
            document.getElementById('footerBottom').style.display = 'none';
        }
    }
    return (
        <UserLayout onScroll={handlePageScroll}>
            <section style={{
                backgroundImage: `linear-gradient(rgba(0, 0, 0, 0.7),
                        rgba(0, 0, 0, 0.7)), url('/assets/images/construction.png')`,
                backgroundSize: "cover",
                backgroundPosition: "top",
                backgroundRepeat: "no-repeat"
            }}
                className={"h-full items-end text-white flex w-full relative"}>
                <div
                    className={`banner-container -m-1 flex flex-col h-full items-center justify-center lg:mt-56 mb-20 mt-48 relative w-full z-10`}
                >
                    <h2 className="full-website-coming-soon font-extrabold font-light lg:mb-3 lg:text-lg lg:w-1/2 lg:w-full text-center text-palette-purple-main text-xs w-1/4">
                        FULL WEBSITE COMING SOON
                        </h2>
                    <span
                        className={`banner-slogan font-bold leading-none lg:text-8xl mt-5 text-5xl text-center text-white`}
                    >
                        Your success <br />
                            begins here!
                        </span>
                    <p className={`under-construction-banner-message lg:my-0 lg:my-12 lg:px-24 lg:text-base lg:w-2/4 mb-0 my-10 px-12 text-center `}>
                        This June 30th, we are bringing to you Australia's first property e-commerce platform offering access to the most exclusive deals, all from trusted developers at unbeatable prices. Join the GroupBuyer community now and get VIP access to exciting benefits exclusive to early members.
                    </p>

                    <button
                        className={
                            "font-light hidden lg:block max-w-xs mb-16 mt-4 py-2 rounded-sm text-base text-center text-white w-full"
                        }
                        onClick={scrollToBottom}
                        style={{ backgroundColor: "#E91AFC" }}
                    >
                        Join us for free
                        </button>

                    <h2 className="learn-more mb-3 font-light text-white xs:mt-0 lg:mt-16 text-base md:block hidden">
                        Learn more
                    </h2>
                    <a href="#how-it-works" className={`md:block hidden`}>
                        <img src={triple_chevron_down_purple} className={"w-6"} />
                    </a>
                </div>
            </section>
            { !isMobile && <TheMore bgColor={"#4D0B98"} txtColor={`text-white`} />}
            <section className="h-full">
                <img src={`/assets/images/test.jpg`} className={`w-full`} />
            </section>
            <section id="how-it-works">
                <HowItWorksComponent />
            </section>
            { !isMobile && <PieceMeal />}
            { isMobile && <TheMore bgColor={"#ffffff"} txtColor={`text-gray-900`} />}
            <section id="join-us-section">
                <div className={"flex flex-col pt-12 pb-32 px-32 xs:pt-10 items-center xs:px-4 xs:pb-10 xs:bg-right"} style={{
                    backgroundImage: `linear-gradient(#3c0e73e6, #3c0e73e6),
                         url(${isMobile ? '/assets/images/buyer_benefits.png' : '/assets/images/site_construciotn_join_us.jpg'})`,
                    backgroundSize: "cover", backgroundPositionX: "center", backgroundPositionY: "bottom"
                }}>
                    {/* <div className={"flex flex-col pt-12 pb-32 px-32 xs:pt-10 items-center xs:px-4 xs:pb-10 xs:bg-right"} style={{ background: `url(${isMobile ? '/assets/images/buyer_benefits.png' : '/assets/images/site_construciotn_join_us.jpg'})`, backgroundSize: "cover", backgroundPositionX: "right", backgroundPositionY: "bottom" }}> */}
                    <div className={"md:flex lg:flex"}>
                        <div className={"flex-1 p-6 xs:p-1"}>
                            <div className={"flex flex-col items-center px-24 py-12 rounded-md xs:pb-10 xs:pt-2 xs:px-2"}>
                                <p className={"lg:text-7xl text-white text-4xl lg:text-5xl font-bold lg:text-center m-0 p-0 leading-10 whitespace-no-wrap"}>Join us for free and</p>
                                <p className={"font-bold lg:text-5xl lg:text-7xl lg:text-center m-0 p-0 text-4xl text-center text-white whitespace-no-wrap"}>stay in the loop</p>
                                <p className={"text-palette-purple-main xs:text-sm lg:text-lg text-center pb-8 mt-3"}>A new and innovative platform to access properties from trusted developers.</p>
                                <span
                                    className={`flex font-light items-center justify-center leading-normal px-8 text-base text-center text-white`}
                                >
                                    <p className={`lg:mb-4 mb-5 text-justify`}>
                                        Join a community of people getting great deals on properties. The larger we are the better the deals become. Sign up to be notified of Australia’s best property deals.
                            </p>
                                </span>
                            </div>
                        </div>
                    </div>
                    <div id="be-in-the-loop" className={"md:flex lg:block hidden"}>
                        <RegistrationForm
                            containerRounded={true}
                            checkIconColor={`text-white`}
                            containerBgColor={`rgb(255, 255, 255, 0.2)`}
                        />
                    </div>
                </div>
                <div id={`footerBottom`} className={`pt-6 pb-10 block fixed flex items-center justify-center lg:hidden w-full`} style={{ bottom: "0px", background: "linear-gradient(transparent, rgb(12 12 12 / 50%) 15%,rgb(12 12 12 / 70%) 40%, rgb(0 0 0 / 90%))" }}>
                    <button
                        className={
                            "font-light max-w-xs mt-4 py-2 rounded-sm text-base text-center text-white w-full"
                        }
                        onClick={handleOpenModal}
                        style={{ backgroundColor: "#E91AFC" }}
                    >
                        Join us for free
                </button>
                </div>
                <Modal
                    aria-labelledby="simple-modal-title"
                    aria-describedby="simple-modal-description"
                    open={modal}

                    // centered
                    style={{ display: 'flex', alignItems: 'center', justifyContent: 'center' }}
                    onClose={handleCloseModal}
                >
                    <div style={{ zoom: '0.85' }}>
                        <RegistrationForm
                            containerBgColor="#fff"
                            containerRounded={true}
                            displayCloseButton={true}
                            rightPadded={false}
                            subHeader="JOIN US FOR FREE"
                            textColorTheme="dark"
                            inputBgColor="#dadada5c"
                            headerTop="Create"
                            headerBottom="account"
                            handleCloseModal={handleCloseModal}
                            opacity={"100%"}
                            isPopup={true}
                            subHeaderColor="text-gray-900"
                        />
                    </div>
                </Modal>
            </section>
        </UserLayout >);
}

export default SiteUnderConstruction;
