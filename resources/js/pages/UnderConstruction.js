import { Divider } from "@material-ui/core";
import React from "react";
import { ContactForm } from "~/components";
import UserLayout from "~/layouts/users/UserLayout";

const UnderConstruction = () => {
  return (
    <UserLayout>
      <div
        className="bg-white flex flex-col items-center lg:flex-row mx-auto mx-10"
        style={{ height: "100%" }}
      >
        <div
          className="flex flex-1 flex-col h-full items-center justify-center"
          style={{ minHeight: "500px" }}
        >
          <img
            className="lg:mt-0 mt-32"
            src={`/assets/images/LOGO.png`}
            // className={`object-cover`}
            style={{ height: 120, width: 'auto' }}
          />
          <h1 className="font-bold text-3xl text-center text-gray-800 text-palette-purple">
            Coming Soon
          </h1>
          <div className="flex justify-center">
            <img
              className="lg:my-10 my-6 px-6"
              src={`/assets/svg/undraw_under_construction_46pa.svg`}
              // className={`object-cover`}
              style={{ height: 250 }}
            />
          </div>
          <p className="font-semibold lg:mx-24 lg:mx-4 mx-8 text-base text-center text-gray-800">
            GroupBuyer is currently under construction and will return very soon
            with more of Australia's best property deals. Join us for free and
            stay in the loop with our upcoming launch.
          </p>
        </div>
        <div
          className="flex flex-1 flex-col h-full items-center justify-center w-full pt-16 px-5 sm:pt-0 w-full"
          style={{ minHeight: "700px" }}
        >
          <div className="flex-col pt-16 px-5 sm:pt-0 w-full">
            <p className="font-semibold  px-6 text-2xl text-center text-gray-800">
              Questions? Send us an enquiry
            </p>
            <ContactForm backgroundColor={"#000024"} />
          </div>
        </div>
      </div>
    </UserLayout>
  );
};

export default UnderConstruction;
