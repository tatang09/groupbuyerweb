import React from "react";

const TotalSize = ({ apartment }) => {
  const getTotalSize = data => {
    if (data) {
      return data.internal_size + data.external_size + data.parking_size;
    }
    return 0;
  };

  return (
    <div className={`mt-10 border-gray-500 border-b-2 pb-8`}>
      <span className={`text-base font-bold`}>
        Total Size: {`${getTotalSize(apartment)}sqm`}
      </span>
      <div className={`flex flex-col lg:flex-row items-center mt-4`}>
        <div
          className={`flex-1 font-semibold text-base text-gray-700`}
        >{`Internal: ${
          apartment.internal_size ? apartment.internal_size : 0
        }sqm`}</div>

        <div
          className={`flex-1 font-semibold text-base text-gray-700`}
        >{`External: ${
          apartment.external_size ? apartment.external_size : 0
        }sqm`}</div>

        <div
          className={`flex-1 font-semibold text-base text-gray-700`}
        >{`Parking: ${
          apartment.parking_size ? apartment.parking_size : 0
        }sqm`}</div>
      </div>
    </div>
  );
};

export default TotalSize;
