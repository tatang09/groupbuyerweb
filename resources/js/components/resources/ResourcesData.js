import React from "react";
import { tableRows } from "../../helpers/resourceHelper/index";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faTimes } from "@fortawesome/free-solid-svg-icons";
 
import { TextInput } from "~/components/_base";

const ResourcesData = ({
  builderProfile,
  devProfile,
  arcProfile,
  handleDelete,
  handleFileChange,
  changeInputType,
  setCheckedState,
  getFileName,
  setProjectVideo,
  projectVideoDefaultValue="",
  setDevProfileLink,
  devProfileLink=""
}) => {
  return tableRows.map((row, key) => {
    return (
      <React.Fragment key={key}>
        <tr>
          <td
            className={`pt-4 lg:px-4 w-1/2 ${
              row.label === "Builder Profile" ||
              row.label === "Architect Profile" ||
              row.label === "Developer Profile"
                ? "flex w-full"
                : "w-1/2"
            } `}
          >
            <span className={`block font-semibold  mb-4`}>{row.label}</span>
            {(row.label === "Builder Profile" ||
              row.label === "Architect Profile" ||
              row.label === "Developer Profile") && (
              <div className={`ml-40`}>
                <input
                  className={`${row.label === "Builder Profile" ? "ml-5" : ""}
                          ${row.label === "Architect Profile" ? "ml-3" : ""}`}
                  type="checkbox"
                  checked={setCheckedState(row)}
                  onChange={e => {
                    changeInputType(row.label, e.target.checked);
                  }}
                />
                <span className={`p-1`}>Site URL</span>
              </div>
            )}
          </td>
        </tr>
        <tr>
          {row.label === "Builder Profile" && (
            <td className={`pb-4 lg:px-4 pr:4 pr-2 w-1/2 `}>
              {builderProfile === "file" ? (
                <label>
                  <div
                    className="py-2 px-4 cursor-pointer w-full "
                    style={{
                      border: "2px dashed #ccc"
                    }}
                  >
                    Select File...
                    <input
                      name={row.resource}
                      className="block hidden w-full"
                      type="file"
                      onChange={e => {
                        handleFileChange(e, row.resource);
                        // e.target.value = null;
                      }}
                    />
                  </div>
                </label>
              ) : (
                <TextInput
                  className={`border-gray-400 border pl-2 py-1`}
                  border={false}
                  width="w-full"
                  placeholder={`https://`}
                  onChange={e => {
                    setBuilderProfileLink(e.target.value);
                  }}
                  // defaultValue={builderProfileLink}
                  // name="builderProfileLink"
                />
              )}
            </td>
          )}
          {row.label === "Developer Profile" && (
            <td className={`pb-4 lg:px-4 pr:4 pr-2 w-1/2 `}>
              {devProfile === "file" ? (
                <label>
                  <div
                    className="py-2 px-4 cursor-pointer w-full "
                    style={{
                      border: "2px dashed #ccc"
                    }}
                  >
                    Select File...
                    <input
                      name={row.resource}
                      className="block hidden w-full"
                      type="file"
                      onChange={e => {
                        handleFileChange(e, row.resource);
                        // e.target.value = null;
                      }}
                    />
                  </div>
                </label>
              ) : (
                <TextInput
                  className={`border-gray-400 border pl-2 py-1`}
                  border={false}
                  width="w-full"
                  placeholder={`https://`}
                  onChange={e => {
                    setDevProfileLink(e.target.value);
                  }}
                  defaultValue={devProfileLink}
                />
              )}
            </td>
          )}
          {row.label === "Architect Profile" && (
            <td className={`pb-4 lg:px-4 pr-4 pr-2 w-1/2 `}>
              {arcProfile === "file" ? (
                <label>
                  <div
                    className="py-2 px-4 cursor-pointer w-full "
                    style={{
                      border: "2px dashed #ccc"
                    }}
                  >
                    Select File...
                    <input
                      name={row.resource}
                      className="block hidden w-full"
                      type="file"
                      onChange={e => {
                        handleFileChange(e, row.resource);
                        // e.target.value = null;
                      }}
                    />
                  </div>
                </label>
              ) : (
                <TextInput
                  className={`border-gray-400 border pl-2 py-1`}
                  border={false}
                  width="w-full"
                  placeholder={`https://`}
                  onChange={e => {
                    setArcProfileLink(e.target.value);
                  }}
                />
              )}
            </td>
          )}
          {row.label === "Project Video" && (
            <td className={`pb-4 lg:px-4 pr:4 pr-2 w-1/2 `}>
              <TextInput
                className={`border-gray-400 border pl-2 py-1`}
                border={false}
                width="w-full"
                // name="projectVideo"
                onChange={(e) => setProjectVideo(e.target.value)}
                defaultValue={projectVideoDefaultValue}
              />
            </td>
          )}
          {row.label !== "Architect Profile" &&
            row.label !== "Builder Profile" &&
            row.label !== "Project Video" &&
            row.label !== "Developer Profile" && (
              <td className={`pb-4 lg:px-4 pr:4 pr-2 w-1/2 `}>
                <label>
                  <div
                    className="py-2 px-4 cursor-pointer w-full "
                    style={{
                      border: "2px dashed #ccc"
                    }}
                  >
                    Select File...
                    <input
                      name={row.resource}
                      className="block hidden w-full"
                      type="file"
                      onChange={e => {
                        handleFileChange(e, row.resource);
                        // e.target.value = null;
                      }}
                    />
                  </div>
                </label>
              </td>
            )}

          {row.label !== "Architect Profile" &&
            row.label !== "Builder Profile" &&
            row.label !== "Developer Profile" &&
            row.label !== "Project Video" && (
              <>
                <td className={`pb-4 w-4/12 text-center `}>
                  <span className={`pt-5 text-palette-blue-light`}>
                    {getFileName(row.resource)}
                  </span>
                </td>
                <td className={`pb-4 w-1/12 text-center cursor-pointer`}>
                  <FontAwesomeIcon
                    className={`text-lg text-red-500 hover:text-red-400`}
                    onClick={e => {
                      handleDelete(e, row.resource);
                    }}
                    icon={faTimes}
                  />
                </td>
              </>
            )}

          {row.label === "Developer Profile" && devProfile === "file" && (
            <>
              <td className={`pb-4 w-4/12 text-center `}>
                <span className={`pt-5 text-palette-blue-light`}>
                  {getFileName(row.resource)}
                </span>
              </td>
              <td className={`pb-4 w-1/12 text-center cursor-pointer`}>
                <FontAwesomeIcon
                  className={`text-lg text-red-500 hover:text-red-400`}
                  onClick={e => {
                    handleDelete(e, row.resource);
                  }}
                  icon={faTimes}
                />
              </td>
            </>
          )}

          {row.label === "Architect Profile" && arcProfile === "file" && (
            <>
              <td className={`pb-4 w-4/12 text-center `}>
                <span className={`pt-5 text-palette-blue-light`}>
                  {getFileName(row.resource)}
                </span>
              </td>
              <td className={`pb-4 w-1/12 text-center cursor-pointer`}>
                <FontAwesomeIcon
                  className={`text-lg text-red-500 hover:text-red-400`}
                  onClick={e => {
                    handleDelete(e, row.resource);
                  }}
                  icon={faTimes}
                />
              </td>
            </>
          )}

          {row.label === "Builder Profile" && builderProfile === "file" && (
            <>
              <td className={`pb-4 w-4/12 text-center `}>
                <span className={`pt-5 text-palette-blue-light`}>
                  {getFileName(row.resource)}
                </span>
              </td>
              <td className={`pb-4 w-1/12 text-center cursor-pointer`}>
                <FontAwesomeIcon
                  className={`text-lg text-red-500 hover:text-red-400`}
                  onClick={e => {
                    handleDelete(e, row.resource);
                  }}
                  icon={faTimes}
                />
              </td>
            </>
          )}
        </tr>
      </React.Fragment>
    );
  });
};

export default ResourcesData;
