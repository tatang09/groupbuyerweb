import React, { createRef, useState, useRef, useEffect } from 'react';
import { Button, Form, TextInput, TextArea } from '~/components/_base';
import isEmpty from "lodash/isEmpty";
import { addtionalInfoFields } from '~/helpers/salesAdviceHelper/salesAdviceHelper';


const AdditionalInfos = ({
  additionalInfoErrors,
  additionalInfoForm,
  width =`w-full`
}) => {
  
  return (
    <section className={`relative mt-6 rounded`}>
      <div className={`mx-auto`} style={{ maxWidth: 1366 }}>
        <div className={`flex justify-center mt-4 mb-8 ${width} lg:pr-10`}>
          <div className={`border lg:mt-0 mt-8 px-6 rounded w-full`}>
            <h1 className={`font-bold py-6 text-base`}>
              Additional Information
            </h1>
            <form ref={additionalInfoForm}>
              <Form errors={additionalInfoErrors} formFields={addtionalInfoFields} />
            </form>
          </div>
        </div>
      </div>
    </section>
  );
};

export default AdditionalInfos;
