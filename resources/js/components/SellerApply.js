import React, { useState, createRef } from "react";

import UserGlobal from "~/states/userGlobal";
import { Button, Modal, TextInput, TextArea } from "~/components/_base";
import { axios } from "~/helpers/bootstrap";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import Snackbar from "@material-ui/core/Snackbar";
import MuiAlert from "@material-ui/lab/Alert";

function Alert(props) {
  return <MuiAlert elevation={6} variant="filled" {...props} />;
}

const SellerApply = ({ handleClose }) => {
  const [userState, userAction] = UserGlobal();
  const [errors, setErrors] = useState({});
  const [loading, setLoading] = useState(false);
  const [showAlert, setShowAlert] = useState(false);
  const [emailData, setEmailData] = useState({
    first_name: "",
    last_name: "",
    company: "",
    location: "",
    phone: "",
    email: "",
    message: ""
  });

  const handleSubmit = async e => {
    e.preventDefault();

    let errors = {};
    let formData = { ...emailData };
    formData.phone = "+61" + formData.phone;

    setLoading(true);

    try {
      await axios.post("/api/send-apply-now", formData);

      setLoading(false);
      setShowAlert(true);
      setEmailData({
        name: "",
        phone: "",
        email: "",
        message: ""
      });
      handleClose();
    } catch (error) {
      let { data } = error.response;

      errors = data.errors;
      setLoading(false);
      setShowAlert(false);
    }
    setErrors(errors || {});
  };

  const changeHandler = (key, value) => {
    let data = Object.assign({}, emailData);
    data[key] = value;

    setEmailData(data);
  };

  const handleAlertClose = (event, reason) => {
    if (reason === "clickaway") {
      return;
    }

    setShowAlert(false);
  };

  return (
    <>
      <Modal
        show={userState.showSellerApply}
        title={`Enquire`}
        onClose={() => handleClose()}
        transition={`grow`}
      >
        <form
          className={`flex-row p-6 text-white pb-16`}
          onSubmit={handleSubmit}
        >
          <div className={`flex flex-col items-center lg:flex-row`}>
            <div className={`w-full`}>
              <TextInput
                type={`text`}
                placeholder={`First Name`}
                name={`first_name`}
                prefix={`user`}
                errors={errors.first_name}
                value={emailData.first_name}
                onChange={e => changeHandler("first_name", e.target.value)}
              />
              {errors.first_name && (
                <span className="text-red-500 text-xs">
                  {errors.first_name[0]}
                </span>
              )}
            </div>
            <div className={`hidden lg:block w-12`}></div>
            <div className={`w-full`}>
              <TextInput
                type={`text`}
                placeholder={`Last Name`}
                name={`last_name`}
                prefix={`user`}
                errors={errors.last_name}
                value={emailData.last_name}
                onChange={e => changeHandler("last_name", e.target.value)}
              />
              {errors.last_name && (
                <span className="text-red-500 text-xs">
                  {errors.last_name[0]}
                </span>
              )}
            </div>
          </div>

          <TextInput
            type={`text`}
            placeholder={`Company Name`}
            name={`company`}
            prefix={`home`}
            errors={errors.company}
            value={emailData.company}
            onChange={e => changeHandler("company", e.target.value)}
          />
          {errors.company && (
            <span className="text-red-500 text-xs">{errors.company[0]}</span>
          )}

          <TextInput
            type={`text`}
            placeholder={`Mobile`}
            name={`phone`}
            prefix={`phone-call`}
            value={emailData.phone}
            errors={errors.phone}
            onChange={e => changeHandler("phone", e.target.value)}
          />
          {errors.phone && (
            <span className="text-red-500 text-xs">{errors.phone[0]}</span>
          )}

          <TextInput
            type={`text`}
            placeholder={`Email`}
            name={`email`}
            prefix={`mail`}
            value={emailData.email}
            errors={errors.email}
            onChange={e => changeHandler("email", e.target.value)}
          />
          {errors.email && (
            <span className="text-red-500 text-xs">{errors.email[0]}</span>
          )}

          <TextInput
            type={`text`}
            placeholder={`Location`}
            name={`location`}
            prefix={`map-pin`}
            errors={errors.location}
            value={emailData.location}
            onChange={e => changeHandler("location", e.target.value)}
          />
          {errors.location && (
            <span className="text-red-500 text-xs">{errors.location[0]}</span>
          )}

          <TextArea
            placeholder={`Message`}
            name={`message`}
            prefix={`message-square`}
            value={emailData.message}
            onChange={e => changeHandler("message", e.target.value)}
            // errors={errors.message}
            // value={emailData.message}
            // onChange={e => changeHandler('message', e.target.value)}
          />

          {/* <TextInput
            type={`text`}
            placeholder={`Phone`}
            name={`phone`}
            prefix={`phone`}
          /> */}

          {/* <TextInput
            type={showPassword ? "text" : "password"}
            placeholder={`Password`}
            name={`password`}
            prefix={`lock`}
            suffix={
              <button onClick={() => handleShowPassword()}>
                <svg className={`feather-icon h-5 w-5 opacity-50`}>
                  <use xlinkHref={`/assets/svg/feather-sprite.svg#${showPassword ? "eye-off" : "eye"}`} />
                </svg>
              </button>
            }
          /> */}

          <input type={`hidden`} name={`role`} value={`project_developer`} />

          <div
            className={`flex flex-col items-center justify-between lg:flex-row lg:mt-0 mt-16`}
          >
            {/* <Button className={`px-20`}>Apply Now</Button> */}
            <Button className={`py-2 mt-12 block`} disabled={loading}>
              {loading && (
                <FontAwesomeIcon
                  icon={["fas", "spinner"]}
                  className={`fa-spin mr-2`}
                />
              )}
              Send Enquiry
            </Button>
            {/* <div className={`text-gray-600 text-base`}>
              Already have an account?
              <span
                className={`cursor-pointer text-palette-blue-light hover:text-palette-teal transition-all duration-300`}
                onClick={() => {
                  handleClose()
                  userAction.setState({ showSignIn: true })
                }}
              >
                &nbsp;Log in
              </span>
            </div> */}
          </div>
        </form>
      </Modal>
      <Snackbar
        anchorOrigin={{ vertical: "bottom", horizontal: "right" }}
        open={showAlert}
        autoHideDuration={6000}
        onClose={handleAlertClose}
      >
        <Alert severity="success" onClose={handleAlertClose}>
          Thank you for your message, we will contact you shortly!
        </Alert>
      </Snackbar>
    </>
  );
};

export default SellerApply;
