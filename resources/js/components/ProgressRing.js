import React from 'react';
import { isMobile } from "react-device-detect";

const ProgressRing = ({ progress = 100 }) => {
    let radius = isMobile ? 50 : 80;
    let stroke = isMobile ? 6 : 10;

    const normalizedRadius = radius - stroke * 2;
    const circumference = normalizedRadius * 2 * Math.PI;
    const strokeDashoffset = circumference - progress / 100 * circumference;
  
    return (
        <div className="relative flex h-auto overflow-hidden text-xs">
            <svg
                className={"flex mx-auto transform -rotate-90"}
                height={radius * 2}
                width={radius * 2}
            >
                {/* First circle stroke (gray) */}
                <circle 
                    stroke="#D1D5DB"
                    fill="transparent"
                    strokeWidth={ stroke }
                    style={ { strokeDashoffset } }
                    r={ normalizedRadius }
                    cx={ radius }
                    cy={ radius }
                />
                {/* Second circle stroke (purple) */}
                <circle
                    stroke="#E91AFC"
                    fill="transparent"
                    strokeWidth={ stroke }
                    strokeDasharray={ circumference + ' ' + circumference }
                    style={ { strokeDashoffset } }
                    r={ normalizedRadius }
                    cx={ radius }
                    cy={ radius }
                />
            </svg>
            <div className="absolute top-0 bottom-0 left-0 right-0 z-10 flex items-center justify-center">
                <div className={"flex justify-center w-full px-4"}>
                    <div className={"font-bold uppercase leading-none mt-1 lg:text-3xl xs:text-xl text-palette-purple-main"}>
                        {progress}%
                    </div>
                </div>
            </div>
        </div>
    )
}

export default ProgressRing;