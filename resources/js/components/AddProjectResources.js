import React, { useState, useRef } from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import UserGlobal from "../states/userGlobal";

import { saveUpdateResources } from "../data/index";
import {
  tableRows,
  resources,
  tableHeaders
} from "../helpers/resourceHelper/index";

import { Button } from "~/components/_base";

import { faSpinner } from "@fortawesome/free-solid-svg-icons";
import { sweetAlert } from "../components/_base/alerts/sweetAlert";
 
import ResourcesData from "./resources/ResourcesData";
import LandResourcesData from "./resources/LandResourcesData";

const AddProjectResources = ({ toggleFetch }) => {
  const [userState, userActions] = UserGlobal();
  const [projectResources, setProjectResources] = useState([]);
  const resourceForm = useRef(null);
  const [loading, setLoading] = useState(false);
  const [builderProfile, setBuilderProfile] = useState("file");
  const [builderProfileLink, setBuilderProfileLink] = useState("");
  const [arcProfile, setArcProfile] = useState("file");
  const [arcProfileLink, setArcProfileLink] = useState("");
  const [devProfile, setDevProfile] = useState("file");
  const [devProfileLink, setDevProfileLink] = useState("");
  const [projectVideo, setProjectVideo] = useState("");

  const handleSubmit = async e => {
    e.preventDefault();
    saveResources();
  };

  const saveResources = async () => {
    const formData = new FormData(resourceForm.current);

    projectResources.forEach((resource, key) => {
      if (arcProfile === "text" && resource.name === "architect_profile") {
        return;
      }
      if (builderProfile === "text" && resource.name === "builder_profile") {
        return;
      }

      if (devProfile === "text" && resource.name === "developer_profile") {
        return;
      }

      formData.append(`resources[${key}][file]`, resource.file);
      formData.append(`resources[${key}][name]`, resource.name);
    });

    if (devProfileLink) {
      formData.append(
        "devProfileLink",
        resources.appendHttpsToResource(devProfileLink)
      );
    }

    if (arcProfileLink) {
      formData.append(
        "arcProfileLink",
        resources.appendHttpsToResource(arcProfileLink)
      );
    }

    if (builderProfileLink) {
      formData.append(
        "builderProfileLink",
        resources.appendHttpsToResource(builderProfileLink)
      );
    }

    setLoading(true);
    formData.append("id", userState.projectData.id);
    formData.append("name", userState.projectData.name);
    formData.append("projectVideo", projectVideo);

    let url = `/api/project-resource/upload`;

    await saveUpdateResources({ formData, url })
      .then(r => {
        if (r.status === 200) {
          userActions.setState({ projectData: r.data });
          userActions.setState({ currentProjStep: 3 });
        }
        toggleFetch();
        setLoading(false);
        sweetAlert("success", "Project resources successfully saved.");
      })
      .catch(err => {
        setLoading(false);
      })
      .then(() => {});
  };

  const handleFileChange = (e, resource) => {
    e.preventDefault();

    let c = projectResources.filter(file => {
      if (file["name"] !== resource) {
        return file;
      }
    });

    if (e.target.files.length > 0) {
      setProjectResources(
        c.concat(resources.trimFilename(e.target.files[0], resource))
      );
      e.target.value = null;
    }
  };

  const handleDelete = (e, resource) => {
    e.preventDefault();
    setProjectResources(
      projectResources.filter(file => {
        if (file["name"] !== resource) {
          return file;
        }
      })
    );
    e.target.value = null;
  };

  const getFileName = resource => {
    let filename = "";
    projectResources.forEach(file => {
      if (file["name"] === resource) {
        filename = file["file"]["name"];
      }
    });
    return filename;
  };

  const changeInputType = (rowName, checked) => {
    if (rowName === "Builder Profile") {
      if (checked) {
        setBuilderProfile("text");
      } else {
        setBuilderProfileLink("");
        setBuilderProfile("file");
      }
    }

    if (rowName === "Developer Profile") {
      if (checked) {
        setDevProfile("text");
      } else {
        setDevProfileLink("");
        setDevProfile("file");
      }
    }

    if (rowName === "Architect Profile") {
      if (checked) {
        setArcProfile("text");
      } else {
        setArcProfileLink("");
        setArcProfile("file");
      }
    }
  };

  const setCheckedState = row => {
    let retval = false;
    if (row.label === "Builder Profile") {
      if (builderProfile === "text") {
        retval = true;
      }
    }

    if (row.label === "Developer Profile") {
      if (devProfile === "text") {
        retval = true;
      }
    }

    if (row.label === "Architect Profile") {
      if (arcProfile === "text") {
        retval = true;
      }
    }
    return retval;
  };

  return (
    <div className={`bg-white rounded-lg py-5 px-8 m-5`}>
      <form ref={resourceForm} onSubmit={handleSubmit}>
        <table className={`w-full`}>
          <thead className={`w-full`}>
            <tr>
              {tableHeaders.map((header, key) => {
                return (
                  <th
                    key={key}
                    className={` ${header.style} border-b font-bold px-4 py-2 text-palette-gray px-4 py-2 ${header.width} `}
                  >
                    {header.label}
                  </th>
                );
              })}
            </tr>
          </thead>
          <tbody className={`w-full `}>
            {userState.projectData &&
            userState.projectData.sub_property_id == "4" ? (
              <LandResourcesData
                devProfile={devProfile}
                getFileName={getFileName}
                setCheckedState={setCheckedState}
                changeInputType={changeInputType}
                handleFileChange={handleFileChange}
                handleDelete={handleDelete}
                setProjectVideo={setProjectVideo}
                setDevProfileLink={setDevProfileLink}
              />
            ) : (
              <ResourcesData
                builderProfile={builderProfile}
                devProfile={devProfile}
                arcProfile={arcProfile}
                getFileName={getFileName}
                setCheckedState={setCheckedState}
                changeInputType={changeInputType}
                handleFileChange={handleFileChange}
                handleDelete={handleDelete}
                setProjectVideo={setProjectVideo}
                setDevProfileLink={setDevProfileLink}
              />
            )}
          </tbody>
        </table>
        <div className="flex justify-end mt-5">
          <Button className={`font-bold rounded-full`} disabled={loading}>
            {loading && (
              <FontAwesomeIcon icon={faSpinner} className={`fa-spin mr-2`} />
            )}
            Save &amp; Continue
          </Button>
        </div>
      </form>
    </div>
  );
};

export default AddProjectResources;
