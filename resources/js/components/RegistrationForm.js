import React, { useState, createRef, useRef } from 'react';
import { useForm } from '../hooks/customHooks'
import { Checkbox } from "pretty-checkbox-react";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import UserGlobal from "../states/userGlobal";
import { axios } from "~/helpers/bootstrap";
import { sweetAlert } from '../components/_base/alerts/sweetAlert';
import { setToken } from '~/services/auth';
import { isMobile } from 'react-device-detect';

const RegistrationForm = ({
    containerBgColor = "#4D0B98",
    containerRounded = false,
    subHeader = "BE IN THE LOOP",
    headerTop = "Create your",
    headerBottom = "account for free",
    inputBgColor = "#FFFFFF5C",
    displayCloseButton = false,
    rightPadded = true,
    subHeaderColor = "text-white ",
    headerColor = "text-palette-purple-main",
    // checkboxClass = "landing_checkbox",
    textColorTheme = "light",
    handleCloseModal,
    isPopup = false,
    opacity = "100%",
    handleLoginModal,
    checkIconColor="text-palette-purple-main"
}) => {
    const [userState, userAction] = UserGlobal();
    const [errors, setErrors] = useState({});
    const [loading, setLoading] = useState(false);
    const form = createRef();
    const [isAgree, setTermsAndCondition] = useState(false);
    const [receiveNotices, setToReceiveNotices] = useState(false);
    const [state, formChange] = useForm({
        first_name: "",
        last_name: "",
        email: "",
        password: "",
        repeat_password: "",
        user_type: "customer",
        receive_notice: false,
        user_type: "customer"
    });

    const handleSubmit = async e => {

        e.preventDefault();

        if (state.password.length < 5) {
            sweetAlert(
                'error',
                'Password must be 5 or more characters.',
            );
            return;
        }

        if (state.repeat_password !== state.password) {
            sweetAlert(
                'error',
                'Please retype password.',
            );
            return;
        }

        if (!isAgree) {
            sweetAlert(
                'error',
                'Please agree to our terms and conditions.',
            );
            return;
        }

        state.receive_notice = receiveNotices;
        let errors = {};
        let formData = { ...state };

        setLoading(true);

        try {

            let { data } = await axios.post('/api/register', formData);

            setToken(data.access_token);

            setLoading(false);

            sweetAlert(
                'success',
                'Thank you for registering and welcome to GroupBuyer.',
            );

            resetFields();

            if (isPopup) {
                handleCloseModal();
            }

        } catch (error) {

            let { data } = error.response;

            errors = data.errors;

            setLoading(false);
        }
        setErrors(errors || {});
    };


    const resetFields = () => {
        state.first_name = "";
        state.last_name = ""
        state.email = "";
        state.password = "";
        state.repeat_password = "";
        state.user_type = "customer";
        state.receive_notice = receiveNotices;
        state.user_type = "customer";

        setToReceiveNotices(!receiveNotices);
        setTermsAndCondition(!isAgree);
    }

    return (
        <div className={`flex items-center justify-center ${isPopup ? "lg:w-1/3" : ""} w-full`} style={{ zoom: !isPopup || isMobile ? 1 : 0.8 }}>
            <form
                className={`w-full`}
                ref={form}
                onSubmit={handleSubmit}>

                {/* <div className={`relative ${rightPadded ? 'lg:pr-24' : 'lg:pr-12'} pl-12 py-8 w-full xs:flex-1 xs:px-12 ${containerRounded ? 'rounded-lg' : ''}`} style={{ backgroundColor: containerBgColor }}> */}
                <div className={`relative ${rightPadded ? 'lg:pr-24' : 'lg:pr-12'} pl-12 py-8 w-full xs:flex-1 xs:px-12 ${isPopup ? "" : "rounded-lg"}`} 
                    style={
                        opacity !== "100%" 
                        ? ({
                            background: `linear-gradient(45deg, rgb(255 255 255 / ${opacity}), rgb(255 255 255 / ${opacity}))`
                        }) 
                        : ({
                            backgroundColor: containerBgColor
                        })
                    }>
                    {displayCloseButton && (
                        <img onClick={handleCloseModal} src={"/assets/svg/close_button.svg"} className={"absolute top-0 right-0 w-6 my-6 mx-8 cursor-pointer"} />
                    )}
                    <h3 className={`mb-2 ${subHeaderColor} font-bold`}>{subHeader}</h3>
                    <h2 className={`${headerColor} font-bold leading-none mb-4 xs:text-3xl`} style={{ fontSize: "3rem" }}>{headerTop} <br />{headerBottom}</h2>
                    <div className={"flex xs:flex-col text-base mt-2 mb-4"}>
                        <div className={"flex-1 lg:pr-1 xs:mb-4"}>
                            <p className={`${textColorTheme == 'light' ? 'text-white' : 'text-gray-900'} mb-1`}>First Name</p>
                            <input className={`rounded-sm ${textColorTheme == 'light' ? 'text-white' : 'text-gray-900'} placeholder-white py-2 px-4 mr-2 w-full`}
                                name='first_name'
                                value={state.first_name}
                                onChange={formChange}
                                style={{ backgroundColor: inputBgColor }} />
                            {errors.first_name && (
                                <span className="text-xs text-red-500">
                                    {errors.first_name[0]}
                                </span>
                            )}
                        </div>
                        <div className={"flex-1 lg:pl-1"}>
                            <p className={`${textColorTheme == 'light' ? 'text-white' : 'text-gray-900'} mb-1`}>Last Name</p>
                            <input className={`rounded-sm ${textColorTheme == 'light' ? 'text-white' : 'text-gray-900'} placeholder-white py-2 px-4 mr-2 w-full`} name='last_name' value={state.last_name} onChange={formChange} style={{ backgroundColor: inputBgColor }} />
                            {errors.last_name && (
                                <span className="text-xs text-red-500">
                                    {errors.last_name[0]}
                                </span>
                            )}
                        </div>
                    </div>
                    <div className={"flex text-base mb-4"}>
                        <div className={"flex-1"}>
                            <p className={`${textColorTheme == 'light' ? 'text-white' : 'text-gray-900'} mb-1`}>Email</p>
                            <input className={`rounded-sm ${textColorTheme == 'light' ? 'text-white' : 'text-gray-900'} placeholder-white py-2 px-4 mr-2 w-full`} name='email' value={state.email} onChange={formChange} style={{ backgroundColor: inputBgColor }} />
                            {errors.email && (
                                <span className="text-xs text-red-500">{errors.email[0]}</span>
                            )}
                        </div>
                    </div>
                    <div className={"flex xs:flex-col text-base mb-4"}>
                        <div className={"flex-1 lg:pr-1 xs:mb-4"}>
                            <p className={`${textColorTheme == 'light' ? 'text-white' : 'text-gray-900'} mb-1`}>Password <i className={"font-light text-sm xs:text-xs"}>5 or more characters</i></p>
                            <input type={`password`} className={`rounded-sm ${textColorTheme == 'light' ? 'text-white' : 'text-gray-900'} placeholder-white py-2 px-4 mr-2 w-full`} name='password' value={state.password} onChange={formChange} style={{ backgroundColor: inputBgColor }}></input>
                            {errors.password && (
                                <span className="text-xs text-red-500">{errors.password[0]}</span>
                            )}
                        </div>
                        <div className={"flex-1 lg:pl-1"}>
                            <p className={`${textColorTheme == 'light' ? 'text-white' : 'text-gray-900'} mb-1`}>Repeat Password</p>
                            <input type={`password`} className={`rounded-sm ${textColorTheme == 'light' ? 'text-white' : 'text-gray-900'} placeholder-white py-2 px-4 mr-2 w-full`} name='repeat_password' value={state.repeat_password} onChange={formChange} style={{ backgroundColor: inputBgColor }}></input>
                        </div>
                    </div>
                    <div className={"flex text-sm mb-4"}>
                        <div className={"flex-1"}>
                            <Checkbox
                                onChange={() => setTermsAndCondition(!isAgree)}
                                className={`text-base ${textColorTheme == 'light' ? 'text-white landing_checkbox' : 'text-gray-900 landing_checkbox_dark'} font-light lg:mb-0 mb-8 xs:text-xs xs:mb-3`}
                                shape={`curve`}
                                color={`default`}
                                icon={<FontAwesomeIcon icon={['fas', 'check']} size={`xs`} className={`${checkIconColor}`} style={{ padding: "0.15rem" }} />}
                                inputProps={{
                                    name: `landing_checkbox`
                                }}
                            >
                                <i className={"ml-2"}>I accept the <span className={"text-palette-purple-main"}>Terms &amp; Conditions</span></i>
                            </Checkbox>
                        </div>
                    </div>
                    {/* <div className={"flex text-sm mb-4"}>
                        <div className={"flex-1"}>
                            <Checkbox
                                onChange={() => setToReceiveNotices(!receiveNotices)}
                                className={`text-base ${textColorTheme == 'light' ? 'text-white landing_checkbox' : 'text-gray-900 landing_checkbox_dark'} font-light lg:mb-0 mb-8 xs:text-xs xs:mb-3`}
                                shape={`curve`}
                                color={`default`}
                                icon={<FontAwesomeIcon icon={['fas', 'check']} size={`xs`} className={"text-palette-purple-main"} style={{ padding: "0.15rem" }} />}
                                inputProps={{
                                    name: `landing_checkbox`
                                }}
                            >
                                <i className={"ml-2"}>I want to receive a newsletters and special offer notices from GroupBuyer</i>
                            </Checkbox>
                        </div>
                    </div> */}
                    <div className={"flex text-base items-center mt-8 mb-4 xs:text-sm"}>
                        <button className={"rounded-sm text-white text-base font-bold py-2 px-12 mr-8 xs:py-3 xs:px-6 xs:mr-4 xs:text-sm"} style={{ backgroundColor: '#E91AFC' }}>Get Started</button>
                        <span className={`${textColorTheme == 'light' ? 'text-white' : 'text-gray-900'} font-light`}>Already have an account? <b onClick={handleLoginModal} className={"text-palette-purple-main ml-2"}><br className={'lg:hidden'} />Log in</b></span>
                    </div>
                </div>
            </form>
        </div>
    );
}

export default RegistrationForm;
