import React, { useEffect, useState } from "react";

import GoogleMaps from "./GoogleMaps";
import { sweetAlert } from "../components/_base/alerts/sweetAlert";
import UserGlobal from "../states/userGlobal";
 

const mapKey = `${process.env.MIX_GOOGLE_API_KEY}`;

const AgentDetailsSection = ({ project, isProjectLand }) => {
  const [origin, setOrigin] = useState("");
  const [destination, setDestination] = useState(
    `${project.lat},${project.long}`
  );
 
  const [destinationDisPlaySuite, setDestinationDisPlaySuite] = useState(
    `${project.display_suite_lat}, ${project.display_suite_long}`
  );

  const [userState, seState] = UserGlobal();

  useEffect(() => {
    getCurrentLocation();
  }, []);

  const getDirection = destination => {
    return (
      <div
        className={`font-semibold mt-5 text-palette-purple hover:text-palette-violet transition-all duration-300`}
      >
        <a
          href={`https://www.google.com/maps/dir/?api=1&origin=${origin}&destination=${destination}&travelmode=driving`}
          target="#"
        >
          <span className={`cursor-pointer`}>
            <svg className={`inline feather-icon h-4 mr-2 w-4`}>
              <use xlinkHref={`/assets/svg/feather-sprite.svg#map-pin`} />
            </svg>
            Get Directions
          </span>
        </a>
      </div>
    );
  };

  const getCurrentLocation = () => {
    if (navigator.geolocation) {
      navigator.geolocation.getCurrentPosition(
        function(position) {
          let currPosition = `${position.coords.latitude},${position.coords.longitude}`;
          // let currPosition = `-37.831387, 145.04581`
          setOrigin(currPosition);
        },
        function() {
          //handle error
        }
      );
    } else {
      // Browser doesn't support Geolocation
      sweetAlert(
        "error",
        "Browser doesn't support Geolocation, please update your browser!"
      );
    }
  };

  const projectAdress = (project, isProjectLand) => {
    return (
      <div
        className={`lg:flex lg:flex-1 lg:items-center lg:text-left text-base text-center`}
      >
        <div className={`lg:flex-1 my-6 lg:my-0`}>
          <div className={`font-black lg:mt-0 mb-4 mt-6`}>{isProjectLand ? "OFFICE ADDRESS" : "PROJECT ADDRESS"}</div>
          <span className={`font-normal text-base`}>
            {project.address ? project.address.line_1 + ", " : ""}
          </span>
          <br />
          <span className={`font-normal text-base`}>
            {project.address ? project.address.suburb + ", " : ""}
          </span>
          <span className={`font-normal text-base`}>
            {project.address ? project.address.state + ", " : ""}
          </span>
          <span className={`font-normal text-base`}>
            {project.address ? project.address.postal : ""}
          </span>
          {getDirection(destination)}
        </div>

        {!project.is_completed && !isProjectLand && (
          <div className={`lg:flex-1 my-6 lg:my-0`}>
            <div className={`font-black mb-4`}>DISPLAY SUITE ADDRESS</div>
            <span className={`font-normal text-base`}>
              {project.display_suite_address
                ? project.display_suite_address.line_1 + ", "
                : ""}
            </span>
            <br />
            <span className={`font-normal text-base`}>
              {project.display_suite_address
                ? project.display_suite_address.suburb + ", "
                : ""}
            </span>
            <span className={`font-normal text-base`}>
              {project.display_suite_address
                ? project.display_suite_address.state + ", "
                : ""}
            </span>
            <span className={`font-normal text-base`}>
              {project.display_suite_address
                ? project.display_suite_address.postal
                : ""}
            </span>
            {getDirection(destinationDisPlaySuite)}
          </div>
        )}
      </div>
    );
  };

  return (
    <section>
      <div
        className={`py-6`}
        style={{ background: "rgba(177, 173, 173, 0.21)" }}
      >
        <div
          className={`mx-auto justify-center lg:flex lg:flex-row`}
          style={{ maxWidth: 1366 }}
        >
          <div className="lg:flex lg:flex-1 text-center text-gray-700">
            <div className="flex flex-col items-center lg:flex-1 lg:flex-row">
              <div
                className={`lg:-mt-20 lg:flex lg:flex-1 lg:flex-col lg:items-center`}
              >
                <div className="mt-2 text-center">
                  <img
                    src="/assets/images/_directors/luke-hayes.jpg"
                    className={`mx-auto rounded-full`}
                    style={{ width: 120 }}
                  />
                </div>
                <div className="text-center mt-2 text-center">
                  <div className="text-base text-gray-900 mt-2 font-bold">
                    {project && project.agent_name}
                  </div>
                  <div className="border rounded my-1 border-gray-700 font-normal text-base text-center text-gray-900 inline-block">
                    <a className={`px-4`} href={`tel:${project.agent_phone}`}>
                      {project && project.agent_phone}
                    </a>
                  </div>
                  <div className="font-bold text-base text-gray-900 truncate w-48 hover:text-palette-purple duration-300 transition-all">
                    <a href={`mailto:${project.agent_email}`}>
                      {project && project.agent_email}
                    </a>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div className={`lg:flex lg:flex-1 lg:items-center`}>
            {projectAdress(project, isProjectLand)}
          </div>
        </div>
      </div>
      {project && (
       
          <GoogleMaps mapProps={project}   />
      
      )}

      {/*      
      {showAdd && userState.windowSize <= deviceSize.standardScreen && (
        <AdContainerComponent />
      )} */}
    </section>
  );
};

export default AgentDetailsSection;
