import React from 'react'
import { Checkbox } from "pretty-checkbox-react";
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
import RegistrationForm from "./RegistrationForm";

function JoinUs() {
    return (
        <div>
            <section>
            <div id="JoinUsBGpos" className={"flex flex-col pt-12 pb-32 px-32 xs:pt-2 xs:px-6 xs:pb-2 xs:bg-right"} style={{background:"url('/assets/images/howitworks-joinusforfree-bg-image.jpg')", backgroundSize:"cover", backgroundPositionX:"right", backgroundPositionY:"bottom"}}>
                <div className={"md:flex lg:flex"}>
                <div className={"flex-1 p-6 xs:p-1"}>
                    <div className={"flex flex-col py-12 px-24 rounded-md xs:pt-2 xs:px-2 xs:pb-2"}>
                    <p className={"lg:text-7xl text-white xs:text-4xl font-bold lg:text-center mb-4 xs:mt-4 text-center"}>Join us for free</p>
                    <p className={"text-palette-purple-main xs:text-sm lg:text-lg text-center lg:pb-8 2xl:text-2xl"}>A new and innovative platform to access properties from trusted developers.</p>
                    <p className={"lg:text-3xl text-palette-purple-main xs:text-2xl font-bold text-center xs:mt-6 2xl:text-4xl"}>Exclusive properties</p>
                    <div className={"lg:px-40 md:px-40 xs:px-4 joinUsContent"}><p className={"text-white font-light xs:text-sm lg:text-base text-center lg:pb-4 xs:-mt-4 2xl:text-xl"}>Enjoy peace of mind knowing all properties are exclusively listed with GroupBuyer. You will never see the same deal elsewhere.</p></div>
                    <p className={"lg:text-3xl text-palette-purple-main xs:text-2xl font-bold text-center 2xl:text-4xl"}>Unbeatable price</p>
                    <div className={"lg:px-40 md:px-40 xs:px-4 joinUsContent"}><p className={"text-white font-light xs:text-sm lg:text-base text-center lg:pb-4 xs:-mt-4 2xl:text-xl"}>Thanks to the long relationship with our developers and agents, the exclusive prices you see are not available elsewhere, not even directly from the developer.</p></div>
                    <p className={"lg:text-3xl text-palette-purple-main xs:text-2xl font-bold text-center 2xl:text-4xl"}>Full transparency</p>
                    <div className={"lg:px-40 md:px-40 xs:px-4 joinUsContent"}><p className={"text-white font-light xs:text-sm lg:text-base text-center lg:pb-4 xs:-mt-4 2xl:text-xl"}>We believe in building relationships with the foundation of trust. We are transparent with our process, and every resource and detail about the property buying process is available for you to review.</p></div>
                    <p className={"lg:text-3xl text-palette-purple-main xs:text-2xl font-bold text-center 2xl:text-4xl"}>Power of knowledge</p>
                    <div className={"lg:px-40 md:px-40 xs:px-4 joinUsContent"}><p className={"text-white font-light xs:text-sm lg:text-base text-center lg:pb-4 xs:-mt-4 2xl:text-xl"}>When it comes to property, all the details matter. That is why we empower you with our decades of knowledge and experience, helping you make smarter decisions to achieve your personal goals. We have made the property buying process easy and stress-free!</p></div>
                    </div>
                </div>
                </div>
                <div id="joinUsFormSize" className={`rounded-lg ml-56 mr-56 xs:hidden`} style={{background:"linear-gradient(45deg, rgb(255 255 255 / 30%), rgb(255 255 255 / 30%))"}}>
                  <div className={`py-2 px-4`}> 
                    <RegistrationForm
                        containerBgColor="transparent"
                        subHeaderColor="text-white"
                        headerColor="text-palette-purple-main"
                    />
                  </div>
                </div>
            </div>
            </section>

        <section>
          <div className={"lg:hidden md:hidden"}>
            <RegistrationForm
              subHeaderColor="text-palette-purple-main"
              headerColor="text-white"
              isPopup={false}
            />
          </div>
        </section>

        </div>
    )   
}

export default JoinUs