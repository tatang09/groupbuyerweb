import React from 'react';

const InspectProperty = ({}) => {
    return (
        <div className={"block lg:py-12 lg:pl-16 lg:pr-32 xs:px-12 xs:py-8 bg-palette-purple-main lg:w-1/2 xs:w-full space-y-8"}>
            <div className={"block w-full space-y-4"}>
                <p className={"font-bold uppercase text-sm text-palette-violet-main-dark"}>
                Inspect the property
                </p>
                <span className={"font-bold leading-none text-white lg:text-5xl xs:text-3xl"}>
                Want to visit this property?
                </span>
                <p className={"font-normal leading-normal lg:text-xl xs:text-base text-white"}>
                Get in touch with the agent to schedule a 1:1 private inspection
                </p>
                <button className={"rounded mt-2 text-white text-sm xs:w-full font-bold lg:py-2 xs:py-4 px-8 bg-palette-violet-main-dark"}>Book inspection time</button>
            </div>
            <div className={"block w-full space-y-4"}>
                <span className={"font-bold leading-none text-white lg:text-5xl xs:text-3xl"}>
                Can't inspect in person?
                </span>
                <p className={"font-normal leading-normal lg:text-xl xs:text-base text-white"}>
                Contact the agent to request a personal walkthrough of the property over video call
                </p>
                <button className={"rounded mt-2 text-white text-sm xs:w-full font-bold lg:py-2 xs:py-4 px-8 bg-palette-violet-main-dark"}>Request online inspection</button>
            </div>
        </div>
    )
}

export default InspectProperty;