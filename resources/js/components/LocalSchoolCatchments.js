import React from 'react';

const LocalSchoolCatchments = ({}) => {
    const [active, setActive] = React.useState(0);

    return (
        <div className={"block lg:py-12 lg:px-12 xs:p-8 lg:w-1/2 xs:w-full space-y-8"}>
            <div className={"block w-full"}>
                <p className={"font-bold leading-none lg:text-4xl xs:text-2xl"}>
                    Local school catchments
                </p>
                <div className={"flex lg:space-x-4 xs:space-x-2 w-full"}>
                    <button 
                        onClick={() => {
                            setActive(0)
                        }}
                        className={"lg:text-sm xs:text-xs xs:w-full tracking-tighter font-bold lg:py-2 xs:py-4 border-b-4 uppercase transition duration-300 ease-in-out " + (
                            active === 0 ? "border-palette-purple-main text-black" : "border-white text-gray-600"
                        )}
                    >
                        All
                    </button>
                    <button 
                        onClick={() => {
                            setActive(1)
                        }}
                        className={"lg:text-sm xs:text-xs xs:w-full tracking-tighter font-bold lg:py-2 xs:py-4 border-b-4 uppercase transition duration-300 ease-in-out " + (
                            active === 1 ? "border-palette-purple-main text-black" : "border-white text-gray-600"
                        )}
                    >
                        Primary
                    </button>
                    <button 
                        onClick={() => {
                            setActive(2)
                        }}
                        className={"lg:text-sm xs:text-xs xs:w-full tracking-tighter font-bold lg:py-2 xs:py-4 border-b-4 uppercase transition duration-300 ease-in-out " + (
                            active === 2 ? "border-palette-purple-main text-black" : "border-white text-gray-600"
                        )}
                    >
                        Secondary
                    </button>
                    <button 
                        onClick={() => {
                            setActive(3)
                        }}
                        className={"lg:text-sm xs:text-xs xs:w-full tracking-tighter font-bold lg:py-2 xs:py-4 border-b-4 uppercase transition duration-300 ease-in-out " + (
                            active === 3 ? "border-palette-purple-main text-black" : "border-white text-gray-600"
                        )}
                    >
                        Private
                    </button>
                    <button 
                        onClick={() => {
                            setActive(4)
                        }}
                        className={"lg:text-sm xs:text-xs xs:w-full tracking-tighter font-bold lg:py-2 xs:py-4 border-b-4 uppercase transition duration-300 ease-in-out " + (
                            active === 4 ? "border-palette-purple-main text-black" : "border-white text-gray-600"
                        )}
                    >
                        Childcare
                    </button>
                </div>
                <div className={"block w-full pt-8"}>
                    <p className={"font-bold uppercase leading-none text-sm"}>
                        Government school catchment
                    </p>
                    <div className={"block items-center w-full"}>
                        <div className={"flex w-full py-4 border-b items-center"}>
                            <div className={"flex lg:w-2/3 xs:w-1/2 space-x-2 items-center"}>
                                <div className={"lg:w-3/6 xs:w-4/6 lg:text-base xs:text-xs font-bold text-gray-700"}>
                                    Rose Bay Public School
                                </div>
                                <div className={"lg:w-1/6 xs:w-2/6 text-xs font-thin text-gray-700"}>
                                    0.2 km away
                                </div>
                                <div className={"xs:hidden lg:flex lg:flex-wrap lg:w-2/6"}>
                                    <span className={"w-auto mr-1 mb-1 whitespace-pre rounded bg-gray-600 text-white font-bold text-xs py-1 px-2"}>
                                        K - 6
                                    </span>
                                    <span className={"w-auto mr-1 mb-1 whitespace-pre rounded bg-gray-600 text-white font-bold text-xs py-1 px-2"}>
                                        CoEd
                                    </span>
                                    <span className={"w-auto mr-1 mb-1 whitespace-pre rounded bg-gray-600 text-white font-bold text-xs py-1 px-2"}>
                                        Government
                                    </span>
                                </div>
                            </div>
                            <div className={"block lg:w-1/3 xs:w-1/2 xs:pl-4"}>
                                <button className={"rounded whitespace-pre text-white lg:text-sm xs:text-xs w-full font-semibold p-2 bg-palette-violet-main-dark"}>View Catchment</button>
                            </div>
                        </div>
                        <div className={"flex w-full py-4 border-b items-center"}>
                            <div className={"flex lg:w-2/3 xs:w-1/2 space-x-2 items-center"}>
                                <div className={"lg:w-3/6 xs:w-4/6 lg:text-base xs:text-xs font-bold text-gray-700"}>
                                    Rose Bay Secondary School
                                </div>
                                <div className={"lg:w-1/6 xs:w-2/6 text-xs font-thin text-gray-700"}>
                                    0.2 km away
                                </div>
                                <div className={"xs:hidden lg:flex lg:flex-wrap lg:w-2/6"}>
                                    <span className={"w-auto mr-1 mb-1 whitespace-pre rounded bg-gray-600 text-white font-bold text-xs py-1 px-2"}>
                                        K - 6
                                    </span>
                                    <span className={"w-auto mr-1 mb-1 whitespace-pre rounded bg-gray-600 text-white font-bold text-xs py-1 px-2"}>
                                        CoEd
                                    </span>
                                    <span className={"w-auto mr-1 mb-1 whitespace-pre rounded bg-gray-600 text-white font-bold text-xs py-1 px-2"}>
                                        Government
                                    </span>
                                </div>
                            </div>
                            <div className={"block lg:w-1/3 xs:w-1/2 xs:pl-4"}>
                                <button className={"rounded whitespace-pre text-white lg:text-sm xs:text-xs w-full font-semibold p-2 bg-palette-violet-main-dark"}>View Catchment</button>
                            </div>
                        </div>
                    </div>
                </div>
                <div className={"block w-full pt-8"}>
                    <p className={"font-bold uppercase leading-none text-sm"}>
                        Independent schools
                    </p>
                    <div className={"block items-center w-full"}>
                        <div className={"flex w-full py-4 border-b items-center"}>
                            <div className={"flex lg:w-2/3 xs:w-1/2 space-x-2 items-center"}>
                                <div className={"lg:w-3/6 xs:w-4/6 lg:text-base xs:text-xs font-bold text-gray-700"}>
                                    The Stots College ELC Campus
                                </div>
                                <div className={"lg:w-1/6 xs:w-2/6 text-xs font-thin text-gray-700"}>
                                    0 km away
                                </div>
                                <div className={"xs:hidden lg:flex lg:flex-wrap lg:w-2/6"}>
                                    <span className={"w-auto mr-1 mb-1 whitespace-pre rounded bg-gray-600 text-white font-bold text-xs py-1 px-2"}>
                                        Private
                                    </span>
                                </div>
                            </div>
                            <div className={"block lg:w-1/3 xs:w-1/2 xs:pl-4"}>
                                <button className={"rounded whitespace-pre text-white lg:text-sm xs:text-xs w-full font-semibold p-2 bg-palette-violet-main-dark"}>More info</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default LocalSchoolCatchments;