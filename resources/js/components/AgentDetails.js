import React, { useState, useEffect } from "react";
import { isMobile } from "react-device-detect";

const AgentDetails = () => {



    const agentsArr = [
        {
            "title": "Real Estate Agents",
            "agentName": "Ben Daniel",
            "phone": "+61 999 999 999",
            "email": "bendaniel@groupbuyer.com.au",
            "linkedin": "Linkedin",
            "img": "/assets/images/_directors/BenDaniel.png",
            "img_web": "/assets/images/_directors/Ben_Daniel_web.png",
            "skills": [
                "Lorem ipsum dolor sit amet",
                "consectetur adipiscing elit.",
                "ut turpis in velit ultricies ",
                
            ],
            "message": "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi ut turpis in velit. "
        },
        {
            "title": "Property Developers",
            "agentName": "Luke Hayes",
            "phone": "+61 777 777 777",
            "email": "lukehayes@groupbuyer.com.au",
            "linkedin": "Linkedin",
            "img": "/assets/images/_directors/LukeHayes.png",
            "img_web": "/assets/images/_directors/Luke_Hayes_web.png",
            "skills": [
                "Lorem ipsum dolor sit amet",
                "consectetur adipiscing elit.",
                "ut turpis in velit ultricies",
               
            ],
            "message": "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi ut turpis in velit."
        }
    ];


    const agentSkills = (skills) => {
        return skills.map((skill, key) => {
            return <p key={key} className={`mb-0 text-xxs`}>
                {key + 1}.{" "}{skill}
            </p>
        });
    }

    const agentsWebView = (agentsArr) => {
        return agentsArr.map((agent, key) => {
            return <div key={key} className={`rounded-lg bg-palette-black-main flex flex flex-1 xl:w-3/4 ${key == 0 ? "mb-3" : "mb-0"}`}>
                <img src={`${agent.img_web}`} className={`lg:h-24 m-3 w-1/2 w-1/3 xl:w-auto ${isMobile ? "w-1/2" : ""} xl:w-auto  `} />
                <div className={`flex flex-col lg:flex-row lg:items-center`}>
                    <div className={`flex flex-1 flex-col`}>
                        <div className={`lg:pt-0 pt-4`}>
                            <p className={`leading-5 mb-0 md:text-2xl text-base text-palette-purple-main`}>{agent.title}</p>
                            <p className={`mb-0 text-xs sm:text-base`}>{agent.agentName}</p>
                            <p className={`mb-0 text-xxs text-gray-500`}>{agent.phone}</p>
                            <p className={`mb-0 text-xxs text-gray-500`}>{agent.email}</p>
                            <p className={`mb-0 text-xs text-palette-purple-main`}>{agent.linkedin}</p>
                        </div>
                    </div>
                    <div className={`flex flex-1 flex-col md:pb-4`}>
                        <div className={`xs:py-4`}>
                            <p className={`lg:my-2 mb-2 text-xxs w-2/3`}>{agent.message}</p>
                            {agentSkills(agent.skills)}
                        </div>
                    </div>
                </div>
            </div>
        });
    }


    return agentsWebView(agentsArr)

}



export default AgentDetails;