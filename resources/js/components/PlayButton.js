import React from "react"
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome"

const PlayButton = ({ onClick, className, size = `2x`, ...rest }) => {

  return (
    <button
      className={`
        w-24 h-24 border-2 rounded-full mx-auto flex items-center justify-center cursor-pointer transform hover:scale-110 transition-all duration-300
        ${className}
      `}
      style={{background: "linear-gradient(rgba(109, 109, 109, 0.5), rgba(51, 51, 51, 0.8))"}}
      onClick={onClick}
      {...rest}
    >
      {/* <svg className={`feather-icon text-white w-12 h-12`}>
        <use xlinkHref={`/assets/svg/feather-sprite.svg#play`}/>
      </svg> */}
      <FontAwesomeIcon icon={`play`} size={size}/>
    </button>
  )

}

export default PlayButton
