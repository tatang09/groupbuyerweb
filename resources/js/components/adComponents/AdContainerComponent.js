import React, { useState, useEffect } from "react";
 
import UserGlobal from "../../states/userGlobal";
 
const AdContainerComponent = ({src, className, ...rest}) => {
  const [img, setImage] = useState("");
  const [userState, seState] = UserGlobal();
 
  // useEffect(() => {
  //   if (userState.windowSize > 0) {
  //     mapAddImage();
  //   }
  // }, []);

  // const mapAddImage = () => {
  //   if (isMobile) {
  //     setImage(`/assets/images/Fenero_MREC.gif`);
  //   } else {
  //     if (userState.windowSize <= deviceSize.standardScreen) {
  //       setImage(`/assets/images/Fenero_Leaderboard.gif`);
  //     } else {
  //       setImage(`/assets/images/Fenero_Half_Page.gif`);
  //     }
  //   }
  // };

  return (
    <div>
      <img className={`${className} w-full`} src={src} />
    </div>
  );
};

export default AdContainerComponent;
