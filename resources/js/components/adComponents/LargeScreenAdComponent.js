import React, { useState, useEffect } from "react";
import { isMobile } from "react-device-detect";
import UserGlobal from "../../states/userGlobal";
import * as deviceSize from "../../helpers/deviceSizeHelper";

const LargeScreenAdComponent = () => {
  const [userState, setState] = UserGlobal();
  if (userState.windowSize > deviceSize.standardScreen) {
    return (
      <div>
        <img
          className={`w-full h-full`}
          src={"/assets/images/Fenero_Half_Page.gif"}
        />
      </div>
    );
  } else {
    return <></>;
  }
};

export default LargeScreenAdComponent;
