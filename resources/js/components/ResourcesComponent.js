import React, { useEffect, useState } from "react";
import { sweetAlert } from "../components/_base/alerts/sweetAlert";
import { resources } from "~/helpers/resourceHelper/index";
import MediaPlayerContainer from "./MediaPlayerContainer";
import { isMobile } from "react-device-detect";

const ResourcesComponent = ({ project, floorPlan = null }) => {
  const [resourceData, setResourceData] = useState([]);
  const [toogleShowPlayer, setToogleShowPlayerModal] = useState(false);

  useEffect(() => {
    mapURLLinks();
  }, [resourceData]);

  const mapURLLinks = () => {
    let resourceArr = project.resources;

    if (project.architect_profile_link) {
      resourceArr.push({
        file: project.architect_profile_link,
        name: "architect_profile"
      });
    }

    if (project.developer_profile_link) {
      resourceArr.push({
        file: project.developer_profile_link,
        name: "developer_profile"
      });
    }

    if (project.builder_profile_link) {
      resourceArr.push({
        file: project.builder_profile_link,
        name: "builder_profile"
      });
    }

    if (project.video_link) {
      resourceArr.push({ file: project.video_link, name: "project_video" });
    }

    if (floorPlan) {
      resourceArr.push(floorPlan);
    }

    setResourceData(resourceArr);
  };

  const toogleShowPlayerModal = () => {
    setToogleShowPlayerModal(!toogleShowPlayer);
  };

  const renderResources = () => {
    const dataArr = resources.resourcesArr(resourceData);
    return dataArr.map((resource, key) => {
      return (
        <li key={key} className={`mb-3`}>
          {resource.name &&
            resource.link &&
            resource.name !== "project_video" &&
            linkContainer(resource)}

          {resource.name === "project_video" &&
            resource.link &&
            videoContainer(resource)}
        </li>
      );
    });
  };

  const linkContainer = resource => {
    return (
      <a
        href={resource.link.file}
        target={`_blank`}
        className={`bg-gray-200 block font-bold px-3 py-1 leading-relaxed text-base rounded text-palette-blue-light ${isMobile ? "text-center" : "text-left"}`}
      >
        {resource.label}
      </a>
    );
  };

  const videoContainer = resource => {
    return (
      <a
        onClick={e => {
          e.preventDefault(), toogleShowPlayerModal();
        }}
        target={`_blank`}
        className={`bg-gray-200 block cursor-pointer font-bold leading-relaxed px-3 py-1 rounded text-base text-palette-blue-light ${isMobile ? "text-center" : "text-left"}`}
      >
        {resource.label}
      </a>
    );
  };

  return (
    <div>
      {project.video_link && (
        <MediaPlayerContainer
          url={project.video_link}
          toogleShowPlayerModal={toogleShowPlayerModal}
          show={toogleShowPlayer}
          height={`75%`}
        />
      )}
      {renderResources()}
    </div>
  );
};

export default ResourcesComponent;
