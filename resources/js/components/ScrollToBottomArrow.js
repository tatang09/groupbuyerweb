import React, {useState} from 'react';

const ScrollToBottomArrow = ({className, top=true, icon}) => {
 
  const [showScroll, setShowScroll] = useState(false);

  const checkScrollBottom = () => {
    if (!showScroll && window.pageYOffset < 400) {
      setShowScroll(true);
    } else if (showScroll && window.pageYOffset >= 400) {
      setShowScroll(false);
    }
  };

  const scrollBottom = () => {
    window.scrollTo({
      left: 0,
      top:
        document.documentElement.scrollHeight -
        document.documentElement.clientHeight,
      behavior: 'smooth',
    });
  };

  window.addEventListener('scroll', checkScrollBottom);

  return (
    <div
      className={className}
      onClick={scrollBottom}>
      <svg className={`feather-icon h-6 text-white w-6`}>
        <use xlinkHref={`/assets/svg/feather-sprite.svg#${icon}`} />
      </svg>
    </div>
  );
};

export default ScrollToBottomArrow;
