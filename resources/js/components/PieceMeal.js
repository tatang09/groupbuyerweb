import React, { useRef } from "react";
import HeroSlider, { Slide, OverlayContainer } from "hero-slider";
import Button from "@material-ui/core/Button";

const triple_chevron_right = "/assets/images/triple-chevron-right.svg";
const triple_chevron_left = "/assets/images/triple-chevron-left.svg";
const triple_chevron_down = "/assets/images/triple-chevron-down.svg";


const PieceMeal = ({ breakpoint = `md` }) => {
  const nextSlideHandler = React.useRef();
  const previousSlideHandler = React.useRef();
  return (
    <>
      <section id="four-easy-step" className={`hidden ${breakpoint}:block`}>
        <div
          className={
            "w-100 text-center flex flex-col w-full items-center xs:py-6 lg:py-16 w-100 xs:px-8 lg:px-32"
          }
          style={{
            background:
              "linear-gradient(rgb(255 255 255 / 98%), rgb(255 255 255 / 98%)) 0% 0% / cover, url(/assets/images/easy_steps_bg.jpg) 0% 0% no-repeat padding-box padding-box rgba(255, 255, 255, 0)",
            backgroundSize: "cover",
            backgroundPosition: "center"
          }}
        >
          <h2 className="text-palette-purple-main font-bold text-base">
            FOUR EASY STEPS TO BENEFIT
          </h2>
          <p className={"text-base font-light"}>
            We understand that the property buying process can be overwhelming. GroupBuyer 
            is here to guide you through all of it.<br />  Become a member of the GroupBuyer 
            community for free and get access to all available property deals.<br /> You can purchase 
            a property directly on the website, or you can always rely on a GroupBuyer representative to help you.
          </p>
          <div className="flex items-center justify-items-center w-full justify-around pb-8 xs:hidden max-content-width">
            <div className={"text-center cursor-default easy-step-container"}>
              <p
                className={
                  "text-transparent font-bold text-center easy-step-number"
                }
                style={{
                  WebkitTextStroke: "1.5px #E91AFC",
                  fontSize: "12rem",
                  lineHeight: "12rem"
                }}
              >
                01
              </p>
              <p
                className={"text-3xl font-bold leading-tight easy-step-text"}
                style={{ color: "#00000029" }}
              >
                Join us for <br />
                Free
              </p>
            </div>
            <img src={triple_chevron_right} className={"w-8 -mt-12"} />
            <div className={"text-center cursor-default easy-step-container"}>
              <p
                className={
                  "text-transparent font-bold text-center easy-step-number"
                }
                style={{
                  WebkitTextStroke: "1.5px #E91AFC",
                  fontSize: "12rem",
                  lineHeight: "12rem"
                }}
              >
                02
              </p>
              <p
                className={"text-3xl font-bold leading-tight easy-step-text"}
                style={{ color: "#00000029" }}
              >
                Access to great <br />
                Properties
              </p>
            </div>
            <img src={triple_chevron_right} className={"w-8 -mt-12"} />
            <div className={"text-center cursor-default easy-step-container"}>
              <p
                className={
                  "text-transparent font-bold text-center easy-step-number"
                }
                style={{
                  WebkitTextStroke: "1.5px #E91AFC",
                  fontSize: "12rem",
                  lineHeight: "12rem"
                }}
              >
                03
              </p>
              <p
                className={"text-3xl font-bold leading-tight easy-step-text"}
                style={{ color: "#00000029" }}
              >
                Price beat <br />
                Guarantee
              </p>
            </div>
            <img src={triple_chevron_right} className={"w-8 -mt-12"} />
            <div className={"text-center cursor-default easy-step-container"}>
              <p
                className={
                  "text-transparent font-bold text-center easy-step-number"
                }
                style={{
                  WebkitTextStroke: "1.5px #E91AFC",
                  fontSize: "12rem",
                  lineHeight: "12rem"
                }}
              >
                04
              </p>
              <p
                className={"text-3xl font-bold leading-tight easy-step-text"}
                style={{ color: "#00000029" }}
              >
                Secure your <br />
                Deal
              </p>
            </div>
          </div>
          <div className="flex items-center justify-items-center w-full justify-around pb-8 lg:hidden md:hidden">
            <p>here we add the sliders</p>
          </div>
          <h2 className="text-palette-purple-main text-base font-bold mb-3">
            Create Account
          </h2>
          <a href="#be-in-the-loop">
            <img src={triple_chevron_down} className={"w-6"} />
          </a>
        </div>
      </section>
      <section
        id="four-easy-step"
        className={`block ${breakpoint}:hidden`}
        style={{
          background:
            "linear-gradient(rgb(255 255 255 / 98%), rgb(255 255 255 / 98%)) 0% 0% / cover, url(/assets/images/easy_steps_bg.jpg) 0% 0% no-repeat padding-box padding-box rgba(255, 255, 255, 0)",
          backgroundSize: "cover",
          backgroundPosition: "center"
        }}
      >
        <div
          className={
            "w-100 text-center flex flex-col w-full items-center xs:pt-6 lg:py-16 w-100 xs:px-8 lg:px-32"
          }
        >
          <h2 className="text-palette-purple-main font-bold text-base">
            FOUR EASY STEP TO BENEFIT
          </h2>
          <p className={"text-base font-light"}>
            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi ut
            turpis in velit ultricies dignissim.
          </p>
        </div>
        <HeroSlider
          nextSlide={nextSlideHandler}
          previousSlide={previousSlideHandler}
          slidingAnimation="left_to_right"
          orientation="horizontal"
          initialSlide={1}
          style={{}}
          settings={{
            slidingDuration: 250,
            slidingDelay: 100,
            shouldAutoplay: false,
            shouldDisplayButtons: false,
            height: "40vh",
            backgroundColor: "blue"
          }}
        >
          <OverlayContainer>
            <div>
              <Button
                style={{
                  width: "100px",
                  margin: "12px 8px",
                  float: "left",
                  marginTop: "100px"
                }}
                onClick={() => previousSlideHandler.current()}
              >
                <img src={triple_chevron_left} />
              </Button>
              <Button
                style={{
                  width: "100px",
                  margin: "12px 8px",
                  float: "right",
                  marginTop: "100px"
                }}
                onClick={() => nextSlideHandler.current()}
              >
                <img src={triple_chevron_right} />
              </Button>
            </div>
          </OverlayContainer>

          <Slide>
            <div className={"text-center cursor-default easy-step-container"}>
              <p
                className={
                  "text-palette-purple-main font-bold text-center easy-step-number mb-0"
                }
                style={{
                  WebkitTextStroke: "1.5px #E91AFC",
                  fontSize: "12rem",
                  lineHeight: "12rem"
                }}
              >
                01
              </p>
              <p
                className={"text-3xl font-bold leading-tight easy-step-text"}
                style={{ color: "#211B24" }}
              >
                Join us for <br />
                Free
              </p>
            </div>
          </Slide>
          <Slide>
            <div className={"text-center cursor-default easy-step-container"}>
              <p
                className={
                  "text-palette-purple-main font-bold text-center easy-step-number mb-0"
                }
                style={{
                  WebkitTextStroke: "1.5px #E91AFC",
                  fontSize: "12rem",
                  lineHeight: "12rem"
                }}
              >
                02
              </p>
              <p
                className={"text-3xl font-bold leading-tight easy-step-text"}
                style={{ color: "#211B24" }}
              >
                Access to great <br />
                Properties
              </p>
            </div>
          </Slide>

          <Slide>
            <div className={"text-center cursor-default easy-step-container"}>
              <p
                className={
                  "text-palette-purple-main font-bold text-center easy-step-number mb-0"
                }
                style={{
                  WebkitTextStroke: "1.5px #E91AFC",
                  fontSize: "12rem",
                  lineHeight: "12rem"
                }}
              >
                03
              </p>
              <p
                className={"text-3xl font-bold leading-tight easy-step-text"}
                style={{ color: "#211B24" }}
              >
                Price beat <br />
                Guarantee
              </p>
            </div>
          </Slide>

          <Slide>
            <div className={"text-center cursor-default easy-step-container"}>
              <p
                className={
                  "text-palette-purple-main font-bold text-center easy-step-number mb-0"
                }
                style={{
                  WebkitTextStroke: "1.5px #E91AFC",
                  fontSize: "12rem",
                  lineHeight: "12rem"
                }}
              >
                04
              </p>
              <p
                className={"text-3xl font-bold leading-tight easy-step-text"}
                style={{ color: "#211B24" }}
              >
                Secure your <br />
                Deal
              </p>
            </div>
          </Slide>

          {/* <Nav /> */}
        </HeroSlider>
        <div className="flex flex-col items-center pt-4 pb-8">
          <h2 className="text-palette-purple-main text-base font-bold mb-3">
            Create Account
          </h2>
          <a href="#be-in-the-loop">
            <img src={triple_chevron_down} className={"w-6"} />
          </a>
        </div>
      </section>{" "}
    </>
  );
};

export default PieceMeal;
