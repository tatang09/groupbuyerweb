import React from "react";
import moment from "moment";

const EstimatedCompletionDate = ({ loading, project }) => {
  return (
    <div
      className={`flex font-bold h-8 items-center justify-center mb-2 mr-1 mr-2 mt-6 rounded-lg shadow-lg text-sm xs:w-2/3`}
      style={{ backgroundColor: "rgb(255, 0, 255, 0.1)" }}
    >
      Est. completion:&nbsp;
      {!loading &&
        project &&
        moment(project.proposed_settlement).format("MMM YYYY")}
    </div>
  );
};

export default EstimatedCompletionDate
