import React from 'react';

const BenefitsForBuyers = () => {

    const buyerBenefitsArr = [
        { title: "Exclusive properties", paragh: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi ut turpis in velit ultricies dignissim. Morbi viverra." },
        { title: "Unbeatable price", paragh: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi ut turpis in velit ultricies dignissim. Morbi viverra." },
        { title: "Full transparency", paragh: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi ut turpis in velit ultricies dignissim. Morbi viverra." },
        { title: "Power of knowledge", paragh: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi ut turpis in velit ultricies dignissim. Morbi viverra." }
    ];

    const displayBenefits = () => {
        return buyerBenefitsArr.map((item, key) => {
            return <div key={key}>
                <span className={`font-bold leading-7 sm:text-2xl text-base text-lg text-palette-violet-main-dark`}>{item.title}</span>
                <p className={`pt-2 text-white`}>{item.paragh}</p>
            </div>
        });
    }

    return (
        <div className={`bg-palette-purple-main w-full lg:w-1/2 flex flex-col`}>
            <div className={`py-8 px-12 w-full`}>
                <p className={`font-bold text-base text-palette-violet-main-dark`}>A NEW INNOVATIVE PLATFORM</p>
                <h1 className={`font-bold leading-none my-8 text-5xl text-white xs:text-3xl`}>Benefits for Buyers</h1>
                <div className={`pr-20 sm:flex`}>
                    <div className={`flex-1 flex`}>
                        <div className={`flex flex-col items-center justify-end sm:pb-8`}>{displayBenefits()}</div>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default BenefitsForBuyers;