import React from "react";
import UserGlobal from "../states/userGlobal";
import { isMobile } from "react-device-detect";

const EditSteps = ({
  activeProjectView,
  activeResourcesView,
  activePropertyView,
  activeAgreementView,
  toggleProjectView,
  toggleResourcesView,
  togglePropertyView,
  toggleAgreementView,
}) => {
  const [userState, userActions] = UserGlobal();

  return (
    <div className={`flex justify-between text-palette-blue-light lg:px-10`}>
      <div className={`cursor-pointer ${isMobile ? "flex flex-col items-center justify-center text-center" : ""}`}
       onClick={() => toggleProjectView()}>
        <h1 className={`text-center mb-2`}>
          <span
            className={`border ${
              activeProjectView
                ? "border-palette-blue-light"
                : "border-gray-600 text-gray-600"
            }  rounded-full py-1`}
            style={{ fontSize: "1.2rem", paddingLeft:"0.85rem", paddingRight:"0.85rem" }}
          >
            1
          </span>
        </h1>
        <h4
          className={`font-semibold tracking-wider  ${
            activeProjectView ? "border-palette-blue-light" : "text-gray-600"
          }  `}
        >
          Project Details
        </h4>
      </div>
      <div
        className={`flex-1 border-b border-white mx-6 mb-2 border-palette-gray`}
      ></div>
      <div className={`cursor-pointer ${isMobile ? "flex flex-col items-center justify-center text-center" : ""}`} onClick={() => toggleResourcesView()}>
        <h1 className={`text-center mb-2`}>
          <span
            className={`border ${
              activeResourcesView
                ? "border-palette-blue-light"
                : "border-gray-600 text-gray-600"
            } rounded-full py-1 px-3`}
            style={{ fontSize: "1.2rem" }}
          >
            2
          </span>
        </h1>
        <h4
          className={`font-semibold tracking-wider  ${
            activeResourcesView ? "border-palette-blue-light" : "text-gray-600"
          }  `}
        >
          Project Resources
        </h4>
      </div>
      <div
        className={`flex-1 border-b border-white mx-6 mb-2 border-palette-gray`}
      ></div>
      <div className={`cursor-pointer ${isMobile ? "flex flex-col items-center justify-center text-center" : ""}`} onClick={() => togglePropertyView()}>
        <h1 className={`text-center mb-2`}>
          <span
            className={`border ${
              activePropertyView
                ? "border-palette-blue-light"
                : "border-gray-600 text-gray-600"
            } rounded-full py-1 px-3`}
            style={{ fontSize: "1.2rem" }}
          >
            3
          </span>
        </h1>
        <h4
          className={`font-semibold tracking-wider  ${
            activePropertyView ? "border-palette-blue-light" : "text-gray-600"
          } `}
        >
          Project Properties
        </h4>
      </div>
      <div
        className={`flex-1 border-b border-white mx-6 mb-2 border-palette-gray`}
      ></div>
      <div className={`cursor-pointer ${isMobile ? "flex flex-col items-center justify-center text-center" : ""}`} onClick={() => toggleAgreementView()}>
        <h1 className={`text-center mb-2`}>
          <span
            className={`border ${
              activeAgreementView
                ? "border-palette-blue-light"
                : "border-gray-600 text-gray-600"
            } rounded-full py-1 px-3`}
            style={{ fontSize: "1.2rem" }}
          >
            4
          </span>
        </h1>
        <h4
          className={`font-semibold tracking-wider  ${
            activeAgreementView ? "border-palette-blue-light" : "text-gray-600"
          } `}
        >
          Agency Agreement
        </h4>
      </div>
    </div>
  );
};

export default EditSteps;
