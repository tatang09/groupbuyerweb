import React, { useEffect } from "react";
import { isEmpty } from "lodash/fromPairs";
import CurrencyFormat from "react-currency-format";
import { discountedPrice } from "~/helpers/pricingHelper/discountHelper";
const SalesAdvicePropertyDetails = ({ project, apartment, deal }) => {
  
  useEffect(() => {
    
  }, []);

  return (
    <section className={`relative mt-6 rounded`}>
      <div className={`mx-auto`} style={{ maxWidth: 1366 }}>
        <div className={`flex justify-center mt-4 mb-8 lg:w-9/12 lg:pr-10`}>
          <div className={`border lg:mt-0 mt-8 px-6 rounded w-full`}>
            <h1 className={`font-bold py-6 text-base`}>Property Details</h1>
            <div className={`pb-4 flex ml-6`}>
              <span className={`text-base font-bold mr-10 `}>
                Apartment/Lot:
              </span>
              <div className={`flex flex-col`}>
                <span className={`text-base`}>
                  <span className={`font-bold text-base`}>
                    Unit {apartment && apartment.unit_name}, {project.name}
                    ,&nbsp;
                  </span>
                  {!isEmpty(project) && project.address.line_1
                    ? project.address.line_1 + ", "
                    : ""}
                  {!isEmpty(project) && project.address.line_2
                    ? project.address.line_2 + ", "
                    : ""}
                  {!isEmpty(project) && project.address.suburbs
                    ? project.address.suburbs + ", "
                    : ""}
                  {!isEmpty(project) && project.address.state
                    ? project.address.state + " "
                    : ""}
                  {!isEmpty(project) && project.address.postal
                    ? project.address.postal
                    : ""}
                </span>
              </div>
            </div>
            <div className={`mb-8`}>
              <span className={`text-base font-bold mr-10 ml-6 `}>
                Purchase price:
              </span>
              <span className={`font-bold text-base text-red-700`}>
                <CurrencyFormat
                  value={deal.property_price - (deal.property_price * (deal.discount / 100))}
                  displayType={`text`}
                  thousandSeparator={true}
                  prefix={`$`}
                />
                {/* {discountedPrice(deal.discount_is_percentage, deal.discount, deal.property_price)} */}
              </span>
            </div>
          </div>
        </div>
      </div>
    </section>
  );
};

export default SalesAdvicePropertyDetails;
