import React, { useState, createRef, useEffect } from "react";
import {
  Dialog,
  DialogContent,
  DialogTitle,
  IconButton,
  Fade,
  Grow,
  Slide,
  Zoom
} from "@material-ui/core";
import { v4 as uuidv4 } from "uuid";
import swal from "sweetalert2";
import { isLoggedIn } from "~/services/auth";

import PurchaserDetails from "./PurchaserDetails";
import SalesAdvicePropertyDetails from "./SalesAdvicePropertyDetails";
import AdditionalInfos from "./AdditionalInfos";
import SolicitorDetails from "./SolicitorDetails";
import {
  fieldValidator,
  defaultPurchasers,
  formDataFieldsValidator,
  purchasersMapper
} from "~/helpers/salesAdviceHelper/salesAdviceHelper";
import { isEmpty } from "lodash/isEmpty";
import { fromPairs } from "lodash/fromPairs";
import { discountedPrice } from "~/helpers/pricingHelper/discountHelper";
import CloseIcon from "@material-ui/icons/Close";
import UserGlobal from "~/states/userGlobal";
import { isMobile } from "react-device-detect";

import { Button } from "~/components/_base";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { sweetAlert } from "./_base/alerts/sweetAlert";
const SalesAdviceModal = ({
  onClose,
  purchasersParam,
  show = false,
  apartment,
  deal,
  allowOutsideInput = false,
  maxWidth = `md`
}) => {
  const [loading, setLoading] = useState(false);
  const [purchaserErrors, setPurchaserErrors] = useState([]);
  const [solicitorErrors, setSolicitorErrors] = useState({});
  const [additionalInfoErrors, setAddtionalInfoErrors] = useState({});
  const [purchasers, setPurchasers] = useState(
    purchasersParam.length > 0 ? purchasersParam : defaultPurchasers
  );

  const solicitorForm = createRef();
  const additionalInfoForm = createRef();

  useEffect(() => {}, []);

  const mapDeal = () => {
    return new Object({
      name: deal.deal_name,
      address: {
        line_1: deal.property_address.line_1,
        suburbs: deal.property_address.suburbs,
        state: deal.property_address.state,
        postal: deal.property_address.postal
      }
    });
  };

  const handleSubmit = isSubmit => {
    let solicitorFormData = new FormData(solicitorForm.current);
    let additionalInfoFormData = new FormData(additionalInfoForm.current);
    setLoading(true);

    let purchaserArr = [];

    let solicitorAddress = {
      suburb: solicitorFormData.get("suburb"),
      state: solicitorFormData.get("state"),
      country: solicitorFormData.get("country"),
      postcode: solicitorFormData.get("postcode")
    };

    solicitorFormData.append("propertyId", apartment.id);
    solicitorFormData.append("address", JSON.stringify(solicitorAddress));
    let phone = solicitorFormData.get("phone")
      ? "+61" + solicitorFormData.get("phone")
      : "";
    solicitorFormData.set("phone", phone);

    purchasers.forEach(purchaser => {
      let data = {};
 
      data.address = {
        address_line_1: purchaser.address_line_1,
        suburb: purchaser.suburb,
        country: purchaser.country,
        state: purchaser.state,
        postcode: purchaser.postcode
      };

      data.address_line_1 = purchaser.address_line_1;
      data.suburb = purchaser.suburb;
      data.country = purchaser.country;
      data.state = purchaser.state;
      data.title = purchaser.title;
      data.postcode = purchaser.postcode;

      data.phone = purchaser["phone"] ? "+61" + purchaser["phone"] : "";

      data.alternate_phone = purchaser["alternate_phone"]
        ? "+61" + purchaser["alternate_phone"]
        : "";

      data.email = purchaser["email"];
      data.first_name = purchaser["first_name"];
      data.last_name = purchaser["last_name"];
      data.propertyId = purchaser["propertyId"];
      data.id = purchaser["id"];

      purchaserArr.push(data);
    });

    let param = {
      propertyId: apartment.id,
      purchasers: purchaserArr,
      solicitor: fromPairs(Array.from(solicitorFormData.entries())),
      additionalInfo: fromPairs(Array.from(additionalInfoFormData.entries())),
      isSubmit: isSubmit
    };

    try {
      axios.post("/api/sales-advice", param).then(res => {
        setLoading(false);
        if (res.status === 200 && !res.data.errorCode) {
          setPurchasers(purchasersMapper(res.data[0].original.purchasers));
          setSolicitorErrors([]);
          setPurchaserErrors([]);
          sweetAlert("success", "Sales advice successfully saved.");
        }
        if (res.data.errorCode === 422) {
          setLoading(false);
          if (!isEmpty(res.data.solicitorValidator)) {
            setSolicitorErrors(res.data.solicitorValidator);
          }
          if (!isEmpty(res.data.purchaserValidator)) {
            setPurchaserErrors(res.data.purchaserValidator);
          }
        }
      });
    } catch (error) {
      setLoading(false);
    }
  };

  return (
    <Dialog
      // TransitionComponent={Transition}
      transitionDuration={400}
      maxWidth={`sm`}
      keepMounted
      disableEnforceFocus={allowOutsideInput}
      fullScreen={isMobile ? true : false}
      open={show}
      onClose={() => onClose()}
      fullWidth={false}
      maxWidth={isMobile ? false : maxWidth}
      scroll={`paper`}
      PaperProps={{ style: styles.paperProps }}
    >
      <DialogTitle>
        <div
          className={`text-gray-700 font-bold text-4xl lg:text-4xl mt-10 lg:px-2 leading-none`}
        >
          Sales Advice
        </div>

        <IconButton
          className={`p-3 text-gray-700`}
          onClick={() => onClose()}
          style={{ right: 12, top: 12, position: "absolute" }}
        >
          <CloseIcon />
        </IconButton>
      </DialogTitle>

      <DialogContent>
        {purchasers.length > 0 && (
          <PurchaserDetails
            showHeader={false}
            setPurchasers={setPurchasers}
            purchasers={purchasers}
            errors={purchaserErrors}
            loading={loading}
            showLink={false}
          />
        )}
        {apartment && (
          <SalesAdvicePropertyDetails
            project={mapDeal()}
            apartment={apartment}
            deal={deal}
          />
        )}
        <SolicitorDetails
          solicitorErrors={solicitorErrors}
          solicitorForm={solicitorForm}
        />
        <AdditionalInfos
          additionalInfoErrors={additionalInfoErrors}
          additionalInfoForm={additionalInfoForm}
        />
      </DialogContent>
      <div className={`flex justify-center lg:justify-end mb-10 mr-6 mt-8`}>
        <Button
          onClick={() => handleSubmit(false)}
          className={`${
            loading ? "bg-gray-500" : "bg-palette-blue-light"
          } text-white mr-2 block button button-primary rounded-full`}
          disabled={loading}
        >
          {loading && (
            <FontAwesomeIcon
              icon={["fas", "spinner"]}
              className={`fa-spin mr-2`}
            />
          )}
          Save
        </Button>
        <Button
          onClick={onClose}
          className={`bg-red-600 button button-primary font-bold mr-2 rounded-full h-10`}
          disabled={loading}
        >
          Cancel
        </Button>
        <Button
          onClick={() => handleSubmit(true)}
          className={`block button button-primary rounded-full`}
          disabled={loading}
        >
          {loading && (
            <FontAwesomeIcon
              icon={["fas", "spinner"]}
              className={`fa-spin mr-2`}
            />
          )}
          Submit
        </Button>
      </div>
    </Dialog>
  );
};

const styles = {
  paperProps: {
    backgroundColor: "#fff"
  }
};

export default SalesAdviceModal;
