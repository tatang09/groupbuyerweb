import React from "react";
import { Modal } from "~/components/_base";
import UserGlobal from "~/states/userGlobal";

const TermsAndConditionsModal = ({ handleClose }) => {
  // const TermsAndConditionModal = () => {
  const [userState] = UserGlobal();

  return (
    <Modal
      show={userState.showTermsAndConditions}
      title={`Terms and Conditions`}
      onClose={() => handleClose()}
      transition={`grow`}
      maxWidth={`md`}
    >
      <section
        className="text-white bg-palette-blue-dark"
        style={{ marginTop: -130, paddingTop: 160 }}
      >
        <div className={`mx-auto`} style={{ maxWidth: 1366 }}>
          <div className="bg-palette-blue-dark mx-auto pb-10 lg:pt-16 px-4 text-white">
            {/* <h1 className={"font-extrabold lg:text-5xl mb-5 text-3xl text-center"}>Terms and Conditions</h1> */}
            <h2 className="font-extrabold lg:text-4xl mb-4 text-2xl">USER</h2>

            <p>
              Group Buyer Pty Ltd ACN 641 556 470 (Group Buyer) has the right to
              operate the Group Buyer website (Website). These Terms and
              Conditions apply to your use of the Website. Your use of the
              Website including accessing or browsing the Website or uploading
              any material to the Website is deemed to be your acceptance of
              these Terms and Conditions. Please read these carefully and if you
              do not accept the Terms and Conditions set out below please exit
              the Website and refrain from using the Website.
            </p>
            <p>
              If you breach any provision of these Terms and Conditions your
              right to access the Website will be immediately revoked. By
              registering as a Group Buyer account member and using the Website
              you acknowledge that you are at least eighteen (18) years of age
              and you agree to be bound by these Terms and Conditions.
            </p>

            <h4 className="text-2xl mb-3 mt-8 font-bold">
              1. Intellectual Property
            </h4>
            <p>
              Group Buyer uses the word ‘Group Buyer’ as a trade mark in
              Australia (Trade Mark). You are not permitted to use the Trade
              Mark or any other trademarks which belong or are licensed to Group
              Buyer or any of Group Buyer’ Intellectual Property which includes
              but is not limited to copyright material, confidential
              information, designs and other content located on the Website
              without Group Buyer’ prior written authorisation.
            </p>
            <p>
              The information, images and text on the Website may be subject to
              copyright and are the property of or otherwise property that is
              licensed to Group Buyer. Trade marks used on the Website to
              describe other companies and their products or services are trade
              marks which belong to or are licensed to those companies (Other
              Trade Marks). The Other Trade Marks are displayed on the Website
              to provide information to you about other products or services via
              the Website. You agree not to download, copy, reproduce or in any
              way use the Other Trade Marks without authorisation from the
              owner/s of the Other Trade Marks.
            </p>
            <p>
              You acknowledge that you are expressly prohibited from using the
              phrase ‘Group Buyer’ or any part of the website domain name
              without the written consent of Group Buyer. You agree that you are
              prohibited from reverse engineering the Website or otherwise
              attempting to copy the Website functionality or obtain from the
              Website information or data including but not limited to user’s
              personal information.
            </p>

            <h4 className="text-2xl mb-3 mt-8 font-bold">
              2. Information and Liability
            </h4>
            <p>
              You acknowledge that Group Buyer is not liable or responsible for
              any errors or omissions in the content of the Website. The
              information contained on the Website is provided for general
              information purposes only. Group Buyer makes no warranties as to
              the accuracy or completeness of the recommendations, information
              or material contained on the Website.
            </p>
            <p>
              You acknowledge that there are risks associated with using the
              internet and World Wide Web and to the fullest extent permitted by
              law, Group Buyer or its affiliates or subsidiaries are not liable
              for any direct or indirect damages or losses arising by way of
              your use or access to the Website.
            </p>
            <p>
              You acknowledge that Group Buyer makes no warranty that the
              Website will meet your user requirements or that the information
              contained on the Website or your use of the Website will be
              uninterrupted, timely, secure, error free, accurate, virus-free or
              compliant with legislation or regulations.
            </p>
            <p>
              You acknowledge and agree that Group Buyer have no liability of
              any kind either express or implied with respect to any claims
              arising in any way from your use of the Website, by virtue of the
              information provided or contained on the Website. The Website
              contains general statements regarding goods and/or services
              offered by other businesses however it is your responsibility to
              ensure the goods and/or services are the product you wish to
              purchase and must review the relevant product or service documents
              supplied by the relevant businesses.
            </p>
            <p>
              You acknowledge that Group Buyer may from time to time improve or
              make changes to the Website to upgrade security on the Website,
              update information on the Website or for any other reason in Group
              Buyer’s sole discretion.
            </p>
            <p>
              You acknowledge that you are responsible for ensuring the
              protection of your electronic equipment when accessing or browsing
              the Website and unreservedly hold Group Buyer harmless in respect
              to damage sustained to your electronic equipment or losses
              incurred by you as a result of the Website.
            </p>

            <h4 className="text-2xl mb-3 mt-8 font-bold">3. Privacy</h4>
            <p>
              You acknowledge that during your use of the Website you may
              provide or upload certain personal information, photos, images or
              written material to the Website. Group Buyer is committed to
              preserving the privacy of its users and will ensure that all
              personal information collected will be kept secure and in
              accordance with any applicable privacy laws. By using the Website,
              you agree to provide certain personal information necessary to
              utilise the Website to its full capability.
            </p>
            <p>
              Whilst Group Buyer agrees to ensure that all personal information
              is kept secure, you agree that Group Buyer may provide your
              personal information to other companies for the purpose of dealing
              with your property request. You acknowledge that Group Buyer will
              receive remuneration for providing your personal information to
              relevant third-party businesses.
            </p>
            <p>
              Group Buyer reserves the right to cooperate fully with any law
              enforcement authorities relating to any lawful request for the
              disclosure of personal information of its users. You acknowledge
              that you have read and agree to the terms of Group Buyer’s{" "}
              <a href="#">.Privacy Policy</a>
            </p>

            <h4 className="text-2xl mb-3 mt-8 font-bold">4. Use of Website</h4>
            <p>
              You acknowledge that your use of the Website is undertaken at your
              own risk and you unreservedly indemnify Group Buyer from any
              responsibility in respect of information or content listed on the
              Website or any linked Websites not controlled by Group Buyer.
            </p>
            <p>
              You agree that you will not engage in any activity that is
              disruptive or is detrimental to the operation or function of the
              Website. You are prohibited from displaying or transmitting
              unlawful, threatening, defamatory, obscene, or profane material on
              the Website. Group Buyer may prevent you from accessing the
              Website if in Group Buyer’ sole opinion you have violated or acted
              inconsistently with these Terms and Conditions.
            </p>
            <h4 className="text-2xl mb-3 mt-8 font-bold">
              5. Links and Advertisements
            </h4>
            <p>
              You acknowledge that any links or advertisements containing links
              to external websites that are provided or located on the Website
              are for convenience only. Group Buyer does not endorse or make any
              warranty with respect to any external websites or to the accuracy
              or reliability of the links or advertisements contained on the
              Website.
            </p>
            <p>
              You acknowledge that items, resources or learning material which
              are advertised or displayed on the Website may not be available to
              purchase or download at the time you access the Website.
            </p>

            <h4 className="text-2xl mb-3 mt-8 font-bold">6. Security</h4>
            <p>
              Group Buyer, from time to time, may use technology to improve
              security on the Website. You agree to cooperate with Group Buyer
              and you are responsible to check and read the amended Terms and
              Conditions.
            </p>

            <h4 className="text-2xl mb-3 mt-8 font-bold">
              7. Cookies and Viruses
            </h4>
            <p>
              Group Buyer uses its best endeavours to ensure the use of the best
              technology to protect its users, but accepts no responsibility in
              relation to any damage to any electronic equipment used by you,
              including mobile devices, as a result of your use of the Website.
              You acknowledge that you are responsible for ensuring the
              protection of your electronic equipment and unreservedly indemnify
              Group Buyer from any responsibility in respect of damage to any
              electronic equipment as a result of your use of the Website.
            </p>

            <h4 className="text-2xl mb-3 mt-8 font-bold">8. Payment</h4>
            <p>
              You acknowledge that you may be charged for the services you
              receive from using the Website. You agree to provide Group Buyer
              with at least one payment method supported by Group Buyer upon
              request from Group Buyer. You acknowledge that Group Buyer may
              vary the amount charged for a service at any time and at the sole
              discretion of Group Buyer.
            </p>

            <h4 className="text-2xl mb-3 mt-8 font-bold">9. General</h4>
            <p>
              If any part of these Terms and Conditions are held to be illegal
              or unenforceable, you authorise Group Buyer to amend or sever that
              part of the Terms and Conditions to make that part legal and
              enforceable or to ensure that the remaining parts of the Terms and
              Conditions remain in full force and effect.
            </p>
            <p>
              You acknowledge that Group Buyer may vary these Terms and
              Conditions and you agree to be bound by that variation from that
              time when that variation is advised. You understand that it is
              your responsibility to regularly check these Terms and Conditions
              so that you are aware of any variations which apply. Your
              continued use of the Website constitutes your agreement to these
              Terms and Conditions and any variations to these Terms and
              Conditions.
            </p>

            <h4 className="text-2xl mb-3 mt-8 font-bold">10. Applicable Law</h4>
            <p>
              You agree that the content of the Website is intended for
              Australian residents only and may not be available in your
              Country. The laws of the State of New South Wales will apply with
              respect to these Terms and Conditions and your use of the Website.
              You submit to the exclusive jurisdiction of the applicable Courts
              located in the State of New South Wales, Australia with respect to
              the Website and these Terms and Conditions.
            </p>
          </div>
        </div>
      </section>
    </Modal>
  );
};

export default TermsAndConditionsModal;
