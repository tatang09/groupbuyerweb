import React, { useEffect, useState } from "react";
import { Form } from "~/components/_base";
import { solicitorDetailsMapper } from "~/helpers/salesAdviceHelper/salesAdviceHelper";
 
const SolicitorDetails = ({ solicitorErrors, solicitorForm, width = `w-full` }) => {

  useEffect(() => {}, []);

  return (
    <section className={`relative mt-6 rounded`}>
      <div className={`mx-auto`} style={{ maxWidth: 1366 }}>
        <div className={`flex justify-center mt-4 mb-8 ${width} lg:pr-10`}>
          <div className={`border lg:mt-0 mt-8 px-6 rounded w-full`}>
            <h1 className={`font-bold py-6 text-base`}>
              Purchaser's solicitor details
            </h1>
            <form ref={solicitorForm}>
              <Form
                errors={solicitorErrors}
                formFields={solicitorDetailsMapper({})}
              />
            </form>
          </div>
        </div>
      </div>
    </section>
  );
};

export default SolicitorDetails;
