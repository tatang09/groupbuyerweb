import React from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

const triple_chevron_down_purple =
    "/assets/images/triple-chevron-down-purple.svg";

const HowItWorksComponent = () => {
    return (
        <div className={"flex flex-col pt-20 pb-20 px-12 xs:pt-10 xs:px-4 xs:pb-10 xs:bg-center bg-palette-black-main"}>
            <span className={"font-bold text-white text-7xl text-center xs:text-4xl xs:leading-tight how-it-works-header"}>How it Works?</span>
            <div className={"mt-10 lg:mt-10 flex flex-col items-center justify-around lg:mt-20 lg:mb-20 xl:flex-row mb-10 lg:mb-0"}>
                <div className={"flex items-center justify-center"}>
                    <img src="assets/svg/how-it-works-1.svg" className="lg:w-64" id="imageSizes-HIW" />
                </div>
                <div className={"flex items-center justify-center"}>
                    <img src={triple_chevron_down_purple} className="lg:-rotate-90 lg:transform xl:mt-10 mt-0 xl:block hidden xs:w-8 lg:w-16 p-1 hiw-arrow-down" />
                    <img src={triple_chevron_down_purple} className={"w-6 xl:hidden block xs:my-2"} />
                    {/* <img src="assets/svg/how-it-works_tripple_chevron_down.svg" className="lg:hidden md:hidden" id="imageSize-HIW-arrow-down" /> */}
                </div>
                <div className={"flex items-center justify-center"}>
                    <img src="assets/svg/how-it-works-2.svg" className={"lg:hidden md:hidden xs:my-4"} id="imageSizes-HIW2" />
                    <img src="assets/svg/how-it-works-2.svg" className="lg:w-64" className={"xs:hidden -mt-4"} id="imageSizes-HIW2" />
                </div>
                <div className={"flex items-center justify-center"}>
                    <img src={triple_chevron_down_purple} className="lg:-rotate-90 lg:transform xl:mt-10 mt-0 xl:block hidden xs:w-8 lg:w-16 p-1 hiw-arrow-down" />
                    <img src={triple_chevron_down_purple} className={"w-6 xl:hidden block xs:my-2"} />
                    {/* <img src="assets/svg/how-it-works_tripple_chevron_down.svg" className="lg:hidden md:hidden" id="imageSize-HIW-arrow-down" /> */}
                </div>
                <div className={"flex items-center justify-center"}>
                    <img src="assets/svg/how-it-works-3.svg" className={"lg:hidden md:hidden xs:my-4"} id="imageSizes-HIW3" />
                    <img src="assets/svg/how-it-works-3.svg" className="lg:w-64" className={"xs:hidden -mt-5"} id="imageSizes-HIW3" />
                </div>
            </div>
            <div className={"max-content-width-lg flex flex-col items-start justify-center lg:flex-row px-20 xs:px-6 m-auto"}>
                <div className={"flex-1 p-6 xs:p-1"}>
                    <div className={"flex justify-between items-center text-white leading-tight font-bold text-3xl"}>
                        <p>Power of <br className={"xs:hidden"} />Community</p>
                        <FontAwesomeIcon icon={['fas', 'plus']} size={`xs`} className="xs:hidden" />
                    </div>
                    <p className={"text-base font-light text-white xs:leading-tight"}>
                        GroupBuyer is a community of like-minded people, all sharing the passion for property ownership. We give you the opportunity to use our collective power to buy the home you deserve.
                </p>
                    {/* <ol className={"why-groupbuyer-ol text-base text-white font-light xs:leading-tight"}>
                        <li><b className={"text-palette-purple-main font-bold text-2xl xs:text-lg"}>1.</b> Lorem ipsum dolor sit amet</li>
                        <li><b className={"text-palette-purple-main font-bold text-2xl xs:text-lg"}>2.</b> consectetur adipiscing elit</li>
                        <li><b className={"text-palette-purple-main font-bold text-2xl xs:text-lg"}>3.</b> ut turpis in velit ultricies</li>
                        <li><b className={"text-palette-purple-main font-bold text-2xl xs:text-lg"}>4.</b> Morbi viverra libero quis</li>
                    </ol> */}
                    <div className={"text-center text-white pt-5 text-3xl"}><FontAwesomeIcon icon={['fas', 'plus']} size={`sm`} className="lg:hidden md:hidden" /></div>
                </div>

                <div className={"flex-1 p-6 xs:p-1"}>
                    <div className={"flex justify-between items-center text-white leading-tight font-bold text-3xl"}>
                        <p>Power of <br className={"xs:hidden"} />Purchase</p>
                        <FontAwesomeIcon icon={['fas', 'plus']} size={`xs`} className="xs:hidden" />
                    </div>
                    <p className={"text-base font-light text-white xs:leading-tight"}>
                        The GroupBuyer community gives us the power to negotiate unbeatable prices on a range of premium properties. The more we are, the more we negotiate, the more you save.
                </p>
                    {/* <ol className={"why-groupbuyer-ol text-base text-white font-light xs:leading-tight"}>
                        <li><b className={"text-palette-purple-main font-bold text-2xl xs:text-lg"}>1.</b> Lorem ipsum dolor sit amet</li>
                        <li><b className={"text-palette-purple-main font-bold text-2xl xs:text-lg"}>2.</b> consectetur adipiscing elit</li>
                        <li><b className={"text-palette-purple-main font-bold text-2xl xs:text-lg"}>3.</b> ut turpis in velit ultricies</li>
                        <li><b className={"text-palette-purple-main font-bold text-2xl xs:text-lg"}>4.</b> Morbi viverra libero quis</li>
                    </ol> */}
                    <div className={"text-center text-white pt-5 text-3xl"}><FontAwesomeIcon icon={['fas', 'plus']} size={`sm`} className="lg:hidden md:hidden" /></div>
                </div>

                <div className={"flex-1 p-6 xs:p-1"}>
                    <div className={"flex justify-between items-center text-white leading-tight font-bold text-3xl"}>
                        <p>Power of <br className={"xs:hidden"} />Knowledge</p>
                        <FontAwesomeIcon icon={['fas', 'equals']} size={`xs`} className="xs:hidden" />
                    </div>
                    <p className={"text-base font-light text-white xs:leading-tight"}>
                        Years of experience, expertise and relationships with the country’s most trusted developers and real estate agents, all at your disposal. Using our knowledge of the property market, we bring the best deals to you.
                </p>
                    {/* <ol className={"why-groupbuyer-ol text-base text-white font-light xs:leading-tight"}>
                        <li><b className={"text-palette-purple-main font-bold text-2xl xs:text-lg"}>1.</b> Lorem ipsum dolor sit amet</li>
                        <li><b className={"text-palette-purple-main font-bold text-2xl xs:text-lg"}>2.</b> consectetur adipiscing elit</li>
                        <li><b className={"text-palette-purple-main font-bold text-2xl xs:text-lg"}>3.</b> ut turpis in velit ultricies</li>
                        <li><b className={"text-palette-purple-main font-bold text-2xl xs:text-lg"}>4.</b> Morbi viverra libero quis</li>
                    </ol> */}
                    <div className={"text-center text-white pt-5 text-3xl"}><FontAwesomeIcon icon={['fas', 'equals']} size={`sm`} className="lg:hidden md:hidden" /></div>
                </div>
                <div className={"flex-1 p-6 xs:p-1"}>
                    <div className={"flex justify-between items-center text-palette-purple-main leading-tight font-bold text-3xl"}>
                        <p>The One Stop<br className={"xs:hidden"} /> Shop Solution</p>
                    </div>
                    <p className={"text-base font-light text-white xs:leading-tight"}>
                        GroupBuyer is the destination for all things property. Want to know how the property market is changing? Are you just starting out in your property investment journey? GroupBuyer representatives are always available to help you.
                </p>
                    {/* <ol className={"why-groupbuyer-ol text-base text-white font-light xs:leading-tight"}>
                        <li><b className={"text-palette-purple-main font-bold text-2xl xs:text-lg"}>1.</b> Lorem ipsum dolor sit amet</li>
                        <li><b className={"text-palette-purple-main font-bold text-2xl xs:text-lg"}>2.</b> consectetur adipiscing elit</li>
                        <li><b className={"text-palette-purple-main font-bold text-2xl xs:text-lg"}>3.</b> ut turpis in velit ultricies</li>
                        <li><b className={"text-palette-purple-main font-bold text-2xl xs:text-lg"}>4.</b> Morbi viverra libero quis</li>
                    </ol> */}
                </div>
            </div>
        </div>
    )
}

export default HowItWorksComponent;
