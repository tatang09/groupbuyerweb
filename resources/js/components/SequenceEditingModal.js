import React, { useEffect, useState } from "react";
import EditSteps from "./EditSteps";
import EditProjectPart from "./EditProjectPart";
import AgencyAgreementPart from "./AgencyAgreementPart";
import EditProjectResources from "../admin/components/EditProjectResources";
import DeveloperPropertyManager from "./DeveloperPropertyManager";
import { Modal } from "../components/_base";
import UserGlobal from "../states/userGlobal";
 
const SequenceEditingModal = ({
  project,
  toggleSequenceEditModal,
  editToggleFetch
}) => {
  const [userState, userActions] = UserGlobal();
  const [projectView, setProjectView] = useState(true);
  const [resourcesView, setResourcesView] = useState(false);
  const [propertyView, setPropertyView] = useState(false);
  const [agreementView, setAgreementView] = useState(false);

  useEffect(() => {
    userActions.setState({ projectData: project });
    userActions.setState({ currentProjStep: 1 });
  }, []);

  const toggleProjV = () => {
    setProjectView(true);
    setResourcesView(false);
    setPropertyView(false);
    setAgreementView(false);
  };

  const toggleResV = () => {
    setResourcesView(true);
    setPropertyView(false);
    setProjectView(false);
    setAgreementView(false);
  };

  const togglePropView = () => {
    setPropertyView(true);
    setProjectView(false);
    setResourcesView(false);
    setAgreementView(false);
  };

  const toggleAgreeView = () => {
    setPropertyView(false);
    setProjectView(false);
    setResourcesView(false);
    setAgreementView(true);
  };

  return (
    <Modal
      show={true}
      disableBackdropClick={true}
      topOff={true}
      maxWidth={`md`}
      title={`Edit Project`}
      onClose={() => toggleSequenceEditModal()}
    >
      <EditSteps
        activeProjectView={projectView}
        activeResourcesView={resourcesView}
        activePropertyView={propertyView}
        activeAgreementView={agreementView}
        toggleProjectView={() => toggleProjV()}
        toggleResourcesView={() => toggleResV()}
        togglePropertyView={() => togglePropView()}
        toggleAgreementView={() => toggleAgreeView()}
      />
      {projectView && <EditProjectPart toggleFetch={editToggleFetch} />}
      {resourcesView && <EditProjectResources toggleFetch={editToggleFetch} />}
      {propertyView && (
        <DeveloperPropertyManager
          toggleCloseModal={toggleSequenceEditModal}
          toggleFetch={editToggleFetch}
          toggleAgreementView={toggleAgreeView}
        />
      )}
      {agreementView && <AgencyAgreementPart toggleFetch={editToggleFetch}/>}
    </Modal>
  );
};

export default SequenceEditingModal;
