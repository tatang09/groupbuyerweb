import React from "react";

const LandProjectSize = ({ apartment }) => {
  

  return (
    <div className={`mt-10 border-gray-500 border-b-2 pb-8`}>
      <span className={`text-base font-bold`}>
        Size
      </span>
      <div className={`flex flex-col lg:flex-row items-center mt-4`}>
      <div
          className={`flex-1 font-semibold text-base text-gray-700`}
        >{`Size: ${
          apartment.size ? apartment.size : 0
        }sqm`}</div>

        <div
          className={`flex-1 font-semibold text-base text-gray-700`}
        >{`Frontage: ${
          apartment.frontage ? apartment.frontage : 0
        }sqm`}</div>

        <div
          className={`flex-1 font-semibold text-base text-gray-700`}
        >{`Width: ${
          apartment.width ? apartment.width : 0
        }sqm`}</div>

        <div
          className={`flex-1 font-semibold text-base text-gray-700`}
        >{`Depth: ${
          apartment.depth ? apartment.depth : 0
        }sqm`}</div>
      </div>
    </div>
  );
};

export default LandProjectSize;
