import React from 'react';
import ProgressRing from './ProgressRing';
import ProgressBarSingle from './ProgressBarSingle';
import ProgressBarDouble from './ProgressBarDouble';

const NeighbourhoodInsights = ({}) => {
    return (
        <div className={"block lg:py-12 lg:px-12 xs:p-8 lg:w-1/2 xs:w-full space-y-8"}>
            <div className={"block w-full space-y-6"}>
                <div className={"block w-full space-y-1"}>
                    <div className={"font-bold leading-none lg:text-4xl xs:text-2xl"}>
                        Neighbourhood Insights
                    </div>
                    <div className={"font-light text-xs text-gray-600"}>
                        A little bit about who lives locally, as provided by government census data
                    </div>
                    <button className={"flex mt-3 rounded text-sm xs:w-full font-bold py-2 text-palette-purple-main uppercase whitespace-pre items-center"}>
                        <img src="/assets/images/map.svg" className={"xs:w-6 lg:w-8 mr-3"} />
                        View neighbourhood map here
                    </button>
                </div>
                <div className={"block w-full"}>
                    <div className={"flex w-full space-x-4"}>
                        <div className={"block lg:w-2/3 xs:w-3/5 space-y-2"}>
                            <div className={"flex w-full space-x-2"}>
                                <div className={"w-1/3 font-bold uppercase leading-none text-base"}>
                                    Age
                                </div>
                                <div className={"w-2/3 font-bold uppercase leading-none text-base"}>
                                    %
                                </div>
                            </div>
                            <div className={"block w-full space-y-3"}>
                                <div className={"flex w-full space-x-2 items-center"}>
                                    <div className={"w-1/3 font-bold uppercase leading-none lg:text-base xs:text-xs text-gray-700"}>
                                        Under 20
                                    </div>
                                    <div className={"w-2/3 font-bold uppercase leading-none text-base text-gray-700"}>
                                        <div className="w-full">
                                            <ProgressBarSingle
                                                progress={20}
                                            />
                                        </div>
                                    </div>
                                </div>
                                <div className={"flex w-full space-x-2 items-center"}>
                                    <div className={"w-1/3 font-bold uppercase leading-none lg:text-base xs:text-xs text-gray-700"}>
                                        20 - 39
                                    </div>
                                    <div className={"w-2/3 font-bold uppercase leading-none text-base text-gray-700"}>
                                        <div className="w-full">
                                            <ProgressBarSingle
                                                progress={15}
                                            />
                                        </div>
                                    </div>
                                </div>
                                <div className={"flex w-full space-x-2 items-center"}>
                                    <div className={"w-1/3 font-bold uppercase leading-none lg:text-base xs:text-xs text-gray-700"}>
                                        40 - 59
                                    </div>
                                    <div className={"w-2/3 font-bold uppercase leading-none text-base text-gray-700"}>
                                        <div className="w-full">
                                            <ProgressBarSingle
                                                progress={27}
                                            />
                                        </div>
                                    </div>
                                </div>
                                <div className={"flex w-full space-x-2 items-center"}>
                                    <div className={"w-1/3 font-bold uppercase leading-none lg:text-base xs:text-xs text-gray-700"}>
                                        60+
                                    </div>
                                    <div className={"w-2/3 font-bold uppercase leading-none text-base text-gray-700"}>
                                        <div className="w-full">
                                            <ProgressBarSingle
                                                progress={38}
                                            />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className={"block lg:w-1/3 xs:w-2/5 space-y-3"}>
                            <div className={"flex w-full"}>
                                <div className={"w-full font-bold uppercase leading-none text-base"}>
                                    Long term residents
                                </div>
                            </div>
                            <div className={"block w-full"}>
                                <div className={"w-full"}>
                                    <ProgressRing
                                        progress={50}
                                    />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div className={"block w-full space-y-6"}>
                    <div className={"block w-full space-y-4"}>
                        <div className={"flex justify-between w-full"}>
                            <div className={"font-bold uppercase leading-none text-base"}>
                                Owner
                            </div>
                            <div className={"font-bold uppercase leading-none text-base"}>
                                Renter
                            </div>
                        </div>
                        <div className={"flex justify-between w-full"}>
                            <div className="w-full">
                                <ProgressBarDouble
                                    progress={80}
                                />
                            </div>
                        </div>
                    </div>
                    <div className={"block w-full space-y-4"}>
                        <div className={"flex justify-between w-full"}>
                            <div className={"font-bold uppercase leading-none text-base"}>
                                Family
                            </div>
                            <div className={"font-bold uppercase leading-none text-base"}>
                                Single
                            </div>
                        </div>
                        <div className={"flex justify-between w-full"}>
                            <div className="w-full">
                                <ProgressBarDouble
                                    progress={64}
                                />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default NeighbourhoodInsights;