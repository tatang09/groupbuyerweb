import React, {useState} from "react";
import UserLayout from "~/layouts/users/UserLayout";
import { Button, Modal, TextInput } from "~/components/_base";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

const ForgetPassword = () => {
  const [errors, setErrors] = useState({});
  const handleSubmit = () => {};
  return (
    <UserLayout>
      <section
        className="text-white bg-palette-blue-dark"
        style={{ marginTop: -130, paddingTop: 160 }}
      >
        <div className={`mx-auto`} style={{ maxWidth: 1366 }}>
          <h1 className={`font-bold leading-10 pb-20 pt-24 text-4xl`}>
            Get in touch.
          </h1>
          <TextInput
          className={`text-base`}
          placeholder={`Password`}
          name={`password`}
          prefix={`mail`}
          errors={errors.password}
        />
        {errors.password && (
          <span className="text-red-500 text-xs">{errors.password[0]}</span>
        )}

          <Button className={`px-20`} disabled={loading}>
            {loading && (
              <FontAwesomeIcon
                icon={["fas", "spinner"]}
                className={`fa-spin mr-2`}
              />
            )}
            Log in
          </Button>
        </div>
      </section>
    </UserLayout>
  );
};

export default ForgetPassword;
