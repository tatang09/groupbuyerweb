import React from 'react';

const TalkExpert = ({}) => {
    return (
        <div className={"block lg:w-1/2 h-full"}>
            <div className={"block lg:py-12 lg:px-12 xs:p-8 h-2/3 bg-palette-gray-dark w-full"}>
              <div className={"flex w-full lg:space-x-8 xs:space-x-4 items-center"}>
                <div className={"block w-1/4"}>
                  <div 
                    style={{
                        backgroundImage: "url('/assets/images/_directors/Luke_Hayes_web.png')",
                    }}
                    className={"rounded-md shadow-lg items-end bg-cover bg-center bg-no-repeat"}
                  >
                    <div className={"lg:py-32 xs:py-8"}>
                    </div>
                  </div>
                </div>
                <div className={"block w-3/4 lg:space-y-4 xs:space-y-1"}>
                  <span className={"font-bold leading-none text-white lg:text-4xl xs:text-lg"}>
                    Talk to an expert
                  </span>
                  <p className={"font-normal lg:text-base xs:text-xs text-white"}>
                    Morbi viverra libero quis est porttitor tincidunt. 
                  </p>
                  <button className={"flex w-full rounded leading-none text-palette-purple-main lg:text-base xs:text-sm font-bold xs:whitespace-pre items-center"}>
                    Schedule a call with Luke
                    <span>
                      <img src="/assets/images/call.svg" className={"lg:w-5 ml-2 xs:hidden"} />
                    </span>
                  </button>
                </div>
                <div className={"lg:hidden xs:w-auto"}>
                  <div className={"h-auto w-full"}>
                    <span className={"font-bold pb-2 leading-none text-4xl"}>
                      <img src="/assets/images/call.svg" className={"w-24 h-auto"} />
                    </span>
                  </div>
                </div>
              </div>
            </div>
            <div className={"block lg:py-12 lg:px-12 xs:p-8 h-1/3 bg-white w-full"}>
              <p className={"font-bold uppercase text-sm text-palette-purple-main"}>
                Be in the know
              </p>
              <div className={"lg:flex xs:block w-full lg:space-x-4 xs:space-y-4"}>
                <button className={"rounded-lg shadow-lg flex whitespace-pre items-center text-white text-xl font-bold py-4 justify-between px-6 bg-palette-violet-main-dark xs:w-full lg:w-1/2"}>
                  Developer Profile
                  <img src="/assets/images/dev.svg" className={"w-8"} />
                </button>
                <button className={"rounded-lg shadow-lg flex justify-between whitespace-pre items-center text-white text-xl font-bold py-4 px-6 bg-palette-violet-main-dark xs:w-full lg:w-1/2"}>
                  Builder Profile
                    <img src="/assets/images/builder.svg" className={"w-8"} />
                </button>
              </div>
            </div>
        </div>
    )
}

export default TalkExpert;