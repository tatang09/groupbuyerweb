import React from "react";
import { Modal } from "~/components/_base";
import MediaPlayer from "./MediaPlayer";

const MediaPlayerContainer = ({ toogleShowPlayerModal, url, show }) => {
  return (
    <Modal
      show={show}
      maxWidth={`md`}
      title={`Project video`}
      onClose={() => toogleShowPlayerModal()}
    >
      <div className={`bg-white m-10 p-4 rounded`}>
        <MediaPlayer url={url}   />
      </div>
    </Modal>
  );
};

export default MediaPlayerContainer;
