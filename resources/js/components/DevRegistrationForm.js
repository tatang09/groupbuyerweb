import React, { useState, createRef, useRef } from 'react';
import { useForm } from '../hooks/customHooks'
import { Radio } from 'pretty-checkbox-react';
import { sweetAlert } from '../components/_base/alerts/sweetAlert';
import { setToken } from '~/services/auth';
import { isMobile } from 'react-device-detect';



const DevRegistrationForm = ({
    containerBgColor = "#4D0B98",
    containerRounded = false,
    subHeader = "LET'S WORK TOGETHER",
    headerTop = "Request a demonstration",
    headerBottom = "account for free",
    inputBgColor = "#FFFFFF5C",
    displayCloseButton = false,
    rightPadded = true,
    // checkboxClass = "landing_checkbox",
    textColorTheme = "light",
    opacity = "30%",
    handleCloseModal,
    isPopup = false
}) => {


    const devBenefitsArr = [
        { title: "Display current stock", paragh: "Showcase a selection of your properties to an audience ready to buy" },
        { title: "Raise brand awareness", paragh: "Get a bunch of fresh eyes across your brand or project " },
        { title: "Promote opportunities", paragh: "No need to wait for a special occasion, promote your opportunities right now! " },
        { title: "Secure cash flow", paragh: "Faster sales means less time on market. Less finance interest and less marketing spend." }
    ];

    const displayBenefits = (arr, titleColor, pColor) => {
        return arr.map((item, key) => {
            return <div key={key}>
                <span className={`font-bold leading-7 sm:text-2xl text-base ${titleColor}`}>{item.title}</span>
                <p className={`pt-2 ${pColor}`}>{item.paragh}</p>
            </div>
        });
    }


    const [errors, setErrors] = useState({});
    const [loading, setLoading] = useState(false);
    const [isDeveloper, setIsDeveloper] = useState(true);

    const [state, formChange] = useForm({
        first_name: "",
        last_name: "",
        email: "",
        phone: "",
        company_name: "",
        title: ""
    });

    const form = createRef();

    const handleSubmit = async e => {

        e.preventDefault();

        state.title = isDeveloper ? "Project Developer" : "Real Estate Agent";

        let errors = {};
        let formData = { ...state }

        setLoading(true);

        try {

            let { data } = await axios.post('/api/request-demo', formData);


            setLoading(false);

            sweetAlert(
                'success',
                'Thank you for sending a demo request.',
            );

            resetFields();

            if (isPopup) {
                handleCloseModal();
            }

        } catch (error) {

            let { data } = error.response;

            errors = data.errors;

            setLoading(false);
        }
        setErrors(errors || {});
    };

    const resetFields = () => {
        state.first_name = "";
        state.last_name = ""
        state.email = "";
        state.phone = "";
        state.company_name = "";
    }

    return (
        <div className={`dev-registration-form lg:overflow-x-visible lg:overflow-y-visible overflow-scroll bg-palette-violet-main-dark flex flex-col h-full lg:flex-row lg:h-auto lg:w-2/3`} style={{ zoom: isMobile ? 1 : 0.8 }}>
            <div className={`flex items-center justify-center p-8 flex-1`}>
                {displayCloseButton && isMobile && (
                    <img onClick={handleCloseModal} src={"/assets/svg/close_button.svg"} className={"absolute top-0 right-0 w-6 my-6 mx-8 cursor-pointer z-10"} />
                )}
                <div className={`flex flex-1 flex-col md:justify-center  `}>
                    <span className={`font-bold leading-none mb-8 text-4xl text-white`}>Benefits for Developers</span>
                    <p className={`text-white`}>GroupBuyer is designed to help - when help is needed! Whether you’re selling through a master-planned project or a smaller boutique development, we provide groups of buyers to help you maintain cash flow and meet monthly sales targets. Leave your details below and we will contact you for an in-depth demonstration of our platform.</p>
                    <div className={`flex flex-col sm:justify-center `}>{displayBenefits(devBenefitsArr, "text-palette-purple-main", "text-white")}</div>
                </div>
            </div>

            <div className={`flex items-center justify-center flex-1`}>
                <form
                    className={`w-full`}
                    ref={form}
                    onSubmit={handleSubmit}>

                    {/* <div className={`relative ${rightPadded ? 'lg:pr-24' : 'lg:pr-12'} pl-12 py-8 w-full xs:flex-1 xs:px-12 ${containerRounded ? 'rounded-lg' : ''}`} style={{ backgroundColor: containerBgColor }}> */}
                    <div className={`relative ${rightPadded ? 'lg:pr-24' : 'lg:pr-12'} pl-12 py-8 w-full xs:flex-1 xs:px-8`}
                        style={{
                            background:
                                `linear-gradient(45deg, rgb(255 255 255 / ${opacity}), rgb(255 255 255 / ${opacity}))`
                        }}>
                        {displayCloseButton && !isMobile && (
                            <img onClick={handleCloseModal} src={"/assets/svg/close_button.svg"} className={"absolute top-0 right-0 w-6 my-6 mx-8 cursor-pointer"} />
                        )}
                        <h3 className={`mb-2 ${textColorTheme == 'light' ? 'text-white' : 'text-gray-900'} font-bold`}>{subHeader}</h3>
                        <h2 className={"text-palette-purple-main font-bold leading-none mb-4 xs:text-3xl"} style={{ fontSize: "3rem" }}>{headerTop} <br />{headerBottom}</h2>
                        <div className={"flex xs:flex-col text-base mt-2 mb-4"}>
                            <div className={"flex-1 lg:pr-1 xs:mb-4"}>
                                <p className={`${textColorTheme == 'light' ? 'text-white' : 'text-gray-900'} mb-1`}>First Name</p>
                                <input className={`rounded-sm ${textColorTheme == 'light' ? 'text-white' : 'text-gray-900'} placeholder-white py-2 px-4 mr-2 w-full`}
                                    name='first_name'
                                    value={state.first_name}
                                    onChange={formChange}
                                    style={{ backgroundColor: inputBgColor }} />
                                {errors.first_name && (
                                    <span className="text-red-500 text-xs">
                                        {errors.first_name[0]}
                                    </span>
                                )}
                            </div>
                            <div className={"flex-1 lg:pl-1"}>
                                <p className={`${textColorTheme == 'light' ? 'text-white' : 'text-gray-900'} mb-1`}>Last Name</p>
                                <input className={`rounded-sm ${textColorTheme == 'light' ? 'text-white' : 'text-gray-900'} placeholder-white py-2 px-4 mr-2 w-full`} name='last_name' value={state.last_name} onChange={formChange} style={{ backgroundColor: inputBgColor }}></input>
                                {errors.last_name && (
                                    <span className="text-red-500 text-xs">
                                        {errors.last_name[0]}
                                    </span>
                                )}
                            </div>
                        </div>
                        <div className={"flex text-base mb-4"}>
                            <div className={"flex-1"}>
                                <p className={`${textColorTheme == 'light' ? 'text-white' : 'text-gray-900'} mb-1`}>Email</p>
                                <input className={`rounded-sm ${textColorTheme == 'light' ? 'text-white' : 'text-gray-900'} placeholder-white py-2 px-4 mr-2 w-full`} name='email' value={state.email} onChange={formChange} style={{ backgroundColor: inputBgColor }}></input>
                                {errors.email && (
                                    <span className="text-red-500 text-xs">{errors.email[0]}</span>
                                )}
                            </div>
                        </div>
                        <div className={"flex xs:flex-col text-base mb-4"}>
                            <div className={"flex-1 lg:pr-1 xs:mb-4"}>
                                <p className={`${textColorTheme == 'light' ? 'text-white' : 'text-gray-900'} mb-1`}>Phone</p>
                                <input className={`rounded-sm ${textColorTheme == 'light' ? 'text-white' : 'text-gray-900'} placeholder-white py-2 px-4 mr-2 w-full`} name='phone' value={state.phone} onChange={formChange} style={{ backgroundColor: inputBgColor }}></input>
                                {errors.phone && (
                                    <span className="text-red-500 text-xs">{errors.phone[0]}</span>
                                )}
                            </div>
                            <div className={"flex-1 lg:pl-1"}>
                                <p className={`${textColorTheme == 'light' ? 'text-white' : 'text-gray-900'} mb-1`}>Company Name</p>
                                <input className={`rounded-sm ${textColorTheme == 'light' ? 'text-white' : 'text-gray-900'} placeholder-white py-2 px-4 mr-2 w-full`} name='company_name' value={state.company_name} onChange={formChange} style={{ backgroundColor: inputBgColor }}></input>
                                {errors.company_name && (
                                    <span className="text-red-500 text-xs">{errors.company_name[0]}</span>
                                )}
                            </div>
                        </div>
                        <div className={"flex text-sm mb-4 flex-col"}>
                            <Radio
                                onChange={() => setIsDeveloper(true)}
                                className={`text-base remember_me`}
                                shape={`round`}
                                color={`primary`}
                                name={`user_type`}
                                value={`project_developer`}
                                inputProps={{
                                    defaultChecked: true,
                                }}>
                                <i className={"ml-2 text-sm font-light"}>I'm a Property Developer</i>
                            </Radio>
                            <Radio
                                onChange={() => setIsDeveloper(false)}
                                className={`mt-4 text-base remember_me`}
                                shape={`round`}
                                color={`primary`}
                                name={`user_type`}
                                value={`real_estate_agent`}>
                                <i className={"ml-2 text-sm font-light"}>I'm a Real Estate Agent</i>
                            </Radio>
                        </div>
                        <div className={"flex text-base items-center mt-8 mb-4 xs:text-sm"}>
                            <button className={"py-2 rounded text-base text-white w-full xs:px-6 xs:py-3 xs:text-sm"} style={{ backgroundColor: '#E91AFC' }}>Let's discuss how to make great business</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    );
}

export default DevRegistrationForm;
