import React from "react";
import ReactPlayer from "react-player";
import { isMobile } from "react-device-detect";

const MediaPlayer = ({ url, className = "", height}) => {
  return (
    <ReactPlayer
      className={`${className}`}
      url={url}
      width={`100%`}
      height={height}
    />
  );
};

export default MediaPlayer;
