import React from "react";

const ReadyForOccupancy = () => {
  return (
    <div
      className={`flex font-bold h-8 items-center justify-center mb-2 mr-1 mr-2 mt-6 rounded-lg shadow-lg text-sm xs:w-2/3`}
      style={{ backgroundColor: "rgba(125, 196, 61, 0.2)" }}
    >
      <svg
        className={`feather-icon h-5 rounded-full p-1 text-white w-5`}
        style={{
          backgroundColor: "rgb(137, 176, 54)"
        }}
      >
        <use xlinkHref={`/assets/svg/feather-sprite.svg#check`} />
      </svg>
      <span className="font-bold pl-2 text-gray-900 text-sm">
        Ready for Occupancy
      </span>
    </div>
  );
};

export default ReadyForOccupancy;
