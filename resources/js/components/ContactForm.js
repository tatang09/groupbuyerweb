import React, { createRef, useState, useEffect } from "react";
import { Button, TextInput, TextArea, ResponseModal } from "~/components/_base";
import { axios } from "~/helpers/bootstrap";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import Snackbar from "@material-ui/core/Snackbar";
import MuiAlert from "@material-ui/lab/Alert";

function Alert(props) {
  return <MuiAlert elevation={6} variant="filled" {...props} />;
}

const ContactForm = ({ backgroundColor = "#ffffff" }) => {
  

  return (
    <form
      method="POST"
      action="https://groupbuyer.activehosted.com/proc.php"
      id="_form_3_"
      style={{ background: backgroundColor }}
    >
      <div className="py-2 pt-6 pb-10 rounded-lg">
        <input type="hidden" name="u" value="3" />
        <input type="hidden" name="f" value="3" />
        <input type="hidden" name="s" />
        <input type="hidden" name="c" value="0" />
        <input type="hidden" name="m" value="0" />
        <input type="hidden" name="act" value="sub" />
        <input type="hidden" name="v" value="2" />
        <div className="_form-content">
          <div className="_form_element _x66775207 _full_width ">
            <label className="_form-label font-bold font-sans">Name</label>
            <div className="_field-wrapper">
              <input type="text" name="fullname" placeholder="Name" />
            </div>
          </div>
          <div className="_form_element _x28906445 _full_width ">
            <label className="_form-label font-bold font-sans">Email *</label>
            <div className="_field-wrapper">
              <input
                type="text"
                pattern="[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]{2,4})+$"
                name="email"
                placeholder="Email"
                required
              />
            </div>
          </div>
          <div className="_form_element _x30740924 _full_width ">
            <label className="_form-label font-bold font-sans">Phone</label>
            <div className="_field-wrapper">
              <input
                type="text"
                name="phone"
                placeholder="Phone number"
              />
            </div>
          </div>
          <div className="_form_element _field52 _full_width ">
            <label className="_form-label font-bold font-sans">Message *</label>
            <div className="_field-wrapper">
              <textarea
                name="field[52]"
                placeholder=""
                style={{ height: 102 }}
                required
              />
            </div>
          </div>
          <div className="_button-wrapper _full_width mt-6">
            <button id="_form_3_submit" className="_submit w-full" type="submit" >
              Submit
            </button>
          </div>
          <div className="_clear-element"></div>
        </div>
        <div className="_form-thank-you" style={{ display: "none" }}></div>
        {/* <div className="_form-branding">
          <div className="_marketing-by">Marketing by</div>
          <a
            href="http://www.activecampaign.com"
            target="#"
            className="_logo"
          ></a>
        </div> */}
      </div>
    </form>
  );
};

export default ContactForm;
