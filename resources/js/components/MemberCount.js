import React from "react";
 
import UserGlobal from "~/states/userGlobal";

const MemberCount = ({ append }) => {
  const [userState, userAction] = UserGlobal();

  return (
    <div
      className={`${append}   border flex font-bold h-8 items-center justify-center pt-3 px-4 rounded-full text-base text-palette-teal w-auto`}
    >
      Members:&nbsp;
      {userState.userCount ? userState.userCount : 0 }
    </div>
  );
};

export default MemberCount;
