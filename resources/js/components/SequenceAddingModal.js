import React, { useEffect } from "react";
import AddingSteps from "./AddingSteps";
import AddProject from "./AddProject";
import AddProjectResources from "./AddProjectResources";
import { Modal } from "../components/_base";
import UserGlobal from "../states/userGlobal";
import DeveloperPropertyManager from "./DeveloperPropertyManager";
import AgencyAgreementPart from "./AgencyAgreementPart";

const SequenceAddingModal = ({ toggleSequenceAddingModal, addToggleFetch }) => {
  const [userState, userActions] = UserGlobal();

  useEffect(() => {
    userActions.setState({ currentProjStep: 1 });
    userActions.setState({ projectData: [] });
  }, []);

  return (
    <Modal
      show={true}
      disableBackdropClick={true}
      topOff={true}
      maxWidth={`md`}
      title={`Add Project`}
      onClose={() => toggleSequenceAddingModal()}
    >
      <AddingSteps />
      {userState.currentProjStep === 1 && (
        <AddProject toggleFetch={addToggleFetch} />
      )}
      {userState.currentProjStep === 2 && (
        <AddProjectResources toggleFetch={addToggleFetch} />
      )}
      {userState.currentProjStep === 3 && (
        <DeveloperPropertyManager
          toggleFetch={addToggleFetch}
          toggleCloseModal={toggleSequenceAddingModal}
        />
      )}
      {userState.currentProjStep === 4 && <AgencyAgreementPart />}
    </Modal>
  );
};

export default SequenceAddingModal;
