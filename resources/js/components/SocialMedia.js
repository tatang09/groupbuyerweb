import React from "react"
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome"

const SocialMedia = ({ variant = "primary", size = "lg", className }) => {

  const socialMediaArr = [
    {
      icon: <FontAwesomeIcon icon={["fab", "facebook-f"]} size={size}/>,
      color: "#3B5999",
      linkPath: 'https://www.facebook.com/GroupBuyer.com.au/'
    },
    {
      icon: <FontAwesomeIcon icon={["fab", "instagram"]} size={size}/>,
      color: "#019EFE",
      linkPath: 'https://www.instagram.com/groupbuyerau '
    },
    {
      icon: <FontAwesomeIcon icon={["fab", "linkedin-in"]} size={size}/>,
      color:  "#0077B5",
      linkPath: 'https://www.linkedin.com/company/19168781/'
    },
  ];

  const socialMediaLinks = (variant) => {
    return socialMediaArr.map((scMedia, index) => {
      return (
        <div className="text-center mx-2 my-1" key={index}>
          <a
            href={scMedia.linkPath}
            target="_blank"
            className={`
              ${variant !== "primary" ? "w-8 h-8" : "w-12 h-12"}
              rounded-full block block items-center justify-center flex hover:bg-palette-teal hover:text-white
            `}
            style={{background: variant !== "primary" ? "#151E48" : scMedia.color}}
          >
            {scMedia.icon}
          </a>
        </div>
      )
    })
  }

  return (
    <div className={`flex flex-row flex-wrap items-center ${className}`}>
      {socialMediaLinks(variant)}
    </div>
  )
}

export default SocialMedia
