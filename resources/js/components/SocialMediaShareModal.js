import React, { useState } from "react";

import { Modal } from "~/components/_base";
import {
  FacebookShareButton,
  LinkedinShareButton,
  TwitterShareButton,
  EmailShareButton,
  FacebookMessengerShareButton
} from "react-share";
import UserGlobal from "~/states/userGlobal";

import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { Button } from "~/components/_base";

import { sweetAlert } from "../components/_base/alerts/sweetAlert";

const SocialMediaShareModal = () => {
  const fbApplicationId = `${process.env.MIX_FACEBOOK_APP_ID}`;

  const [userState, userAction] = UserGlobal();

  const handleClose = () => {
    userAction.setState({ showSocialMedia: false });
  };

  return (
    <Modal
      show={userState.showSocialMedia}
      title={`Share this deal`}
      maxWidth={`sm`}
      onClose={() => handleClose()}
    >
      <div className={`flex flex-col items-center justify-center`}>
        <FacebookShareButton url={window.location.href} className={`mt-6`}>
          <div
            className={`w-64 font-bold items-center flex px-8 py-2 rounded-full text-base text-white mb-8`}
            style={{ background: "rgb(59, 89, 153)" }}
          >
            <FontAwesomeIcon icon={["fab", "facebook-f"]} size={`lg`} />
            <span className={`ml-3`}>Share on Facebook</span>
          </div>
        </FacebookShareButton>

        <LinkedinShareButton url={window.location.href} className={`mt-2`}>
          <div
            className={`w-64 font-bold items-center flex px-8 py-2 rounded-full text-base text-white mb-8`}
            style={{ background: "rgb(0, 119, 181)" }}
          >
            <FontAwesomeIcon icon={["fab", "linkedin"]} size={`lg`} />
            <span className={`ml-3`}>Share on Linkedin</span>
          </div>
        </LinkedinShareButton>

        <TwitterShareButton url={window.location.href} className={`mt-2`}>
          <div
            style={{ backgroundColor: "#0e76a8" }}
            className={`bg-blue-400 w-64 font-bold items-center flex px-8 py-2 rounded-full text-base text-white mb-8`}
            style={{ background: "#4267b2" }}
          >
            <FontAwesomeIcon icon={["fab", "twitter"]} size={`lg`} />
            <span className={`ml-3`}>Share on Twitter</span>
          </div>
        </TwitterShareButton>

        {/* <EmailShare
          email="mickey@mouse.com"
          subject="GroupBuyer"
          body="Your message, including the link to this page"
        >
          {link => (
            <a href={link} data-rel="external">
              Share on Email
            </a>
          )}
        </EmailShare> */}

        <EmailShareButton subject={`GroupBuyer`} url={window.location.href} className={`mt-2`} target="_blank">
          <div
            style={{ backgroundColor: "#0e76a8" }}
            className={`w-64 font-bold items-center flex px-8 py-2 rounded-full text-base text-white mb-8`}
            style={{ background: "#1565C0" }}
          >
            <FontAwesomeIcon icon={["fas", "envelope"]} size={`lg`} />
            <span className={`ml-3`}>Share on Email</span>
          </div>
        </EmailShareButton>

        {ShareViaSMS()}

        <FacebookMessengerShareButton
          url={window.location.href}
          redirectUri={window.location.href}
          appId="186207692714514"
          to=""
          className={`mt-2`}
        >
          <div
            style={{ backgroundColor: "#0e76a8" }}
            className={`w-64 font-bold items-center flex px-8 py-2 rounded-full text-base text-white mb-8`}
            style={{ background: "#1565C0" }}
          >
            <FontAwesomeIcon icon={["fab", "facebook-messenger"]} size={`lg`} />
            <span className={`ml-3`}>Share on Messenger</span>
          </div>
        </FacebookMessengerShareButton>

        {CopyLink()}
      </div>
    </Modal>
  );
};

const ShareViaSMS = () => {
  const [targetNumber, setTargetNumber] = useState("");

  return (
    <div
      style={{ backgroundColor: "#0e76a8" }}
      className={`cursor-pointer bg-blue-900 w-64 font-bold items-center flex px-8 py-2 rounded-full text-base text-white mb-8`}
      style={{ background: "#1565C0" }}
    >
      <FontAwesomeIcon icon={["fas", "comment-alt"]} size={`lg`} />
      <a
        href={`sms:${targetNumber}?&body=${window.location.href}`}
        className={`smsLink ml-3`}
      >
        Share link via SMS
      </a>
    </div>
  );
};
 

const CopyLink = () => {
  const copyToClipBoard = async () => {
    try {
      await navigator.clipboard.writeText(window.location.href);
      sweetAlert("success", "Copied!");
    } catch (err) {
      sweetAlert("error", "Failed to copy!");
    }
  };

  return (
    <div
      style={{ backgroundColor: "#0e76a8" }}
      className={`cursor-pointer bg-blue-800 w-64 font-bold items-center flex px-8 py-2 rounded-full text-base text-white mb-8`}
      style={{ background: "#1565C0" }}
      onClick={() => copyToClipBoard()}
    >
      <FontAwesomeIcon icon={["fas", "copy"]} size={`lg`} />
      <span className={`ml-3`}>Copy Link</span>
    </div>
  );
};

export default SocialMediaShareModal;
