import React, { useState, useEffect } from "react";
import EditPropertyModal from "./EditPropertyModal";
import AddPropertyModal from "./AddPropertyModal";
import EditLandPropertyModal from "./EditLandPropertyModal";
import AddLandProperty from "./AddLandProperty";
import UserGlobal from "../states/userGlobal";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faPlus } from "@fortawesome/free-solid-svg-icons";
import BaseAlert from "./_base/alerts/BaseAlert";
import { Button } from "~/components/_base";
import isEmpty from "lodash/isEmpty";

import { shortIntro } from "../helpers/stateAbbreviation";

const DeveloperPropertyManager = ({
  toggleFetch,
  toggleCloseModal,
  toggleAgreementView
}) => {
  const [userState, userActions] = UserGlobal();
  const [properties, setProperties] = useState([]);
  const [property, setProperty] = useState([]);
  const [toggleEditProperty, setToggleEditProperty] = useState(false);
  const [toggleAddProperty, setToggleAddProperty] = useState(false);

  const [toogleLandProperty, setToogleLandProperty] = useState(false);
  const [toogleEditLandProperty, setToogleEditLandProperty] = useState(false);
  const [count, setCount] = useState(0);
  const isProjectLand = userState.projectData.deal_type_id == 4 ? true : false;
 
  useEffect(() => {
    storeProperties();
    userActions.setState({ propertiesData: userState.projectData.properties });
  }, [userState.projectData.properties]);

  const storeProperties = () => {
    if (userState.projectData.properties.length == 5) {
      setProperties(userState.projectData.properties);
      setCount();
      return;
    }

    let arr1 = [];

    for (
      let index = 0;
      index < userState.projectData.properties.length;
      index++
    ) {
      const element = userState.projectData.properties[index];
      arr1.push(element);
    }

    let result = [];

    if (arr1.length < 5) {
      let arr2 = [];
      for (let index = 0; index < 5 - arr1.length; index++) {
        arr2.push({});
      }
      result = [...arr1, ...arr2];
    }
    setProperties(result);
  };

  const nextStep = () => {
    userActions.setState({ currentProjStep: 4 });
    toggleAgreementView();
  };

  const toggleEditPropertyModal = () => {
    if (!isProjectLand) {
      setToggleEditProperty(!toggleEditProperty);
    } else {
      setToogleEditLandProperty(!toogleEditLandProperty);
    }
  };

  const toggleAddPropertyModal = () => {
    if (!isProjectLand) {
      setToggleAddProperty(!toggleAddProperty);
    } else {
      setToogleLandProperty(!toogleLandProperty);
    }
  };

  // const toggleAddPropertyModal = () => {
  //   setToggleAP(!toggleAP);
  // };

  return (
    <div className={`relative bg-white py-5 my-5 lg:m-5 rounded-lg`}>
      {toggleEditProperty && (
        <EditPropertyModal
          project={userState.projectData}
          projectId={userState.projectData.id}
          property={property}
          toggleEditPropertyModal={() => toggleEditPropertyModal()}
          editToggleFetch={toggleFetch}
        />
      )}
      {toggleAddProperty && userState.projectData.properties.length < 5 && (
        <AddPropertyModal
          project={userState.projectData}
          projectId={userState.projectData.id}
          toggleAddPropertyModal={() => toggleAddPropertyModal()}
          addToggleFetch={toggleFetch}
        />
      )}

      {toogleEditLandProperty && (
        <EditLandPropertyModal
          projectName={userState.projectData.name}
          projectId={userState.projectData.id}
          property={property}
          toggleEditPropertyModal={() => toggleEditPropertyModal()}
          editToggleFetch={toggleFetch}
        ></EditLandPropertyModal>
      )}
      {toogleLandProperty && userState.projectData.properties.length < 5 && (
        <AddLandProperty
          projectName={userState.projectData.name}
          projectId={userState.projectData.id}
          toggleAddPropertyModal={() => toggleAddPropertyModal()}
          addToggleFetch={toggleFetch}
        ></AddLandProperty>
      )}
      <div className="flex flex-wrap justify-around">
        {properties.map((property, key) => {
          if (!isEmpty(property)) {
            return (
              <PropertyDetails
                key={key}
                pId={key}
                property={property}
                projectName={userState.projectData.deal_type}
                setProperty={setProperty}
                toggleEditPropertyModal={toggleEditPropertyModal}
                toggleAddPropertyModal={toggleAddPropertyModal}
                isLand={
                  userState.projectData.sub_property_id == 4 ? true : false
                }
              />
            );
          } else {
            return (
              <EmptyProperties
                pId={key}
                key={key}
                toggleAddPropertyModal={toggleAddPropertyModal}
              />
            );
          }
        })}
      </div>

      {userState.projectData.properties.length == 5 && (
        <div className={`flex flex-1 px-3`}>
          <BaseAlert
            message={
              "You have reached the maximum 5 properties per project. Once all 5 properties are sold you can upload another 5 properties."
            }
            icon={`check`}
            type={`success`}
            iconHeight={`h-5`}
            iconWidth={`w-6`}
            className={`w-full`}
          />
        </div>
      )}
      <div className={`flex flex-1 items-center justify-end px-3 mt-2`}>
        <Button
          className={`button button-primary font-bold h-10 ml-10 mt-1 rounded rounded-full bg-blue-500`}
          onClick={() => nextStep()}
          disabled={userState.projectData.properties.length < 5}
        >
          Next Step
        </Button>
        <Button
          className={`button button-primary font-bold h-10 ml-5 mt-1 rounded rounded-full`}
          onClick={() => toggleCloseModal()}
        >
          Save & Close
        </Button>
      </div>
    </div>
  );
};

const EmptyProperties = ({ pId, toggleAddPropertyModal }) => {
  const addProperty = e => {
    e.preventDefault();
    toggleAddPropertyModal();
  };

  return (
    <div key={pId} className={`mb-4 px-3  w-full`}>
      <div
        className={`block bg-white rounded-md p-0`}
        style={{ boxShadow: "0 5px 28px 4px rgba(0,0,0,0.1)" }}
      >
        <div
          onClick={e => addProperty(e)}
          className={`cursor-pointer border border-gray-400 flex h-48 items-center justify-center relative rounded`}
        >
          <span className={`text-2xl text-gray-600`}>
            <FontAwesomeIcon icon={faPlus} className={`fa-plus mr-2`} />
            Add Property
          </span>
        </div>
      </div>
    </div>
  );
};

const parseAddress = strAddress => {
  if (typeof strAddress === "string") {
    return JSON.parse(strAddress);
  } else {
    return strAddress;
  }
};

const PropertyDetails = ({
  pId,
  property,
  projectName,
  setProperty,
  toggleEditPropertyModal,
  isLand
}) => {
  return (
    <div key={pId} className={`mb-4 px-3  w-full`}>
      <div
        className={`block bg-white rounded-md p-0`}
        style={{ boxShadow: "0 5px 28px 4px rgba(0,0,0,0.1)" }}
      >
        <div className={`flex items-center relative`}>
          <img
            src={property.featured_images[0]}
            className={`w-1/3 h-48 p-2 pr-4`}
          />
          <div className={`flex flex-1 flex-col relative`}>
            <div
              className={`lg:text-2xl text-base mb-2 font-bold w-56 leading-none`}
            >
              {projectName} {property.unit_name}
            </div>
            <div className={`lg:mb-2 mr-4 flex items-center text-base`}>
              <svg className={`feather-icon text-palette-purple mr-1`}>
                <use xlinkHref={`/assets/svg/feather-sprite.svg#map-pin`} />
              </svg>
              {/* {property.address.suburb + ", " + property.address.state} */}
              {parseAddress(property.address).suburb +
                ", " +
                parseAddress(property.address).state}
            </div>
            <div className={`mr-4 flex items-center`}>
              <div
                className={`text-base  `}
                dangerouslySetInnerHTML={{
                  __html: shortIntro(property.description)
                }}
              />
            </div>
            <div className={`flex flex-1 justify-start`}>
              {!isLand && (
                <>
                  <span className={`mr-3 text-base`}>
                    {property.no_of_bedrooms}{" "}
                    <FontAwesomeIcon icon={["fas", "bed"]} />
                  </span>
                  <span className={`mr-3 text-base`}>
                    {property.no_of_bathrooms}{" "}
                    <FontAwesomeIcon icon={["fas", "bath"]} />
                  </span>
                  <span className={`text-base`}>
                    {property.no_of_garages}{" "}
                    <FontAwesomeIcon icon={["fas", "car"]} />
                  </span>
                </>
              )}

              {isLand && (
                <>
                  <span className={`mr-3 text-base`}>
                    Size: <span className={'font-bold'}>{property.size}</span>
                  </span>
                  <span className={`mr-3 text-base`}>
                    Frontage: <span className={'font-bold'}>{property.frontage}</span>
                  </span>
                  <span className={`mr-3 text-base`}>
                    Width: <span className={'font-bold'}>{property.width}</span>
                  </span>
                  <span className={`mr-3 text-base`}>
                    Depth: <span className={'font-bold'}> {property.depth}</span>
                  </span>
                </>
              )}
              <div className={`flex flex-1 justify-end px-3`}>
                <button
                  className={`text-base mr-4`}
                  onClick={() => {
                    setProperty(property);
                    toggleEditPropertyModal();
                  }}
                >
                  Edit&nbsp;
                  <FontAwesomeIcon icon={["fas", "edit"]} />
                </button>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default DeveloperPropertyManager;
