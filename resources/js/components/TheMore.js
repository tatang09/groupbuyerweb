import React from 'react';
const chevron_right = "/assets/images/chevron-right.svg";

const TheMore = ({ bgColor, txtColor="text-white" }) => {
    return <section>
        <div className="flex items-center justify-items-center justify-around xs:py-8 lg:py-12 w-100 xs:px-4 xs:text-base lg:px-32 the-more-section" style={{ backgroundColor: bgColor }}>
            <div className={`${txtColor} lg:text-5xl font-bold text-center lg:leading-10`}>The More <br /> We Are</div>
            <img src={chevron_right} className={"xs:w-4 lg:w-16"} />
            <div className={`${txtColor} lg:text-5xl font-bold text-center lg:leading-10`}>The More <br /> We Negotiate</div>
            <img src={chevron_right} className={"xs:w-4 lg:w-16"} />
            <div className={"text-palette-purple-main lg:text-5xl font-bold text-center lg:leading-10"}>The More <br /> You Save</div>
        </div>
    </section>
}

export default TheMore;
