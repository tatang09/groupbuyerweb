import React, { useState, useRef } from "react";

import {
    Avatar,
    Popper,
    Grow,
    MenuList,
    MenuItem,
    Paper
} from "@material-ui/core";
import UserGlobal from "~/states/userGlobal";
import { isLoggedIn, logout, userRole } from "~/services/auth";
import { links } from '../../helpers/headerHelper';
import swal from "sweetalert2";

const WebViewHeader = () => {

    const [popper, setPopper] = useState(null);
    const [userState, userAction] = UserGlobal();
    const anchorRef = useRef(null);

    const handleLogout = async () => {
        swal
            .fire({
                title: "Confirmation",
                text: "Are you sure you want to sign out?",
                showConfirmButton: true,
                showCloseButton: true,
                showCancelButton: true,
                focusConfirm: false,
                confirmButtonColor: "#BD00F2",
                confirmButtonText: "Confirm"
            })
            .then(async result => {
                if (result.value) {
                    try {
                        await axios.post(`/api/logout`, null, {
                            headers: {
                                Authorization: "Bearer " + isLoggedIn()
                            }
                        });

                        logout();
                        userAction.setState({ fetch: !userState.fetch });
                    } catch (e) {

                    }
                }
            });
    };

    const webNavLinks = () => {
        return links.map((link, key) => {
            if (link.name === "Contact Us" || link.name === "FAQ") return;
            return (
                <a
                    key={key}
                    href={link.route}
                    className={`
                mx-4 tracking-wide px-4
                transition-all duration-300 w-full text-center
                ${link.show ? "" : "hidden"}
                whitespace-no-wrap
                ${link.name === "Developers" ? "rounded border border-palette-purple-main " : ""}
              `}
                >
                    <span>{link.name}</span>
                </a>
            );
        });
    }

    return (
        <div>
            <div className={`flex flex-1 items-center justify-start text-white custom-navbar text-base`}>

                {webNavLinks()}
                {!isLoggedIn() ? (
                <a className={`
                        mx-4 tracking-wide
                        transition-all duration-300 w-full text-center
                        whitespace-no-wrap px-6
                        rounded border border-palette-purple-main
                        bg-palette-purple-main
                        cursor-pointer
                        `}
                    >
                        <span onClick={() => userAction.setState({ showSignIn: true })}>Log in</span>
                    </a>
                ):(<></>)}
                {/* {!isLoggedIn() ? (

                    <a className={`
                        mx-4 tracking-wide
                        transition-all duration-300 w-full text-center
                        whitespace-no-wrap px-6
                        rounded border border-palette-purple-main
                        bg-palette-purple-main
                        cursor-pointer
                        `}
                    >
                        <span onClick={() => userAction.setState({ showSignIn: true })}>Log in</span>
                    </a>
                ) : (
                    <div className={`flex items-center`}>
                        <div
                            className={`border-l border-gray-500 mr-6 ml-3`}
                            style={{ width: 1, height: 20 }}
                        />

                        <span
                            onMouseOver={e => setPopper(e.currentTarget)}
                            onMouseLeave={() => setPopper(null)}
                        >
                            {userState.user && userState.user.avatar_path ? (
                                <span className={`flex items-center cursor-pointer`}>
                                    <span className={`tracking-wide lg:text-base font-bold`}>
                                        {userState.user && userState.user.first_name}
                                    </span>
                                    <Avatar
                                        ref={anchorRef}
                                        src={`${userState.user.avatar_path}`}
                                        className={`cursor-pointer ml-2`}
                                    />
                                </span>
                            ) : (
                                <span className={`flex items-center cursor-pointer`}>
                                    <span className={`tracking-wide lg:text-base font-bold`}>
                                        {userState.user && userState.user.first_name}
                                    </span>
                                    <svg
                                        className={`feather-icon h-6 w-6 ml-2`}
                                        ref={anchorRef}
                                    >
                                        <use xlinkHref={`/assets/svg/feather-sprite.svg#user`} />
                                    </svg>
                                </span>
                            )}
                            <Popper
                                open={Boolean(popper)}
                                anchorEl={popper}
                                placement={`bottom-end`}
                                transition
                                disablePortal
                                className={`-mr-4 mt-3 menu-list`}
                            >
                                {({ TransitionProps }) => (
                                    <Grow
                                        {...TransitionProps}
                                        style={{ transformOrigin: "right top" }}
                                    >
                                        <Paper
                                            elevation={4}
                                            style={{ width: 360 }}
                                            className={`relative`}
                                        >
                                            <div
                                                className={`arrow absolute`}
                                                style={{
                                                    top: "-6px",
                                                    right: "24px",
                                                    borderWidth: "0 0.8em 0.8em 0.8em",
                                                    borderColor:
                                                        "transparent transparent white transparent"
                                                }}
                                            />
                                            <MenuList>
                                                <div
                                                    className={`flex items-center px-4 pt-3 pb-4 mb-2 border-b`}
                                                >

                                                    <div className={`flex flex-col`}>
                                                        {userState.user && (
                                                            <>
                                                                <span
                                                                    className={`block lg:text-base font-bold text-xl leading-none`}
                                                                >
                                                                    {userState.user.first_name +
                                                                        " " +
                                                                        userState.user.last_name}
                                                                </span>
                                                                <span
                                                                    className={`lg:text-base block
                                                            `}
                                                                >
                                                                    {userState.user.email}
                                                                </span>
                                                            </>
                                                        )}
                                                    </div>
                                                </div>

                                                {(userRole() === "admin" ||
                                                    userRole() === "super_admin") && (
                                                        <MenuItem
                                                            onClick={() =>
                                                                (window.location = `/admin/projects`)
                                                            }
                                                        >
                                                            Admin
                                                        </MenuItem>
                                                    )}

                                                <MenuItem
                                                    onClick={() =>
                                                        (window.location = `/profile/account-settings`)
                                                    }
                                                >
                                                    My Profile
                                            </MenuItem>
                                                {(userRole() === "admin" ||
                                                    userRole() === "super_admin" ||
                                                    userRole() === "project_developer") && (
                                                        <MenuItem
                                                            onClick={() =>
                                                                (window.location = `/profile/manage-projects`)
                                                            }
                                                        >
                                                            Manage projects
                                                        </MenuItem>
                                                    )}
                                                <MenuItem
                                                    onClick={() =>
                                                        (window.location = `/profile/secured-deals`)
                                                    }
                                                >
                                                    Secured deals
                                            </MenuItem>
                                                <MenuItem
                                                    onClick={() =>
                                                        (window.location = `/profile/wish-list`)
                                                    }
                                                >
                                                    Wish list
                                            </MenuItem>
                                                <MenuItem onClick={() => handleLogout()}>
                                                    Sign out
                                            </MenuItem>
                                            </MenuList>
                                        </Paper>
                                    </Grow>
                                )}
                            </Popper>
                        </span>
                    </div>
                )} */}
            </div>
        </div>
    )
}


export default WebViewHeader;
