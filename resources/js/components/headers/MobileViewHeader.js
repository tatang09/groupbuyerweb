import React, { useState } from "react";
import { A, usePath } from "hookrouter";
import UserGlobal from "~/states/userGlobal";
import { links } from '../../helpers/headerHelper';
import { Modal } from "~/components/_base";
import { isLoggedIn, logout, userRole } from "~/services/auth";
import { isMobile } from "react-device-detect";

const MobileViewHeader = () => {

    const [popper, setPopper] = useState(null);
    const [userState, userAction] = UserGlobal();
    const [showMenu, setShowMenu] = useState(false);
    const path = usePath();

    const handleLogout = async () => {
        try {
            await axios.post(`/api/logout`, null, {
                headers: {
                    Authorization: "Bearer " + isLoggedIn()
                }
            });

            logout();
            setUserRole("");
            userAction.setState({ fetch: !userState.fetch });
        } catch (e) { }
    };

    const mobileNavLinks = () => {
        return links.map((link, key) => {
            return (
                <a
                    key={key}
                    href={link.route}
                    className={`
                ${path !== "/" ? "opacity-50" : "opacity-100"}
                ${path.includes(link.route)
                            ? "text-palette-teal opacity-100"
                            : "text-gray-500"
                        } mb-8 lg:mb-0
                hover:text-palette-purple-main px-3 tracking-wide ${!isMobile ? "text-base" : "text-2xl lg:text-base"
                        } font-bold transition-all duration-300 w-full text-center
                ${link.show ? "" : "hidden"}
              `}
                >
                    <span>{link.name}</span>
                </a>
            );
        });
    };

    return (
        <>
            <svg
               
                onClick={() => setShowMenu(!showMenu)}
                className={`absolute cursor-pointer feather-icon h-8 mr-6 right-0 w-8`}
                style={{ strokeWidth: 3, color: "#E91AFC" }}
            >
                <use xlinkHref={`/assets/svg/feather-sprite.svg#menu`} />
            </svg>
            <Modal show={showMenu} onClose={() => setShowMenu(!showMenu)}>
                <div
                    className={`flex flex-col items-end justify-around mt-16`}
                >

                    {mobileNavLinks()}

                    {!isLoggedIn() ? (
                        <span
                            onClick={() => userAction.setState({ showSignIn: true })}
                            className={`w-full cursor-pointer px-3 tracking-wide text-2xl lg:text-base text-gray-500 text-center font-bold`}
                        >
                            Login
                        </span>
                    ) : (
                        <>
                            <hr className="block w-1/4 mb-6 border border-gray-300 opacity-50" />
                            {(userRole() === "admin" || userRole() === "super_admin") && (
                                <a
                                    href={`/admin/projects`}
                                    className={`mb-6 cursor-pointer px-3 tracking-wide text-2xl lg:text-base text-white font-bold`}
                                >
                                    Admin
                                </a>
                            )}

                            <a
                                href={`/profile/account-settings`}
                                className={`mb-6 cursor-pointer px-3 tracking-wide text-2xl lg:text-base text-white font-bold`}
                            >
                                My Profile
                            </a>
                            {(userRole() === "admin" ||
                                userRole() === "super_admin" ||
                                userRole() === "project_developer") && (
                                    <a
                                        href={`/profile/manage-projects`}
                                        className={`mb-6 cursor-pointer px-3 tracking-wide text-2xl lg:text-base text-white font-bold`}
                                    >
                                        Manage Project
                                    </a>
                                )}
                            <a
                                href={`/profile/secured-deals`}
                                className={`mb-6 cursor-pointer px-3 tracking-wide text-2xl lg:text-base text-white font-bold`}
                            >
                                Secured deals
                            </a>
                            <a
                                href={`/profile/wish-list`}
                                className={`mb-6 cursor-pointer px-3 tracking-wide text-2xl lg:text-base text-white font-bold`}
                            >
                                Wish list
                            </a>
                            <span
                                onClick={() => handleLogout()}
                                className={`mb-6 cursor-pointer px-3 tracking-wide text-2xl lg:text-base text-white font-bold`}
                            >
                                Sign out
                            </span>
                        </>
                    )}
                </div>
            </Modal>
        </>
    )
}


export default MobileViewHeader;