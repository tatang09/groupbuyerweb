import React, { useState, useRef } from "react";


import UserGlobal from "~/states/userGlobal";
import DevRegistrationForm from '../DevRegistrationForm';
import swal from "sweetalert2";
import Modal from '@material-ui/core/Modal';

const SiteConstructionHeader = () => {

    const [userState, userAction] = UserGlobal();
    const [modal, setModal] = useState(false);
    const handleOpenModal = () => {
        setModal(true);
    };

    const handleCloseModal = () => {
        setModal(false);
    };


    return (
        <div className={`cursor-pointer`}>
            <div className={`flex flex-1 items-center justify-start text-white md:mr-3 mr-0`} onClick={handleOpenModal}>
                <a
                    className={`
                    developer-agent-btn border border-palette-purple-main duration-300 px-2 py-1 rounded text-center tracking-wide transition-all w-full whitespace-no-wrap
              `}
                >
                    <span>Developers/Agents</span>
                </a>
            </div>
            <Modal
                aria-labelledby="simple-modal-title"
                aria-describedby="simple-modal-description"
                open={modal}

                // centered
                style={{ display: 'flex', alignItems: 'center', justifyContent: 'center' }}
                onClose={handleCloseModal}
            >
                <DevRegistrationForm
                    containerBgColor="#fff"
                    containerRounded={true}
                    displayCloseButton={true}
                    rightPadded={false}
                    subHeader="LET'S WORK TOGETHER"
                    textColorTheme="dark"
                    inputBgColor="#dadada5c"
                    headerTop="Request a"
                    headerBottom="demonstration"
                    handleCloseModal={handleCloseModal}
                    opacity={"100%"}
                    isPopup={true}
                />
            </Modal>
        </div>
    )
}


export default SiteConstructionHeader;
