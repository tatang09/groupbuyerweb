import React from "react"

function TextArea({
  onChange,
  className,
  prefix,
  suffix,
  errors,
  appearance = true,
  ...rest
}) {
  return (
    <div className={`${appearance ? "border-b-2 mt-4 px-2 py-3" : ""} flex items-start w-full`} style={{borderColor: "rgba(255,255,255,0.1)"}}>
      { prefix && (
        <svg className={`feather-icon mr-3 h-6 w-6 opacity-50 ${errors ? "text-red-500" : ""}`}>
          <use xlinkHref={`/assets/svg/feather-sprite.svg#${prefix}`}/>
        </svg>
      )}

      <textarea
        className={`resize-none appearance-none bg-transparent border-0 font-hairline leading-relaxed text-base w-full ${className}`}
        rows="3"
        onChange={onChange}
        {...rest}
      />

      {suffix}
    </div>
  )
}

export default TextArea
