import React, { useState, useEffect } from 'react';
import Pagination from './table/Pagination';
import PageSelectSize from './table/PageSelectSize';
import { axios } from '~/helpers/bootstrap';
import { isMobile } from 'react-device-detect';

const Table = ({
  className,
  header,
  content,
  query,
  queryParams = ``,
  keyword = ``,
  order = ``,
  sort = ``,
  getData,
  toggleFetch,
  emptyComponent
}) => {
  const [limit, setLimit] = useState(12);
  const [offset, setOffset] = useState(0);
  const [total, setTotal] = useState(0);
  const [fetching, setFetching] = useState(false);

  useEffect(() => {
    setFetching(true);

    async function fetchResults() {
      let pathname = window.location.pathname.replace(/\/$/, '');
      // let filters = filter ? (Cookie.get(pathname) ? JSON.parse(Cookie.get(pathname)) : []) : []
      // filters = filters.map(filter => {
      //   delete filter['id']
      //   delete filter['label']
      //   delete filter['options']
      //   delete filter['filterLabel']
      //   return filter
      // })

      let { data } = await axios.get(getFetchUrl());

      setTotal(data.total);
      getData(data.data);
      setFetching(false);
      window.scrollTo({
        top: 0,
        behavior: 'smooth',
      });
    }

    function getFetchUrl() {
      queryParams = queryParams ? `&${queryParams}` : '';
      return (
        query +
        '?keyword=' +
        keyword +
        '&limit=' +
        limit +
        '&offset=' +
        offset +
        '&order=' +
        order +
        '&sort=' +
        sort +
        queryParams
      );
    }

    fetchResults();
  }, [offset, limit, keyword, toggleFetch]); // ✅ Refetch on change

  const changeLimit = value => {
    setOffset(0);
    setLimit(value);
  };

  return (
    <>
      <section>
        {!total ? (
          <>
            {emptyComponent ? (
              emptyComponent
            ) : (
              <div className="flex flex-col items-center justify-center w-full h-64 text-base text-gray-400">
                No result found!
              </div>
            )}
          </>
        ) : (
          <>
            <table className={`table-auto text-left w-full ${className}`}>
              {header}
              {content}
            </table>

            <div className="flex flex-col h-16 items-center justify-center lg:h-32 text-base text-palette-purple-main w-full">
              <a href={`#`}>View More Projects</a>
            </div>
            {/* <div
              className={`flex-col lg:flex items-center justify-center lg:mt-16 mt-6 relative w-full`}>
              <Pagination
                onPageChange={setOffset}
                limit={limit}
                offset={offset}
                total={total}
              />

              <PageSelectSize total={total} onClick={changeLimit} />
            </div> */}
          </>
        )}
      </section>
    </>
  );
};

export default Table;
