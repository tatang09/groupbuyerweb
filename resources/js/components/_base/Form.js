import React, { useState, useRef } from "react";
import { Radio } from "pretty-checkbox-react";
import TextInput from "./TextInput";
import Select from "react-select";
import TextArea from "./TextArea";
import { inputRegex } from "~/helpers/numberInput";
import CurrencyFormat from "react-currency-format";
const Form = ({ errors, formFields, data = {} }) => {
  const Field = key => {
    let type = formFields[key].type;
    let inputFile = useRef(null);
    let [filename, setFilename] = useState("");

    const placeholder =
      typeof formFields[key].placeholder !== "undefined"
        ? formFields[key].placeholder
        : `Enter ${formFields[key].label || key.replace("_", " ")}`;

    switch (type) {
      case "select":
        const handleDefaultValue = () => {
          let value = formFields[key].value;
          let selected = null;

          Object.keys(formFields[key].options).forEach(optionKey => {
            if (formFields[key].options[optionKey].value === value) {
              selected = formFields[key].options[optionKey];
            }
          });
          return selected;
        };

        return (
          <Select
            isOptionSelected
            name={key}
            classNamePrefix={`input-select`}
            onChange={formFields[key].handleChange}
            className={`w-full`}
            placeholder={placeholder}
            // value={formFields[key].value}
            options={formFields[key].options}
            defaultValue={formFields[key].options.find(o => o.value === formFields[key].value)}
          />
        );
      // case "date":
      //   return (
      //     <Flatpickr
      //       className={`text-sm border border-gray-400 focus:border-primary shadow-sm appearance-none rounded-sm px-3 py-2 w-full`}
      //       name={key}
      //       options={{
      //         altInput: true,
      //         altFormat: "F j, Y",
      //         dateFormat: "Y-m-d",
      //         static: true,
      //         defaultDate: formFields[key].value || data[key] || ``,
      //       }}
      //       placeholder={placeholder}
      //     />
      //   )
      case "file":
        const handleFile = () => {
          let name = "";

          if (inputFile.current.files.length) {
            let files = Array.from(inputFile.current.files);
            name = files[0].name;
          }

          setFilename(name);
        };

        const clearFile = () => {
          inputFile.current.type = "";
          inputFile.current.type = "file";
          setFilename("");
        };

        return (
          <div
            className={`c-file-input js-file-input bg-gray-100 hover:bg-gray-200 ${filename &&
              "has-file"}`}
          >
            <div className="c-file-input__indicator">
              <span className="c-file-input__indicator__icon c-icon c-icon--attach">
                <i className="fa fa-paperclip" />
              </span>
            </div>
            <label
              className={`cursor-pointer c-file-input__label js-file-input__label ${filename &&
                "text-primary"}`}
            >
              <span>{filename || "No file choosen"}</span>
              <input
                ref={inputFile}
                type="file"
                name={formFields[key].name || key}
                accept={formFields[key].accept}
                onChange={handleFile}
                className="c-file-input__field js-file-input__field"
              />
            </label>
            <div
              className="c-file-input__remove js-file-input__remove"
              onClick={clearFile}
            >
              <span className="c-file-input__remove__icon c-icon c-icon--remove-circle">
                <i className="fa fa-minus-circle" />
              </span>
            </div>
          </div>
        );

      case "radio":
        let radioWrapperClass = formFields[key].classname || "flex flex-col";

        return (
          <>
            <section className={radioWrapperClass}>
              {Object.keys(formFields[key].options).map(optionKey => (
                <Radio
                  key={optionKey}
                  name={key}
                  value={optionKey}
                  inputProps={{
                    defaultChecked: formFields[key].value === optionKey
                  }}
                  {...formFields[key].methods}
                  color="success-o"
                >
                  {formFields[key].options[optionKey]}
                </Radio>
              ))}
            </section>
            {key === "isFIRB_required" && (
              <span className={`ml-6 mt-2`}>
                (Foreign Investment Review Board)
              </span>
            )}
          </>
        );

      case "textarea":
        return (
          <TextArea
            className={`
                ${formFields[key].class}
                ${formFields[key].width || "w-full"}
                border rounded p-3 mt-0
              `}
            placeholder={placeholder}
            name={key}
            defaultValue={formFields[key].value || data[key] || ``}
            appearance={true}
            name={key}
            autoComplete={`off`}
          />
        );

      default:

        return (
          <>
            {formFields[key].prefix && (
              <div
                className={`px-4 bg-gray-200 flex items-center justify-center rounded-l border-l border-t border-b border-gray-400`}
              >
                +61
              </div>
            )}

            <TextInput
              type={formFields[key].type || `text`}
              step={`any`}
              className={`
              ${formFields[key].prefix ? "border-l-0 rounded-l-none px-2" : ""}
                ${formFields[key].class}
                ${formFields[key].width || "w-full"}
              `}
              name={key}
              autoComplete={`off`}
              // onChange={(e) => onChange(e)}
              defaultValue={formFields[key].value || data[key] || ``}
              placeholder={placeholder}
              minLength={formFields[key].minlength || ``}
              maxLength={formFields[key].maxlength || ``}
              border={false}
              appearance={true}
              onKeyDown={evt =>
                (evt.key === "e" || evt.key === "." || evt.key === "-") &&
                formFields[key].type === "number" &&
                evt.preventDefault()
              }
            />
          </>
        );
    }
  };

  return Object.keys(formFields).map((value, key) => (
    <div
      className={`mb-5 text-base flex`}
      key={formFields[value].id ? `${formFields[value].id}${key}` : key}
    >
      <label
        className={`mr-3 ${
          formFields[value].type !== "radio" ? "w-40" : "w-1/2"
        } mt-2 capitalize`}
      >
        <span className="font-bold">
          {formFields[value].label || value.replace("_", " ")}
        </span>
        {formFields[value].required && key !== "alternate_phone" && (
          <span className="text-red-500"> *</span>
        )}
      </label>

      <div className={`w-full`}>
        <div className={`flex`}>{Field(value)}</div>
        {errors[value] && (
          <span className="text-red-500 text-sm">{errors[value]}</span>
        )}
      </div>
    </div>
  ));
};

export default Form;
