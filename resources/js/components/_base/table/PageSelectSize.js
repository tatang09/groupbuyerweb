import React, { useEffect } from "react";
import * as deviceSizes from "../../../helpers/deviceSizeHelper";
import UserGlobal from "~/states/userGlobal";

const PageSelectSize = ({ total, onClick }) => {
  const pageSizeOptions = [12, 24, 48];
  const [userState, userAction] = UserGlobal();

  useEffect(() => {

  }, [userState.windowSize]);

  if (userState.windowSize <= deviceSizes.baseSmall) {
    return (
      <label
        name={`PageSizeSelect`}
        className={`flex-col lg:absolute lg:right-0 flex items-center justify-center mt-6 lg:mt-0`}
      >
        <span className="font-bold mb-1 text-gray-600">
          Items per page:
        </span>
        <ul className={`flex items-center`}>
          {/* <select className="form-control input-sm -page-size-select" onChange={onChange}>
    {pageSizeOptions.map((value, key) =>
      <option key={key} value={value}>{value}</option>
    )}
  </select> */}

          {pageSizeOptions.map((value, key) => (
            <li
              key={key}
              className={`page-size-select
        ${key !== pageSizeOptions.length - 1 ? "border-r" : ""}
      `}
              onClick={() => onClick(value)}
            >
              {value}
            </li>
          ))}
        </ul>
      </label>
    );
  } else {
    return (
      <label
        name={`PageSizeSelect`}
        className={`lg:absolute lg:right-0 flex items-center justify-center mt-6 lg:mt-0`}
      >
        <span className="font-bold mb-1 mr-2 text-gray-600">
          Items per page:
        </span>
        <ul className={`flex mr-2`}>
          {/* <select className="form-control input-sm -page-size-select" onChange={onChange}>
          {pageSizeOptions.map((value, key) =>
            <option key={key} value={value}>{value}</option>
          )}
        </select> */}

          {pageSizeOptions.map((value, key) => (
            <li
              key={key}
              className={`page-size-select
              ${key !== pageSizeOptions.length - 1 ? "border-r" : ""}
            `}
              onClick={() => onClick(value)}
            >
              {value}
            </li>
          ))}
        </ul>
      </label>
    );
  }
};

export default PageSelectSize;
