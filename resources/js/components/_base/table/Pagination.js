import React from "react"

const Pagination = ({ offset, limit, total, onPageChange }) => {

  const isFirstPage = () => {
    return offset === 0 || limit >= total
  }

  const isLastPage = () => {
    return offset + limit >= total
  }

  const totalPage = () => {
    return Math.ceil(total / limit)
  }

  const curPage = () => {
    return Math.ceil(offset / limit) + 1
  }

  const dspBtns = () => {
    const n = totalPage()
    const i = curPage()
    /* eslint-disable */
    if (n <= 9) return ((n) => {
      const arr = Array(n)
      while (n) { arr[n - 1] = n-- }
      return arr
    })(n)
    if (i <= 5) return [1, 2, 3, 4, 5, 6, 7, 0, n] // 0 represents `···`
    if (i >= n - 4) return [1, 0, n-6, n-5, n-4, n-3, n-2, n-1, n]
    return [1, 0, i-2, i-1, i, i+1, i+2, 0, n]
    /* eslint-enable */
  }

  const handleClick = (n) => {
    let offset = (n - 1) * limit
    onPageChange(offset)
  }

  const turnPage = (i) => {
    onPageChange(offset + (i * limit))
  }

  return (
    <div className={`flex items-center justify-center mb-3 lg:mb-0`}>
      <ul className="pagination" name="Pagination">
        { ! isFirstPage() && (
          <li className="page-item" onClick={() => turnPage(-1)}>
            <button className="page-link">
              <svg className={`feather-icon w-full`}>
                <use xlinkHref={`/assets/svg/feather-sprite.svg#chevron-left`}/>
              </svg>
            </button>
        </li>
        )}

        {dspBtns().map((button, i) =>
          <li key={i} className={`page-item ${button === curPage() ? 'active' : ''}`}>
            { button ? (
              <button className="page-link" onClick={() => handleClick(button)}>
                { button }
              </button>
            ) : (
              <button className="page-link">
                <i className="fa fa-ellipsis-h"></i>
              </button>
            )}
          </li>
        )}

        { ! isLastPage() && (
          <li className="page-item" onClick={() => turnPage(1)}>
            <button className="page-link">
              <svg className={`feather-icon w-full`}>
                <use xlinkHref={`/assets/svg/feather-sprite.svg#chevron-right`}/>
              </svg>
            </button>
          </li>
        )}
      </ul>
    </div>
  )
}

export default Pagination
