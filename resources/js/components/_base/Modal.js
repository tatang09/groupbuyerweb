import React, { forwardRef } from "react";
import {
  Dialog,
  DialogContent,
  DialogTitle,
  IconButton,
  Fade,
  Grow,
  Slide,
  Zoom
} from "@material-ui/core";
import CloseIcon from "@material-ui/icons/Close";
import { isMobile } from "react-device-detect";

const Modal = ({
  disableBackdropClick = false,
  topOff = false,
  allowOutsideInput = false,
  maxWidth = `sm`,
  title = ``,
  show = false,
  onClose,
  children,
  transition,
  scroll=`body`
}) => {
  const Transition = forwardRef(function Transition(props, ref) {
    switch (transition) {
      case "slide":
        return <Slide direction="down" ref={ref} {...props} />;
      case "grow":
        return <Grow ref={ref} {...props} />;
      case "zoom":
        return <Zoom ref={ref} {...props} />;
      default:
        return <Fade ref={ref} {...props} />;
    }
  });

  if (!show) return null;

  return (
    <Dialog
      // TransitionComponent={Transition}
      disableBackdropClick={disableBackdropClick}
      transitionDuration={400}
      keepMounted
      disableEnforceFocus={allowOutsideInput}
      fullScreen={isMobile ? true : false}
      open={show}
      onClose={() => onClose()}
      fullWidth={isMobile ? false : true}
      maxWidth={isMobile ? false : maxWidth}
      scroll={scroll}
      PaperProps={{ style: styles.paperProps }}
    >
      <DialogTitle>
        <div
          className={`text-white font-bold text-4xl lg:text-5xl ${
            topOff ? "mt-1" : "mt-10"
          }  lg:px-2 leading-none`}
        >
          {title}
        </div>

        <IconButton
          className={`p-3 text-palette-purple-main`}
          onClick={() => onClose()}
          style={{ right: 12, top: 12, position: "absolute" }}
        >
          <CloseIcon />
        </IconButton>
      </DialogTitle>

      <DialogContent>{children}</DialogContent>
    </Dialog>
  );
};

const styles = {
  paperProps: {
    backgroundColor: "#4d0b98"
  }
};

export default Modal;
