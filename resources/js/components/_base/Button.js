import React from "react"

const Button = ({ onClick, className, children, type = `primary`, ...rest }) => {

  return (
    <button
      className={`button button-${type} ${className}`}
      onClick={onClick}
      {...rest}
    >
      {children}
    </button>
  )
}

export default Button
