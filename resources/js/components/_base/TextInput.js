import React from "react";
import {inputRegex} from "../../helpers/numberInput";


function TextInput({
  onChange,
  className,
  prefix,
  suffix,
  errors,
  border = true,
  appearance = false,
  width,
  ...rest
}) {
  
  return (
    <div
      className={`${
        border ? "border-b-2 mt-4 px-2 py-3" : ""
      } flex items-start ${width ? width : "w-full"}`}
      style={{ borderColor: "rgba(255,255,255,0.1)" }}
    >
      {prefix && (
        <svg
          className={`feather-icon mr-3 h-6 w-6 opacity-50 ${
            errors ? "text-red-500" : ""
          }`}
        >
          <use xlinkHref={`/assets/svg/feather-sprite.svg#${prefix}`} />
        </svg>
      )}

      {/* {prefix === "phone" && (
        <span className={`text-base mr-1 text-gray-500`}>Phone</span>
      )} */}



      <input
        className={`
          font-hairline leading-relaxed w-full  
          ${
            appearance
              ? "border border-gray-400 rounded px-3 py-1"
              : "appearance-none bg-transparent border-0"
          }
          ${className}
        `}
        onChange={onChange}
        {...rest}
      />

      {suffix}
    </div>
  );
}

export default TextInput;
