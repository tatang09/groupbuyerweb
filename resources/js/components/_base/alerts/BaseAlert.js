import React, { useState, useEffect } from "react";

const BaseAlert = ({ message, icon, type, className, iconHeight = `h-5`, iconWidth = `w-5` }) => {
  const [bgColor, setBackgrounColor] = useState("");
  const [msgBGColor, setMsgBGColor] = useState("");
  const [msgType, setMsgType] = useState(message);

  useEffect(() => {
    assignValues();
  }, []);

  const assignValues = () => {
    switch (type) {
      case "success":
        setBackgrounColor("rgba(137, 176, 54, 0.3)");
        setMsgBGColor("rgb(137, 176, 54)");
        break;
      case "info":
        break;
      case "error":
        setBackgrounColor("rgba(216, 67, 21, 1)");
        setMsgBGColor("rgba(255, 255, 255, 1)");
        break;
      default:
        break;
    }
  };

  return (
    <div
      className={`flex items-center mb-2 px-6 py-3 rounded ${className}`}
      style={{ backgroundColor: bgColor }}
    >
      <svg
        className={`feather-icon ${iconHeight} ${iconWidth} lg:rounded-full text-white mr-4 p-1`}
        style={{ backgroundColor: msgBGColor }}
      >
        <use xlinkHref={`/assets/svg/feather-sprite.svg#${icon}`} />
      </svg>
      <span className="text-base text-center" style={{ color: msgBGColor }}>
        {message}
      </span>
    </div>
  );
};

export default BaseAlert;
