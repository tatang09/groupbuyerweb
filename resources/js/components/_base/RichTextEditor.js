import React, { useState, useRef } from "react";
import { Editor, EditorState, RichUtils } from "draft-js";
import "../../helpers/styles/editor.css";
import "draft-js/dist/Draft.css";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faBold,
  faItalic,
  faUnderline,
  faListUl,
  faListOl
} from "@fortawesome/free-solid-svg-icons";

import { sweetAlert } from "../_base/alerts/sweetAlert";

export default ({
  title = "",
  richEditorState,
  richEditorStateContent,
  characterLimit = -1
}) => {
  const [editorState, setEditorState] = useState(richEditorState);
  const editor = useRef(null);
  const focus = editor.focus;
  const onChange = editorState => {
    setEditorState(editorState);
    richEditorStateContent(editorState);
  };
  const handleKeyCommand = command => _handleKeyCommand(command);

  const toggleBlockType = type => _toggleBlockType(type);
  const toggleInlineStyle = style => _toggleInlineStyle(style);

  const _handleKeyCommand = command => {
    const newState = RichUtils.handleKeyCommand(editorState, command);
    if (newState) {
      onChange(newState);
      return true;
    }
    return false;
  };

  const _toggleBlockType = blockType => {
    onChange(RichUtils.toggleBlockType(editorState, blockType));
  };

  const _toggleInlineStyle = inlineStyle => {
    onChange(RichUtils.toggleInlineStyle(editorState, inlineStyle));
  };

  let className = "RichEditor-editor";
  var contentState = editorState.getCurrentContent();
  if (!contentState.hasText()) {
    if (
      contentState
        .getBlockMap()
        .first()
        .getType() !== "unstyled"
    ) {
      className += " RichEditor-hidePlaceholder";
    }
  }

  const getLengthOfSelectedText = () => {
    const currentSelection = editorState.getSelection();
    const isCollapsed = currentSelection.isCollapsed();

    let length = 0;

    if (!isCollapsed) {
      const currentContent = editorState.getCurrentContent();
      const startKey = currentSelection.getStartKey();
      const endKey = currentSelection.getEndKey();
      const startBlock = currentContent.getBlockForKey(startKey);
      const isStartAndEndBlockAreTheSame = startKey === endKey;
      const startBlockTextLength = startBlock.getLength();
      const startSelectedTextLength =
        startBlockTextLength - currentSelection.getStartOffset();
      const endSelectedTextLength = currentSelection.getEndOffset();
      const keyAfterEnd = currentContent.getKeyAfter(endKey);
      
      if (isStartAndEndBlockAreTheSame) {
        length +=
          currentSelection.getEndOffset() - currentSelection.getStartOffset();
      } else {
        let currentKey = startKey;

        while (currentKey && currentKey !== keyAfterEnd) {
          if (currentKey === startKey) {
            length += startSelectedTextLength + 1;
          } else if (currentKey === endKey) {
            length += endSelectedTextLength;
          } else {
            length += currentContent.getBlockForKey(currentKey).getLength() + 1;
          }

          currentKey = currentContent.getKeyAfter(currentKey);
        }
      }
    }

    return length;
  };

  const handleBeforeInput = () => {
    const currentContent = editorState.getCurrentContent();
    const currentContentLength = currentContent.getPlainText("").length;
    const selectedTextLength = getLengthOfSelectedText(editorState);

    if (characterLimit !== -1) {
      if (currentContentLength - selectedTextLength > characterLimit - 1) {
       
        return "handled";
      }
    }
  };

  const handlePastedText = pastedText => {
    const currentContent = editorState.getCurrentContent();
    const currentContentLength = currentContent.getPlainText("").length;
    const selectedTextLength = getLengthOfSelectedText();

    if (characterLimit !== -1) {
      if (
        currentContentLength + pastedText.length - selectedTextLength >
        characterLimit
      ) {
        sweetAlert('error','Input should be less than or equal to 2000 characters!');
        return "handled";
      }
    }
  };

  return (
    <>
      <div className={`flex`}>
  
        <div className={`w-full font-bold`}>{title} </div>
        {characterLimit !== -1 && (
          <div className={` Richeditor-counter text-right `}>
            {`Characters remaining: `}
            <span className="w-10">
              {characterLimit -
                editorState.getCurrentContent().getPlainText().length}
            </span>
          </div>
        )}
      </div>
      <div className="RichEditor-root">
        <InlineStyleControls
          editorState={editorState}
          onToggle={toggleInlineStyle}
        />
        <BlockStyleControls
          editorState={editorState}
          onToggle={toggleBlockType}
        />

        <div className={className} onClick={focus}>
          <Editor
            blockStyleFn={getBlockStyle}
            customStyleMap={styleMap}
            editorState={editorState}
            handleKeyCommand={handleKeyCommand}
            handleBeforeInput={handleBeforeInput}
            handlePastedText={handlePastedText}
            onChange={onChange}
            ref={editor}
            spellCheck={true}
          />
        </div>
      </div>
    </>
  );
};

const styleMap = {
  CODE: {
    backgroundColor: "rgba(0, 0, 0, 0.05)",
    fontFamily: '"Inconsolata", "Menlo", "Consolas", monospace',
    fontSize: 16,
    padding: 2
  }
};

const getBlockStyle = block => {
  switch (block.getType()) {
    case "blockquote":
      return "RichEditor-blockquote";
    default:
      return null;
  }
};

const BLOCK_TYPES = [
  //  { label: "H1", style: "header-one" },
  //  { label: "H2", style: "header-two" },
  //  { label: "H3", style: "header-three" },
  //  { label: "H4", style: "header-four" },
  //  { label: "H5", style: "header-five" },
  //  { label: "H6", style: "header-six" },
  // { label: "Blockquote", style: "blockquote" },
  { label: "list-ul", style: "unordered-list-item" },
  { label: "list-ol", style: "ordered-list-item" }
  //  { label: "Code Block", style: "code-block" }
];

const BlockStyleControls = props => {
  const { editorState } = props;
  const selection = editorState.getSelection();
  const blockType = editorState
    .getCurrentContent()
    .getBlockForKey(selection.getStartKey())
    .getType();

  return (
    <div className="RichEditor-controls inline-flex">
      {BLOCK_TYPES.map(type => (
        <StyleButton
          key={type.label}
          active={type.style === blockType}
          label={type.label}
          onToggle={props.onToggle}
          style={type.style}
        />
      ))}
    </div>
  );
};

var INLINE_STYLES = [
  { label: "bold", style: "BOLD" },
  { label: "italic", style: "ITALIC" },
  { label: "underline", style: "UNDERLINE" }
  //  { label: "Monospace", style: "CODE" }
];

const InlineStyleControls = props => {
  var currentStyle = props.editorState.getCurrentInlineStyle();
  return (
    <div className="RichEditor-controls inline-flex">
      {INLINE_STYLES.map(type => (
        <StyleButton
          key={type.label}
          active={currentStyle.has(type.style)}
          label={type.label}
          onToggle={props.onToggle}
          style={type.style}
        />
      ))}
    </div>
  );
};

const StyleButton = props => {
  const onToggle = e => {
    e.preventDefault();
    props.onToggle(props.style);
  };

  let className = "RichEditor-styleButton";
  if (props.active) {
    className += " RichEditor-activeButton";
  }

  return (
    <span className={className} onMouseDown={onToggle}>
      <FontAwesomeIcon icon={["fas", `${props.label}`]} />
    </span>
  );
};
