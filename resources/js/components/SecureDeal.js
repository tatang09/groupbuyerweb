import React, { createRef, useState } from "react";

import UserGlobal from "~/states/userGlobal";
import { Modal, Button, Tooltip } from "~/components/_base";
import { isLoggedIn } from "~/services/auth";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

import { Checkbox } from "pretty-checkbox-react";
import CurrencyFormat from "react-currency-format";
import isEmpty from "lodash/isEmpty";
import { sweetAlert } from "../components/_base/alerts/sweetAlert";

import PaymentConfirmationModal from "./payment/PaymentConfirmationModal";
 
import * as deviceSizes from "../helpers/deviceSizeHelper";

const CommBankIntegration = ({ property, project, handleClose }) => {
  const [errors, setErrors] = useState({});
  const [loading, setLoading] = useState(false);
  const [userState, userAction] = UserGlobal();
  const [cardNumber, setCardNumber] = useState({});
  const [cardCvc, setCardCvc] = useState({});
  const [cardExpiryYear, setCardExpiryYear] = useState({});
  const [cardExpiryMonth, setCardExpiryMonth] = useState({});
  const [agree, setAgree] = useState(false);
  const [toggleOpen, setToggleOpen] = useState(false);
  const [tokenData, setTokenData] = useState({});
  const [nameOnCard, setNameOnCard] = useState("");
  const [formData, setFormData] = useState(null);

  const form = createRef();

  const createPayment = async formData => {
    let errors = {};
    try {
      window.location = `/weekly-deals/${project.id}/apartment/${
        property.id
      }/${false}/confirm`;
      // await axios.post("/api/create-payment", formData).then(res => {
      //   if (
      //     res.status === 200 &&
      //     res.data.status === "DECLINED" &&
      //     res.data.statusCode == 400
      //   ) {
      //     if (res.data.errors) {
      //       setErrors(res.data.errors);
      //     } else {
      //       let msg = res.data.declineReason.replace(/_/g, " ");
      //       sweetAlert("error", msg);
      //     }
      //     setLoading(false);
      //   }

      //   if (
      //     res.status == 200 &&
      //     res.data.statusCode == 200 &&
      //     res.data.status === "APPROVED"
      //   ) {
      //     window.location = `/weekly-deals/${project.id}/apartment/${
      //       property.id
      //     }/${false}/confirm`;
      //   }
      // });
    } catch (error) {
      sweetAlert("error", "Please check your card details.");
      let { data } = error.response;
      errors = data.errors;
      setLoading(false);
      setErrors(errors || {});
      return;
    }
  };

  const handleSubmit = async e => {
    e.preventDefault();
    let errors = {};

    let formData = new FormData(form.current);
    formData.append("property_id", property.id);
    formData.append("expMonth", cardExpiryMonth);
    formData.append("expYear", cardExpiryYear);
    formData.append("cardNumber", cardNumber);
    setFormData(formData);
    setLoading(true);

    if (!agree) {
      sweetAlert("error", "Please agree with our Terms & Conditions.");
      setLoading(false);
      return;
    }

    try {
      createPayment(formData);
      // await axios
      //   .post("/api/generate-payment-token", formData, {
      //     headers: {
      //       Authorization: "Bearer " + isLoggedIn()
      //     }
      //   })
      //   .then(res => {
         
      //     setLoading(false);
      //     if (res.status === 200) {
      //       if (
      //         !isEmpty(res.data.card) &&
      //         res.data.card.secure3DData &&
      //         res.data.card.secure3DData.isEnrolled
      //       ) {
      //         setToggleOpen(!toggleOpen);
      //         setTokenData(res);
      //       }

      //       if (
      //         !isEmpty(res.data.card) &&
      //         res.data.card.secure3DData &&
      //         !res.data.card.secure3DData.isEnrolled
      //       ) {
      //         createPayment(formData);
      //       }
      //     }
      //     if (
      //       (res.status === 200 &&
      //         res.data.status === "DECLINED" &&
      //         res.data.statusCode == 400) ||
      //       res.data.statusCode == 405
      //     ) {
      //       setLoading(false);
      //       if (res.data.statusCode != 405) {
      //         setErrors(res.data.errors);
      //       } else {
      //         sweetAlert("error", res.data.errors.message);
      //       }
      //     }
      //   });
    } catch (error) {
      let { data } = error.response;
      errors = data.errors;
      setLoading(false);
      setErrors(errors || {});
      return;
    }
  };

  const limit = (val, max) => {
    if (val.length === 1 && val[0] > max[0]) {
      val = "0" + val;
    }

    if (val.length === 2) {
      if (Number(val) === 0) {
        val = "01";

        //this can happen when user paste number
      } else if (val > max) {
        val = max;
      }
    }

    return val;
  };

  const setExpiration = value => {
    let month = limit(value.substring(0, 2), "12");
    let year = value.substring(3, 5);

    setCardExpiryMonth(parseInt(month));
    setCardExpiryYear(parseInt(year));
  };

  const sanitizeCardNumber = e => {
    let val = e.replace(/\s/g, "");
    setCardNumber(parseInt(val));
  };

  return (
    <>
      <form ref={form} className={`text-white pb-6`} onSubmit={handleSubmit}>
        <div className={`bg-gray-200 flex mt-10 rounded-lg flex-col`}>
          <div className={`bg-white lg:p-8 mb-8 mt-4 mx-3 p-4 pb-6 rounded-lg`}>
            <div className={`text-xl font-bold text-black mb-2`}>
              Payment Details
            </div>
            <div className={`border-b-2 px-2 py-3 flex items-center w-full`}>
              <svg
                className={`feather-icon mr-3 h-6 w-6 text-gray-600 ${
                  errors["nameOnCard"] ? "text-red-500" : ""
                }`}
              >
                <use xlinkHref={`/assets/svg/feather-sprite.svg#user`} />
              </svg>

              <input
                className={`appearance-none bg-transparent text-black border-0 font-hairline leading-relaxed text-base w-full`}
                name={`nameOnCard`}
                placeholder={`Name on card`}
                onChange={e => {
                  setNameOnCard(e.target.value);
                  setErrors({});
                }}
              />
            </div>
            {errors["nameOnCard"] && (
              <span className={`text-red-500 mt-2`}>
                {errors["nameOnCard"]}
              </span>
            )}

            <div className={`flex flex-1 items-start`}>
              <div className={`flex-1`}>
                <div
                  className={`border-b-2 px-2 py-3 flex items-center w-full`}
                >
                  <svg
                    className={`feather-icon mr-3 h-6 w-6 text-gray-600 ${
                      errors["cardNumber"] ? "text-red-500" : ""
                    }`}
                  >
                    <use
                      xlinkHref={`/assets/svg/feather-sprite.svg#credit-card`}
                    />
                  </svg>
                  <CurrencyFormat
                    placeholder="Card number"
                    name={`cardNumber`}
                    format="#### #### #### ####"
                    className={`text-gray-600 text-base w-full`}
                    onChange={e => {
                      sanitizeCardNumber(e.target.value);
                      setErrors({});
                    }}
                  />
                </div>
                {errors["cardNumber"] && (
                  <span className={`text-red-500 mt-2`}>
                    {errors["cardNumber"]}
                  </span>
                )}
              </div>
            </div>

            {window.innerWidth <= deviceSizes.baseSmall && (
              <div className={`flex-1`}>
                <div
                  className={`border-b-2 px-2 py-3 flex items-center w-full`}
                >
                  <svg
                    className={`feather-icon h-6 mr-3 text-gray-600 w-6 ${
                      errors["expMonth"] || errors["expYear"]
                        ? "text-red-500"
                        : ""
                    }`}
                  >
                    <use
                      xlinkHref={`/assets/svg/feather-sprite.svg#calendar`}
                    />
                  </svg>

                  <CurrencyFormat
                    format="##/##"
                    placeholder="MM/YY"
                    className={`text-gray-600 text-base`}
                    onChange={e => {
                      setExpiration(e.target.value);
                      setErrors({});
                    }}
                  />
                </div>
                {(errors["expYear"] || errors["expMonth"]) && (
                  <span className={`text-red-500 mt-2`}>
                    {`The expiry month and year is required.`}
                  </span>
                )}
              </div>
            )}

            {window.innerWidth <= deviceSizes.baseSmall && (
              <div className={`flex-1`}>
                <div
                  className={`border-b-2 px-2 py-3 flex items-center w-full`}
                >
                  <svg
                    className={`feather-icon mr-3 h-6 w-6 text-gray-600 ${
                      errors["cvc"] ? "text-red-500" : ""
                    }`}
                  >
                    <use xlinkHref={`/assets/svg/feather-sprite.svg#key`} />
                  </svg>
                  <CurrencyFormat
                    format="###"
                    placeholder="CVC"
                    className={`text-gray-600 text-base`}
                    name={`cvc`}
                    onChange={e => {
                      setCardCvc(e.target.value);
                      setErrors({});
                    }}
                  />
                </div>
                {errors["cvc"] && (
                  <span className={`text-red-500 mt-2`}>{errors["cvc"]}</span>
                )}
              </div>
            )}

            {window.innerWidth > deviceSizes.baseSmall && (
              <div className={`flex flex-1 items-start`}>
                <div className={`flex-1 mr-2`}>
                  <div
                    className={`border-b-2 px-2 py-3 flex items-center w-full`}
                  >
                    <svg
                      className={`feather-icon h-6 mr-3 text-gray-600 w-6 ${
                        errors["expMonth"] || errors["expYear"]
                          ? "text-red-500"
                          : ""
                      }`}
                    >
                      <use
                        xlinkHref={`/assets/svg/feather-sprite.svg#calendar`}
                      />
                    </svg>

                    <CurrencyFormat
                      format="##/##"
                      placeholder="MM/YY"
                      className={`text-gray-600 text-base`}
                      onChange={e => {
                        setExpiration(e.target.value);
                        setErrors({});
                      }}
                    />
                  </div>
                  {(errors["expYear"] || errors["expMonth"]) && (
                    <span className={`text-red-500 mt-2`}>
                      {`The expiry month and year is required.`}
                    </span>
                  )}
                </div>
                <div className={`flex-1`}>
                  <div
                    className={`border-b-2 px-2 py-3 flex items-center w-full`}
                  >
                    <svg
                      className={`feather-icon mr-3 h-6 w-6 text-gray-600 ${
                        errors["cvc"] ? "text-red-500" : ""
                      }`}
                    >
                      <use xlinkHref={`/assets/svg/feather-sprite.svg#key`} />
                    </svg>
                    <CurrencyFormat
                      format="###"
                      placeholder="CVC"
                      className={`text-gray-600 text-base`}
                      name={`cvc`}
                      onChange={e => {
                        setCardCvc(e.target.value);
                        setErrors({});
                      }}
                    />
                  </div>
                  {errors["cvc"] && (
                    <span className={`text-red-500 mt-2`}>{errors["cvc"]}</span>
                  )}
                </div>
              </div>
            )}

            <div className="flex flex-col mb-4 mt-5">
              <div className={`flex flex-1 flex-row`}>
                <div className={`flex-1`}>
                  <Checkbox
                    className={`lg:text-base remember_me mr-0 text-gray-800 font-bold`}
                    shape={`round`}
                    color={`primary`}
                    value={agree}
                    onChange={() => {
                      setAgree(!agree);
                      setErrors({});
                    }}
                  >
                    I agree to the&nbsp;
                  </Checkbox>
                  <a>
                    <span
                      className="cursor-pointer font-bold hover:text-blue-600 lg:text-base text-gray-800"
                      onClick={() =>
                        userAction.setState({ showTermsAndConditions: true })
                      }
                    >
                      Terms &amp; Conditions
                    </span>
                  </a>
                </div>
              </div>
              {errors["agree"] && (
                <span className={`ml-6 mt-2 text-red-500`}>
                  {errors["agree"]}
                </span>
              )}
            </div>

            <div className="flex items-center mt-8">
              <Button disabled={loading}>
                {loading && (
                  <FontAwesomeIcon
                    icon={["fas", "spinner"]}
                    className={`fa-spin mr-2`}
                  />
                )}
                {loading
                  ? "Saving..."
                  : window.innerWidth >= deviceSizes.baseSmall
                  ? "Submit Payment"
                  : "Submit"}
              </Button>
              <div className={`ml-6 mt-3`}>
                <span className={`text-gray-800 text-base font-bold italic`}>
                  Powered by:
                  <img
                    className={`-mt-3`}
                    src={`/assets/images/commbank_logo.png`}
                    style={{ width: 200 }}
                  />
                </span>
              </div>
            </div>
          </div>
        </div>
      </form>

      <PaymentConfirmationModal
        show={toggleOpen}
        data={tokenData}
        handleClose={setToggleOpen}
        formData={formData}
        project={project}
        property={property}
      />
    </>
  );
};

const SecureDeal = ({ show, project, apartment, handleClose }) => {
  const [userState] = UserGlobal();

  return (
    <Modal
      show={show}
      title={`Secure this deal`}
      maxWidth={`sm`}
      onClose={() => handleClose()}
    >
      <div className={`lg:px-2`}>
        <p className="text-base text-white font-bold mb-8">
          {/* {userState.user && userState.user.first_name}
          , you can secure this deal for only $1,000 which is fully refunded at exchange of contracts. */}
          {userState.user && userState.user.first_name}, secure this deal today
          for only $1,000 and it's fully refunded at exchange of contracts.
        </p>
        <div className="bg-gray-200 flex flex-col px-4 lg:px-8 py-4 rounded-lg">
          <div className="flex flex-row">
            <div>
              <h3 className="font-bold text-2xl lg:text-4xl">
                Apartment {apartment.unit_name}
              </h3>
            </div>
            <div>{/* the orchrads logo */}</div>
          </div>

          <div className={`my-2`}>
            <div className="text-base lg:text-lg font-bold">
              {project.address && (
                <>
                  {project.address.line_1 ? project.address.line_1 + ", " : ""}
                  {project.address.line_2 ? project.address.line_2 + ", " : ""}
                  {project.address.suburbs
                    ? project.address.suburbs + ", "
                    : ""}
                  {project.address.state ? project.address.state : ""}
                </>
              )}
            </div>
            <div className={`font-bold text-base flex items-center`}>
              <span className={`mr-4`}>
                {apartment.no_of_bedrooms}{" "}
                <FontAwesomeIcon icon={["fas", "bed"]} />
              </span>
              <span className={`mr-4`}>
                {apartment.no_of_bathrooms}{" "}
                <FontAwesomeIcon icon={["fas", "bath"]} />
              </span>
              <span className={`mr-4`}>
                {apartment.no_of_garages}{" "}
                <FontAwesomeIcon icon={["fas", "car"]} />
              </span>
            </div>
          </div>

          <hr
            className="my-2 border-b  opacity-50"
            style={{ borderColor: "#bdbdbd" }}
          />

          <div className="flex">
            <div className="flex-1">
              <span className="font-semibold line-through text-lg lg:text-2xl">
                WAS:{" "}
                <CurrencyFormat
                  value={apartment.price || 0}
                  displayType={`text`}
                  thousandSeparator={true}
                  prefix={`$`}
                />
              </span>
            </div>
            <div className="flex flex-1 justify-end">
              <span className="font-semibold text-lg lg:text-2xl text-red-700">
                SAVE:{" "}
                <CurrencyFormat
                  value={(apartment.price * project.discount) / 100}
                  displayType={`text`}
                  thousandSeparator={true}
                  prefix={`$`}
                />
              </span>
            </div>
          </div>

          <div className="flex mt-2">
            <div className="flex-1">
              <span className="text-base lg:text-lg font-bold">Price</span>
            </div>
            <div className="flex-1 text-right">
              <span className="text-base lg:text-lg font-bold">
                <CurrencyFormat
                  value={
                    apartment.price - (apartment.price * project.discount) / 100
                  }
                  displayType={`text`}
                  thousandSeparator={true}
                  prefix={`$`}
                />
              </span>
            </div>
          </div>

          <div className="flex mt-1">
            <div className="flex-1">
              <div className="flex font-bold">
                <p className="text-base lg:text-lg mb-0">
                  GroupBuyer "secure deal" fee{" "}
                </p>
                <Tooltip
                  title={`Please refer to our FAQ's and Terms & Conditions`}
                  placement={`top`}
                >
                  <svg
                    className={`border-b-0 feather-icon h-5 ml-2 mt-1 w-5`}
                    style={{ color: "#BD00F2" }}
                  >
                    <use xlinkHref={`/assets/svg/feather-sprite.svg#info`} />
                  </svg>
                </Tooltip>
              </div>
              <span className="text-xs lg:text-sm opacity-50">
                Refunded at exchange of contracts
              </span>
            </div>
            <div className="text-right">
              <p className="text-base lg:text-lg font-bold">$1,000</p>
            </div>
          </div>

          <hr
            className="mt-4 mb-2 border-b opacity-50"
            style={{ borderColor: "#bdbdbd" }}
          />

          <div className="flex">
            <span className="flex-1 font-semibold lg:text-2xl text-xl">
              Total to pay now
            </span>
            <span className="lg:text-2xl text-xl font-semibold text-right">
              $1,000
            </span>
          </div>
        </div>

        <CommBankIntegration
          property={apartment}
          project={project}
          handleClose={() => handleClose()}
        />

        {/* <Elements stripe={stripePromise}>
          <StripeForm
            property={apartment}
            project={project}
            handleClose={() => handleClose()}
          />
        </Elements> */}
      </div>
    </Modal>
  );
};

export default SecureDeal;
