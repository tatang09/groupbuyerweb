import React, { createRef, useState, useEffect } from "react";

import isEmpty from "lodash/isEmpty";
import { isLoggedIn } from "~/services/auth";

const paypalClientId = process.env.MIX_PAYPAL_CLIENT_ID;

const appendScriptToBody = loadPaypalComponent => {
  const script = document.createElement("script");

  script.src = `https://www.paypal.com/sdk/js?client-id=${paypalClientId}&currency=AUD&components=buttons`;
  // "https://www.paypal.com/sdk/js?client-id=SB_CLIENT_ID";

  script.addEventListener("load", loadPaypalComponent);

  document.body.appendChild(script);
};

const smartButtonStyle = () => {
  return {
    layout: "vertical",
    color: "black",
    shape: "rect",
    label: "paypal",
    size: "medium"
    //tagline: true
  };
};

export const PaypalButton = ({
  property_id,
  project_id,
  setLoading,
  setOpen
}) => {
  const [paypalLoaded, setPaypalLoaded] = useState(false);
  const [showCancelBtn, setShowCancelBtn] = useState(false);

  const form = createRef();

  const handleCallBack = async details => {
    let formData = new FormData();

    formData.append("property_id", property_id);

    if (!isEmpty(details) && details.status === "COMPLETED") {
      formData.append("transaction_id", details.id);
      formData.append("paypal_payer_id", details.payer.payer_id);
      formData.append("amount", 1000);
      formData.append("status", details.status);

      const { data } = await axios.post("/api/paypal-payment", formData, {
        headers: {
          Authorization: "Bearer " + isLoggedIn()
        }
      });
      setLoading(false);
      window.location = `/weekly-deals/${project_id}/apartment/${property_id}/confirm`;
    }
  };

  useEffect(() => {
    appendScriptToBody(loadPaypalComponent);
  }, []);

  const smartBtnConfig = () => {
    return {
      intent: "Secure Deal Fee",

      currency_code: "AUD",

      style: smartButtonStyle(),

      onButtonReady: () => {
        setPaypalLoaded(!paypalLoaded);
      },

      onClick: () => {
        setOpen(false);
      },

      createOrder: (data, actions) => {
        setShowCancelBtn(true);

        return actions.order.create({
          purchase_units: [
            {
              description: "Secure Deal Fee",
              amount: {
                currency_code: "AUD",
                value: 1000
              }
            }
          ]
        });
      },

      onApprove: async (data, actions) => {
        return await actions.order.capture().then(function(details) {
          handleCallBack(details);
        });
      },

      onError: err => {
      
      }
    };
  };

  const loadPaypalComponent = () => {
    setPaypalLoaded(!paypalLoaded);
    window.paypal.Buttons(smartBtnConfig()).render("#paypal-button-container");
  };

  return (
    <div
      id="paypal-button-container"
      className={`rounded-lg mx-3`}
      style={{ height: 160 }}
    >
      {showCancelBtn && (
        // style={{ backgroundColor: 'rgb(0, 112, 186)' }}
        // style={{ color: 'rgb(255, 255, 255)' }}
        <div
          className={`cursor-pointer flex h-12 items-center justify-center mb-4 mx-16 rounded rounded-full`}
        >
          <button
            onClick={() => {
              {
                setShowCancelBtn(false);
                appendScriptToBody(loadPaypalComponent);
              }
            }}
            className={`font-bold hover:underline text-gray-700 text-lg`}
            style={{ color: "rgb(255, 255, 255)" }}
          >
            Cancel
          </button>
        </div>
      )}
    </div>
  );
};

export const PaypalDummyButton = ({ agree, swal }) => {
  const [paypalLoaded, setPaypalLoaded] = useState(false);

  const smartBtnConfig = () => {
    return {
      style: smartButtonStyle(),

      onButtonReady: () => {
        setPaypalLoaded(!paypalLoaded);
      },

      onInit: function(data, actions) {
        // Disable the buttons
        actions.disable();
      },

      onClick: function() {
        if (!agree) {
          swal.fire({
            type: "error",
            title: "Please agree with our Terms & Conditions.",
            showConfirmButton: false,
            timer: 3000
          });
        }
      }
    };
  };

  const loadPaypalComponent = () => {
    setPaypalLoaded(!paypalLoaded);
    window.paypal
      .Buttons(smartBtnConfig())
      .render("#paypal-dummy-button-container");
  };

  useEffect(() => {
    appendScriptToBody(loadPaypalComponent);
  }, []);

  return (
    <div id="paypal-dummy-button-container" className={`rounded-lg mx-3`} />
  );
};
