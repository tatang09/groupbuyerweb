import React, { useState, useEffect } from "react";
import { isMobile } from "react-device-detect";

const Testimonials = () => {

    const testimonialsArr = [
        {
            "comment": "“Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi ut turpis in velit ultricies dignissim. Morbi viverra libero quis est porttitor tincidunt.",
            "name": "John Smith",
            "company": "Sekisui House"
        },
        {
            "comment": "“Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi ut turpis in velit ultricies dignissim. Morbi viverra libero quis est porttitor tincidunt.",
            "name": "Emile Rastol",
            "company": "The French Toast"
        },
        {
            "comment": "“Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi ut turpis in velit ultricies dignissim. Morbi viverra libero quis est porttitor tincidunt.",
            "name": "Nick Sammut",
            "company": "Superman"
        }
    ];


    const displayComments = () => {
        return testimonialsArr.map((item, key) => {
            return <div key={key}>
                <p>{item.comment}</p>
                <p className={`mb-10 text-right`}>{item.name}, {item.company}</p>
            </div>
        });
    }

    return (
        <section style={{
            background:
                !isMobile ? "url('/assets/images/Testimonials_large.png')" : "url('/assets/images/Testimonials.png')",
            // marginTop: isMobile ? -162 : -130,
            minHeight: "100%",
            backgroundBlendMode: "multiply",
            backgroundSize: "cover",
            backgroundRepeat: "no-repeat"
        }}>
            <div className={`flex flex-col items-center justify-center text-white`} >
                <div className={`lg:py-24 lg:w-2/3 px-8 py-12 sm:px-32`}>
                    <div className={`flex items-center justify-center mb-10 lg:mb-16`}>
                        <p className={`font-bold leading-none lg:text-5xl m-0 md:2/3 text-4xl text-center `}>What do they say about us</p>
                    </div>
                    <div className={`flex flex-col items-center justify-center`}>
                        {displayComments()}
                    </div>
                </div>
            </div>
        </section>
    );
}



export default Testimonials;