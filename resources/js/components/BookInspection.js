import React, { useState, useEffect } from "react";
import { Modal } from "~/components/_base";
import { Button, TextInput, TextArea, ResponseModal } from "~/components/_base";
import MuiAlert from "@material-ui/lab/Alert";
import Snackbar from "@material-ui/core/Snackbar";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import axios from "axios";
import UserGlobal from "~/states/userGlobal";
import { requestBookingInspection } from "../data/propertiesData/propertiesData";
import { sweetAlert } from "./_base/alerts/sweetAlert";

function Alert(props) {
  return <MuiAlert elevation={6} variant="filled" {...props} />;
}

const BookInspection = ({
  show,
  toogleShowBookingModal,
  backgroundColor = "#ffffff",
  propertyId = null,
  dealId
}) => {
  const [userState, userAction] = UserGlobal();
  const [loading, setLoading] = useState(false);
  const [showAlert, setShowAlert] = useState(false);
  const [emailData, setEmailData] = useState({
    name: "",
    phone: "",
    email: "",
    message: "",
    propertyId: 0,
    dealId: 0
  });
  const [errors, setErrors] = useState({});

  const handleSubmit = async e => {
    e.preventDefault();

    let errors = {};
    let formData = { ...emailData };
    if (formData.phone) {
      formData.phone = "+61" + emailData.phone;
    }
    if(propertyId){
      formData.propertyId = propertyId;
    }
    
    formData.dealId = dealId;
 
    setLoading(true);

    requestBookingInspection({ formData })
      .then(res => {
        if (res.status == 200) {
          setLoading(false);
          sweetAlert("success", res.data.message);
        }
      })
      .catch(err => {
        setLoading(false);
        setErrors(err.response.data.errors);
      });
  };

  const changeHandler = (key, value) => {
    let data = Object.assign({}, emailData);

    data[key] = value;

    setEmailData(data);
  };

  const handleClose = (event, reason) => {
    if (reason === "clickaway") {
      return;
    }

    setShowAlert(false);
  };

  useEffect(() => {
    if (userState.user) {
      let phone = userState.user.phone ? userState.user.phone.replace("+61", "") : "";
      setEmailData({
        name: userState.user.first_name + " " + userState.user.last_name,
        phone: phone,
        email: userState.user.email,
        message: ""
      });
    }
  }, []);

  return (
    <Modal
      show={show}
      maxWidth={`sm`}
      title={`Book Inspection`}
      onClose={() => toogleShowBookingModal()}
    >
      <div className={`bg-white lg:m-6 rounded`}>
        <form className="lg:w-full" onSubmit={handleSubmit}>
          <div
            className="lg:px-8 px-2 py-2 pt-6 pb-10 rounded-lg"
            style={{ background: backgroundColor }}
          >
            <TextInput
              className={`text-base border-b-2 border-gray-400`}
              type={`text`}
              placeholder={`Name`}
              prefix={`user`}
              errors={errors.name}
              value={emailData.name}
              onChange={e => changeHandler("name", e.target.value)}
            />
            {errors.name && (
              <span className="ml-10 text-xs text-red-500">{errors.name[0]}</span>
            )}

            <TextInput
              className={`text-base border-b-2 border-gray-400`}
              type={`text`}
              placeholder={`Email`}
              prefix={`mail`}
              errors={errors.email}
              value={emailData.email}
              onChange={e => changeHandler("email", e.target.value)}
            />
            {errors.email && (
              <span className="ml-10 text-xs text-red-500">{errors.email[0]}</span>
            )}

            <TextInput
              className={`text-base border-b-2 border-gray-400`}
              type={`text`}
              placeholder={`Phone (+61)`}
              prefix={`phone`}
              errors={errors.phone}
              value={emailData.phone}
              maxLength={`10`}
              type={`number`}
              onChange={e => changeHandler("phone", e.target.value)}
            />
            {errors.phone && (
              <span className="ml-10 text-xs text-red-500">{errors.phone[0]}</span>
            )}

            <TextArea
              className={`text-base border-2 p-2 h-40 border-gray-400 rounded`}
              placeholder={`Message`}
              prefix={`message-square`}
              errors={errors.message}
              value={emailData.message}
              onChange={e => changeHandler("message", e.target.value)}
            />
            
            <div className={`flex justify-end`}>
              <Button
                className={`py-2 mt-4 block mr-2 rounded`}
                disabled={loading}
              >
                {loading && (
                  <FontAwesomeIcon
                    icon={["fas", "spinner"]}
                    className={`fa-spin mr-2`}
                  />
                )}
                Submit
              </Button>
            </div>
          </div>
        </form>
      </div>
    </Modal>
  );
};

export default BookInspection;
