import React from "react";
import { isMobile } from "react-device-detect";

const BuyingProgress = ({ current = 1, project_id = 0, isPreview }) => {
  const steps = [
    {
      name: "Select project",
      step: 1
    },
    {
      name: "Select property",
      step: 2
    },
    {
      name: "Secure deal",
      step: 3
    },
    {
      name: "Agent contact",
      step: 4
    }
  ];

  const handleBackToSearch = () => {
    if (current == 3) {
      window.location = `/weekly-deals/${project_id}/${isPreview}`;
    }

    if (current == 2) {
      localStorage.setItem("__use_projects_filters", true);

      window.location = `/weekly-deals`;
    }
  };

  const renderProgress = () => {
    return steps.map((step, key) => {
      return (
        <li
          key={key}
           
          className={`
          ${key == 0 && isMobile ? "lg:pl-4 pl-2" : "pl-0"}
            relative pb-2 pt-4 lg:pt-6 w-1/4 lg:w-auto`}
          style={{
            minWidth: isMobile ? "unset" : 190,
            color: "#68889B",
            borderColor: "#B1ADAD"
          }}
        >
          <div className={`flex flex-row items-center justify-center text-xxs`}>
            {step.step < current && (
              <div
                className={`bg-palette-purple flex h-10 items-center justify-center rounded-full w-10`}
              >
                {step.step > 1 && (
                  <div
                    className={`absolute bg-palette-purple  h-1 items-center justify-center left-0 w-2/5`}
                  ></div>
                )}
                <span className={`text-base font-bold text-white`}>
                  {step.step}
                </span>
                {step.step < 4 && (
                  <div
                    className={`absolute bg-palette-purple   h-1 items-center justify-center right-0 w-2/5`}
                  ></div>
                )}
              </div>
            )}

            {step.step >= current && (
              <div
                className={`bg-gray-500 flex h-10 items-center justify-center rounded-full w-10`}
              >
                {step.step > 1 && (
                  <div
                    className={`absolute bg-gray-500 h-1 items-center justify-center left-0 w-2/5`}
                  ></div>
                )}

                <span className={`text-base font-bold text-white`}>
                  {step.step}
                </span>
                {step.step < 4 && (
                  <div
                    className={`absolute bg-gray-500  h-1 items-center justify-center right-0 w-2/5`}
                  ></div>
                )}
              </div>
            )}
 
          </div>
          <div
            className={`font-bold lg:font-normal text-xs lg:text-base text-center ${
              isMobile ? "truncate" : "" 
            }`}
          >
            {step.name}
          </div>

          {/* {step.step < current && (
            <svg
              className={`feather-icon absolute h-4 w-4 right-0 bottom-0 mr-3 mb-8 text-palette-purple`}
            >
              <use xlinkHref={`/assets/svg/feather-sprite.svg#check-square`} />
            </svg>
          )} */}
        </li>
      );
    });
  };

  return (
    <div style={{ background: "#f5f5f5" }}>
      <div
        className={`mx-auto`}
        style={{ maxWidth: 1366, position: "relative" }}
      >
        {!isMobile && (
          <div
            className={`absolute lg:px-16 mt-6 ${
              current == 1 || current == 5 ? "hidden" : ""
            }`}
          >
            <span
              className={`hover:underline absolute cursor-pointer duration-300 font-bold hover:text-palette-blue inline-flex items-center pr-3 text-sm text-base text-palette-blue-light transition-all z-10`}
              onClick={handleBackToSearch}
            >
              <svg className={`feather-icon h-4 w-4 mr-1 inline`}>
                <use
                  xlinkHref={`/assets/svg/feather-sprite.svg#chevron-left`}
                />
              </svg>
              Back
            </span>
            <div
              className={`absolute border-gray-200 border-t w-full`}
              style={{ top: 10 }}
            />
          </div>
        )}
        <ul
          className={`mx-auto flex`}
          style={{ width: isMobile ? "auto" : "fit-content" }}
        >
          {renderProgress()}
        </ul>
      </div>
    </div>
  );
};

export default BuyingProgress;
