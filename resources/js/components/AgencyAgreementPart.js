import React, { useState, useEffect } from "react";

const AgencyAgreementPart = ({ toggleSEM, toggleFetch }) => {
  return (
    <div className={`relative bg-white py-5 px-5 flex flex-col items-center m-5 rounded-lg`}>
      <img
        src="/assets/images/undraw_fill_forms_yltj.svg"
        height='250px'
        width='420px'
        style={{
          marginTop: '24px',
        }}
      />

      <h4
        className={
          "font-bold lg:w-3/4 w-full px-1 py-3 rounded-t text-2xl text-center"
        }
      >
        Your project will be approved by a GroupBuyer Admin Agent prior to being displayed online. Should your project not meet the requirements of GroupBuyer, a team member will be in contact with you. Click the 'View Agency Agreement' button to learn more about our terms and conditions.
      </h4>

      <a
        href={"/assets/pdf/group-buyer-agency-agreement-2020-10-08-draft.pdf"}
        target={`_blank`}
        className={`border border-gray-900 button py-3 flex items-center justify-around mr-2 mt-2 mb-2`}
      >
        <span className={`text-base text-gray-900`}>
          View Agency Agreement
        </span>
      </a>
    </div>
  );
};

export default AgencyAgreementPart;
