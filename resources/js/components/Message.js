import React from "react";

const Message = () => {
  return (
    <div className={`lg:mt-10 lg:px-64 py-10 rounded`}>
      <h1 className ={`font-semibold mb-4 text-base text-center text-gray-500`}>A message to our members.</h1>
      <p className ={`lg:px-24 text-base text-gray-500`}>
        We apologise for the temporary disruption to our listed projects. We’re
        excited to return very soon with more of Australia's biggest discounts.
        If you're not already a member, sign up for free today. Thank you for
        your patience during this time. 
      </p>      
    </div>
  );
};

export default Message;
