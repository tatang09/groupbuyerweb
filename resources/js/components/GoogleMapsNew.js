import React, { useEffect, useState, useRef } from 'react';
import isEmpty from "lodash/isEmpty";

const mapKey = `${process.env.MIX_GOOGLE_API_KEY}`;

const GoogleMapsNew = (project) => {

    const googleMapRef = useRef();
    const [zoom, setZoom] = useState(16);

    let panPath = [];
    let panQueue = [];
    let STEPS = 200;
    
    useEffect(() => {
        if (!isEmpty(project)) {
            appendMap(project);
        }
    }, []);

    const appendMap = (project) => {
        const googleMapScript = document.createElement("script");
        googleMapScript.src = `https://maps.googleapis.com/maps/api/js?key=${mapKey}`;
        googleMapScript.async = true;
        window.document.body.appendChild(googleMapScript);
        googleMapScript.addEventListener("load", () => {
            getLatLng(project);
        });
    };

   

    const getLatLng = (project) => {

        console.log(project.project.lat)
        let latlng = new google.maps.LatLng(
            parseFloat(project.project.lat),
            parseFloat(project.project.long)
        );

        let mapOptions = {
            zoom: zoom,
            center: latlng,
            gestureHandling: "cooperative"
        };

        let map = new google.maps.Map(googleMapRef.current, mapOptions);

        let contentString =
            '<div id="root-content">' +
            '<div class="flex flex-col mt-2">' +
            '<h2 class="font-black">' +
            !isEmpty(project) && project.project.name +
            "</h2></div></div";

        let infowindow = new google.maps.InfoWindow({
            content: contentString
        });

        let marker = new google.maps.Marker({
            position: latlng
        });

        marker.setMap(map);

        marker.addListener("click", () => {
            infowindow.getMap() ? infowindow.close() : infowindow.open(map, marker);
        });

        map.addListener("zoom_changed", () => {
            panToLocation(
                parseFloat(project.project.lat),
                parseFloat(project.project.long),
                map
            );
        });
        // map.setCenter(marker.getPosition());
    };

    const panToLocation = (newLat, newLng, map) => {
        if (panPath.length > 0) {
            // We are already panning...queue this up for next move
            panQueue.push([newLat, newLng]);
        } else {
            // Lets compute the points we'll use
            panPath.push("LAZY SYNCRONIZED LOCK"); // make length non-zero - 'release' this before calling setTimeout
            let curLat = map.getCenter().lat();
            let curLng = map.getCenter().lng();
            let dLat = (newLat - curLat) / STEPS;
            let dLng = (newLng - curLng) / STEPS;

            for (let i = 0; i < STEPS; i++) {
                panPath.push([curLat + dLat * i, curLng + dLng * i]);
            }
            panPath.push([newLat, newLng]);
            panPath.shift(); // LAZY SYNCRONIZED LOCK
            setTimeout(doPan(map), 700);
        }
    };


    const doPan = map => {
        let next = panPath.shift();
        if (next != null) {
            // Continue our current pan action
            map.panTo(new google.maps.LatLng(next[0], next[1]));
            setTimeout(doPan(map), 700);
        } else {
            // We are finished with this pan - check if there are any queue'd up locations to pan to
            let queued = panQueue.shift();
            if (queued != null) {
                panToLocation(queued[0], queued[1]);
            } else {
                // map.setZoom(zoom);
            }
        }
    };

    return (
        <div
            id="google-map"
            ref={googleMapRef}
            style={{ width: "100%", height: "100vh" }}
        />
    );
}

export default GoogleMapsNew;