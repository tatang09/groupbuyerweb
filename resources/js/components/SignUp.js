import React, {useState, createRef} from 'react';

import UserGlobal from '~/states/userGlobal';
import {Button, Modal, TextInput} from '~/components/_base';
import {axios} from '~/helpers/bootstrap';
import {setToken} from '~/services/auth';

import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
import {Radio} from 'pretty-checkbox-react';

import {sweetAlert} from '../components/_base/alerts/sweetAlert';

import FacebookLogin from 'react-facebook-login/dist/facebook-login-render-props';
import {GoogleLogin} from 'react-google-login';
import {isMobile} from 'react-device-detect';

import isEmpty from "lodash/isEmpty";

const SignUp = ({handleClose}) => {
  const [userState, userAction] = UserGlobal();
  const [showPassword, setShowPassword] = useState(false);
  const [errors, setErrors] = useState({});
  const [loading, setLoading] = useState(false);

  const form = createRef();

  const handleShowPassword = () => {
    setShowPassword(!showPassword);
  };

  const handleSubmit = async e => {
    e.preventDefault();
    let errors = {};
    let formData = new FormData(form.current);
    setLoading(true);

    try {
      let {data} = await axios.post('/api/register', formData);

      setToken(data.access_token);

      setLoading(false);

      userAction.setState({fetch: !userState.fetch});

      userAction.setState({userCount: userState.userCount + 1});

      handleClose();

      setTimeout(() => {
        sweetAlert(
          'success',
          'Thank you for registering and welcome to GroupBuyer.',
        );
      }, 2000);
    } catch (error) {
      let {data} = error.response;

      errors = data.errors;
      setLoading(false);
    }
    setErrors(errors || {});
  };

  const socialMediaLoginDataMapper = (response, isGoogle) => {
  
    let formData = new FormData();

    let newName = [];

    if (!isGoogle) {
      newName = response.name.split(' ', 2);

      formData.append('facebook_id', response.userID);
    } else {
      formData.append('google_id', response.profileObj.googleId);
    }

    formData.append(
      'first_name',
      !isGoogle ? newName[0] : response.profileObj.givenName,
    );

    formData.append(
      'last_name',
      !isGoogle ? newName[1] : response.profileObj.familyName,
    );

    formData.append(
      'email',
      !isGoogle ? response.email : response.profileObj.email,
    );

    formData.append(
      'avatar_path',
      !isGoogle ? response.picture.data.url : response.profileObj.imageUrl,
    );

    formData.append('is_google', isGoogle);

    return formData;
  };

  const responseFacebook = response => {
    if (isEmpty(response.email)) {
      return sweetAlert('error', 'Please use facebook account with email');
    }

    userLogin(socialMediaLoginDataMapper(response, false));
  };

  const responseGoogle = response => {
    userLogin(socialMediaLoginDataMapper(response, true));
  };

  const userLogin = async formData => {
    try {
      let {data} = await axios.post('/api/social-media-login', formData);

      setToken(data.access_token);

      userAction.setState({userCount: userState.userCount + 1});

      if (isMobile) {
        userAction.setState({showSignUp: false});
      }

      userAction.setState({fetch: !userState.fetch});
    } catch (error) {
      sweetAlert('error', 'Error on login, please try again!');
    }
  };

  return (
    <Modal
      show={userState.showSignUp}
      title={`Free to join`}
      onClose={() => handleClose()}
      transition={`grow`}>
      <form
        ref={form}
        className={`flex-row lg:p-6 text-white`}
        onSubmit={handleSubmit}>
        <div className={`flex flex-col lg:flex-row items-center`}>
          <div className={`w-full`}>
            <TextInput
              className={`text-base`}
              type={`text`}
              placeholder={`First Name`}
              name={`first_name`}
              prefix={`user`}
              errors={errors.first_name}
            />
            {errors.first_name && (
              <span className="text-red-500 text-xs">
                {errors.first_name[0]}
              </span>
            )}
          </div>
          <div className={`hidden lg:block w-12`}></div>
          <div className={`w-full`}>
            <TextInput
              type={`text`}
              className={`text-base`}
              placeholder={`Last Name`}
              name={`last_name`}
              prefix={`user`}
              errors={errors.last_name}
            />
            {errors.last_name && (
              <span className="text-red-500 text-xs">
                {errors.last_name[0]}
              </span>
            )}
          </div>
        </div>

        <TextInput
          type={`text`}
          className={`text-base`}
          placeholder={`Email`}
          name={`email`}
          errors={errors.email}
          prefix={`mail`}
        />
        {errors.email && (
          <span className="text-red-500 text-xs">{errors.email[0]}</span>
        )}
        {/*
        <TextInput
          type={`text`}
          placeholder={`Phone`}
          name={`phone`}
          prefix={`phone`}
        /> */}

        <TextInput
          type={showPassword ? 'text' : 'password'}
          placeholder={`Password`}
          className={`text-base`}
          name={`password`}
          prefix={`lock`}
          suffix={
            <span
              className={`cursor-pointer`}
              onClick={() => handleShowPassword()}>
              <svg className={`feather-icon h-5 w-5 opacity-50`}>
                <use
                  xlinkHref={`/assets/svg/feather-sprite.svg#${
                    showPassword ? 'eye-off' : 'eye'
                  }`}
                />
              </svg>
            </span>
          }
          errors={errors.password}
        />
        {errors.password && (
          <span className="text-red-500 text-xs">{errors.password[0]}</span>
        )}

        <div
          className={`flex flex-col lg:flex-row lg:items-center justify-between my-8 px-2`}>
          <Radio
            className={`text-base remember_me`}
            shape={`round`}
            color={`primary`}
            name={`user_type`}
            value={`customer`}
            inputProps={{
              defaultChecked: true,
            }}>
            I'm looking to buy
          </Radio>
          <Radio
            className={`mt-4 lg:mt-0 text-base remember_me`}
            shape={`round`}
            color={`primary`}
            name={`user_type`}
            value={`project_developer`}>
            I'm a developer/seller
          </Radio>
        </div>

        <div
          className={`flex flex-col lg:flex-row items-center justify-between mt-16`}>
          <Button className={`px-20`} disabled={loading}>
            {loading && (
              <FontAwesomeIcon
                icon={['fas', 'spinner']}
                className={`fa-spin mr-2`}
              />
            )}
            Sign up
          </Button>
          <div className={`text-gray-600 text-base mt-4 lg:mt-0`}>
            Already have an account?
            <span
              className={`cursor-pointer text-palette-blue-light hover:text-palette-teal transition-all duration-300`}
              onClick={() => {
                handleClose();
                userAction.setState({showSignIn: true});
              }}>
              &nbsp;Log in
            </span>
          </div>
        </div>
      </form>

      <section>
        <div className={`flex my-4 items-center`}>
          <div
            className={`border-b-2 flex-1`}
            style={{borderColor: 'rgba(255,255,255,0.1)'}}
          />
          <div
            className={`bg-palette-blue-dark opacity-25 px-4 py-1 text-base text-white`}>
            or
          </div>
          <div
            className={`border-b-2 flex-1`}
            style={{borderColor: 'rgba(255,255,255,0.1)'}}
          />
        </div>

        <div className={`flex flex-col items-center mt-8 mb-16`}>
          <FacebookLogin
            appId="186207692714514"
            autoLoad={false}
            fields="name,email,picture"
            callback={responseFacebook}
            render={renderProps => (
              <button
                onClick={renderProps.onClick}
                className={`font-bold items-center flex px-8 py-2 rounded-full text-base text-white mb-8`}
                style={{background: '#4267b2'}}>
                <FontAwesomeIcon icon={['fab', 'facebook-f']} size={`lg`} />
                <span className={`ml-3`}>Sign up with Facebook</span>
              </button>
            )}
          />

          <GoogleLogin
            clientId="970841385117-1r70ifjjdqq7fi5h0vnkvki1k5q0kjsp.apps.googleusercontent.com"
            autoLoad={false}
            render={renderProps => (
              <button
                className={`bg-gray-100 font-bold rounded-full text-gray-800 px-8 py-2 items-center flex text-base`}
                onClick={renderProps.onClick}>
                <img src={`/assets/images/google-logo.png`} />
                <span className={`lg:ml-6 lg:mr-6 ml-3 mr-3`}>
                  Sign up with Google
                </span>
              </button>
            )}
            buttonText="Log in"
            onSuccess={responseGoogle}
            // onFailure={responseGoogle}
            cookiePolicy={'single_host_origin'}
          />
        </div>
      </section>
    </Modal>
  );
};

export default SignUp;
