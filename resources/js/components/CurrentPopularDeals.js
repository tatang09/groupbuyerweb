import React, { useState } from "react";
import { Table } from "~/components/_base";
import CurrencyFormat from "react-currency-format";
import { isMobile } from "react-device-detect";
import {
    discountedPriceRange,
    prices
} from "../helpers/pricingHelper/discountHelper";


const CurrentPopularDeals = ({ queyParams }) => {

    const [toggleFetch, setToggleFetch] = useState(false);
    const [keyword, setKeyword] = useState("");
    const [projectData, setProjectData] = useState([]);
    const [filters, setFilters] = useState({
        sortBy: { value: "deals.created_at", order: "desc", label: "Most Recent" },
        minPrice: { value: 0, label: "Any" },
        maxPrice: { value: 15000000, label: "Any" },
        types: "All",
        selectedTypes: { value: "All", label: "All" },
        locations: "All",
        selectedLocations: { value: "All", label: "All" }
    });

    const renderProjects = () => {
        return (
            <tbody className={`flex flex-wrap`}>
                {projectData &&
                    projectData.map((project, key) => {
                        return (
                            <tr key={key} className={`lg:w-4/12 mb-4 lg:mb-4 lg:px-3 w-full`}>
                                <>
                                    <td
                                        style={{ boxShadow: "2px 2px 5px 0px rgb(0 0 0 / 10%), 2px 2px 5px 0px rgb(0 0 0 / 6%)" }}
                                        className={`p-4 block bg-white rounded-lg lg:hover:scale-105 p-0`}
                                    //className={`p-4 block bg-white rounded-md lg:hover:scale-105 duration-300 transform transition-all p-0`}
                                    >
                                        <div
                                            className={`${key % 5 === 0 && key !== 0
                                                ? "text-palette-gray-main"
                                                : "text-palette-violet-main-dark"
                                                } text-2xl font-bold mb-3 leading-none py-3`}
                                        >
                                            {project.name}
                                        </div>
                                        <div
                                            className={`cursor-pointer`}
                                            onClick={() =>
                                                handleSelectProject(
                                                    `/weekly-deals/${project.id}/${false}`
                                                )
                                            }
                                        >
                                            <div
                                                className={`bg-center bg-cover relative text-white`}
                                                style={{
                                                    minHeight: isMobile ? 225 : 180,
                                                    background: `linear-gradient(rgba(0, 0, 0, 0.9) 0%,rgba(0, 0, 0, 0.1) 25%,rgba(0, 0, 0, 0.1) 0%, rgba(0, 0, 0, 0.2) 82%, rgba(0, 0, 0,0.8) 100%), url("${project.featured_images[0]}")`
                                                }}
                                            >
                                                {key % 5 === 0 && key !== 0 && (
                                                    <div
                                                        className={`absolute left-0 mt-3 overflow-hidden rounded-sm top-0`}
                                                    >
                                                        <div
                                                            className={`flex items-center ml-3`}
                                                        >
                                                            <img src={`assets/svg/sold_out.svg`} className={`w-2/3`} />
                                                        </div>
                                                    </div>
                                                )}

                                                {key % 3 === 0 && (
                                                    <div
                                                        className={`absolute left-0 mt-3 overflow-hidden rounded-sm top-0`}
                                                    >
                                                        <div className={`flex items-center ml-3`}>
                                                            <img src={`assets/svg/premium.svg`} className={`w-2/3`} />
                                                        </div>
                                                    </div>
                                                )}

                                                {project.discount && (
                                                    <div
                                                        className={`absolute top-0 right-0 mt-2 text-xl top-0`}
                                                    >
                                                        <div
                                                            className={`bg-transparent flex items-center mr-3`}
                                                        >
                                                            <span className={`text-base font-semibold`}>
                                                                {`${project.discount}% `}
                                                            </span>
                                                            <span className={`text-base font-semibold `}>
                                                                &nbsp;off
                                                            </span>
                                                        </div>
                                                    </div>
                                                )}

                                                <div
                                                    className={`absolute bottom-0 right-0 mb-2 mr-4 flex items-center text-base font-bold`}
                                                >
                                                    <span>From&nbsp;</span>
                                                    <CurrencyFormat
                                                        value={
                                                            project && discountedPriceRange(project, false)
                                                        }
                                                        displayType={`text`}
                                                        thousandSeparator={true}
                                                        prefix={`$`}
                                                    />
                                                </div>
                                            </div>
                                        </div>
                                        <div className={`pt-5 pb-0 px-2`}>
                                            <p className={`text-xs text-gray-900 font-light text-justify text-justify`}>
                                                Lorem ipsum dolor sit amet, consectetur adipiscing elit,
                                                sed do eiusmod tempor incididunt ut labore et dolore
                                                magna aliqua.
                                            </p>
                                        </div>
                                        <div
                                            className={`text-sm pb-4 px-2 flex justify-center items-center text-gray-900 text-justify`}
                                        >
                                            <div
                                                className={`w-1/3`}
                                            >
                                                <h4
                                                    className={`text-palette-purple-main font-extrabold`}
                                                >
                                                    Goal
                                            </h4>
                                                <h5 className={`font-light`}>15 Investors</h5>
                                            </div>
                                            <div
                                                className={`w-1/3 flex justify-center border-r border-l border-palette-purple-main`}
                                            >
                                                <div>
                                                    <h4
                                                        className={`text-palette-purple-main font-extrabold`}
                                                    >
                                                        Current
                                              </h4>{" "}
                                                    <h5 className={`font-light`}>8 Investors </h5>
                                                </div>
                                            </div>
                                            <div className={`w-1/3 flex justify-end`}>
                                                <div>
                                                    <h4
                                                        className={`text-palette-purple-main font-extrabold`}
                                                    >
                                                        To Go
                                              </h4>{" "}
                                                    <h5 className={`font-light`}>8 Investors </h5>
                                                </div>
                                            </div>
                                        </div>

                                        <div className={`w-full px-2`}>
                                            <button
                                                className={`bg-palette-violet-main-dark lg:py-2 lg:rounded py-3 rounded-sm text-white w-full`}
                                            >
                                                Explore
                                            </button>
                                        </div>
                                        <div className={`pt-3 pb-4 px-5 hidden`}>
                                            <div
                                                className={`flex items-center justify-between border-b pb-2 mb-2`}
                                            >
                                                <span
                                                    className={`text-base font-semibold text-gray-900`}
                                                >
                                                    <CurrencyFormat
                                                        value={
                                                            project && discountedPriceRange(project, false)
                                                        }
                                                        displayType={`text`}
                                                        thousandSeparator={true}
                                                        prefix={`$`}
                                                    />
                                                    <span> - </span>
                                                    <CurrencyFormat
                                                        value={
                                                            project && discountedPriceRange(project, true)
                                                        }
                                                        displayType={`text`}
                                                        thousandSeparator={true}
                                                        prefix={`$`}
                                                    />
                                                </span>
                                                <span
                                                    className={`
                                                        text-red-600
                                                        text-base font-semibold
                                                        ${project.properties.filter(
                                                        property => property.is_secured === false
                                                    ).length < 2
                                                            ? "text-red-700"
                                                            : ""
                                                        }
                                                    `}
                                                >
                                                    {project.properties.filter(
                                                        property => property.is_secured === false
                                                    ).length !== 0
                                                        ? project.properties.filter(
                                                            property => property.is_secured === false
                                                        ).length
                                                        : ""}

                                                    {project.properties.filter(
                                                        property => property.is_secured === false
                                                    ).length > 1
                                                        ? " Available"
                                                        : project.properties.filter(
                                                            property => property.is_secured === false
                                                        ).length === 0
                                                            ? "Sold out"
                                                            : " Remaining"}
                                                </span>
                                            </div>
                                            <div
                                                className={`flex items-center justify-between text-gray-900 font-black`}
                                            >
                                                <span className={`text-base`}>{project.deal_type}</span>
                                                <div className={`text-base flex`}>
                                                    <span
                                                        className={`cursor-pointer`}
                                                        onClick={() =>
                                                            userAction.setState({ showSocialMedia: true })
                                                        }
                                                    >
                                                        <svg className={`feather-icon h-5 w-5`}>
                                                            <use
                                                                xlinkHref={`/assets/svg/feather-sprite.svg#share`}
                                                            />
                                                        </svg>
                                                    </span>
                                                </div>
                                            </div>
                                        </div>
                                    </td>
                                </>
                            </tr>
                        );
                    })}
            </tbody>
        );
    };

    return (
        <section className={`lg:px-16`}>
            <div className={`lg:pb-12 lg:px-0 mx-auto px-6`}
                style={{ maxWidth: 1366 }}>
                <h4 className={`w-full text-center mt-10 text-palette-purple-main font-semibold tracking-wider text-xs lg:mb-4`}>
                    LOREM IPSUM
                </h4>
                <h1 className={`text-gray-900 text-left text-center leading-none font-bold text-4xl lg:text-5xl px-8 mb-4 lg:mb-10`}>
                    Current Popular Deals
                </h1>
                <p className={`lg:px-32 text-gray-900 xl:px-48 px-6`}>
                    We offer exclusive deals that cater to all your property needs, from building a home to securing an investment. The “Express” deals offer the best value for the money and are suited for people who are ready to purchase a property straight away. The “Premium” deals offer the opportunity to own high quality, luxurious properties at unbeatable prices.
                    See something you like? Add it to your wishlist and reach out to a GroupBuyer representative if you need support.
                </p>
            </div>
            <div className={`lg:px-20 text-white p-4 m-auto`} style={{ maxWidth: 1336 }}>
                <Table
                    query={`/api/deal?approved=true&is_featured=${true}`}
                    queryParams={`${queyParams}`}
                    toggleFetch={toggleFetch}
                    toggleFetch={toggleFetch}
                    keyword={keyword}
                    getData={setProjectData}
                    content={renderProjects()}
                    sort={filters.sortBy.value.replace(/asc_|desc_/g, "") || ""}
                    order={filters.sortBy.order || ""}
                />
            </div>
        </section>
    )
}

export default CurrentPopularDeals;
