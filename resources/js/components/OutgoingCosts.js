import React from "react";
import CurrencyFormat from "react-currency-format";

const OutgoingCosts = ({ apartment }) => {
  return (
    <div className={`mt-10 border-gray-500 border-b-2 pb-8`}>
      <span className={`text-base font-bold`}>Outgoing Costs:</span>
      <div className={`flex flex-col lg:flex-row items-center mt-4`}>
        <div className={`flex-1 font-semibold text-base text-gray-700`}>
          Strata:&nbsp;
          <CurrencyFormat
            value={apartment.strata_cost ? apartment.strata_cost : 0}
            displayType={`text`}
            thousandSeparator={true}
            prefix={`$`}
          />
          <span>{` ${apartment.strata === "Annual" ? "p/a" : "p/q"}`}</span>
        </div>

        <div className={`flex-1 font-semibold text-base text-gray-700`}>
          Water:&nbsp;
          <CurrencyFormat
            value={apartment.water_cost ? apartment.water_cost : 0}
            displayType={`text`}
            thousandSeparator={true}
            prefix={`$`}
          />
          <span>{` ${apartment.water === "Annual" ? "p/a" : "p/q"}`}</span>
        </div>

        <div className={`flex-1 font-semibold text-base text-gray-700`}>
          Council:&nbsp;
          <CurrencyFormat
            value={apartment.council_cost ? apartment.council_cost : 0}
            displayType={`text`}
            thousandSeparator={true}
            prefix={`$`}
          />
          <span>{` ${apartment.council === "Annual" ? "p/a" : "p/q"}`}</span>
        </div>
      </div>
    </div>
  );
};

export default OutgoingCosts;
