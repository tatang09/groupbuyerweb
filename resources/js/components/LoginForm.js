import React, { useState, createRef } from 'react';
import { useForm } from '../hooks/customHooks'
import { Checkbox } from "pretty-checkbox-react";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import UserGlobal from "~/states/userGlobal";
import { axios } from "~/helpers/bootstrap";
import {setToken, isLoggedIn, setUserRole} from '../services/auth';
import  sweetAlert from "sweetalert2";
const LoginForm = ({
  containerBgColor = "#4D0B98",
  containerRounded = false,
  subHeader = "BE IN THE LOOP",
  headerTop = "Create your",
  headerBottom = "account for free",
  inputBgColor = "#FFFFFF5C",
  displayCloseButton = false,
  rightPadded = true,
  textColorTheme = "light",
  handleCloseModal,
  hangleSignUpModal
}) => {
    const [userState, userAction] = UserGlobal();
    const [errors, setErrors] = useState({});
    const [loading, setLoading] = useState(false);
    const [state, formChange] = useForm({
        email: "",
        password: "",
    });

    const form = createRef();

    const handleSubmit = async e => {
        e.preventDefault();
        let errors = {};
        let formData = { ...state }
        console.log(formData)
        setLoading(true);

        try {
            let { data } = await axios.post('/api/login', formData);
     
            setToken(data.access_token);
            setLoading(false);
            setUserRole(data.user_role);

            userAction.setState({ fetch: !userState.fetch });
            userAction.setState({ userCount: userState.userCount + 1 });

            window.location.reload();
            // console.log(isLoggedIn());
            // setTimeout(() => {
            //     sweetAlert(
            //         'success',
            //         'Welcome back to GroupBuyer.',
            //     );
            // }, 2000);
        } catch (error) {
            console.log('it errors');
            let { data } = error.response;

            errors = data.errors;
            setLoading(false);
        }
        setErrors(errors || {});
    };

    return (
          <div className={`flex w-full items-center justify-center`}>
            <form
                className={`w-full`}
                ref={form}
                onSubmit={handleSubmit}>
                <div className={`relative ${rightPadded ? 'lg:pr-24' : 'lg:pr-12'} pl-12 py-8 w-full xs:flex-1 xs:px-12 ${containerRounded ? 'rounded-lg' : ''}`} style={{ backgroundColor: containerBgColor }}>
                    {displayCloseButton && (
                      <img onClick={handleCloseModal} src={"/assets/svg/close_button.svg"} className={"absolute top-0 right-0 w-6 my-6 mx-8 cursor-pointer"} />
                    )}
                    <h3 className={`mb-2 ${textColorTheme == 'light' ? 'text-white' : 'text-gray-900'} font-bold`}>{subHeader}</h3>
                    <h2 className={"text-palette-purple-main font-bold leading-none mb-4 xs:text-3xl"} style={{ fontSize: "3rem" }}>{headerTop} <br />{headerBottom}</h2>
                    <div className={"flex text-base mb-4"}>
                        <div className={"flex-1"}>
                            <p className={`${textColorTheme == 'light' ? 'text-white' : 'text-gray-900'} mb-1`}>Email</p>
                            <input className={`rounded-sm ${textColorTheme == 'light' ? 'text-white' : 'text-gray-900'} placeholder-white py-2 px-4 mr-2 w-full`} name='email' value={state.email} onChange={formChange} style={{ backgroundColor: inputBgColor }}></input>
                        </div>
                    </div>
                    <div className={"flex xs:flex-col text-base mb-4"}>
                        <div className={"flex-1 lg:pr-1 xs:mb-4"}>
                            <p className={`${textColorTheme == 'light' ? 'text-white' : 'text-gray-900'} mb-1`}>Password <i className={"font-light text-sm xs:text-xs"}></i></p>
                            <input type="password" className={`rounded-sm ${textColorTheme == 'light' ? 'text-white' : 'text-gray-900'} placeholder-white py-2 px-4 mr-2 w-full`} name='password' value={state.password} onChange={formChange} style={{ backgroundColor: inputBgColor }}></input>
                        </div>
                    </div>
                    <div className={"flex text-base items-center mt-8 mb-4 xs:text-sm"}>
                        <button className={"rounded-sm text-white text-base font-bold py-2 px-12 mr-8 xs:py-3 xs:px-6 xs:mr-4 xs:text-sm"} style={{ backgroundColor: '#E91AFC' }}>Login</button>
                        <span className={`${textColorTheme == 'light' ? 'text-white' : 'text-gray-900'} font-light`}>Dont have an account? <b onClick={hangleSignUpModal} className={"text-palette-purple-main ml-2"}><br className={'lg:hidden'} />Sign up</b></span>
                    </div>
                </div>
            </form>
          </div>
    );
}

export default LoginForm;
