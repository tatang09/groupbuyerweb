import React, { createRef, useState, useRef, useEffect } from "react";

import UserLayout from "~/layouts/users/UserLayout";
import UserGlobal from "~/states/userGlobal";
import { axios } from "~/helpers/bootstrap";
import { Button, Form, TextInput } from "~/components/_base";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import Collapse from "@material-ui/core/Collapse";
import swal from "sweetalert2";
import Select from "react-select";
import { countries } from "~/helpers/countries";
import { states } from "~/helpers/propertyHelper/propertyHelper";
import { v4 as uuidv4 } from "uuid";
import isEmpty from "lodash/isEmpty";
import { setLinkProps } from "hookrouter";
import { isMobile } from "react-device-detect";
import { sweetAlert } from "../components/_base/alerts/sweetAlert";
import { inputRegex } from "~/helpers/numberInput";

const PurchaserDetails = ({
  purchasers,
  setPurchasers,
  errors,
  loading,
  showHeader = true,
  showLink = true,
  width = `w-full`
}) => {
  let defaultPurchaser = {
    uid: uuidv4(),
    id: "",
    title: "",
    first_name: "",
    last_name: "",
    suburb: "",
    address_line_1: "",
    postcode: "",
    state: "",
    country: "",
    phone: "",
    alternate_phone: "",
    email: ""
  };

  const handleAddPeople = () => {
    if (purchasers.length == 4)
      return sweetAlert("error", "You can only add a maximum of 4 person.");

    setPurchasers([...purchasers, defaultPurchaser]);
  };

  useEffect(() => {}, [purchasers]);

  const deletePerson = async id => {
    await swal
      .fire({
        title: "Are you sure you want to delete the person?",
        text: "The person will be deleted permanently.",
        type: "warning",
        showCancelButton: true,
        confirmButtonText: "Yes!",
        cancelButtonText: "No"
      })
      .then(result => {
        if (result.value) {
          axios.delete(`/api/sales-advice/${id}`).then(res => {
            if (res.status == 200) {
              sweetAlert("success", "The person details has been deleted.");
            }
          });
        }
      });
  };

  const handleRemovePeople = person => {
    if (purchasers.length == 1 && person.id == null) return;
    if (purchasers.length == 1 && person.id) {
      deletePerson(person.id);
      setPurchasers(...[defaultPurchaser]);
    } else {
      let result = [];
      if (person.id) {
        deletePerson(person.id);
        result = purchasers.filter(p => p.id != person.id);
      } else {
        result = purchasers.filter(p => p.uid != person.uid);
      }
      setPurchasers(result);
    }
  };

  const handleChange = (index, fieldName, type, e) => {
    let arr = purchasers;
    for (let i = 0; i < arr.length; i++) {
      if (i == index && type === "select") {
        arr[i][fieldName] = e.value;
      }

      if (i == index && type === "text") {
        arr[i][fieldName] = e.target.value;
      }
    }
    setPurchasers([...arr]);
  };

  const titles = [
    { label: "Please select", value: "" },
    { value: "Mr.", label: "Mr." },
    { value: "Ms.", label: "Ms." },
    { value: "Mrs.", label: "Mrs." }
  ];

  const renderPurchasers = () => {
    return purchasers.map((data, index) => {
      return (
        <div
          className={`flex justify-center ${
            index > 0 ? "mt-4" : "mt-0"
          } mb-8 ${width} lg:pr-10`}
          key={index}
        >
          <div className={`border lg:mt-0 mt-8 px-6 rounded w-full`}>
            <div className={`flex mb-5 mt-8`}>
              <span className={`font-bold text-base`}>
                Purchaser(s) details
              </span>
              <span className={`flex-1  text-base text-right`}>
                NB: Full name/s required for contract purposes
              </span>
            </div>
            <h1 className={`font-bold pb-6 text-base`}>Person {index + 1}</h1>

            <form key={data.uid}>
              <div className={`mb-5`}>
                <div className={`mb-3 text-base flex`}>
                  <label className="mr-3 w-32 mt-2 capitalize pr-3">
                    <span className="font-bold">Title</span>
                    <span className="text-red-500"> *</span>
                  </label>
                  <Select
                    isOptionSelected
                    name={`title`}
                    classNamePrefix={`input-select`}
                    onChange={e => handleChange(index, "title", "select", e)}
                    defaultValue={
                      titles.find(t => t.value === data.title) || {
                        label: "Please select",
                        value: ""
                      }
                    }
                    className={`w-full`}
                    placeholder={`Select Title`}
                    options={titles}
                  />
                </div>
                {errors.length > 0 && errors[index].title && (
                  <span className="ml-32 text-red-500 text-xs">
                    {errors[index].title}
                  </span>
                )}
              </div>
              <div className={`mb-5`}>
                <div className={`mb-3 text-base flex`}>
                  <label className="mr-3 w-32 mt-2 capitalize pr-3">
                    <span className="font-bold">First Name</span>
                    <span className="text-red-500"> *</span>
                  </label>
                  <TextInput
                    type={`text`}
                    step={`any`}
                    className={`w-full rounded`}
                    name={`first_name`}
                    autoComplete={`off`}
                    placeholder={`Enter First Name`}
                    border={false}
                    onChange={e => handleChange(index, "first_name", "text", e)}
                    appearance={true}
                    defaultValue={data.first_name || ""}
                  />
                </div>
                {errors.length > 0 && errors[index].first_name && (
                  <span className="ml-32 text-red-500 text-xs">
                    {errors[index].first_name}
                  </span>
                )}
              </div>
              <div className={`mb-5`}>
                <div className={`mb-3 text-base flex`}>
                  <label className="mr-3 w-32 mt-2 capitalize pr-3">
                    <span className="font-bold">Last Name</span>
                    <span className="text-red-500"> *</span>
                  </label>
                  <TextInput
                    type={`text`}
                    step={`any`}
                    className={`w-full rounded`}
                    name={`last_name`}
                    autoComplete={`off`}
                    placeholder={`Enter Last Name`}
                    border={false}
                    appearance={true}
                    onChange={e => handleChange(index, "last_name", "text", e)}
                    defaultValue={data.last_name || ""}
                  />
                </div>
                {errors.length > 0 && errors[index].last_name && (
                  <span className="ml-32 text-red-500 text-xs">
                    {errors[index].last_name}
                  </span>
                )}
              </div>

              <div className={`mb-5`}>
                <div className={`mb-3 text-base flex`}>
                  <label className="mr-3 w-32 mt-2 capitalize pr-3">
                    <span className="font-bold">Address</span>
                    <span className="text-red-500"> *</span>
                  </label>
                  <TextInput
                    type={`text`}
                    step={`any`}
                    className={`w-full rounded`}
                    name={`address_line_1`}
                    autoComplete={`off`}
                    placeholder={`Enter Address Line 1`}
                    border={false}
                    appearance={true}
                    onChange={e =>
                      handleChange(index, "address_line_1", "text", e)
                    }
                    defaultValue={data.address_line_1 || ""}
                  />
                </div>
                {errors.length > 0 && errors[index].address_line_1 && (
                  <span className="ml-32 text-red-500 text-xs">
                    {errors[index].address_line_1}
                  </span>
                )}
              </div>

              <div className={`mb-5`}>
                <div className={`mb-3 text-base flex`}>
                  <label className="mr-3 w-32 mt-2 capitalize pr-3">
                    <span className="font-bold">Suburb</span>
                    <span className="text-red-500"> *</span>
                  </label>
                  <TextInput
                    type={`text`}
                    step={`any`}
                    className={`w-full rounded`}
                    name={`suburb`}
                    autoComplete={`off`}
                    placeholder={`Enter Suburb`}
                    border={false}
                    appearance={true}
                    onChange={e => handleChange(index, "suburb", "text", e)}
                    defaultValue={data.suburb || ""}
                  />
                </div>
                {errors.length > 0 && errors[index].suburb && (
                  <span className="ml-32 text-red-500 text-xs">
                    {errors[index].suburb}
                  </span>
                )}
              </div>

              <div className={`mb-5`}>
                <div className={`mb-3 text-base flex`}>
                  <label className="mr-3 w-32 mt-2 capitalize pr-3">
                    <span className="font-bold">Postcode</span>
                    <span className="text-red-500"> *</span>
                  </label>
                  <TextInput
                    type={`text`}
                    step={`any`}
                    className={`w-full rounded`}
                    name={`postcode`}
                    autoComplete={`off`}
                    placeholder={`Enter Postcode`}
                    border={false}
                    appearance={true}
                    onChange={e => handleChange(index, "postcode", "text", e)}
                    defaultValue={data.postcode || ""}
                  />
                </div>
                {errors.length > 0 && errors[index].postcode && (
                  <span className="ml-32 text-red-500 text-xs">
                    {errors[index].postcode}
                  </span>
                )}
              </div>

              <div className={`mb-5`}>
                <div className={`mb-3 text-base flex`}>
                  <label className="mr-3 w-32 mt-2 capitalize pr-3">
                    <span className="font-bold">State</span>
                    <span className="text-red-500"> *</span>
                  </label>
                  <Select
                    isOptionSelected
                    name={`state`}
                    classNamePrefix={`input-select`}
                    onChange={e => handleChange(index, "state", "select", e)}
                    defaultValue={
                      states.find(t => t.value === data.state) || {
                        label: "Please select",
                        value: ""
                      }
                    }
                    className={`w-full`}
                    placeholder={`Select State`}
                    options={states}
                  />
                </div>
                {errors.length > 0 && errors[index].state && (
                  <span className="ml-32 text-red-500 text-xs">
                    {errors[index].state}
                  </span>
                )}
              </div>

              <div className={`mb-5`}>
                <div className={`mb-3 text-base flex`}>
                  <label className="mr-3 w-32 mt-2 capitalize pr-3">
                    <span className="font-bold">Country</span>
                    <span className="text-red-500"> *</span>
                  </label>
                  <Select
                    isOptionSelected
                    name={`country`}
                    classNamePrefix={`input-select`}
                    onChange={e => handleChange(index, "country", "select", e)}
                    defaultValue={
                      countries.find(c => c.value === data.country) || {
                        label: "Please select",
                        value: ""
                      }
                    }
                    className={`w-full`}
                    placeholder={`Select Country`}
                    options={countries}
                  />
                </div>
                {errors.length > 0 && errors[index].country && (
                  <span className="ml-32 text-red-500 text-xs">
                    {errors[index].country}
                  </span>
                )}
              </div>

              <div className={`mb-5`}>
                <div className={`mb-3 text-base flex`}>
                  <label className="mr-3 w-32 mt-2 capitalize pr-3">
                    <span className="font-bold">Phone</span>
                    <span className="text-red-500"> *</span>
                  </label>
                  <div
                    className={`ml-2 px-4 bg-gray-200 flex items-center justify-center rounded-l border-l border-t border-b border-gray-400`}
                  >
                    +61
                  </div>
                  <TextInput
                    type={`number`}
                    step={`any`}
                    className={`w-full rounded border-l-0 rounded-l-none`}
                    // name={`phone`}
                    // value={data && data.phone}
                    defaultValue={data.phone || ""}
                    autoComplete={`off`}
                    placeholder={`Enter Phone`}
                    border={false}
                    appearance={true}
                    maxLength={9}
                    minLength={9}
                    onChange={e => handleChange(index, "phone", "text", e)}
                    onKeyDown={evt =>
                      (evt.key === "e" || evt.key === "." || evt.key === "-") &&
                      evt.preventDefault()
                    }
                  />
                </div>
                {errors.length > 0 && errors[index].phone && (
                  <span className="ml-32 text-red-500 text-xs">
                    {errors[index].phone}
                  </span>
                )}
              </div>

              <div
                className={`mb-5`}
              >
                <div className={`mb-3 text-base flex`}>
                  <label className="mr-3 w-32 mt-2 capitalize pr-3">
                    <span className="font-bold">Alternate</span>
                  </label>
                  <div
                    className={`ml-2 px-4 bg-gray-200 flex items-center justify-center rounded-l border-l border-t border-b border-gray-400`}
                  >
                    +61
                  </div>
                  <TextInput
                    type={`number`}
                    step={`any`}
                    className={`w-full rounded border-l-0 rounded-l-none`}
                    name={`alternate_phone`}
                    autoComplete={`off`}
                    placeholder={`Enter Alternate Phone`}
                    border={false}
                    appearance={true}
                    maxLength={9}
                    minLength={9}
                    onChange={e =>
                      handleChange(index, "alternate_phone", "text", e)
                    }
                    defaultValue={data.alternate_phone}
                    onKeyDown={evt =>
                      (evt.key === "e" || evt.key === "." || evt.key === "-") &&
                      evt.preventDefault()
                    }
                  />
                </div>
                {errors.length > 0 && errors[index].alternate_phone && (
                  <span className="ml-32 text-red-500 text-xs">
                    {errors[index].alternate_phone}
                  </span>
                )}
              </div>

              <div className={`mb-5`}>
                <div className={`mb-3 text-base flex`}>
                  <label className="mr-3 w-32 mt-2 capitalize pr-3">
                    <span className="font-bold">Email</span>
                    <span className="text-red-500"> *</span>
                  </label>
                  <TextInput
                    type={`text`}
                    step={`any`}
                    className={`w-full rounded`}
                    name={`email`}
                    autoComplete={`off`}
                    placeholder={`Enter Email`}
                    border={false}
                    appearance={true}
                    onChange={e => handleChange(index, "email", "text", e)}
                    defaultValue={data.email || ""}
                  />
                </div>
                {errors.length > 0 && errors[index].email && (
                  <span className="ml-32 text-red-500 text-xs">
                    {errors[index].email}
                  </span>
                )}
              </div>
            </form>

            <div className={`flex ml-20 mb-6 justify-end`}>
              {purchasers.length > 1 && (
                <Button
                  className={`bg-red-600 button button-primary font-bold mr-2 rounded-full h-10`}
                  disabled={loading}
                  onClick={() => handleRemovePeople(data)}
                >
                  Remove
                </Button>
              )}
              <Button
                className={`font-bold rounded-full bg-blue-400 h-10`}
                disabled={loading}
                onClick={() => handleAddPeople()}
              >
                Add
              </Button>
            </div>
          </div>
        </div>
      );
    });
  };

  return (
    <section className={`relative mt-6 rounded`}>
      <div className={`mx-auto`} style={{ maxWidth: 1366 }}>
        {showHeader && (
          <h1 className={`font-bold leading-tight pt-10 text-3xl mb-4`}>
            Sales Advice
          </h1>
        )}
        {showLink ? (
          <p className={`text-base`}>
            Please complete the below sales advice information.
            <br /> You can complete this later under
            <a
              className={`hover:text-palette-blue text-palette-blue-light`}
              href={`/profile/secured-deals`}
            >
              <span className="font-bold">&nbsp;'Secured deals'.</span>
            </a>
          </p>
        ) : (
          <p className={`text-base`}>
            Please complete the below sales advice information.
          </p>
        )}
        {renderPurchasers()}
      </div>
    </section>
  );
};

export default PurchaserDetails;
