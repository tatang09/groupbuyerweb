import React from 'react';

const ProgressBarDouble = ({ progress = 100 }) => {
    let width = progress.toString() + "%"
  
    return (
        <div className="relative flex h-8 overflow-hidden text-xs rounded-full bg-palette-violet-main-dark">
            <div style={{ width: width }} className="flex flex-col justify-center text-center text-white shadow-none bg-palette-purple-main whitespace-nowrap">
                <div className={"flex justify-start w-full"}>
                </div>
            </div>
            <div className="absolute top-0 bottom-0 left-0 right-0 z-10 flex items-center justify-center">
                <div className={"flex justify-between w-full px-4"}>
                    <div className={"font-bold uppercase leading-none text-base mt-1 text-white"}>
                        {progress}%
                    </div>
                    <div className={"font-bold uppercase leading-none text-base mt-1 text-white"}>
                        {100 - progress}%
                    </div>
                </div>
            </div>
        </div>
    )
}

export default ProgressBarDouble;