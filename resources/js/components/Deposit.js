import React, { useState, useEffect } from "react";

const Deposit = ({ deposit }) => {
  return (
    <div
      className={`flex h-8 items-center justify-center mb-2 mb-6 ml-1 mt-6 px-2 rounded-lg shadow-lg xs:ml-0 mt-2 xs:w-2/3`}
      style={{
        backgroundColor: "rgba(104,136,155,0.2)"
      }}
    >
      
      <span className="text-sm text-center font-bold text-black ">
        {`${deposit}% deposit required`}
      </span>
    </div>
  );
};

export default Deposit;
