import React, { useState, createRef } from 'react';
import { useForm } from '../hooks/customHooks'
import { Radio } from "pretty-checkbox-react";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

const PleaseContactForm = ({
  containerBgColor = "#4D0B98",
  containerRounded = false,
  subHeader = "BE IN THE LOOP",
  headerTop = "Create your",
  headerBottom = "account for free",
  inputBgColor = "#FFFFFF5C",
  displayCloseButton = false,
  rightPadded = true,
  // checkboxClass = "landing_checkbox",
  textColorTheme = "light",
  handleCloseModal,
  handleLoginModal
}) => {

    const [errors, setErrors] = useState({});
    const [loading, setLoading] = useState(false);
    const [state, formChange] = useForm({
        first_name: "",
        last_name: "",
        email: "",
        phone: "",
        comapny: "",
    });

    const form = createRef();

    const handleSubmit = async e => {
        e.preventDefault();
        let errors = {};
        let formData = { ...state }

        setLoading(true);

        try {
            let { data } = await axios.post('/api/register', formData);

            setToken(data.access_token);

            setLoading(false);

            userAction.setState({ fetch: !userState.fetch });

            userAction.setState({ userCount: userState.userCount + 1 });

            handleClose();

            setTimeout(() => {
                sweetAlert(
                    'success',
                    'Thank you for registering and welcome to GroupBuyer.',
                );
            }, 2000);
        } catch (error) {
            let { data } = error.response;

            errors = data.errors;
            setLoading(false);
        }
        setErrors(errors || {});
    };

    return (
          <div className={`flex w-full items-center justify-center`}>
            <form
                className={`w-full`}
                ref={form}
                onSubmit={handleSubmit}>
                <div className={`relative ${rightPadded ? 'lg:pr-24' : 'lg:pr-12'} pl-12 py-8 w-full xs:flex-1 xs:px-12 ${containerRounded ? 'rounded-lg' : ''}`} style={{ backgroundColor: containerBgColor }}>
                    {displayCloseButton && (
                      <img onClick={handleCloseModal} src={"/assets/svg/close_button.svg"} className={"absolute top-0 right-0 w-6 my-6 mx-8 cursor-pointer"} />
                    )}
                    <h3 className={`mb-2 ${textColorTheme == 'light' ? 'text-white' : 'text-gray-900'} font-bold`}>{subHeader}</h3>
                    <h2 className={"text-palette-purple-main font-bold leading-none mb-4 xs:text-3xl"} style={{ fontSize: "3rem" }}>{headerTop} <br />{headerBottom}</h2>
                    <div className={"flex xs:flex-col text-base mt-2 mb-4"}>
                        <div className={"flex-1 lg:pr-1 xs:mb-4"}>
                            <p className={`${textColorTheme == 'light' ? 'text-white' : 'text-gray-900'} mb-1`}>First Name</p>
                            <input className={`rounded-sm ${textColorTheme == 'light' ? 'text-white' : 'text-gray-900'} placeholder-white py-2 px-4 mr-2 w-full`}
                                name='first_name'
                                value={state.first_name}
                                onChange={formChange}
                                style={{ backgroundColor: inputBgColor }}></input>
                        </div>
                        <div className={"flex-1 lg:pl-1"}>
                            <p className={`${textColorTheme == 'light' ? 'text-white' : 'text-gray-900'} mb-1`}>Last Name</p>
                            <input className={`rounded-sm ${textColorTheme == 'light' ? 'text-white' : 'text-gray-900'} placeholder-white py-2 px-4 mr-2 w-full`} name='last_name' value={state.last_name} onChange={formChange} style={{ backgroundColor: inputBgColor }}></input>
                        </div>
                    </div>
                    <div className={"flex text-base mb-4"}>
                        <div className={"flex-1"}>
                            <p className={`${textColorTheme == 'light' ? 'text-white' : 'text-gray-900'} mb-1`}>Email</p>
                            <input className={`rounded-sm ${textColorTheme == 'light' ? 'text-white' : 'text-gray-900'} placeholder-white py-2 px-4 mr-2 w-full`} name='email' value={state.email} onChange={formChange} style={{ backgroundColor: inputBgColor }}></input>
                        </div>
                    </div>
                    <div className={"flex xs:flex-col text-base mb-4"}>
                        <div className={"flex-1 lg:pr-1 xs:mb-4"}>
                            <p className={`${textColorTheme == 'light' ? 'text-white' : 'text-gray-900'} mb-1`}>Phone Number <i className={"font-light text-sm xs:text-xs"}>5 or more characters</i></p>
                            <input className={`rounded-sm ${textColorTheme == 'light' ? 'text-white' : 'text-gray-900'} placeholder-white py-2 px-4 mr-2 w-full`} name='phone' value={state.phone} onChange={formChange} style={{ backgroundColor: inputBgColor }}></input>
                        </div>
                        <div className={"flex-1 lg:pr-1 xs:mb-4"}>
                            <p className={`${textColorTheme == 'light' ? 'text-white' : 'text-gray-900'} mb-1`}>Company Name <i className={"font-light text-sm xs:text-xs"}>5 or more characters</i></p>
                            <input className={`rounded-sm ${textColorTheme == 'light' ? 'text-white' : 'text-gray-900'} placeholder-white py-2 px-4 mr-2 w-full`} name='company' value={state.company} onChange={formChange} style={{ backgroundColor: inputBgColor }}></input>
                        </div>
                    </div>
                    <div className={"flex text-sm mb-4"}>
                        <div className={"flex-1"}>
                            <Radio
                                className={`text-base ${textColorTheme == 'light' ? 'text-white landing_checkbox' : 'text-gray-900 landing_checkbox_dark'} font-light lg:mb-0 mb-8 xs:text-xs xs:mb-3`}
                                shape={`curve`}
                                color={`default`}
                                icon={<FontAwesomeIcon icon={['fas', 'check']} size={`xs`} className={"text-palette-purple-main"} style={{ padding: "0.15rem" }} />}
                                inputProps={{
                                    name: `landing_checkbox`
                                }}
                            >
                                <i className={"ml-2"}>I'm a Property Developer</i>
                            </Radio>
                        </div>
                        <div className={"flex-1"}>
                            <Radio
                                className={`text-base ${textColorTheme == 'light' ? 'text-white landing_checkbox' : 'text-gray-900 landing_checkbox_dark'} font-light lg:mb-0 mb-8 xs:text-xs xs:mb-3`}
                                shape={`curve`}
                                color={`default`}
                                icon={<FontAwesomeIcon icon={['fas', 'check']} size={`xs`} className={"text-palette-purple-main"} style={{ padding: "0.15rem" }} />}
                                inputProps={{
                                    name: `landing_checkbox`
                                }}
                            >
                                <i className={"ml-2"}>I'm a Real Estate Agent</i>
                            </Radio>
                        </div>
                    </div>
                    <div className={"flex text-base items-center mt-8 mb-4 xs:text-sm"}>
                        <button className={"rounded-sm text-white text-base font-bold py-2 px-12 mr-8 xs:py-3 xs:px-6 xs:mr-4 xs:text-sm"} style={{ backgroundColor: '#E91AFC' }}>Get Started</button>
                        <span className={`${textColorTheme == 'light' ? 'text-white' : 'text-gray-900'} font-light`}>Already have an account? <b onClick={handleLoginModal} className={"text-palette-purple-main ml-2"}><br className={'lg:hidden'}/>Log in</b></span>
                    </div>
                </div>
            </form>
          </div>
    );
}

export default PleaseContactForm;
