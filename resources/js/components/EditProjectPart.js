import React, { useState, useEffect, useRef } from "react";
import Dropdown from "react-dropdown";
import "../helpers/styles/dropdown.css";
import moment from "moment";
import { sweetAlert } from "../components/_base/alerts/sweetAlert";

import { isMobile } from "react-device-detect";
import { sanitizeFilename } from "../helpers/fileHelper/fileHelper";
import { userRole } from "~/services/auth";

import { TextInput, Tooltip, Button, RichTextEditor } from "~/components/_base";
import { EditorState } from "draft-js";
import DatePicker from "react-datepicker";
import "../helpers/styles/datepicker.css";
import { convertToHTML, convertFromHTML } from "draft-convert";
import {
  states,
  property_types
} from "../helpers/propertyHelper/propertyHelper";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faTimes, faCamera } from "@fortawesome/free-solid-svg-icons";
import { faCalendarCheck } from "@fortawesome/free-regular-svg-icons";
import UserGlobal from "../states/userGlobal";
import { faSpinner } from "@fortawesome/free-solid-svg-icons";

import { developers } from "../data/index";
import isEmpty from "lodash/isEmpty";

import { saveUpdateDeal } from "../data/index";
import { getMapCoordinates } from "../helpers/mapCoordinateHelper";

const EditProjectPart = ({ toggleFetch }) => {
  const [userState, userActions] = UserGlobal();

  const [currUser, setCurrUser] = useState(userState.user);

  const [editorState, setEditorState] = useState(() =>
    EditorState.createEmpty()
  );

  const [editorStateBackup, setEditorStateBackup] = useState(() =>
    EditorState.createEmpty()
  );

  const [loading, setLoading] = useState(false);

  const data = userState.projectData.expires_at
    ? JSON.parse(userState.projectData.expires_at)
    : null;

  const [proposed_settlement, setDate] = useState(
    userState.projectData.proposed_settlement
      ? new Date(
          moment(userState.projectData.proposed_settlement).format("YYYY-MM-DD")
        )
      : new Date(Date.now("YYYY-MM-DD"))
  );

  const [default_proposed_settlement, setDefaultDate] = useState(
    userState.projectData.proposed_settlement
      ? new Date(
          moment(userState.projectData.proposed_settlement).format("YYYY-MM-DD")
        )
      : new Date(Date.now("YYYY-MM-DD"))
  );

  const [timeLimitOptionValue, setTimeLimitOptionValue] = useState(
    data && data.option ? parseInt(data.option) : 0
  );

  const [timeLimitValue, setTimeLimitValue] = useState(
    data && data.optionVal ? parseInt(data.optionVal) : 0
  );

  const [featuredImages, setFeaturedImages] = useState([
    ...userState.projectData.featured_images.map(image => {
      return {
        image: image,
        preview: image
      };
    })
  ]);

  const [fmPlaceHolder, setFmPlaceHolder] = useState([]);

  const [developerValue, setDeveloperValue] = useState("");
  const [developerSelectValue, setDeveloperSelectValue] = useState("");

  const [projectState, setProjectState] = useState(
    userState.projectData.address.state
  );

  const [displaySuiteState, setDisplaySuiteState] = useState(
    !isEmpty(userState.projectData.display_suite_address) &&
      userState.projectData.display_suite_address.state
      ? userState.projectData.display_suite_address.state
      : ""
  );

  const [propertyType, setPropertyType] = useState(
    String(userState.projectData.sub_property_id)
  );

  const [errors, setErrors] = useState([]);

  const [desc, setDesc] = useState(userState.projectData.description);
  const [projectCountry, setProjectCountry] = useState("Australia");
  const [displaySuiteCountry, setDisplaySuiteCountry] = useState("Australia");
  const addProjectForm = useRef(null);

  const countries = [{ label: "Australia", value: "Australia" }];
  const [project, setProject] = useState(userState.projectData);
  const [sameAddress, setSame] = useState(
    userState.projectData.is_address_same == 1 ? true : false
  );
  const [limitError, setLimitError] = useState(false);

  const [projectDevelopers, setProjectDevelopers] = useState([]);

  const [deposit, setDeposit] = useState(
    userState.projectData.deposit
      ? userState.projectData.deposit.toString()
      : null
  );

  const [completedNow, setCompletedNow] = useState(
    userState.projectData.is_completed
  );
  const [isProjectLand, setIsProjectLand] = useState(
    userState.projectData && userState.projectData.sub_property_id == 4
  );

  useEffect(() => {
    let phs = [];
    for (let index = 0; index < 12 - featuredImages.length; index++) {
      const ph = {
        classes:
          "cursor-pointer m-2 inline-flex bg-transparent text-palette-gray"
      };
      phs.push(ph);
    }

    setFmPlaceHolder(phs);
  }, [featuredImages.length]);

  useEffect(() => {
    const url = `/api/user`;
    developers(url)
      .then(result => {
        setProjectDevelopers([
          ...result.data.map(developer => {
            return {
              label: `${developer.first_name} ${developer.last_name}`,
              value: String(developer.id)
            };
          })
        ]);
      })
      .catch(error => {
        setLoading(false);
        setErrors(error.response.data.errors);
      });
  }, []);

  useEffect(() => {
    setEditorState(() =>
      EditorState.push(
        editorState,
        convertFromHTML(userState.projectData.description)
      )
    );

    setEditorStateBackup(() =>
      EditorState.push(
        editorStateBackup,
        convertFromHTML(userState.projectData.description)
      )
    );
  }, []);

  useEffect(() => {
    let currentProjectDeveloper = projectDevelopers.filter((val, i) => {
      return parseInt(val.value) === userState.projectData.user_id;
    });

    setDeveloperValue(userState.projectData.user_id);
    setDeveloperSelectValue(currentProjectDeveloper[0]);
  }, [projectDevelopers]);

  const getDealCoordinates = async ({ formData, url }) => {
    const address = formData.get("project_address_line_1");
    const suburb = formData.get("project_address_suburb");
    const addressParam = address
      .concat(" ", suburb)
      .concat(" ", projectState)
      .concat(" ", projectCountry);

    let data = await getMapCoordinates(addressParam);

    formData.append("long", data.long);
    formData.append("lat", data.lat);

    if (!sameAddress) {
      const address = formData.get("display_suite_address_line_1");
      const suburb = formData.get("display_suite_address_suburb");
      const addressParam = address
        .concat(" ", suburb)
        .concat(" ", displaySuiteState)
        .concat(" ", displaySuiteCountry);

      let data = await getMapCoordinates(addressParam);

      formData.append("display_suite_long", data.long);
      formData.append("display_suite_lat", data.lat);
    }
  };

  const saveDeal = async ({ formData, url }) => {
    await getDealCoordinates({ formData });
    await saveUpdateDeal({ formData, url })
      .then(r => {
        if (r.status === 200) {
          setLoading(false);
          sweetAlert("success", "Project successfully updated!");
          setDefaultDate(proposed_settlement);
          toggleFetch();
        }
      })
      .catch(error => {
        if (error.response.data.errors) {
          sweetAlert("error", "Some fields are missing");
        }
        setLoading(false);
        setErrors(error.response.data.errors);
      })
      .then(() => {});
  };

  const saveProject = () => {
    setLoading(true);

    let formData = new FormData(addProjectForm.current);

    let newDate = new Date(
      moment(userState.projectData.created_at).format("YYYY-MM-DD")
    );

    formData.append("role", "project_developer");
    formData.append("propertyType", propertyType);
    formData.append("project_address_state", projectState);
    formData.append("project_address_country", projectCountry);
    formData.append("display_suite_address_state", displaySuiteState);
    formData.append("display_suite_address_country", displaySuiteCountry);
    formData.append("deposit", deposit);
    formData.append("developer", developerValue || currUser.id);
    formData.append("time_limit[optionVal]", timeLimitValue || 0);
    formData.append("time_limit[option]", timeLimitOptionValue || 0);
    formData.append("time_limit[createdAt]", newDate);
    formData.append("same_address", sameAddress);
    formData.append("isCompleted", completedNow);
    formData.append("proposed_settlement", proposed_settlement);
    formData.append("isPropertyTypeLand", isProjectLand);

    formData.append(
      "description",
      editorState.getCurrentContent().getPlainText()
    );
    formData.append(
      "descriptionHTML",
      convertToHTML(editorState.getCurrentContent())
    );

    featuredImages.forEach((image, key) => {
      formData.append(`featuredImages[${key}]`, image.image);
    });

    formData.append("_method", "PATCH");
    let url = `/api/deal/${userState.projectData.id}`;
    saveDeal({ formData, url });
  };

  const handleSubmit = e => {
    e.preventDefault();
    setLimitError(false);
    saveProject();
  };

  const handleDrop = e => {
    e.preventDefault();
    e.stopPropagation();

    const fmCount = featuredImages.length;
    const onCount = e.dataTransfer.files.length;

    if (fmCount + onCount > 12) {
      e.target.value = null;
      setLimitError(true);
      return;
    } else {
      setLimitError(false);
    }

    setFeaturedImages(
      featuredImages.concat([
        ...[...e.dataTransfer.files].map(file => {
          let newFilename = sanitizeFilename(file.name);
          let newFileObj = new File([file], newFilename);
          return {
            image: newFileObj,
            preview: URL.createObjectURL(newFileObj)
          };
        })
      ])
    );
    e.target.value = null;
  };

  const handleChange = e => {
    const fmCount = featuredImages.length;
    const onCount = e.target.files.length;

    if (fmCount + onCount > 12) {
      setLimitError(true);
      e.target.value = null;
      return;
    } else {
      setLimitError(false);
    }

    setFeaturedImages(
      featuredImages.concat([
        ...[...e.target.files].map(file => {
          let newFilename = sanitizeFilename(file.name);
          let newFileObj = new File([file], newFilename);
          return {
            image: newFileObj,
            preview: URL.createObjectURL(newFileObj)
          };
        })
      ])
    );
    e.target.value = null;
  };

  const handleRemove = (e, i) => {
    e.preventDefault();
    e.stopPropagation();
    setFeaturedImages([
      ...featuredImages.filter((image, k) => {
        if (i !== k) {
          return image;
        }
      })
    ]);
    if (featuredImages.length > 12) {
      setLimitError(true);
    } else {
      setLimitError(false);
    }
  };

  const compareDate = selectedDate => {
    let date = moment(selectedDate, "DD-MM-YYYY"); //Date format
    let defaultDate = moment(default_proposed_settlement, "DD-MM-YYYY");
    let durationDays = date.diff(defaultDate, "days");
    if (durationDays < 0) {
      return false;
    }
    return true;
  };

  return (
    <div
      className={`bg-white rounded-lg py-5 px-8 ${isMobile ? "my-5" : "m-5"}`}
    >
      <form ref={addProjectForm} onSubmit={handleSubmit}>
        <div className={`text-black`}>
          {userRole() === "admin" && (
            <div
              className={`flex ${isMobile ? "flex flex-col" : "items-center"}`}
            >
              <label className={`block py-3 font-semibold w-40`}>
                Project Developer
              </label>
              <div className={`relative`}>
                {!isEmpty(projectDevelopers) ? (
                  <Dropdown
                    className={`${
                      isMobile
                        ? "w-full pointer-events-none cursor-not-allowed"
                        : "pointer-events-none cursor-not-allowed"
                    }`}
                    id="project_developer"
                    value={developerSelectValue}
                    options={projectDevelopers}
                    onChange={option => {
                      setDeveloperValue(option.value);
                    }}
                    placeholder="Please select..."
                    name="project_developer"
                  />
                ) : (
                  <FontAwesomeIcon
                    icon={faSpinner}
                    className={`fa-spin mr-2`}
                  />
                )}
              </div>
              {errors["developer"] && (
                <span className="absolute px-1 text-red-400 tracking-widest text-xs ">
                  {errors["developer"][0]}
                </span>
              )}
            </div>
          )}

          <div
            className={`py-3 flex  ${
              isMobile ? "flex flex-col" : "items-center"
            } `}
          >
            <label
              className={`font-semibold ${isMobile ? "py-3" : "w-1/5"}`}
              htmlFor={`name`}
            >
              Project Name:
            </label>
            <div className="flex-1 relative">
              <TextInput
                id={`name`}
                defaultValue={userState.projectData.name}
                className={`border-gray-400 border py-1 text-sm pl-2`}
                border={false}
                name="name"
              />
              {errors["name"] && (
                <span className="absolute px-1 text-red-400 tracking-widest  text-xs ">
                  {errors["name"][0]}
                </span>
              )}
            </div>
          </div>

          <div className={`py-3`}>
            <label>
              <div className="w-full text-sm bg-white text-black ">
                {editorStateBackup.getCurrentContent().getPlainText() && (
                  <RichTextEditor
                    title="Project Description"
                    richEditorState={editorState}
                    richEditorStateContent={content => setEditorState(content)}
                    characterLimit={2000}
                  ></RichTextEditor>
                )}
              </div>
            </label>
            {errors["description"] && (
              <span className="px-1 text-red-400 tracking-widest  text-xs ">
                {errors["description"][0]}
              </span>
            )}
          </div>

          <div className={`flex justify-between`}>
            <div className={`flex flex-start items-center`}>
              <label className={`block py-3 w-32`}>Project Type</label>
              <div className={`relative`}>
                <Dropdown
                  id="property_type"
                  value={propertyType}
                  options={property_types}
                  onChange={option => {
                    if (option.label === "Land") {
                      setIsProjectLand(true);
                    } else {
                      setIsProjectLand(false);
                    }
                    setPropertyType(option.value);
                  }}
                  placeholder="Please select..."
                  name="property_type"
                />

                {errors["propertyType"] && (
                  <span className="absolute px-1 text-red-400 tracking-widest  text-xs ">
                    {errors["propertyType"][0]}
                  </span>
                )}
              </div>
            </div>

            <div className={`px-10 flex flex-start items-center `}>
              <label className={`block py-3 pr-10`}>Deposit</label>
              <div className={`relative flex items-center w-48`}>
                <Dropdown
                  id="deposit"
                  value={deposit}
                  options={[
                    { label: "5", value: "5" },
                    { label: "10", value: "10" }
                  ]}
                  onChange={option => {
                    setDeposit(option.value);
                  }}
                  placeholder="Please select..."
                  name="deposit"
                />
                <span className={`font-extrabold pl-3`}> % </span>
                {errors["deposit"] && (
                  <span className="absolute px-1 text-red-400 tracking-widest  text-xs ">
                    {errors["deposit"][0]}
                  </span>
                )}
              </div>
            </div>
          </div>

          <h1 className={`font-semibold mt-5`}>Project Address</h1>
          <div className="w-full">
            <div
              className={`py-3 flex w-full ${
                isMobile ? "flex-col" : "items-center"
              }`}
            >
              <label className={`${isMobile ? "w-full pb-3" : "w-32"}`}>
                Address Line 1
              </label>
              <div
                className={`flex-1 relative ${isMobile ? "w-full" : "w-32"}`}
              >
                <TextInput
                  defaultValue={userState.projectData.address.line_1}
                  className={` border-gray-400 border py-1 pl-2`}
                  border={false}
                  name="project_address_line_1"
                />
                {errors["project_address_line_1"] && (
                  <span className="absolute px-1 text-red-400 tracking-widest  text-xs ">
                    {errors["project_address_line_1"][0]}
                  </span>
                )}
              </div>
            </div>

            <div
              className={`flex justify-between ${
                isMobile ? "flex-col" : "py-3"
              }`}
            >
              <div className={`flex ${isMobile ? "flex-col" : "items-center"}`}>
                <label className={`${isMobile ? "w-full" : "w-32"} block py-3`}>
                  Suburb{" "}
                </label>
                <div
                  style={{ width: "320px" }}
                  className={`flex-1 relative ${isMobile ? "w-full" : "w-40"}`}
                >
                  <TextInput
                    className={` border-gray-400 border py-1 pl-2`}
                    border={false}
                    name="project_address_suburb"
                    defaultValue={userState.projectData.address.suburb}
                  />
                  {errors["project_address_suburb"] && (
                    <span className="absolute px-1 text-red-400 tracking-widest  text-xs ">
                      {errors["project_address_suburb"][0]}
                    </span>
                  )}
                </div>
              </div>

              {!isProjectLand && (
                <div
                  className={`py-3 flex ${
                    isMobile ? "flex-col" : "ml-5 items-center "
                  }`}
                >
                  <label
                    className={`block py-3 ${isMobile ? "w-full" : "w-32"}`}
                  >
                    City{" "}
                  </label>
                  <div className={`relative ${isMobile ? "w-full" : ""}`}>
                    <TextInput
                      className={` border-gray-400 border pl-2 py-1`}
                      border={false}
                      name="project_address_city"
                      defaultValue={userState.projectData.address.city}
                    />
                    {errors["project_address_city"] && (
                      <span className="absolute px-1 text-red-400 tracking-widest  text-xs ">
                        {errors["project_address_city"][0]}
                      </span>
                    )}
                  </div>
                </div>
              )}
            </div>

            <div
              className={`flex justify-between ${
                isMobile ? "flex-col" : "py-3"
              }`}
            >
              <div className={`flex ${isMobile ? "flex-col" : "items-center"}`}>
                <label className={`${isMobile ? "w-full" : "w-32"} block py-3`}>
                  State{" "}
                </label>
                <div className={`relative`}>
                  <Dropdown
                    className={`${isMobile ? "w-full" : ""}`}
                    id="project_address_state"
                    value={projectState}
                    options={states}
                    onChange={option => {
                      setProjectState(option.value);
                    }}
                    placeholder="Select state"
                    name="project_address_state"
                  />
                  {errors["project_address_state"] && (
                    <span className="absolute px-1 text-red-400 tracking-widest  text-xs ">
                      {errors["project_address_state"][0]}
                    </span>
                  )}
                </div>
              </div>

              {!isProjectLand && (
                <div
                  className={`flex items-center ${
                    isMobile ? "flex-col py-3" : "ml-5"
                  }`}
                >
                  <label
                    className={`block py-3 ${isMobile ? "w-full" : "w-32"}`}
                  >
                    Postcode/ZIP{" "}
                  </label>
                  <div className={`relative ${isMobile ? "w-full" : ""}`}>
                    <TextInput
                      className={` border-gray-400 border pl-2 py-1`}
                      border={false}
                      name="project_address_zip"
                      defaultValue={userState.projectData.address.postal}
                    />
                    {errors["project_address_zip"] && (
                      <span className="absolute px-1 text-red-400 tracking-widest  text-xs ">
                        {errors["project_address_zip"][0]}
                      </span>
                    )}
                  </div>
                </div>
              )}
            </div>

            <div className={` flex ${isMobile ? "flex-col" : "py-3"}`}>
              <label className={`${isMobile ? "w-full" : "w-32"} block py-3`}>
                Country{" "}
              </label>
              <div className={`relative`}>
                <Dropdown
                  className={`${isMobile ? "w-full" : ""}`}
                  id="project_address_country"
                  value={projectCountry}
                  options={countries}
                  onChange={option => {
                    setProjectCountry(option.value);
                  }}
                  name="project_address_country"
                />

                {errors["project_address_country"] && (
                  <span className="absolute px-1 text-red-400 tracking-widest  text-xs ">
                    {errors["project_address_country"][0]}
                  </span>
                )}
              </div>
            </div>
          </div>
          <div
            className={`flex justify-between mt-5 ${
              isMobile ? "flex-col" : ""
            }`}
          >
            <h1 className={`font-semibold`}>
              {isProjectLand ? "Office" : "Display Suite"} Address
            </h1>
            <div
              className={`flex p-1 items-center ${
                isMobile ? "" : "justify-between"
              } `}
            >
              <input
                type="checkbox"
                name="same_address_check"
                id="same_address"
                onChange={e => {
                  setSame(e.target.checked);
                }}
                checked={sameAddress}
              />
              <span className={`p-1`}>
                {isProjectLand ? "Office" : "Display Suite"} Address is same as
                Project Address
              </span>
            </div>
          </div>
          {!sameAddress && (
            <div>
              <div
                className={`py-3 flex w-full ${
                  isMobile ? "flex-col" : "items-center"
                }`}
              >
                <label className={`${isMobile ? "w-full pb-3" : "w-32"}`}>
                  Address Line 1
                </label>
                <div
                  className={`flex-1 relative ${isMobile ? "w-full" : "w-32"}`}
                >
                  <TextInput
                    defaultValue={
                      userState.projectData.display_suite_address.line_1
                    }
                    className={` border-gray-400 border py-1 flex-1 pl-2`}
                    border={false}
                    name="display_suite_address_line_1"
                  />
                  {errors["display_suite_address_line_1"] && (
                    <span className="absolute px-1 text-red-400 tracking-widest  text-xs ">
                      {errors["display_suite_address_line_1"][0]}
                    </span>
                  )}
                </div>
              </div>

              <div
                className={`flex justify-between ${
                  isMobile ? "flex-col" : "py-3"
                }`}
              >
                <div
                  className={`flex ${isMobile ? "flex-col" : "items-center"}`}
                >
                  <label
                    className={`${isMobile ? "w-full" : "w-32"} block py-3`}
                  >
                    Suburb{" "}
                  </label>
                  <div
                    style={{ width: "320px" }}
                    className={`flex-1 relative ${
                      isMobile ? "w-full" : "w-40"
                    }`}
                  >
                    <TextInput
                      className={` border-gray-400 border py-1 pl-2`}
                      border={false}
                      name="display_suite_address_suburb"
                      defaultValue={
                        userState.projectData.display_suite_address.suburb
                      }
                    />
                    {errors["display_suite_address_suburb"] && (
                      <span className="absolute px-1 text-red-400 tracking-widest  text-xs ">
                        {errors["display_suite_address_suburb"][0]}
                      </span>
                    )}
                  </div>
                </div>

                {!isProjectLand && (
                  <div
                    className={`py-3 flex ${
                      isMobile ? "flex-col" : "ml-5 items-center"
                    }`}
                  >
                    <label
                      className={`block py-3 ${isMobile ? "w-full" : "w-32"}`}
                    >
                      City{" "}
                    </label>
                    <div className={`relative ${isMobile ? "w-full" : ""}`}>
                      <TextInput
                        className={` border-gray-400 border pl-2 py-1`}
                        border={false}
                        name="display_suite_address_city"
                        defaultValue={
                          userState.projectData.display_suite_address.city
                        }
                      />
                      {errors["display_suite_address_city"] && (
                        <span className="absolute px-1 text-red-400 tracking-widest  text-xs ">
                          {errors["display_suite_address_city"][0]}
                        </span>
                      )}
                    </div>
                  </div>
                )}
              </div>

              {/* <div className={`py-3 flex justify-between`}>
                <div className={`py-3 flex items-center`}>
                  <label className={`w-32 block py-3`}>State </label> */}
              <div
                className={`flex justify-between ${
                  isMobile ? "flex-col" : "py-3"
                }`}
              >
                <div
                  className={`flex ${isMobile ? "flex-col" : "items-center"}`}
                >
                  <label
                    className={`${isMobile ? "w-full" : "w-32"} block py-3`}
                  >
                    State{" "}
                  </label>
                  <div className={`relative`}>
                    <Dropdown
                      className={`${isMobile ? "w-full" : ""}`}
                      id="display_suite_address_state"
                      value={displaySuiteState}
                      options={states}
                      onChange={option => setDisplaySuiteState(option.value)}
                      placeholder="Select state"
                      name="display_suite_address_state"
                    />
                    {errors["display_suite_address_state"] && (
                      <span className="absolute px-1 text-red-400 tracking-widest  text-xs ">
                        {errors["display_suite_address_state"][0]}
                      </span>
                    )}
                  </div>
                </div>
                {/* <div className={`ml-5 flex items-center`}>
                  <label className={`w-32 block py-3`}>Postcode/ZIP </label>
                  <div className={`relative`}> */}

                {!isProjectLand && (
                  <div
                    className={`flex ${
                      isMobile ? "flex-col py-3" : "ml-5 items-center"
                    }`}
                  >
                    <label
                      className={`block py-3 ${isMobile ? "w-full" : "w-32"}`}
                    >
                      Postcode/ZIP{" "}
                    </label>
                    <div className={`relative ${isMobile ? "w-full" : ""}`}>
                      <TextInput
                        className={` border-gray-400 border pl-2 py-1`}
                        border={false}
                        name="display_suite_address_zip"
                        defaultValue={
                          userState.projectData.display_suite_address.postal
                        }
                      />
                      {errors["display_suite_address_zip"] && (
                        <span className="absolute px-1 text-red-400 tracking-widest  text-xs ">
                          {errors["display_suite_address_zip"][0]}
                        </span>
                      )}
                    </div>
                  </div>
                )}
              </div>
              {/* <div className={`py-3 flex items-center`}>
                <label className={`w-32 block py-3`}>Country </label> */}
              <div className={` flex ${isMobile ? "flex-col" : "py-3"}`}>
                <label className={`${isMobile ? "w-full" : "w-32"} block py-3`}>
                  Country{" "}
                </label>
                <div className={`relative`}>
                  <Dropdown
                    className={`${isMobile ? "w-full" : ""}`}
                    id="display_suite_address_country"
                    value={displaySuiteCountry}
                    options={countries}
                    onChange={option => {
                      setDisplaySuiteCountry(option.value);
                    }}
                    name="display_suite_address_country"
                  />

                  {errors["display_suite_address_country"] && (
                    <span className="absolute px-1 text-red-400 tracking-widest  text-xs ">
                      {errors["display_suite_address_country"][0]}
                    </span>
                  )}
                </div>
              </div>
            </div>
          )}

          <div className={` ${isMobile ? "py-8" : "py-3"}`}>
            <label className={`cursor-pointer`}>
              <h1 className="font-semibold mb-4">
                Estimated Completion/
                {isProjectLand ? "Land Registration" : "Settlement"}
              </h1>
              <FontAwesomeIcon className={`mr-2`} icon={faCalendarCheck} />
              <DatePicker
                className={`px-2 py-1`}
                id={`proposed_settlement`}
                // name={`proposed_settlement`}
                showMonthDropdown={true}
                showYearDropdown={true}
                dateFormat={`PP`}
                selected={proposed_settlement}
                disabled={completedNow}
                onSelect={date => {
                  if (!compareDate(date)) {
                    sweetAlert("error", "Please select future date!");
                  } else {
                    setDate(date);
                  }
                }}
                onChange={date => {
                  if (!compareDate(date)) {
                    sweetAlert("error", "Please select future date!");
                  } else {
                    setDate(date);
                  }
                }}
              />
            </label>
            <div className="p-1 flex items-center mt-3">
              <input
                type="checkbox"
                id="completed_now"
                checked={completedNow}
                onChange={e => {
                  setCompletedNow(e.target.checked);
                }}
              />
              <span className={`p-1`}>Completed Now</span>
            </div>
          </div>

          <div className={`py-3`}>
            <h1 className="font-semibold"> Featured Images (min. 3 images) </h1>
            <label className="button " htmlFor="upload-photos">
              <div className={``}>
                <div
                  // cursor-pointer flex flex-col flex-wrap h-full overflow-auto w-full
                  className="w-full flex flex-wrap  cursor-pointer"
                  style={{
                    border: "2px dashed #ccc",
                    borderRadius: "10px",
                    padding: "20px",
                    textAlign: "center",
                    marginBottom: "0px"
                  }}
                  onDrop={e => handleDrop(e)}
                  onDragOver={e => {
                    e.preventDefault();
                    e.stopPropagation();
                  }}
                  onDragEnter={e => {
                    e.preventDefault();
                    e.stopPropagation();
                  }}
                  onDragLeave={e => {
                    e.preventDefault();
                    e.stopPropagation();
                  }}
                >
                  <input
                    type="file"
                    id="upload-photos"
                    accept="image/*"
                    multiple
                    className="hidden"
                    name="image[]"
                    onChange={e => handleChange(e)}
                  />

                  {featuredImages.map((image, index) => {
                    return (
                      <div
                        key={index}
                        className="rounded-sm border m-2 relative"
                        style={{
                          backgroundImage: `url('${image.preview}')`,
                          backgroundPositionX: "center",
                          width: "6.9em",
                          height: "8.9rem",
                          backgroundSize: "cover"
                        }}
                      >
                        <div
                          className={`absolute bg-palette-blue-light font-bold leading-none ml-1 mt-1 rounded-full text-white w-1/4`}
                        >
                          {index + 1}
                        </div>
                        <Tooltip title={`Remove this item`}>
                          <span
                            onClick={e => handleRemove(e, index)}
                            className={`absolute bottom-0 right-0 p-2 -mb-2 `}
                          >
                            <FontAwesomeIcon
                              className={`text-red-500 shadow-xl hover:font-bold hover:text-red-700`}
                              icon={faTimes}
                            />
                          </span>
                        </Tooltip>
                      </div>
                    );
                  })}
                  {fmPlaceHolder.map((p, index) => {
                    return (
                      <div
                        key={index}
                        className={`${p.classes} cursor-pointer flex justify-center items-center`}
                        style={{
                          width: "6.9rem",
                          height: "8.9rem",
                          border: "2px solid #ccc"
                        }}
                      >
                        <div
                          className={`absolute font-bold leading-none w-1/4`}
                        >
                          {index + featuredImages.length + 1}
                        </div>
                      </div>
                    );
                  })}
                </div>
                {limitError && (
                  <span className="px-1 text-red-400 tracking-widest  text-xs ">
                    Please limit your photos to twelve(12) items only.
                  </span>
                )}
              </div>
            </label>
            {errors["featuredImages"] && (
              <span className="px-1 text-red-400 tracking-widest  text-xs ">
                {errors["featuredImages"][0]}
              </span>
            )}
          </div>

          <div className="flex justify-end my-10">
            <Button className={`font-bold rounded-full`} disabled={loading}>
              {loading && (
                <FontAwesomeIcon icon={faSpinner} className={`fa-spin mr-2`} />
              )}
              Continue
            </Button>
          </div>
        </div>
      </form>
    </div>
  );
};

export default EditProjectPart;
