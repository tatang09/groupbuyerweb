import React, { createRef, useState, useEffect } from "react";
import { Modal, Button, Tooltip } from "~/components/_base";
import isEmpty from "lodash/isEmpty";
import { isLoggedIn } from "~/services/auth";
import { sweetAlert } from "../../components/_base/alerts/sweetAlert";

const PaymentConfirmationModal = ({
  show,
  handleClose,
  data,
  formData,
  project,
  property
}) => {
  useEffect(() => {
    if (!isEmpty(data)) {
      loadIFrame();
    }
  }, [data]);

  const iframeNode = $("#secure3d-frame");

  const [simplifyDomain, setSimplifyDomain] = useState(
    "https://www.simplify.com"
  );

  const loadIFrame = () => {
    var newSecure3dForm = create3DSecureForm(data.data.card); // Step 2

    $(newSecure3dForm).insertAfter(iframeNode);
    iframeNode.removeClass("hidden");

    var process3dSecureCallback = function(threeDsResponse) {
      window.removeEventListener("message", process3dSecureCallback);
 
      if(threeDsResponse.origin === simplifyDomain
        && JSON.parse(threeDsResponse.data)['secure3d']['authenticated']){
        securedDealPayment();
      } else {
        handleClose();
      }
    };

    iframeNode.on("load", function() {
      window.addEventListener("message", process3dSecureCallback); // Step 3
    });

    newSecure3dForm.submit();
  };

  const securedDealPayment = async () => {
    formData.append("token", data.data.id);
    return await axios.post("/api/secure-deal", formData, {
      headers: {
        Authorization: "Bearer " + isLoggedIn()
      }
    }).then(res => {
      
      if(res.status == 200 && res.data.statusCode == 400){
        let msg = res.data.declineReason.replace(/_/g, " ")
        sweetAlert("error", msg);
        iframeNode.hide();
        handleClose();
      } 

      if(res.status == 200 && res.data.statusCode == 200 && res.data.status === "APPROVED"){
        window.location = `/weekly-deals/${project.id}/apartment/${
          property.id
        }/${false}/confirm`;
      }      
    });
  };

  const create3DSecureForm = data => {
    var secure3dData = data["secure3DData"];
    var secure3dForm = document.createElement("form");
    secure3dForm.setAttribute("method", "POST");
    secure3dForm.setAttribute("action", secure3dData.acsUrl);
    secure3dForm.setAttribute("target", "secure3d-frame");

    var merchantDetails = secure3dData.md;
    var paReq = secure3dData.paReq;
    var termUrl = secure3dData.termUrl;

    secure3dForm.append(createInput("PaReq", paReq));
    secure3dForm.append(createInput("TermUrl", termUrl));
    secure3dForm.append(createInput("MD", merchantDetails));

    return secure3dForm;
  };

  const createInput = (name, value) => {
    var input = document.createElement("input");
    input.setAttribute("type", "hidden");
    input.setAttribute("name", name);
    input.setAttribute("value", value);
    return input;
  };

  return (
    <Modal show={show} title={``} maxWidth={`sm`} onClose={() => handleClose()}>
      <div className="bg-gray-200 flex flex-col px-4 lg:px-8 py-4 rounded-lg mb-6">
        <div className="flex flex-col">
          <div className={`flex justify-center item-center`}>
            <h3 className="font-bold text-2xl lg:text-4xl">
              Payment Authentication
            </h3>
          </div>
          <div>
            <iframe
              name="secure3d-frame"
              id="secure3d-frame"
              className="hidden w-full"
              style={{ height: 500 }}
            ></iframe>
          </div>
        </div>
      </div>
    </Modal>
  );
};

export default PaymentConfirmationModal;
