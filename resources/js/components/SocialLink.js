import React from 'react';
import RegistrationForm from './RegistrationForm';
const messenger = "/assets/svg/messenger.svg";
const SocialLink = () => {
    return (
        <div style={{zIndex: '101',position: 'fixed', bottom: -60, right: -60}}>
            <img src={messenger}/>
          </div>
    );
}


export default SocialLink;
