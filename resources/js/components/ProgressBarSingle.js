import React from 'react';

const ProgressBarSingle = ({ progress = 100 }) => {
    let width = progress.toString() + "%"
  
    return (
        <div className="relative flex overflow-hidden text-xs bg-gray-300 rounded-full lg:h-8 xs:h-6">
            <div style={{ width: width }} className="flex flex-col justify-center text-center text-white rounded-full shadow-none bg-palette-purple-main whitespace-nowrap">
                <div className={"flex justify-start w-full"}>
                </div>
            </div>
            <div className="absolute top-0 bottom-0 left-0 right-0 z-10 flex items-center justify-center">
                <div className={"flex justify-start w-full px-4"}>
                    <div className={"font-bold uppercase leading-none mt-1 lg:text-base xs:text-xs text-white"}>
                        {progress}%
                    </div>
                </div>
            </div>
        </div>
    )
}

export default ProgressBarSingle;