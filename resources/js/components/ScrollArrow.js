import React, {useState} from 'react';

const ScrollArrow = ({className, top, icon}) => {
  const [showScroll, setShowScroll] = useState(false);

  const checkScrollDirection = () => {
    if (top) {
      if (!showScroll && window.pageYOffset > 400) {
        setShowScroll(true);
      } else if (showScroll && window.pageYOffset <= 400) {
        setShowScroll(false);
      }
    } else {
      if (!showScroll && window.pageYOffset < 400) {
        setShowScroll(true);
      } else if (showScroll && window.pageYOffset >= 400) {
        setShowScroll(false);
      }
    }
  };

  const scrollDirection = () => {
    if (top) {
      window.scrollTo({
        top: 0,
        behavior: 'smooth',
      });
    } else {
      window.scrollTo({
        left: 0,
        top:
          document.documentElement.scrollHeight -
          document.documentElement.clientHeight,
        behavior: 'smooth',
      });
    }
  };

  window.addEventListener('scroll', checkScrollDirection);

  return (
    <div className={className} onClick={scrollDirection}>
      <svg className={`feather-icon h-6 text-white w-6`}>
        <use xlinkHref={`/assets/svg/feather-sprite.svg#${icon}`} />
      </svg>
    </div>
  );
};

export default ScrollArrow;
