import React from 'react';

const NewGoogleMaps = ({}) => {
    return <section>
      <div className={"block w-full"}>
        <img src="/assets/images/sample_map.jpg" className={"w-full"} />
      </div>
    </section>  
}

export default NewGoogleMaps;