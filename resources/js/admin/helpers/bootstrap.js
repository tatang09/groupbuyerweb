import Cookie from "js-cookie";

let token = Cookie.get("oToken_admin");

if (token) {
  axios.interceptors.request.use(
    config => {
      config.headers.common["Authorization"] = `Bearer ${token}`;
      return config;
    },
    error => {
      return Promise.reject(error);
    }
  );

  axios.interceptors.response.use(
    function(response) {
      return response;
    },
    function(error) {
      if (401 === error.response.status) {
        logout();
      } else {
        return Promise.reject(error);
      }
    }
  );
} else {
  window.location = "/admin/login";
}

const logout = async () => {
  Cookie.set("oToken_admin", "");
  await axios.post("/api/logout");
  window.location = "/admin/login";
};
