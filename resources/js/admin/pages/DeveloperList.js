import React, { useState, useEffect, createRef } from "react";
import { debounce } from "lodash";

import UserGlobal from "~/states/userGlobal";

import { isMobile } from "react-device-detect";
import { SaveUpdateModal } from "~/components";
import AddDeveloperModal from "../components/AddDeveloperModal";
import UpdateDeveloperModal from "../components/UpdateDeveloperModal";
import { Table, TextInput, Tooltip, Button } from "../../components/_base";
import {isEmpty} from 'lodash';

const DeveloperList = () => {
  const [userState, userAction] = UserGlobal();
  const [filters, setFilters] = useState({
    sortBy: { value: "developers.created_at", order: "desc" }
  });
  const [toggleFetch, setToggleFetch] = useState(false);
  const [keyword, setKeyword] = useState("");
  const [developers, setDevelopers] = useState([]);
  const [developer, setSelectedDeveloper] = useState({});
  const [loading, setLoading] = useState(true);
  const [toggleOrder, setToggleOrder] = useState(false);
  const [toogleAddDeveloper, setToogleAddDeveloper] = useState(false);
  const [toogleUpdateDeveloper, setToogleUpdateDeveloper] = useState(false);
  const [title, setTitle] = useState("Add Developer");
  const [isAdd, setIsAdd] = useState(false);

  const tableHeaders = [
    {
      label: "Company Name",
      column: "developers.name",
      width: 230
    },
    {
      label: "Contact",
      // column: "developers.name",
      width: 230
    },
    {
      label: "Phone",
      //column: "developers.phone",
      width: 150
    },
    {
      label: "Email",
      column: "developers.email",
      width: 150
    },
    {
      label: "Edit",
      width: 50
    }
  ];

  const handleSaveUpdateDeveloper = async developer => {
    setLoading(true);
  };

  const onClose = () => {
    userAction.setState({ showSaveUpdateModal: false });
  };

  const fetchDevelopers = () => {
    setToggleFetch(!toggleFetch);
  };

  const renderDeveloperList = () => {
    return (
      <>
        <tbody>
          {developers &&
            developers.map((developer, index) => {
            
              return !isMobile ? (
                <tr
                  key={index}
                  className={`hover:bg-gray-100 border-b text-base`}
                >
                  <td className="p-4">
                    <div className={`flex items-center`}>
                      <img
                        src={
                          !isEmpty(developer.developer) && developer.developer.company_logo_path
                            ? developer.developer.company_logo_path
                            : "/assets/images/profile_default.jpg"
                        }
                        className={`w-12 h-12 mr-3`}
                      />
                      <div>
                        <div className={`font-bold text-palette-purple`}>
                          {`${!isEmpty(developer.developer) && developer.developer.name ? developer.developer.name : ""}`}
                        </div>
                      </div>
                    </div>
                  </td>
                  <td className="p-4">{`${developer.first_name} ${developer.last_name}`}</td>
                  <td className="p-4">{`${
                    developer.phone
                      ? developer.phone
                      : developer.developer && developer.developer.phone
                      ? developer.developer.phone
                      : ""
                  }`}</td>
                  <td className="p-4 text-palette-blue-light">
                    {developer.email ? developer.email : ""}
                  </td>
                  <td className={`p-4`}>
                    <Tooltip title={`Edit Developer`}>
                      <button
                        onClick={() => {
                          setTitle("Edit Developer");
                          setIsAdd(false);
                          toogleUpdateDeveloperModal();
                          setSelectedDeveloper(developer);
                        }}
                        className={`cursor-pointer bg-white border border-gray-400 duration-300 hover:bg-palette-purple hover:border-palette-purple hover:text-white inline-flex px-3 py-1 rounded transition-all`}
                      >
                        <svg className={`feather-icon h-5 w-5 opacity-50`}>
                          <use
                            xlinkHref={`/assets/svg/feather-sprite.svg#edit`}
                          />
                        </svg>
                      </button>
                    </Tooltip>
                  </td>
                </tr>
              ) : (
                <tr key={index} className={`flex flex-col`}>
                  <td
                    className={`p-4 rounded`}
                    style={{ boxShadow: "0 5px 28px 4px rgba(0,0,0,0.1)" }}
                  >
                    <div className={`flex items-center justify-between`}>
                      <div className={`flex items-center`}>
                        <img
                          src={`/storage/projects/${developer.developer.property_images[0]}`}
                          className={`rounded-full w-12 h-12 mr-3`}
                        />
                        <div>
                          <div
                            className={`font-bold text-base text-palette-purple`}
                          >
                            {developer.developer.developer_name}
                          </div>
                          <div className={`opacity-50 text-sm`}>
                            {"Apartment " + developer.property_unit_name}
                          </div>
                        </div>
                      </div>
                      <div
                        onClick={() => handleSaveUpdateDeveloper(developer)}
                        className={`cursor-pointer bg-white border border-gray-400 duration-300 hover:bg-palette-purple hover:border-palette-purple hover:text-white inline-flex px-3 py-1 rounded transition-all`}
                      >
                        <svg className={`feather-icon h-5 w-5 opacity-50`}>
                          <use
                            xlinkHref={`/assets/svg/feather-sprite.svg#edit`}
                          />
                        </svg>
                      </div>
                      <a
                        href={`/weekly-deals/${developer.developer_id}/apartment/${developer.property_id}`}
                      >
                        <div
                          className={`cursor-pointer bg-white border border-gray-400 duration-300 hover:bg-palette-purple hover:border-palette-purple hover:text-white inline-flex px-3 py-1 rounded transition-all`}
                        >
                          <svg className={`feather-icon h-5 w-5 opacity-50`}>
                            <use
                              xlinkHref={`/assets/svg/feather-sprite.svg#eye`}
                            />
                          </svg>
                        </div>
                      </a>
                    </div>
                    <hr className="block border border-gray-200 my-2" />
                    <div></div>
                    <div
                      className={`flex items-center justify-between mt-2`}
                    ></div>
                  </td>
                  <td className={`p-3`}></td>
                </tr>
              );
            })}
        </tbody>
      </>
    );
  };

  const formatFilter = column => {
    if (column) {
      setToggleOrder(!toggleOrder);

      setFilters({
        sortBy: { value: column, order: toggleOrder ? "desc" : "asc" }
      });

      setToggleFetch(!toggleFetch);
    }
  };

  const renderHeaders = () => {
    return (
      <thead>
        <tr>
          {tableHeaders.map((th, index) => {
            return (
              <th
                onClick={() => formatFilter(th.column)}
                style={{ width: th.width }}
                className={`border-b font-bold 
                px-4 py-2 text-palette-gray 
                px-4 py-2 cursor-pointer 
                hover:text-palette-gray-dark `}
                key={index}
              >
                {th.label}
              </th>
            );
          })}
        </tr>
      </thead>
    );
  };

  const handleSearch = debounce(value => {
    setKeyword(value);
  }, 800);

  const toogleAddDeveloperModal = () => {
    setToogleAddDeveloper(!toogleAddDeveloper);
  };

  const toogleUpdateDeveloperModal = () => {
    setToogleUpdateDeveloper(!toogleUpdateDeveloper);
  };

  return (
    <div className={`border mt-10`}>
      <div className={`bg-white`}>
        {toogleAddDeveloper && (
          <AddDeveloperModal
            title={title}
            developer={isAdd ? {} : developer}
            toogleAddDeveloper={() => toogleAddDeveloperModal()}
            fetchDevelopers={() => fetchDevelopers()}
          ></AddDeveloperModal>
        )}
        {toogleUpdateDeveloper && (
          <UpdateDeveloperModal
            title={title}
            developer={isAdd ? {} : developer}
            toogleUpdateDeveloper={() => toogleUpdateDeveloperModal()}
            fetchDevelopers={() => fetchDevelopers()}
          ></UpdateDeveloperModal>
        )}
        <section>
          <section className={`relative bg-white`}>
            <div className={`pb-16 px-6 mx-auto`}>
              <h1
                className={`font-bold leading-tight text-4xl pt-10 pb-8 lg:pb-16 text-center`}
              >
                Developers
              </h1>

              <div className="flex justify-between">
                <div className={`flex justify-center lg:justify-end mb-8`}>
                  <div className={`w-64 relative`}>
                    <svg
                      className={`absolute feather-icon h-8 left-0 mx-3 text-gray-500`}
                    >
                      <use
                        xlinkHref={`/assets/svg/feather-sprite.svg#search`}
                      />
                    </svg>
                    <TextInput
                      className={`pl-8`}
                      type={`text`}
                      placeholder={`Search`}
                      border={false}
                      appearance={true}
                      onChange={e => handleSearch(e.target.value)}
                    />
                  </div>
                </div>
                <div className={` flex justify-center lg:justify-end mb-8`}>
                  <Tooltip title={`Click to add Developer`}>
                    <div className={`w-64 relative `}>
                      <svg
                        className={`absolute feather-icon pt-2 h-full left-0 ml-5 text-white pb-2`}
                      >
                        <use
                          xlinkHref={`/assets/svg/feather-sprite.svg#plus-circle`}
                        />
                      </svg>
                      <Button
                        onClick={() => {
                          setTitle("Add Developer");
                          setIsAdd(true);
                          toogleAddDeveloperModal();
                        }}
                      >
                        Add Developer
                      </Button>
                    </div>
                  </Tooltip>
                </div>
              </div>
              <Table
                query={`/api/developer`}
                toggleFetch={toggleFetch}
                keyword={keyword}
                getData={setDevelopers}
                content={renderDeveloperList()}
                header={isMobile ? null : renderHeaders()}
              />
            </div>
          </section>
        </section>
        {!loading && (
          <SaveUpdateModal
            show={userState.showSaveUpdateModal}
            onClose={() => onClose()}
            developer={selecteddeveloper}
            apartment={selecteddeveloperProperty}
            purchasersParam={purchasers}
          />
        )}
      </div>
    </div>
  );
};

export default DeveloperList;
