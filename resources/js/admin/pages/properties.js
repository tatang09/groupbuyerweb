import React, { useState, useEffect } from "react";
import {
  Table,
  TextInput,
  Tooltip,
  Modal,
  Button
} from "../../components/_base";

import EditPropertyModal from "../components/EditPropertyModal";
import AddPropertyModal from "../components/AddPropertyModal";

import EditLandPropertyModal from "../components/EditLandPropertyModal";
import AddLandPropertyModal from "../components/AddLandPropertyModal";

import CurrencyFormat from "react-currency-format";
import UserGlobal from "../../states/userGlobal";
import { debounce } from "lodash";
import { isMobile } from "react-device-detect";
import Checkbox from "@material-ui/core/Checkbox";

import FormControlLabel from "@material-ui/core/FormControlLabel";
import { withStyles } from "@material-ui/core/styles";
import { green } from "@material-ui/core/colors";

import { sweetAlert } from "../../components/_base/alerts/sweetAlert";

import { addressHelper } from "../../helpers/addressHelper";

const Properties = () => {
  
  const [keyWord, setKeyWord] = useState("");
  const [toggleFetch, setToggleFetch] = useState(false);
  const [properties, setProperties] = useState([]);
  const [filters, setFilters] = useState({
    sortBy: { value: "properties.created_at", order: "desc" }
  });
  const [toggleOrder, setToggleOrder] = useState(false);
  const [toggleEditProperty, setToggleEditProperty] = useState(false);
  const [toggleAddProperty, setToggleAddProperty] = useState(false);
  const [property, setProperty] = useState([]);
  const [currentProject, setCurrentProject] = useState([]);
  const [isProjectLand, setIsProjectLand] = useState(false);
  const [toogleLandProperty, setToogleLandProperty] = useState(false);
  const [toogleEditLandProperty, setToogleEditLandProperty] = useState(false);

  const formatFilter = column => {
    if (column) {
      setToggleOrder(!toggleOrder);

      setFilters({
        sortBy: { value: column, order: toggleOrder ? "desc" : "asc" }
      });

      setToggleFetch(!toggleFetch);
    }
  };

  useEffect(() => {
    if (!toggleAddProperty) {
      setToggleFetch(!toggleFetch);
    }
    return () => {};
  }, [toggleAddProperty]);

  useEffect(() => {
    if (!toggleEditProperty) {
      setToggleFetch(!toggleFetch);
    }
    return () => {};
  }, [toggleEditProperty]);

  const GreenCheckbox = withStyles({
    root: {
      color: green[400],
      "&$checked": {
        color: green[600]
      }
    },
    checked: {}
  })(props => <Checkbox color="default" {...props} />);

  const tableHeaders = [
    {
      label: "Id",
      column: "properties.id",
      width: 40
    },
    {
      label: "Project Name",
      column: "properties.deal_id",
      width: 100
    },
    {
      label: "Property Number",
      column: "properties.unit_name",
      width: 100
    },
    {
      label: "Property Address",
      column: "properties.address",
      width: 200
    },
    {
      label: "Developer Price",
      column: "properties.price",
      width: 150
    },
    {
      label: "Discount",
      column: "properties.price",
      width: 80
    },
    {
      label: "GroupBuyer Price",
      column: "properties.price",
      width: 150
    },
    // {
    //   label: "Floor Area",
    //   column: "properties.floor_area",
    //   width: 150
    // },
    {
      label: "Approved",
      column: "is_property_approved",
      width: 150
    },
    {
      label: "Edit",
      column: "",
      width: 40
    }
  ];

  const handleSearch = debounce(text => {
    setKeyWord(text);
  }, 800);

  const renderHeaders = () => {
    return (
      <thead>
        <tr>
          {tableHeaders.map((th, index) => {
            return (
              <th
                onClick={() => formatFilter(th.column)}
                style={{ width: th.width }}
                className={`border-b font-bold px-4 py-2 text-palette-gray px-4 py-2
                cursor-pointer hover:text-palette-gray-dark
                ${th === "Edit" ? "text-center" : "text-left"}`}
                key={index}
              >
                {th.label}
              </th>
            );
          })}
        </tr>
      </thead>
    );
  };

  const getProperty = async propId => {
    const url = `/api/property/${propId}`;
    await axios.get(url).then(res => {
      if (res.data.property_type_id == 4) {
        setIsProjectLand(true);
      } else {
        setIsProjectLand(false);
      }
      setProperty(res.data);
      showModal(res.data.property_type_id == 4);
    });
  };

  const showModal = (show) => {
    if (!show) {
      setToggleEditProperty(!toggleEditProperty);
    } else {
      setToogleEditLandProperty(!toogleEditLandProperty);
    }
  };


  const toggleEditPropertyModal = () => {
    if (!isProjectLand) {
      setToggleEditProperty(!toggleEditProperty);
    } else {
      setToogleEditLandProperty(!toogleEditLandProperty);
    }
  };

  const toggleAddPropertyModal = () => {
    setToggleAddProperty(!toggleAddProperty);
  };

  const toggleAddLandPropertyModal = () => {
    setToogleLandProperty(!toogleLandProperty);
  };


  const handleApproval = (event, propId, property) => {
    let name = `${property.deal_name} ${property.unit_name}`;
    let status = event.target.checked;
    axios({
      method: "post",
      url: "/api/approve-property",
      data: {
        value: status,
        propertyId: propId
      }
    }).then(res => {
      if (res.status === 200) {
        let msg = status
          ? `${name} is now added to the approved properties!`
          : `${name} is now removed from approved properties!`;
        sweetAlert("success", msg);
        setToggleFetch(!toggleFetch);
      }
    });
  };

  const propertiesContent = () => {
    return (
      <>
        <tbody>
          {properties.map(property => {
            return (
              <tr
                key={property.id}
                className={`hover:bg-gray-100 border-b text-base`}
              >
                <td className={`p-4`}>{property.id}</td>
                <td className={`p-4 font-bold text-palette-purple`}>
                  {property.deal_name}
                </td>
                <td className={`p-4`}>{property.unit_name}</td>
                <td className={`p-4`}>{addressHelper(property.address)}</td>
                <td className={`p-4`}>
                  <CurrencyFormat
                    value={property.price}
                    displayType={"text"}
                    thousandSeparator={true}
                    prefix={"$"}
                  />
                </td>
                <td className={`p-4`}>{`${
                  property.discount ? property.discount + "%" : "N/A"
                } `}</td>
                <td className={`p-4 font-semibold text-red-700 pl-10`}>
                  <Tooltip title={`Price discount: ${property.discount}%`}>
                    <CurrencyFormat
                      value={Math.ceil(
                        property.price -
                          property.price * (property.discount / 100)
                      )}
                      displayType={"text"}
                      thousandSeparator={true}
                      prefix={"$"}
                    />
                  </Tooltip>
                </td>
                <td className={`p-4 pl-10`}>
                  <FormControlLabel
                    control={
                      <GreenCheckbox
                        checked={property.is_property_approved ? true : false}
                        onChange={e => handleApproval(e, property.id, property)}
                        name={property.name}
                      />
                    }
                    label=""
                  />
                </td>
                <td className={`p-4`}>
                  <Tooltip title={`Edit Property`}>
                    <div
                      onClick={() => getProperty(property.id)}
                      className={`cursor-pointer bg-white border border-gray-400 duration-300 hover:bg-palette-purple hover:border-palette-purple hover:text-white inline-flex px-3 py-1 rounded transition-all`}
                    >
                      <svg className={`feather-icon h-5 w-5 opacity-50`}>
                        <use
                          xlinkHref={`/assets/svg/feather-sprite.svg#edit`}
                        />
                      </svg>
                    </div>
                  </Tooltip>
                </td>
              </tr>
            );
          })}
        </tbody>
      </>
    );
  };

  const editToggleFetch = () => {
    setToggleFetch(!toggleFetch);
  };

  const addToggleFetch = () => {
    setToggleFetch(!toggleFetch);
  };

  return (
    <div className={`border mt-10`}>
      <div className={`bg-white`}>
        {toggleEditProperty && (
          <EditPropertyModal
            projectName={currentProject.name}
            projectId={currentProject.id}
            property={property}
            toggleEditPropertyModal={() => toggleEditPropertyModal()}
            editToggleFetch={editToggleFetch}
          ></EditPropertyModal>
        )}
        {toggleAddProperty && (
          <AddPropertyModal
            projectName={currentProject.name}
            projectId={currentProject.id}
            toggleAddPropertyModal={() => toggleAddPropertyModal()}
            addToggleFetch={addToggleFetch}
          ></AddPropertyModal>
        )}
        {toogleLandProperty && (
          <AddLandPropertyModal
            projectName={currentProject.name}
            projectId={currentProject.id}
            toggleAddLandPropertyModal={() => toggleAddLandPropertyModal()}
            addToggleFetch={toggleFetch}
          ></AddLandPropertyModal>
        )}
        {toogleEditLandProperty && (
          <EditLandPropertyModal
            projectName={currentProject.name}
            projectId={currentProject.id}
            property={property}
            toggleEditPropertyModal={() => toggleEditPropertyModal()}
            editToggleFetch={toggleFetch}
          ></EditLandPropertyModal>
        )}
        <div className={`pb-16 px-6 lg:px-10 mx-auto`}>
          <h1
            className={`font-bold leading-tight text-4xl pt-10 pb-8 lg:pb-16 text-center`}
          >
            Properties
          </h1>
          <div className="flex justify-between">
            <div className={`flex justify-center lg:justify-end mb-8`}>
              <div className={`w-64 relative `}>
                <svg
                  className={`absolute feather-icon h-8 left-0 mx-3 text-gray-500`}
                >
                  <use xlinkHref={`/assets/svg/feather-sprite.svg#search`} />
                </svg>
                <TextInput
                  className={`pl-8`}
                  type={`text`}
                  placeholder={`Search`}
                  border={false}
                  appearance={true}
                  onChange={e => handleSearch(e.target.value)}
                />
              </div>
            </div>
            <div className={` flex justify-center lg:justify-end mb-8`}>
              <Tooltip title={`Click to add a Project`}>
                <div className={`mr-3 relative `}>
                  <svg
                    className={`absolute feather-icon pt-2 h-full left-0 ml-5 text-white pb-2`}
                  >
                    <use
                      xlinkHref={`/assets/svg/feather-sprite.svg#plus-circle`}
                    />
                  </svg>
                  <Button onClick={() => toggleAddPropertyModal()}>
                    Add Property
                  </Button>
                </div>
              </Tooltip>
              <Tooltip title={`Click to add a Project`}>
                <div className={`w-64 relative `}>
                  <svg
                    className={`absolute feather-icon pt-2 h-full left-0 ml-5 text-white pb-2`}
                  >
                    <use
                      xlinkHref={`/assets/svg/feather-sprite.svg#plus-circle`}
                    />
                  </svg>
                  <Button onClick={() => toggleAddLandPropertyModal()}>
                    Add Land Property
                  </Button>
                </div>
              </Tooltip>
            </div>
          </div>

        
          <Table
            query={`/api/property`}
            toggleFetch={toggleFetch}
            keyword={keyWord}
            getData={setProperties}
            content={propertiesContent()}
            sort={filters.sortBy.value.replace(/asc_|desc_/g, "") || ""}
            order={filters.sortBy.order || ""}
            header={isMobile ? null : renderHeaders()}
          />
        </div>
      </div>
    </div>
  );
};

export default Properties;
