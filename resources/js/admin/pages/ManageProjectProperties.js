import React, { useState, useEffect } from "react";
import {
  Table,
 
  Tooltip,
 
  Button
} from "../../components/_base";
import EditPropertyModal from "../components/EditPropertyModal";
import AddProperty from "../components/AddProperty";

import EditLandPropertyModal from "../components/EditLandPropertyModal";
import AddLandProperty from "../components/AddLandProperty";

 
import { isMobile } from "react-device-detect";

import CurrencyFormat from "react-currency-format";

import Checkbox from "@material-ui/core/Checkbox";
import FormControlLabel from "@material-ui/core/FormControlLabel";

import { withStyles } from "@material-ui/core/styles";
import { green } from "@material-ui/core/colors";

import { sweetAlert } from "../../components/_base/alerts/sweetAlert";
import { addressHelper } from "../../helpers/addressHelper";

const GreenCheckbox = withStyles({
  root: {
    color: green[400],
    "&$checked": {
      color: green[600]
    }
  },
  checked: {}
})(props => <Checkbox color="default" {...props} />);

window.axios = require("axios");

const ManageProjectProperties = ({ dealId, dealSubPropertyId }) => {
  const isProjectLand  = dealSubPropertyId == 4 ? true : false;
  const [keyWord, setKeyWord] = useState("");
  const [toggleFetch, setToggleFetch] = useState(false);
  const [currentProject, setCurrentProject] = useState([]);
  const [filters, setFilters] = useState({
    sortBy: { value: "deals.name", order: "desc" }
  });
  const [toggleOrder, setToggleOrder] = useState(false);
  const [toggleEditProperty, setToggleEditProperty] = useState(false);
  const [toggleAddProperty, setToggleAddProperty] = useState(false);
  const [property, setProperty] = useState([]);

  const [toogleLandProperty, setToogleLandProperty] = useState(false);
  const [toogleEditLandProperty, setToogleEditLandProperty] = useState(false);

  const formatFilter = column => {
    if (column) {
      setToggleOrder(!toggleOrder);

      setFilters({
        sortBy: { value: column, order: toggleOrder ? "desc" : "asc" }
      });

      setToggleFetch(!toggleFetch);
    }
  };

  useEffect(() => {
    if (!toggleAddProperty) {
      setToggleFetch(!toggleFetch);
    }
    return () => {};
  }, [toggleAddProperty]);

  useEffect(() => {
    if (!toggleEditProperty) {
      setToggleFetch(!toggleFetch);
    }
    return () => {};
  }, [toggleEditProperty]);

  const tableHeaders = [
    {
      label: "Id",
      column: "properties.id",
      width: 40
    },
    {
      label: "Property Number",
      column: "properties.unit_name",
      width: 150
    },
    {
      label: "Property Address",
      column: "property.address",
      width: 150
    },
    {
      label: "Developer Price",
      column: "properties.price",
      width: 150
    },
    {
      label: "Discount",
      column: "properties.price",
      width: 80
    },
    {
      label: "GroupBuyer Price",
      column: "properties.price",
      width: 150
    },
    {
      label: "Approved",
      column: "",
      column: "properties.is_property_approved",
      width: 150
    },

    {
      label: "Edit",
      column: "",
      width: 40
    }
  ];

  const renderHeaders = () => {
    return (
      <thead>
        <tr>
          {tableHeaders.map((th, index) => {
            return (
              <th
                onClick={() => formatFilter(th.column)}
                style={{ width: th.width }}
                className={`border-b font-bold px-4 py-2 text-palette-gray px-4 py-2`}
                key={index}
              >
                {th.label}
              </th>
            );
          })}
        </tr>
      </thead>
    );
  };

  const toggleEditPropertyModal = () => {
    if (!isProjectLand) {
      setToggleEditProperty(!toggleEditProperty);
    } else {
      setToogleEditLandProperty(!toogleEditLandProperty);
    }
  };

  const toggleAddPropertyModal = () => {
    if (!isProjectLand) {
      setToggleAddProperty(!toggleAddProperty);
    } else {
      setToogleLandProperty(!toogleLandProperty);
    }
  };

  const handleApproval = (event, propId, name) => {
    let status = event.target.checked;

    axios({
      method: "post",
      url: "/api/approve-property",
      data: {
        value: event.target.checked,
        propertyId: propId
      }
    }).then(res => {
      if (res.status === 200) {
        let msg = status
          ? `${name} is now added to approved properties!`
          : `${name} is now removed on the approved properties!`;
        sweetAlert("success", msg);
        setToggleFetch(!toggleFetch);
      }
    });
  };

  const propertiesContent = () => {
    return (
      <>
        <tbody>
          {currentProject.properties &&
            currentProject.properties.map(property => {
              return (
                <tr
                  key={property.id}
                  className={`hover:bg-gray-100 border-b text-base`}
                >
                  <td className={`p-4`}>{property.id}</td>
                  <td className={`p-4 font-bold text-palette-purple`}>
                    {property.unit_name}
                  </td>
               
                  <td className={`p-4`}>{addressHelper(property.address)}</td>
                  <td className={`p-4`}>
                    <CurrencyFormat
                      value={property.price}
                      displayType={"text"}
                      thousandSeparator={true}
                      prefix={"$"}
                    />
                  </td>
                  <td className={`p-4`}>{`${
                    currentProject.discount
                      ? currentProject.discount + "%"
                      : "N/A"
                  } `}</td>
                  <td className={`p-4 text-left font-semibold text-red-700`}>
                    <Tooltip
                      title={`Price discount: ${currentProject.discount}%`}
                    >
                      <CurrencyFormat
                        value={Math.ceil(
                          property.price -
                            property.price * (currentProject.discount / 100)
                        )}
                        displayType={"text"}
                        thousandSeparator={true}
                        prefix={"$"}
                      />
                    </Tooltip>
                  </td>
                  <td className={`p-4 pl-10`}>
                    <FormControlLabel
                      control={
                        <GreenCheckbox
                          checked={property.is_property_approved ? true : false}
                          onChange={e =>
                            handleApproval(e, property.id, property.unit_name)
                          }
                          name={property.name}
                        />
                      }
                      label=""
                    />
                  </td>
                  <td className={`p-4`}>
                    <Tooltip title={`Edit Property`}>
                      <button
                        onClick={() => {
                          toggleEditPropertyModal();
                          setProperty(property);
                        }}
                        className={`cursor-pointer bg-white border border-gray-400 duration-300 hover:bg-palette-purple hover:border-palette-purple hover:text-white inline-flex px-3 py-1 rounded transition-all`}
                      >
                        <svg className={`feather-icon h-5 w-5 opacity-50`}>
                          <use
                            xlinkHref={`/assets/svg/feather-sprite.svg#edit`}
                          />
                        </svg>
                      </button>
                    </Tooltip>
                  </td>
                </tr>
              );
            })}
        </tbody>
      </>
    );
  };

  const editToggleFetch = () => {
    setToggleFetch(!toggleFetch);
  };

  const addToggleFetch = () => {
    setToggleFetch(!toggleFetch);
  };

  return (
    <div className={`border mt-10`}>
      <div className={`bg-white`}>
        {toggleEditProperty && (
          <EditPropertyModal
            project={currentProject}
            projectName={currentProject.name}
            projectId={dealId}
            property={property}
            toggleEditPropertyModal={() => toggleEditPropertyModal()}
            editToggleFetch={editToggleFetch}
          ></EditPropertyModal>
        )}
        {toggleAddProperty && (
          <AddProperty
            projectName={currentProject.name}
            projectId={dealId}
            toggleAddPropertyModal={() => toggleAddPropertyModal()}
            addToggleFetch={addToggleFetch}
          ></AddProperty>
        )}
        {toogleEditLandProperty && (
          <EditLandPropertyModal
            projectName={currentProject.name}
            projectId={currentProject.id}
            property={property}
            toggleEditPropertyModal={() => toggleEditPropertyModal()}
            editToggleFetch={toggleFetch}
          ></EditLandPropertyModal>
        )}
        {toogleLandProperty && (
          <AddLandProperty
            projectName={currentProject.name}
            projectId={currentProject.id}
            toggleAddPropertyModal={() => toggleAddPropertyModal()}
            addToggleFetch={toggleFetch}
          ></AddLandProperty>
        )}
        <div
          className={`pb-16 px-6 lg:px-10 mx-auto`}
          // style={{ maxWidth: 1366 }}
        >
          <div
            className={`flex items-center flex items-center lg:pb-16 pb-10 pt-10`}
          >
            <span
              className={`bg-white cursor-pointer duration-300 font-bold hover:text-palette-blue inline-flex items-center pr-3 relative text-palette-blue-light text-sm transition-all z-10`}
              onClick={() => (window.location = "/admin/projects")}
            >
              <svg className={`feather-icon h-4 w-4 mr-1 inline`}>
                <use
                  xlinkHref={`/assets/svg/feather-sprite.svg#chevron-left`}
                />
              </svg>
              Back to Projects Page
            </span>
            <h1
              className={`flex-1 font-bold leading-tight text-4xl text-center`}
            >
              <span className={`mr-40 text-center`}>{currentProject.name}</span>
            </h1>
          </div>

          <div className="flex justify-end">
            <div className={` flex justify-center lg:justify-end mb-8`}>
              {currentProject.properties &&
              currentProject.properties.length < 5 ? (
                <Tooltip title={`Click to add a Project`}>
                  <div className={`w-64 relative `}>
                    <svg
                      className={`absolute feather-icon h-full left-0 ml-5 text-white`}
                    >
                      <use
                        xlinkHref={`/assets/svg/feather-sprite.svg#plus-circle`}
                      />
                    </svg>
                    <Button onClick={() => toggleAddPropertyModal()}>
                      Add Property
                    </Button>
                  </div>
                </Tooltip>
              ) : (
                <Tooltip
                  title={`You have reached the maximum 5 properties per project. Once all 5 properties are sold you can upload another 5 properties.`}
                >
                  <div className={`w-64 relative `}>
                    <svg
                      className={`absolute feather-icon h-full left-0 ml-5 text-gray-500`}
                    >
                      <use
                        xlinkHref={`/assets/svg/feather-sprite.svg#plus-circle`}
                      />
                    </svg>
                    <Button
                      disabled={true}
                      onClick={() => toggleAddPropertyModal()}
                    >
                      Add a Property
                    </Button>
                  </div>
                </Tooltip>
              )}
            </div>
          </div>
          <Table
            query={`/api/deal/${dealId}?paginated=true`}
            toggleFetch={toggleFetch}
            keyword={keyWord}
            getData={data => setCurrentProject(data[0])}
            content={propertiesContent()}
            sort={filters.sortBy.value.replace(/asc_|desc_/g, "") || ""}
            order={filters.sortBy.order || ""}
            header={isMobile ? null : renderHeaders()}
          />
        </div>
      </div>
    </div>
  );
};

export default ManageProjectProperties;
