import React, { useState, useEffect } from "react";

import {
  Table,
  TextInput,
  Tooltip,
  Modal,
  Button
} from "../../components/_base";

import SequenceAddingModal from "../components/SequenceAddingModal";
import SequenceEditingModal from "../components/SequenceEditingModal";

import UserGlobal from "../../states/userGlobal";

import { debounce } from "lodash";
import { isMobile } from "react-device-detect";
import moment from "moment";

import Checkbox from "@material-ui/core/Checkbox";

import FormControlLabel from "@material-ui/core/FormControlLabel";

import { withStyles } from "@material-ui/core/styles";
import { green } from "@material-ui/core/colors";

import { sweetAlert } from "../../components/_base/alerts/sweetAlert";
import { addressHelper } from "../../helpers/addressHelper";
import { isEmpty } from "lodash";

const Projects = () => {
  const [userState, userAction] = UserGlobal();
  const [keyWord, setKeyWord] = useState("");
  const [toggleFetch, setToggleFetch] = useState(false);
  const [projects, setProjects] = useState([]);
  const [filters, setFilters] = useState({
    sortBy: { value: "deals.created_at", order: "desc" }
  });
  const [toggleOrder, setToggleOrder] = useState(false);

  const [toggleSequenceAddingModal, setToggleSequenceAddingModal] = useState(
    false
  );
  const [toggleSequenceEditModal, setToggleSequenceEditModal] = useState(false);
  const [proj, setProj] = useState([]);

  const [agents, setAgents] = useState([]);

  const GreenCheckbox = withStyles({
    root: {
      color: green[400],
      "&$checked": {
        color: green[600]
      }
    },
    checked: {}
  })(props => <Checkbox color="default" {...props} />);

  useEffect(() => {
    axios.get("/api/agents").then(response => {
      setAgents(response.data);
    });
  }, []);

  useEffect(() => {
    if (!toggleSequenceAddingModal) {
      setToggleFetch(!toggleFetch);
    }
    return () => {};
  }, [toggleSequenceAddingModal]);

  useEffect(() => {
    if (!toggleSequenceEditModal) {
      setToggleFetch(!toggleFetch);
    }
    return () => {};
  }, [toggleSequenceEditModal]);

  const formatFilter = column => {
    if (column) {
      setToggleOrder(!toggleOrder);

      setFilters({
        sortBy: { value: column, order: toggleOrder ? "desc" : "asc" }
      });

      setToggleFetch(!toggleFetch);
    }
  };

  const tableHeaders = [
    {
      label: "Id",
      column: "deals.id",
      width: 40
    },
    {
      label: "Name",
      column: "deals.name",
      width: 180
    },
    {
      label: "Property Address",
      column: "deals.address",
      width: 200
    },
    {
      label: "Property Type",
      column: "deals.sub_property_id",
      width: 150
    },
    {
      label: "Agent Name",
      column: "deals.agent_name",
      width: 150
    },
    {
      label: "Proposed Settlement",
      column: "deals.proposed_settlement",
      width: 150
    },
    {
      label: "Project Time Limit",
      column: "deals.deal_time_limit",
      width: 150
    },
    {
      label: "Featured",
      column: "is_deal_approved",
      width: 80
    },
    {
      label: "Weekly",
      column: "is_weekly",
      width: 80
    },
    {
      label: "Approved",
      column: "is_featured",
      width: 80
    },
    {
      label: "Preview",
      column: "",
      width: 60
    },
    {
      label: "Edit",
      column: "",
      width: 60
    }
  ];

  const handleSearch = debounce(text => {
    setKeyWord(text);
  }, 800);

 
  const renderHeaders = () => {
    return (
      <thead>
        <tr>
          {tableHeaders.map((th, index) => {
            return (
              <th
                onClick={() => formatFilter(th.column)}
                style={{ width: th.width }}
                className={`border-b font-bold px-4 py-2 text-palette-gray px-4 py-2 cursor-pointer hover:text-palette-gray-dark `}
                key={index}
              >
                {th.label}
              </th>
            );
          })}
        </tr>
      </thead>
    );
  };

  const toggleSequenceAM = () => {
    setToggleSequenceAddingModal(!toggleSequenceAddingModal);
  };

  const toggleSequenceEM = () => {
    setToggleSequenceEditModal(!toggleSequenceEditModal);
  };

  const handleApproval = (event, pId) => {
    axios({
      method: "post",
      url: "/api/approve-deal",
      data: {
        value: event.target.checked,
        projectId: pId
      }
    }).then(res => {
      if (res.status === 200) {
        const { data } = res;
        if (!data.success) {
          sweetAlert("error", data.message);
        } else {
          setToggleFetch(!toggleFetch);
          sweetAlert("success", data.message);
        }
      }
    });
  };

  const handleFeatured = (event, pId, dealApproved, name) => {
    if (!dealApproved) {
      return sweetAlert("error", `${name} is not yet approved!`);
    }
    let status = event.target.checked;
    axios({
      method: "post",
      url: "/api/featured-deal",
      data: {
        value: status,
        id: pId,
        field: "is_featured"
      }
    }).then(res => {
      if (res.status === 200) {
        setToggleFetch(!toggleFetch);
        let msg = status
          ? `${name} is now added to the featured projects!`
          : `${name} is now removed on the featured projects!`;
        sweetAlert("success", msg);
      }
    });
  };

  const handleWeekly = (event, pId, dealApproved, name) => {
    if (!dealApproved) {
      return sweetAlert("error", `${name} is not yet approved!`);
    }
    let status = event.target.checked;
    axios({
      method: "post",
      url: "/api/weekly-deal",
      data: {
        value: status,
        id: pId,
        field: "is_weekly"
      }
    }).then(res => {
      if (res.status === 200) {
        setToggleFetch(!toggleFetch);
        let msg = status
          ? `${name} is now set as deal of the week!`
          : `${name} is now removed as deal of the week!`;
        sweetAlert("success", msg);
      }
    });
  };

  const projectsContent = () => {
    return (
      <>
        <tbody>
          {!isEmpty(projects) &&
            projects.map((project, index) => {
              let timeLimit = project.expires_at
                ? JSON.parse(project.expires_at)
                : project.created_at;
              return (
                <tr
                  key={project.id}
                  className={`hover:bg-gray-100 border-b text-base`}
                >
                  <td className={`p-4`}>{project.id}</td>
                  <td
                    onClick={() => {
                      window.location = `/admin/manage-properties/${project.id}/${project.sub_property_id}`;
                    }}
                    className={`p-4 transform font-bold text-palette-purple hover:text-palette-blue-light cursor-pointer`}
                  >
                    {project.name}
                  </td>
                  <td className={`p-4`}>{addressHelper(project.address)}</td>
                  <td className={`p-4`}>{project.deal_type}</td>
                  <td className={`p-4`}>{project.agent_name}</td>
                  <td className={`p-4`}>
                    {" "}
                    {project.sub_property_id != 4
                      ? moment(project.proposed_settlement).format("DD-MM-YYYY")
                      : moment(project.land_registration).format("DD-MM-YYYY")}
                  </td>
                  <td className={`p-4`}>
                    {" "}
                    {project.expires_at
                      ? moment(timeLimit.time_limit).format("DD-MM-YYYY")
                      : moment(project.deal_time_limit).format("DD-MM-YYYY")}
                    {/* {moment(project.deal_time_limit).format("LL")} */}
                  </td>
                  <td className={`p-4 pl-10`}>
                    <FormControlLabel
                      control={
                        <GreenCheckbox
                          checked={project.is_featured}
                          onChange={e =>
                            handleFeatured(
                              e,
                              project.id,
                              project.is_deal_approved,
                              project.name
                            )
                          }
                          name={project.name}
                        />
                      }
                      label=""
                    />
                  </td>
                  <td className={`p-4 pl-10`}>
                    <FormControlLabel
                      control={
                        <GreenCheckbox
                          checked={project.is_weekly}
                          onChange={e =>
                            handleWeekly(
                              e,
                              project.id,
                              project.is_deal_approved,
                              project.name
                            )
                          }
                          name={project.name}
                        />
                      }
                      label=""
                    />
                  </td>
                  <td className={`p-4 pl-10`}>
                    <FormControlLabel
                      control={
                        <GreenCheckbox
                          checked={project.is_deal_approved}
                          onChange={e => handleApproval(e, project.id)}
                          name={project.name}
                        />
                      }
                      label=""
                    />
                  </td>
                  <td className={`p-4`}>
                    <Tooltip title={`Preview`}>
                      <button
                        onClick={() => {
                          window.location = `/weekly-deals/${
                            project.id
                          }/${true}`;
                        }}
                        className={`cursor-pointer bg-white border border-gray-400 duration-300 hover:bg-palette-purple hover:border-palette-purple hover:text-white inline-flex px-3 py-1 rounded transition-all`}
                      >
                        <svg className={`feather-icon h-5 w-5 opacity-50`}>
                          <use
                            xlinkHref={`/assets/svg/feather-sprite.svg#eye`}
                          />
                        </svg>
                      </button>
                    </Tooltip>
                  </td>
                  <td className={`p-4`}>
                    <Tooltip title={`Edit`}>
                      <button
                        onClick={() => {
                          setProj(project);
                          toggleSequenceEM(project);
                        }}
                        className={`cursor-pointer bg-white border border-gray-400 duration-300 hover:bg-palette-purple hover:border-palette-purple hover:text-white inline-flex px-3 py-1 rounded transition-all`}
                      >
                        <svg className={`feather-icon h-5 w-5 opacity-50`}>
                          <use
                            xlinkHref={`/assets/svg/feather-sprite.svg#edit`}
                          />
                        </svg>
                      </button>
                    </Tooltip>
                  </td>
                </tr>
              );
            })}
        </tbody>
      </>
    );
  };

  return (
    <div className={`border mt-10`}>
      <div className={`bg-white`}>
        {toggleSequenceEditModal && (
          <SequenceEditingModal
            project={proj}
            editToggleFetch={() => {
              setToggleFetch(!toggleFetch);
            }}
            toggleSequenceEditModal={() => toggleSequenceEM()}
          ></SequenceEditingModal>
        )}
        {toggleSequenceAddingModal && (
          <SequenceAddingModal
            addToggleFetch={() => {
              setToggleFetch(!toggleFetch);
            }}
            toggleSequenceAddingModal={() => toggleSequenceAM()}
          ></SequenceAddingModal>
        )}
        <div className={`pb-16 px-6 lg:px-10 mx-auto`}>
          <h1
            className={`font-bold leading-tight text-4xl pt-10 pb-8 lg:pb-16 text-center`}
          >
            Projects
          </h1>
          <div className="flex justify-between">
            <div className={`flex justify-center lg:justify-end mb-8`}>
              <div className={`w-64 relative `}>
                <svg
                  className={`absolute feather-icon h-8 left-0 mx-3 text-gray-500`}
                >
                  <use xlinkHref={`/assets/svg/feather-sprite.svg#search`} />
                </svg>
                <TextInput
                  className={`pl-8`}
                  type={`text`}
                  placeholder={`Search`}
                  border={false}
                  appearance={true}
                  onChange={e => handleSearch(e.target.value)}
                />
              </div>
            </div>
            <div className={` flex justify-center lg:justify-end mb-8`}>
              <Tooltip title={`Click to add Project`}>
                <div className={`w-64 relative `}>
                  <svg
                    className={`absolute feather-icon h-full pt-2 left-0 ml-5 text-white pb-2`}
                  >
                    <use
                      xlinkHref={`/assets/svg/feather-sprite.svg#plus-circle`}
                    />
                  </svg>
                  <Button onClick={() => toggleSequenceAM()}>
                    Add Project
                  </Button>
                </div>
              </Tooltip>
            </div>
          </div>

          {/* <FormGroup row className={`items-center justify-start mb-6`}>
            <label className={`font-semibold ml-4 mr-4 text-base`}>Filters:&nbsp;</label>
            <FormControlLabel
              control={<GreenCheckbox checked={state.checkedA} onChange={handleApproval} name="checkedA" />}
              label="All"
            />
            <FormControlLabel
              control={<GreenCheckbox checked={state.checkedA} onChange={handleApproval} name="checkedA" />}
              label="Approved"
            />
            <FormControlLabel
              control={<GreenCheckbox checked={state.checkedA} onChange={handleApproval} name="checkedA" />}
              label="Featured"
            />
            <FormControlLabel
              control={<GreenCheckbox checked={state.checkedA} onChange={handleApproval} name="checkedA" />}
              label="Weekly"
            />
          </FormGroup> */}

          <Table
            query={`/api/deal`}
            toggleFetch={toggleFetch}
            keyword={keyWord}
            getData={setProjects}
            content={projectsContent()}
            sort={filters.sortBy.value.replace(/asc_|desc_/g, "") || ""}
            order={filters.sortBy.order || ""}
            header={isMobile ? null : renderHeaders()}
          />
        </div>
      </div>
    </div>
  );
};

export default Projects;
