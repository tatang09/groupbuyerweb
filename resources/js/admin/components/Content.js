import React, { Suspense, lazy } from "react";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";

import Loadable from 'react-loadable';
 
import Projects from "../pages/Projects";
import Properties from "../pages/Properties";
import ManageProjectProperties from "../pages/ManageProjectProperties";
import SecuredDealsListPage from "../../pages/SecuredDealsListPage";
import DeveloperList from "../pages/DeveloperList";

const Content = () => {
  return (
    <div className="h-full flex-1 p-8 bg-gray-200">
      <Switch>       
        <Route exact={true} path="/projects" component={() => <Projects />} />
        <Route
          exact={true}
          path="/properties"
          component={() => <Properties />}
        />
        <Route
          exact={true}
          path="/manage-properties/:dealId/:dealSubPropertyId"
          component={routeParams => (
            <ManageProjectProperties
              dealId={parseInt(routeParams.match.params.dealId)}
              dealSubPropertyId={parseInt(routeParams.match.params.dealSubPropertyId)}
            />
          )}
        />
        <Route
          exact={true}
          path="/secured-deals"
          component={routeParams => (
            <SecuredDealsListPage isAdmin={true} />
          )}
        />
        <Route
          exact={true}
          path="/developers"
          component={routeParams => (
            <DeveloperList  />
          )}
        />
      
      </Switch>
    </div>
  );
};

export default React.memo(Content);
