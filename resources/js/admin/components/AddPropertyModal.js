import React, { useState, useEffect, useRef } from "react";
import Dropdown from "react-dropdown";
import "../../helpers/styles/dropdown.css";
import {
  Table,
  TextInput,
  TextArea,
  Tooltip,
  Modal,
  Button
} from "~/components/_base";
import { faSpinner } from "@fortawesome/free-solid-svg-icons";
import {
  property_types,
  costDueDates,
  floorPlanHelper
} from "../../helpers/propertyHelper/propertyHelper";
import RichTextEditor from "../components/RichTextEditor";
import { EditorState } from "draft-js";
import DatePicker from "react-datepicker";
import "../../helpers/styles/datepicker.css";
import { convertToHTML, convertFromHTML } from "draft-convert";
import CurrencyFormat from "react-currency-format";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faTimes, faCamera } from "@fortawesome/free-solid-svg-icons";
import UserGlobal from "../../states/userGlobal";
import { isEmpty, values } from "lodash";
import { LocalHotel, Bathtub, DirectionsCar } from "@material-ui/icons";

import { sweetAlert } from "../../components/_base/alerts/sweetAlert";
import BaseAlert from "../../components/_base/alerts/BaseAlert";
import {
  sanitizeFilename,
  imageFileTypeFilter,
  PDFFileTypeFilter
} from "../../helpers/fileHelper/fileHelper";
import {resources} from "../../helpers/resourceHelper/index";

export default ({
  projectName,
  projectId,
  toggleAddPropertyModal,
  addToggleFetch
}) => {
  const [userState, userActions] = UserGlobal();
  const [featuredImages, setFeaturedImages] = useState([]);
  const [state, setState] = useState("");
  const [propertyType, setPropertyType] = useState("");
  const [subPropertyType, setSubPropertyType] = useState("");
  const [ownershipType, setOwnershipType] = useState("");
  const [errors, setErrors] = useState([]);
  const [editorState, setEditorState] = useState(() =>
    EditorState.createEmpty()
  );
  const [floorPlan, setFLoorPlan] = useState([]);
  const [filename, setFilename] = useState("");
  const [previewImage, setPreviewImage] = useState([]);
  const [prevFilename, setPrevFilename] = useState("");

  const [fmPlaceHolder, setFmPlaceHolder] = useState([]);
  const [limitError, setLimitError] = useState(false);

  const [deals, setDeals] = useState({});
  const [selectedDeal, setSelectedDeal] = useState("");

  const addPropertyForm = useRef(null);

  const [loading, setLoading] = useState(true);

  const [strata, setStrata] = useState("");
  const [water, setWater] = useState("");
  const [council, setCouncil] = useState("");
  const [dates, setDates] = useState(costDueDates);

  let previewImageRef = useRef(null);
  let floorPlanRef = useRef(null);

  useEffect(() => {
    let phs = [];
    for (let index = 0; index < 6 - featuredImages.length; index++) {
      const ph = {
        classes:
          "cursor-pointer m-2 inline-flex bg-transparent text-palette-gray"
      };
      phs.push(ph);
    }

    setFmPlaceHolder(phs);
  }, [featuredImages.length]);

  useEffect(() => {
    getDeals();
  }, []);

  const getDeals = async () => {
    const url = `${window.location.origin}/api/deal`;
    await axios({
      method: "get",
      url,
      params: {
        intent: "listings",
        isProjectLand: false
      }
    })
      .then(result => {
        let array = values(result.data);
        setLoading(false);
        setDeals([
          ...array.map(deal => {
            return {
              label: `${deal.name}`,
              value: String(deal.id)
            };
          })
        ]);
      })
      .catch(error => {});
  };

  const handlePreviewImageDisplay = e => {
    e.preventDefault();

    if (e.target.files.length > 1) {
      sweetAlert("error", "Multiple upload is not allowed!");
      return;
    }

    if (!imageFileTypeFilter(e.target.files[0]["name"])) {
      sweetAlert("error", "Filetype is not supported!");
      e.target.value = null;
      return;
    }

    setPreviewImage(
      previewImage.concat([
        ...[...e.target.files].map(file => {
          let newFilename = sanitizeFilename(file.name);
          let newFileObj = new File([file], newFilename);
          setPrevFilename(newFilename);
          return {
            image: newFileObj,
            preview: URL.createObjectURL(newFileObj)
          };
        })
      ])
    );
  };

  const handleSubmit = e => {
    e.preventDefault();
    setLimitError(false);

    setLoading(true);

    let formData = new FormData(addPropertyForm.current);
    formData.append("role", "admin");

    formData.append("project", selectedDeal);

    if (floorPlan && !isEmpty(floorPlan)) {
      formData.append("floorPlan[file]", floorPlan[0]);
    }

    formData.append("projectName", projectName);
    formData.append("projectId", selectedDeal);

    formData.append("propertyType", propertyType);
    formData.append("water", water);
    formData.append("council", council);
    formData.append("strata", strata);
    // formData.append("state", state);
    formData.append(
      "description",
      editorState.getCurrentContent().getPlainText()
    );
    formData.append(
      "descriptionHTML",
      convertToHTML(editorState.getCurrentContent())
    );

    featuredImages.map((image, key) => {
      formData.append(`featuredImages[${key}]`, image.image);
    });

    if (previewImage.length) {
      formData.append(`preview_img[${0}]`, previewImage[0].image);
    } else {
      formData.append(`preview_img[${0}]`, "");
    }

    let url = `/api/property/`;
    axios({
      method: "post",
      url,
      data: formData,
      name: "project",
      headers: {
        "content-type": `multipart/form-data`
      }
    })
      .then(r => {
        if (r.status === 200) {
          sweetAlert("success", "Property added successfully.");
          setLoading(false);
          setErrors([]);
          addToggleFetch();
          toggleAddPropertyModal();
        }
      })
      .catch(error => {
        setErrors(error.response.data.errors);
        setLoading(false);
      })
      .then(() => {});
  };

  const handleDrop = e => {
    e.preventDefault();
    e.stopPropagation();

    const fmCount = featuredImages.length;
    const onCount = e.dataTransfer.files.length;

    if (fmCount + onCount > 6) {
      e.target.value = null;
      setLimitError(true);
      return;
    } else {
      setLimitError(false);
    }

    setFeaturedImages(
      featuredImages.concat([
        ...[...e.dataTransfer.files].map(file => {
          let newFilename = sanitizeFilename(file.name);
          let newFileObj = new File([file], newFilename);
          return {
            image: newFileObj,
            preview: URL.createObjectURL(newFileObj)
          };
        })
      ])
    );
    e.target.value = null;
  };

  const handleChange = e => {
    const fmCount = featuredImages.length;
    const onCount = e.target.files.length;

    if (fmCount + onCount > 6) {
      setLimitError(true);
      e.target.value = null;
      return;
    } else {
      setLimitError(false);
    }

    setFeaturedImages(
      featuredImages.concat([
        ...[...e.target.files].map(file => {
          return { image: file, preview: URL.createObjectURL(file) };
        })
      ])
    );
    e.target.value = null;
  };

  const handleRemove = (e, i) => {
    e.preventDefault();
    e.stopPropagation();
    setFeaturedImages([
      ...featuredImages.filter((image, k) => {
        if (i !== k) {
          return image;
        }
      })
    ]);
    if (featuredImages.length > 6) {
      setLimitError(true);
    } else {
      setLimitError(false);
    }
  };

  const handleFloorPlanChange = e => {
    e.preventDefault();

    if (!PDFFileTypeFilter(e.target.files[0]["name"])) {
      sweetAlert("error", "Filetype is not supported!");
      e.target.value = null;
      return;
    }

    let file = Object.values(e.target.files).map(file => {
      let newFilename = file.name.replace(/[^.a-zA-Z0-9]/g, "");
      let newFileObj = new File([file], newFilename);
      return Object.assign(newFileObj, { resource: URL.createObjectURL(newFileObj) });
    });

    setFLoorPlan(floorPlanHelper(e.target.files));

    let newFilename = sanitizeFilename(e.target.files[0]["name"]);
    setFilename(newFilename);
    // e.target.value = null;
  };

  const handleDeletePrevFile = e => {
    e.preventDefault();
    setPreviewImage([]);
    previewImageRef.current.type = "";
    previewImageRef.current.type = "file";
    setPrevFilename("");
    e.target.value = null;
  };

  const handleDeleteFile = e => {
    e.preventDefault();
    setFLoorPlan([]);
    floorPlanRef.current.type = "";
    floorPlanRef.current.type = "file";
    setFilename("");
    e.target.value = null;
  };

  return (
    <Modal
      show={true}
      maxWidth={`md`}
      title={`Add Property`}
      onClose={() => toggleAddPropertyModal()}
    >
      <form
        ref={addPropertyForm}
        className={`bg-white rounded-lg py-5 px-8 m-5`}
        onSubmit={handleSubmit}
      >
        <div className={`text-black`}>
          {loading && (
            <div
              className={`bg-palette-purple flex flex-1 items-center mb-4 px-2 rounded text-white`}
            >
              <label className={`block py-3 font-semibold w-32 mr-2`}>
                Loading Projects
              </label>
              <FontAwesomeIcon icon={faSpinner} className={`fa-spin mr-2`} />
            </div>
          )}

          {isEmpty(deals) && !loading && (
            <BaseAlert
              message={
                "You cannot add property at the moment. All projects already have five properties."
              }
              icon={`x`}
              type={`error`}
            />
          )}

          {!isEmpty(deals) && !loading && (
            <div className={`my-4 flex items-center`}>
              <label className={`block py-3 font-semibold w-32`}>
                Project Name
              </label>
              <div className={`relative`}>
                <Dropdown
                  id="deal"
                  value={selectedDeal}
                  options={deals}
                  onChange={option => {
                    setSelectedDeal(option.value);
                  }}
                  placeholder="Please select..."
                  name="deal"
                />

                {errors["project"] && (
                  <span className="absolute px-1 text-red-400 tracking-widest  text-xs ">
                    {errors["project"][0]}
                  </span>
                )}
              </div>
            </div>
          )}

          <div className={`text-black`}>
            <div className={`py-3 flex justify-start `}>
              <div className={`flex justify-center items-center`}>
                <label className={`font-semibold w-24`}>Property Type</label>
                <div>
                  <Dropdown
                    id="property_type"
                    value={propertyType}
                    options={property_types}
                    onChange={option => {
                      setPropertyType(option.value);
                    }}
                    placeholder="Please select..."
                    // name="property_type"
                    className={`w-48 ml-4`}
                  />

                  {errors["propertyType"] && (
                    <span className="px-1 text-red-400 tracking-widest  text-xs ">
                      {errors["propertyType"][0]}
                    </span>
                  )}
                </div>
              </div>

              <div className={`ml-10 flex-1 flex justify-end items-center`}>
                <label className={`font-semibold w-32 mr-4`}>
                  Property Number
                </label>
                <div>
                  <TextInput
                    className={`border-gray-400 border pl-2 py-1`}
                    border={false}
                    name="unit_name"
                    width="w-24"
                  />

                  {errors["unit_name"] && (
                    <span className="px-1 text-red-400 tracking-widest  text-xs ">
                      {errors["unit_name"][0]}
                    </span>
                  )}
                </div>
              </div>

              <div className={`ml-4 flex-1 flex justify-end items-center`}>
                <label className={`font-semibold w-24 mr-4`}>Lot Number</label>
                <div>
                  <TextInput
                    className={`border-gray-400 border pl-2 py-1`}
                    border={false}
                    name="unit_no"
                    width="w-24"
                  />

                  {errors["unit_no"] && (
                    <span className="px-1 text-red-400 tracking-widest  text-xs ">
                      {errors["unit_no"][0]}
                    </span>
                  )}
                </div>
              </div>
            </div>

            <div className={`flex justify-start my-3`}>
              <div className={`flex items-center justify-start py-3`}>
                <label className={`w-12`}>Price</label>
                <div className={`relative`}>
                  <div>
                    <CurrencyFormat
                      allowNegative={false}
                      className={`border text-sm border-gray-400 px-2 py-1 w-48`}
                      thousandSeparator={true}
                      name="price"
                      id="price"
                      prefix={`$`}
                    />
                  </div>
                  {errors["price"] && (
                    <span className="absolute px-1 text-red-400 tracking-widest text-xxs ">
                      {errors["price"][0]}
                    </span>
                  )}
                </div>
              </div>

              <div className={`py-3 flex items-center ml-12`}>
                <label
                  title={`Number of bedrooms`}
                  className={`cursor-pointer mr-4`}
                >
                  <LocalHotel />
                </label>
                <div className={`flex flex-col w-24`}>
                  <TextInput
                    className={`pl-2 text-sm border-gray-400 w-32 border py-1`}
                    border={false}
                    type={`number`}
                    name={`number_of_bedrooms`}
                  />

                  {errors["number_of_bedrooms"] && (
                    <span className="px-1 text-red-400 tracking-widest  text-xxs ">
                      {errors["number_of_bedrooms"][0]}
                    </span>
                  )}
                </div>
              </div>

              <div className={`py-3 flex items-center ml-16`}>
                <label
                  title={`Number of bathrooms`}
                  className={`cursor-pointer mr-4`}
                >
                  <Bathtub />
                </label>
                <div className={`flex flex-col w-24`}>
                  <TextInput
                    className={`pl-2 text-sm border-gray-400 w-32 border py-1`}
                    border={false}
                    type={`number`}
                    name={`number_of_bathrooms`}
                  />

                  {errors["number_of_bathrooms"] && (
                    <span className="px-1 text-red-400 tracking-widest  text-xxs ">
                      {errors["number_of_bathrooms"][0]}
                    </span>
                  )}
                </div>
              </div>

              <div className={`py-3 flex items-center ml-12`}>
                <label
                  title={`Number of garages`}
                  className={`cursor-pointer mr-4 `}
                >
                  <DirectionsCar />
                </label>

                <div className={`flex flex-col w-24`}>
                  <TextInput
                    className={`pl-2 text-sm border-gray-400 w-32 border py-1`}
                    border={false}
                    type={`number`}
                    name={`number_of_garages`}
                    min="1"
                  />

                  {errors["number_of_garages"] && (
                    <span className="px-1 text-red-400 tracking-widest  text-xxs ">
                      {errors["number_of_garages"][0]}
                    </span>
                  )}
                </div>
              </div>
            </div>

            <div className={`flex`}>
              <div className={`py-3 flex justify-start items-center`}>
                <label className={`w-24`}>Internal SQM</label>
                <div className={`relative`}>
                  <div className={`ml-4 w-32`}>
                    <TextInput
                      className={`pl-2 text-sm border-gray-400 w-32 border py-1`}
                      border={false}
                      type={`number`}
                      name={`internal_size`}
                    />
                  </div>
                  {errors["internal_size"] && (
                    <span className="absolute px-1 text-red-400 tracking-widest  text-xxs ">
                      {errors["internal_size"][0]}
                    </span>
                  )}
                </div>
              </div>

              <div className={`py-3 flex justify-start items-center mx-10`}>
                <label className={`w-24`}>External SQM</label>
                <div className={`relative`}>
                  <div className={`ml-4 w-32`}>
                    <TextInput
                      className={`pl-2 text-sm border-gray-400 w-32 border py-1`}
                      border={false}
                      type={`number`}
                      name={`external_size`}
                    />
                  </div>
                  {errors["external_size"] && (
                    <span className="absolute px-1 text-red-400 tracking-widest  text-xxs ">
                      {errors["external_size"][0]}
                    </span>
                  )}
                </div>
              </div>

              <div className={`py-3 flex justify-start items-center ml-3`}>
                <label className={`w-24`}>Parking SQM</label>
                <div className={`relative`}>
                  <div className={`ml-4 w-32`}>
                    <TextInput
                      className={`pl-2 text-sm border-gray-400 w-32 border py-1 mr-1`}
                      border={false}
                      type={`number`}
                      name={`parking_size`}
                    />
                  </div>
                  {errors["parking_size"] && (
                    <span className="absolute px-1 text-red-400 tracking-widest  text-xxs ">
                      {errors["parking_size"][0]}
                    </span>
                  )}
                </div>
              </div>
            </div>

            <div className={`flex justify-center my-3`}>
              <div className={`py-3 flex flex-1 items-center`}>
                <label
                  title={`Strata`}
                  className={`cursor-pointer mr-4 ${
                    errors["strata"] ? "mb-4" : "mb-0"
                  }`}
                >
                  Strata
                </label>
                <div className={`ml-1`}>
                  <div className={`flex ml-1`}>
                    <CurrencyFormat
                      allowNegative={false}
                      className={`border text-sm border-gray-400 px-2 py-1 w-32`}
                      thousandSeparator={true}
                      name="strata_cost"
                      id="strata_cost"
                      prefix={`$`}
                    />

                    <Dropdown
                      id="strata"
                      value={strata}
                      options={dates}
                      onChange={option => {
                        setStrata(option.value);
                      }}
                      placeholder="Please select..."
                      name="strata"
                      className={`w-48 ml-2`}
                    />
                  </div>
                  <div>
                    {errors["strata"] && (
                      <span className="px-1 text-red-400 tracking-widest  text-xxs ">
                        {errors["strata"][0]}
                      </span>
                    )}
                    {errors["strata_cost"] && (
                      <span className="px-1 text-red-400 tracking-widest  text-xxs ">
                        {errors["strata_cost"][0]}
                      </span>
                    )}
                  </div>
                </div>
              </div>

              <div className={`flex flex-1 items-center justify-end py-3`}>
                <label
                  title={`Water`}
                  className={`cursor-pointer mr-4 ${
                    errors["water"] ? "mb-4" : "mb-0"
                  }`}
                >
                  Water
                </label>
                <div>
                  <div className={`flex ml-2`}>
                    <CurrencyFormat
                      allowNegative={false}
                      className={`border text-sm border-gray-400 px-2 py-1 w-32`}
                      thousandSeparator={true}
                      name="water_cost"
                      id="water_cost"
                      prefix={`$`}
                    />

                    <Dropdown
                      id="water"
                      value={water}
                      options={dates}
                      onChange={option => {
                        setWater(option.value);
                      }}
                      placeholder="Please select..."
                      name="water"
                      className={`w-48 ml-2`}
                    />
                  </div>
                  <div>
                    {errors["water"] && (
                      <span className="px-1 text-red-400 tracking-widest  text-xxs ">
                        {errors["water"][0]}
                      </span>
                    )}
                    {errors["water_cost"] && (
                      <span className="px-1 text-red-400 tracking-widest  text-xxs ">
                        {errors["water_cost"][0]}
                      </span>
                    )}
                  </div>
                </div>
              </div>
            </div>

            <div className={`flex justify-center my-3`}>
              <div className={`py-3 flex flex-1 items-center`}>
                <label
                  title={`Council`}
                  className={`cursor-pointer mr-3 ${
                    errors["council"] ? "mb-4" : "mb-0"
                  }`}
                >
                  Council
                </label>
                <div>
                  <div className={`flex ml-1`}>
                    <CurrencyFormat
                      allowNegative={false}
                      className={`border text-sm border-gray-400 px-2 py-1 w-32`}
                      thousandSeparator={true}
                      name="council_cost"
                      id="council_cost"
                      prefix={`$`}
                    />

                    <Dropdown
                      id="council"
                      value={council}
                      options={dates}
                      onChange={option => {
                        setCouncil(option.value);
                      }}
                      placeholder="Please select..."
                      name="council"
                      className={`w-48 ml-2`}
                    />
                  </div>
                  <div>
                    {errors["council"] && (
                      <span className="px-1 text-red-400 tracking-widest text-xxs ">
                        {errors["council"][0]}
                      </span>
                    )}
                    {errors["council_cost"] && (
                      <span className="px-1 text-red-400 tracking-widest text-xxs ">
                        {errors["council_cost"][0]}
                      </span>
                    )}
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div className={`py-3`}>
            <label>
              <div className="w-full text-sm bg-white text-black ">
                <RichTextEditor
                  title="Property Description"
                  richEditorState={editorState}
                  richEditorStateContent={content => setEditorState(content)}
                  characterLimit={2000}
                ></RichTextEditor>
              </div>
            </label>
            {errors["description"] && (
              <span className="px-1 text-red-400 tracking-widest  text-xs ">
                {errors["description"][0]}
              </span>
            )}
          </div>

          <div className={`py-3`}>
            <h1 className="font-semibold"> Featured Images (min. 3 images) </h1>
            <label className="button " htmlFor="upload-photos">
              <div className={``}>
                <div
                  className="w-full flex flex-wrap  cursor-pointer"
                  style={{
                    border: "2px dashed #ccc",
                    borderRadius: "20px",
                    padding: "20px",
                    textAlign: "center",
                    marginBottom: "0px"
                  }}
                  onDrop={e => handleDrop(e)}
                  onDragOver={e => {
                    e.preventDefault();
                    e.stopPropagation();
                  }}
                  onDragEnter={e => {
                    e.preventDefault();
                    e.stopPropagation();
                  }}
                  onDragLeave={e => {
                    e.preventDefault();
                    e.stopPropagation();
                  }}
                >
                  <input
                    type="file"
                    id="upload-photos"
                    accept="image/*"
                    multiple
                    className="hidden"
                    name="image[]"
                    onChange={e => handleChange(e)}
                  />

                  {featuredImages.map((image, index) => {
                    return (
                      <div
                        key={index}
                        className="rounded-sm border m-2 relative"
                        style={{
                          backgroundImage: `url('${image.preview}')`,
                          backgroundPositionX: "center",
                          width: "6.9em",
                          height: "8.9rem",
                          backgroundSize: "cover"
                        }}
                      >
                        <Tooltip title={`Remove this item`}>
                          <span
                            onClick={e => handleRemove(e, index)}
                            className={`absolute bottom-0 right-0 p-2 -mb-2 `}
                          >
                            <FontAwesomeIcon
                              className={`text-red-500 shadow-xl hover:font-bold hover:text-red-700`}
                              icon={faTimes}
                            />
                          </span>
                        </Tooltip>
                      </div>
                    );
                  })}
                  {fmPlaceHolder.map((p, i) => {
                    return (
                      <div
                        key={i}
                        className={`${p.classes} cursor-pointer flex justify-center items-center`}
                        style={{
                          width: "6.9rem",
                          height: "8.9rem",
                          border: "2px solid #ccc"
                        }}
                      >
                        {" "}
                        <FontAwesomeIcon
                          className={`text-xl`}
                          icon={faCamera}
                        />{" "}
                      </div>
                    );
                  })}
                </div>
                {limitError && (
                  <span className="px-1 text-red-400 tracking-widest  text-xs ">
                    Please limit your photos to six items only.
                  </span>
                )}
              </div>
            </label>
            {errors["featuredImages"] && (
              <span className="px-1 text-red-400 tracking-widest  text-xs ">
                {errors["featuredImages"][0]}
              </span>
            )}
          </div>

          <div className={`flex flex-row`}>
            <div className={`w-1/2 mr-2`}>
              <h1 className={`font-semibold`}>Floor Plan </h1>
              <div className={`flex flex-col justify-between py-3`}>
                <div>
                  <label className={`w-full`}>
                    <div
                      className={`items-center cursor-pointer w-full p-3 rounded-lg flex`}
                      style={{
                        border: "2px dashed #ccc",
                        borderRadius: "20px"
                      }}
                    >
                      <input
                        id="floorPlan"
                        ref={floorPlanRef}
                        type="file"
                        name="floor_plan"
                        onChange={e => handleFloorPlanChange(e)}
                        className={`hidden border text-sm border-gray-400 px-2 py-1 w-full py-1`}
                      />
                      <label
                        htmlFor="floorPlan"
                        className={` cursor-pointer hover:text-palette-blue-light ${
                          filename ? "text-palette-blue-light" : ""
                        }`}
                      >
                        {filename || "Select File"}
                      </label>
                      {filename && (
                        <div
                          className={`text-red-500 text-lg cursor-pointer ml-3`}
                        >
                          <FontAwesomeIcon
                            onClick={e => handleDeleteFile(e)}
                            icon={faTimes}
                          />
                        </div>
                      )}
                    </div>
                  </label>

                  {errors["floorPlan"] && (
                    <span className="px-1 text-red-400 tracking-widest  text-xs ">
                      {errors["floorPlan"][0]}
                    </span>
                  )}
                </div>
              </div>
            </div>

            <div className={`w-1/2 ml-2`}>
              <h1 className={`font-semibold`}>Rich Text Preview </h1>
              <div className={`flex flex-col justify-between py-3`}>
                <div className={`flex`}>
                  <label className={`w-full`}>
                    <div
                      className={`items-center cursor-pointer w-full p-3 rounded-lg flex`}
                      style={{
                        border: "2px dashed #ccc",
                        borderRadius: "20px"
                      }}
                    >
                      <input
                        id="rtp"
                        ref={previewImageRef}
                        type="file"
                        name="prev_image"
                        onChange={e => handlePreviewImageDisplay(e)}
                        className={`hidden border text-sm border-gray-400 px-2 py-1 w-full py-1`}
                      />
                      <label
                        htmlFor="rtp"
                        className={` cursor-pointer hover:text-palette-blue-light ${
                          prevFilename ? "text-palette-blue-light" : ""
                        }`}
                      >
                        {prevFilename || "Select File"}
                      </label>
                      {prevFilename && (
                        <div
                          className={`text-red-500 text-lg cursor-pointer ml-3`}
                        >
                          <FontAwesomeIcon
                            onClick={e => handleDeletePrevFile(e)}
                            icon={faTimes}
                          />
                        </div>
                      )}
                    </div>
                  </label>
                </div>
              </div>
            </div>
          </div>

          <div className="flex justify-end my-5">
            <Button className={`font-bold rounded-full`} disabled={loading}>
              {loading && (
                <FontAwesomeIcon icon={faSpinner} className={`fa-spin mr-2`} />
              )}
              Save
            </Button>
          </div>
        </div>
      </form>
    </Modal>
  );
};
