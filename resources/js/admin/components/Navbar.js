import React, { useState, useRef } from "react";
import { A, usePath } from "hookrouter";

import UserGlobal from "~/states/UserGlobal";
import { isLoggedIn, logout } from "../../services/auth";
import {
  Avatar,
  Popper,
  Grow,
  MenuList,
  MenuItem,
  Paper
} from "@material-ui/core";
import { AccountCircle } from "@material-ui/icons";
import swal from "sweetalert2";
import { isMobile } from "react-device-detect";

import { Modal } from "../../components/_base";
import { SocialMedia } from "../../components";

import CurrencyFormat from "react-currency-format";

const Header = ({}) => {
  const path = usePath();
  const anchorRef = useRef(null);

  const [userState, userAction] = UserGlobal();
  const [popper, setPopper] = useState(null);
  const [showMenu, setShowMenu] = useState(false);

  const links = [
    {
      name: "FAQ",
      route: "/frequently-asked-questions"
    },
    {
      name: "How it works",
      route: "/how-it-works"
    },
    // {
    //   name: "About",
    //   route: "/about"
    // },
    {
      name: "Buying",
      route: "/weekly-deals"
    },
    // {
    //   name: "Selling",
    //   route: "/selling"
    // },
    {
      name: "Contact",
      route: "/contact"
    }
  ];

  const handleLogout = async () => {
    if (isMobile) {
      try {
        await axios.post(`/api/logout`, null, {
          headers: {
            Authorization: "Bearer " + isLoggedIn()
          }
        });

        logout();
        userAction.setState({ fetch: !userState.fetch });
      } catch (e) {
       
      }
      return;
    }

    swal
      .fire({
        title: "Confirmation",
        text: "Are you sure you want to sign out?",
        showConfirmButton: true,
        showCloseButton: true,
        showCancelButton: true,
        focusConfirm: false,
        confirmButtonColor: "#BD00F2",
        confirmButtonText: "Confirm"
      })
      .then(async result => {
        if (result.value) {
          try {
            await axios.post(`/api/logout`, null, {
              headers: {
                Authorization: "Bearer " + isLoggedIn()
              }
            });

            logout();
            userAction.setState({ fetch: !userState.fetch });
          } catch (e) {
           
          }
        }
      });
  };

  const navLinks = () => {
    return links.map((link, key) => {
      return (
        <a
          key={key}
          href={link.route}
          className={`
            ${path !== "/" ? "opacity-50" : "opacity-100"}
            ${
              path.includes(link.route)
                ? "text-palette-teal opacity-100"
                : "text-white"
            } mb-6 lg:mb-0
            hover:text-palette-teal px-3 tracking-wide text-2xl lg:text-base font-bold transition-all duration-300
          `}
        >
          <span>{link.name}</span>
        </a>
      );
    });
  };

  return (
    <header
      style={{ maxWidth: 1366 }}
      className={`relative z-50 flex items-center mx-auto justify-center lg:justify-between px-6 lg:px-16 pt-12`}
    >
      <a href={`/`}>
        <img
          src="/assets/svg/upload_logo_min_negative.svg"
          style={{ height: 40 }}
        />
      </a>

      {!isMobile && (
        <div
          className={`border border-bac flex font-bold h-8 items-center justify-center ml-3 mt-1 px-4 rounded-full text-base text-palette-teal w-auto`}
        >
          Members:&nbsp;
          <CurrencyFormat
            value={userState.userCount}
            displayType={`text`}
            thousandSeparator={true}
            // prefix={`$`}
          />
        </div>
      )}

      {!isMobile ? (
        <div className={`flex items-center text-white`}>
          {/* <a
            onClick={() => userAction.setState({showFAQ: true})}
            href={`#`}
            className={`
          ${path !== '/' ? 'opacity-50' : 'opacity-100'}
          hover:text-palette-teal px-3 tracking-wide text-2xl lg:text-base font-bold transition-all duration-300 mb-6 lg:mb-0 text-white opacity-50`}>
            <span>FAQ</span>
          </a> */}

          {navLinks()}

          {!isLoggedIn() ? (
            <span
              onClick={() => userAction.setState({ showSignIn: true })}
              className={`cursor-pointer hover:text-palette-teal px-3 tracking-wide text-base text-white font-bold transition-all duration-300`}
            >
              Sign in
            </span>
          ) : (
            <div className={`flex items-center`}>
              <div
                className={`border-l border-gray-500 mr-6 ml-3`}
                style={{ width: 1, height: 20 }}
              />
              {/* <svg className={`feather-icon h-5 w-5 mr-4 cursor-pointer`}>
                  <use xlinkHref={`/assets/svg/feather-sprite.svg#heart`}/>
                </svg> */}
              <span
                onMouseOver={e => setPopper(e.currentTarget)}
                onMouseLeave={() => setPopper(null)}
              >
                {userState.user && userState.user.avatar_path ? (
                  <span className={`flex items-center cursor-pointer`}>
                    <span className={`tracking-wide text-base font-bold`}>
                      {userState.user && userState.user.first_name}
                    </span>
                    <Avatar
                      ref={anchorRef}
                      src={`${userState.user.avatar_path}`}
                      className={`cursor-pointer ml-2`}
                    />
                  </span>
                ) : (
                  <span className={`flex items-center cursor-pointer`}>
                    <span className={`tracking-wide text-base font-bold`}>
                      {userState.user && userState.user.first_name}
                    </span>
                    <svg
                      className={`feather-icon h-6 w-6 ml-2`}
                      ref={anchorRef}
                    >
                      <use xlinkHref={`/assets/svg/feather-sprite.svg#user`} />
                    </svg>
                  </span>
                )}
                <Popper
                  open={Boolean(popper)}
                  anchorEl={popper}
                  placement={`bottom-end`}
                  transition
                  disablePortal
                  className={`-mr-4 mt-3 menu-list`}
                >
                  {({ TransitionProps }) => (
                    <Grow
                      {...TransitionProps}
                      style={{ transformOrigin: "right top" }}
                    >
                      <Paper
                        elevation={4}
                        style={{ width: 360 }}
                        className={`relative`}
                      >
                        <div
                          className={`arrow absolute`}
                          style={{
                            top: "-6px",
                            right: "24px",
                            borderWidth: "0 0.8em 0.8em 0.8em",
                            borderColor:
                              "transparent transparent white transparent"
                          }}
                        />
                        <MenuList>
                          <div
                            className={`flex items-center px-4 pt-3 pb-4 mb-2 border-b`}
                          >
                            <div className={`mr-3`}>
                              {userState.user && userState.user.avatar_path ? (
                                <Avatar
                                  ref={anchorRef}
                                  src={`/storage/avatars/${userState.user.avatar_path}`}
                                  className={`cursor-pointer`}
                                />
                              ) : (
                                <AccountCircle style={{ fontSize: 50 }} />
                              )}
                            </div>
                            <div className={`flex flex-col`}>
                              {userState.user && (
                                <>
                                  <span
                                    className={`block font-bold text-xl leading-none`}
                                  >
                                    {userState.user.first_name +
                                      " " +
                                      userState.user.last_name}
                                  </span>
                                  <span className={`block opacity-50`}>
                                    {userState.user.email}
                                  </span>
                                </>
                              )}
                            </div>
                          </div>
                          <MenuItem
                            onClick={() =>
                              (window.location = `/profile/account-settings`)
                            }
                          >
                            Account settings
                          </MenuItem>
                          <MenuItem
                            onClick={() =>
                              (window.location = `/profile/manage-projects`)
                            }
                          >
                            Manage projects
                          </MenuItem>
                          <MenuItem
                            onClick={() =>
                              (window.location = `/profile/secured-deals`)
                            }
                          >
                            Secured deals
                          </MenuItem>
                          <MenuItem
                            onClick={() =>
                              (window.location = `/profile/wish-list`)
                            }
                          >
                            Wish list
                          </MenuItem>
                          <MenuItem onClick={() => handleLogout()}>
                            Sign out
                          </MenuItem>
                        </MenuList>
                      </Paper>
                    </Grow>
                  )}
                </Popper>
              </span>
            </div>
          )}
        </div>
      ) : (
        <>
          <svg
            onClick={() => setShowMenu(!showMenu)}
            className={`absolute cursor-pointer feather-icon h-8 mr-6 right-0 text-white w-8`}
          >
            <use xlinkHref={`/assets/svg/feather-sprite.svg#menu`} />
          </svg>
          <Modal show={showMenu} onClose={() => setShowMenu(!showMenu)}>
            <div
              className={`flex flex-col items-end justify-around bg-opacity-50`}
            >
              {/* <a
                onClick={() => userAction.setState({showFAQ: true})}
                href={`#`}
                className={`
                ${path !== '/' ? 'opacity-50' : 'opacity-100'}
                hover:text-palette-teal px-3 tracking-wide text-2xl lg:text-base font-bold transition-all duration-300 mb-6 lg:mb-0 text-white opacity-50`}>
                <span>FAQ</span>
              </a> */}

              {navLinks()}

              {!isLoggedIn() ? (
                <span
                  onClick={() => userAction.setState({ showSignIn: true })}
                  className={`cursor-pointer px-3 tracking-wide text-2xl lg:text-base text-white font-bold`}
                >
                  Sign in
                </span>
              ) : (
                <>
                  <hr className="block border border-gray-300 mb-6 w-1/4 opacity-25" />
                  <a
                    href={`/profile/account-settings`}
                    className={`mb-6 cursor-pointer px-3 tracking-wide text-2xl lg:text-base text-white font-bold`}
                  >
                    Account settings
                  </a>
                  <a
                    href={`/profile/secured-deals`}
                    className={`mb-6 cursor-pointer px-3 tracking-wide text-2xl lg:text-base text-white font-bold`}
                  >
                    Secured deals
                  </a>
                  <a
                    href={`/profile/wish-list`}
                    className={`mb-6 cursor-pointer px-3 tracking-wide text-2xl lg:text-base text-white font-bold`}
                  >
                    Wish list
                  </a>
                  <span
                    onClick={() => handleLogout()}
                    className={`mb-6 cursor-pointer px-3 tracking-wide text-2xl lg:text-base text-white font-bold`}
                  >
                    Sign out
                  </span>
                </>
              )}

              <span className={`text-white`}>
                <SocialMedia
                  className={`flex flex-row mb-6 lg:justify-start justify-center mt-6`}
                  size={`2x`}
                />
              </span>
            </div>
          </Modal>
        </>
      )}
    </header>
  );
};

export default Header;
