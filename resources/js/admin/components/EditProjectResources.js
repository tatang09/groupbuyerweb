import React, { useState, useRef, useEffect } from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
 
import UserGlobal from "../../states/userGlobal";
import {
  resources,
  tableHeaders
} from "../../helpers/resourceHelper/index";

import { Button } from "~/components/_base";
 
import ResourcesData from "../../components/resources/ResourcesData";
import LandResourcesData from "../../components/resources/LandResourcesData";

import { saveUpdateResources } from "../../data/index";

import {
  sweetAlert,
} from "../../components/_base/alerts/sweetAlert";

const EditProjectResources = ({ toggleFetch }) => {
  const [userState, userActions] = UserGlobal();
  const [projectResources, setProjectResources] = useState(
    userState.projectData.resources ?? []
  );
  const [resourcesHolder, setResourcesHolder] = useState([]);
  const [loading, setLoading] = useState(false);
  const resourceForm = useRef(null);
  const [builderProfile, setBuilderProfile] = useState(
    userState.projectData.builder_profile_link ? "text" : "file"
  );
  const [builderProfileLink, setBuilderProfileLink] = useState("");
  const [arcProfile, setArcProfile] = useState(
    userState.projectData.architect_profile_link ? "text" : "file"
  );
  const [arcProfileLink, setArcProfileLink] = useState("");
  const [projectVideo, setProjectVideo] = useState(
    userState.projectData.video_link ? userState.projectData.video_link : ""
  );

  const [devProfile, setDevProfile] = useState(
    userState.projectData.developer_profile_link ? "text" : "file"
  );
  const [devProfileLink, setDevProfileLink] = useState("");

  useEffect(() => {
    if (userState.projectData.architect_profile_link) {
      setArcProfileLink(userState.projectData.architect_profile_link);
    }

    if (userState.projectData.builder_profile_link) {
      setBuilderProfileLink(userState.projectData.builder_profile_link);
    }

   

    if (userState.projectData.developer_profile_link) {
      setDevProfileLink(userState.projectData.developer_profile_link);
    }
  }, []);

  const handleSubmit = e => {
    e.preventDefault();
    saveProjectResources();
  };

  const saveProjectResources = async () => {
    const formData = new FormData(resourceForm.current);
 
    projectResources.forEach((resource, key) => {
      if (arcProfile === "text" && resource.name === "architect_profile") {
        return;
      }
      if (builderProfile === "text" && resource.name === "builder_profile") {
        return;
      }

   
      if (devProfile === "text" && resource.name === "developer_profile") {
        return;
      }

      formData.append(`resources[${key}][file]`, resource.file);
      formData.append(`resources[${key}][name]`, resource.name);
    });

    if (devProfileLink) {
      formData.append(
        "devProfileLink",
        resources.appendHttpsToResource(devProfileLink)
      );
    }

    if (arcProfileLink) {
      formData.append(
        "arcProfileLink",
        resources.appendHttpsToResource(arcProfileLink)
      );
    }

    if (builderProfileLink) {
      formData.append(
        "builderProfileLink",
        resources.appendHttpsToResource(builderProfileLink)
      );
    }

    setLoading(true);

    formData.append("id", userState.projectData.id);
    formData.append("name", userState.projectData.name);
    formData.append("projectVideo", projectVideo);

    let url = `/api/project-resource/update`;

    await saveUpdateResources({ formData, url })
      .then(r => {
        if (r.status === 200) {
          toggleFetch();
          setLoading(false);
          sweetAlert("success", "Project resources successfully updated.");
          userActions.setState({ projectData: r.data });
          userActions.setState({ propertiesData: r.data.properties });
        }
      })
      .catch(err => {
        setLoading(false);
      })
      .then(() => {});
  };

  const handleFileChange = (e, resource) => {
    e.preventDefault();

    let c = projectResources.filter(file => {
      if (file["name"] !== resource) {
        return file;
      }
    });

    if (e.target.files.length > 0) {
      setProjectResources(
        c.concat(resources.trimFilename(e.target.files[0], resource))
      );
      e.target.value = null;
    }
  };

  const handleDelete = (e, resource) => {
    e.preventDefault();
 
    setProjectResources(
      projectResources.filter(file => {
        return file["name"] !== resource;
      })
    );

    setResourcesHolder(
      resourcesHolder.filter(file => {
        return file["name"] !== resource;
      })
    );
    e.target.value = null;
  };

  const getFileName = resource => {
    let filename = "";

    projectResources.forEach(file => {
      if (file["name"] === resource) {
        if (typeof file["file"].name == "string") {
          filename = file["file"]["name"];
        } else {
          const first = file["file"].split(`/${resource}/`)[1];
          filename = first.split("?")[0];
        }
      }
    });

    return filename;
  };

  const changeInputType = (rowName, checked) => {
    if (rowName === "Builder Profile") {
      if (checked) {
        setBuilderProfile("text");
      } else {
        setBuilderProfileLink("");
        setBuilderProfile("file");
      }
    }

    if (rowName === "Architect Profile") {
      if (checked) {
        setArcProfile("text");
      } else {
        setArcProfileLink("");
        setArcProfile("file");
      }
    }

    if (rowName === "Developer Profile") {
      if (checked) {
        setDevProfile("text");
      } else {
        setDevProfileLink("");
        setDevProfile("file");
      }
    }
  };

  const setCheckedState = row => {
    let retval = false;
    if (row.label === "Builder Profile") {
      if (builderProfile === "text") {
        retval = true;
      }
    }

    if (row.label === "Architect Profile") {
      if (arcProfile === "text") {
        retval = true;
      }
    }

    if (row.label === "Developer Profile") {
      if (devProfile === "text") {
        retval = true;
      }
    }
    return retval;
  };

  return (
    <div className={`bg-white rounded-lg py-5 lg:px-8 px-4 lg:m5 my-5`}>
      <form ref={resourceForm} onSubmit={handleSubmit}>
        <div className={`overflow-x-auto`}>
          <table className={`w-full`}>
            <thead className={`w-full`}>
              <tr>
                {tableHeaders.map((header, key) => {
                  return (
                    <th
                      key={key}
                      className={` ${header.style} border-b font-bold lg:px-4 px-1 py-2 text-palette-gray py-2 ${header.width} `}
                    >
                      {header.label}
                    </th>
                  );
                })}
              </tr>
            </thead>
            <tbody className={`w-full`}>
              {userState.projectData &&
              userState.projectData.sub_property_id == "4" ? (
                <LandResourcesData
                  devProfile={devProfile}
                  devProfileLink={devProfileLink}
                  getFileName={getFileName}
                  setCheckedState={setCheckedState}
                  changeInputType={changeInputType}
                  handleFileChange={handleFileChange}
                  handleDelete={handleDelete}
                  setProjectVideo={setProjectVideo}
                  projectVideoDefaultValue={projectVideo}
                  setDevProfileLink={setDevProfileLink}
                />
              ) : (
                <ResourcesData
                  builderProfile={builderProfile}
                  devProfile={devProfile}
                  devProfileLink={devProfileLink}
                  arcProfile={arcProfile}
                  getFileName={getFileName}
                  setCheckedState={setCheckedState}
                  changeInputType={changeInputType}
                  handleFileChange={handleFileChange}
                  handleDelete={handleDelete}
                  setProjectVideo={setProjectVideo}
                  projectVideoDefaultValue={projectVideo}
                  setDevProfileLink={setDevProfileLink}
                />
              )}
            </tbody>
          </table>
        </div>
        <div className="flex justify-center lg:justify-end my-5">
          <Button
            className={`block button button-primary rounded-full`}
            disabled={loading}
          >
            {loading && (
              <FontAwesomeIcon
                icon={["fas", "spinner"]}
                className={`fa-spin mr-2`}
              />
            )}
            Continue
          </Button>
        </div>
      </form>
    </div>
  );
};

export default EditProjectResources;
