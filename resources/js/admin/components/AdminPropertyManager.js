import React, { useState, useEffect } from "react";
import EditPropertyModal from "./EditPropertyModal";
import AddProperty from "./AddProperty";
import AddLandProperty from "./AddLandProperty";
import EditLandPropertyModal from './EditLandPropertyModal';
import UserGlobal from "../../states/userGlobal";
import { sweetAlert } from "../../components/_base/alerts/sweetAlert";
import BaseAlert from "~/components/_base/alerts/BaseAlert";
import { Button } from "~/components/_base";

const AdminPropertyManager = ({
  toggleSEM,
  toggleFetch,
  toggleAgreementView
}) => {
  const [userState, userActions] = UserGlobal();
  const [properties, setProperties] = useState([]);
  const [property, setProperty] = useState([]);
  const [toggleEditProperty, setToggleEditProperty] = useState(false);
  const [toggleAP, setToggleAP] = useState(false);

  const [toogleLandProperty, setToogleLandProperty] = useState(false);
  const [toogleEditLandProperty, setToogleEditLandProperty] = useState(false);
  const [isProjectLand, setIsprojectLand] = useState(userState.projectData.sub_property_id == 4 ? true : false);

  useEffect(() => {
    setProperties(userState.projectData.properties);
    userActions.setState({ propertiesData: userState.projectData.properties });
  }, []);

  const nextStep = () => {
    userActions.setState({ currentProjStep: 4 });
    toggleAgreementView();
  };

  const toggleEditPropertyModal = () => {
    if(!isProjectLand){
      setToggleEditProperty(!toggleEditProperty);
    }else{
      setToogleEditLandProperty(!toogleEditLandProperty);
    }
  };

  const toggleAddPropertyModal = () => {
    if (userState.propertiesData.length >= 5) {
      return sweetAlert("error", "You can only add up to 5 properties");
    }
    if(!isProjectLand){
      setToggleAP(!toggleAP);
    }else{
      setToogleLandProperty(!toogleLandProperty);
    }
  };

  return (
    <div className={`relative bg-white py-5 m-5 rounded-lg`}>
      {toggleEditProperty && (
        <EditPropertyModal
          projectName={userState.projectData.name}
          projectId={userState.projectData.id}
          property={property}
          toggleEditPropertyModal={() => toggleEditPropertyModal()}
          editToggleFetch={toggleFetch}
        ></EditPropertyModal>
      )}
       {toogleEditLandProperty && (
        <EditLandPropertyModal
          projectName={userState.projectData.name}
          projectId={userState.projectData.id}
          property={property}
          toggleEditPropertyModal={() => toggleEditPropertyModal()}
          editToggleFetch={toggleFetch}
        ></EditLandPropertyModal>
      )}
      {toggleAP && userState.propertiesData.length < 5 && (
        <AddProperty
          projectName={userState.projectData.name}
          projectId={userState.projectData.id}
          toggleAddPropertyModal={() => toggleAddPropertyModal()}
          addToggleFetch={toggleFetch}
        ></AddProperty>
      )}
      {toogleLandProperty && userState.propertiesData.length < 5 && (
        <AddLandProperty
          projectName={userState.projectData.name}
          projectId={userState.projectData.id}
          toggleAddPropertyModal={() => toggleAddPropertyModal()}
          addToggleFetch={toggleFetch}
        ></AddLandProperty>
      )}
      <div className="flex justify-around flex-wrap ">
        {userState.propertiesData &&
          userState.propertiesData.map((property, k) => {
            return (
              <div
                className={`cursor-pointer text-palette-gray hover:bg-gray-200 hover:text-gray-800 `}
                onClick={() => {
                  setProperty(property);
                  toggleEditPropertyModal();
                }}
                key={property.id}
              >
                <PropertyInfo pId={k + 1} property={property} />
              </div>
            );
          })}
        <div onClick={() => toggleAddPropertyModal()}>
          <PropertyPlaceholder properties={properties} />
        </div>
      </div>
      <div className={`flex flex-1 px-3 ${userState.propertiesData.length < 5 ? "justify-end mr-16" : ""}`}>
        {userState.propertiesData.length == 5 && (
          <BaseAlert
            message={
              "You have reached the maximum 5 properties per project. Once all 5 properties are sold you can upload another 5 properties."
            }
            icon={`check`}
            type={`success`}
            iconHeight={`h-6`}
            iconWidth={`w-8`}
            className={`flex items-center mb-2 px-6 py-2 rounded w-9/12`}
          />
        )}

        <Button
          className={`button button-primary font-bold h-10 ml-10 mt-3 rounded rounded-full`}
          onClick={() => nextStep()}
          disabled={userState.propertiesData.length < 5}
        >
          Next Step
        </Button>
      </div>
    </div>
  );
};

const PropertyPlaceholder = ({ properties, toggleAPM }) => {
  const [userState, userActions] = UserGlobal();
  const [placeHolder, setPlaceHolder] = useState([]);

  useEffect(() => {
    let phs = [];
    for (let index = 0; index < 5 - userState.propertiesData.length; index++) {
      const ph = (
        <div
          key={index}
          className={`cursor-pointer m-5 inline-flex bg-transparent text-palette-blue-light`}
          style={{
            width: "7rem",
            height: "7rem",
            border: "2px dashed #ccc",
            borderRadius: "5px"
          }}
        ></div>
      );
      phs.push(ph);
    }
    setPlaceHolder(phs);
  }, [userState.propertiesData.length]);

  return <div>{placeHolder}</div>;
};

const PropertyInfo = ({ pId, property }) => {
  return (
    <div className={` m-5`}>
      <div>
        <div
          className={` bg-transparent rounded-lg border border-gray-400`}
          style={{
            backgroundImage: `url('${property.featured_images[0]}')`,
            backgroundPositionX: "center",
            backgroundSize: "cover",
            width: "7rem",
            height: "7rem"
          }}
        ></div>
        <span
          className={`p-2  text-xs`}
        >{`Property #${pId} - ${property.unit_name}`}</span>
      </div>
    </div>
  );
};

export default AdminPropertyManager;
