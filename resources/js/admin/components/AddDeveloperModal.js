import React, { useState, useEffect, createRef, useRef } from "react";
import "../../helpers/styles/dropdown.css";
import { Modal } from "~/components/_base";

import "../../helpers/styles/toast.css";

import { isEmpty } from "lodash";

import UserGlobal from "~/states/userGlobal";
import { Button, Form, TextInput } from "~/components/_base";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faSpinner } from "@fortawesome/free-solid-svg-icons";
import Collapse from "@material-ui/core/Collapse";
import { sweetAlert } from "../../components/_base/alerts/sweetAlert";
import Select from "react-select";
import {
  fields,
  developerFields
} from "../../helpers/userAccountSettingsHelper/userAccountHelper";

import { countries } from "~/helpers/countries";
import { states } from "../../helpers/propertyHelper/propertyHelper";
import { fetchBanks } from "../../data/index";

const AddDeveloperModal = ({ toogleAddDeveloper, fetchDevelopers }) => {
  const [userState, userAction] = UserGlobal();
  const [loading, setLoading] = useState(false);
  const [errors, setErrors] = useState({});
  const [isOpen, setIsOpen] = useState(true);

  const [selectedState, setSelectedState] = useState([]);
  const [selectedCountry, setSelectedCountry] = useState([]);
  const [profilePic, setProfilePic] = useState(
    "/assets/images/profile_default.jpg"
  );
  const [companyPic, setCompanyPic] = useState("/assets/images/no_logo.png");

  const form = createRef();

  const profileInput = useRef(null);
  const companyInput = useRef(null);

  const [selectedBank, setSelectedBank] = useState("");
  const [bankList, setBankList] = useState([]);

  const [showOtherBankText, setShowOtherBankText] = useState(false);

  Object.keys(fields).forEach(key => {
    fields[key].value = "";
  });

  Object.keys(developerFields).forEach(key => {
    developerFields[key].value = "";
  });

  useEffect(() => {
    getBanks();
  }, []);

  const getBanks = async () => {
    let url = "/api/bank";
    await fetchBanks({ url }).then(res => {
      let newBankList = [
        ...res.data.map(bank => {
          return {
            label: `${bank.name}`,
            value: String(bank.id)
          };
        })
      ];

      newBankList.push({ label: "Other", value: "Other" });
      setBankList(newBankList);
    });
  };

  const handleClick = isProfile => {
    if (isProfile) {
      profileInput.current.click();
    } else {
      companyInput.current.click();
    }
  };

  const handleFileChange = (e, isProfile) => {
    const imageFile = e.target.files[0];

    if (!imageFile.name.match(/\.(jpg|jpeg|png|gif)$/)) {
      return sweetAlert("error", "Please select valid image.");
    }

    if (isProfile) {
      setProfilePic(URL.createObjectURL(imageFile));
    } else {
      setCompanyPic(URL.createObjectURL(imageFile));
    }
  };

  const handleSubmit = async e => {
    e.preventDefault();

    if (profilePic.substring(0, 8) === "/assets/") {
      return sweetAlert("warning", "Developer profile picture is required.");
    }

    if (companyPic.substring(0, 8) === "/assets/") {
      return sweetAlert("warning", "Company profile picture is required.");
    }


    let formData = new FormData(form.current);

    let phone = formData.get("phone") ? "+61" + formData.get("phone") : "";

    formData.set("phone", phone);

    formData.append("user_type", "project_developer");

    // let email = formData.get("userEmail");

    // formData.append("email", email);

    formData.append("dev_state", selectedState.value || "");

    formData.append("dev_country", selectedCountry.value || "");

    if (!isEmpty(selectedBank) && selectedBank.value !== "Other") {
      formData.set("bank_id", selectedBank.value);
    }

    setLoading(true);

    let url = "/api/user";

    await axios
      .post(url, formData)
      .then(res => {
        if (res.status == 200) {

          sweetAlert("success", "Developer successfully saved.");

          userAction.setState({ fetch: !userState.fetch });

          fetchDevelopers();

          setLoading(false);
        }
      })
      .catch(error => {

        let errors = {};

        let { data } = error.response;

        if (data.errors.length == 1 && data.errors[0].phone) {
          errors = [];
        }

        errors = data.errors;

        setLoading(false);

        if (errors && errors.avatar_path) {
          sweetAlert("error", errors.avatar_path[0]);
          setProfilePic("/assets/images/profile_default.jpg");
        }

        setErrors(errors || {});
      });

  };

  const handleChange = (key, e) => {
    if (key === "dev_country") {
      setSelectedCountry(e);
    } else if (key === "dev_state") {
      setSelectedState(e);
    } else {
      if (e.value === "Other") {
        setShowOtherBankText(true);
      } else {
        setShowOtherBankText(false);
      }
      setSelectedBank(e);
    }
  };

  return (
    <Modal
      show={true}
      title={"Add Developer"}
      disableBackdropClick={true}
      maxWidth={`md`}
      topOff={true}
      scroll={`body`}
      onClose={() => toogleAddDeveloper()}
    >
      <div className={`bg-white rounded-lg py-5 px-8 m-5`}>
        <div>
          <section>
            <section className={`relative bg-white`}>
              <div className={`pb-16 mx-auto`} style={{ maxWidth: 1366 }}>
                <h1
                  className={`font-bold leading-tight text-4xl pt-10 pb-8 lg:pb-16 text-center`}
                >
                  Member Profile
                </h1>

                <form ref={form} onSubmit={handleSubmit}>
                  <div className={`flex flex-col-reverse lg:flex-row-reverse`}>
                    <div className={`lg:mt-0 lg:w-8/12 ml-16 mt-8`}>
                      <Form errors={errors} formFields={fields} />

                      <div className={`mb-5 text-base flex`}>
                        <label className="mr-3 w-32 mt-2 capitalize pr-3">
                          <span className="font-bold">Password</span>
                        </label>

                        <div className={`w-full`}>
                          <div
                            onClick={() => setIsOpen(!isOpen)}
                            className={`
                          border border-palette-gray text-palette-gray hover:bg-palette-gray hover:text-white transition-all duration-300
                          cursor-pointer font-bold inline-block px-6 text-sm py-2 rounded shadow
                        `}
                          >
                            Change Password
                          </div>

                          <Collapse in={isOpen}>
                            {isOpen && (
                              <div>
                                <div className={`mt-5 text-sm`}>
                                  New Password
                                </div>
                                <TextInput
                                  type={`password`}
                                  placeholder={`New Password`}
                                  name={`password`}
                                  border={false}
                                  appearance={true}
                                />
                                {errors.password && (
                                  <span className="text-red-500 text-xs">
                                    {errors.password[0]}
                                  </span>
                                )}

                                <div className={`mt-5 text-sm`}>
                                  Confirm Password
                                </div>
                                <TextInput
                                  type={`password`}
                                  placeholder={`Confirm Password`}
                                  name={`password_confirmation`}
                                  border={false}
                                  appearance={true}
                                />
                              </div>
                            )}
                          </Collapse>
                        </div>
                      </div>

                      <>
                        <div
                          className={`mx-auto -ml-20`}
                          style={{ maxWidth: 1366 }}
                        >
                          <h1
                            className={`font-bold leading-tight ml-3 lg:pb-16 pb-8 pt-10 text-4xl`}
                          >
                            Seller Profile
                          </h1>
                        </div>

                        <Form errors={errors} formFields={developerFields} />

                        <div className={`mb-5 text-base flex`}>
                          <label className="mr-3 w-32 mt-2 capitalize pr-3">
                            <span className="font-bold">Bank</span>
                          </label>

                          <div className={`w-full`}>
                            <div className={`flex`}>
                              <Select
                                isOptionSelected
                                name={`bank`}
                                classNamePrefix={`input-select`}
                                onChange={e => handleChange("bank", e)}
                                className={`w-full`}
                                placeholder={`Select Bank`}
                                value={selectedBank}
                                options={bankList}
                              />
                            </div>
                          </div>
                        </div>

                        <Collapse in={showOtherBankText}>
                          <div className={`mb-5 text-base flex`}>
                            <label className="mr-3 w-32 mt-2 capitalize pr-3">
                              <span className="font-bold"></span>
                            </label>

                            <div className={`w-full`}>
                              <div className={`flex`}>
                                <TextInput
                                  type={`text`}
                                  placeholder={`Enter Bank Name`}
                                  name={`new_bank_name`}
                                  border={false}
                                  appearance={true}
                                />
                              </div>
                            </div>
                          </div>
                        </Collapse>

                        <div className={`mb-5 text-base flex`}>
                          <label className="mr-3 w-32 mt-2 capitalize pr-3">
                            <span className="font-bold">Acct. Name</span>
                          </label>

                          <div className={`w-full`}>
                            <div className={`flex`}>
                              <TextInput
                                type={`text`}
                                placeholder={`Account Name`}
                                name={`bank_account_name`}
                                border={false}
                                appearance={true}
                              />
                              {errors.bank_account_name && (
                                <span className="text-red-500 text-xs">
                                  {errors.bank_account_name[0]}
                                </span>
                              )}
                            </div>
                          </div>
                        </div>

                        <div className={`mb-5 text-base flex`}>
                          <label className="mr-3 w-32 mt-2 capitalize pr-3">
                            <span className="font-bold">BSB</span>
                          </label>

                          <div className={`w-full`}>
                            <div className={`flex`}>
                              <TextInput
                                type={`text`}
                                placeholder={`Enter BSB`}
                                name={`bank_bsb`}
                                border={false}
                                appearance={true}
                              />
                              {errors.bank_bsb && (
                                <span className="text-red-500 text-xs">
                                  {errors.bank_bsb[0]}
                                </span>
                              )}
                            </div>
                          </div>
                        </div>

                        <div className={`mb-5 text-base flex`}>
                          <label className="mr-3 w-32 mt-2 capitalize pr-3">
                            <span className="font-bold">Acct. No.</span>
                          </label>

                          <div className={`w-full`}>
                            <div className={`flex`}>
                              <TextInput
                                type={`text`}
                                placeholder={`Enter Account Number`}
                                name={`bank_account_number`}
                                border={false}
                                appearance={true}
                              />
                              {errors.bank_account_number && (
                                <span className="text-red-500 text-xs">
                                  {errors.bank_account_number[0]}
                                </span>
                              )}
                            </div>
                          </div>
                        </div>

                        <div className={`mb-5 text-base flex`}>
                          <label className="mr-3 w-32 mt-2 capitalize pr-3">
                            <span className="font-bold">State</span>
                          </label>

                          <div className={`w-full`}>
                            <div className={`flex`}>
                              <Select
                                isOptionSelected
                                name={`dev_state`}
                                classNamePrefix={`input-select`}
                                onChange={e => handleChange("dev_state", e)}
                                className={`w-full`}
                                placeholder={`Select State`}
                                value={selectedState}
                                options={states}
                              />
                            </div>
                            {errors["value"] && (
                              <span className="text-red-500 text-sm">
                                {errors[value]}
                              </span>
                            )}
                          </div>
                        </div>

                        <div className={`mb-5 text-base flex`}>
                          <label className="mr-3 w-32 mt-2 capitalize pr-3">
                            <span className="font-bold">Country</span>
                          </label>

                          <div className={`w-full`}>
                            <div className={`flex`}>
                              <Select
                                isOptionSelected
                                name={`dev_country`}
                                classNamePrefix={`input-select`}
                                onChange={e => handleChange("dev_country", e)}
                                className={`w-full`}
                                placeholder={`Select Country`}
                                value={selectedCountry}
                                options={countries}
                              />
                            </div>
                            {errors["value"] && (
                              <span className="text-red-500 text-sm">
                                {errors[value]}
                              </span>
                            )}
                          </div>
                        </div>
                      </>
                    </div>

                    <div className={`flex flex-col w-1/2`}>
                      <div
                        className={`flex-1`}
                        //className={`lg:w-4/12`}
                      >
                        <input
                          type={`file`}
                          name={`avatar_path`}
                          className={`hidden`}
                          ref={profileInput}
                          onChange={e => handleFileChange(e, true)}
                        />
                        <div
                          className={`relative mx-auto`}
                          style={{ width: "fit-content" }}
                        >
                          <img
                            className={`border-4 border-palette-gray h-48 lg:h-56 rounded-full shadow-lg w-48 lg:w-56 object-cover`}
                            src={profilePic}
                          />
                          <span
                            className={`
                        absolute bg-white border border-gray-300 bottom-0 cursor-pointer flex h-10 items-center justify-center mb-3 mr-3 right-0
                        rounded-full shadow-md w-10 transition-all duration-300 hover:bg-palette-purple hover:text-white hover:border-palette-purple
                      `}
                            onClick={() => handleClick(true)}
                          >
                            <svg className={`feather-icon`}>
                              <use
                                xlinkHref={`/assets/svg/feather-sprite.svg#edit-2`}
                              />
                            </svg>
                          </span>
                        </div>
                      </div>

                      <div
                        className={`flex-1 mb-24`}
                        //className={`lg:w-4/12`}
                      >
                        <input
                          type={`file`}
                          name={`company_logo_path`}
                          className={`hidden`}
                          ref={companyInput}
                          onChange={e => handleFileChange(e, false)}
                        />
                        <div
                          className={`relative mx-auto`}
                          style={{ width: "fit-content" }}
                        >
                          <img
                            className={`border-4 border-palette-gray h-48 lg:h-56 shadow-lg w-48 lg:w-56 object-cover`}
                            src={companyPic}
                          />
                          <span
                            className={`
                        absolute bg-white border border-gray-300 bottom-0 cursor-pointer flex h-10 items-center justify-center mb-3 mr-3 right-0
                        rounded-full shadow-md w-10 transition-all duration-300 hover:bg-palette-purple hover:text-white hover:border-palette-purple
                      `}
                            onClick={() => handleClick(false)}
                          >
                            <svg className={`feather-icon`}>
                              <use
                                xlinkHref={`/assets/svg/feather-sprite.svg#edit-2`}
                              />
                            </svg>
                          </span>
                        </div>
                      </div>
                    </div>
                  </div>

                  <div className={`flex justify-center lg:justify-end mt-8`}>
                    <Button
                      onClick={() => toogleAddDeveloper()}
                      className={`bg-red-700 button font-bold mr-2 rounded text-gray-100`}
                      disabled={loading}
                    >
                      Cancel
                    </Button>
                    <Button className={`font-bold rounded`} disabled={loading}>
                      {loading && (
                        <FontAwesomeIcon
                          icon={faSpinner}
                          className={`fa-spin mr-2`}
                        />
                      )}
                      Save
                    </Button>
                  </div>
                </form>
              </div>
            </section>
          </section>
        </div>
      </div>
    </Modal>
  );
};

export default AddDeveloperModal;
