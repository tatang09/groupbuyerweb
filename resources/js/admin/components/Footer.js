import React, { useState } from "react";
import { ContactForm, SocialMedia } from "../../components";
import UserGlobal from "~/states/userGlobal";
import { isMobile } from "react-device-detect";
import moment from "moment";

const Footer = ({ topFooter }) => {
  const [userState, userAction] = UserGlobal();

  const gBuyerContactDetail = {
    address: "Level 27, 20 Bond St, Sydney 2000",
    poBox: "1300 031 835",
    email: "info@groupbuyer.com.au"
  };

  return (
    <footer name={`footer`} className={`bg-palette-blue-dark`}>
      {topFooter && (
        <>
          {/* Top Footer */}
          <div className={`mx-auto`} style={{ maxWidth: 1366 }}>
            <div className="bg-palette-blue-dark lg:flex lg:px-16 mx-auto pb-10 pt-16 px-4 text-white">
              <div className="flex-col lg:pr-16 lg:w-5/12">
                {/* {isMobile && (
                  <SocialMedia className={`flex flex-row mb-6`} size={`2x`} />
                )} */}
                <div>
                  <h2 className="text-4xl lg:text-5xl font-bold mb-4 lg:mb-8">
                    Questions?
                  </h2>
                  <p className="text-lg mb-8">
                    Check out our FAQ page{" "}
                    <a
                      href="/frequently-asked-questions"
                      className={`text-palette-teal underline`}
                    >
                      here
                    </a>{" "}
                    for all frequently asked questions or feel free to get in
                    touch directly with one of our team members.
                  </p>
                  <h2 className="lg:block lg:text-left mb-2 text-xl">
                    Connect with us
                  </h2>
                  <SocialMedia
                    className={`lg:flex flex-row mb-6 lg:justify-start`}
                    size={`2x`}
                  />
                </div>
                {/* {!isMobile && (
                  <div>
                    <h4 className="text-3xl font-bold mb-8">Our directors</h4>
                    <div className="flex">
                      <div className="text-center">
                        <img
                          src="/assets/images/_directors/ben-daniel.jpg"
                          className={`rounded-full`}
                          style={{ width: 120 }}
                        />
                        <div className="text-base mt-2 font-bold">Ben Daniel</div>
                      </div>
                      <div className='text-center ml-10'>
                        <img
                          src="/assets/images/_directors/luke-hayes.jpg"
                          className={`rounded-full`}
                          style={{ width: 120 }}
                        />
                        <div className="text-base mt-2 font-bold">Luke Hayes</div>
                      </div>
                    </div>
                  </div>
                )} */}
                <div className="lg:mb-0 lg:py-8 text-lg">
                  <div>{gBuyerContactDetail.address}</div>
                  <div className={`font-bold flex flex-col lg:flex-row`}>
                    <span>
                      P:{" "}
                      <a
                        className="text-blue-500 hover:text-blue-300"
                        href="tel:1300031835"
                      >
                        {gBuyerContactDetail.poBox}
                      </a>
                    </span>
                    <span className="hidden lg:inline">&nbsp;•&nbsp;</span>
                    <span>
                      E:{" "}
                      <a
                        className="text-blue-500 hover:text-blue-300"
                        href="mailto:info@groupbuyer.com.au"
                      >
                        {gBuyerContactDetail.email}
                      </a>
                    </span>
                  </div>
                </div>
              </div>

              <div className="flex-col lg:text-left lg:w-7/12 lg:pt-6">
                {/* flex flex-row flex-wrap items-center justify-center lg:justify-start mb-6 */}
                <ContactForm
                  backgroundColor={isMobile ? "transparent" : "#000024"}
                />
              </div>

              {/* {isMobile && (
                <div>
                  <h4 className="text-3xl font-bold mb-8">Our Directors</h4>
                  <div className="flex justify-center">
                    <div className="text-center">
                      <img
                        src="/assets/images/_directors/ben-daniel.jpg"
                        className={`rounded-full`}
                        style={{ width: 120 }}
                      />
                      <div className="text-base mt-2 font-bold">Ben Daniel</div>
                    </div>
                    <div className='text-center ml-10'>
                      <img
                        src="/assets/images/_directors/luke-hayes.jpg"
                        className={`rounded-full`}
                        style={{ width: 120 }}
                      />
                      <div className="text-base mt-2 font-bold">Luke Hayes</div>
                    </div>
                  </div>
                </div>
              )} */}
            </div>
          </div>
        </>
      )}

      {/* Bottom Footer */}
      <div
        style={{ maxWidth: 1366 }}
        className={`flex flex-col flex-wrap items-center justify-center lg:flex-row lg:px-16 mx-auto px-4 text-gray-500`}
      >
        <div className="flex items-center lg:inline-block lg:py-12 lg:w-1/4">
          <a href={`/terms-and-conditions`}>
            <span
              className="transition-all duration-300 text-xs cursor-pointer hover:text-palette-teal mr-4"
              // onClick={() => userAction.setState({ showTermsAndConditions: true })}
            >
              Terms &amp; Conditions
            </span>
          </a>

          <a href={`/privacy-policy`}>
            <span
              className="transition-all duration-300 text-xs cursor-pointer hover:text-palette-teal"
              // onClick={() => userAction.setState({ showPrivacyPolicy: true })}
            >
              Privacy Policy
            </span>
          </a>
          {/* <span
            className="transition-all duration-300 text-xs cursor-pointer hover:text-palette-teal ml-4"
            onClick={() => userAction.setState({ showFAQ: true })}
          >
            FAQ
          </span> */}
        </div>
        <div className="py-4 lg:py-0 flex flex-col lg:flex-row flex-wrap items-center justify-center lg:w-1/2 text-xs">
          <span>© {moment().format("YYYY")} Group Buyer •&nbsp;</span>
          <span>
            Designed by
            <a
              className="hover:text-palette-teal"
              href="http://emblm.agency/"
              target="_blank"
            >
              <strong>&nbsp;Emblm&nbsp;</strong>|
            </a>
            <span>
              {" "}
              Developed by
              <a
                className={`hover:text-palette-teal`}
                href="https://fligno.com/"
                target="_blank"
              >
                <strong>&nbsp;Fligno</strong>
              </a>
            </span>
          </span>
        </div>
        <div className="flex hidden items-center lg:inline-block lg:py-12 lg:w-1/4">
          <SocialMedia className={`justify-end`} variant={`secondary`} />
        </div>
      </div>
    </footer>
  );
};

export default Footer;
