import React, { useEffect } from "react";
import AddingSteps from "../../components/AddingSteps";
import AddProjectPart from "./AddProjectPart";
import AddProjectResources from "./AddProjectResources";
import AdminPropertyManager from "./AdminPropertyManager";
import AgencyAgreementPart from "./AgencyAgreementPart";
import { Modal, Button } from "../../components/_base";
import UserGlobal from "../../states/userGlobal";

const SequenceAddingModal = ({ toggleSequenceAddingModal, addToggleFetch }) => {
  const [userState, userActions] = UserGlobal();
  useEffect(() => {
    userActions.setState({ currentProjStep: 1 });
    userActions.setState({ projectData: [] });
  }, []);
  return (
    <Modal
      show={true}
      disableBackdropClick={true}
      topOff={true}
      maxWidth={`md`}
      title={`Add Project`}
      onClose={() => toggleSequenceAddingModal()}
    >
      <AddingSteps />
      {userState.currentProjStep === 1 && <AddProjectPart />}
      {userState.currentProjStep === 2 && <AddProjectResources />}
      {userState.currentProjStep === 3 && <AdminPropertyManager 
      toggleFetch={addToggleFetch}   />}
      {userState.currentProjStep === 4 && <AgencyAgreementPart />}
    </Modal>
  );
};

export default SequenceAddingModal;
