import React, { useState, useEffect, useRef } from "react";
import Dropdown from "react-dropdown";
import "../../helpers/styles/dropdown.css";
import {
  Table,
  TextInput,
  TextArea,
  Tooltip,
  Modal,
  Button
} from "~/components/_base";
import RichTextEditor from "../components/RichTextEditor";
import { EditorState } from "draft-js";
import DatePicker from "react-datepicker";
import "../../helpers/styles/datepicker.css";
import { convertToHTML, convertFromHTML } from "draft-convert";
import CurrencyFormat from "react-currency-format";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import UserGlobal from "../../states/userGlobal";
import { LocalHotel, Bathtub, DirectionsCar } from "@material-ui/icons";
import {InputRegex} from "../../helpers/numberInput";
import { faSpinner } from "@fortawesome/free-solid-svg-icons";
import { faTimes, faCamera } from "@fortawesome/free-solid-svg-icons";
 
import { inputRegex } from "~/helpers/numberInput";
import { property_types } from "../../helpers/propertyHelper/propertyHelper";


export default ({ projectName, projectId, toggleAddPropertyModal }) => {
  const [userState, userActions] = UserGlobal();
  const [featuredImages, setFeaturedImages] = useState([]);
  const [state, setState] = useState("");
  const [propertyType, setPropertyType] = useState("");
  const [subPropertyType, setSubPropertyType] = useState("");
  const [ownershipType, setOwnershipType] = useState("");
  const [errors, setErrors] = useState([]);
  const [editorState, setEditorState] = useState(() =>
    EditorState.createEmpty()
  );
  const [floorPlan, setFLoorPlan] = useState([]);
  const [filename, setFilename] = useState("");


  const addPropertyForm = useRef(null);
  const [loading, setLoading] = useState(false);

  const ownership_types = [
    { label: "Owner Occupied", value: "1" },
    { label: "Investment", value: "2" }
  ];

  const sub_property_types = [
    { label: "Apartment", value: "1" },
    { label: "House", value: "2" },
    { label: "House and Land", value: "3" },
    { label: "Land", value: "4" },
    { label: "Townhouse", value: "5" }
  ];

  const states = [
    { label: "New South Wales", value: "New South Wales" },
    { label: "Queensland", value: "Queensland" },
    { label: "South Australia", value: "South Australia" },
    { label: "Tasmania", value: "Tasmania" },
    { label: "Victoria", value: "Victoria" },
    { label: "Western Australia", value: "Western Australia" },
    {
      label: "Australian Capital Territory",
      value: "Australian Capital Territory"
    },
    { label: "Northern Territory", value: "Northern Territory" }
  ];

  const handleClose = e => {
    e.preventDefault();
    toggleAddPropertyModal();
  };

  const handleSubmit = e => {
    e.preventDefault();
    setLoading(true);
    let formData = new FormData(addPropertyForm.current);
    formData.append("role", "project_developer");

    if (floorPlan.length > 0) {
      floorPlan.map(file => {
        formData.append("floorPlan[file]", file);
      });
    } else {
      formData.append("floorPlan", []);
    }

    // formData.append("projectName", projectName);
    formData.append("projectId", userState.projectData.id);
    // formData.append("ownershipType", ownershipType);
    // formData.append("subPropertyType", subPropertyType);
    formData.append("propertyType", propertyType);
    // formData.append("state", state);
    formData.append(
      "description",
      editorState.getCurrentContent().getPlainText()
    );
    formData.append(
      "descriptionHTML",
      convertToHTML(editorState.getCurrentContent())
    );

    featuredImages.map(image => {
      formData.append(`featuredImages[]`, image.image);
    });

    let url = `/api/property/`;
    axios({
      method: "post",
      url,
      data: formData,
      name: "project",
      headers: {
        "content-type": `multipart/form-data`
      }
    })
      .then(r => {
        if (r.status === 200) {
          //  toggleAddPropertyModal();
        }
        setLoading(false);
      })
      .catch(error => {
        setErrors(error.response.data.errors);
        setLoading(false);
      })
      .then(() => {});

    // window.location.href = window.location.href;
  };

  const filterFiles = files => {
    return Object.values(files).filter(file => file.type.includes("image"));
  };

  const handleDrop = e => {
    e.preventDefault();
    e.stopPropagation();
    featuredImages.forEach(file => URL.revokeObjectURL(file));
    setFeaturedImages([
      ...filterFiles(event.dataTransfer.files).map(file => {
        return {
          image: Object.assign(file, { preview: URL.createObjectURL(file) })
        };
      })
    ]);
  };

  const handleFileChange = e => {
    e.preventDefault();
    e.stopPropagation();
    featuredImages.forEach(file => URL.revokeObjectURL(file));
    setFeaturedImages([
      ...filterFiles(event.target.files).map(file => {
        return {
          image: Object.assign(file, { preview: URL.createObjectURL(file) })
        };
      })
    ]);
  };

  const handleFloorPlanChange = e => {
    e.preventDefault();
    setFLoorPlan(
      Object.values(e.target.files).map(file => {
        return Object.assign(file, { resource: URL.createObjectURL(file) });
      })
    );
    setFilename(e.target.files[0]["name"]);
    e.target.value = null;
  };

  const handleDeleteFile = e => {
    e.preventDefault();
    setFLoorPlan([]);
    setFilename("");
    e.target.value = null;
  };

  return (
    <div className={`bg-white rounded-lg py-5 px-8 m-5`}>
      <form ref={addPropertyForm} onSubmit={handleSubmit}>
        <div className={`text-black`}>
          <div className={`py-3 flex justify-start `}>
            <div className={`flex justify-center items-center`}>
              <label className={`w-32`}>Property Type</label>
              <div>
                <Dropdown
                  id="property_type"
                  value={propertyType}
                  options={property_types}
                  onChange={option => {
                    setPropertyType(option.value);
                  }}
                  placeholder="Please select..."
                  name="property_type"
                />

                {errors["propertyType"] && (
                  <span className="px-1 text-red-400 tracking-widest  text-xs ">
                    {errors["propertyType"][0]}
                  </span>
                )}
              </div>
            </div>
            <div className={`ml-4 flex-1 flex justify-center items-center`}>
              <label className={`w-32`}>Property Number</label>
              <div>
                <TextInput
                  className={`border-gray-400 border pl-2 py-1`}
                  border={false}
                  name="unit_name"
                />

                {errors["unit_name"] && (
                  <span className="px-1 text-red-400 tracking-widest  text-xs ">
                    {errors["unit_name"][0]}
                  </span>
                )}
              </div>
            </div>
          </div>

          <div className={`flex justify-start`}>
            <div className={`py-3 flex items-center`}>
              <label
                title={`Number of bedrooms`}
                className={`cursor-pointer mr-4 text-lg`}
              >
                <LocalHotel />
              </label>
              <div>
                <TextInput
                  className={`border-gray-400 border pl-2 py-1`}
                  border={false}
                  name="number_of_bedrooms"
                />

                {errors["number_of_bedrooms"] && (
                  <span className="px-1 text-red-400 tracking-widest  text-xs ">
                    {errors["number_of_bedrooms"][0]}
                  </span>
                )}
              </div>
            </div>
            <div className={`py-3 flex mx-5 items-center`}>
              <label
                title={`Number of bathrooms`}
                className={`cursor-pointer mr-4 text-lg`}
              >
                <Bathtub />
              </label>
              <div>
                <TextInput
                  className={`border-gray-400 border pl-2 py-1`}
                  border={false}
                  name="number_of_bathrooms"
                  onChange={(text) => InputRegex(text)}
                />

                {errors["number_of_bathrooms"] && (
                  <span className="px-1 text-red-400 tracking-widest  text-xs ">
                    {errors["number_of_bathrooms"][0]}
                  </span>
                )}
              </div>
            </div>

            <div className={`py-3 flex items-center`}>
              <label
                title={`Number of garages`}
                className={`cursor-pointer mr-4 text-lg`}
              >
                <DirectionsCar />
              </label>
              <div>
                <TextInput
                  className={`border-gray-400 border pl-2 py-1`}
                  border={false}
                  name="number_of_garages"
                />

                {errors["number_of_garages"] && (
                  <span className="px-1 text-red-400 tracking-widest  text-xs ">
                    {errors["number_of_garages"][0]}
                  </span>
                )}
              </div>
            </div>
          </div>

          <div className={`py-3 flex justify-start items-center`}>
            <label className={`w-12`}>Price</label>
            <div className={``}>
              {/* <TextInput
                className={`border-gray-400 border`}
                border={false}
                name="price"
              /> */}
              <div>
                <span>$</span>
                <CurrencyFormat
                  allowNegative={false}
                  className={` border w-40  py-1`}
                  thousandSeparator={true}
                  name="price"
                  id="price"
                />
              </div>
              {errors["price"] && (
                <span className="px-1 text-red-400 tracking-widest  text-xs ">
                  {errors["price"][0]}
                </span>
              )}
            </div>
          </div>

          <div className={`py-3`}>
            <label>
              <div className="w-full text-sm bg-white text-black ">
                <RichTextEditor
                  title="Property Description"
                  richEditorState={editorState}
                  richEditorStateContent={content => setEditorState(content)}
                  characterLimit={2000}
                ></RichTextEditor>
              </div>
            </label>
            {errors["description"] && (
              <span className="px-1 text-red-400 tracking-widest  text-xs ">
                {errors["description"][0]}
              </span>
            )}
          </div>

          {/* <div className={`py-3 flex justify-center`}>
            <label className={`w-32`}>Sub Property Type </label>
            <div className={``}>
              <Dropdown
                id="sub_property_type"
                value={subPropertyType}
                options={sub_property_types}
                onChange={option => {
                  setSubPropertyType(option.value);
                  
                }}
                placeholder="Sub Property type"
                name="sub_property_type"
              />

              {errors["subPropertyType"] && (
                <span className="px-1 text-red-400 tracking-widest  text-xs ">
                  {errors["subPropertyType"][0]}
                </span>
              )}
            </div>
          </div> */}
          {/* <div className={`py-3 flex justify-center`}>
            <label className={`w-32`}>Ownership Type </label>
            <div>
              <Dropdown
                id="ownership_type"
                value={ownershipType}
                options={ownership_types}
                onChange={option => {
                  setOwnershipType(option.value);
                 
                }}
                placeholder="Ownership type"
                name="ownership_type"
              />

              {errors["ownershipType"] && (
                <span className="px-1 text-red-400 tracking-widest  text-xs ">
                  {errors["ownershipType"][0]}
                </span>
              )}
            </div>
          </div> */}
          {/* <h1 className={``}>Property Address:</h1>
          <div>
            <div className={`py-3 flex justify-center`}>
              <div className={`py-3 flex justify-center`}>
                <label className={`w-32`}>Address Line 1:</label>
                <div>
                  <TextInput
                    className={`border-gray-400 border`}
                    border={false}
                    name="line_1"
                  />
                  {errors["line_1"] && (
                    <span className="px-1 text-red-400 tracking-widest  text-xs ">
                      {errors["line_1"][0]}
                    </span>
                  )}
                </div>
              </div> */}
          {/* <label className={`w-32`}>Address Line 2 </label>
              <TextInput
                className={`border-white border-b`}
                border={false}
                name="line_2"
              /> */}
          {/* <div className={`py-3 flex justify-center`}>
                <label className={`w-32`}>City </label>
                <div>
                  <TextInput
                    className={`border-gray-400 border`}
                    border={false}
                    name="city"
                  />
                  {errors["city"] && (
                    <span className="px-1 text-red-400 tracking-widest  text-xs ">
                      {errors["city"][0]}
                    </span>
                  )}
                </div>
              </div>
              <div className={`py-3 flex justify-center`}>
                <label className={`w-32`}>Suburb </label>
                <div>
                  <TextInput
                    className={`border-gray-400 border`}
                    border={false}
                    name="suburb"
                  />

                  {errors["suburb"] && (
                    <span className="px-1 text-red-400 tracking-widest  text-xs ">
                      {errors["suburb"][0]}
                    </span>
                  )}
                </div>
              </div>
              <div className={`py-3 flex justify-center`}>
                <label className={`w-32`}>State </label>
                <div>
                  <Dropdown
                    id="state"
                    value={state}
                    options={states}
                    onChange={option => {
                      setState(option.value);
                    
                    }}
                    placeholder="Select state"
                    name="state"
                  />

                  {errors["state"] && (
                    <span className="px-1 text-red-400 tracking-widest  text-xs ">
                      {errors["state"][0]}
                    </span>
                  )}
                </div>
              </div>
            </div>
          </div> */}

          <div className={`py-3`}>
            <h1 className="font-semibold"> Featured Images (min. 3 images) </h1>
            <label className="button cursor-pointer" htmlFor="upload-photos">
              <div className="flex justify-center flex-wrap h-auto">
                <div
                  className="w-full"
                  style={{
                    border: "2px dashed #ccc",
                    borderRadius: "20px",
                    padding: "20px",
                    textAlign: "center",
                    marginBottom: "5px"
                  }}
                  onDrop={e => handleDrop(e)}
                  onDragOver={e => {
                    e.preventDefault();
                    e.stopPropagation();
                  }}
                  onDragEnter={e => {
                    e.preventDefault();
                    e.stopPropagation();
                  }}
                  onDragLeave={e => {
                    e.preventDefault();
                    e.stopPropagation();
                  }}
                >
                  Select/Drag some images
                  <input
                    type="file"
                    id="upload-photos"
                    multiple
                    accept="image/*"
                    className="hidden"
                    name="images[]"
                    onChange={e => handleFileChange(e)}
                  />
                  <div className={`flex justify-center flex-wrap`}>
                  
                    {featuredImages.map((featured, index) => {
                      return (
                        <div
                          key={index}
                          className="rounded-sm border m-2"
                          style={{
                            backgroundImage: `url('${featured.image.preview}')`,
                            backgroundPositionX: "center",
                            height: 150,
                            width: 150,
                            backgroundSize: "cover"
                          }}
                        ></div>
                      );
                    })}
                  </div>
                </div>
              </div>
            </label>
            {errors["featuredImages"] && (
              <span className="px-1 text-red-400 tracking-widest  text-xs ">
                {errors["featuredImages"][0]}
              </span>
            )}
          </div>
          <h1 className={`font-semibold`}>Floor Plan </h1>
          <div className={`py-3 flex justify-between items-center`}>
            <div className={`w-1/2`}>
              <label>
                <div
                  className={`cursor-pointer w-full p-3 rounded-lg`}
                  style={{
                    border: "2px dashed #ccc",
                    borderRadius: "20px"
                  }}
                >
                  Select File...
                  <TextInput
                    className={`border-gray-400 border hidden`}
                    border={false}
                    type="file"
                    name="floor_plan"
                    onChange={e => handleFloorPlanChange(e)}
                  />
                </div>
              </label>
              {errors["floorPlan"] && (
                <span className="px-1 text-red-400 tracking-widest  text-xs ">
                  {errors["floorPlan"][0]}
                </span>
              )}
            </div>
            <div> {filename} </div>
            <div className={`text-red-500 text-lg cursor-pointer mr-8`}>
              <FontAwesomeIcon
                onClick={e => handleDeleteFile(e)}
                icon={faTimes}
              />
            </div>
          </div>

          <div className="flex justify-end my-5">
            <Button> Save </Button>
          </div>

          <div className="flex justify-end my-10">
            <Button className={`font-bold rounded-full`} disabled={loading}>
              {loading && (
                <FontAwesomeIcon icon={faSpinner} className={`fa-spin mr-2`} />
              )}
              Continue
            </Button>
          </div>
        </div>
      </form>
    </div>
  );
};
