import React, { useEffect, useState } from "react";
import EditSteps from "../../components/EditSteps";
import EditProjectPart from "./EditProjectPart";
import EditProjectResources from "./EditProjectResources";
import AdminPropertyManager from "./AdminPropertyManager";
import AgencyAgreementPart from "./AgencyAgreementPart";
import { Modal, Button } from "../../components/_base";
import UserGlobal from "../../states/userGlobal";

const SequenceEditingModal = ({
  project,
  toggleSequenceEditModal,
  editToggleFetch
}) => {
  const [userState, userActions] = UserGlobal();
  const [projectView, setProjectView] = useState(true);
  const [resourcesView, setResourcesView] = useState(false);
  const [propertyView, setPropertyView] = useState(false);
  const [agreementView, setAgreementView] = useState(false);

  useEffect(() => {
    userActions.setState({ projectData: project });
    userActions.setState({ currentProjStep: 1 });
  }, []);

  const toggleProjV = () => {
    setProjectView(true);
    setResourcesView(false);
    setPropertyView(false);
    setAgreementView(false);
  };

  const toggleResV = () => {
    setResourcesView(true);
    setPropertyView(false);
    setProjectView(false);
    setAgreementView(false);
  };

  const togglePropView = () => {
    setPropertyView(true);
    setProjectView(false);
    setResourcesView(false);
    setAgreementView(false);
  };

  const toggleAgreeView = () => {
    setPropertyView(false);
    setProjectView(false);
    setResourcesView(false);
    setAgreementView(true);
  };

  return (
    <Modal
      show={true}
      disableBackdropClick={true}
      topOff={true}
      maxWidth={`md`}
      title={`Edit Project`}
      onClose={() => toggleSequenceEditModal()}
    >
      <EditSteps
        activeProjectView={projectView}
        activeResourcesView={resourcesView}
        activePropertyView={propertyView}
        activeAgreementView={agreementView}
        toggleProjectView={() => toggleProjV()}
        toggleResourcesView={() => toggleResV()}
        togglePropertyView={() => togglePropView()}
        toggleAgreementView={() => toggleAgreeView()}
      />
      {projectView && <EditProjectPart toggleFetch={editToggleFetch} />}
      {resourcesView && <EditProjectResources toggleFetch={editToggleFetch} />}
      {propertyView && (
        <AdminPropertyManager
          toggleSEM={() => toggleSequenceEditModal()}
          toggleFetch={editToggleFetch}
          toggleAgreementView={() => toggleAgreeView()} />
      )}
      {agreementView && <AgencyAgreementPart toggleFetch={editToggleFetch}/>}
    </Modal>
  );
};

export default SequenceEditingModal;
