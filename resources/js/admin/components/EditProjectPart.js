import React, { useState, useEffect, useRef } from "react";
import Dropdown from "react-dropdown";
import "../../helpers/styles/dropdown.css";

import "react-toastify/dist/ReactToastify.css";
import moment from "moment";
import { developers } from "../../data/index";

import { TextInput, Tooltip, Button } from "~/components/_base";
import RichTextEditor from "./RichTextEditor";
import { EditorState } from "draft-js";
import DatePicker from "react-datepicker";
import "../../helpers/styles/datepicker.css";
import { convertToHTML, convertFromHTML } from "draft-convert";
import { states } from "../../helpers/propertyHelper/propertyHelper";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faTimes, faPlus } from "@fortawesome/free-solid-svg-icons";
import { faSpinner } from "@fortawesome/free-solid-svg-icons";
import { faCalendarCheck } from "@fortawesome/free-regular-svg-icons";
import UserGlobal from "../../states/userGlobal";
import { isEmpty } from "lodash";
import Geocode from "react-geocode";
import { inputRegex } from "~/helpers/numberInput";
import AddDeveloperModal from "../components/AddDeveloperModal";
import { property_types } from "../../helpers/propertyHelper/propertyHelper";
import { getMapCoordinates } from "../../helpers/mapCoordinateHelper";
import { saveUpdateDeal } from "../../data/index";
import {
  sanitizeFilename,
  imageFileTypeFilter
} from "../../helpers/fileHelper/fileHelper";

import { sweetAlert } from "../../components/_base/alerts/sweetAlert";

const mapKey = `${process.env.MIX_GOOGLE_API_KEY}`;

const EditProjectPart = ({ toggleFetch }) => {
  Geocode.setApiKey(mapKey);

  const [userState, userActions] = UserGlobal();

  const timeLimitData = userState.projectData.expires_at
    ? JSON.parse(userState.projectData.expires_at)
    : null;

  const [land_registration, setLandRegistrationDate] = useState(
    userState.projectData.land_registration
      ? new Date(
          moment(userState.projectData.land_registration).format("YYYY-MM-DD")
        )
      : new Date(Date.now())
  );

  const [proposed_settlement, setDate] = useState(
    userState.projectData.proposed_settlement
      ? new Date(
          moment(userState.projectData.proposed_settlement).format("YYYY-MM-DD")
        )
      : new Date(Date.now())
  );

  const [default_proposed_settlement, setDefaultDate] = useState(
    userState.projectData.proposed_settlement
      ? new Date(
          moment(userState.projectData.proposed_settlement).format("YYYY-MM-DD")
        )
      : new Date(Date.now())
  );

  const [timeLimitOptionValue, setTimeLimitOptionValue] = useState(
    timeLimitData && timeLimitData.option ? parseInt(timeLimitData.option) : 0
  );

  const [timeLimitSelectValue, setTimeLimitSelectValue] = useState(
    timeLimitData && timeLimitData.option ? parseInt(timeLimitData.option) : 0
  );

  const [timeLimitValue, setTimeLimitValue] = useState(
    timeLimitData && timeLimitData.optionVal
      ? parseInt(timeLimitData.optionVal)
      : 0
  );

  const [discount, setDiscount] = useState("");
  const [deposit, setDeposit] = useState(
    userState.projectData.deposit
      ? userState.projectData.deposit.toString()
      : null
  );

  const [featuredImages, setFeaturedImages] = useState([
    ...userState.projectData.featured_images.map(image => {
      return {
        image: image,
        preview: image
      };
    })
  ]);

  const toogleAddUpdateDeveloper = () => {
    setToogleAddEditDeveloper(!toogleAddEditDeveloper);
  };

  const [toogleAddEditDeveloper, setToogleAddEditDeveloper] = useState(false);

  const [loading, setLoading] = useState(false);

  const [fmPlaceHolder, setFmPlaceHolder] = useState([]);
  const [limitError, setLimitError] = useState(false);

  const [projectState, setProjectState] = useState(
    userState.projectData.address.state
  );
  const [displaySuiteState, setDisplaySuiteState] = useState(
    !isEmpty(userState.projectData.display_suite_address) &&
      userState.projectData.display_suite_address.state
      ? userState.projectData.display_suite_address.state
      : ""
  );
  const [propertyType, setPropertyType] = useState(
    String(userState.projectData.sub_property_id)
  );

  const [errors, setErrors] = useState([]);
  const [editorState, setEditorState] = useState(() =>
    EditorState.createEmpty()
  );

  const [editorStateBackup, setEditorStateBackup] = useState(() =>
    EditorState.createEmpty()
  );
  const [projectCountry, setProjectCountry] = useState("Australia");
  const [displaySuiteCountry, setDisplaySuiteCountry] = useState("Australia");
  const addProjectForm = useRef(null);

  const countries = [{ label: "Australia", value: "Australia" }];
  const [project, setProject] = useState(userState.projectData);
  const [sameAddress, setSame] = useState(
    userState.projectData.is_address_same == 1 ? true : false
  );

  const [developerValue, setDeveloperValue] = useState(
    `${userState.projectData.user_id}`
  );

  const [projectDevelopers, setProjectDevelopers] = useState({});

  const [filename, setFilename] = useState("");

  const [previewImage, setPreviewImage] = useState([]);

  const [isProjectLand, setIsProjectLand] = useState(
    userState.projectData && userState.projectData.sub_property_id == 4
  );

  let previewImageRef = useRef(null);

  const time_limit_options = [
    { label: "Please select...", value: 0 },
    { label: "Hour/s", value: "1" },
    { label: "Day/s", value: "2" },
    { label: "Week/s", value: "3" },
    { label: "Month/s", value: "4" },
    { label: "Year/s", value: "5" }
  ];

  const [completedNow, setCompletedNow] = useState(
    userState.projectData.is_completed
  );

  useEffect(() => {
    let phs = [];
    for (let index = 0; index < 12 - featuredImages.length; index++) {
      const ph = {
        classes:
          "cursor-pointer m-2 inline-flex bg-transparent text-palette-gray"
      };
      phs.push(ph);
    }

    setFmPlaceHolder(phs);
  }, [featuredImages.length]);

  useEffect(() => {
    let data = userState.projectData.description;

    setEditorState(() => EditorState.push(editorState, convertFromHTML(data)));
    setEditorStateBackup(() =>
      EditorState.push(editorStateBackup, convertFromHTML(data))
    );

    if (timeLimitData) {
      let currentTimeLimit = time_limit_options.filter((val, i) => {
        return val.value === timeLimitData.option;
      });

      if (currentTimeLimit[0]) {
        setTimeLimitOptionValue(currentTimeLimit[0].value);
      }
      setTimeLimitSelectValue(currentTimeLimit[0]);
    }
  }, []);

  useEffect(() => {
    getDevelopers();
    setDiscount(userState.projectData.discount || "");

    if (userState.projectData.rich_preview_image) {
      let img = [];
      img.push({
        image: userState.projectData.rich_preview_image,
        preview: userState.projectData.rich_preview_image
      });

      let first = "";

      if (
        userState.projectData.rich_preview_image.includes("featured-images")
      ) {
        first = userState.projectData.rich_preview_image.split(
          `/featured-images/`
        )[1];
      }

      if (userState.projectData.rich_preview_image.includes("imgPreview")) {
        first = userState.projectData.rich_preview_image.split(
          `/imgPreview/`
        )[1];
      }

      if (first) {
        let newFilename = first.split("?")[0];
        setFilename(newFilename);
        setPreviewImage(img);
      }
    }
  }, []);

  const getDevelopers = () => {
    const url = `${window.location.origin}/api/user`;
    developers(url)
      .then(result => {
        setProjectDevelopers([
          ...result.data.map(developer => {
            return {
              label: `${developer.first_name} ${developer.last_name}`,
              value: String(developer.id)
            };
          })
        ]);
      })
      .catch(error => {});
  };

  const getDealCoordinates = async ({ formData, url }) => {
    const address = formData.get("project_address_line_1");
    const suburb = formData.get("project_address_suburb");
    const addressParam = address
      .concat(" ", suburb)
      .concat(" ", projectState)
      .concat(" ", projectCountry);

    let data = await getMapCoordinates(addressParam);

    formData.append("long", data.long);
    formData.append("lat", data.lat);

    if (!sameAddress) {
      const address = formData.get("display_suite_address_line_1");
      const suburb = formData.get("display_suite_address_suburb");
      const addressParam = address
        .concat(" ", suburb)
        .concat(" ", displaySuiteState)
        .concat(" ", displaySuiteCountry);

      let data = await getMapCoordinates(addressParam);

      formData.append("display_suite_long", data.long);
      formData.append("display_suite_lat", data.lat);
    }
  };

  const saveDeal = async ({ formData, url }) => {
    await getDealCoordinates({ formData });
    await saveUpdateDeal({ formData, url })
      .then(r => {
        if (r.status === 200) {
          setLoading(false);
          sweetAlert("success", "Project successfully updated!");
          setDefaultDate(proposed_settlement);
          toggleFetch();
        }
      })
      .catch(error => {
        if (error.response.data.errors) {
          sweetAlert("error", "Some fields are missing");
        }
        setLoading(false);
        setErrors(error.response.data.errors);
      })
      .then(() => {});
  };

  const handleSubmit = e => {
    e.preventDefault();
    setLimitError(false);
    saveProject();
  };

  const saveProject = () => {
    if (discount && discount < 10) {
      return sweetAlert(
        "error",
        "Project discount should not be less than 10 percent."
      );
    }

    if (discount && discount > 100) {
      return sweetAlert(
        "error",
        "Project discount should not be more than 100 percent."
      );
    }

    setLoading(true);

    let newDate = new Date(
      moment(userState.projectData.created_at).format("YYYY-MM-DD")
    );

    let formData = new FormData(addProjectForm.current);
    formData.append("role", "project_developer");
    formData.append("propertyType", propertyType);
    formData.append("project_address_state", projectState);
    formData.append("project_address_country", projectCountry);
    formData.append("display_suite_address_state", displaySuiteState);
    formData.append("display_suite_address_country", displaySuiteCountry);
    formData.append("time_limit[optionVal]", timeLimitValue);
    formData.append("time_limit[option]", timeLimitOptionValue);
    formData.append("time_limit[createdAt]", newDate);
    formData.append("developer", developerValue);
    formData.append("project_discount", discount || 0);
    formData.append("deposit", deposit);
    formData.append("isCompleted", completedNow);
    formData.append(
      "description",
      editorState.getCurrentContent().getPlainText()
    );

    formData.append(
      "descriptionHTML",
      convertToHTML(editorState.getCurrentContent())
    );
    featuredImages.forEach((image, key) => {
      formData.append(`featuredImages[${key}]`, image.image);
    });

    if (previewImage.length) {
      formData.append(`preview_img[${0}]`, previewImage[0].image);
    } else {
      formData.append(`preview_img[${0}]`, "");
    }

    formData.append("_method", "PATCH");
    formData.append("same_address", sameAddress);
    formData.append("isPropertyTypeLand", isProjectLand);

    let url = `/api/deal/${userState.projectData.id}`;
    saveDeal({ formData, url });
  };

  const handleDrop = e => {
    e.preventDefault();
    e.stopPropagation();

    const fmCount = featuredImages.length;
    const onCount = e.dataTransfer.files.length;

    if (fmCount + onCount > 12) {
      e.target.value = null;
      setLimitError(true);
      return;
    } else {
      setLimitError(false);
    }

    setFeaturedImages(
      featuredImages.concat([
        ...[...e.dataTransfer.files].map(file => {
          let newFilename = sanitizeFilename(file.name);
          let newFileObj = new File([file], newFilename);
          return {
            image: newFileObj,
            preview: URL.createObjectURL(newFileObj)
          };
        })
      ])
    );
    e.target.value = null;
  };

  const handleChange = e => {
    const fmCount = featuredImages.length;
    const onCount = e.target.files.length;

    if (fmCount + onCount > 12) {
      setLimitError(true);
      e.target.value = null;
      return;
    } else {
      setLimitError(false);
    }

    setFeaturedImages(
      featuredImages.concat([
        ...[...e.target.files].map(file => {
          let newFilename = sanitizeFilename(file.name);
          let newFileObj = new File([file], newFilename);
          return {
            image: newFileObj,
            preview: URL.createObjectURL(newFileObj)
          };
        })
      ])
    );
    e.target.value = null;
  };

  const handlePreviewImageDisplay = e => {
    if (e.target.files.length > 1) {
      sweetAlert("error", "Multiple upload is not allowed!");
      return;
    }

    if (!imageFileTypeFilter(e.target.files[0]["name"])) {
      sweetAlert("error", "Filetype is not supported!");
      e.target.value = null;
      return;
    }

    let imgArr = [];

    setPreviewImage(
      imgArr.concat([
        ...[...e.target.files].map(file => {
          let newFilename = sanitizeFilename(file.name);
          let newFileObj = new File([file], newFilename);
          setFilename(newFilename);
          return {
            image: newFileObj,
            preview: URL.createObjectURL(newFileObj)
          };
        })
      ])
    );
  };

  const handleDeleteFile = e => {
    e.preventDefault();
    setPreviewImage([]);
    previewImageRef.current.type = "";
    previewImageRef.current.type = "file";
    setFilename("");
    e.target.value = null;
  };

  const handleRemove = (e, i) => {
    e.preventDefault();
    e.stopPropagation();
    setFeaturedImages([
      ...featuredImages.filter((image, k) => {
        if (i !== k) {
          return image;
        }
      })
    ]);
    if (featuredImages.length > 12) {
      setLimitError(true);
    } else {
      setLimitError(false);
    }
  };

  const compareDate = selectedDate => {
    let date = moment(selectedDate, "DD-MM-YYYY"); //Date format

    let currDate = moment().format("YYYY-MM-DD");
    let durationHours = date.diff(currDate, "hours");

    let defaultDate = moment(default_proposed_settlement, "DD-MM-YYYY");
    let durationDays = date.diff(defaultDate, "days");

    if (durationDays < 0 && durationHours < 0) {
      return false;
    }
    return true;
  };

  const setSelctedSettlementDate = date => {
    if (isProjectLand) {
      setLandRegistrationDate(date);
    } else {
      if (!compareDate(date)) {
        sweetAlert("error", "Please select future date!");
      } else {
        setDate(date);
      }
    }
  };

  return (
    <div className={`bg-white rounded-lg py-5 px-8 m-5`}>
      <form ref={addProjectForm} onSubmit={handleSubmit}>
        <div className={`text-black`}>
          <div className={`flex items-center`}>
            <div className={`flex items-center`}>
              <label className={`block py-3 font-semibold w-40`}>
                Project Developer
              </label>
              <div className={`relative`}>
                {!isEmpty(projectDevelopers) ? (
                  <Dropdown
                    id="project_developer"
                    value={developerValue}
                    options={projectDevelopers}
                    onChange={option => {
                      setDeveloperValue(option.value);
                    }}
                    placeholder="Please select..."
                    name="project_developer"
                  />
                ) : (
                  <FontAwesomeIcon
                    icon={faSpinner}
                    className={`fa-spin mr-2`}
                  />
                )}
              </div>
              {errors["developer"] && (
                <span className="absolute px-1 text-red-400 tracking-widest  text-xs ">
                  {errors["developer"][0]}
                </span>
              )}
            </div>
            {!loading && (
              <div className={`flex`}>
                <div className="flex justify-end ml-10">
                  <span
                    className={`font-semibold text-palette-purple cursor-pointer`}
                    disabled={loading}
                    onClick={() => toogleAddUpdateDeveloper()}
                  >
                    <FontAwesomeIcon icon={faPlus} className={`fa-plus mr-2`} />
                    Add Project Developer
                  </span>
                </div>
              </div>
            )}
          </div>

          <div className={`py-3 flex items-center `}>
            <label className={`font-semibold w-1/5`} htmlFor={`name`}>
              Project Name
            </label>
            <div className="flex-1 ">
              <TextInput
                id={`name`}
                defaultValue={userState.projectData.name}
                className={`border border-gray-400 px-2 py-1 w-20 py-1`}
                border={false}
                name="name"
              />
              {errors["name"] && (
                <span className="px-1 text-red-400 tracking-widest  text-xs ">
                  {errors["name"][0]}
                </span>
              )}
            </div>
          </div>

          <div className={`py-3`}>
            <div className="w-full text-sm bg-white text-black ">
              {editorStateBackup.getCurrentContent().getPlainText() && (
                <RichTextEditor
                  title="Project Description"
                  richEditorState={editorState}
                  richEditorStateContent={content => setEditorState(content)}
                  characterLimit={2000}
                />
              )}
            </div>

            {errors["description"] && (
              <span className="px-1 text-red-400 tracking-widest  text-xs ">
                {errors["description"][0]}
              </span>
            )}
          </div>
          <div className={`flex justify-between`}>
            <div className={`flex flex-start items-center`}>
              <label className={`block py-3 w-32`}>Project Type</label>
              <div className={`relative`}>
                <Dropdown
                  id="property_type"
                  value={propertyType}
                  options={property_types}
                  onChange={option => {
                    if (option.label === "Land") {
                      setIsProjectLand(true);
                    } else {
                      setIsProjectLand(false);
                    }
                    setPropertyType(option.value);
                  }}
                  placeholder="Please select..."
                  name="property_type"
                />

                {errors["propertyType"] && (
                  <span className="absolute px-1 text-red-400 tracking-widest  text-xs ">
                    {errors["propertyType"][0]}
                  </span>
                )}
              </div>
            </div>

            <div className={`px-10 flex flex-start items-center `}>
              <label className={`block pr-5 py-3`}>Discount</label>
              <div className={`relative`}>
                <div className={`flex items-center`}>
                  <TextInput
                    className={`pl-2 text-sm border-gray-400 w-32 border py-1`}
                    border={false}
                    type={`number`}
                    // name={`project_discount`}
                    value={discount}
                    onChange={e =>
                      inputRegex(e.target.value)
                        ? setDiscount(e.target.value)
                        : null
                    }
                    //name="time_limit_value"
                  />
                  <span className={`font-extrabold pl-3`}> % </span>
                </div>

                {errors["project_discount"] && (
                  <span className="absolute px-1 text-red-400 tracking-widest  text-xs ">
                    {errors["project_discount"][0]}
                  </span>
                )}
              </div>
            </div>
          </div>

          <div className={`flex flex-start items-center mt-3`}>
            <label className={`block py-3 w-32`}>Deposit</label>
            <div className={`relative flex items-center`}>
              <Dropdown
                id="deposit"
                value={deposit}
                options={[
                  { label: "5", value: "5" },
                  { label: "10", value: "10" }
                ]}
                onChange={option => {
                  setDeposit(option.value);
                }}
                placeholder="Please select..."
                name="deposit"
              />
              <span className={`font-extrabold pl-3`}> % </span>
              {errors["deposit"] && (
                <span className="absolute px-1 text-red-400 tracking-widest  text-xs ">
                  {errors["deposit"][0]}
                </span>
              )}
            </div>
          </div>

          <h1 className={`font-semibold mt-5`}>Project Address</h1>
          <div className="w-full">
            <div className="py-3 flex w-full items-center">
              <label className={`w-32`}>Address Line 1</label>
              <div className=" flex-1 relative">
                <TextInput
                  defaultValue={userState.projectData.address.line_1}
                  className={`border text-sm border-gray-400 px-2 py-1 w-20`}
                  border={false}
                  name="project_address_line_1"
                />
                {errors["project_address_line_1"] && (
                  <span className="absolute px-1 text-red-400 tracking-widest  text-xs ">
                    {errors["project_address_line_1"][0]}
                  </span>
                )}
              </div>
            </div>

            <div className={`py-3 flex justify-between`}>
              <div className={`flex items-center`}>
                <label className={`w-32 block py-3`}>Suburb </label>
                <div style={{ width: "320px" }} className="flex-1 relative">
                  <TextInput
                    className={` border-gray-400 border py-1 pl-2`}
                    border={false}
                    name="project_address_suburb"
                    defaultValue={userState.projectData.address.suburb}
                  />

                  {errors["project_address_suburb"] && (
                    <span className="absolute px-1 text-red-400 tracking-widest  text-xs ">
                      {errors["project_address_suburb"][0]}
                    </span>
                  )}
                </div>
              </div>

              {!isProjectLand && (
                <div className={`ml-5 flex items-center`}>
                  <label className={`w-32 block py-3`}>City </label>
                  <div className={`relative`}>
                    <TextInput
                      className={` border-gray-400 border pl-2 py-1`}
                      border={false}
                      name="project_address_city"
                      defaultValue={userState.projectData.address.city}
                    />
                    {errors["project_address_city"] && (
                      <span className="absolute px-1 text-red-400 tracking-widest  text-xs ">
                        {errors["project_address_city"][0]}
                      </span>
                    )}
                  </div>
                </div>
              )}
            </div>

            <div className={`py-3 flex justify-between`}>
              <div className={`py-3 flex items-center`}>
                <label className={`w-32 block py-3`}>State </label>
                <div className={`relative`}>
                  <Dropdown
                    id="project_address_state"
                    value={projectState}
                    options={states}
                    onChange={option => {
                      setProjectState(option.value);
                    }}
                    placeholder="Select state"
                    name="project_address_state"
                  />
                  {errors["project_address_state"] && (
                    <span className="absolute px-1 text-red-400 tracking-widest  text-xs ">
                      {errors["project_address_state"][0]}
                    </span>
                  )}
                </div>
              </div>
              {!isProjectLand && (
                <div className={`ml-5 flex items-center`}>
                  <label className={`w-32 block py-3`}>Postcode/ZIP </label>
                  <div className={`relative`}>
                    <TextInput
                      className={` border-gray-400 border pl-2 py-1`}
                      border={false}
                      name="project_address_zip"
                      defaultValue={userState.projectData.address.postal}
                    />
                    {errors["project_address_zip"] && (
                      <span className="absolute px-1 text-red-400 tracking-widest  text-xs ">
                        {errors["project_address_zip"][0]}
                      </span>
                    )}
                  </div>
                </div>
              )}
            </div>

            <div className={`py-3 flex items-center`}>
              <label className={`w-32 block py-3`}>Country </label>
              <div className={`relative`}>
                <Dropdown
                  id="project_address_country"
                  value={projectCountry}
                  options={countries}
                  onChange={option => {
                    setProjectCountry(option.value);
                  }}
                  name="project_address_country"
                />

                {errors["project_address_country"] && (
                  <span className="absolute px-1 text-red-400 tracking-widest  text-xs ">
                    {errors["project_address_country"][0]}
                  </span>
                )}
              </div>
            </div>
          </div>
          <div className="flex justify-between mt-5">
            <h1 className={`font-semibold`}>
              {isProjectLand ? "Office" : "Display Suite"} Address
            </h1>
            <div className="flex justify-between items-center">
              <input
                type="checkbox"
                name="same_address_check"
                id="same_address"
                onChange={e => {
                  e.target.checked ? setSame(true) : setSame(false);
                }}
                checked={sameAddress}
              />
              <span className={`p-1`}>
                {isProjectLand ? "Office" : "Display Suite"} Address is same as
                Project Address
              </span>
            </div>
          </div>
          {!sameAddress && (
            <div>
              <div className="py-3 flex w-full items-center">
                <label className={`w-32`}>Address Line 1</label>
                <div className="flex-1 relative">
                  <TextInput
                    defaultValue={
                      userState.projectData.display_suite_address.line_1
                    }
                    className={`border text-sm border-gray-400 px-2 py-1 w-20`}
                    border={false}
                    name="display_suite_address_line_1"
                  />
                  {errors["display_suite_address_line_1"] && (
                    <span className="absolute px-1 text-red-400 tracking-widest  text-xs ">
                      {errors["display_suite_address_line_1"][0]}
                    </span>
                  )}
                </div>
              </div>

              {/* <div className={`py-3 flex justify-between`}>
                <div className={`flex items-center`}>
                  <label className={`w-32 block py-3`}>Suburb </label>
                  <div className={`relative`} style={{ width: "320px" }}>
                    <TextInput
                      defaultValue={
                        userState.projectData.display_suite_address.suburbs
                      }
                      className={`border text-sm border-gray-400 px-2 py-1 w-20`}
                      border={false}
                      name="display_suite_address_suburb"
                    />
                    {errors["display_suite_address_suburb"] && (
                      <span className="absolute px-1 text-red-400 tracking-widest  text-xs ">
                        {errors["display_suite_address_suburb"][0]}
                      </span>
                    )}
                  </div>
                </div>
                <div className={`flex items-center`}>
                  <label className={`w-32 block py-3`}>Postcode/ZIP </label>
                  <div className={`relative`}>
                    <TextInput
                      defaultValue={
                        userState.projectData.display_suite_address.postal
                      }
                      className={`border text-sm border-gray-400 px-2 py-1 w-20`}
                      border={false}
                      name="display_suite_address_zip"
                    />
                    {errors["display_suite_address_zip"] && (
                      <span className="absolute px-1 text-red-400 tracking-widest  text-xs ">
                        {errors["display_suite_address_zip"][0]}
                      </span>
                    )}
                  </div>
                </div>
              </div> */}

              <div className={`py-3 flex justify-between`}>
                <div className={`flex items-center`}>
                  <label className={`w-32 block py-3`}>Suburb </label>
                  <div style={{ width: "320px" }} className="flex-1 relative">
                    <TextInput
                      className={` border-gray-400 border py-1 pl-2`}
                      border={false}
                      name="display_suite_address_suburb"
                      defaultValue={
                        userState.projectData.display_suite_address.suburb
                      }
                    />
                    {errors["display_suite_address_suburb"] && (
                      <span className="absolute px-1 text-red-400 tracking-widest  text-xs ">
                        {errors["display_suite_address_suburb"][0]}
                      </span>
                    )}
                  </div>
                </div>
                
                {!isProjectLand && (
                  <div className={`ml-5 flex items-center`}>
                    <label className={`w-32 block py-3`}>City </label>
                    <div className={`relative`}>
                      <TextInput
                        className={` border-gray-400 border pl-2 py-1`}
                        border={false}
                        name="display_suite_address_city"
                        defaultValue={
                          userState.projectData.display_suite_address.city
                        }
                      />
                      {errors["display_suite_address_city"] && (
                        <span className="absolute px-1 text-red-400 tracking-widest  text-xs ">
                          {errors["display_suite_address_city"][0]}
                        </span>
                      )}
                    </div>
                  </div>
                )}
              </div>

              <div className={`py-3 flex justify-between`}>
                <div className={`py-3 flex items-center`}>
                  <label className={`w-32 block py-3`}>State </label>
                  <div className={`relative`}>
                    <Dropdown
                      id="display_suite_address_state"
                      value={displaySuiteState}
                      options={states}
                      onChange={option => {
                        setDisplaySuiteState(option.value);
                      }}
                      placeholder="Select state"
                      name="display_suite_address_state"
                    />
                    {errors["display_suite_address_state"] && (
                      <span className="absolute px-1 text-red-400 tracking-widest  text-xs ">
                        {errors["display_suite_address_state"][0]}
                      </span>
                    )}
                  </div>
                </div>

                {!isProjectLand && (
                  <div className={`ml-5 flex items-center`}>
                    <label className={`w-32 block py-3`}>Postcode/ZIP </label>
                    <div className={`relative`}>
                      <TextInput
                        className={` border-gray-400 border pl-2 py-1`}
                        border={false}
                        name="display_suite_address_zip"
                        defaultValue={
                          userState.projectData.display_suite_address.postal
                        }
                      />
                      {errors["display_suite_address_zip"] && (
                        <span className="absolute px-1 text-red-400 tracking-widest  text-xs ">
                          {errors["display_suite_address_zip"][0]}
                        </span>
                      )}
                    </div>
                  </div>
                )}
              </div>

              <div className={`py-3 flex items-center`}>
                <label className={`w-32 block py-3`}>Country </label>
                <div className={`relative`}>
                  <Dropdown
                    id="display_suite_address_country"
                    value={displaySuiteCountry}
                    options={countries}
                    onChange={option => {
                      setDisplaySuiteCountry(option.value);
                    }}
                    name="display_suite_address_country"
                  />

                  {errors["display_suite_address_country"] && (
                    <span className="absolute px-1 text-red-400 tracking-widest  text-xs ">
                      {errors["display_suite_address_country"][0]}
                    </span>
                  )}
                </div>
              </div>
            </div>
          )}

          <div className={`py-3 flex`}>
            <div className={`flex-1`}>
              <div className={`cursor-pointer flex-1`}>
                <h1 className="font-semibold mb-4">
                  Estimated Completion/
                  {isProjectLand ? "Land Registration" : "Settlement"}
                </h1>
                <FontAwesomeIcon className={`mr-2`} icon={faCalendarCheck} />
                <DatePicker
                  className={`py-1 px-2`}
                  id={`proposed_settlement`}
                  // name={`proposed_settlement`}
                  showMonthDropdown={true}
                  showYearDropdown={true}
                  dateFormat={`PP`}
                  selected={
                    isProjectLand ? land_registration : proposed_settlement
                  }
                  disabled={completedNow}
                  onSelect={date => {
                    setSelctedSettlementDate(date);
                  }}
                  onChange={date => {
                    setSelctedSettlementDate(date);
                  }}
                />
              </div>
              <div className="p-1 flex items-center mt-3">
                <input
                  type="checkbox"
                  id="completed_now"
                  checked={completedNow}
                  onChange={e => {
                    setCompletedNow(e.target.checked);
                  }}
                />
                <span className={`p-1`}>Completed Now</span>
              </div>
            </div>

            <div className={`cursor-pointer flex-1`}>
              <h1 className="font-semibold mb-4">Project Time Limit</h1>
              <div className={`relative flex`}>
                <div className={`pr-6`}>
                  <Dropdown
                    className={"project_time_limit"}
                    id="time_limit_option"
                    value={timeLimitOptionValue}
                    options={time_limit_options}
                    placeholder="Please select..."
                    onChange={option => {
                      setTimeLimitOptionValue(option.value);
                    }}
                    name="time_limit_option_value"
                  />

                  {errors["timeLimitValue"] && (
                    <span className="absolute px-1 text-red-400 tracking-widest  text-xs ">
                      {errors["timeLimitValue"][0]}
                    </span>
                  )}
                </div>

                <div>
                  <TextInput
                    className={`pl-2 text-sm border-gray-400 w-32 border py-1`}
                    border={false}
                    value={timeLimitValue}
                    onChange={e =>
                      inputRegex(e.target.value)
                        ? setTimeLimitValue(e.target.value)
                        : null
                    }
                    //name="time_limit_value"
                  />
                  {errors["time_limit_value"] && (
                    <span className="absolute px-1 text-red-400 tracking-widest  text-xs ">
                      {errors["time_limit_value"][0]}
                    </span>
                  )}
                </div>
              </div>
            </div>
          </div>

          <div className={`py-3`}>
            <h1 className="font-semibold"> Featured Images (min. 3 images) </h1>
            <label className="button " htmlFor="upload-photos">
              <div className={``}>
                <div
                  className="w-full flex flex-wrap  cursor-pointer"
                  style={{
                    border: "2px dashed #ccc",
                    borderRadius: "20px",
                    padding: "20px",
                    textAlign: "center",
                    marginBottom: "0px"
                  }}
                  onDrop={e => handleDrop(e)}
                  onDragOver={e => {
                    e.preventDefault();
                    e.stopPropagation();
                  }}
                  onDragEnter={e => {
                    e.preventDefault();
                    e.stopPropagation();
                  }}
                  onDragLeave={e => {
                    e.preventDefault();
                    e.stopPropagation();
                  }}
                >
                  <input
                    type="file"
                    id="upload-photos"
                    accept="image/*"
                    multiple
                    className="hidden"
                    name="image[]"
                    onChange={e => handleChange(e)}
                  />

                  {featuredImages.map((image, index) => {
                    return (
                      <div
                        key={index}
                        className="rounded-sm border m-2 relative"
                        style={{
                          backgroundImage: `url('${image.preview}')`,
                          backgroundPositionX: "center",
                          width: "6.9em",
                          height: "8.9rem",
                          backgroundSize: "cover"
                        }}
                      >
                        <div
                          className={`absolute bg-palette-blue-light font-bold leading-none ml-1 mt-1 rounded-full text-white w-1/4`}
                        >
                          {index + 1}
                        </div>
                        <Tooltip title={`Remove this item`}>
                          <span
                            onClick={e => handleRemove(e, index)}
                            className={`absolute bottom-0 right-0 p-2 -mb-2 `}
                          >
                            <FontAwesomeIcon
                              className={`text-red-500 shadow-xl hover:font-bold hover:text-red-700`}
                              icon={faTimes}
                            />
                          </span>
                        </Tooltip>
                      </div>
                    );
                  })}
                  {fmPlaceHolder.map((p, index) => {
                    return (
                      <div
                        key={index}
                        className={`${p.classes} cursor-pointer flex justify-center items-center`}
                        style={{
                          width: "6.9rem",
                          height: "8.9rem",
                          border: "2px solid #ccc"
                        }}
                      >
                        <div
                          className={`absolute font-bold leading-none w-1/4`}
                        >
                          {index + featuredImages.length + 1}
                        </div>
                      </div>
                    );
                  })}
                </div>
                {limitError && (
                  <span className="px-1 text-red-400 tracking-widest  text-xs ">
                    Please limit your photos to twelve(12) items only.
                  </span>
                )}
              </div>
            </label>
            {errors["featuredImages"] && (
              <span className="px-1 text-red-400 tracking-widest  text-xs ">
                {errors["featuredImages"][0]}
              </span>
            )}
          </div>

          <div className="flex flex-col mb-10">
            <div className={`flex-1`}>
              <h1 className={`font-semibold`}>Rich Preview Image </h1>
              <div className={`py-3 flex flex-col`}>
                <div className={`flex`}>
                  <label className={`w-full`}>
                    <div
                      className={`cursor-pointer w-full p-3 rounded-lg flex`}
                      style={{
                        border: "2px dashed #ccc",
                        borderRadius: "20px"
                      }}
                    >
                      <input
                        id="rtp"
                        ref={previewImageRef}
                        type="file"
                        name="prev_image"
                        onChange={e => handlePreviewImageDisplay(e)}
                        className={`border text-sm border-gray-400 px-2 py-1 w-full py-1 hidden`}
                      />
                      <label
                        htmlFor="rtp"
                        className={`cursor-pointer hover:text-palette-blue-light ${
                          filename ? "text-palette-blue-light" : ""
                        }`}
                      >
                        {filename || "Select File"}
                      </label>
                      {filename && (
                        <div
                          className={`cursor-pointer ml-4 mr-8 text-lg text-red-500`}
                        >
                          <FontAwesomeIcon
                            onClick={e => handleDeleteFile(e)}
                            icon={faTimes}
                          />
                        </div>
                      )}
                    </div>
                  </label>
                  <div className={`flex items-center justify-end w-1/2`}>
                    <Button
                      className={`font-bold rounded-full`}
                      disabled={loading}
                    >
                      {loading && (
                        <FontAwesomeIcon
                          icon={faSpinner}
                          className={`fa-spin mr-2`}
                        />
                      )}
                      Continue
                    </Button>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </form>
      {toogleAddEditDeveloper && (
        <AddDeveloperModal
          title={`Add Developer`}
          developer={{}}
          toogleAddDeveloper={() => toogleAddUpdateDeveloper()}
        ></AddDeveloperModal>
      )}
    </div>
  );
};

export default EditProjectPart;
