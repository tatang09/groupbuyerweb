import React, { useState, useRef } from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

import UserGlobal from "../../states/userGlobal";
import { resources } from "../../helpers/resourceHelper/index";
import { saveUpdateResources } from "../../data/index";

import { Button } from "~/components/_base";

import ResourcesData from "../../components/resources/ResourcesData";
import LandResourcesData from "../../components/resources/LandResourcesData";

import { sweetAlert } from "../../components/_base/alerts/sweetAlert";

const AddProjectResources = () => {
  const [userState, userActions] = UserGlobal();
  const [projectResources, setProjectResources] = useState([]);
  const [loading, setLoading] = useState(false);
  const resourceForm = useRef(null);
  const [builderProfile, setBuilderProfile] = useState("file");
  const [builderProfileLink, setBuilderProfileLink] = useState("");
  const [arcProfile, setArcProfile] = useState("file");
  const [arcProfileLink, setArcProfileLink] = useState("");
  const [devProfile, setDevProfile] = useState("file");
  const [devProfileLink, setDevProfileLink] = useState("");
  const [projectVideo, setProjectVideo] = useState("");
 
  const tableHeaders = [
    {
      label: "Project Resources",
      style: "",
      width: "w-1/2"
    },
    {
      label: "Document Name",
      style: "text-center",
      width: "w-4/12"
    },
    {
      label: "Delete",
      style: "text-center",
      width: "w-1/12"
    }
  ];

  const handleSubmit = e => {
    e.preventDefault();
    saveProjectResources();
  };

  const saveProjectResources = async () => {
    const formData = new FormData(resourceForm.current);

    projectResources.forEach((resource, key) => {
      if (arcProfile === "text" && resource.name === "architect_profile") {
        return;
      }
      if (builderProfile === "text" && resource.name === "builder_profile") {
        return;
      }

      if (devProfile === "text" && resource.name === "developer_profile") {
        return;
      }

      formData.append(`resources[${key}][file]`, resource.file);
      formData.append(`resources[${key}][name]`, resource.name);
    });

    if (devProfileLink) {
      formData.append(
        "devProfileLink",
        resources.appendHttpsToResource(devProfileLink)
      );
    }

    if (arcProfileLink) {
      formData.append(
        "arcProfileLink",
        resources.appendHttpsToResource(arcProfileLink)
      );
    }

    if (builderProfileLink) {
      formData.append(
        "builderProfileLink",
        resources.appendHttpsToResource(builderProfileLink)
      );
    }

    setLoading(true);

    formData.append("id", userState.projectData.id);
    formData.append("name", userState.projectData.name);
    formData.append("projectVideo", projectVideo);

    let url = `/api/project-resource/upload`;

    await saveUpdateResources({ formData, url })
      .then(r => {
        if (r.status === 200) {
          setLoading(false);
          sweetAlert("success", "Project resources successfully added.");
          userActions.setState({ projectData: r.data });
          userActions.setState({ currentProjStep: 3 });
        }
      })
      .catch(err => {
        setLoading(false);
      })
      .then(() => {});
  };

  const handleFileChange = (e, resource) => {
    e.preventDefault();

    let c = projectResources.filter(file => {
      if (file["name"] !== resource) {
        return file;
      }
    });

    if (e.target.files.length > 0) {
      setProjectResources(
        c.concat(resources.trimFilename(e.target.files[0], resource))
      );
      e.target.value = null;
    }
  };

  const handleDelete = (e, resource) => {
    e.preventDefault();
    setProjectResources(
      projectResources.filter(file => {
        return file["name"] !== resource;
      })
    );
    e.target.value = null;
  };

  const getFileName = resource => {
    let filename = "";
    projectResources.forEach(file => {
      if (file["name"] === resource) {
        if (typeof file["file"].name == "string") {
          filename = file["file"]["name"];
        } else {
          const first = file["file"].split(`/${resource}/`)[1];
          filename = first.split("?")[0];
        }
      }
    });
    return filename;
  };

  const changeInputType = (rowName, checked) => {
    if (rowName === "Builder Profile") {
      if (checked) {
        setBuilderProfile("text");
      } else {
        setBuilderProfileLink("");
        setBuilderProfile("file");
      }
    }

    if (rowName === "Developer Profile") {
      if (checked) {
        setDevProfile("text");
      } else {
        setDevProfileLink("");
        setDevProfile("file");
      }
    }

    if (rowName === "Architect Profile") {
      if (checked) {
        setArcProfile("text");
      } else {
        setArcProfileLink("");
        setArcProfile("file");
      }
    }
  };

  const setCheckedState = row => {
    let retval = false;
    if (row.label === "Builder Profile") {
      if (builderProfile === "text") {
        retval = true;
      }
    }

    if (row.label === "Developer Profile") {
      if (devProfile === "text") {
        retval = true;
      }
    }

    if (row.label === "Architect Profile") {
      if (arcProfile === "text") {
        retval = true;
      }
    }
    return retval;
  };

  return (
    <div className={`bg-white rounded-lg py-5 px-8 m-5`}>
      <form ref={resourceForm} onSubmit={handleSubmit}>
        <table className={`w-full`}>
          <thead className={`w-full`}>
            <tr>
              {tableHeaders.map((header, key) => {
                return (
                  <th
                    key={key}
                    className={` ${header.style} border-b font-bold px-4 py-2 text-palette-gray px-4 py-2 ${header.width} `}
                  >
                    {header.label}
                  </th>
                );
              })}
            </tr>
          </thead>
          <tbody className={`w-full`}>
            {userState.projectData &&
            userState.projectData.sub_property_id == "4" ? (
              <LandResourcesData
                devProfile={devProfile}
                getFileName={getFileName}
                setCheckedState={setCheckedState}
                changeInputType={changeInputType}
                handleFileChange={handleFileChange}
                handleDelete={handleDelete}
                setProjectVideo={setProjectVideo}
                setDevProfileLink={setDevProfileLink}
              />
            ) : (
              <ResourcesData
                builderProfile={builderProfile}
                devProfile={devProfile}
                arcProfile={arcProfile}
                getFileName={getFileName}
                setCheckedState={setCheckedState}
                changeInputType={changeInputType}
                handleFileChange={handleFileChange}
                handleDelete={handleDelete}
                setProjectVideo={setProjectVideo}
                setDevProfileLink={setDevProfileLink}
              />
            )}
          </tbody>
        </table>
        <div className="flex justify-end my-5">
          <Button className={`font-bold rounded-full`} disabled={loading}>
            {loading && (
              <FontAwesomeIcon
                icon={["fas", "spinner"]}
                className={`fa-spin mr-2`}
              />
            )}
            Continue
          </Button>
        </div>
      </form>
    </div>
  );
};

export default AddProjectResources;
