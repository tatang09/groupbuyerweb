import React from "react";
import useGlobalHook from "use-global-hook";

const actions = {
  setState: (store, payload) => {
    Object.keys(payload).forEach(key => {
      store.setState({ [key]: payload[key] });
    });
  }
};


const initialState = {
  user: null,
  deals: [],
  featuredDeals: [],
  weeklyDeal: null,
  showSignIn: false,
  showSignUp: false,
  showSellerApply: false,
  showSecureDeal: false,
  showTermsAndConditions: false,
  showPrivacyPolicy: false,
  showMenu: false,
  fetch: false,
  showSocialMedia: false,
  showFAQ: false,
  userCount: 0,
  showSalesAdvice: false,
  projectData: [],
  currentProject: [],
  propertiesData: [],
  currentProjStep: 1,
  windowSize: 0,
  adminDrawerOpen: true,
  aveSavings: 0
};

const UserGlobal = useGlobalHook(React, initialState, actions);

export default UserGlobal;
