import React from "react";

import axios from "axios";
import { isLoggedIn } from "../../services/auth";

export const sendContactFormEmail = async ({ formData }) => {
  let url = "https://groupbuyer.activehosted.com/proc.php";
  return await axios({
    method: "post",
    url,
    data: formData,
    mode: "no-cors"
  });
};
