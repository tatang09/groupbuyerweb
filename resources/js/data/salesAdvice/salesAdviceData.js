import React from "react";

import axios from "axios";
import { isLoggedIn } from "../../services/auth";

export const salesAdviceAdditionalInfo = async ({ additionalInfoFormData }) => {
    let url = "/api/additional-info";
  return await axios({
    method: "post",
    url,
    data: additionalInfoFormData,
    name: "property",
    headers: { Authorization: "Bearer " + isLoggedIn() }
  });
};

export const salesAdviceSolicitorInfo = async ({ solicitorFormData }) => {
    let url = "/api/solicitor-detail";
    return await axios({
      method: "post",
      url,
      data: solicitorFormData,
      name: "property",
      headers: { Authorization: "Bearer " + isLoggedIn() }
    });
  };

  export const salesAdvicePurchaserInfo = async ({ purchaserFormData }) => {
    let url = "/api/purchaser";
    return await axios({
      method: "post",
      url,
      data: purchaserFormData,
      name: "property",
      headers: { Authorization: "Bearer " + isLoggedIn() }
    });
  };
  
