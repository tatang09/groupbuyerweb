import React from "react";

import axios from "axios";

export const developers = async url => {
  return await axios({
    method: "get",
    url,
    params: {
      role: "project_developers"
    }
  });
};

export const getDeveloperById = async userId => {
  return await axios.get(`/api/developer/${userId}`);
}
 