import React from "react";

import axios from "axios";

export const fetchBanks = async({url}) => {
  return await axios({
    method: "get",
    url: url
  });
};
 