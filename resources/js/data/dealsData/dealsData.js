 
import { ArrowForwardIosTwoTone } from "@material-ui/icons";
import axios from "axios";
import { isLoggedIn } from "../../services/auth";

export const saveUpdateDeal = async ({ url, formData }) => {
  return await axios({
    method: "post",
    url,
    data: formData,
    name: "project",
    headers: { Authorization: "Bearer " + isLoggedIn() }
  });
};

export const saveUpdateResources = async ({ url, formData }) => {
  return await axios({
    method: "post",
    url,
    data: formData,
    headers: {
      "content-type": `multipart/form-data`
    }
  });
};

export const getDealByID = async ({ url }) => {
  return await axios.get(`${url}`);
};

export const getFeaturedDeals = async () => {
  return await axios.get(`/api/deal?featured=true`);
}

export const getAverageSavings = async () => {
  return await axios.get(`/api/average-savings`);
}

export const getUserCount = async () => {
  return await axios.get(`/api/user-count`);
}
