import React from "react";

import { developers } from "./developersData/developersData";
import {
  saveUpdateDeal,
  saveUpdateResources,
  getDealByID
} from "./dealsData/dealsData";
import { saveUpdateProperty } from "./propertiesData/propertiesData";
import {
  salesAdviceAdditionalInfo,
  salesAdviceSolicitorInfo,
  salesAdvicePurchaserInfo
} from "./salesAdvice/salesAdviceData";
import { sendContactFormEmail } from "./activeCampaignData/activeCampaignData";
import { fetchBanks } from "./banksData/banksData";

export {
  salesAdviceAdditionalInfo,
  developers,
  saveUpdateDeal,
  saveUpdateResources,
  saveUpdateProperty,
  getDealByID,
  salesAdviceSolicitorInfo,
  salesAdvicePurchaserInfo,
  sendContactFormEmail,
  fetchBanks
};
