import React from "react";

import axios from "axios";

export const sendResetPasswordLink = async({url, email}) => {
  return await axios({
    method: "post",
    url: url,
    data: {email: email}
  });
};

export const resetPassword = async({url, param}) => {
  return await axios({
    method: "post",
    url: url,
    data: param
  });
};
 