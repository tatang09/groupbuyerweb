import React from "react";

import axios from "axios";
import { isLoggedIn } from "../../services/auth";

export const saveUpdateProperty = async ({ formData, url }) => {
  return await axios({
    method: "post",
    url,
    data: formData,
    name: "property",
    headers: { Authorization: "Bearer " + isLoggedIn() }
  });
};

export const requestBookingInspection = async ({formData}) => {
  let url = "/api/book-inspection";
  return await axios.get(url, {
    params: {
      name: formData.name,
      phone: formData.phone,
      email: formData.email,
      message: formData.message,
      propertyId: formData.propertyId,
      id: formData.dealId,
    }
  });
}
