import { LOADING_STATUS, PROCCESSING } from "~/actions/loading"

export default (state = { isProcessing: false, status: `Loading...` }, action) => {
  switch (action.type) {
    case PROCCESSING:
      return Object.assign({}, state, { isProcessing: action.isProcessing })
    case LOADING_STATUS:
      return Object.assign({}, state, { status: action.status })
    default:
      return state
  }
}
