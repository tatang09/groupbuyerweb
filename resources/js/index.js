import React, { useEffect } from "react";
import ReactDOM from "react-dom";
import { useRoutes } from "hookrouter";
import { Provider } from "react-redux";
import store from "./store";

import { library } from "@fortawesome/fontawesome-svg-core";
import { fab } from "@fortawesome/free-brands-svg-icons";
import { fas } from "@fortawesome/free-solid-svg-icons";
import UserGlobal from "~/states/userGlobal";

import Routes from "./routes";

library.add(fab, fas);

const App = () => {
  const [userState, userAction] = UserGlobal();
  const routeResult = useRoutes(Routes);

  useEffect(() => {
    userAction.setState({ windowSize: window.screen.width });
  }, []);

  return (
    <>
      <Provider store={store}>{routeResult}</Provider>
    </>
  );
};

ReactDOM.render(<App />, document.getElementById("app"));
