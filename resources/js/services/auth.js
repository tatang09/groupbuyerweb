export const setToken = (token) => localStorage.setItem('token', token)
export const isLoggedIn = () => localStorage.getItem('token')
export const setUserRole = (role) => localStorage.setItem('role', role)
export const userRole = () => localStorage.getItem('role')
export const logout = callback => {
  setToken('')
  window.location = '/'
  callback()
}
