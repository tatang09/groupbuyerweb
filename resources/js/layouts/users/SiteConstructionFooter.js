import React, { useState } from "react";
import { ContactForm, SocialMedia } from "~/components";
import UserGlobal from "~/states/userGlobal";
import { isMobile } from "react-device-detect";
import moment from "moment";
import * as deviceSizes from "../../helpers/deviceSizeHelper";
import { map } from "jquery";

const SiteConstructionFooter = ({ topFooter }) => {
 
    return (
        <footer name={`footer`} className={`bg-palette-black-main`}>
            {topFooter && (
                <>
                    {/* Top Footer */}
                    <div className={`bg-palette-black-main mx-auto pt-6`} style={{ maxWidth: 1366 }}>
                        <div className={`flex flex-col sm:flex-row pb-8 pt-2 px-8`}>
                            <div className={`flex flex-col items-center justify-center pb-8 px-8 w-full`}>
                                <div className={`mb-4 sm:flex-1`}>
                                    <img
                                        src="/assets/svg/groupguyer_logo_negative.svg"
                                        style={{ height: 30 }}
                                    />
                                </div>
                                <div className={`sm:flex-1`}>
                                    <div className={`flex flex-col items-center justify-center md:flex-row sm:mt-2`}>
                                        <p className={`text-gray-500 whitespace-no-wrap text-xs`}>{`© GroupBuyer 2021 | Founded by Ben Daniel`}</p>
                                        <p className={`text-gray-500 visible ml-1 text-xs md:block hidden`}>{`| Designed by TOAST CREATIVE`}</p>
                                        <p className={`text-gray-500 visible text-xs md:hidden block`}>{`Designed by TOAST CREATIVE`}</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </>
            )}
        </footer>
    );
};

export default SiteConstructionFooter;
