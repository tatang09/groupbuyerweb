import React, { useState } from "react";
import { ContactForm, SocialMedia } from "~/components";
import UserGlobal from "~/states/userGlobal";
import { isMobile } from "react-device-detect";
import moment from "moment";
import * as deviceSizes from "../../helpers/deviceSizeHelper";
import { map } from "jquery";

const Footer = ({ topFooter }) => {
  const [userState, userAction] = UserGlobal();

  const siteMapArr = [
    "What is GroupBuyer?",
    "How it works?",
    "Properties",
    "Developers",
    "FAQ"
  ];

  const connectWithUsArr = [
    { label: "info@groupbuyer.com.au", value: "info@groupbuyer.com.au" },
    { label: "1300 031 835", value: "1300 031 835" },
    { label: "Facebook", value: "" },
    { label: "Instagram", value: "" },
    { label: "LinkedIn", value: "" },
  ];

  const addressArr = [
    "Level 27, Bond St.",
    "Sydney, NSW",
    "Australia",
    "2000"
  ];

  const quickLinksArr = [
    { label: "Terms & Condition", value: "" },
    { label: "Privacy Policy", value: "" },
  ];

  const siteMap = () => {
    return siteMapArr.map((item, key) => {
      return <span key={key} className={`pb-1 ${key == 0 ? "whitespace-no-wrap" : ""}`}>{item}</span>
    });
  }

  const connectWithUs = () => {
    return connectWithUsArr.map((item, key) => {
      return <span key={key} className={`pb-1`}>{item.label}</span>
    });
  }

  const address = () => {
    return addressArr.map((item, key) => {
      return <span key={key} className={`pb-1`}>{item}</span>
    });
  }

  const quickLinks = () => {
    return quickLinksArr.map((item, key) => {
      return <span key={key} className={`pb-1`}>{item.label}</span>
    });
  }

  return (
    <footer name={`footer`} className={`bg-palette-black-main`}>
      {topFooter && (
        <>
          {/* Top Footer */}
          <div className={`relative flex flex-col lg:px-56 items-center bg-palette-black-main mx-auto pt-6`} style={{ maxWidth: 1366 }}>
            <div className={`w-full flex flex-col sm:flex-row pb-8 pt-2 px-8`}>
              <div className={`flex sm:flex-1 sm:mt-8`}>
                <div className={`flex-1`}>
                  <div className={`mb-2`}>
                    <span className={`text-sm text-white`}>Sitemap</span>
                  </div>
                  <div className={`flex flex-col text-gray-500`}>
                    {siteMap()}
                  </div>
                </div>
                <div className={`flex flex-1 justify-end sm:items-center visible sm:invisible`}>
                  <img src={`/assets/images/chevron-up.png`}
                    style={{ height: 40 }} />
                </div>
              </div>
              <div className={`mt-8 sm:flex-1`}>
                <div className={`mb-2`}>
                  <span className={`text-sm text-white`}>Connect with us</span>
                </div>
                <div className={`flex flex-col text-gray-500`}>
                  {connectWithUs()}
                </div>
              </div>
              <div className={`mt-8 sm:flex-1 sm:ml-5 ml-0`}>
                <div className={`mb-2`}>
                  <span className={`text-sm text-white`}>Address</span>
                </div>
                <div className={`flex flex-col text-gray-500`}>
                  {address()}
                </div>
              </div>
              <div className={`mt-8 sm:flex-1`}>
                <div className={`mb-2`}>
                  <span className={`text-sm text-white`}>Quick links</span>
                </div>
                <div className={`flex flex-col text-gray-500`}>
                  {quickLinks()}
                </div>
              </div>
            </div>
            <div className={`w-full flex flex-col sm:flex-row pb-8 px-8`}>
              <div className={`mb-4 sm:flex-1`}>
                <img
                  src="/assets/svg/groupguyer_logo_negative.svg"
                  style={{ height: 30 }}
                />
              </div>
              <div className={`sm:flex-1`}>
                <div className={`sm:mt-2`}>
                  <span className={`text-gray-500 whitespace-no-wrap text-xs`}>© GroupBuyer 2021 | Founded by Ben Daniel <span className={`invisible sm:visible`}>| Designed by TOAST CREATIVE</span></span>
                </div>
                <div>
                  <span className={`text-gray-500 visible sm:invisible text-xs`}>Designed by TOAST CREATIVE</span>
                </div>
              </div>
            </div>
            <img className={`absolute right-0 mr-16 mt-24 invisible sm:visible`} src={`/assets/images/chevron-up.png`} style={{ height: 40 }} />
          </div>
        </>
      )}
    </footer>
  );
};

export default Footer;
