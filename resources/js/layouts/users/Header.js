import React, { useState, useRef } from "react";
import { A, usePath } from "hookrouter";

import UserGlobal from "~/states/userGlobal";
import WebViewHeader from "../../components/headers/WebViewHeader";
import MobileViewHeader from "../../components/headers/MobileViewHeader";
import SiteConstructionHeader from "../../components/headers/SiteConstructionHeader";
import { isMobile } from "react-device-detect";


const Header = () => {

  const [userState, userAction] = UserGlobal();

  const renderHeader = () => {
    if (!isMobile) {
      return <WebViewHeader />
    }
    return <MobileViewHeader />
  }

  return (
    <header
      style={{ maxWidth: 1920 }}
      className={"absolute w-full"}
    >
      <div className={`flex flex-col items-center justify-center w-full lg:justify-between mx-auto px-8 py-6 relative z-50`} style={{ background: "linear-gradient(rgb(0 0 0 / 80%), rgb(0 0 0 / 0%))" }}>
        <div className={`flex items-center w-full`}>

          <a href={`/`} className={`flex-1`}>
            <img
              className={`main-header-logo`}
              src="/assets/images/logo_negative.png"
              style={{ height: isMobile ? 40 : 50 }}
            />
          </a>

          <SiteConstructionHeader />

          {/* {window.location.pathname === "/under-construction"
            ? <SiteConstructionHeader /> : renderHeader() 
          } */}

        </div>
      </div>
    </header>
  );
};

export default Header;