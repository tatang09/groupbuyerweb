import React, { useState } from "react";
import { ContactForm, SocialMedia } from "~/components";
import UserGlobal from "~/states/userGlobal";
import { isMobile } from "react-device-detect";
import moment from "moment";
import * as deviceSizes from "../../helpers/deviceSizeHelper";

const FooterOld = ({ topFooter }) => {
  const [userState, userAction] = UserGlobal();

  const gBuyerContactDetail = {
    address: "Level 27, 20 Bond St, Sydney NSW Australia 2000",
    poBox: "1300 031 835",
    email: "info@groupbuyer.com.au"
  };

  return (
    <footer name={`footer`} className={`bg-palette-blue-dark`}>
      {topFooter && (
        <>
          {/* Top Footer */}
          <div className={`mx-auto pt-4`} style={{ maxWidth: 1366 }}>
            <div className="px-4 pt-16 pb-2 mx-auto text-white bg-palette-blue-dark lg:flex lg:px-16">
              <div className="flex-col lg:pr-16 lg:w-5/12">
                <div>
                  <h2 className="mb-4 text-3xl font-bold text-center lg:text-left lg:text-5xl lg:mb-8">
                    Questions?
                  </h2>
                  <p className="px-4 mb-8 text-lg text-center lg:px-0 lg:text-left">
                    Check out our FAQ page&nbsp;
                    <a
                      href="/frequently-asked-questions"
                      className={`text-palette-teal underline`}
                    >
                      here
                    </a>
                    &nbsp; for all frequently asked questions or feel free to
                    get in touch directly with one of our team members.
                  </p>
                  <h2 className="mb-2 text-xl text-center lg:block lg:text-left">
                    Connect with us
                  </h2>
                  <SocialMedia
                    className={`lg:flex flex-row mb-6 lg:justify-start justify-center`}
                    size={`2x`}
                  />
                </div>

                <div className="text-lg lg:mb-0 lg:py-8">
                  <div className={`lg:text-left text-center`}>
                    {gBuyerContactDetail.address}
                  </div>
                  <div
                    className={`font-bold flex flex-col lg:flex-row lg:text-left text-center`}
                  >
                    <span>
                      P:&nbsp;
                      <a
                        className="text-blue-500 hover:text-blue-300"
                        href="tel:1300031835"
                      >
                        {gBuyerContactDetail.poBox}
                      </a>
                    </span>
                    <span className="hidden lg:inline">&nbsp;•&nbsp;</span>
                    <span>
                      E:&nbsp;
                      <a
                        className="text-blue-500 hover:text-blue-300"
                        href="mailto:info@groupbuyer.com.au"
                      >
                        {gBuyerContactDetail.email}
                      </a>
                    </span>
                  </div>
                </div>
              </div>

              <div className="flex-col lg:text-left lg:w-7/12">
                <ContactForm
                  backgroundColor={isMobile ? "transparent" : "#000024"}
                />
              </div>
            </div>
          </div>
        </>
      )}

      {/* Bottom Footer */}
      <div
        style={{ maxWidth: 1366 }}
        className={`flex flex-col flex-wrap items-center justify-center lg:flex-row lg:px-16 mx-auto px-4 lg:py-16 text-gray-500`}
      >
        {!isMobile && (
          <div className="flex items-center lg:inline-block lg:py-12 lg:w-1/4">
            <a href={`/terms-and-conditions`}>
              <span
                className="mr-4 text-sm transition-all duration-300 cursor-pointer hover:text-palette-teal"
              // onClick={() => userAction.setState({ showTermsAndConditions: true })}
              >
                Terms &amp; Conditions
              </span>
            </a>

            <a href={`/privacy-policy`}>
              <span
                className="text-sm transition-all duration-300 cursor-pointer hover:text-palette-teal"
              // onClick={() => userAction.setState({ showPrivacyPolicy: true })}
              >
                Privacy Policy
              </span>
            </a>
          </div>
        )}

        {!isMobile && (
          <div className="flex flex-col flex-wrap items-center justify-center py-4 text-xs lg:py-0 lg:flex-row lg:w-1/2">
            <span className={`text-sm`}>
              © GroupBuyer {moment().format("YYYY")}&nbsp;|&nbsp;
            </span>
            {/* {!isMobile ? "|" : ""} */}
            <span>&nbsp;</span>
            <span className={`text-sm`}>
              <span>
                Founded by <strong>&nbsp;Ben Daniel&nbsp;</strong>
              </span>
              {/*| &nbsp; Developed by
              <a
                className={`hover:text-palette-teal`}
                href="https://fligno.com/"
                target="_blank"
              >
                <strong>&nbsp;Fligno&nbsp;</strong>|
              </a> */}
            </span>
          </div>
        )}

        {/* {isMobile && userState.windowSize < deviceSizes.baseLarge && ( */}
        {isMobile && (
          <>
            {/* <div className="flex items-center lg:inline-block lg:py-12 lg:w-1/4">
              <a href={`/terms-and-conditions`}>
                <span
                  className="mr-4 text-sm transition-all duration-300 cursor-pointer hover:text-palette-teal"
                  // onClick={() => userAction.setState({ showTermsAndConditions: true })}
                >
                  Terms &amp; Conditions
                </span>
              </a>

              <a href={`/privacy-policy`}>
                <span
                  className="text-sm transition-all duration-300 cursor-pointer hover:text-palette-teal"
                  // onClick={() => userAction.setState({ showPrivacyPolicy: true })}
                >
                  Privacy Policy
                </span>
              </a>
            </div> */}
            <div className="flex flex-col flex-wrap items-center justify-center py-4 text-xs lg:py-0 lg:flex-row lg:w-1/2">
              <div className="flex items-center lg:inline-block lg:py-12 lg:w-1/4 mb-1">
                <a href={`/terms-and-conditions`}>
                  <span
                    className="mr-4 text-sm transition-all duration-300 cursor-pointer hover:text-palette-teal"
                  // onClick={() => userAction.setState({ showTermsAndConditions: true })}
                  >
                    Terms &amp; Conditions
                  </span>
                </a>

                <a href={`/privacy-policy`}>
                  <span
                    className="text-sm transition-all duration-300 cursor-pointer hover:text-palette-teal"
                  // onClick={() => userAction.setState({ showPrivacyPolicy: true })}
                  >
                    Privacy Policy
                  </span>
                </a>
              </div>
              <span className={`text-sm mb-1`}>
                © GroupBuyer {moment().format("YYYY")}&nbsp;|&nbsp;
                <span>
                  Founded by <strong>Ben Daniel</strong>&nbsp;
                </span>
              </span>
              {/* {!isMobile ? "|" : ""} */}
              <span className={`mvFooterText`}>
                {/* Developed by
                <a
                  className={`hover:text-palette-teal`}
                  href="https://fligno.com/"
                  target="_blank"
                >
                  <strong>&nbsp;Fligno&nbsp;</strong>
                </a>                  */}
              </span>
            </div>
          </>
        )}

        <div className="items-center hidden lg:inline-block lg:py-12 lg:w-1/4">
          <SocialMedia className={`justify-end`} variant={`secondary`} />
        </div>
      </div>
    </footer>
  );
};

export default FooterOld;
