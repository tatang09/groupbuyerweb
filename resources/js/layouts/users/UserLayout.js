import React, { useEffect, useState } from "react";
import Header from "./Header";
import Footer from "./Footer";
import SiteConstructionFooter from "./SiteConstructionFooter";

import {
  SignIn,
  SignUp,
  SellerApply,
  TermsAndConditionsModal,
  ScrollArrow,
  SalesAdviceModal,
  MemberCount
} from "~/components";
import { isLoggedIn } from "~/services/auth";
import { axios } from "~/helpers/bootstrap";
import UserGlobal from "~/states/userGlobal";

import { usePath } from "hookrouter";

import { isMobile } from "react-device-detect";
import {
  getFeaturedDeals,
  getUserCount
} from "../../data/dealsData/dealsData";

const UserLayout = ({ children, topFooter = true }) => {
  const [userState, userAction] = UserGlobal();

  const currentPath = usePath();

  const userCount = () => {
    getUserCount().then(res => {
      userAction.setState({ userCount: res.data });
    });
  };

  const featuredDeals = () => {
    getFeaturedDeals().then(res => {
      userAction.setState({ featuredDeals: res.data.data });
    });
  };




  useEffect(() => {
    featuredDeals();
    userCount();

  }, []);

  useEffect(() => { }, [currentPath]);

  useEffect(() => {
    const handleResize = () => {
      userAction.setState({ windowSize: window.screen.width });
    };
    window.addEventListener("resize", handleResize);
    return () => window.removeEventListener("resize", handleResize);
  }, []);

  useEffect(() => {
    const getUser = async () => {
      if (!isLoggedIn()) {
        userAction.setState({ user: null });
      } else {
        let { data } = await axios.get(
          `/api/me`,
          userState.user === null
            ? {
              headers: {
                Authorization: "Bearer " + isLoggedIn()
              }
            }
            : null
        );

        userAction.setState({ user: data.data });
      }
    };

    getUser();
  }, [userState.fetch]);

  const handleClose = () => {
    userAction.setState({
      showSignIn: false,
      showSignUp: false,
      showSellerApply: false,
      showSecureDeal: false,
      showTermsAndConditions: false,
      showSalesAdvice: false
    });
  };
  


  return (
    <>
      <main className={`relative`}>
        <Header />
        {children}

      </main>

      {window.location.pathname === "/under-construction"
        ? <SiteConstructionFooter topFooter={topFooter} /> : <Footer topFooter={topFooter} />
      }
 

      {/* <SignIn handleClose={() => handleClose()} />
      <SignUp handleClose={() => handleClose()} />
      <SellerApply handleClose={() => handleClose()} />
      <TermsAndConditionsModal handleClose={() => handleClose()} /> */}
    </>
  );
};

export default UserLayout;
