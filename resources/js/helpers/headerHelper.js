
export const links = [
    {
        name: "What is GroupBuyer?",
        route: "/",
        show: true
    },
    {
        name: "How it works?",
        route: "/how-it-works",
        show: true
    },
    {
        name: "Properties",
        route: "/weekly-deals",
        show: true
    },
    {
        name: "Developers",
        route: "/developers",
        show: true
    },
    {
        name: "Contact Us",
        route: "/contact",
        show: true
    },
    {
        name: "FAQ",
        route: "/frequently-asked-questions",
        show: true
    }
];

 