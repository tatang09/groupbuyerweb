import React from "react";

const mapResources = (arr, name) => {
  return arr.find(element => element.name === name);
};

export const resourcesArr = resourceArr => {
  return [
    {
      name: "project_brochure",
      label: "Project Brochure",
      link: resourceArr ? mapResources(resourceArr, "project_brochure") : null
    },
    {
      name: "executive_summary",
      label: "Executive summary",
      link: resourceArr ? mapResources(resourceArr, "executive_summary") : null
    },
    {
      name: "schedule_of_finishes",
      label: "Schedule of finishes",
      link: resourceArr
        ? mapResources(resourceArr, "schedule_of_finishes")
        : null
    },
    {
      name: "developer_profile",
      label: "Developer profile",
      link: resourceArr ? mapResources(resourceArr, "developer_profile") : null
    },
    {
      name: "builder_profile",
      label: "Builder profile",
      link: resourceArr ? mapResources(resourceArr, "builder_profile") : null
    },
    {
      name: "architect_profile",
      label: "Architect profile",
      link: resourceArr
        ? mapResources(resourceArr, "architect_profile")
        : null
    },
    {
      name: "project_video",
      label: "Project video",
      link: resourceArr
        ? mapResources(resourceArr, "project_video")
        : null
    },
    {
      name: "floor_plates",
      label: "Floor plates",
      link: resourceArr ? mapResources(resourceArr, "floor_plates") : null
    },
    {
      name: "floor_plan",
      label: "Floor plan",
      link: resourceArr ? mapResources(resourceArr, "floor_plan") : null
    },
    {
      name: "master_contract",
      label: "Master contract",
      link: resourceArr ? mapResources(resourceArr, "master_contract") : null
    },
    {
      name: "strata_plan",
      label: "Strata plan",
      link: resourceArr
        ? mapResources(resourceArr, "project_strata_plan")
        : null
    },
    {
      name: "rental_estimate",
      label: "Rental estimate",
      link: resourceArr ? mapResources(resourceArr, "rental_estimate") : null
    },
    {
      name: "depreciation_schedule",
      label: "Depreciation schedule",
      link: resourceArr ? mapResources(resourceArr, "depreciation_schedule") : null
    },
    {
      name: "marketing_contract",
      label: "Marketing Contract",
      link: resourceArr ? mapResources(resourceArr, "marketing_contract") : null
    },
    {
      name: "subdivision_plan",
      label: "DP/Subdivision Plan",
      link: resourceArr ? mapResources(resourceArr, "subdivision_plan") : null
    },
    {
      name: "lot_plan",
      label: "Lot Plan",
      link: resourceArr ? mapResources(resourceArr, "lot_plan") : null
    },
    {
      name: "draft_88b",
      label: "Draft 88b",
      link: resourceArr ? mapResources(resourceArr, "draft_88b") : null
    },
    {
      name: "draft_contours",
      label: "Draft Contours",
      link: resourceArr ? mapResources(resourceArr, "draft_contours") : null
    },
    {
      name: "draft_sewer_diagram",
      label: "Draft Sewer Diagram",
      link: resourceArr ? mapResources(resourceArr, "draft_sewer_diagram") : null
    },
    {
      name: "build_envelope_plan",
      label: "Build Envelope Plan",
      link: resourceArr ? mapResources(resourceArr, "build_envelope_plan") : null
    },
    {
      name: "estate_guidelines",
      label: "Estate Guidelines",
      link: resourceArr ? mapResources(resourceArr, "estate_guidelines") : null
    },
    {
      name: "project_brochure",
      label: "Project Brochure",
      link: resourceArr ? mapResources(resourceArr, "project_brochure") : null
    },
    {
      name: "location_map",
      label: "Location Map",
      link: resourceArr ? mapResources(resourceArr, "location_map") : null
    },
    {
      name: "other",
      label: "Other",
      link: resourceArr ? mapResources(resourceArr, "other") : null
    }
  ];
};

export const trimFilename = (file, resource) => {
  let selectedFile = file;
  let newFilename = selectedFile.name.replace(/[^.a-zA-Z0-9]/g, "");
  let newFileObj = new File([selectedFile], newFilename);
  return {
    name: resource,
    file: newFileObj
  };
};

export const appendHttpsToResource = (link) => {
  let baseString = "https://"
  return link.includes('https://') ? link  : baseString.concat("", link);
}

export const trimFloorPlan = (file) => {
  let selectedFile = file;
  let newFilename = selectedFile.name.replace(/[^.a-zA-Z0-9]/g, "");
  return new File([selectedFile], newFilename);
}
