import React from "react";

export const tableRows = [
  {
    label: "Executive Summary",
    resource: "executive_summary",
    fileName: "",
    icon: "times"
  },
  {
    label: "Schedule of Finishes",
    resource: "schedule_of_finishes",
    fileName: "",
    icon: "times"
  },
  {
    label: "Developer Profile",
    resource: "developer_profile",
    fileName: "",
    icon: "times"
  },
  {
    label: "Builder Profile",
    resource: "builder_profile",
    fileName: "",
    icon: "times"
  },
  {
    label: "Project Video",
    resource: "project_video",
    fileName: "",
    icon: "times"
  },
  {
    label: "Architect Profile",
    resource: "architect_profile",
    fileName: "",
    icon: "times"
  },
  {
    label: "Floor Plates",
    resource: "floor_plates",
    fileName: "",
    icon: "times"
  },
  {
    label: "Master Contract",
    resource: "master_contract",
    fileName: "",
    icon: "times"
  },
  {
    label: "Project Brochure",
    resource: "project_brochure",
    fileName: "",
    icon: "times"
  },
  {
    label: "Project Strata Plan",
    resource: "project_strata_plan",
    fileName: "",
    icon: "times"
  },
  {
    label: "Rental Estimate",
    resource: "rental_estimate",
    fileName: "",
    icon: "times"
  },
  {
    label: "Depreciation Schedule",
    resource: "depreciation_schedule",
    fileName: "",
    icon: "times"
  }
];


export const landTableRows = [
  {
    label: "Developer Profile",
    resource: "developer_profile",
    fileName: "",
    icon: "times"
  },
  {
    label: "Marketing Contract",
    resource: "marketing_contract",
    fileName: "",
    icon: "times"
  },
  {
    label: "DP/Subdivision Plan",
    resource: "subdivision_plan",
    fileName: "",
    icon: "times"
  },
  {
    label: "Lot Plan",
    resource: "lot_plan",
    fileName: "",
    icon: "times"
  },
  {
    label: "Draft 88b",
    resource: "draft_88b",
    fileName: "",
    icon: "times"
  },
  {
    label: "Draft Contours",
    resource: "draft_contours",
    fileName: "",
    icon: "times"
  },
  {
    label: "Draft Sewer Diagram",
    resource: "draft_sewer_diagram",
    fileName: "",
    icon: "times"
  },
  {
    label: "Build Envelope Plan",
    resource: "build_envelope_plan",
    fileName: "",
    icon: "times"
  },
  {
    label: "Estate Guidelines",
    resource: "estate_guidelines",
    fileName: "",
    icon: "times"
  },
  {
    label: "Project Brochure",
    resource: "project_brochure",
    fileName: "",
    icon: "times"
  },
  {
    label: "Location Map",
    resource: "location_map",
    fileName: "",
    icon: "times"
  },
  {
    label: "Project Video",
    resource: "project_video",
    fileName: "",
    icon: "times"
  },
  {
    label: "Other",
    resource: "other",
    fileName: "",
    icon: "times"
  }
];

export const tableHeaders = [
  {
    label: "Project Resources",
    style: "",
    width: "w-1/2"
  },
  {
    label: "Document Name",
    style: "text-center",
    width: "w-4/12"
  },
  {
    label: "Delete",
    style: "text-center",
    width: "w-1/12"
  }
];
