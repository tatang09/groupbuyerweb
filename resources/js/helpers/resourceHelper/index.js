import * as resources  from "./resources";
import { tableRows, tableHeaders, landTableRows } from "./tableHelper";


export { resources, tableRows, tableHeaders, landTableRows };
