import React from 'react'

 export const sanitizeFilename = (filename) => {
    return filename.replace(/[^.a-zA-Z0-9]/g, "");
 }

 export const imageFileTypeFilter = (filename) => {
   return filename.match(/\.(jpg|jpeg|png|gif)$/);
 }

 export const PDFFileTypeFilter = (filename) => {
   return filename.match(/\.(pdf)$/);
 }