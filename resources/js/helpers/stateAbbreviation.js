import React from 'react';
import {size, words} from 'lodash';

export const stateAbbreviation = state => {
  if (!state) return;
  state = state.toLowerCase();
  let stateAbbr;
  switch (state) {
    case 'new south wales':
      stateAbbr = 'NSW';
      break;
    case 'queensland':
      stateAbbr = 'QLD';
      break;
    case 'south australia':
      stateAbbr = 'SA';
      break;
    case 'tasmania':
      stateAbbr = 'TAS';
      break;
    case 'victoria':
      stateAbbr = 'VIC';
      break;
    case 'australian capital territory':
      stateAbbr = 'ACT';
      break;
    case 'northern territory':
      stateAbbr = 'NT';
      break;
    case 'wester australia':
      stateAbbr = 'WA';
      break;
    default:
      stateAbbr = state.toUpperCase();
      break;
  }
  return stateAbbr;
};

export const shortIntro = desc => {
  if (!desc) return;
  let initialStr = desc.split('</strong>')[0];
  let wordCount = _.size(_.words(initialStr));

  if (wordCount > 23) {
    return (
      initialStr
        .split(' ')
        .slice(0, 19)
        .join(' ') + '...'
    );
  } else {
    return initialStr;
  }
};
