import React from "react";
import Geocode from "react-geocode";

export const getMapCoordinates = async (addressParam) => {
  const coordinates = { lat: "", long: "" };
  const mapKey = `${process.env.MIX_GOOGLE_API_KEY}`;
  Geocode.setApiKey(mapKey);

  await Geocode.fromAddress(addressParam).then(
    response => {
      const { lat, lng } = response.results[0].geometry.location;
      coordinates.lat = lat;
      coordinates.long = lng;
    },
    error => {}
  );

  return coordinates;
};
