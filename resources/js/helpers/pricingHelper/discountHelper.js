import React from "react";

import CurrencyFormat from "react-currency-format";

export const discountedPrice = (
  discount_is_percentage,
  discount,
  property_price
) => {
  const newPrice = discount_is_percentage
    ? property_price - property_price * discount
    : property_price - discount;
 
  return (
    <CurrencyFormat
      value={newPrice}
      displayType={"text"}
      thousandSeparator={true}
      prefix={"$"}
    />
  );
};

export const discountedPriceRange = (project, isMax) => {
  let discountedPrice = 0;

  if (project.properties.length == 0) {
    return discountedPrice;
  }

  if (isMax) {
    let max = project.properties.reduce(
      (a, b) => (b["price"] > a ? b["price"] : a),
      project.properties[0].price
    );
    discountedPrice = max - max * (project.discount / 100);
  } else {
    let min = project.properties.reduce(
      (a, b) => (b["price"] < a ? b["price"] : a),
      project.properties[0].price
    );
    discountedPrice = min - min * (project.discount / 100);
  }
  return discountedPrice;
};

export const prices = [
  50000,
  100000,
  150000,
  200000,
  250000,
  300000,
  350000,
  400000,
  450000,
  500000,
  550000,
  600000,
  650000,
  700000,
  750000,
  800000,
  850000,
  900000,
  950000,
  1000000,
  1100000,
  1200000,
  1300000,
  1400000,
  1500000,
  1600000,
  1700000,
  1800000,
  1900000,
  2000000,
  2100000,
  2200000,
  2300000,
  2400000,
  2500000,
  2600000,
  2700000,
  2800000,
  2900000,
  3000000,
  4000000,
  5000000,
  6000000,
  7000000,
  8000000,
  9000000,
  10000000,
  11000000,
  12000000,
  13000000,
  14000000,
  15000000
];
