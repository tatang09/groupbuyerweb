import { SignalCellularNullSharp } from "@material-ui/icons";
import React from "react"

export const ownership_types = [
  { label: "Owner Occupied", value: "1" },
  { label: "Investment", value: "2" }
];

export const sub_property_types = [
  { label: "Apartment", value: "1" },
  { label: "House", value: "2" },
  { label: "House & Land", value: "3" },
  { label: "Land", value: "4" },
  { label: "Townhouse", value: "5" }
];
export const property_types = [
  { label: "Apartment", value: "1" },
  { label: "House", value: "2" },
  { label: "House & Land", value: "3" },
  { label: "Land", value: "4" },
  { label: "Townhouse", value: "5" }
];

export const states = [
  { label: "Please select", value: "" },
  { label: "NSW", value: "NSW" },
  { label: "QLD", value: "QLD" },
  { label: "SA", value: "SA" },
  { label: "TAS", value: "TAS" },
  { label: "VIC", value: "VIC" },
  { label: "WA", value: "WA" },
  {
    label: "ACT",
    value: "ACT"
  },
  { label: "NT", value: "NT" }
];

export const costDueDates = [
  { label: "Please select...", value: "" },
  { label: "Annual", value: "Annual" },
  { label: "Quarterly", value: "Quarterly" }
];


export const floorPlanHelper = (fileParam) => {
  return Object.values(fileParam).map(file => {
    let newFilename = file.name.replace(/[^.a-zA-Z0-9]/g, "");
    let newFileObj = new File([file], newFilename);
    return Object.assign(newFileObj, { resource: URL.createObjectURL(newFileObj) });
  });
}

 
