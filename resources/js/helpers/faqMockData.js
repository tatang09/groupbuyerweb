import React from "react"

export const dummyFAQ = [
    {
        id: 0,
        question: '1. How is GroupBuyer able to discount properties so much?',
        answer:
            'GroupBuyer lists properties in bulk and therefore pre-negotiate selling prices where buyers can then purchase them individually. Our strong relationship with property developers allow us to do this.',
        isOpen: false
    },
    {
        id: 1,
        question: '2. How much is the holding deposit to secure a property deal?',
        answer:
            'Each property deal costs $1,000 to secure. You are welcome to secure multiple property deals but they need to be purchased individually.',
        isOpen: false
    },
    {
        id: 2,
        question: '3. How do I pay my holding deposit?',
        answer:
            'You can pay your holding deposit via Credit Card right here on GroupBuyer.com.au.',
        isOpen: false
    },
    {
        id: 3,
        question: '4. Is my holding deposit refundable?',
        answer:
            'Your $1,000 holding deposit is fully refunded when your property contract is exchanged unconditionally with a GroupBuyer agent.',
        isOpen: false
    },
    {
        id: 4,
        question:
            '5. What if I change my mind after I pay my holding deposit?',
        answer:
            'That\'s OK. If you change your mind before the project is finalised (sold out) your holding deposit is refunded in full. However, if you change your mind after your project is finalised you will forfeit your holding deposit.',
        isOpen: false
    },
    {
        id: 5,
        question: '6. Is my discount guaranteed once I pay my holding deposit?',
        answer:
            'Yes. GroupBuyer records your online purchase and displays full details in your Member Profile under \'Secured deals\'. ',
        isOpen: false
    },
    {
        id: 6,
        question: '7. I missed out buying the property deal I wanted. What can I do?',
        answer:
            'We\'re sorry to hear that! You can either secure a different property deal in the project you like OR alternatively you can send us a Message and we\'ll put you on the \'Waiting list\' and notify you should the buyer pull out. Further, a GroupBuyer team member can discuss similar upcoming properties suitable to you. ',
        isOpen: false
    },
    {
        id: 7,
        question: '8. Do I have to share my project on social media to claim a discount?',
        answer:
            'No. But once you purchase your property deal online we encourage you to share your project on social media to ensure the project time limit does not expire prior to your project being finalised (sold out).',
        isOpen: false
    },
    {
        id: 8,
        question: '9. What happens if a project\'s time limit expires?',
        answer:
            'If the time limit expires on a project the GroupBuyer offer is no longer valid. It\'s then at the developer/seller\'s discretion if he/she chooses to extend the time limit.',
        isOpen: false
    },
    {
        id: 9,
        question: '10. What if I secure a property deal but the time limit expires before the project is finalised?',
        answer:
            'If the time limit expires before all 5 property deals have been secured the project deal is withdrawn and all holding deposits are refunded to those buyers who previously secured a property deal within the project.',
        isOpen: false
    },
    {
        id: 10,
        question: '11. Can the developer/seller change the property price or discount % after I pay my holding deposit?',
        answer:
            'No. GroupBuyer.com.au property deals are listed exclusively and all prices/discounts are 100% valid within each project time limit.',
        isOpen: false
    },
    {
        id: 11,
        question: '12. How many buyers are required in a project?',
        answer:
           'There are 5 property deals per project. Buyers can secure multiple property deals if they wish however once all 5 property deals are secured within a project the project is finalised (sold out).',
        isOpen: false
    },
    {
        id: 12,
        question: '13. How long does it take to finalise (sell out) a project?',
        answer:
           'It doesn\'t pay to wait! Project deals can be sold out in a matter of minutes. We encourage buyers to secure their property deal by paying the required holding deposit or risk losing their desired property deal.',
        isOpen: false
    },
    {
        id: 13,
        question: '14. What\'s required from me after a project is finalised (sold out)?',
        answer:
           'All buyers within a project are required to pay a 10% deposit (unless stated otherwise) and sign the individual Contract for sale. Your GroupBuyer project agent will coordinate with your chosen Solicitor to execute your purchase.',
        isOpen: false
    },
    {
        id: 14,
        question: '15. What if I have legal questions about the Contract for sale?',
        answer:
           'All project\'s Master contracts are available in the Resources section of each project & property page. You can download/print/send the Master contract to your Solicitor for review and the Master contract will be "individualised" by the developer/seller\'s Solicitor after the project is finalised (sold out) and you are pending exchange of contracts. ',
        isOpen: false
    },
    {
        id: 15,
        question: '16. Will I get notified when a project is finalised?',
        answer:
           'Yes. You\'ll receive a call from your GroupBuyer agent and also notified by email when your project is finalised (sold out). ',
        isOpen: false
    },
    {
        id: 16,
        question: '17. Will I get notified when other buyers join my project after me?',
        answer:
           'Yes. You will be notified by email when each buyer enters your project after you.',
        isOpen: false
    },
    {
        id: 17,
        question: '18. What happens if a buyer pulls out after a project is finalised?',
        answer:
           'Buyers who pull out after a project is finalised forfeit their $1,000 holding deposit.',
        isOpen: false
    },
    {
        id: 18,
        question: '19. Is the project deal still valid to remaining buyers if a buyer (or multiple buyers) pull out after the project is finalised?',
        answer:
           'Yes. All remaining buyers in a project keep their property deal despite any buyers pulling out prior to exchange of property contracts.',
        isOpen: false
    },
    {
        id: 19,
        question: '20. What\'s required from me to exchange property contracts?',
        answer:
           'You are required to pay a 10% deposit (unless stated otherwise) and sign the individual Contract for sale. Your GroupBuyer agent will coordinate with your Solicitor to assist you.',
        isOpen: false
    },
    {
        id: 20,
        question: '21. Who do I pay my property deposit to?',
        answer:
          'Your 10% property deposit is paid into the developer/seller\'s Solicitor\'s Trust Account (unless stated otherwise). This is required along with your signed property Contract for sale prior to unconditional exchange of contracts. ',
        isOpen: false
    },
    {
        id: 21,
        question: '22. What does unconditional exchange of contracts mean?',
        answer:
           'Unconditional exchange of contracts means there is no Cooling Off included in your property purchase. This is standard practice when buying within new developments throughout Australia.',
        isOpen: false
    },
    {
        id: 22,
        question: '23. How long do I get to exchange property contracts after a project is finalised (sold out)?',
        answer:
           'You have 10 business days (2 weeks) to formalise your property purchase with your Solicitor. Your GroupBuyer agent is on stand-by to help coordinate this with all parties involved.',
        isOpen: false
    },
    {
        id: 23,
        question: '24. What if a buyer in my project can\'t (or won\'t) exchange contracts?',
        answer:
           'If a buyer cannot exchange property contracts for any reason he/she will forfeit their $1,000 holding deposit.',
        isOpen: false
    },
    {
        id: 24,
        question: '25. Is my discount still valid if a buyer in my project doesn\'t exchange contracts?',
        answer:
           'Yes. Once all property deals are secured within the GroupBuyer project time limit the offer is valid to all buyers. Buyers who fail to exchange property contracts after a project is finalised (sold out) forfeit their $1,000 holding deposit.',
        isOpen: false
    },
    {
        id: 25,
        question: '26. Can I visit a Display Suite to see the property finishes & fixtures?',
        answer:
           'Yes. In fact we encourage it. You\'ll find your project Display Suite address (and Project address) located on both the project page and individual property pages. Click \'Get directions\' for Google Maps directions to either address. ',
        isOpen: false
    },
    {
        id: 26,
        question: '27. Can I visit the project location and go onsite with the developer?',
        answer:
           'If your project is under construction you may not be able to walk onsite due to health & safety restrictions. If your project is built/complete you can speak to your GroupBuyer agent who may be able to coordinate a property inspection.',
        isOpen: false
    },
    {
        id: 27,
        question: '28. Can I talk to the developer/seller directly?',
        answer:
           'No. However your GroupBuyer agent can answer all of your questions. Further, you can click \'Developer profile\' in the Resources section on each project/property page to learn more about the Property Developer.',
        isOpen: false
    },
    {
        id: 28,
        question: '29. Can I talk to the property developer\'s internal sales team?',
        answer:
           'Yes. However a developer/sellers internal sale team do not have authority to discuss (nor discount) properties listed exclusively with GroupBuyer.com.au. They may be able to assist you with general project questions however it\'s worth pointing out that their sole interest may be in selling you a different property at full price. ',
        isOpen: false
    },
    {
        id: 29,
        question: '30. Are the properties on GroupBuyer.com.au listed exclusively?',
        answer:
           'Yes. All individual properties on GroupBuyer.com.au are listed exclusively to GroupBuyer.com.au. The specific properties are not available via the developer/seller and/or his/her internal sales team.',
        isOpen: false
    },
    {
        id: 30,
        question: '31. I\'m ready to purchase a property deal, but I just want to speak to a human before I do?',
        answer:
           'That\'s no problem whatsoever. We\'d love to hear from you and help where we can. Phone 1300 031 835 to speak to one of our GroupBuyer team members. If we miss you, leave a message with your name & return phone number and we\'ll come straight back to you.',
        isOpen: false
    },
]