import React from 'react'

export const metaHelper = [
    {desc: "Australian-first online platform that allows buyers to purchase quality new properties at significantly reduced prices from top shelf developers"},
    // {keywords: "Quality new properties at significantly reduced prices."},
    {currPath: window.location.pathname}
];