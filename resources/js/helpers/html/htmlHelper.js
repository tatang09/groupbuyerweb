 
import React from 'react';


export const extracDescription = node => {
    let nodes = node;
    let htmlObject = document.createElement("div");
    htmlObject.innerHTML = nodes;
    let features = htmlObject.getElementsByTagName("ul")[0];
    let inclusions = htmlObject.getElementsByTagName("ul")[1];
    let featureHeader = htmlObject.getElementsByTagName("p");

    for (let index = 0; index <= featureHeader.length; index++) {
        let p = featureHeader[index];
        if (
            p &&
            (p.innerHTML === "<strong>Features:</strong>" ||
                p.innerHTML === "<strong>Inclusions:</strong>")
        ) {
            p.setAttribute("hidden", "true");
        }
    }

    features.remove();
    inclusions.remove();

    return htmlObject.innerHTML;
};

export const extractFeatureOrInclusion = (node, isFeature) => {
    let nodes = node;
    let htmlObject = document.createElement("div");
    htmlObject.innerHTML = nodes;
    
    let ul = htmlObject.getElementsByTagName("ul")[isFeature ? 0 : 1];
    let li = ul.getElementsByTagName("li");

    let arr = [];
 
    Array.from(li).forEach(element => {
        arr.push(element.innerHTML);
    });

    return arr.map((feature, index) => {
        return (
            <div className="flex text-base font-normal" key={index}>
                <span>
                    <img src="/assets/images/chevron-right.svg" className={"w-2 mr-2 mt-1"} />
                </span>
                {feature}
            </div>
        );
    });
}

     

 
