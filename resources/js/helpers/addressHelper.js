import React from "react";
import { isEmpty } from "lodash";

export const addressHelper = address => {
  if (!address || isEmpty(address)) return;

  if (typeof address === "string") {
    address = JSON.parse(address);
  }

  let properties = ["line_1", "suburb", "city", "state", "country", "postal"];

  let newAdd = "";

  properties.forEach((item, index) => {
    if (address[item]) {
      if (index == 0) {
        newAdd = newAdd + address[item];
      } else {
        newAdd = newAdd + ", " + address[item];
      }
    }
  });

  return newAdd;
};
