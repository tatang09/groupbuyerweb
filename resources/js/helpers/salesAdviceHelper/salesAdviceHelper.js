import React from 'react';
import { countries } from '~/helpers/countries';
import { states } from '~/helpers/propertyHelper/propertyHelper';
import { isEmpty } from 'lodash/isEmpty';
import { v4 as uuidv4 } from 'uuid';

export const formDataFieldsValidator = formData => {
  let hasError = false;
  for (let f of formData.entries()) {
    if (!f[1]) {
      hasError = true
    }
  }
  return hasError;
}

export const fieldValidator = paramObj => {

  let errors = [];

  if (Array.isArray(paramObj)) {
    paramObj.forEach(purchaser => {
      let err = {};
      Object.entries(purchaser).forEach(([key, value]) => {

        if (key !== 'id' && key !== 'alternate_phone' && key !== 'address_line_1' && key !== 'first_name' && key !== 'last_name' && !value) {
          err[key] = `The ${key} is required!`;
        }

        if (key === 'first_name' && !value) {
          err[key] = `The first name is required!`;
        }

        if (key === 'address_line_1' && !value) {
          err[key] = `The address line 1 is required!`;
        }

        if (key === 'last_name' && !value) {
          err[key] = `The last name is required!`;
        }

        if (key == 'phone' && typeof parseInt(value) != 'number') {
          err[key] = `The ${key} accepts numeric input only!`;
        }
      });

      if (!isEmpty(err)) {
        err.uid = purchaser.uid;
        errors.push(err);
      }
    });
  }
  return errors;
};

export const addtionalInfoFields = {
  heard_about_us: {
    label: 'How did you hear about us?',
    type: 'radio',
    classname: `ml-20 mt-2`,
    value: `SocialMedia`,
    options: {
      SocialMedia: 'Social Media',
      Google: 'Google',
      Friend: 'Friend',
    },
  },
  other: {
    label: 'Other',
  },
  purpose: {
    label: 'Purpose?',
    type: 'radio',
    classname: `ml-20 mt-2`,
    value: `OwnerOccupier`,
    options: {
      OwnerOccupier: 'Owner occupier',
      Investor: 'Investor',
    },
  },
  isFirstHomeBuyer: {
    label: 'First Home Buyer?',
    type: 'radio',
    classname: `ml-20 mt-2`,
    value: `No`,
    options: {
      Yes: 'Yes',
      No: 'No',
    },
  },
  isPreviousBoughtPlan: {
    label: 'Previously bought off the plan?',
    type: 'radio',
    classname: `ml-20 mt-2`,
    value: `No`,
    options: {
      Yes: 'Yes',
      No: 'No',
    },
  },
  isFIRB_required: {
    label: 'FIRB required?',
    type: 'radio',
    classname: `ml-20 mt-2`,
    value: `No`,
    options: { Yes: 'Yes', No: 'No' },
  },

  comment: {
    label: 'Comment',
    type: 'textarea',
    placeholder: '',
  },
};

export const defaultPurchasers = [
  {
    uid: uuidv4(),
    id: '',
    title: '',
    first_name: '',
    last_name: '',
    suburb: '',
    address_line_1: '',
    postcode: '',
    state: '',
    country: '',
    phone: '',
    alternate_phone: '',
    email: ''
  },
];

export const titles = [
  { value: 'Mr.', label: 'Mr.' },
  { value: 'Ms.', label: 'Ms.' },
  { value: 'Mrs.', label: 'Mrs.' },
];

export const solicitorDetailsFields = {
  company: {
    label: 'Company',
    required: true,
  },
  contact: {
    label: 'Contact',
    // prefix: '+61',
    // maxlength: '10',
    required: true,
  },
  phone: {
    label: 'Phone',
    required: true,
    prefix: '+61',
    maxlength: '10',
    type: 'number'
  },
  email: {
    label: 'Email',
    required: true,
  },
  poBox: {
    label: 'PO Box',
    required: true,
    placeholder: 'Enter Address/PO Box',
  },
  suburb: {
    label: 'Suburb',
    required: true,
  },
  postcode: {
    label: 'Postcode',
    required: true,
    placeholder: 'Enter Postcode/ZIP',
  },
  state: {
    label: 'State',
    required: true,
    type: 'select',
    placeholder: 'Select State',
    options: states,
    
  },
  country: {
    label: 'Country',
    required: true,
    type: 'select',
    placeholder: 'Select Country',
    options: countries,
  },
};

export const purchasersMapper = purchasersParam => {
  let newArr = [];
  if (purchasersParam.length > 0) {

    purchasersParam.map(p => {
      
      let address = !isEmpty(p.address) ? JSON.parse(p.address) : "";

      let obj = new Object({
        uid: uuidv4(),
        id: p.id,
        title: p.title,
        first_name: p.first_name,
        last_name: p.last_name,
        suburb: address.suburb,
        address_line_1: address.address_line_1,
        postcode: address.postcode,
        state: address.state,
        country: address.country,
        phone: p.phone ? p.phone.replace("+61", "") : '',
        alternate_phone: p.alternate_phone
          ? p.alternate_phone.replace("+61", "")
          : '',
        email: p.email,
      });
      newArr.push(obj);
    });
  }
  return newArr;
};

//

export const additionalInfoMapper = AdditionalInfoDetails => {

 
  if(isEmpty(AdditionalInfoDetails)) return;

  Object.keys(addtionalInfoFields).forEach(key => {
    if (AdditionalInfoDetails.hasOwnProperty(key)) {
      addtionalInfoFields[key].value = AdditionalInfoDetails[key];
    }
  });
  return addtionalInfoFields;
};

export const solicitorDetailsMapper = solicitorDetailsParam => {

  if (!isEmpty(solicitorDetailsParam)) {
    let address = JSON.parse(solicitorDetailsParam.address);
 
    Object.keys(solicitorDetailsFields).forEach(key => {
 
      if (key === 'suburb') {
        solicitorDetailsFields[key].value = address[key];
      } else if (key === 'state') {
        solicitorDetailsFields[key].value = address[key];
      } else if (key === 'country') {
        solicitorDetailsFields[key].value = address[key];
      } else if (key === 'postcode') {
        solicitorDetailsFields[key].value = address[key];
      } else if (key === 'phone') {
        solicitorDetailsFields[key].value = solicitorDetailsParam[key]
          ? solicitorDetailsParam[key].replace("+61", "")
          : '';
      } else {
        solicitorDetailsFields[key].value = solicitorDetailsParam[key];
      }
    });
   
  }
  return solicitorDetailsFields;
};
