import {isLoggedIn} from '~/services/auth';
import {axios} from '~/helpers/bootstrap';
import UserGlobal from '~/states/UserGlobal';
import {setToken} from '~/services/auth';

async function getUser({userState, userAction}) {

  if (!isLoggedIn()) {
    userAction.setState({user: null});
  } else {
    let {data} = await axios.get(
      `/api/me`,
      userState.user === null
        ? {
            headers: {
              Authorization: 'Bearer ' + isLoggedIn(),
            },
          }
        : null,
    );

    userAction.setState({user: data.data});
  }
}

 