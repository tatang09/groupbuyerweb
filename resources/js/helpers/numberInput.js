import React from "react";

export const inputRegex = (input) => {
  let regEx = new RegExp('^[0-9]*$');
  return regEx.test(input);
};
