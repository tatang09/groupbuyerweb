import axios from "axios"
import { isLoggedIn, logout } from "~/services/auth"
import { setProcessing } from "~/actions/loading"
import store from "~/store"

axios.defaults.baseURL =  process.env.APP_URL

let token = isLoggedIn()

if (token) {
  axios.interceptors.request.use((config) => {
    config.headers.common['Authorization']  = `Bearer ${token}`

    if (config.data && ! config.data.isLoading)  {
      return config
    }

    store.dispatch(setProcessing(true))

    return config
  }, (error) => {
    store.dispatch(setProcessing(false))

    return Promise.reject(error)
  })

  axios.interceptors.response.use(function (response) {
    store.dispatch(setProcessing(false))

    return response
  }, function (error) {
    store.dispatch(setProcessing(false))
   
    if (401 === error.response.status) {
      logout()
    } else {
      return Promise.reject(error)
    }
  })
}

export { axios }

export const initializeBootstrap = () => {
  window.axios = axios
}
