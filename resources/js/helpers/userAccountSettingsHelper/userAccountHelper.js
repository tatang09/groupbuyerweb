import React from "react";
import { states, countries } from "../countries";
import { inputRegex } from "../numberInput";

export const fields = {
  first_name: {
    label: "First Name",
    required: true
  },
  last_name: {
    label: "Last Name",
    required: true
  },
  email: {
    label: "Email",
    required: true
  },
  phone: {
    label: "Phone",
    prefix: "+61",
    maxlength: "10",
    type: "number"
  }
};

export const developerFields = {
  dev_name: {
    label: "Name",
    placeholder: "Office/Company Name"
  },
  dev_email: {
    label: "Email",
    placeholder: "Office/Company Email"
  },
  dev_phone: {
    label: "Phone",
    prefix: "+61",
    maxlength: "10",
    type: "number"
  },
  company_url: {
    label: "Site URL",
  },
  dev_address_line_1: {
    label: "Address",
    placeholder: "Enter Address Line 1"
  },
  dev_suburb: {
    label: "Suburb"
  },
  dev_postcode: {
    label: "Postcode",
    placeholder: "Enter ZIP/Postcode"
  },
  // bank_account_name: {
  //   label: "Account Name"
  // },
  // bank_bsb: {
  //   label: "BSB"
  // },
  // bank_account_number: {
  //   label: "Account No.",
  //   placeholder: "Enter Account Number"
  // }
};

export const descriptions = [
  { value: "All", label: "All" },
  { value: "Owner Occupier", label: "Owner Occupier" },
  { value: "First Home Buyer", label: "First Home Buyer" },
  { value: "Investor", label: "Investor" }
];

export const locations = [
  { value: "All", label: "All" },
  { value: "Adelaide", label: "Adelaide" },
  { value: "Brisbane", label: "Brisbane" },
  { value: "Gold Coast", label: "Gold Coast" },
  { value: "Melbourne", label: "Melbourne" },
  { value: "Newcastle", label: "Newcastle" },
  { value: "Sydney", label: "Sydney" }
];

export const prop_types = [
  { value: "All", label: "All" },
  { value: "Apartment", label: "Apartment" },
  { value: "House", label: "House" },
  { value: "House & Land", label: "House & Land" },
  { value: "Land", label: "Land" },
  { value: "Townhouse", label: "Townhouse" }
];

export const prices = [
  // { value: "All", label: "All" },
  { value: 50000, label: 50000 },
  { value: 100000, label: 100000 },
  { value: 150000, label: 150000 },
  { value: 200000, label: 200000 },
  { value: 250000, label: 250000 },
  { value: 300000, label: 300000 },
  { value: 350000, label: 350000 },
  { value: 400000, label: 400000 },
  { value: 450000, label: 450000 },
  { value: 500000, label: 500000 },
  { value: 550000, label: 550000 },
  { value: 600000, label: 600000 },
  { value: 650000, label: 650000 },
  { value: 700000, label: 700000 },
  { value: 750000, label: 750000 },
  { value: 800000, label: 800000 },
  { value: 900000, label: 900000 },
  { value: 950000, label: 950000 },
  { value: 1000000, label: 1000000 },
  { value: 1100000, label: 1100000 },
  { value: 1200000, label: 1200000 },
  { value: 1300000, label: 1300000 },
  { value: 1400000, label: 1400000 },
  { value: 1500000, label: 1500000 },
  { value: 1600000, label: 1600000 },
  { value: 1700000, label: 1700000 },
  { value: 1800000, label: 1800000 },
  { value: 1900000, label: 1900000 },
  { value: 2000000, label: 2000000 },
  { value: 2100000, label: 2100000 },
  { value: 2200000, label: 2200000 },
  { value: 2300000, label: 2300000 },
  { value: 2400000, label: 2400000 },
  { value: 2500000, label: 2500000 },
  { value: 2600000, label: 2600000 },
  { value: 2700000, label: 2700000 },
  { value: 2800000, label: 2800000 },
  { value: 2900000, label: 2900000 },
  { value: 3000000, label: 3000000 },
  { value: 4000000, label: 4000000 },
  { value: 5000000, label: 5000000 },
  { value: 6000000, label: 6000000 },
  { value: 7000000, label: 7000000 },
  { value: 8000000, label: 8000000 },
  { value: 9000000, label: 9000000 },
  { value: 10000000, label: 10000000 },
  { value: 11000000, label: 11000000 },
  { value: 12000000, label: 12000000 },
  { value: 13000000, label: 13000000 },
  { value: 14000000, label: 14000000 },
  { value: 15000000, label: 15000000 }
];
