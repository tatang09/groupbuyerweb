import React from "react";

export const xxSmall = 320;
export const xtraSmall = 375;
export const small = 640;
export const baseSmall = 430;
export const medium = 990;
export const baseLarge = 667;
export const midLarge = 1000;
export const large = 1024;
export const xtraLarge = 1280;
//Small screen laptop
export const smallScreen = 1366;
//Standard screen laptop
export const standardScreen = 1440;

 