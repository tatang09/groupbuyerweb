export const SET_AUTHENTICATED = 'SET_AUTHENTICATED'
export const SET_USER_DATA = 'SET_USER_DATA'

export const setAuthenticated = authenticated => ({
  type: SET_AUTHENTICATED,
  authenticated
})

export const setUserData = user => ({
  type: SET_USER_DATA,
  user
})
