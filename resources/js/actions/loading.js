export const LOADING_STATUS = 'LOADING_STATUS'
export const PROCCESSING = 'PROCCESSING'

export const setStatus = status => ({
  type: LOADING_STATUS,
  status
})

export const setProcessing = isProcessing => ({
  type: PROCCESSING,
  isProcessing
})
