<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdatePropertiesFeaturedImagesToNullable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('properties', function (Blueprint $table) {
            $table->json('featured_images')->nullable()->change();
            $table->longText('description')->nullable()->change();
            $table->renameColumn('unit','unit_name');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('properties', function (Blueprint $table) {
            $table->json('featured_images')->nullable(false)->change();
            $table->longText('description')->nullable(false)->change();
            $table->renameColumn('unit_name','unit');
        });
    }
}
