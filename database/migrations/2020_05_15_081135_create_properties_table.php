<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePropertiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('properties', function (Blueprint $table) {
            $table->id();
            $table->foreignId('deal_id')->constrained('deals')->onDelete('cascade');
            $table->float('price');
            $table->string('unit');
            $table->integer('no_of_bedrooms');
            $table->integer('no_of_bathrooms');
            $table->integer('garages');
            $table->integer('floor_area')->nullable();
            $table->boolean('is_secured');
            $table->json('featured_images');
            $table->longText('description');
            $table->longText('address');
            $table->foreignId('property_type_id')->constrained('property_types')->onDelete('cascade');
            $table->foreignId('sub_property_id')->constrained('sub_property_types')->onDelete('cascade');
            $table->foreignId('ownership_type_id')->constrained('ownership_types')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('properties');
    }
}
