<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDealsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('deals', function (Blueprint $table) {
            $table->id();
            $table->foreignId('incentive_id')->constrained('incentives')->onDelete('cascade');
            $table->foreignId('sub_property_id')->nullable()->constrained('sub_property_types')->onDelete('cascade');
            $table->foreignId('agent_id')->constrained('agents')->onDelete('cascade');     
            $table->string('name');
            $table->longText('description');
            $table->longText('address')->nullable();
            $table->json('featured_images')->nullable();
        });

    }

   


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('deals');
    }
}
