<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePaypalPaymentDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('paypal_payment_details', function (Blueprint $table) {
            $table->id();
            $table->foreignId('secured_deal_id')->constrained('secured_deals')->onDelete('cascade');         
            $table->integer('transaction_id');
            $table->integer('paypal_payer_id'); 
            $table->float('amount');
            $table->string('status');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('paypal_payment_details');
    }
}
