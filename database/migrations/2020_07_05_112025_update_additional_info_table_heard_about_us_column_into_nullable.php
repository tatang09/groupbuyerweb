<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateAdditionalInfoTableHeardAboutUsColumnIntoNullable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        
        Schema::table('additional_info', function (Blueprint $table) {
            $table->string('heard_about_us')->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('additional_info', function (Blueprint $table) {
            $table->string('heard_about_us')->nullable(false)->change();
        });
    }
}
