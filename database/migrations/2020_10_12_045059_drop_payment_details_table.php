<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class DropPaymentDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('payment_details');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::create('payment_details', function (Blueprint $table) {
            $table->id();
            $table->foreignId('secured_deal_id')->constrained('users')->onDelete('cascade');
            $table->string('payment_id')->nullable();
            $table->string('auth_code')->nullable();
            $table->string('card_data')->nullable();
            $table->string('transaction_data')->nullable();
            $table->string('payment_status')->nullable();
            $table->timestamps();
        });
    }
}
