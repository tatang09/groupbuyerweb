<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnsToPropertiesTableNewValue extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('properties', function (Blueprint $table) {
            $table->decimal('strata_cost', 8,2)->nullable()->after('strata');
            $table->decimal('water_cost', 8,2)->nullable()->after('water');
            $table->decimal('council_cost', 8,2)->nullable()->after('council');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('properties', function (Blueprint $table) {
            $table->dropColumn(['council_cost','water_cost','strata_cost']);
        });
    }
}
