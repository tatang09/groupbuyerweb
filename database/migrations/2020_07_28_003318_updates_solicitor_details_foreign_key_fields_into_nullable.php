<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdatesSolicitorDetailsForeignKeyFieldsIntoNullable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('solicitor_details', function (Blueprint $table) {
            $table->unsignedBigInteger('secured_deal_id')->nullable()->change();
            $table->unsignedBigInteger('property_id')->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('solicitor_details', function (Blueprint $table) {
            $table->unsignedBigInteger('secured_deal_id')->nullable(false)->change();
            $table->unsignedBigInteger('property_id')->nullable(false)->change();
        });
    }
}
