<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateDealsColumnChangeIncentivesIdToDiscount extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
  
    public function up()
    {
        Schema::disableForeignKeyConstraints();
        Schema::table('deals', function (Blueprint $table) {
            $table->renameColumn('incentive_id', 'discount');
        });
        Schema::enableForeignKeyConstraints(); // Reenable constraint
    }

    
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('deals', function (Blueprint $table) {
            $table->renameColumn('discount', 'incentive_id');
        });
    }
}
