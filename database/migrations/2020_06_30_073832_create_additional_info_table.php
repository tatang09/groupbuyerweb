<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAdditionalInfoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('additional_info', function (Blueprint $table) {
            $table->id();
            $table->foreignId('secured_deal_id')->constrained('secured_deals')->onDelete('cascade');
            $table->foreignId('property_id')->constrained('properties')->onDelete('cascade');   
            $table->string('heard_about_us');
            $table->string('purpose');
            $table->boolean('isFirstHomeBuyer');
            $table->boolean('isPreviousBoughtPlan');
            $table->boolean('isFIRB_required');
            $table->LongText('comment');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('additional_info');
    }
}
