<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSolicitorDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('solicitor_details', function (Blueprint $table) {
            $table->id();
            $table->foreignId('secured_deal_id')->constrained('secured_deals')->onDelete('cascade');
            $table->foreignId('property_id')->constrained('properties')->onDelete('cascade');   
            $table->string('company');
            $table->string('contact');
            $table->string('poBox');
            $table->longText('address');
            $table->string('phone');
            $table->string('email');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('solicitor_details');
    }
}
