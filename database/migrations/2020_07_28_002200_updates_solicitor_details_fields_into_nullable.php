<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdatesSolicitorDetailsFieldsIntoNullable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('solicitor_details', function (Blueprint $table) {
            $table->string('company')->nullable()->change();
            $table->string('contact')->nullable()->change();
            $table->longText('poBox')->nullable()->change();
            $table->longText('address')->nullable()->change();
            $table->string('phone')->nullable()->change();
            $table->string('email')->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('solicitor_details', function (Blueprint $table) {
            $table->string('company')->nullable(false)->change();
            $table->string('contact')->nullable(false)->change();
            $table->string('poBox')->nullable(false)->change();
            $table->longText('address')->nullable(false)->change();
            $table->string('phone')->nullable(false)->change();
            $table->string('email')->nullable(false)->change();
        });
    }
}
