<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ChangeSomePropertiesColumnsIntoNullables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('properties', function (Blueprint $table) {
            $table->integer('no_of_bedrooms')->nullable()->change();
            $table->integer('no_of_bathrooms')->nullable()->change();
            $table->integer('no_of_garages')->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('properties', function (Blueprint $table) {
            $table->integer('no_of_bedrooms')->nullable(false)->change();
            $table->integer('no_of_bathrooms')->nullable(false)->change();
            $table->integer('no_of_garages')->nullable(false)->change();
        });
    }
}
