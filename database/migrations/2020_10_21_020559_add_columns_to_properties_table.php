<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnsToPropertiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('properties', function (Blueprint $table) {
            $table->integer('external_sqm')->nullable()->after('internal_size');
            $table->integer('parking_sqm')->nullable()->after('external_sqm');
            $table->string('strata')->nullable()->after('parking_sqm');
            $table->string('water')->nullable()->after('strata');
            $table->string('council')->nullable()->after('water');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('properties', function (Blueprint $table) {
            $table->dropColumn(['external_sqm','parking_sqm','strata', 'water', 'council']);
        });
    }
}
