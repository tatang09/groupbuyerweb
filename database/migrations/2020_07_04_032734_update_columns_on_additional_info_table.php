<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateColumnsOnAdditionalInfoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('additional_info', function (Blueprint $table) {
            $table->string('isFirstHomeBuyer')->change();
            $table->string('isPreviousBoughtPlan')->change();
            $table->string('isFIRB_required')->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('additional_info', function (Blueprint $table) {
            $table->boolean('isFirstHomeBuyer')->change();
            $table->boolean('isPreviousBoughtPlan')->change();
            $table->boolean('isFIRB_required')->change();
        });
    }
}
 