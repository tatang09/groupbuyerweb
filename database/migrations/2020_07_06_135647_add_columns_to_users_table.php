<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnsToUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->longText('best_describe')->nullable();
            $table->longText('location')->nullable();
            $table->longText('max_price')->nullable();
            $table->longText('min_price')->nullable();
            $table->longText('propertyType')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn(['best_describe','location', 'max_price', 'min_price','propertyType']);
        });
    }
}
