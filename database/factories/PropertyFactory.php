<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Property;
use Faker\Generator as Faker;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;

$factory->define(Property::class, function (Faker $faker) {
    Storage::fake();

    $images = [];

    for($i = 0 ; $i <= 5 ; $i++) {
        $file = UploadedFile::fake()->image('test' . $i . '.jpg');
        array_push($images, $file);
    }

    return [
        'featuredImages' => $images,
        'price' => $faker->randomFloat(2),
        'number_of_bedrooms' => $faker->randomDigit,
        'number_of_bathrooms' => $faker->randomDigit,
        'number_of_garages' => $faker->randomDigit,
        'floor_area' => $faker->randomFloat(2),
        'unit_name' => $faker->words(2, true),
        'description' => $faker->paragraph,
        'descriptionHTML' => $faker->paragraph,
        'propertyType' => 2,
        'subPropertyType' => 2,
        'ownershipType' => 1,
        'line_1' => $faker->streetAddress,
        'suburb' => $faker->citySuffix,
        'city' => $faker->city,
        'state' => $faker->state,
    ];
});
