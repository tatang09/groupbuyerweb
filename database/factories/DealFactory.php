<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Deal;
use Faker\Generator as Faker;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;
use Carbon\Carbon;

$factory->define(Deal::class, function (Faker $faker) {

    Storage::fake();

    $images = [];

    for($i = 0 ; $i <= 5 ; $i++) {
        $file = UploadedFile::fake()->image('test' . $i . '.jpg');
        array_push($images, $file);
    }

    return [
        'name' => $faker->words(3, true),
        'developer' => 2,
        'description' => $faker->paragraph,
        'descriptionHTML' =>  $faker->paragraph,
        'propertyType' => 2,
        'project_address_line_1' => $faker->streetAddress,
        'project_address_suburb' => $faker->city,
        'project_address_state' => $faker->state,
        'project_address_zip' => $faker->postcode,
        'project_address_country' => $faker->country,
        'display_suite_address_line_1' => $faker->streetAddress,
        'display_suite_address_suburb' => $faker->city,
        'display_suite_address_state' => $faker->state,
        'display_suite_address_zip' => $faker->postcode,
        'display_suite_address_country' => $faker->country,
        'featuredImages' => $images,
        'proposed_settlement' => Carbon::now(),
        'project_time_limit' => Carbon::now(),
    ];
});
