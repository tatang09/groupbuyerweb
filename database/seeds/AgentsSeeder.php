<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class AgentsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('agents')->insert([
            [
                'name' => 'Luke Hayes',
                'email' =>'lukehayes@groupbuyer.com.au',
                'phone_number' => '+61123456789'
            ],
            [
                'name' => 'Ben Daniel',
                'email' =>'bendaniel@groupbuyer.com.au',
                'phone_number' => '+61123456789'
            ],
            [
                'name' => 'John Doe',
                'email' =>'johndoe@groupbuyer.com.au',
                'phone_number' => '+61123456789'
            ]
        ]);
    }
}
