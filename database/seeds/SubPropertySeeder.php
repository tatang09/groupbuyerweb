<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\SubPropertyType;

class SubPropertySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0');
        SubPropertyType::truncate();
        DB::table('sub_property_types')->insert([
            [
                'name' => 'Apartment',
            ],
            [
                'name' => 'House',
            ],
            [
                'name' => 'House & Land',
            ],
            [
                'name' => 'Land',
            ],
            [
                'name' => 'Townhouse',
            ]
        ]);
        DB::statement('SET FOREIGN_KEY_CHECKS=1');
    }
}
