<?php

use Illuminate\Database\Seeder;

class OwnershipTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('ownership_types')->insert([
            [
                'name' => 'Owner Occupied',
            ],
            [
                'name' => 'Investment',
            ],
        ]);
    }
}
