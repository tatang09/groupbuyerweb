<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use App\User;

class RolesAndPermissionsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        app()[\Spatie\Permission\PermissionRegistrar::class]->forgetCachedPermissions();

        $super_admin = Role::create(['name' => 'super_admin', 'guard_name' => 'api', 'access_level' => 0]);
        Role::create(['name' => 'admin', 'guard_name' => 'api', 'access_level' => 1]);
        Role::create(['name' => 'project_developer', 'guard_name' => 'api', 'access_level' => 2]);
        Role::create(['name' => 'customer', 'guard_name' => 'api', 'access_level' => 2]);
        Role::create(['name' => 'guest', 'guard_name' => 'api', 'access_level' => null]);
        Role::create(['name' => 'real_estate_agent', 'guard_name' => 'api', 'access_level' => 2]);

        //admin
        User::create([
            'first_name' => 'Group Buyer',
            'last_name' => 'Admin',
            'email' => 'bendaniel@groupbuyer.com.au',
            'password' => bcrypt('password')
        ])->assignRole('admin');

        //developers
        User::create([
            'first_name' => 'Isla',
            'last_name' => 'Hollway',
            'email' => 'IslaHollway@armyspy.com',
            'password' => bcrypt('password')
        ])->assignRole('project_developer');

        User::create([
            'first_name' => 'Amelia',
            'last_name' => 'Burns',
            'email' => 'AmeliaBurns@teleworm.us',
            'password' => bcrypt('password')
        ])->assignRole('project_developer');

        User::create([
            'first_name' => 'Dylan',
            'last_name' => 'Chabrillan',
            'email' => 'DylanChabrillan@jourrapide.com',
            'password' => bcrypt('password')
        ])->assignRole('project_developer');

        //customers
        User::create([
            'first_name' => 'Rory',
            'last_name' => 'Bini',
            'email' => 'RoryBini@teleworm.us',
            'password' => bcrypt('password')
        ])->assignRole('customer');

        User::create([
            'first_name' => 'Erin',
            'last_name' => 'Badger',
            'email' => 'ErinBadger@teleworm.us',
            'password' => bcrypt('password')
        ])->assignRole('customer');

        User::create([
            'first_name' => 'John',
            'last_name' => 'Doe',
            'email' => 'JohnDoe@teleworm.us',
            'password' => bcrypt('password')
        ])->assignRole('real_estate_agent');

    }
}
