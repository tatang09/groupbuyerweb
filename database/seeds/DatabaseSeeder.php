<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\File;
use App\Deal;
use App\Property;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $confirm = $this->command
            ->confirm('Do you want to include uploading files to S3? This may overwrite any existing files and folders. If the files are already uploaded you may opt for no. but if you want to re-upload the files you may do so.');     

        $this->call([
           RolesAndPermissionsSeeder::class,
            // IncentivesSeeder::class,
            AgentsSeeder::class,
            SubPropertySeeder::class,
            PropertyTypeSeeder::class,
            OwnershipTypeSeeder::class,
            DealsSeeder::class,
            PropertiesSeeder::class,
            BankSeeder::class
        ]);
        
        if($confirm) {
            $this->command->info('seeding properties');   
            $deals = Deal::all();
            foreach ($deals as $key => $deal) {
                $this->command->info('seeding ' . $deal->name);
                $project_name = strtolower(str_replace(' ', '-', trim($deal->name)));
                foreach ($deal->featured_images as $k => $image) { 
                    $filename = explode('/', $image)[3];
                    $s3_dir = 'projects/' . $project_name . '/featured-images/';
                    $public_path = 'app/public/projects/' . $project_name . '/featured-images/' . $filename;
                    Storage::disk(config('filesystems.cloud'))
                    ->putFileAs($s3_dir,
                    new File(storage_path($public_path)),
                    $filename);   
                }
            
                foreach($deal->properties()->get() as $key => $property) {
                    $property_name = strtolower(str_replace(' ', '-', trim($property->unit_name)));
                    $this->command->info('seeding ' . $property->unit_name);
                    foreach ($property->featured_images as $k => $image) {      
                        $filename = explode('/', $image)[4];
                        $s3_dir = 'projects/' . $project_name . '/properties/' . $property_name;
                        // $s3_dir_full = 'projects/' . $project_name . '/properties/' . $property_name . '/' . $filename;
                        // Storage::disk(config('filesystems.cloud'))->delete($s3_dir_full . '/' . $filename);
                        $public_path = 'app/public/projects/' . $project_name . '/properties/' . $property_name . '/' . $filename;
                        
                        $this->command->info('storing to s3 ' . $filename);
                        Storage::disk(config('filesystems.cloud'))
                        ->putFileAs($s3_dir,
                        new File(storage_path($public_path)),
                        $filename);   
                    }
                }
            }
        }

        foreach(Deal::all() as $deal) {
            $deal->is_deal_approved = true;
            $deal->save();
        }

        foreach(Property::all() as $property) {
            $property->is_property_approved = true;
            $property->save();
        }

    }
}
