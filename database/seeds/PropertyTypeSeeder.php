<?php

use Illuminate\Database\Seeder;
use App\PropertyType;

class PropertyTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0');
        PropertyType::truncate();
        DB::table('property_types')->insert([
            [
                'name' => 'Apartment',
            ],
            [
                'name' => 'House',
            ],
            [
                'name' => 'House & Land',
            ],
            [
                'name' => 'Land',
            ],
            [
                'name' => 'Townhouse',
            ]
        ]);
        DB::statement('SET FOREIGN_KEY_CHECKS=1');
    }
}
