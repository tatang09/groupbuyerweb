<?php

use Illuminate\Database\Seeder;
use App\Bank;

class BankSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('banks')->insert([
            [
                'name' => 'Commonwealth Bank',
            ],
            [
                'name' => 'Australia and New Zealand Banking Group (ANZ)',
            ],
            [
                'name' => 'National Australian Bank (NAB)',
            ],
            [
                'name' => 'Westpac Bank',
            ],
            [
                'name' => 'Bank of Queensland',
            ],
            [
                'name' => 'Macquarie Bank',
            ],
            [
                'name' => 'AMP Bank Ltd',
            ],
            [
                'name' => 'Suncorp Bank',
            ],
            [
                'name' => 'Bankwest',
            ]
        ]);
    }
}
