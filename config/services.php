<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Mailgun, Postmark, AWS and more. This file provides the de facto
    | location for this type of information, allowing packages to have
    | a conventional file to locate the various service credentials.
    |
    */

    'mailgun' => [
        'domain' => env('MAILGUN_DOMAIN'),
        'secret' => env('MAILGUN_SECRET'),
        'endpoint' => env('MAILGUN_ENDPOINT', 'api.mailgun.net'),
    ],

    'postmark' => [
        'token' => env('POSTMARK_TOKEN'),
    ],

    'ses' => [
        'key' => env('AWS_ACCESS_KEY_ID'),
        'secret' => env('AWS_SECRET_ACCESS_KEY'),
        'region' => env('AWS_DEFAULT_REGION', 'us-east-1'),
    ],

    'active_campaign' => [
        'key' => env('ACTIVE_CAMPAIGN_KEY'),
        'url' => env('ACTIVE_CAMPAIGN_URL')
    ],

    'commbank_keys' => [
        'private_sandbox' => env('COMMBANK_SANDBOX_PRIVATE_KEY'),
        'public_sandbox' => env('COMMBANK_SANDBOX_PUBLIC_KEY'),
        'private_default' => env('COMMBANK_DEFAULT_PRIVATE_KEY'),
        'public_default' => env('COMMBANK_DEFAULT_PUBLIC_KEY')
    ],

    'payment' => [
        'testing_amount' => env('TESTING_SECURED_DEAL_AMOUNT'),
        'production_amount' => env('PRODUCTION_SECURED_DEAL_AMOUNT'),
    ],

    'booking_inspection' => [
        'default_receiver' => env('BOOKING_INSPECTION_EMAIL_FROM'),
        'default_cc' => env('BOOKING_INSPECTION_EMAIL_CC'),
    ],

    'registration_notif' => [
        'receiver' => env('REGISTRATION_NOTIF')
    ],

    'requestdemo_notif' => [
        'cc1' => env('REQUEST_DEMO_NOTIF_CC_1'),
        'cc2' => env('REQUEST_DEMO_NOTIF_CC_2')
    ],

    'aws_preview_image_path' => [
        'local' => env('AWS_LOCAL_PATH_PREVIEW_IMAGE'),
        'dev' => env('AWS_DEV_PATH_PREVIEW_IMAGE'),
        'stg' => env('AWS_STG_PATH_PREVIEW_IMAGE'),
        'prod' => env('AWS_PROD_PATH_PREVIEW_IMAGE'),
    ],

    'open_graph' => [
        'open_graph' => env('OPEN_GRAPH_KEY')
    ]
];
