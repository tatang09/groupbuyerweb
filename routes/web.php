<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Storage;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::prefix('admin')->group(function () {
    Route::view('/', 'admin.app');
    Route::get('{all}', function () {
        return view('admin.app');
    })->where('all', '.*');
});

Route::get('/weekly-deals/{id}/apartment/{propertyId}/{preview?}', 'OpenGraphController@processPropertyContent');
Route::get('/weekly-deals/{id}/{preview?}', 'OpenGraphController@processProjectContent');


Route::get('{all}', function () {
    $content = [
        "name" => "GroupBuyer",
        "title" => "GroupBuyer",
        "description" => "Brand new homes at significantly reduced prices. Save an average of $107,134 off your new home.",
        "image" => Storage::disk(config('filesystems.cloud'))->temporaryUrl('assets/fb_preview.png', now()->addDays(1)),
        "url" => url()->current(),
    ];

    return view('app', ['items' => $content]);
})->where('all', '.*');

Auth::routes();

// Route::get('/home', 'HomeController@index')->name('home');
