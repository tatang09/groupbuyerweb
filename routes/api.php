<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Resources\UserResource;

use App\ProjectDeveloper;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


Route::post('password/email', 'ResetPassword\ForgotPasswordController@sendResetLinkEmail');
Route::post('password/reset', 'ResetPassword\ResetPasswordController@reset');

Route::resource('deal','DealController');
Route::resource('property','PropertyController');
Route::resource('property-types','PropertyTypeController');
Route::resource('sub-property-type','SubPropertyTypeController');

Route::post('send-contact-email', 'MailController@contactFormEmail')->name('contact-email');
Route::post('send-apply-now', 'MailController@applyFormEmail')->name('apply-now');

Route::get('user-count','UserController@userCount')->name('user-count');
Route::get('average-savings','PropertyController@calculateAverageSavings')->name('average-savings');
 
Route::group(['middleware' => 'auth:api', 'role:customer'], function () {
    Route::resource('secure-deal', 'SecuredDealController');
    Route::post('generate-payment-token', 'SecuredDealController@generateSecure3DPaymentToken');
    Route::post('create-payment', 'SecuredDealController@initiatePayment');
    Route::resource('sales-advice', 'SalesAdviceController');
    Route::resource('wish-list', 'WishlistController');
});

Route::group(['middleware' => 'auth:api'], function () {
    Route::get('me', function (Request $request) {
        return new UserResource($request->user());
    })->name('me');

    Route::resource('user', 'UserController');
    Route::post('stripe/webhook', 'WebhookController@handleWebhook');
    Route::post('logout', 'Auth\LoginController@logout');
    Route::post('approve-deal','Admin\ApproveDealController');
    Route::post('featured-deal', 'Admin\ApproveDealController@featuredDeal');
    Route::post('weekly-deal', 'Admin\ApproveDealController@weeklyDeal');
    Route::post('approve-property', 'Admin\ApprovePropertyController');
    Route::resource('property','PropertyController');
    Route::post('open-graph', 'OpenGraphController@processContent');
});

Route::middleware(['auth:api'])->group( function () {
    Route::get('project', 'ProjectController');
    Route::post('project-resource/upload','ProjectResourceController@store');
    Route::post('project-resource/update', 'ProjectResourceController@update');
    Route::get('bank', 'BankController@index');
});

Route::resource('developer', 'DeveloperController');
Route::post('request-demo', 'DeveloperController@requestDemo');
Route::resource('deal','DealController');
Route::get('book-inspection', 'PropertyController@requestInspection')->name('book-inspection');

Route::group(['middleware' => 'guest:api'], function () {
    Route::post('login', 'Auth\LoginController@login')->name('login');
    Route::post('register', 'Auth\RegisterController@register')->name('register');
    Route::post('social-media-login', 'Auth\LoginController@socialMediaLogin')->name('social-media-login');
});


