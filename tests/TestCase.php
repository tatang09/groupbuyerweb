<?php

namespace Tests;

use Illuminate\Foundation\Testing\TestCase as BaseTestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;
use App\User;

abstract class TestCase extends BaseTestCase
{
    use CreatesApplication;
    use RefreshDatabase;
    
    public function signIn($user = null)
    {
        $user = $user ?: factory(User::class)->create();
      //  $user->assignRole('project_developer');
        $this->actingAs($user,'api');

        return $this;
    }

    public function signInAsProjectDeveloper($user = null)
    {
      $user = $user ?: factory(User::class)->create();
        $user->assignRole('project_developer');
        $this->actingAs($user,'api');

        return $this;
    }

    public function signInAsAdmin($user = null)
    {
      $user = $user ?: factory(User::class)->create();
        $user->assignRole('admin');
        $this->actingAs($user,'api');

        return $this;
    }
}
