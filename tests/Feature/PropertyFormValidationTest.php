<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class PropertyFormValidationTest extends TestCase
{
    public function setUp() : void
    {   
        parent::setUp();

        $this->artisan("db:seed --class='RolesAndPermissionsSeeder'");

        $this->app->make(\Spatie\Permission\PermissionRegistrar::class)->registerPermissions();    
    }

    /** @test */
    public function add_property_form_required_fields_are_required()
    {
        $this->signInAsProjectDeveloper();

        $this->post('/api/property', [])
        ->assertSessionHasErrors([
            'featuredImages',
            'price',
            'number_of_bedrooms',
            'number_of_bathrooms',
            'number_of_garages',
            'floor_area',
            'unit_name',
            'description',
            'propertyType',
            'subPropertyType',
            'ownershipType',
            'line_1',
            'suburb',
            'city',
            'state',
        ]);
    }
}
