<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use App\Deal;

class AddProjectTest extends TestCase
{   
    public function setUp() : void
    {   
        parent::setUp();

        $this->artisan("db:seed --class='RolesAndPermissionsSeeder'");

        $this->app->make(\Spatie\Permission\PermissionRegistrar::class)->registerPermissions();    
    }

     /** @test */
    public function project_developer_can_add_project() 
    {   
        $this->withoutExceptionHandling();
        $this->signInAsProjectDeveloper();

        $project = factory(Deal::class)->raw();
     
        $this->post('/api/deal', $project)
            ->assertStatus(200);

        $this->assertDatabaseHas('deals', [
            'name' => $project['name'],
            'description' => $project['descriptionHTML'],
            'sub_property_id' => $project['propertyType']
        ]);
    }

    /** @test */
    public function admin_can_add_project()
    {
        $this->withoutExceptionHandling();
        $this->signInAsAdmin();

        $project = factory(Deal::class)->raw();

        $this->post('/api/deal', $project)
            ->assertStatus(200);

        $this->assertDatabaseHas('deals', [
            'name' => $project['name'],
            'description' => $project['descriptionHTML'],
            'sub_property_id' => $project['propertyType']
        ]);
    }
}
