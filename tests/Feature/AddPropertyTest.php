<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use App\Deal;
use App\Property;

class AddPropertyTest extends TestCase
{
    public function setUp() : void
    {   
        parent::setUp();

        $this->artisan("db:seed --class='RolesAndPermissionsSeeder'");

        $this->app->make(\Spatie\Permission\PermissionRegistrar::class)->registerPermissions();    
    }

 

    /** @test */
    public function project_developer_can_add_property()
    {
        $this->withoutExceptionHandling();
        $this->signInAsProjectDeveloper();

        $property = factory(Property::class)->raw(['projectId' => 1]);
  
        $this->post('/api/property', $property)
            ->assertStatus(200);

        $this->assertDatabaseHas('properties', [
            'unit_name' => $property['unit_name'],
            'description' =>$property['descriptionHTML']
        ]);
    }

    /**@test */
    public function admin_can_add_property()
    {
        $this->withoutExceptionHandling();
        $this->signInAsAdmin();

        $property = factory(Property::class)->raw(['projectId' => 1]);

        $this->post('/api/property', $property)
            ->assertStatus(200);

        $this->assertDatabaseHas('properties', [
            'unit_name' => $property['unit_name'],
            'description' => $property['descriptionHTML']
        ]);
    }

}
