<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use App\Deal;

class ProjectFormValidationTest extends TestCase
{
    public function setUp() : void
    {   
        parent::setUp();

        $this->artisan("db:seed --class='RolesAndPermissionsSeeder'");

        $this->app->make(\Spatie\Permission\PermissionRegistrar::class)->registerPermissions();    
    }
   
    /** @test */
    public function add_project_form_required_fields_are_required() 
    {   
        $this->signInAsProjectDeveloper();
        
        $this->post('/api/deal', [])
            ->assertSessionHasErrors([
                'name',
                'description',
                'propertyType',
                'project_address_line_1',
                'project_address_suburb',
                'project_address_state',
                'project_address_zip',
                'project_address_country',
                'display_suite_address_line_1',
                'display_suite_address_suburb',
                'display_suite_address_state',
                'display_suite_address_zip',
                'display_suite_address_country',
                'featuredImages',
                'proposed_settlement',
            ]);
    }
    
    /** @test */
    public function suite_address_fields_not_required_if_same_address()
    {
        $this->signInAsProjectDeveloper();
        $this->post('/api/deal', ['same_address' => 'on'])
        ->assertSessionHasErrors([
            'name',
            'description',
            'propertyType',
            'project_address_line_1',
            'project_address_suburb',
            'project_address_state',
            'project_address_zip',
            'project_address_country',
            'featuredImages',
            'proposed_settlement',
        ]);
    } 
}
