(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["BuyingListPage"],{

/***/ "./resources/js/pages/BuyingListPage.js":
/*!**********************************************!*\
  !*** ./resources/js/pages/BuyingListPage.js ***!
  \**********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime/regenerator */ "./node_modules/@babel/runtime/regenerator/index.js");
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _layouts_users_UserLayout__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../layouts/users/UserLayout */ "./resources/js/layouts/users/UserLayout.js");
/* harmony import */ var _states_userGlobal__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../states/userGlobal */ "./resources/js/states/userGlobal.js");
/* harmony import */ var _components__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../components */ "./resources/js/components/index.js");
/* harmony import */ var _components__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(_components__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var _components_base__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../components/_base */ "./resources/js/components/_base/index.js");
/* harmony import */ var _components_base__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(_components_base__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var _helpers_pricingHelper_discountHelper__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../helpers/pricingHelper/discountHelper */ "./resources/js/helpers/pricingHelper/discountHelper.js");
/* harmony import */ var _material_ui_core__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @material-ui/core */ "./node_modules/@material-ui/core/esm/index.js");
/* harmony import */ var react_device_detect__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! react-device-detect */ "./node_modules/react-device-detect/main.js");
/* harmony import */ var react_device_detect__WEBPACK_IMPORTED_MODULE_8___default = /*#__PURE__*/__webpack_require__.n(react_device_detect__WEBPACK_IMPORTED_MODULE_8__);
/* harmony import */ var react_currency_format__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! react-currency-format */ "./node_modules/react-currency-format/lib/currency-format.js");
/* harmony import */ var react_currency_format__WEBPACK_IMPORTED_MODULE_9___default = /*#__PURE__*/__webpack_require__.n(react_currency_format__WEBPACK_IMPORTED_MODULE_9__);
/* harmony import */ var react_select__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! react-select */ "./node_modules/react-select/dist/react-select.browser.esm.js");
/* harmony import */ var react_helmet__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! react-helmet */ "./node_modules/react-helmet/es/Helmet.js");
/* harmony import */ var _helpers_metaHelper_metaHelper__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ../helpers/metaHelper/metaHelper */ "./resources/js/helpers/metaHelper/metaHelper.js");
function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }



function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _unsupportedIterableToArray(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

function _iterableToArrayLimit(arr, i) { if (typeof Symbol === "undefined" || !(Symbol.iterator in Object(arr))) return; var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }














var BuyingListPage = function BuyingListPage() {
  var _React$createElement;

  var _UserGlobal = Object(_states_userGlobal__WEBPACK_IMPORTED_MODULE_3__["default"])(),
      _UserGlobal2 = _slicedToArray(_UserGlobal, 2),
      userState = _UserGlobal2[0],
      userAction = _UserGlobal2[1];

  var _useState = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(""),
      _useState2 = _slicedToArray(_useState, 2),
      keyword = _useState2[0],
      setKeyword = _useState2[1];

  var _useState3 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(null),
      _useState4 = _slicedToArray(_useState3, 2),
      popper = _useState4[0],
      setPopper = _useState4[1];

  var _useState5 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(false),
      _useState6 = _slicedToArray(_useState5, 2),
      openSearch = _useState6[0],
      setOpenSearch = _useState6[1];

  var _useState7 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(false),
      _useState8 = _slicedToArray(_useState7, 2),
      toggleFetch = _useState8[0],
      setToggleFetch = _useState8[1];

  var _useState9 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(false),
      _useState10 = _slicedToArray(_useState9, 2),
      priceDropdown = _useState10[0],
      setPriceDropdown = _useState10[1];

  var _useState11 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])([]),
      _useState12 = _slicedToArray(_useState11, 2),
      projectData = _useState12[0],
      setProjectData = _useState12[1];

  var _useState13 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])([]),
      _useState14 = _slicedToArray(_useState13, 2),
      options = _useState14[0],
      setOptions = _useState14[1];

  var _useState15 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])([]),
      _useState16 = _slicedToArray(_useState15, 2),
      priceOptions = _useState16[0],
      setPriceOptions = _useState16[1];

  var _useState17 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])({
    sortBy: {
      value: "deals.created_at",
      order: "desc",
      label: "Most Recent"
    },
    minPrice: {
      value: 0,
      label: "Any"
    },
    maxPrice: {
      value: 15000000,
      label: "Any"
    },
    types: "All",
    selectedTypes: {
      value: "All",
      label: "All"
    },
    locations: "All",
    selectedLocations: {
      value: "All",
      label: "All"
    }
  }),
      _useState18 = _slicedToArray(_useState17, 2),
      filters = _useState18[0],
      setFilters = _useState18[1];

  var anchorRef = Object(react__WEBPACK_IMPORTED_MODULE_1__["useRef"])(null);
  Object(react__WEBPACK_IMPORTED_MODULE_1__["useEffect"])(function () {
    if (localStorage.getItem("__use_projects_filters")) {
      var savedFilters = JSON.parse(localStorage.getItem("__projects_filters"));
      setFilters(savedFilters);
      setToggleFetch(!toggleFetch);
      localStorage.setItem("__use_projects_filters", "");
    }

    var getType = /*#__PURE__*/function () {
      var _ref = _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee() {
        var _yield$axios$get, data, $typeOptions;

        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                _context.next = 2;
                return axios.get("/api/sub-property-type");

              case 2:
                _yield$axios$get = _context.sent;
                data = _yield$axios$get.data;
                $typeOptions = data.map(function (type) {
                  return {
                    value: type.id,
                    label: type.name
                  };
                });
                $typeOptions.unshift({
                  value: "All",
                  label: "All"
                });
                setOptions($typeOptions);

              case 7:
              case "end":
                return _context.stop();
            }
          }
        }, _callee);
      }));

      return function getType() {
        return _ref.apply(this, arguments);
      };
    }();

    var getPriceOptions = function getPriceOptions() {
      var options = [];
      _helpers_pricingHelper_discountHelper__WEBPACK_IMPORTED_MODULE_6__["prices"].map(function (price) {
        options.push({
          value: price,
          label: new Intl.NumberFormat("en-AU", {
            style: "currency",
            currency: "AUD",
            minimumFractionDigits: 0
          }).format(price || 0)
        });
      });
      setPriceOptions(options);
    };

    getType();
    getPriceOptions();
  }, []);

  var handleSortBy = function handleSortBy(selectedItem) {
    setFilters(_objectSpread(_objectSpread({}, filters), {}, {
      sortBy: selectedItem
    }));
    setToggleFetch(!toggleFetch);
  };

  var handleMinPrice = function handleMinPrice(selectedItem) {
    if (!selectedItem) {
      setFilters(_objectSpread(_objectSpread({}, filters), {}, {
        minPrice: {
          value: 0,
          label: "Any"
        }
      }));
      setToggleFetch(!toggleFetch);
      return;
    }

    setFilters(_objectSpread(_objectSpread({}, filters), {}, {
      minPrice: selectedItem
    }));
    setToggleFetch(!toggleFetch);
  };

  var handleMaxPrice = function handleMaxPrice(selectedItem) {
    if (!selectedItem) {
      setFilters(_objectSpread(_objectSpread({}, filters), {}, {
        maxPrice: {
          value: 15000000,
          label: "Any"
        }
      }));
      setToggleFetch(!toggleFetch);
      return;
    }

    setFilters(_objectSpread(_objectSpread({}, filters), {}, {
      maxPrice: selectedItem
    }));
    setToggleFetch(!toggleFetch);
  };

  var handleSelectType = function handleSelectType(selectedItem) {
    if (!selectedItem || selectedItem.length === 0) {
      setFilters(_objectSpread(_objectSpread({}, filters), {}, {
        types: "All",
        selectedTypes: {
          value: "All",
          label: "All"
        }
      }));
      setToggleFetch(!toggleFetch);
      return;
    }

    var types = [];
    var lastSelected = selectedItem[selectedItem.length - 1];
    var filteredTypes = selectedItem.filter(function (item) {
      return item.value !== "All";
    });

    if (lastSelected.value === "All") {
      setFilters(_objectSpread(_objectSpread({}, filters), {}, {
        types: lastSelected.value,
        selectedTypes: lastSelected
      }));
      setToggleFetch(!toggleFetch);
      return;
    }

    filteredTypes.map(function (type) {
      types.push(type.value);
    });
    setFilters(_objectSpread(_objectSpread({}, filters), {}, {
      types: types,
      selectedTypes: filteredTypes
    }));
    setToggleFetch(!toggleFetch);
  };

  var handleSelectLocation = function handleSelectLocation(selectedItem) {
    if (!selectedItem || selectedItem.length === 0) {
      setFilters(_objectSpread(_objectSpread({}, filters), {}, {
        locations: "All",
        selectedLocations: {
          value: "All",
          label: "All"
        }
      }));
      setToggleFetch(!toggleFetch);
      return;
    }

    var locations = [];
    var lastSelected = selectedItem[selectedItem.length - 1];
    var filteredLocations = selectedItem.filter(function (item) {
      return item.value !== "All";
    });

    if (lastSelected.value === "All") {
      setFilters(_objectSpread(_objectSpread({}, filters), {}, {
        locations: lastSelected.value,
        selectedLocations: lastSelected
      }));
      setToggleFetch(!toggleFetch);
      return;
    }

    filteredLocations.map(function (city) {
      locations.push(city.value);
    });
    setFilters(_objectSpread(_objectSpread({}, filters), {}, {
      locations: locations,
      selectedLocations: filteredLocations
    }));
    setToggleFetch(!toggleFetch);
  };

  var handleSelectProject = function handleSelectProject(url) {
    // if (!isLoggedIn()) {
    //   userAction.setState({ showSignIn: true });
    //   return;
    // }
    localStorage.setItem("__projects_filters", JSON.stringify(filters));
    window.location = url;
  };

  var renderProjects = function renderProjects() {
    return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("tbody", {
      className: "flex flex-wrap"
    }, projectData && projectData.map(function (project, key) {
      return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("tr", {
        key: key,
        className: "lg:w-4/12 mb-4 lg:mb-4 lg:px-3 w-full"
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(react__WEBPACK_IMPORTED_MODULE_1___default.a.Fragment, null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("td", {
        style: {
          boxShadow: "2px 2px 5px 0px rgb(0 0 0 / 10%), 2px 2px 5px 0px rgb(0 0 0 / 6%)"
        },
        className: "p-5 block bg-white rounded-lg lg:hover:scale-105 p-0" //className={`p-4 block bg-white rounded-md lg:hover:scale-105 duration-300 transform transition-all p-0`}

      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
        className: "".concat(key % 5 === 0 && key !== 0 ? "text-palette-gray-main" : "text-palette-violet-main-dark", " text-2xl font-bold mb-3 leading-none py-3")
      }, project.name), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
        className: "cursor-pointer",
        onClick: function onClick() {
          return handleSelectProject("/weekly-deals/".concat(project.id));
        }
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
        className: "bg-center bg-cover relative text-white",
        style: {
          minHeight: react_device_detect__WEBPACK_IMPORTED_MODULE_8__["isMobile"] ? 225 : 180,
          background: "linear-gradient(rgba(0, 0, 0, 0.9) 0%,rgba(0, 0, 0, 0.1) 25%,rgba(0, 0, 0, 0.1) 0%, rgba(0, 0, 0, 0.2) 82%, rgba(0, 0, 0,0.8) 100%), url(\"".concat(project.featured_images[0], "\")")
        }
      }, key % 5 === 0 && key !== 0 && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
        className: "absolute left-0 mt-3 overflow-hidden rounded-sm top-0"
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
        className: "flex items-center ml-3"
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("img", {
        src: "assets/svg/sold_out.svg",
        className: "w-2/3"
      }))), key % 3 === 0 && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
        className: "absolute left-0 mt-3 overflow-hidden rounded-sm top-0"
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
        className: "flex items-center ml-3"
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("img", {
        src: "assets/svg/premium.svg",
        className: "w-3/4"
      }))), project.discount && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
        className: "absolute top-0 right-0 mt-2 text-xl top-0"
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
        className: "bg-transparent flex items-center mr-3"
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
        className: "text-base font-semibold"
      }, "".concat(project.discount, "% ")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
        className: "text-base font-semibold "
      }, "\xA0off"))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
        className: "absolute bottom-0 right-0 mb-2 mr-4 flex items-center text-sm font-bold"
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", null, "From\xA0"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(react_currency_format__WEBPACK_IMPORTED_MODULE_9___default.a, {
        value: project && Object(_helpers_pricingHelper_discountHelper__WEBPACK_IMPORTED_MODULE_6__["discountedPriceRange"])(project, false),
        displayType: "text",
        thousandSeparator: true,
        prefix: "$"
      })))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
        className: "pt-5 pb-0 px-2"
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("p", {
        className: "text-xs text-gray-900 font-light text-justify text-justify"
      }, "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
        className: "text-sm pb-4 px-2 flex justify-center items-center text-gray-900 text-justify"
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
        className: "w-1/3"
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("h4", {
        className: "text-palette-purple-main font-extrabold"
      }, "Goal"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("h5", {
        className: "font-light"
      }, "15 Investors")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
        className: "w-1/3 flex justify-center border-r border-l border-palette-purple-main"
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("h4", {
        className: "text-palette-purple-main font-extrabold"
      }, "Current"), " ", /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("h5", {
        className: "font-light"
      }, "8 Investors "))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
        className: "w-1/3 flex justify-end"
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("h4", {
        className: "text-palette-purple-main font-extrabold"
      }, "To Go"), " ", /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("h5", {
        className: "font-light"
      }, "8 Investors ")))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
        className: "w-full px-2"
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("button", {
        className: "bg-palette-violet-main-dark lg:py-2 lg:rounded py-3 rounded-sm text-white w-full",
        onClick: function onClick() {
          window.location = "/weekly-deals/".concat(project.id);
        }
      }, "Explore")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
        className: "pt-3 pb-4 px-5 hidden"
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
        className: "flex items-center justify-between border-b pb-2 mb-2"
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
        className: "text-base font-semibold text-gray-900"
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(react_currency_format__WEBPACK_IMPORTED_MODULE_9___default.a, {
        value: project && Object(_helpers_pricingHelper_discountHelper__WEBPACK_IMPORTED_MODULE_6__["discountedPriceRange"])(project, false),
        displayType: "text",
        thousandSeparator: true,
        prefix: "$"
      }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", null, " - "), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(react_currency_format__WEBPACK_IMPORTED_MODULE_9___default.a, {
        value: project && Object(_helpers_pricingHelper_discountHelper__WEBPACK_IMPORTED_MODULE_6__["discountedPriceRange"])(project, true),
        displayType: "text",
        thousandSeparator: true,
        prefix: "$"
      })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
        className: "text-red-600 text-base font-semibold ".concat(project.properties.filter(function (property) {
          return property.is_secured === false;
        }).length < 2 ? "text-red-700" : "")
      }, project.properties.filter(function (property) {
        return property.is_secured === false;
      }).length !== 0 ? project.properties.filter(function (property) {
        return property.is_secured === false;
      }).length : "", project.properties.filter(function (property) {
        return property.is_secured === false;
      }).length > 1 ? " Available" : project.properties.filter(function (property) {
        return property.is_secured === false;
      }).length === 0 ? "Sold out" : " Remaining")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
        className: "flex items-center justify-between text-gray-900 font-black"
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
        className: "text-base"
      }, project.deal_type), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
        className: "text-base flex"
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
        className: "cursor-pointer",
        onClick: function onClick() {
          return userAction.setState({
            showSocialMedia: true
          });
        }
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("svg", {
        className: "feather-icon h-5 w-5"
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("use", {
        xlinkHref: "/assets/svg/feather-sprite.svg#share"
      })))))))));
    }));
  };

  var renderFilters = function renderFilters() {
    return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
      className: "\n        lg:flex-row lg:px-16 lg:m-0\n        flex-col flex mt-8 mb-10 text-gray-600 px-8 items-center justify-center relative\n      "
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
      className: "flex-1 mb-4 lg:mb-0 w-full",
      style: {
        maxWidth: react_device_detect__WEBPACK_IMPORTED_MODULE_8__["isMobile"] ? "100%" : "25%"
      }
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
      className: "lg:block uppercase font-bold px-3 text-xs lg:absolute",
      style: {
        top: "-24px"
      }
    }, "Location"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
      className: "border-2 w-full h-full flex items-center"
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(react_select__WEBPACK_IMPORTED_MODULE_10__["default"], {
      isMulti: true,
      isClearable: false,
      closeMenuOnSelect: false,
      hideSelectedOptions: false,
      isSearchable: false,
      options: [{
        value: "All",
        label: "All"
      }, {
        value: "Adelaide",
        label: "Adelaide"
      }, {
        value: "Brisbane",
        label: "Brisbane"
      }, {
        value: "Gold Coast",
        label: "Gold Coast"
      }, {
        value: "Melbourne",
        label: "Melbourne"
      }, {
        value: "Newcastle",
        label: "Newcastle"
      }, {
        value: "Sydney",
        label: "Sydney"
      }],
      classNamePrefix: "input-select",
      className: "gb-multi-select h-full w-full",
      placeholder: "Select Location",
      value: filters.selectedLocations,
      onChange: handleSelectLocation
    }))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
      className: "flex-1 mb-4 lg:mb-0 w-full",
      style: {
        maxWidth: react_device_detect__WEBPACK_IMPORTED_MODULE_8__["isMobile"] ? "100%" : "25%"
      }
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
      className: "lg:block uppercase font-bold px-3 text-xs lg:absolute",
      style: {
        top: "-24px"
      }
    }, "Type"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
      className: "border-2 lg:border-l-0 w-full h-full flex items-center"
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(react_select__WEBPACK_IMPORTED_MODULE_10__["default"], {
      isMulti: true,
      isClearable: false,
      closeMenuOnSelect: false,
      hideSelectedOptions: false,
      isSearchable: false,
      options: options,
      classNamePrefix: "input-select",
      className: "gb-multi-select h-full w-full",
      placeholder: "Select Type",
      value: filters.selectedTypes,
      onChange: handleSelectType
    }))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
      className: "relative flex-1 mb-4 lg:mb-0 w-full",
      style: {
        maxWidth: react_device_detect__WEBPACK_IMPORTED_MODULE_8__["isMobile"] ? "100%" : "25%"
      }
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
      className: "lg:block uppercase font-bold px-3 text-xs lg:absolute",
      style: {
        top: "-24px"
      }
    }, "Price"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
      className: "border-2 lg:border-l-0 w-full h-full flex items-center cursor-pointer relative",
      onClick: function onClick() {
        return setPriceDropdown(!priceDropdown);
      }
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
      className: "px-3 py-2 text-base text-gray-500 w-full flex items-center justify-between"
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
      className: "flex items-center",
      style: {
        height: 36
      }
    }, filters.minPrice.label, " ~ ", filters.maxPrice.label), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("svg", {
      className: "feather-icon opacity-50 hover:opacity-100 ".concat(priceDropdown ? "opacity-100" : ""),
      style: {
        color: "hsl(0, 0%, 60%)",
        strokeWidth: "3px",
        width: 18,
        height: 18
      }
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("use", {
      xlinkHref: "/assets/svg/feather-sprite.svg#chevron-down"
    })))), priceDropdown && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_material_ui_core__WEBPACK_IMPORTED_MODULE_7__["ClickAwayListener"], {
      onClickAway: function onClickAway() {
        return setPriceDropdown(!priceDropdown);
      }
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
      className: "absolute bg-white border-2 p-2 z-50",
      style: {
        top: "calc(100% - 2px)",
        width: react_device_detect__WEBPACK_IMPORTED_MODULE_8__["isMobile"] ? "100%" : "calc(100% + 2px)",
        left: react_device_detect__WEBPACK_IMPORTED_MODULE_8__["isMobile"] ? 0 : "-2px"
      }
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
      className: "flex items-center mb-2"
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
      className: "font-bold mr-1 w-16"
    }, "Min Price"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(react_select__WEBPACK_IMPORTED_MODULE_10__["default"], {
      hideSelectedOptions: false,
      isSearchable: false,
      isClearable: true,
      options: priceOptions,
      classNamePrefix: "input-select",
      className: "gb-price-select flex-1 ml-2",
      placeholder: "Min Price",
      defaultValue: filters.minPrice,
      onChange: handleMinPrice
    })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
      className: "flex items-center"
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
      className: "font-bold mr-1 w-16"
    }, "Max Price"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(react_select__WEBPACK_IMPORTED_MODULE_10__["default"], {
      hideSelectedOptions: false,
      isSearchable: false,
      isClearable: true,
      options: priceOptions,
      classNamePrefix: "input-select",
      className: "gb-price-select flex-1 ml-2",
      placeholder: "Max Price",
      defaultValue: filters.maxPrice,
      onChange: handleMaxPrice
    }))))), !react_device_detect__WEBPACK_IMPORTED_MODULE_8__["isMobile"] && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
      className: "flex-1 w-full",
      style: {
        maxWidth: react_device_detect__WEBPACK_IMPORTED_MODULE_8__["isMobile"] ? "100%" : "25%"
      }
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
      className: "hidden lg:block uppercase font-bold px-3 text-xs absolute",
      style: {
        top: "-24px"
      }
    }, "Sort By"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
      className: "border-2 lg:border-l-0 w-full h-full flex items-center"
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(react_select__WEBPACK_IMPORTED_MODULE_10__["default"], {
      hideSelectedOptions: false,
      isSearchable: false,
      options: [{
        value: "deals.created_at",
        order: "desc",
        label: "Most Recent"
      }, {
        value: "deals.discount",
        order: "desc",
        label: "Biggest Discount"
      }, {
        value: "deals.expires_at",
        order: "asc",
        label: "Closing Soon"
      }, {
        value: "asc_properties_total_price",
        order: "asc",
        label: "Price (low-high)"
      }, {
        value: "desc_properties_total_price",
        order: "desc",
        label: "Price (high-low)"
      }],
      classNamePrefix: "input-select",
      className: "gb-multi-select h-full w-full",
      placeholder: "Sort By",
      value: filters.sortBy,
      onChange: handleSortBy
    }))));
  };

  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_layouts_users_UserLayout__WEBPACK_IMPORTED_MODULE_2__["default"], null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(react_helmet__WEBPACK_IMPORTED_MODULE_11__["Helmet"], null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("meta", {
    name: "description",
    content: _helpers_metaHelper_metaHelper__WEBPACK_IMPORTED_MODULE_12__["metaHelper"].desc
  })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("section", {
    className: "text-black bg-white"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("section", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "lg:grid lg:grid-cols-8 xs:block"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "lg:col-span-5 xs:block"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    style: {
      backgroundImage: "linear-gradient(rgba(0, 0, 0, 0), rgba(0, 0, 0, 0.8)), url('/assets/images/homepage_carrousel_b_4.jpg')"
    },
    className: "flex items-end w-full text-white bg-left bg-no-repeat bg-cover lg:h-full xs:h-screen"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "lg:pb-12 lg:pl-16 lg:pr-56 xs:pt-56 xs:px-8 xs:pb-8 space-y-4"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
    className: "font-bold leading-none lg:text-6xl xs:text-5xl"
  }, "Sanctuary"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("p", {
    className: "text-base font-light"
  }, "Dolor sit amet, consectetur adipiscing elit. Morbi ut turpis in velit ultricies dignissim. Morbi viverra libero quis est porttitor tincidunt. Nunc in metus sit amet felis feugiat tincidunt sit amet quis lectus. Vivamus tincidunt a ligula a malesuada. Aenean viverra lacus nulla, sit amet bibendum ipsum suscipit sit amet."), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("p", {
    className: "font-bold leading-none lg:text-3xl xs:text-2xl"
  }, "13% off"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("button", {
    className: "rounded-sm text-white text-xs font-semibold py-3 px-12",
    style: {
      backgroundColor: '#E91AFC'
    }
  }, "Explore")))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "lg:col-span-3 xs:block"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "h-full items-end bg-cover text-white bg-left bg-no-repeat flex w-full bg-palette-violet-main-dark"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "lg:pr-20 lg:pl-16 lg:pt-48 lg:pb-32 xs:px-8 xs:pt-12 xs:pb-8 space-y-4"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
    className: "font-bold leading-none lg:text-6xl xs:text-5xl"
  }, "Deal of the Week"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("p", {
    className: "text-base font-light"
  }, "We believe... dolor sit amet, consectetur adipiscing elit. Morbi ut turpis in velit ultricies dignissim. Morbi viverra libero quis est porttitor tincidunt. Nunc in metus sit amet felis feugiat tincidunt sit amet quis lectus. Vivamus tincidunt a ligula a malesuada. Aenean viverra lacus nulla, sit amet bibendum ipsum suscipit sit amet."), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("button", {
    className: "rounded-sm text-white text-xs font-semibold py-3 px-8",
    style: {
      backgroundColor: '#E91AFC'
    }
  }, "See the Properties")))))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components__WEBPACK_IMPORTED_MODULE_4__["TheMore"], {
    bgColor: "#211B24"
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("section", {
    className: "relative bg-white lg:px-24"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "pb-4 px-6 lg:px-0 mx-auto",
    style: {
      maxWidth: 1366
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("h4", {
    className: "w-full text-center mt-10 text-palette-purple-main font-semibold tracking-wider text-xs"
  }, "LOREM IPSUM"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("h1", {
    className: "text-left text-center leading-none font-bold text-4xl lg:text-5xl px-8 mb-4 lg:mb-10"
  }, "Current Popular Deals"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("p", {
    className: "text-justify mb-5  lg:px-32 lg:mb-16"
  }, "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Mi proin sed libero enim sed faucibus turpis. Amet consectetur adipiscing elit ut aliquam purus sit."), react_device_detect__WEBPACK_IMPORTED_MODULE_8__["isMobile"] && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "flex items-center justify-between"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    onClick: function onClick() {
      return setOpenSearch(!openSearch);
    },
    className: "uppercase font-bold text-palette-gray flex items-center"
  }, "Refine Search", /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("svg", {
    className: "\n                    ml-1 transition-all duration-300 feather-icon text-palette-gray transform\n                    ".concat(openSearch ? "rotate-45" : "", "\n                  ")
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("use", {
    xlinkHref: "/assets/svg/feather-sprite.svg#plus"
  }))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    ref: anchorRef,
    onClick: function onClick(e) {
      return setPopper(popper ? null : e.currentTarget);
    },
    className: "uppercase font-bold text-palette-gray flex items-center"
  }, filters.sortBy.label, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("svg", {
    className: "\n                    ml-1 transition-all duration-300 feather-icon text-palette-gray transform\n                    ".concat(popper ? "rotate-180" : "", "\n                  ")
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("use", {
    xlinkHref: "/assets/svg/feather-sprite.svg#chevron-down"
  }))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_material_ui_core__WEBPACK_IMPORTED_MODULE_7__["Popper"], {
    open: Boolean(popper),
    anchorEl: popper,
    placement: "bottom-end",
    transition: true,
    disablePortal: true,
    className: "z-50 menu-list"
  }, function (_ref2) {
    var TransitionProps = _ref2.TransitionProps;
    return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_material_ui_core__WEBPACK_IMPORTED_MODULE_7__["Grow"], _extends({}, TransitionProps, {
      style: {
        transformOrigin: "right top"
      }
    }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_material_ui_core__WEBPACK_IMPORTED_MODULE_7__["Paper"], {
      elevation: 2,
      className: "relative"
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_material_ui_core__WEBPACK_IMPORTED_MODULE_7__["MenuList"], null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_material_ui_core__WEBPACK_IMPORTED_MODULE_7__["MenuItem"], {
      onClick: function onClick() {
        setFilters(_objectSpread(_objectSpread({}, filters), {}, {
          sortBy: {
            value: "deals.created_at",
            order: "desc",
            label: "Most Recent"
          }
        }));
        setToggleFetch(!toggleFetch);
        setPopper(null);
      }
    }, "Most Recent"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_material_ui_core__WEBPACK_IMPORTED_MODULE_7__["MenuItem"], {
      onClick: function onClick() {
        setFilters(_objectSpread(_objectSpread({}, filters), {}, {
          sortBy: {
            value: "deals.discount",
            order: "desc",
            label: "Biggest Discount"
          }
        }));
        setToggleFetch(!toggleFetch);
        setPopper(null);
      }
    }, "Biggest Discount"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_material_ui_core__WEBPACK_IMPORTED_MODULE_7__["MenuItem"], {
      onClick: function onClick() {
        setFilters(_objectSpread(_objectSpread({}, filters), {}, {
          sortBy: {
            value: "deals.expires_at",
            order: "asc",
            label: "Closing Soon"
          }
        }));
        setToggleFetch(!toggleFetch);
        setPopper(null);
      }
    }, "Closing Soon"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_material_ui_core__WEBPACK_IMPORTED_MODULE_7__["MenuItem"], {
      onClick: function onClick() {
        setFilters(_objectSpread(_objectSpread({}, filters), {}, {
          sortBy: {
            value: "properties_total_price",
            order: "asc",
            label: "Price (low-high)"
          }
        }));
        setToggleFetch(!toggleFetch);
        setPopper(null);
      }
    }, "Price (low-high)"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_material_ui_core__WEBPACK_IMPORTED_MODULE_7__["MenuItem"], {
      onClick: function onClick() {
        setFilters(_objectSpread(_objectSpread({}, filters), {}, {
          sortBy: {
            value: "properties_total_price",
            order: "desc",
            label: "Price (high-low)"
          }
        }));
        setToggleFetch(!toggleFetch);
        setPopper(null);
      }
    }, "Price (high-low)"))));
  })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_material_ui_core__WEBPACK_IMPORTED_MODULE_7__["Collapse"], {
    "in": react_device_detect__WEBPACK_IMPORTED_MODULE_8__["isMobile"] ? openSearch : true
  }, renderFilters()), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "lg:px-20 mt-4 lg:mt-24 text-white m-auto",
    style: {
      maxWidth: 1336
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components_base__WEBPACK_IMPORTED_MODULE_5__["Table"], (_React$createElement = {
    query: "/api/deal?approved=true&",
    queryParams: "&projects=true&types=".concat(filters.types, "&locations=").concat(filters.locations, "&min_price=").concat(filters.minPrice.value, "&max_price=").concat(filters.maxPrice.value) // queryParams={`&projects=true&types=${filters.types}`}
    ,
    toggleFetch: toggleFetch
  }, _defineProperty(_React$createElement, "toggleFetch", toggleFetch), _defineProperty(_React$createElement, "keyword", keyword), _defineProperty(_React$createElement, "getData", setProjectData), _defineProperty(_React$createElement, "content", renderProjects()), _defineProperty(_React$createElement, "sort", filters.sortBy.value.replace(/asc_|desc_/g, "") || ""), _defineProperty(_React$createElement, "order", filters.sortBy.order || ""), _React$createElement))))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("section", {
    className: "lg:flex xs:block"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "lg:w-1/2 xs:w-full",
    style: {
      backgroundColor: "#4D0B98"
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components__WEBPACK_IMPORTED_MODULE_4__["RegistrationForm"], {
    subHeaderColor: "text-white",
    headerColor: "text-palette-purple-main",
    isPopup: false,
    opacity: "100%",
    containerBgColor: "#4D0B98"
  })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components__WEBPACK_IMPORTED_MODULE_4__["BenefitsForBuyers"], null)), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components__WEBPACK_IMPORTED_MODULE_4__["Testimonials"], null)), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components__WEBPACK_IMPORTED_MODULE_4__["SocialMediaShareModal"], null));
};

/* harmony default export */ __webpack_exports__["default"] = (BuyingListPage);

/***/ })

}]);