(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["BuyingConfirmationPage"],{

/***/ "./resources/js/pages/BuyingConfirmationPage.js":
/*!******************************************************!*\
  !*** ./resources/js/pages/BuyingConfirmationPage.js ***!
  \******************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime/regenerator */ "./node_modules/@babel/runtime/regenerator/index.js");
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _layouts_users_UserLayout__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../layouts/users/UserLayout */ "./resources/js/layouts/users/UserLayout.js");
/* harmony import */ var _states_userGlobal__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../states/userGlobal */ "./resources/js/states/userGlobal.js");
/* harmony import */ var _components__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../components */ "./resources/js/components/index.js");
/* harmony import */ var _components__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(_components__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var _services_auth__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../services/auth */ "./resources/js/services/auth.js");
/* harmony import */ var _fortawesome_react_fontawesome__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @fortawesome/react-fontawesome */ "./node_modules/@fortawesome/react-fontawesome/index.es.js");
/* harmony import */ var _components_base_alerts_BaseAlert__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../components/_base/alerts/BaseAlert */ "./resources/js/components/_base/alerts/BaseAlert.js");
/* harmony import */ var react_currency_format__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! react-currency-format */ "./node_modules/react-currency-format/lib/currency-format.js");
/* harmony import */ var react_currency_format__WEBPACK_IMPORTED_MODULE_8___default = /*#__PURE__*/__webpack_require__.n(react_currency_format__WEBPACK_IMPORTED_MODULE_8__);
/* harmony import */ var react_device_detect__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! react-device-detect */ "./node_modules/react-device-detect/main.js");
/* harmony import */ var react_device_detect__WEBPACK_IMPORTED_MODULE_9___default = /*#__PURE__*/__webpack_require__.n(react_device_detect__WEBPACK_IMPORTED_MODULE_9__);
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! sweetalert2 */ "./node_modules/sweetalert2/dist/sweetalert2.all.js");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_10___default = /*#__PURE__*/__webpack_require__.n(sweetalert2__WEBPACK_IMPORTED_MODULE_10__);
/* harmony import */ var _components_base__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ../components/_base */ "./resources/js/components/_base/index.js");
/* harmony import */ var _components_base__WEBPACK_IMPORTED_MODULE_11___default = /*#__PURE__*/__webpack_require__.n(_components_base__WEBPACK_IMPORTED_MODULE_11__);
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! lodash */ "./node_modules/lodash/lodash.js");
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_12___default = /*#__PURE__*/__webpack_require__.n(lodash__WEBPACK_IMPORTED_MODULE_12__);
/* harmony import */ var _data_index__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ../data/index */ "./resources/js/data/index.js");
/* harmony import */ var _helpers_deviceSizeHelper__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ../helpers/deviceSizeHelper */ "./resources/js/helpers/deviceSizeHelper.js");
/* harmony import */ var _helpers_salesAdviceHelper_salesAdviceHelper__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! ../helpers/salesAdviceHelper/salesAdviceHelper */ "./resources/js/helpers/salesAdviceHelper/salesAdviceHelper.js");
/* harmony import */ var _components_base_alerts_sweetAlert__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! ../components/_base/alerts/sweetAlert */ "./resources/js/components/_base/alerts/sweetAlert.js");


function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _unsupportedIterableToArray(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

function _iterableToArrayLimit(arr, i) { if (typeof Symbol === "undefined" || !(Symbol.iterator in Object(arr))) return; var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }




















var BuyingConfirmationPage = function BuyingConfirmationPage(_ref) {
  var dealId = _ref.dealId,
      propertyId = _ref.propertyId;

  var _UserGlobal = Object(_states_userGlobal__WEBPACK_IMPORTED_MODULE_3__["default"])(),
      _UserGlobal2 = _slicedToArray(_UserGlobal, 2),
      userState = _UserGlobal2[0],
      userAction = _UserGlobal2[1];

  var _useState = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(false),
      _useState2 = _slicedToArray(_useState, 2),
      loading = _useState2[0],
      setLoading = _useState2[1];

  var _useState3 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])([]),
      _useState4 = _slicedToArray(_useState3, 2),
      purchaserErrors = _useState4[0],
      setPurchaserErrors = _useState4[1];

  var _useState5 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])({}),
      _useState6 = _slicedToArray(_useState5, 2),
      solicitorErrors = _useState6[0],
      setSolicitorErrors = _useState6[1];

  var _useState7 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])({}),
      _useState8 = _slicedToArray(_useState7, 2),
      additionalInfoErrors = _useState8[0],
      setAddtionalInfoErrors = _useState8[1];

  var _useState9 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(_helpers_salesAdviceHelper_salesAdviceHelper__WEBPACK_IMPORTED_MODULE_15__["defaultPurchasers"]),
      _useState10 = _slicedToArray(_useState9, 2),
      purchasers = _useState10[0],
      setPurchasers = _useState10[1];

  var solicitorForm = /*#__PURE__*/Object(react__WEBPACK_IMPORTED_MODULE_1__["createRef"])();
  var additionalInfoForm = /*#__PURE__*/Object(react__WEBPACK_IMPORTED_MODULE_1__["createRef"])();

  var _useState11 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])({}),
      _useState12 = _slicedToArray(_useState11, 2),
      currentProject = _useState12[0],
      setProject = _useState12[1];

  var _useState13 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])({}),
      _useState14 = _slicedToArray(_useState13, 2),
      apartment = _useState14[0],
      setApartment = _useState14[1];

  Object(react__WEBPACK_IMPORTED_MODULE_1__["useEffect"])(function () {
    if (!Object(_services_auth__WEBPACK_IMPORTED_MODULE_5__["isLoggedIn"])()) {
      window.location = "/weekly-deals";
    }

    getDeal();
  }, []);

  var getDeal = /*#__PURE__*/function () {
    var _ref2 = _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee() {
      var url;
      return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee$(_context) {
        while (1) {
          switch (_context.prev = _context.next) {
            case 0:
              setLoading(true);
              url = "/api/deal/".concat(dealId);
              Object(_data_index__WEBPACK_IMPORTED_MODULE_13__["getDealByID"])({
                url: url
              }).then(function (res) {
                setProject(res.data);
                var property = res.data.properties.filter(function (property) {
                  return property.id === propertyId;
                })[0];
                setApartment(property || {});
                setLoading(false);
              });

            case 3:
            case "end":
              return _context.stop();
          }
        }
      }, _callee);
    }));

    return function getDeal() {
      return _ref2.apply(this, arguments);
    };
  }();

  var handleSubmit = function handleSubmit(isSubmit) {
    var solicitorFormData = new FormData(solicitorForm.current);
    var additionalInfoFormData = new FormData(additionalInfoForm.current);
    sweetalert2__WEBPACK_IMPORTED_MODULE_10___default.a.fire({
      title: "".concat(isSubmit ? "Submit" : "Save", " sales advice"),
      text: "Are you sure you want to ".concat(isSubmit ? "Submit" : "Save", " sales advice?"),
      type: "info",
      showCancelButton: true,
      confirmButtonText: "Yes!",
      cancelButtonText: "No"
    }).then(function (result) {
      if (result.value) {
        setLoading(true);
        var purchaserArr = [];
        var solicitorAddress = {
          suburb: solicitorFormData.get("suburb"),
          state: solicitorFormData.get("state"),
          country: solicitorFormData.get("country"),
          postcode: solicitorFormData.get("postcode")
        };
        solicitorFormData.append("propertyId", propertyId);
        solicitorFormData.append("address", JSON.stringify(solicitorAddress));
        var phone = solicitorFormData.get("phone") ? "+61" + solicitorFormData.get("phone") : "";
        solicitorFormData.set("phone", phone);
        purchasers.forEach(function (purchaser) {
          var data = {};
          data.address = {
            address_line_1: purchaser.address_line_1,
            suburb: purchaser.suburb,
            country: purchaser.country,
            state: purchaser.state,
            postcode: purchaser.postcode
          };
          data.address_line_1 = purchaser.address_line_1;
          data.suburb = purchaser.suburb;
          data.country = purchaser.country;
          data.state = purchaser.state;
          data.title = purchaser.title;
          data.postcode = purchaser.postcode;
          data.phone = purchaser["phone"] ? "+61" + purchaser["phone"] : "";
          data.alternate_phone = purchaser["alternate_phone"] ? "+61" + purchaser["alternate_phone"] : "";
          data.email = purchaser["email"];
          data.first_name = purchaser["first_name"];
          data.last_name = purchaser["last_name"];
          data.propertyId = purchaser["propertyId"];
          data.id = purchaser["id"];
          purchaserArr.push(data);
        });
        var param = {
          propertyId: propertyId,
          purchasers: purchaserArr,
          solicitor: Object(lodash__WEBPACK_IMPORTED_MODULE_12__["fromPairs"])(Array.from(solicitorFormData.entries())),
          additionalInfo: Object(lodash__WEBPACK_IMPORTED_MODULE_12__["fromPairs"])(Array.from(additionalInfoFormData.entries())),
          isSubmit: isSubmit
        };

        try {
          axios.post("/api/sales-advice", param).then(function (res) {
            setLoading(false);

            if (res.status === 200 && !res.data.errorCode) {
              setPurchasers(Object(_helpers_salesAdviceHelper_salesAdviceHelper__WEBPACK_IMPORTED_MODULE_15__["purchasersMapper"])(res.data[0].original.purchasers));
              setSolicitorErrors([]);
              setPurchaserErrors([]);
              Object(_components_base_alerts_sweetAlert__WEBPACK_IMPORTED_MODULE_16__["sweetAlert"])("success", "Sales advice successfully saved.");
            }

            if (res.data.errorCode === 422) {
              setLoading(false);

              if (!Object(lodash__WEBPACK_IMPORTED_MODULE_12__["isEmpty"])(res.data.solicitorValidator)) {
                setSolicitorErrors(res.data.solicitorValidator);
              }

              if (!Object(lodash__WEBPACK_IMPORTED_MODULE_12__["isEmpty"])(res.data.purchaserValidator)) {
                setPurchaserErrors(res.data.purchaserValidator);
              }
            }
          });
        } catch (error) {
          setLoading(false);
        }
      }
    });
  };

  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_layouts_users_UserLayout__WEBPACK_IMPORTED_MODULE_2__["default"], null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("section", {
    className: "bg-palette-blue-dark",
    style: {
      marginTop: -130,
      paddingTop: 160
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("section", {
    className: "relative bg-white text-gray-800"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components__WEBPACK_IMPORTED_MODULE_4__["BuyingProgress"], {
    current: 5,
    isPreview: false
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "flex flex-col lg:flex-row lg:px-10 mt-8 pb-16 mx-auto",
    style: {
      maxWidth: 1366
    }
  }, !react_device_detect__WEBPACK_IMPORTED_MODULE_9__["isMobile"] && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "lg:w-1/5 py-2 text-center text-gray-700"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "relative"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("a", {
    href: "/weekly-deals/".concat(dealId, "/false"),
    className: "bg-white cursor-pointer duration-300 font-bold hover:text-palette-blue inline-flex items-center pr-3 relative text-palette-blue-light text-sm transition-all z-10"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("svg", {
    className: "feather-icon h-4 w-4 mr-1 inline"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("use", {
    xlinkHref: "/assets/svg/feather-sprite.svg#chevron-left"
  })), "Back to project details"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "absolute border-gray-200 border-t w-full",
    style: {
      top: 10
    }
  })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "flex flex-col items-center"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "text-center mt-5"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("h2", {
    className: "font-bold font-serif leading-6 mb-2 text-3xl"
  }, "The", /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("br", null), "Orchards"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
    className: "font-black text-base"
  }, "Northwest")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "mt-2 text-center"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("img", {
    src: "/assets/images/_directors/luke-hayes.jpg",
    className: "mx-auto rounded-full",
    style: {
      width: 120
    }
  })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "text-center mt-2 text-center"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "text-base text-gray-900 mt-2 font-bold"
  }, !Object(lodash__WEBPACK_IMPORTED_MODULE_12__["isEmpty"])(currentProject) && currentProject.agent_name), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "border rounded my-1 border-gray-700 font-normal text-base text-center text-gray-900 inline-block"
  }, !Object(lodash__WEBPACK_IMPORTED_MODULE_12__["isEmpty"])(currentProject) && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("a", {
    className: "px-4",
    href: "tel:".concat(currentProject.agent_phone)
  }, currentProject && currentProject.agent_phone)), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "font-bold text-base text-gray-900 truncate w-48 hover:text-palette-purple duration-300 transition-all"
  }, !Object(lodash__WEBPACK_IMPORTED_MODULE_12__["isEmpty"])(currentProject) && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("a", {
    href: "mailto:".concat(currentProject.agent_email)
  }, currentProject && currentProject.agent_email))))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "lg:ml-16 px-6 lg:px-0"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "flex flex-col lg:w-9/12"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components_base_alerts_BaseAlert__WEBPACK_IMPORTED_MODULE_7__["default"], {
    message: "Payment successful",
    icon: "check",
    type: "success"
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("h1", {
    className: "font-bold flex flex-col lg:flex-row text-4xl mb-4 lg:mb-2"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", null, "Congratulations!\xA0"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
    className: "text-2xl lg:text-4xl"
  }, "You just saved\xA0", /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(react_currency_format__WEBPACK_IMPORTED_MODULE_8___default.a, {
    value: apartment.price * (currentProject.discount / 100),
    displayType: "text",
    thousandSeparator: true,
    prefix: "$"
  }))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "text-lg mb-4 lg:mb-2"
  }, "You have just secured", " ", /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
    className: "font-bold"
  }, "Apartment ", apartment && apartment.unit_name, " at", " ", currentProject.name))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("hr", {
    className: "hidden lg:block border border-gray-300 mb-3"
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "flex flex-col lg:flex-row"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "lg:w-9/12 lg:mr-10"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("p", {
    className: "text-base font-bold lg:pr-24"
  }, "An email has now been sent to you with the following details. Your project agent", " ", " ".concat(currentProject.agent_name && currentProject.agent_name.split(" ")[0], " "), " ", "will be in contact shortly."), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "mt-6 mb-12"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("p", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
    className: "text-base font-bold"
  }, "Your agent's details:"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("br", null), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
    className: "text-base"
  }, currentProject.agent_name, " |", " ", currentProject.agent_phone, " |", " "), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("a", {
    href: "mailto:".concat(currentProject.agent_email),
    className: "text-palette-blue-light text-base"
  }, currentProject.agent_email))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "flex flex-row"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "bg-blue-100 leading-6 lg:p-10 p-6 rounded-lg"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("h2", {
    className: "font-black font-bold text-2xl mb-4"
  }, "Next steps"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("p", {
    className: "text-base font-bold"
  }, "1. In the next 48 hours submit your Sales Advice with the details of your purchase."), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("p", {
    className: "text-base font-bold"
  }, "2. Once all 5 apartments are sold within your project your GroupBuyer deal is valid."), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("p", {
    className: "text-base font-bold"
  }, "3. The Vendor's Solicitor will issue your individual contract for sale (to your Solicitor)."), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("p", {
    className: "text-base font-bold"
  }, "4. Sign your contract and pay your property deposit."), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("p", {
    className: "text-base font-bold"
  }, "5. Exchange contracts via your solicitor and/or GroupBuyer agent."), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "text-base"
  }, "Your GroupBuyer agent will be in contact shortly to assist you every step of the way. If you need to come back to your Sales Advice form later you can find it in your ", /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
    className: "font-bold"
  }, "Profile"), " under", " ", /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
    className: "font-bold"
  }, "\xA0'Secured deals'"), ".")))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "lg:w-3/12 px-12 lg:px-0 relative"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "font-bold my-3 text-base text-palette-gray text-center lg:text-left uppercase"
  }, "Resources"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("ul", null, !Object(lodash__WEBPACK_IMPORTED_MODULE_12__["isEmpty"])(currentProject) && currentProject.resources.length > 0 && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components__WEBPACK_IMPORTED_MODULE_4__["ResourcesComponent"], {
    project: currentProject
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
    onClick: function onClick() {
      return userAction.setState({
        showSocialMedia: true
      });
    },
    className: "mb-3 cursor-pointer flex items-center bg-gray-200 block font-bold px-3 py-1 leading-relaxed text-base rounded text-palette-blue-light"
  }, "Share this project", /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("svg", {
    className: "feather-icon h-4 w-4 ml-2"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("use", {
    xlinkHref: "/assets/svg/feather-sprite.svg#share"
  }))))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "bg-gray-200 bottom-0 leading-6 px-4 py-6 rounded self-end"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("h2", {
    className: "font-black font-bold  text-lg  "
  }, "Connect with us..."), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("p", {
    className: "font-hairline text-base text-sm"
  }, "Check our reviews, project updates, latest projects and more")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "flex justify-center lg:justify-start text-white"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components__WEBPACK_IMPORTED_MODULE_4__["SocialMedia"], {
    size: "2x"
  }))))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components__WEBPACK_IMPORTED_MODULE_4__["PurchaserDetails"], {
    setPurchasers: setPurchasers,
    purchasers: purchasers,
    errors: purchaserErrors,
    loading: loading,
    width: "lg:w-9/12"
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "flex justify-center mt-8 ".concat(userState.windowSize <= _helpers_deviceSizeHelper__WEBPACK_IMPORTED_MODULE_14__["xtraSmall"] ? "flex-1 flex-col" : "")
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components_base__WEBPACK_IMPORTED_MODULE_11__["Button"], {
    onClick: function onClick() {
      return handleSubmit(false);
    },
    className: "".concat(loading ? "bg-gray-500" : "bg-palette-blue-light", " text-white mr-2 block button button-primary rounded-full"),
    disabled: loading
  }, loading && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_fortawesome_react_fontawesome__WEBPACK_IMPORTED_MODULE_6__["FontAwesomeIcon"], {
    icon: ["fas", "spinner"],
    className: "fa-spin mr-2"
  }), "Save"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components_base__WEBPACK_IMPORTED_MODULE_11__["Button"], {
    className: "bg-red-600 button button-primary font-bold mr-2 rounded-full h-10\n                  ".concat(userState.windowSize <= _helpers_deviceSizeHelper__WEBPACK_IMPORTED_MODULE_14__["xtraSmall"] ? "mt-2" : "mt-0"),
    disabled: loading
  }, "Cancel"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components_base__WEBPACK_IMPORTED_MODULE_11__["Button"], {
    onClick: function onClick() {
      return handleSubmit(true);
    },
    className: "block button button-primary rounded-full\n                  ".concat(userState.windowSize <= _helpers_deviceSizeHelper__WEBPACK_IMPORTED_MODULE_14__["xtraSmall"] ? "mt-2" : "mt-0"),
    disabled: loading
  }, loading && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_fortawesome_react_fontawesome__WEBPACK_IMPORTED_MODULE_6__["FontAwesomeIcon"], {
    icon: ["fas", "spinner"],
    className: "fa-spin mr-2"
  }), "Submit")))), !Object(lodash__WEBPACK_IMPORTED_MODULE_12__["isEmpty"])(currentProject) && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components__WEBPACK_IMPORTED_MODULE_4__["AgentDetailsSection"], {
    project: currentProject
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components__WEBPACK_IMPORTED_MODULE_4__["SocialMediaShareModal"], null))));
};

/* harmony default export */ __webpack_exports__["default"] = (BuyingConfirmationPage);

/***/ })

}]);