(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["AboutPage~BuyingApartmentDetailsPage~BuyingConfirmationPage~BuyingListPage~BuyingProjectDetailsPage~~e7af3a51"],{

/***/ "./node_modules/css-loader/index.js?!./node_modules/postcss-loader/src/index.js?!./resources/js/helpers/styles/editor.css":
/*!********************************************************************************************************************************!*\
  !*** ./node_modules/css-loader??ref--6-1!./node_modules/postcss-loader/src??ref--6-2!./resources/js/helpers/styles/editor.css ***!
  \********************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(/*! ../../../../node_modules/css-loader/lib/css-base.js */ "./node_modules/css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ".RichEditor-root {\n  background: #fff;\n  border: 1px solid #ddd;\n  /* font-family: \"Georgia\", serif; */\n  font-size: 14px;\n}\n\n.RichEditor-editor {\n  width: 100%;\n  height: 200px;\n  overflow-y: auto;\n  overflow-x: hidden;\n  cursor: text;\n  font-size: 14px;\n  padding-left: 4px;\n  padding-right: 4px;\n}\n\n.RichEditor-editor .public-DraftEditorPlaceholder-root,\r\n.RichEditor-editor .public-DraftEditor-content {\n  margin: 0 -15px -15px;\n  padding-right: 15px;\n  padding-left: 15px;\n  padding-bottom: 15px;\n}\n\n.RichEditor-editor .public-DraftEditor-content {\n  min-height: 100px;\n}\n\n.RichEditor-hidePlaceholder .public-DraftEditorPlaceholder-root {\n  display: none;\n}\n\n.RichEditor-editor .RichEditor-blockquote {\n  border-left: 5px solid #eee;\n  color: #666;\n  font-family: \"Hoefler Text\", \"Georgia\", serif;\n  font-style: italic;\n  margin: 16px 0;\n  padding: 10px 20px;\n}\n\n.RichEditor-editor .public-DraftStyleDefault-pre {\n  background-color: rgba(0, 0, 0, 0.05);\n  font-family: \"Inconsolata\", \"Menlo\", \"Consolas\", monospace;\n  font-size: 16px;\n  padding: 20px;\n}\n\n.RichEditor-controls {\n  font-family: \"Helvetica\", sans-serif;\n  font-size: 14px;\n  padding-left: 15px;\n  padding-top: 4px;\n  padding-bottom: 4px;\n  margin-bottom: 4px;\n  -webkit-user-select: none;\n     -moz-user-select: none;\n      -ms-user-select: none;\n          user-select: none;\n}\n\n.RichEditor-styleButton {\n  color: #999;\n  cursor: pointer;\n  margin-right: 16px;\n  padding: 2px 0;\n  display: inline-block;\n}\n\n.RichEditor-activeButton {\n  color: #5890ff;\n}\n\n.Richeditor-counter {\n  color: black;\n  width: 100%;\n}\r\n", ""]);

// exports


/***/ }),

/***/ "./resources/js/components/_base sync .*\\.(js)$":
/*!*******************************************************************!*\
  !*** ./resources/js/components/_base sync nonrecursive .*\.(js)$ ***!
  \*******************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"./Button.js": "./resources/js/components/_base/Button.js",
	"./Form.js": "./resources/js/components/_base/Form.js",
	"./Modal.js": "./resources/js/components/_base/Modal.js",
	"./RichTextEditor.js": "./resources/js/components/_base/RichTextEditor.js",
	"./Table.js": "./resources/js/components/_base/Table.js",
	"./TextArea.js": "./resources/js/components/_base/TextArea.js",
	"./TextInput.js": "./resources/js/components/_base/TextInput.js",
	"./Tooltip.js": "./resources/js/components/_base/Tooltip.js",
	"./index.js": "./resources/js/components/_base/index.js"
};


function webpackContext(req) {
	var id = webpackContextResolve(req);
	return __webpack_require__(id);
}
function webpackContextResolve(req) {
	if(!__webpack_require__.o(map, req)) {
		var e = new Error("Cannot find module '" + req + "'");
		e.code = 'MODULE_NOT_FOUND';
		throw e;
	}
	return map[req];
}
webpackContext.keys = function webpackContextKeys() {
	return Object.keys(map);
};
webpackContext.resolve = webpackContextResolve;
module.exports = webpackContext;
webpackContext.id = "./resources/js/components/_base sync .*\\.(js)$";

/***/ }),

/***/ "./resources/js/components/_base/Button.js":
/*!*************************************************!*\
  !*** ./resources/js/components/_base/Button.js ***!
  \*************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

function _objectWithoutProperties(source, excluded) { if (source == null) return {}; var target = _objectWithoutPropertiesLoose(source, excluded); var key, i; if (Object.getOwnPropertySymbols) { var sourceSymbolKeys = Object.getOwnPropertySymbols(source); for (i = 0; i < sourceSymbolKeys.length; i++) { key = sourceSymbolKeys[i]; if (excluded.indexOf(key) >= 0) continue; if (!Object.prototype.propertyIsEnumerable.call(source, key)) continue; target[key] = source[key]; } } return target; }

function _objectWithoutPropertiesLoose(source, excluded) { if (source == null) return {}; var target = {}; var sourceKeys = Object.keys(source); var key, i; for (i = 0; i < sourceKeys.length; i++) { key = sourceKeys[i]; if (excluded.indexOf(key) >= 0) continue; target[key] = source[key]; } return target; }



var Button = function Button(_ref) {
  var onClick = _ref.onClick,
      className = _ref.className,
      children = _ref.children,
      _ref$type = _ref.type,
      type = _ref$type === void 0 ? "primary" : _ref$type,
      rest = _objectWithoutProperties(_ref, ["onClick", "className", "children", "type"]);

  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("button", _extends({
    className: "button button-".concat(type, " ").concat(className),
    onClick: onClick
  }, rest), children);
};

/* harmony default export */ __webpack_exports__["default"] = (Button);

/***/ }),

/***/ "./resources/js/components/_base/Form.js":
/*!***********************************************!*\
  !*** ./resources/js/components/_base/Form.js ***!
  \***********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var pretty_checkbox_react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! pretty-checkbox-react */ "./node_modules/pretty-checkbox-react/dist/pretty-checkbox-react.umd.min.js");
/* harmony import */ var pretty_checkbox_react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(pretty_checkbox_react__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _TextInput__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./TextInput */ "./resources/js/components/_base/TextInput.js");
/* harmony import */ var react_select__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! react-select */ "./node_modules/react-select/dist/react-select.browser.esm.js");
/* harmony import */ var _TextArea__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./TextArea */ "./resources/js/components/_base/TextArea.js");
/* harmony import */ var _helpers_numberInput__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../helpers/numberInput */ "./resources/js/helpers/numberInput.js");
/* harmony import */ var react_currency_format__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! react-currency-format */ "./node_modules/react-currency-format/lib/currency-format.js");
/* harmony import */ var react_currency_format__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(react_currency_format__WEBPACK_IMPORTED_MODULE_6__);
function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _unsupportedIterableToArray(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

function _iterableToArrayLimit(arr, i) { if (typeof Symbol === "undefined" || !(Symbol.iterator in Object(arr))) return; var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }









var Form = function Form(_ref) {
  var errors = _ref.errors,
      formFields = _ref.formFields,
      _ref$data = _ref.data,
      data = _ref$data === void 0 ? {} : _ref$data;

  var Field = function Field(key) {
    var _React$createElement;

    var type = formFields[key].type;
    var inputFile = Object(react__WEBPACK_IMPORTED_MODULE_0__["useRef"])(null);

    var _useState = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])(""),
        _useState2 = _slicedToArray(_useState, 2),
        filename = _useState2[0],
        setFilename = _useState2[1];

    var placeholder = typeof formFields[key].placeholder !== "undefined" ? formFields[key].placeholder : "Enter ".concat(formFields[key].label || key.replace("_", " "));

    switch (type) {
      case "select":
        var handleDefaultValue = function handleDefaultValue() {
          var value = formFields[key].value;
          var selected = null;
          Object.keys(formFields[key].options).forEach(function (optionKey) {
            if (formFields[key].options[optionKey].value === value) {
              selected = formFields[key].options[optionKey];
            }
          });
          return selected;
        };

        return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_select__WEBPACK_IMPORTED_MODULE_3__["default"], {
          isOptionSelected: true,
          name: key,
          classNamePrefix: "input-select",
          onChange: formFields[key].handleChange,
          className: "w-full",
          placeholder: placeholder // value={formFields[key].value}
          ,
          options: formFields[key].options,
          defaultValue: formFields[key].options.find(function (o) {
            return o.value === formFields[key].value;
          })
        });
      // case "date":
      //   return (
      //     <Flatpickr
      //       className={`text-sm border border-gray-400 focus:border-primary shadow-sm appearance-none rounded-sm px-3 py-2 w-full`}
      //       name={key}
      //       options={{
      //         altInput: true,
      //         altFormat: "F j, Y",
      //         dateFormat: "Y-m-d",
      //         static: true,
      //         defaultDate: formFields[key].value || data[key] || ``,
      //       }}
      //       placeholder={placeholder}
      //     />
      //   )

      case "file":
        var handleFile = function handleFile() {
          var name = "";

          if (inputFile.current.files.length) {
            var files = Array.from(inputFile.current.files);
            name = files[0].name;
          }

          setFilename(name);
        };

        var clearFile = function clearFile() {
          inputFile.current.type = "";
          inputFile.current.type = "file";
          setFilename("");
        };

        return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
          className: "c-file-input js-file-input bg-gray-100 hover:bg-gray-200 ".concat(filename && "has-file")
        }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
          className: "c-file-input__indicator"
        }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
          className: "c-file-input__indicator__icon c-icon c-icon--attach"
        }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("i", {
          className: "fa fa-paperclip"
        }))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("label", {
          className: "cursor-pointer c-file-input__label js-file-input__label ".concat(filename && "text-primary")
        }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", null, filename || "No file choosen"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("input", {
          ref: inputFile,
          type: "file",
          name: formFields[key].name || key,
          accept: formFields[key].accept,
          onChange: handleFile,
          className: "c-file-input__field js-file-input__field"
        })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
          className: "c-file-input__remove js-file-input__remove",
          onClick: clearFile
        }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
          className: "c-file-input__remove__icon c-icon c-icon--remove-circle"
        }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("i", {
          className: "fa fa-minus-circle"
        }))));

      case "radio":
        var radioWrapperClass = formFields[key].classname || "flex flex-col";
        return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react__WEBPACK_IMPORTED_MODULE_0___default.a.Fragment, null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("section", {
          className: radioWrapperClass
        }, Object.keys(formFields[key].options).map(function (optionKey) {
          return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(pretty_checkbox_react__WEBPACK_IMPORTED_MODULE_1__["Radio"], _extends({
            key: optionKey,
            name: key,
            value: optionKey,
            inputProps: {
              defaultChecked: formFields[key].value === optionKey
            }
          }, formFields[key].methods, {
            color: "success-o"
          }), formFields[key].options[optionKey]);
        })), key === "isFIRB_required" && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
          className: "ml-6 mt-2"
        }, "(Foreign Investment Review Board)"));

      case "textarea":
        return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_TextArea__WEBPACK_IMPORTED_MODULE_4__["default"], (_React$createElement = {
          className: "\n                ".concat(formFields[key]["class"], "\n                ").concat(formFields[key].width || "w-full", "\n                border rounded p-3 mt-0\n              "),
          placeholder: placeholder,
          name: key,
          defaultValue: formFields[key].value || data[key] || "",
          appearance: true
        }, _defineProperty(_React$createElement, "name", key), _defineProperty(_React$createElement, "autoComplete", "off"), _React$createElement));

      default:
        return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react__WEBPACK_IMPORTED_MODULE_0___default.a.Fragment, null, formFields[key].prefix && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
          className: "px-4 bg-gray-200 flex items-center justify-center rounded-l border-l border-t border-b border-gray-400"
        }, "+61"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_TextInput__WEBPACK_IMPORTED_MODULE_2__["default"], {
          type: formFields[key].type || "text",
          step: "any",
          className: "\n              ".concat(formFields[key].prefix ? "border-l-0 rounded-l-none px-2" : "", "\n                ").concat(formFields[key]["class"], "\n                ").concat(formFields[key].width || "w-full", "\n              "),
          name: key,
          autoComplete: "off" // onChange={(e) => onChange(e)}
          ,
          defaultValue: formFields[key].value || data[key] || "",
          placeholder: placeholder,
          minLength: formFields[key].minlength || "",
          maxLength: formFields[key].maxlength || "",
          border: false,
          appearance: true,
          onKeyDown: function onKeyDown(evt) {
            return (evt.key === "e" || evt.key === "." || evt.key === "-") && formFields[key].type === "number" && evt.preventDefault();
          }
        }));
    }
  };

  return Object.keys(formFields).map(function (value, key) {
    return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: "mb-5 text-base flex",
      key: formFields[value].id ? "".concat(formFields[value].id).concat(key) : key
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("label", {
      className: "mr-3 ".concat(formFields[value].type !== "radio" ? "w-40" : "w-1/2", " mt-2 capitalize")
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
      className: "font-bold"
    }, formFields[value].label || value.replace("_", " ")), formFields[value].required && key !== "alternate_phone" && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
      className: "text-red-500"
    }, " *")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: "w-full"
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: "flex"
    }, Field(value)), errors[value] && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
      className: "text-red-500 text-sm"
    }, errors[value])));
  });
};

/* harmony default export */ __webpack_exports__["default"] = (Form);

/***/ }),

/***/ "./resources/js/components/_base/Modal.js":
/*!************************************************!*\
  !*** ./resources/js/components/_base/Modal.js ***!
  \************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _material_ui_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @material-ui/core */ "./node_modules/@material-ui/core/esm/index.js");
/* harmony import */ var _material_ui_icons_Close__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @material-ui/icons/Close */ "./node_modules/@material-ui/icons/Close.js");
/* harmony import */ var _material_ui_icons_Close__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(_material_ui_icons_Close__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var react_device_detect__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! react-device-detect */ "./node_modules/react-device-detect/main.js");
/* harmony import */ var react_device_detect__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(react_device_detect__WEBPACK_IMPORTED_MODULE_3__);
function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }






var Modal = function Modal(_ref) {
  var _ref$disableBackdropC = _ref.disableBackdropClick,
      disableBackdropClick = _ref$disableBackdropC === void 0 ? false : _ref$disableBackdropC,
      _ref$topOff = _ref.topOff,
      topOff = _ref$topOff === void 0 ? false : _ref$topOff,
      _ref$allowOutsideInpu = _ref.allowOutsideInput,
      allowOutsideInput = _ref$allowOutsideInpu === void 0 ? false : _ref$allowOutsideInpu,
      _ref$maxWidth = _ref.maxWidth,
      maxWidth = _ref$maxWidth === void 0 ? "sm" : _ref$maxWidth,
      _ref$title = _ref.title,
      title = _ref$title === void 0 ? "" : _ref$title,
      _ref$show = _ref.show,
      show = _ref$show === void 0 ? false : _ref$show,
      _onClose = _ref.onClose,
      children = _ref.children,
      transition = _ref.transition,
      _ref$scroll = _ref.scroll,
      scroll = _ref$scroll === void 0 ? "body" : _ref$scroll;
  var Transition = Object(react__WEBPACK_IMPORTED_MODULE_0__["forwardRef"])(function Transition(props, ref) {
    switch (transition) {
      case "slide":
        return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_material_ui_core__WEBPACK_IMPORTED_MODULE_1__["Slide"], _extends({
          direction: "down",
          ref: ref
        }, props));

      case "grow":
        return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_material_ui_core__WEBPACK_IMPORTED_MODULE_1__["Grow"], _extends({
          ref: ref
        }, props));

      case "zoom":
        return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_material_ui_core__WEBPACK_IMPORTED_MODULE_1__["Zoom"], _extends({
          ref: ref
        }, props));

      default:
        return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_material_ui_core__WEBPACK_IMPORTED_MODULE_1__["Fade"], _extends({
          ref: ref
        }, props));
    }
  });
  if (!show) return null;
  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_material_ui_core__WEBPACK_IMPORTED_MODULE_1__["Dialog"] // TransitionComponent={Transition}
  , {
    disableBackdropClick: disableBackdropClick,
    transitionDuration: 400,
    keepMounted: true,
    disableEnforceFocus: allowOutsideInput,
    fullScreen: react_device_detect__WEBPACK_IMPORTED_MODULE_3__["isMobile"] ? true : false,
    open: show,
    onClose: function onClose() {
      return _onClose();
    },
    fullWidth: react_device_detect__WEBPACK_IMPORTED_MODULE_3__["isMobile"] ? false : true,
    maxWidth: react_device_detect__WEBPACK_IMPORTED_MODULE_3__["isMobile"] ? false : maxWidth,
    scroll: scroll,
    PaperProps: {
      style: styles.paperProps
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_material_ui_core__WEBPACK_IMPORTED_MODULE_1__["DialogTitle"], null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "text-white font-bold text-4xl lg:text-5xl ".concat(topOff ? "mt-1" : "mt-10", "  lg:px-2 leading-none")
  }, title), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_material_ui_core__WEBPACK_IMPORTED_MODULE_1__["IconButton"], {
    className: "p-3 text-white opacity-50",
    onClick: function onClick() {
      return _onClose();
    },
    style: {
      right: 12,
      top: 12,
      position: "absolute"
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_material_ui_icons_Close__WEBPACK_IMPORTED_MODULE_2___default.a, null))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_material_ui_core__WEBPACK_IMPORTED_MODULE_1__["DialogContent"], null, children));
};

var styles = {
  paperProps: {
    backgroundColor: "#000033"
  }
};
/* harmony default export */ __webpack_exports__["default"] = (Modal);

/***/ }),

/***/ "./resources/js/components/_base/RichTextEditor.js":
/*!*********************************************************!*\
  !*** ./resources/js/components/_base/RichTextEditor.js ***!
  \*********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var draft_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! draft-js */ "./node_modules/draft-js/lib/Draft.js");
/* harmony import */ var draft_js__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(draft_js__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _helpers_styles_editor_css__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../helpers/styles/editor.css */ "./resources/js/helpers/styles/editor.css");
/* harmony import */ var _helpers_styles_editor_css__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(_helpers_styles_editor_css__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var draft_js_dist_Draft_css__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! draft-js/dist/Draft.css */ "./node_modules/draft-js/dist/Draft.css");
/* harmony import */ var draft_js_dist_Draft_css__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(draft_js_dist_Draft_css__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var _fortawesome_react_fontawesome__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @fortawesome/react-fontawesome */ "./node_modules/@fortawesome/react-fontawesome/index.es.js");
/* harmony import */ var _fortawesome_free_solid_svg_icons__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @fortawesome/free-solid-svg-icons */ "./node_modules/@fortawesome/free-solid-svg-icons/index.es.js");
/* harmony import */ var _base_alerts_sweetAlert__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../_base/alerts/sweetAlert */ "./resources/js/components/_base/alerts/sweetAlert.js");
function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _unsupportedIterableToArray(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

function _iterableToArrayLimit(arr, i) { if (typeof Symbol === "undefined" || !(Symbol.iterator in Object(arr))) return; var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }








/* harmony default export */ __webpack_exports__["default"] = (function (_ref) {
  var _ref$title = _ref.title,
      title = _ref$title === void 0 ? "" : _ref$title,
      richEditorState = _ref.richEditorState,
      richEditorStateContent = _ref.richEditorStateContent,
      _ref$characterLimit = _ref.characterLimit,
      characterLimit = _ref$characterLimit === void 0 ? -1 : _ref$characterLimit;

  var _useState = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])(richEditorState),
      _useState2 = _slicedToArray(_useState, 2),
      editorState = _useState2[0],
      setEditorState = _useState2[1];

  var editor = Object(react__WEBPACK_IMPORTED_MODULE_0__["useRef"])(null);
  var focus = editor.focus;

  var onChange = function onChange(editorState) {
    setEditorState(editorState);
    richEditorStateContent(editorState);
  };

  var handleKeyCommand = function handleKeyCommand(command) {
    return _handleKeyCommand(command);
  };

  var toggleBlockType = function toggleBlockType(type) {
    return _toggleBlockType(type);
  };

  var toggleInlineStyle = function toggleInlineStyle(style) {
    return _toggleInlineStyle(style);
  };

  var _handleKeyCommand = function _handleKeyCommand(command) {
    var newState = draft_js__WEBPACK_IMPORTED_MODULE_1__["RichUtils"].handleKeyCommand(editorState, command);

    if (newState) {
      onChange(newState);
      return true;
    }

    return false;
  };

  var _toggleBlockType = function _toggleBlockType(blockType) {
    onChange(draft_js__WEBPACK_IMPORTED_MODULE_1__["RichUtils"].toggleBlockType(editorState, blockType));
  };

  var _toggleInlineStyle = function _toggleInlineStyle(inlineStyle) {
    onChange(draft_js__WEBPACK_IMPORTED_MODULE_1__["RichUtils"].toggleInlineStyle(editorState, inlineStyle));
  };

  var className = "RichEditor-editor";
  var contentState = editorState.getCurrentContent();

  if (!contentState.hasText()) {
    if (contentState.getBlockMap().first().getType() !== "unstyled") {
      className += " RichEditor-hidePlaceholder";
    }
  }

  var getLengthOfSelectedText = function getLengthOfSelectedText() {
    var currentSelection = editorState.getSelection();
    var isCollapsed = currentSelection.isCollapsed();
    var length = 0;

    if (!isCollapsed) {
      var currentContent = editorState.getCurrentContent();
      var startKey = currentSelection.getStartKey();
      var endKey = currentSelection.getEndKey();
      var startBlock = currentContent.getBlockForKey(startKey);
      var isStartAndEndBlockAreTheSame = startKey === endKey;
      var startBlockTextLength = startBlock.getLength();
      var startSelectedTextLength = startBlockTextLength - currentSelection.getStartOffset();
      var endSelectedTextLength = currentSelection.getEndOffset();
      var keyAfterEnd = currentContent.getKeyAfter(endKey);

      if (isStartAndEndBlockAreTheSame) {
        length += currentSelection.getEndOffset() - currentSelection.getStartOffset();
      } else {
        var currentKey = startKey;

        while (currentKey && currentKey !== keyAfterEnd) {
          if (currentKey === startKey) {
            length += startSelectedTextLength + 1;
          } else if (currentKey === endKey) {
            length += endSelectedTextLength;
          } else {
            length += currentContent.getBlockForKey(currentKey).getLength() + 1;
          }

          currentKey = currentContent.getKeyAfter(currentKey);
        }
      }
    }

    return length;
  };

  var handleBeforeInput = function handleBeforeInput() {
    var currentContent = editorState.getCurrentContent();
    var currentContentLength = currentContent.getPlainText("").length;
    var selectedTextLength = getLengthOfSelectedText(editorState);

    if (characterLimit !== -1) {
      if (currentContentLength - selectedTextLength > characterLimit - 1) {
        return "handled";
      }
    }
  };

  var handlePastedText = function handlePastedText(pastedText) {
    var currentContent = editorState.getCurrentContent();
    var currentContentLength = currentContent.getPlainText("").length;
    var selectedTextLength = getLengthOfSelectedText();

    if (characterLimit !== -1) {
      if (currentContentLength + pastedText.length - selectedTextLength > characterLimit) {
        Object(_base_alerts_sweetAlert__WEBPACK_IMPORTED_MODULE_6__["sweetAlert"])('error', 'Input should be less than or equal to 2000 characters!');
        return "handled";
      }
    }
  };

  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react__WEBPACK_IMPORTED_MODULE_0___default.a.Fragment, null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "flex"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "w-full font-bold"
  }, title, " "), characterLimit !== -1 && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: " Richeditor-counter text-right "
  }, "Characters remaining: ", /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
    className: "w-10"
  }, characterLimit - editorState.getCurrentContent().getPlainText().length))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "RichEditor-root"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(InlineStyleControls, {
    editorState: editorState,
    onToggle: toggleInlineStyle
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(BlockStyleControls, {
    editorState: editorState,
    onToggle: toggleBlockType
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: className,
    onClick: focus
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(draft_js__WEBPACK_IMPORTED_MODULE_1__["Editor"], {
    blockStyleFn: getBlockStyle,
    customStyleMap: styleMap,
    editorState: editorState,
    handleKeyCommand: handleKeyCommand,
    handleBeforeInput: handleBeforeInput,
    handlePastedText: handlePastedText,
    onChange: onChange,
    ref: editor,
    spellCheck: true
  }))));
});
var styleMap = {
  CODE: {
    backgroundColor: "rgba(0, 0, 0, 0.05)",
    fontFamily: '"Inconsolata", "Menlo", "Consolas", monospace',
    fontSize: 16,
    padding: 2
  }
};

var getBlockStyle = function getBlockStyle(block) {
  switch (block.getType()) {
    case "blockquote":
      return "RichEditor-blockquote";

    default:
      return null;
  }
};

var BLOCK_TYPES = [//  { label: "H1", style: "header-one" },
//  { label: "H2", style: "header-two" },
//  { label: "H3", style: "header-three" },
//  { label: "H4", style: "header-four" },
//  { label: "H5", style: "header-five" },
//  { label: "H6", style: "header-six" },
// { label: "Blockquote", style: "blockquote" },
{
  label: "list-ul",
  style: "unordered-list-item"
}, {
  label: "list-ol",
  style: "ordered-list-item"
} //  { label: "Code Block", style: "code-block" }
];

var BlockStyleControls = function BlockStyleControls(props) {
  var editorState = props.editorState;
  var selection = editorState.getSelection();
  var blockType = editorState.getCurrentContent().getBlockForKey(selection.getStartKey()).getType();
  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "RichEditor-controls inline-flex"
  }, BLOCK_TYPES.map(function (type) {
    return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(StyleButton, {
      key: type.label,
      active: type.style === blockType,
      label: type.label,
      onToggle: props.onToggle,
      style: type.style
    });
  }));
};

var INLINE_STYLES = [{
  label: "bold",
  style: "BOLD"
}, {
  label: "italic",
  style: "ITALIC"
}, {
  label: "underline",
  style: "UNDERLINE"
} //  { label: "Monospace", style: "CODE" }
];

var InlineStyleControls = function InlineStyleControls(props) {
  var currentStyle = props.editorState.getCurrentInlineStyle();
  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "RichEditor-controls inline-flex"
  }, INLINE_STYLES.map(function (type) {
    return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(StyleButton, {
      key: type.label,
      active: currentStyle.has(type.style),
      label: type.label,
      onToggle: props.onToggle,
      style: type.style
    });
  }));
};

var StyleButton = function StyleButton(props) {
  var onToggle = function onToggle(e) {
    e.preventDefault();
    props.onToggle(props.style);
  };

  var className = "RichEditor-styleButton";

  if (props.active) {
    className += " RichEditor-activeButton";
  }

  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
    className: className,
    onMouseDown: onToggle
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_fortawesome_react_fontawesome__WEBPACK_IMPORTED_MODULE_4__["FontAwesomeIcon"], {
    icon: ["fas", "".concat(props.label)]
  }));
};

/***/ }),

/***/ "./resources/js/components/_base/Table.js":
/*!************************************************!*\
  !*** ./resources/js/components/_base/Table.js ***!
  \************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime/regenerator */ "./node_modules/@babel/runtime/regenerator/index.js");
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _table_Pagination__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./table/Pagination */ "./resources/js/components/_base/table/Pagination.js");
/* harmony import */ var _table_PageSelectSize__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./table/PageSelectSize */ "./resources/js/components/_base/table/PageSelectSize.js");
/* harmony import */ var _helpers_bootstrap__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../helpers/bootstrap */ "./resources/js/helpers/bootstrap.js");
/* harmony import */ var react_device_detect__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! react-device-detect */ "./node_modules/react-device-detect/main.js");
/* harmony import */ var react_device_detect__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(react_device_detect__WEBPACK_IMPORTED_MODULE_5__);


function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _unsupportedIterableToArray(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

function _iterableToArrayLimit(arr, i) { if (typeof Symbol === "undefined" || !(Symbol.iterator in Object(arr))) return; var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }







var Table = function Table(_ref) {
  var className = _ref.className,
      header = _ref.header,
      content = _ref.content,
      query = _ref.query,
      _ref$queryParams = _ref.queryParams,
      queryParams = _ref$queryParams === void 0 ? "" : _ref$queryParams,
      _ref$keyword = _ref.keyword,
      keyword = _ref$keyword === void 0 ? "" : _ref$keyword,
      _ref$order = _ref.order,
      order = _ref$order === void 0 ? "" : _ref$order,
      _ref$sort = _ref.sort,
      sort = _ref$sort === void 0 ? "" : _ref$sort,
      getData = _ref.getData,
      toggleFetch = _ref.toggleFetch,
      emptyComponent = _ref.emptyComponent;

  var _useState = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(12),
      _useState2 = _slicedToArray(_useState, 2),
      limit = _useState2[0],
      setLimit = _useState2[1];

  var _useState3 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(0),
      _useState4 = _slicedToArray(_useState3, 2),
      offset = _useState4[0],
      setOffset = _useState4[1];

  var _useState5 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(0),
      _useState6 = _slicedToArray(_useState5, 2),
      total = _useState6[0],
      setTotal = _useState6[1];

  var _useState7 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(false),
      _useState8 = _slicedToArray(_useState7, 2),
      fetching = _useState8[0],
      setFetching = _useState8[1];

  Object(react__WEBPACK_IMPORTED_MODULE_1__["useEffect"])(function () {
    setFetching(true);

    function fetchResults() {
      return _fetchResults.apply(this, arguments);
    }

    function _fetchResults() {
      _fetchResults = _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee() {
        var pathname, _yield$axios$get, data;

        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                pathname = window.location.pathname.replace(/\/$/, ''); // let filters = filter ? (Cookie.get(pathname) ? JSON.parse(Cookie.get(pathname)) : []) : []
                // filters = filters.map(filter => {
                //   delete filter['id']
                //   delete filter['label']
                //   delete filter['options']
                //   delete filter['filterLabel']
                //   return filter
                // })

                _context.next = 3;
                return _helpers_bootstrap__WEBPACK_IMPORTED_MODULE_4__["axios"].get(getFetchUrl());

              case 3:
                _yield$axios$get = _context.sent;
                data = _yield$axios$get.data;
                setTotal(data.total);
                getData(data.data);
                setFetching(false);
                window.scrollTo({
                  top: 0,
                  behavior: 'smooth'
                });

              case 9:
              case "end":
                return _context.stop();
            }
          }
        }, _callee);
      }));
      return _fetchResults.apply(this, arguments);
    }

    function getFetchUrl() {
      queryParams = queryParams ? "&".concat(queryParams) : '';
      return query + '?keyword=' + keyword + '&limit=' + limit + '&offset=' + offset + '&order=' + order + '&sort=' + sort + queryParams;
    }

    fetchResults();
  }, [offset, limit, keyword, toggleFetch]); // ✅ Refetch on change

  var changeLimit = function changeLimit(value) {
    setOffset(0);
    setLimit(value);
  };

  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(react__WEBPACK_IMPORTED_MODULE_1___default.a.Fragment, null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("section", null, !total ? /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(react__WEBPACK_IMPORTED_MODULE_1___default.a.Fragment, null, emptyComponent ? emptyComponent : /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "flex flex-col items-center justify-center w-full h-64 text-base text-gray-400"
  }, "No result found!")) : /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(react__WEBPACK_IMPORTED_MODULE_1___default.a.Fragment, null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("table", {
    className: "table-auto text-left w-full ".concat(className)
  }, header, content), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "flex-col lg:flex items-center justify-center lg:mt-16 mt-6 relative w-full"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_table_Pagination__WEBPACK_IMPORTED_MODULE_2__["default"], {
    onPageChange: setOffset,
    limit: limit,
    offset: offset,
    total: total
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_table_PageSelectSize__WEBPACK_IMPORTED_MODULE_3__["default"], {
    total: total,
    onClick: changeLimit
  })))));
};

/* harmony default export */ __webpack_exports__["default"] = (Table);

/***/ }),

/***/ "./resources/js/components/_base/TextArea.js":
/*!***************************************************!*\
  !*** ./resources/js/components/_base/TextArea.js ***!
  \***************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

function _objectWithoutProperties(source, excluded) { if (source == null) return {}; var target = _objectWithoutPropertiesLoose(source, excluded); var key, i; if (Object.getOwnPropertySymbols) { var sourceSymbolKeys = Object.getOwnPropertySymbols(source); for (i = 0; i < sourceSymbolKeys.length; i++) { key = sourceSymbolKeys[i]; if (excluded.indexOf(key) >= 0) continue; if (!Object.prototype.propertyIsEnumerable.call(source, key)) continue; target[key] = source[key]; } } return target; }

function _objectWithoutPropertiesLoose(source, excluded) { if (source == null) return {}; var target = {}; var sourceKeys = Object.keys(source); var key, i; for (i = 0; i < sourceKeys.length; i++) { key = sourceKeys[i]; if (excluded.indexOf(key) >= 0) continue; target[key] = source[key]; } return target; }



function TextArea(_ref) {
  var onChange = _ref.onChange,
      className = _ref.className,
      prefix = _ref.prefix,
      suffix = _ref.suffix,
      errors = _ref.errors,
      _ref$appearance = _ref.appearance,
      appearance = _ref$appearance === void 0 ? true : _ref$appearance,
      rest = _objectWithoutProperties(_ref, ["onChange", "className", "prefix", "suffix", "errors", "appearance"]);

  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "".concat(appearance ? "border-b-2 mt-4 px-2 py-3" : "", " flex items-start w-full"),
    style: {
      borderColor: "rgba(255,255,255,0.1)"
    }
  }, prefix && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("svg", {
    className: "feather-icon mr-3 h-6 w-6 opacity-50 ".concat(errors ? "text-red-500" : "")
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("use", {
    xlinkHref: "/assets/svg/feather-sprite.svg#".concat(prefix)
  })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("textarea", _extends({
    className: "resize-none appearance-none bg-transparent border-0 font-hairline leading-relaxed text-base w-full ".concat(className),
    rows: "3",
    onChange: onChange
  }, rest)), suffix);
}

/* harmony default export */ __webpack_exports__["default"] = (TextArea);

/***/ }),

/***/ "./resources/js/components/_base/TextInput.js":
/*!****************************************************!*\
  !*** ./resources/js/components/_base/TextInput.js ***!
  \****************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _helpers_numberInput__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../helpers/numberInput */ "./resources/js/helpers/numberInput.js");
function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

function _objectWithoutProperties(source, excluded) { if (source == null) return {}; var target = _objectWithoutPropertiesLoose(source, excluded); var key, i; if (Object.getOwnPropertySymbols) { var sourceSymbolKeys = Object.getOwnPropertySymbols(source); for (i = 0; i < sourceSymbolKeys.length; i++) { key = sourceSymbolKeys[i]; if (excluded.indexOf(key) >= 0) continue; if (!Object.prototype.propertyIsEnumerable.call(source, key)) continue; target[key] = source[key]; } } return target; }

function _objectWithoutPropertiesLoose(source, excluded) { if (source == null) return {}; var target = {}; var sourceKeys = Object.keys(source); var key, i; for (i = 0; i < sourceKeys.length; i++) { key = sourceKeys[i]; if (excluded.indexOf(key) >= 0) continue; target[key] = source[key]; } return target; }




function TextInput(_ref) {
  var onChange = _ref.onChange,
      className = _ref.className,
      prefix = _ref.prefix,
      suffix = _ref.suffix,
      errors = _ref.errors,
      _ref$border = _ref.border,
      border = _ref$border === void 0 ? true : _ref$border,
      _ref$appearance = _ref.appearance,
      appearance = _ref$appearance === void 0 ? false : _ref$appearance,
      width = _ref.width,
      rest = _objectWithoutProperties(_ref, ["onChange", "className", "prefix", "suffix", "errors", "border", "appearance", "width"]);

  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "".concat(border ? "border-b-2 mt-4 px-2 py-3" : "", " flex items-start ").concat(width ? width : "w-full"),
    style: {
      borderColor: "rgba(255,255,255,0.1)"
    }
  }, prefix && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("svg", {
    className: "feather-icon mr-3 h-6 w-6 opacity-50 ".concat(errors ? "text-red-500" : "")
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("use", {
    xlinkHref: "/assets/svg/feather-sprite.svg#".concat(prefix)
  })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("input", _extends({
    className: "\n          font-hairline leading-relaxed w-full  \n          ".concat(appearance ? "border border-gray-400 rounded px-3 py-1" : "appearance-none bg-transparent border-0", "\n          ").concat(className, "\n        "),
    onChange: onChange
  }, rest)), suffix);
}

/* harmony default export */ __webpack_exports__["default"] = (TextInput);

/***/ }),

/***/ "./resources/js/components/_base/Tooltip.js":
/*!**************************************************!*\
  !*** ./resources/js/components/_base/Tooltip.js ***!
  \**************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _material_ui_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @material-ui/core */ "./node_modules/@material-ui/core/esm/index.js");
/* harmony import */ var _material_ui_core_styles__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @material-ui/core/styles */ "./node_modules/@material-ui/core/esm/styles/index.js");
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! lodash */ "./node_modules/lodash/lodash.js");
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(lodash__WEBPACK_IMPORTED_MODULE_3__);
function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _unsupportedIterableToArray(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

function _iterableToArrayLimit(arr, i) { if (typeof Symbol === "undefined" || !(Symbol.iterator in Object(arr))) return; var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }

function _objectWithoutProperties(source, excluded) { if (source == null) return {}; var target = _objectWithoutPropertiesLoose(source, excluded); var key, i; if (Object.getOwnPropertySymbols) { var sourceSymbolKeys = Object.getOwnPropertySymbols(source); for (i = 0; i < sourceSymbolKeys.length; i++) { key = sourceSymbolKeys[i]; if (excluded.indexOf(key) >= 0) continue; if (!Object.prototype.propertyIsEnumerable.call(source, key)) continue; target[key] = source[key]; } } return target; }

function _objectWithoutPropertiesLoose(source, excluded) { if (source == null) return {}; var target = {}; var sourceKeys = Object.keys(source); var key, i; for (i = 0; i < sourceKeys.length; i++) { key = sourceKeys[i]; if (excluded.indexOf(key) >= 0) continue; target[key] = source[key]; } return target; }

function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }






function arrowGenerator(color) {
  return {
    '&[x-placement*="bottom"] $arrow': {
      top: 0,
      left: 0,
      marginTop: '-0.95em',
      width: '2em',
      height: '1em',
      '&::before': {
        borderWidth: '0 1em 1em 1em',
        borderColor: "transparent transparent ".concat(color, " transparent")
      }
    },
    '&[x-placement*="top"] $arrow': {
      bottom: 0,
      left: 0,
      marginBottom: '-0.95em',
      width: '2em',
      height: '1em',
      '&::before': {
        borderWidth: '1em 1em 0 1em',
        borderColor: "".concat(color, " transparent transparent transparent")
      }
    },
    '&[x-placement*="right"] $arrow': {
      left: 0,
      marginLeft: '-0.95em',
      height: '2em',
      width: '1em',
      '&::before': {
        borderWidth: '1em 1em 1em 0',
        borderColor: "transparent ".concat(color, " transparent transparent")
      }
    },
    '&[x-placement*="left"] $arrow': {
      right: 0,
      marginRight: '-0.95em',
      height: '2em',
      width: '1em',
      '&::before': {
        borderWidth: '1em 0 1em 1em',
        borderColor: "transparent transparent transparent ".concat(color)
      }
    }
  };
}

var useStyles = Object(_material_ui_core_styles__WEBPACK_IMPORTED_MODULE_2__["makeStyles"])(function () {
  return {
    arrow: {
      position: 'absolute',
      fontSize: 6,
      '&::before': {
        content: '""',
        margin: 'auto',
        display: 'block',
        width: 0,
        height: 0,
        borderStyle: 'solid'
      }
    },
    popper: arrowGenerator('#009EFF'),
    tooltip: {
      position: 'relative',
      backgroundColor: '#009EFF',
      fontSize: 12,
      padding: '4px 12px',
      borderRadius: 3,
      letterSpacing: 0.5,
      fontFamily: 'Basis Grotesque Pro, sans-serif',
      maxWidth: 180
    },
    tooltipPlacementLeft: {
      margin: '0 8px'
    },
    tooltipPlacementRight: {
      margin: '0 8px'
    },
    tooltipPlacementTop: {
      margin: '8px 0'
    },
    tooltipPlacementBottom: {
      margin: '8px 0'
    }
  };
});

var Tooltip = function Tooltip(_ref) {
  var rest = _extends({}, _ref);

  var _useStyles = useStyles(),
      arrow = _useStyles.arrow,
      classes = _objectWithoutProperties(_useStyles, ["arrow"]);

  var _useState = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])(null),
      _useState2 = _slicedToArray(_useState, 2),
      arrowRef = _useState2[0],
      setArrowRef = _useState2[1];

  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_material_ui_core__WEBPACK_IMPORTED_MODULE_1__["Tooltip"], _extends({
    classes: classes,
    PopperProps: {
      popperOptions: {
        modifiers: {
          arrow: {
            enabled: Boolean(arrowRef),
            element: arrowRef
          }
        }
      },
      disablePortal: true
    } // open={true}

  }, rest, {
    title: /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react__WEBPACK_IMPORTED_MODULE_0___default.a.Fragment, null, rest.title, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
      className: arrow,
      ref: setArrowRef
    }))
  }));
};

/* harmony default export */ __webpack_exports__["default"] = (Tooltip);

/***/ }),

/***/ "./resources/js/components/_base/alerts/sweetAlert.js":
/*!************************************************************!*\
  !*** ./resources/js/components/_base/alerts/sweetAlert.js ***!
  \************************************************************/
/*! exports provided: sweetAlert, sweetAlertConfirm */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "sweetAlert", function() { return sweetAlert; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "sweetAlertConfirm", function() { return sweetAlertConfirm; });
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! sweetalert2 */ "./node_modules/sweetalert2/dist/sweetalert2.all.js");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(sweetalert2__WEBPACK_IMPORTED_MODULE_0__);

var sweetAlert = function sweetAlert(type, title) {
  sweetalert2__WEBPACK_IMPORTED_MODULE_0___default.a.fire({
    type: "".concat(type),
    title: "".concat(title),
    showConfirmButton: false,
    timer: 3000
  });
};
var sweetAlertConfirm = function sweetAlertConfirm(title, text, cb) {
  return sweetalert2__WEBPACK_IMPORTED_MODULE_0___default.a.fire({
    title: "".concat(title),
    text: "".concat(text),
    type: "info",
    showCancelButton: true,
    confirmButtonText: "Yes!",
    cancelButtonText: "No"
  }).then(function (result) {
    if (result.value) {
      cb();
    }
  });
};

/***/ }),

/***/ "./resources/js/components/_base/index.js":
/*!************************************************!*\
  !*** ./resources/js/components/_base/index.js ***!
  \************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var requireContext = __webpack_require__("./resources/js/components/_base sync .*\\.(js)$");

var components = {};
requireContext.keys().forEach(function (fileName) {
  if (fileName === './index.js') return;
  var name = fileName.replace(/(\.\/|\.js)/g, '');
  components[name] = requireContext(fileName)["default"];
});
module.exports = components;

/***/ }),

/***/ "./resources/js/components/_base/table/PageSelectSize.js":
/*!***************************************************************!*\
  !*** ./resources/js/components/_base/table/PageSelectSize.js ***!
  \***************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _helpers_deviceSizeHelper__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../helpers/deviceSizeHelper */ "./resources/js/helpers/deviceSizeHelper.js");
/* harmony import */ var _states_userGlobal__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../states/userGlobal */ "./resources/js/states/userGlobal.js");
function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _unsupportedIterableToArray(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

function _iterableToArrayLimit(arr, i) { if (typeof Symbol === "undefined" || !(Symbol.iterator in Object(arr))) return; var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }





var PageSelectSize = function PageSelectSize(_ref) {
  var total = _ref.total,
      _onClick = _ref.onClick;
  var pageSizeOptions = [12, 24, 48];

  var _UserGlobal = Object(_states_userGlobal__WEBPACK_IMPORTED_MODULE_2__["default"])(),
      _UserGlobal2 = _slicedToArray(_UserGlobal, 2),
      userState = _UserGlobal2[0],
      userAction = _UserGlobal2[1];

  Object(react__WEBPACK_IMPORTED_MODULE_0__["useEffect"])(function () {}, [userState.windowSize]);

  if (userState.windowSize <= _helpers_deviceSizeHelper__WEBPACK_IMPORTED_MODULE_1__["baseSmall"]) {
    return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("label", {
      name: "PageSizeSelect",
      className: "flex-col lg:absolute lg:right-0 flex items-center justify-center mt-6 lg:mt-0"
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
      className: "font-bold mb-1 text-gray-600"
    }, "Items per page:"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("ul", {
      className: "flex items-center"
    }, pageSizeOptions.map(function (value, key) {
      return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("li", {
        key: key,
        className: "page-size-select\n        ".concat(key !== pageSizeOptions.length - 1 ? "border-r" : "", "\n      "),
        onClick: function onClick() {
          return _onClick(value);
        }
      }, value);
    })));
  } else {
    return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("label", {
      name: "PageSizeSelect",
      className: "lg:absolute lg:right-0 flex items-center justify-center mt-6 lg:mt-0"
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
      className: "font-bold mb-1 mr-2 text-gray-600"
    }, "Items per page:"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("ul", {
      className: "flex mr-2"
    }, pageSizeOptions.map(function (value, key) {
      return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("li", {
        key: key,
        className: "page-size-select\n              ".concat(key !== pageSizeOptions.length - 1 ? "border-r" : "", "\n            "),
        onClick: function onClick() {
          return _onClick(value);
        }
      }, value);
    })));
  }
};

/* harmony default export */ __webpack_exports__["default"] = (PageSelectSize);

/***/ }),

/***/ "./resources/js/components/_base/table/Pagination.js":
/*!***********************************************************!*\
  !*** ./resources/js/components/_base/table/Pagination.js ***!
  \***********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);


var Pagination = function Pagination(_ref) {
  var offset = _ref.offset,
      limit = _ref.limit,
      total = _ref.total,
      onPageChange = _ref.onPageChange;

  var isFirstPage = function isFirstPage() {
    return offset === 0 || limit >= total;
  };

  var isLastPage = function isLastPage() {
    return offset + limit >= total;
  };

  var totalPage = function totalPage() {
    return Math.ceil(total / limit);
  };

  var curPage = function curPage() {
    return Math.ceil(offset / limit) + 1;
  };

  var dspBtns = function dspBtns() {
    var n = totalPage();
    var i = curPage();
    /* eslint-disable */

    if (n <= 9) return function (n) {
      var arr = Array(n);

      while (n) {
        arr[n - 1] = n--;
      }

      return arr;
    }(n);
    if (i <= 5) return [1, 2, 3, 4, 5, 6, 7, 0, n]; // 0 represents `···`

    if (i >= n - 4) return [1, 0, n - 6, n - 5, n - 4, n - 3, n - 2, n - 1, n];
    return [1, 0, i - 2, i - 1, i, i + 1, i + 2, 0, n];
    /* eslint-enable */
  };

  var handleClick = function handleClick(n) {
    var offset = (n - 1) * limit;
    onPageChange(offset);
  };

  var turnPage = function turnPage(i) {
    onPageChange(offset + i * limit);
  };

  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "flex items-center justify-center mb-3 lg:mb-0"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("ul", {
    className: "pagination",
    name: "Pagination"
  }, !isFirstPage() && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("li", {
    className: "page-item",
    onClick: function onClick() {
      return turnPage(-1);
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("button", {
    className: "page-link"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("svg", {
    className: "feather-icon w-full"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("use", {
    xlinkHref: "/assets/svg/feather-sprite.svg#chevron-left"
  })))), dspBtns().map(function (button, i) {
    return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("li", {
      key: i,
      className: "page-item ".concat(button === curPage() ? 'active' : '')
    }, button ? /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("button", {
      className: "page-link",
      onClick: function onClick() {
        return handleClick(button);
      }
    }, button) : /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("button", {
      className: "page-link"
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("i", {
      className: "fa fa-ellipsis-h"
    })));
  }), !isLastPage() && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("li", {
    className: "page-item",
    onClick: function onClick() {
      return turnPage(1);
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("button", {
    className: "page-link"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("svg", {
    className: "feather-icon w-full"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("use", {
    xlinkHref: "/assets/svg/feather-sprite.svg#chevron-right"
  }))))));
};

/* harmony default export */ __webpack_exports__["default"] = (Pagination);

/***/ }),

/***/ "./resources/js/data/activeCampaignData/activeCampaignData.js":
/*!********************************************************************!*\
  !*** ./resources/js/data/activeCampaignData/activeCampaignData.js ***!
  \********************************************************************/
/*! exports provided: sendContactFormEmail */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "sendContactFormEmail", function() { return sendContactFormEmail; });
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime/regenerator */ "./node_modules/@babel/runtime/regenerator/index.js");
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! axios */ "./node_modules/axios/index.js");
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(axios__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _services_auth__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../services/auth */ "./resources/js/services/auth.js");


function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }




var sendContactFormEmail = /*#__PURE__*/function () {
  var _ref2 = _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee(_ref) {
    var formData, url;
    return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            formData = _ref.formData;
            url = "https://groupbuyer.activehosted.com/proc.php";
            _context.next = 4;
            return axios__WEBPACK_IMPORTED_MODULE_2___default()({
              method: "post",
              url: url,
              data: formData,
              mode: "no-cors"
            });

          case 4:
            return _context.abrupt("return", _context.sent);

          case 5:
          case "end":
            return _context.stop();
        }
      }
    }, _callee);
  }));

  return function sendContactFormEmail(_x) {
    return _ref2.apply(this, arguments);
  };
}();

/***/ }),

/***/ "./resources/js/data/banksData/banksData.js":
/*!**************************************************!*\
  !*** ./resources/js/data/banksData/banksData.js ***!
  \**************************************************/
/*! exports provided: fetchBanks */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "fetchBanks", function() { return fetchBanks; });
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime/regenerator */ "./node_modules/@babel/runtime/regenerator/index.js");
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! axios */ "./node_modules/axios/index.js");
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(axios__WEBPACK_IMPORTED_MODULE_2__);


function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }



var fetchBanks = /*#__PURE__*/function () {
  var _ref2 = _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee(_ref) {
    var url;
    return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            url = _ref.url;
            _context.next = 3;
            return axios__WEBPACK_IMPORTED_MODULE_2___default()({
              method: "get",
              url: url
            });

          case 3:
            return _context.abrupt("return", _context.sent);

          case 4:
          case "end":
            return _context.stop();
        }
      }
    }, _callee);
  }));

  return function fetchBanks(_x) {
    return _ref2.apply(this, arguments);
  };
}();

/***/ }),

/***/ "./resources/js/data/dealsData/dealsData.js":
/*!**************************************************!*\
  !*** ./resources/js/data/dealsData/dealsData.js ***!
  \**************************************************/
/*! exports provided: saveUpdateDeal, saveUpdateResources, getDealByID, getFeaturedDeals, getAverageSavings, getUserCount */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "saveUpdateDeal", function() { return saveUpdateDeal; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "saveUpdateResources", function() { return saveUpdateResources; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getDealByID", function() { return getDealByID; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getFeaturedDeals", function() { return getFeaturedDeals; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getAverageSavings", function() { return getAverageSavings; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getUserCount", function() { return getUserCount; });
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime/regenerator */ "./node_modules/@babel/runtime/regenerator/index.js");
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _material_ui_icons__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @material-ui/icons */ "./node_modules/@material-ui/icons/esm/index.js");
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! axios */ "./node_modules/axios/index.js");
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(axios__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _services_auth__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../services/auth */ "./resources/js/services/auth.js");


function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }




var saveUpdateDeal = /*#__PURE__*/function () {
  var _ref2 = _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee(_ref) {
    var url, formData;
    return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            url = _ref.url, formData = _ref.formData;
            _context.next = 3;
            return axios__WEBPACK_IMPORTED_MODULE_2___default()({
              method: "post",
              url: url,
              data: formData,
              name: "project",
              headers: {
                Authorization: "Bearer " + Object(_services_auth__WEBPACK_IMPORTED_MODULE_3__["isLoggedIn"])()
              }
            });

          case 3:
            return _context.abrupt("return", _context.sent);

          case 4:
          case "end":
            return _context.stop();
        }
      }
    }, _callee);
  }));

  return function saveUpdateDeal(_x) {
    return _ref2.apply(this, arguments);
  };
}();
var saveUpdateResources = /*#__PURE__*/function () {
  var _ref4 = _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee2(_ref3) {
    var url, formData;
    return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee2$(_context2) {
      while (1) {
        switch (_context2.prev = _context2.next) {
          case 0:
            url = _ref3.url, formData = _ref3.formData;
            _context2.next = 3;
            return axios__WEBPACK_IMPORTED_MODULE_2___default()({
              method: "post",
              url: url,
              data: formData,
              headers: {
                "content-type": "multipart/form-data"
              }
            });

          case 3:
            return _context2.abrupt("return", _context2.sent);

          case 4:
          case "end":
            return _context2.stop();
        }
      }
    }, _callee2);
  }));

  return function saveUpdateResources(_x2) {
    return _ref4.apply(this, arguments);
  };
}();
var getDealByID = /*#__PURE__*/function () {
  var _ref6 = _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee3(_ref5) {
    var url;
    return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee3$(_context3) {
      while (1) {
        switch (_context3.prev = _context3.next) {
          case 0:
            url = _ref5.url;
            _context3.next = 3;
            return axios__WEBPACK_IMPORTED_MODULE_2___default.a.get("".concat(url));

          case 3:
            return _context3.abrupt("return", _context3.sent);

          case 4:
          case "end":
            return _context3.stop();
        }
      }
    }, _callee3);
  }));

  return function getDealByID(_x3) {
    return _ref6.apply(this, arguments);
  };
}();
var getFeaturedDeals = /*#__PURE__*/function () {
  var _ref7 = _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee4() {
    return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee4$(_context4) {
      while (1) {
        switch (_context4.prev = _context4.next) {
          case 0:
            _context4.next = 2;
            return axios__WEBPACK_IMPORTED_MODULE_2___default.a.get("/api/deal?featured=true");

          case 2:
            return _context4.abrupt("return", _context4.sent);

          case 3:
          case "end":
            return _context4.stop();
        }
      }
    }, _callee4);
  }));

  return function getFeaturedDeals() {
    return _ref7.apply(this, arguments);
  };
}();
var getAverageSavings = /*#__PURE__*/function () {
  var _ref8 = _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee5() {
    return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee5$(_context5) {
      while (1) {
        switch (_context5.prev = _context5.next) {
          case 0:
            _context5.next = 2;
            return axios__WEBPACK_IMPORTED_MODULE_2___default.a.get("/api/average-savings");

          case 2:
            return _context5.abrupt("return", _context5.sent);

          case 3:
          case "end":
            return _context5.stop();
        }
      }
    }, _callee5);
  }));

  return function getAverageSavings() {
    return _ref8.apply(this, arguments);
  };
}();
var getUserCount = /*#__PURE__*/function () {
  var _ref9 = _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee6() {
    return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee6$(_context6) {
      while (1) {
        switch (_context6.prev = _context6.next) {
          case 0:
            _context6.next = 2;
            return axios__WEBPACK_IMPORTED_MODULE_2___default.a.get("/api/user-count");

          case 2:
            return _context6.abrupt("return", _context6.sent);

          case 3:
          case "end":
            return _context6.stop();
        }
      }
    }, _callee6);
  }));

  return function getUserCount() {
    return _ref9.apply(this, arguments);
  };
}();

/***/ }),

/***/ "./resources/js/data/developersData/developersData.js":
/*!************************************************************!*\
  !*** ./resources/js/data/developersData/developersData.js ***!
  \************************************************************/
/*! exports provided: developers, getDeveloperById */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "developers", function() { return developers; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getDeveloperById", function() { return getDeveloperById; });
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime/regenerator */ "./node_modules/@babel/runtime/regenerator/index.js");
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! axios */ "./node_modules/axios/index.js");
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(axios__WEBPACK_IMPORTED_MODULE_2__);


function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }



var developers = /*#__PURE__*/function () {
  var _ref = _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee(url) {
    return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            _context.next = 2;
            return axios__WEBPACK_IMPORTED_MODULE_2___default()({
              method: "get",
              url: url,
              params: {
                role: "project_developers"
              }
            });

          case 2:
            return _context.abrupt("return", _context.sent);

          case 3:
          case "end":
            return _context.stop();
        }
      }
    }, _callee);
  }));

  return function developers(_x) {
    return _ref.apply(this, arguments);
  };
}();
var getDeveloperById = /*#__PURE__*/function () {
  var _ref2 = _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee2(userId) {
    return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee2$(_context2) {
      while (1) {
        switch (_context2.prev = _context2.next) {
          case 0:
            _context2.next = 2;
            return axios__WEBPACK_IMPORTED_MODULE_2___default.a.get("/api/developer/".concat(userId));

          case 2:
            return _context2.abrupt("return", _context2.sent);

          case 3:
          case "end":
            return _context2.stop();
        }
      }
    }, _callee2);
  }));

  return function getDeveloperById(_x2) {
    return _ref2.apply(this, arguments);
  };
}();

/***/ }),

/***/ "./resources/js/data/index.js":
/*!************************************!*\
  !*** ./resources/js/data/index.js ***!
  \************************************/
/*! exports provided: salesAdviceAdditionalInfo, developers, saveUpdateDeal, saveUpdateResources, saveUpdateProperty, getDealByID, salesAdviceSolicitorInfo, salesAdvicePurchaserInfo, sendContactFormEmail, fetchBanks */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _developersData_developersData__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./developersData/developersData */ "./resources/js/data/developersData/developersData.js");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "developers", function() { return _developersData_developersData__WEBPACK_IMPORTED_MODULE_1__["developers"]; });

/* harmony import */ var _dealsData_dealsData__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./dealsData/dealsData */ "./resources/js/data/dealsData/dealsData.js");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "saveUpdateDeal", function() { return _dealsData_dealsData__WEBPACK_IMPORTED_MODULE_2__["saveUpdateDeal"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "saveUpdateResources", function() { return _dealsData_dealsData__WEBPACK_IMPORTED_MODULE_2__["saveUpdateResources"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "getDealByID", function() { return _dealsData_dealsData__WEBPACK_IMPORTED_MODULE_2__["getDealByID"]; });

/* harmony import */ var _propertiesData_propertiesData__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./propertiesData/propertiesData */ "./resources/js/data/propertiesData/propertiesData.js");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "saveUpdateProperty", function() { return _propertiesData_propertiesData__WEBPACK_IMPORTED_MODULE_3__["saveUpdateProperty"]; });

/* harmony import */ var _salesAdvice_salesAdviceData__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./salesAdvice/salesAdviceData */ "./resources/js/data/salesAdvice/salesAdviceData.js");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "salesAdviceAdditionalInfo", function() { return _salesAdvice_salesAdviceData__WEBPACK_IMPORTED_MODULE_4__["salesAdviceAdditionalInfo"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "salesAdviceSolicitorInfo", function() { return _salesAdvice_salesAdviceData__WEBPACK_IMPORTED_MODULE_4__["salesAdviceSolicitorInfo"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "salesAdvicePurchaserInfo", function() { return _salesAdvice_salesAdviceData__WEBPACK_IMPORTED_MODULE_4__["salesAdvicePurchaserInfo"]; });

/* harmony import */ var _activeCampaignData_activeCampaignData__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./activeCampaignData/activeCampaignData */ "./resources/js/data/activeCampaignData/activeCampaignData.js");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "sendContactFormEmail", function() { return _activeCampaignData_activeCampaignData__WEBPACK_IMPORTED_MODULE_5__["sendContactFormEmail"]; });

/* harmony import */ var _banksData_banksData__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./banksData/banksData */ "./resources/js/data/banksData/banksData.js");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "fetchBanks", function() { return _banksData_banksData__WEBPACK_IMPORTED_MODULE_6__["fetchBanks"]; });










/***/ }),

/***/ "./resources/js/data/propertiesData/propertiesData.js":
/*!************************************************************!*\
  !*** ./resources/js/data/propertiesData/propertiesData.js ***!
  \************************************************************/
/*! exports provided: saveUpdateProperty, requestBookingInspection */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "saveUpdateProperty", function() { return saveUpdateProperty; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "requestBookingInspection", function() { return requestBookingInspection; });
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime/regenerator */ "./node_modules/@babel/runtime/regenerator/index.js");
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! axios */ "./node_modules/axios/index.js");
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(axios__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _services_auth__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../services/auth */ "./resources/js/services/auth.js");


function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }




var saveUpdateProperty = /*#__PURE__*/function () {
  var _ref2 = _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee(_ref) {
    var formData, url;
    return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            formData = _ref.formData, url = _ref.url;
            _context.next = 3;
            return axios__WEBPACK_IMPORTED_MODULE_2___default()({
              method: "post",
              url: url,
              data: formData,
              name: "property",
              headers: {
                Authorization: "Bearer " + Object(_services_auth__WEBPACK_IMPORTED_MODULE_3__["isLoggedIn"])()
              }
            });

          case 3:
            return _context.abrupt("return", _context.sent);

          case 4:
          case "end":
            return _context.stop();
        }
      }
    }, _callee);
  }));

  return function saveUpdateProperty(_x) {
    return _ref2.apply(this, arguments);
  };
}();
var requestBookingInspection = /*#__PURE__*/function () {
  var _ref4 = _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee2(_ref3) {
    var formData, url;
    return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee2$(_context2) {
      while (1) {
        switch (_context2.prev = _context2.next) {
          case 0:
            formData = _ref3.formData;
            url = "/api/book-inspection";
            _context2.next = 4;
            return axios__WEBPACK_IMPORTED_MODULE_2___default.a.get(url, {
              params: {
                name: formData.name,
                phone: formData.phone,
                email: formData.email,
                message: formData.message,
                propertyId: formData.propertyId,
                id: formData.dealId
              }
            });

          case 4:
            return _context2.abrupt("return", _context2.sent);

          case 5:
          case "end":
            return _context2.stop();
        }
      }
    }, _callee2);
  }));

  return function requestBookingInspection(_x2) {
    return _ref4.apply(this, arguments);
  };
}();

/***/ }),

/***/ "./resources/js/data/salesAdvice/salesAdviceData.js":
/*!**********************************************************!*\
  !*** ./resources/js/data/salesAdvice/salesAdviceData.js ***!
  \**********************************************************/
/*! exports provided: salesAdviceAdditionalInfo, salesAdviceSolicitorInfo, salesAdvicePurchaserInfo */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "salesAdviceAdditionalInfo", function() { return salesAdviceAdditionalInfo; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "salesAdviceSolicitorInfo", function() { return salesAdviceSolicitorInfo; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "salesAdvicePurchaserInfo", function() { return salesAdvicePurchaserInfo; });
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime/regenerator */ "./node_modules/@babel/runtime/regenerator/index.js");
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! axios */ "./node_modules/axios/index.js");
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(axios__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _services_auth__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../services/auth */ "./resources/js/services/auth.js");


function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }




var salesAdviceAdditionalInfo = /*#__PURE__*/function () {
  var _ref2 = _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee(_ref) {
    var additionalInfoFormData, url;
    return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            additionalInfoFormData = _ref.additionalInfoFormData;
            url = "/api/additional-info";
            _context.next = 4;
            return axios__WEBPACK_IMPORTED_MODULE_2___default()({
              method: "post",
              url: url,
              data: additionalInfoFormData,
              name: "property",
              headers: {
                Authorization: "Bearer " + Object(_services_auth__WEBPACK_IMPORTED_MODULE_3__["isLoggedIn"])()
              }
            });

          case 4:
            return _context.abrupt("return", _context.sent);

          case 5:
          case "end":
            return _context.stop();
        }
      }
    }, _callee);
  }));

  return function salesAdviceAdditionalInfo(_x) {
    return _ref2.apply(this, arguments);
  };
}();
var salesAdviceSolicitorInfo = /*#__PURE__*/function () {
  var _ref4 = _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee2(_ref3) {
    var solicitorFormData, url;
    return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee2$(_context2) {
      while (1) {
        switch (_context2.prev = _context2.next) {
          case 0:
            solicitorFormData = _ref3.solicitorFormData;
            url = "/api/solicitor-detail";
            _context2.next = 4;
            return axios__WEBPACK_IMPORTED_MODULE_2___default()({
              method: "post",
              url: url,
              data: solicitorFormData,
              name: "property",
              headers: {
                Authorization: "Bearer " + Object(_services_auth__WEBPACK_IMPORTED_MODULE_3__["isLoggedIn"])()
              }
            });

          case 4:
            return _context2.abrupt("return", _context2.sent);

          case 5:
          case "end":
            return _context2.stop();
        }
      }
    }, _callee2);
  }));

  return function salesAdviceSolicitorInfo(_x2) {
    return _ref4.apply(this, arguments);
  };
}();
var salesAdvicePurchaserInfo = /*#__PURE__*/function () {
  var _ref6 = _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee3(_ref5) {
    var purchaserFormData, url;
    return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee3$(_context3) {
      while (1) {
        switch (_context3.prev = _context3.next) {
          case 0:
            purchaserFormData = _ref5.purchaserFormData;
            url = "/api/purchaser";
            _context3.next = 4;
            return axios__WEBPACK_IMPORTED_MODULE_2___default()({
              method: "post",
              url: url,
              data: purchaserFormData,
              name: "property",
              headers: {
                Authorization: "Bearer " + Object(_services_auth__WEBPACK_IMPORTED_MODULE_3__["isLoggedIn"])()
              }
            });

          case 4:
            return _context3.abrupt("return", _context3.sent);

          case 5:
          case "end":
            return _context3.stop();
        }
      }
    }, _callee3);
  }));

  return function salesAdvicePurchaserInfo(_x3) {
    return _ref6.apply(this, arguments);
  };
}();

/***/ }),

/***/ "./resources/js/helpers/bootstrap.js":
/*!*******************************************!*\
  !*** ./resources/js/helpers/bootstrap.js ***!
  \*******************************************/
/*! exports provided: axios, initializeBootstrap */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function(process) {/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "initializeBootstrap", function() { return initializeBootstrap; });
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! axios */ "./node_modules/axios/index.js");
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(axios__WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (default from non-harmony) */ __webpack_require__.d(__webpack_exports__, "axios", function() { return axios__WEBPACK_IMPORTED_MODULE_0___default.a; });
/* harmony import */ var _services_auth__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../services/auth */ "./resources/js/services/auth.js");
/* harmony import */ var _actions_loading__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../actions/loading */ "./resources/js/actions/loading.js");
/* harmony import */ var _store__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../store */ "./resources/js/store.js");




axios__WEBPACK_IMPORTED_MODULE_0___default.a.defaults.baseURL = process.env.APP_URL;
var token = Object(_services_auth__WEBPACK_IMPORTED_MODULE_1__["isLoggedIn"])();

if (token) {
  axios__WEBPACK_IMPORTED_MODULE_0___default.a.interceptors.request.use(function (config) {
    config.headers.common['Authorization'] = "Bearer ".concat(token);

    if (config.data && !config.data.isLoading) {
      return config;
    }

    _store__WEBPACK_IMPORTED_MODULE_3__["default"].dispatch(Object(_actions_loading__WEBPACK_IMPORTED_MODULE_2__["setProcessing"])(true));
    return config;
  }, function (error) {
    _store__WEBPACK_IMPORTED_MODULE_3__["default"].dispatch(Object(_actions_loading__WEBPACK_IMPORTED_MODULE_2__["setProcessing"])(false));
    return Promise.reject(error);
  });
  axios__WEBPACK_IMPORTED_MODULE_0___default.a.interceptors.response.use(function (response) {
    _store__WEBPACK_IMPORTED_MODULE_3__["default"].dispatch(Object(_actions_loading__WEBPACK_IMPORTED_MODULE_2__["setProcessing"])(false));
    return response;
  }, function (error) {
    _store__WEBPACK_IMPORTED_MODULE_3__["default"].dispatch(Object(_actions_loading__WEBPACK_IMPORTED_MODULE_2__["setProcessing"])(false));

    if (401 === error.response.status) {
      Object(_services_auth__WEBPACK_IMPORTED_MODULE_1__["logout"])();
    } else {
      return Promise.reject(error);
    }
  });
}


var initializeBootstrap = function initializeBootstrap() {
  window.axios = axios__WEBPACK_IMPORTED_MODULE_0___default.a;
};
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! ./../../../node_modules/process/browser.js */ "./node_modules/process/browser.js")))

/***/ }),

/***/ "./resources/js/helpers/countries.js":
/*!*******************************************!*\
  !*** ./resources/js/helpers/countries.js ***!
  \*******************************************/
/*! exports provided: countries */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "countries", function() { return countries; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);

var countries = [{
  label: "Please select",
  value: ""
}, {
  label: "Afghanistan",
  value: "Afghanistan"
}, {
  label: "Åland Islands",
  value: "Åland Islands"
}, {
  label: "Albania",
  value: "Albania"
}, {
  label: "Algeria",
  value: "Algeria"
}, {
  label: "American Samoa",
  value: "American Samoa"
}, {
  label: "Andorra",
  value: "Andorra"
}, {
  label: "Angola",
  value: "Angola"
}, {
  label: "Anguilla",
  value: "Anguilla"
}, {
  label: "Antarctica",
  value: "Antarctica"
}, {
  label: "Antigua and Barbuda",
  value: "Antigua and Barbuda"
}, {
  label: "Argentina",
  value: "Argentina"
}, {
  label: "Armenia",
  value: "Armenia"
}, {
  label: "Aruba",
  value: "Aruba"
}, {
  label: "Australia",
  value: "Australia"
}, {
  label: "Austria",
  value: "AT"
}, {
  label: "Azerbaijan",
  value: "Azerbaijan"
}, {
  label: "Bahamas",
  value: "Bahamas"
}, {
  label: "Bahrain",
  value: "Bahrain"
}, {
  label: "Bangladesh",
  value: "Bangladesh"
}, {
  label: "Barbados",
  value: "Barbados"
}, {
  label: "Belarus",
  value: "Belarus"
}, {
  label: "Belgium",
  value: "Belgium"
}, {
  label: "Belize",
  value: "Belize"
}, {
  label: "Benin",
  value: "Benin"
}, {
  label: "Bermuda",
  value: "Bermuda"
}, {
  label: "Bhutan",
  value: "Bhutan"
}, {
  label: "Bolivia",
  value: "Bolivia"
}, {
  label: "Bosnia and Herzegovina",
  value: "Bosnia and Herzegovina"
}, {
  label: "Botswana",
  value: "Botswana"
}, {
  label: "Bouvet Island",
  value: "Bouvet Island"
}, {
  label: "Brazil",
  value: "Brazil"
}, {
  label: "British Indian Ocean Territory",
  value: "British Indian Ocean Territory"
}, {
  label: "Brunei Darussalam",
  value: "Brunei Darussalam"
}, {
  label: "Bulgaria",
  value: "Bulgaria"
}, {
  label: "Burkina Faso",
  value: "Burkina Faso"
}, {
  label: "Burundi",
  value: "Burundi"
}, {
  label: "Cambodia",
  value: "Cambodia"
}, {
  label: "Cameroon",
  value: "Cameroon"
}, {
  label: "Canada",
  value: "Canada"
}, {
  label: "Cape Verde",
  value: "Cape Verde"
}, {
  label: "Cayman Islands",
  value: "Cayman Islands"
}, {
  label: "Central African Republic",
  value: "Central African Republic"
}, {
  label: "Chad",
  value: "Chad"
}, {
  label: "Chile",
  value: "Chile"
}, {
  label: "China",
  value: "China"
}, {
  label: "Christmas Island",
  value: "Christmas Island"
}, {
  label: "Cocos (Keeling) Islands",
  value: "Cocos (Keeling) Islands"
}, {
  label: "Colombia",
  value: "Colombia"
}, {
  label: "Comoros",
  value: "Comoros"
}, {
  label: "Congo",
  value: "Congo"
}, {
  label: "Congo, The Democratic Republic of the",
  value: "Congo, The Democratic Republic of the"
}, {
  label: "Cook Islands",
  value: "Cook Islands"
}, {
  label: "Costa Rica",
  value: "Costa Rica"
}, {
  label: "Cote D'Ivoire",
  value: "Cote D'Ivoire"
}, {
  label: "Croatia",
  value: "Croatia"
}, {
  label: "Cuba",
  value: "Cuba"
}, {
  label: "Cyprus",
  value: "Cyprus"
}, {
  label: "Czech Republic",
  value: "Czech Republic"
}, {
  label: "Denmark",
  value: "Denmark"
}, {
  label: "Djibouti",
  value: "Djibouti"
}, {
  label: "Dominica",
  value: "Dominica"
}, {
  label: "Dominican Republic",
  value: "Dominican Republic"
}, {
  label: "Ecuador",
  value: "Ecuador"
}, {
  label: "Egypt",
  value: "Egypt"
}, {
  label: "El Salvador",
  value: "El Salvador"
}, {
  label: "Equatorial Guinea",
  value: "Equatorial Guinea"
}, {
  label: "Eritrea",
  value: "Eritrea"
}, {
  label: "Estonia",
  value: "Estonia"
}, {
  label: "Ethiopia",
  value: "Ethiopia"
}, {
  label: "Falkland Islands (Malvinas)",
  value: "Falkland Islands"
}, {
  label: "Faroe Islands",
  value: "Faroe Islands"
}, {
  label: "Fiji",
  value: "Fiji"
}, {
  label: "Finland",
  value: "Finland"
}, {
  label: "France",
  value: "France"
}, {
  label: "French Guiana",
  value: "French Guiana"
}, {
  label: "French Polynesia",
  value: "French Polynesia"
}, {
  label: "French Southern Territories",
  value: "French Southern Territories"
}, {
  label: "Gabon",
  value: "Gabon"
}, {
  label: "Gambia",
  value: "Gambia"
}, {
  label: "Georgia",
  value: "Georgia"
}, {
  label: "Germany",
  value: "Germany"
}, {
  label: "Ghana",
  value: "Ghana"
}, {
  label: "Gibraltar",
  value: "Gibraltar"
}, {
  label: "Greece",
  value: "Greece"
}, {
  label: "Greenland",
  value: "Greenland"
}, {
  label: "Grenada",
  value: "Grenada"
}, {
  label: "Guadeloupe",
  value: "Guadeloupe"
}, {
  label: "Guam",
  value: "Guam"
}, {
  label: "Guatemala",
  value: "Guatemala"
}, {
  label: "Guernsey",
  value: "Guernsey"
}, {
  label: "Guinea",
  value: "Guinea"
}, {
  label: "Guinea-Bissau",
  value: "Guinea-Bissau"
}, {
  label: "Guyana",
  value: "Guyana"
}, {
  label: "Haiti",
  value: "Haiti"
}, {
  label: "Heard Island and Mcdonald Islands",
  value: "Heard Island and Mcdonald Islands"
}, {
  label: "Holy See (Vatican City State)",
  value: "Holy See (Vatican City State)"
}, {
  label: "Honduras",
  value: "Honduras"
}, {
  label: "Hong Kong",
  value: "Hong Kong"
}, {
  label: "Hungary",
  value: "Hungary"
}, {
  label: "Iceland",
  value: "Iceland"
}, {
  label: "India",
  value: "India"
}, {
  label: "Indonesia",
  value: "Indonesia"
}, {
  label: "Iran, Islamic Republic Of",
  value: "Iran, Islamic Republic Of"
}, {
  label: "Iraq",
  value: "Iraq"
}, {
  label: "Ireland",
  value: "Ireland"
}, {
  label: "Isle of Man",
  value: "Isle of Man"
}, {
  label: "Israel",
  value: "Israel"
}, {
  label: "Italy",
  value: "Italy"
}, {
  label: "Jamaica",
  value: "Jamaica"
}, {
  label: "Japan",
  value: "Japan"
}, {
  label: "Jersey",
  value: "Jersey"
}, {
  label: "Jordan",
  value: "Jordan"
}, {
  label: "Kazakhstan",
  value: "Kazakhstan"
}, {
  label: "Kenya",
  value: "Kenya"
}, {
  label: "Kiribati",
  value: "Kiribati"
}, {
  label: "Korea, Democratic People'S Republic of",
  value: "Korea, Democratic People'S Republic of"
}, {
  label: "Korea, Republic of",
  value: "Korea, Republic of"
}, {
  label: "Kuwait",
  value: "Kuwait"
}, {
  label: "Kyrgyzstan",
  value: "Kyrgyzstan"
}, {
  label: "Lao People'S Democratic Republic",
  value: "Lao People'S Democratic Republic"
}, {
  label: "Latvia",
  value: "Latvia"
}, {
  label: "Lebanon",
  value: "Lebanon"
}, {
  label: "Lesotho",
  value: "Lesotho"
}, {
  label: "Liberia",
  value: "Liberia"
}, {
  label: "Libyan Arab Jamahiriya",
  value: "Libyan Arab Jamahiriya"
}, {
  label: "Liechtenstein",
  value: "Liechtenstein"
}, {
  label: "Lithuania",
  value: "Lithuania"
}, {
  label: "Luxembourg",
  value: "Luxembourg"
}, {
  label: "Macao",
  value: "Macao"
}, {
  label: "Macedonia, The Former Yugoslav Republic of",
  value: "Macedonia, The Former Yugoslav Republic of"
}, {
  label: "Madagascar",
  value: "Madagascar"
}, {
  label: "Malawi",
  value: "Malawi"
}, {
  label: "Malaysia",
  value: "Malaysia"
}, {
  label: "Maldives",
  value: "Maldives"
}, {
  label: "Mali",
  value: "Mali"
}, {
  label: "Malta",
  value: "Malta"
}, {
  label: "Marshall Islands",
  value: "Marshall Islands"
}, {
  label: "Martinique",
  value: "Martinique"
}, {
  label: "Mauritania",
  value: "Mauritania"
}, {
  label: "Mauritius",
  value: "Mauritius"
}, {
  label: "Mayotte",
  value: "Mayotte"
}, {
  label: "Mexico",
  value: "Mexico"
}, {
  label: "Micronesia, Federated States of",
  value: "Micronesia, Federated States of"
}, {
  label: "Moldova, Republic of",
  value: "Moldova, Republic of"
}, {
  label: "Monaco",
  value: "Monaco"
}, {
  label: "Mongolia",
  value: "Mongolia"
}, {
  label: "Montserrat",
  value: "Montserrat"
}, {
  label: "Morocco",
  value: "Morocco"
}, {
  label: "Mozambique",
  value: "Mozambique"
}, {
  label: "Myanmar",
  value: "Myanmar"
}, {
  label: "Namibia",
  value: "Namibia"
}, {
  label: "Nauru",
  value: "Nauru"
}, {
  label: "Nepal",
  value: "Nepal"
}, {
  label: "Netherlands",
  value: "Netherlands"
}, {
  label: "Netherlands Antilles",
  value: "Netherlands Antilles"
}, {
  label: "New Caledonia",
  value: "New Caledonia"
}, {
  label: "New Zealand",
  value: "New Zealand"
}, {
  label: "Nicaragua",
  value: "Nicaragua"
}, {
  label: "Niger",
  value: "Niger"
}, {
  label: "Nigeria",
  value: "Nigeria"
}, {
  label: "Niue",
  value: "Niue"
}, {
  label: "Norfolk Island",
  value: "Norfolk Island"
}, {
  label: "Northern Mariana Islands",
  value: "Northern Mariana Islands"
}, {
  label: "Norway",
  value: "Norway"
}, {
  label: "Oman",
  value: "Oman"
}, {
  label: "Pakistan",
  value: "Pakistan"
}, {
  label: "Palau",
  value: "Palau"
}, {
  label: "Palestinian Territory, Occupied",
  value: "Palestinian Territory, Occupied"
}, {
  label: "Panama",
  value: "Panama"
}, {
  label: "Papua New Guinea",
  value: "Papua New Guinea"
}, {
  label: "Paraguay",
  value: "Paraguay"
}, {
  label: "Peru",
  value: "Peru"
}, {
  label: "Philippines",
  value: "Philippines"
}, {
  label: "Pitcairn",
  value: "Pitcairn"
}, {
  label: "Poland",
  value: "Poland"
}, {
  label: "Portugal",
  value: "Portugal"
}, {
  label: "Puerto Rico",
  value: "Puerto Rico"
}, {
  label: "Qatar",
  value: "Qatar"
}, {
  label: "Reunion",
  value: "Reunion"
}, {
  label: "Romania",
  value: "Romania"
}, {
  label: "Russian Federation",
  value: "Russian Federation"
}, {
  label: "Rwanda",
  value: "Rwanda"
}, {
  label: "Saint Helena",
  value: "Saint Helena"
}, {
  label: "Saint Kitts and Nevis",
  value: "Saint Kitts"
}, {
  label: "Saint Lucia",
  value: "Saint Lucia"
}, {
  label: "Saint Pierre and Miquelon",
  value: "Saint Pierre and Miquelon"
}, {
  label: "Saint Vincent and the Grenadines",
  value: "Saint Vincent and the Grenadines"
}, {
  label: "Samoa",
  value: "Samoa"
}, {
  label: "San Marino",
  value: "San Marino"
}, {
  label: "Sao Tome and Principe",
  value: "Sao Tome and Principe"
}, {
  label: "Saudi Arabia",
  value: "Saudi Arabia"
}, {
  label: "Senegal",
  value: "Senegal"
}, {
  label: "Serbia and Montenegro",
  value: "Serbia and Montenegro"
}, {
  label: "Seychelles",
  value: "Seychelles"
}, {
  label: "Sierra Leone",
  value: "Sierra Leone"
}, {
  label: "Singapore",
  value: "Singapore"
}, {
  label: "Slovakia",
  value: "Slovakia"
}, {
  label: "Slovenia",
  value: "Slovenia"
}, {
  label: "Solomon Islands",
  value: "Solomon Islands"
}, {
  label: "Somalia",
  value: "Somalia"
}, {
  label: "South Africa",
  value: "South Africa"
}, {
  label: "South Georgia and the South Sandwich Islands",
  value: "South Georgia and the South Sandwich Islands"
}, {
  label: "Spain",
  value: "Spain"
}, {
  label: "Sri Lanka",
  value: "Sri Lanka"
}, {
  label: "Sudan",
  value: "Sudan"
}, {
  label: "Surilabel",
  value: "Surilabel"
}, {
  label: "Svalbard and Jan Mayen",
  value: "Svalbard and Jan Mayen"
}, {
  label: "Swaziland",
  value: "Swaziland"
}, {
  label: "Sweden",
  value: "Sweden"
}, {
  label: "Switzerland",
  value: "Switzerland"
}, {
  label: "Syrian Arab Republic",
  value: "Syrian Arab Republic"
}, {
  label: "Taiwan, Province of China",
  value: "Taiwan, Province of China"
}, {
  label: "Tajikistan",
  value: "Tajikistan"
}, {
  label: "Tanzania, United Republic of",
  value: "Tanzania, United Republic of"
}, {
  label: "Thailand",
  value: "Thailand"
}, {
  label: "Timor-Leste",
  value: "Timor-Leste"
}, {
  label: "Togo",
  value: "Togo"
}, {
  label: "Tokelau",
  value: "Tokelau"
}, {
  label: "Tonga",
  value: "Tonga"
}, {
  label: "Trinidad and Tobago",
  value: "Trinidad and Tobago"
}, {
  label: "Tunisia",
  value: "Tunisia"
}, {
  label: "Turkey",
  value: "Turkey"
}, {
  label: "Turkmenistan",
  value: "Turkmenistan"
}, {
  label: "Turks and Caicos Islands",
  value: "Turks and Caicos Islands"
}, {
  label: "Tuvalu",
  value: "Tuvalu"
}, {
  label: "Uganda",
  value: "Uganda"
}, {
  label: "Ukraine",
  value: "Ukraine"
}, {
  label: "United Arab Emirates",
  value: "United Arab Emirates"
}, {
  label: "United Kingdom",
  value: "United Kingdom"
}, {
  label: "United States",
  value: "United States"
}, {
  label: "United States Minor Outlying Islands",
  value: "United States Minor Outlying Islands"
}, {
  label: "Uruguay",
  value: "Uruguay"
}, {
  label: "Uzbekistan",
  value: "Uzbekistan"
}, {
  label: "Vanuatu",
  value: "Vanuatu"
}, {
  label: "Venezuela",
  value: "Venezuela"
}, {
  label: "Viet Nam",
  value: "Viet Nam"
}, {
  label: "Virgin Islands, British",
  value: "Virgin Islands, British"
}, {
  label: "Virgin Islands, U.S.",
  value: "Virgin Islands, U.S."
}, {
  label: "Wallis and Futuna",
  value: "Wallis and Futuna"
}, {
  label: "Western Sahara",
  value: "Western Sahara"
}, {
  label: "Yemen",
  value: "Yemen"
}, {
  label: "Zambia",
  value: "Zambia"
}, {
  label: "Zimbabwe",
  value: "Zimbabwe"
}];

/***/ }),

/***/ "./resources/js/helpers/deviceSizeHelper.js":
/*!**************************************************!*\
  !*** ./resources/js/helpers/deviceSizeHelper.js ***!
  \**************************************************/
/*! exports provided: xxSmall, xtraSmall, small, baseSmall, medium, baseLarge, large, xtraLarge, smallScreen, standardScreen */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "xxSmall", function() { return xxSmall; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "xtraSmall", function() { return xtraSmall; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "small", function() { return small; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "baseSmall", function() { return baseSmall; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "medium", function() { return medium; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "baseLarge", function() { return baseLarge; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "large", function() { return large; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "xtraLarge", function() { return xtraLarge; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "smallScreen", function() { return smallScreen; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "standardScreen", function() { return standardScreen; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);

var xxSmall = 320;
var xtraSmall = 375;
var small = 640;
var baseSmall = 430;
var medium = 768;
var baseLarge = 667;
var large = 1024;
var xtraLarge = 1280; //Small screen laptop

var smallScreen = 1366; //Standard screen laptop

var standardScreen = 1440;

/***/ }),

/***/ "./resources/js/helpers/numberInput.js":
/*!*********************************************!*\
  !*** ./resources/js/helpers/numberInput.js ***!
  \*********************************************/
/*! exports provided: inputRegex */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "inputRegex", function() { return inputRegex; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);

var inputRegex = function inputRegex(input) {
  var regEx = new RegExp('^[0-9]*$');
  return regEx.test(input);
};

/***/ }),

/***/ "./resources/js/helpers/propertyHelper/propertyHelper.js":
/*!***************************************************************!*\
  !*** ./resources/js/helpers/propertyHelper/propertyHelper.js ***!
  \***************************************************************/
/*! exports provided: ownership_types, sub_property_types, property_types, states, costDueDates, floorPlanHelper */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ownership_types", function() { return ownership_types; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "sub_property_types", function() { return sub_property_types; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "property_types", function() { return property_types; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "states", function() { return states; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "costDueDates", function() { return costDueDates; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "floorPlanHelper", function() { return floorPlanHelper; });
/* harmony import */ var _material_ui_icons__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @material-ui/icons */ "./node_modules/@material-ui/icons/esm/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);


var ownership_types = [{
  label: "Owner Occupied",
  value: "1"
}, {
  label: "Investment",
  value: "2"
}];
var sub_property_types = [{
  label: "Apartment",
  value: "1"
}, {
  label: "House",
  value: "2"
}, {
  label: "House & Land",
  value: "3"
}, {
  label: "Land",
  value: "4"
}, {
  label: "Townhouse",
  value: "5"
}];
var property_types = [{
  label: "Apartment",
  value: "1"
}, {
  label: "House",
  value: "2"
}, {
  label: "House & Land",
  value: "3"
}, {
  label: "Land",
  value: "4"
}, {
  label: "Townhouse",
  value: "5"
}];
var states = [{
  label: "Please select",
  value: ""
}, {
  label: "NSW",
  value: "NSW"
}, {
  label: "QLD",
  value: "QLD"
}, {
  label: "SA",
  value: "SA"
}, {
  label: "TAS",
  value: "TAS"
}, {
  label: "VIC",
  value: "VIC"
}, {
  label: "WA",
  value: "WA"
}, {
  label: "ACT",
  value: "ACT"
}, {
  label: "NT",
  value: "NT"
}];
var costDueDates = [{
  label: "Please select...",
  value: ""
}, {
  label: "Annual",
  value: "Annual"
}, {
  label: "Quarterly",
  value: "Quarterly"
}];
var floorPlanHelper = function floorPlanHelper(fileParam) {
  return Object.values(fileParam).map(function (file) {
    var newFilename = file.name.replace(/[^.a-zA-Z0-9]/g, "");
    var newFileObj = new File([file], newFilename);
    return Object.assign(newFileObj, {
      resource: URL.createObjectURL(newFileObj)
    });
  });
};

/***/ }),

/***/ "./resources/js/helpers/styles/editor.css":
/*!************************************************!*\
  !*** ./resources/js/helpers/styles/editor.css ***!
  \************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {


var content = __webpack_require__(/*! !../../../../node_modules/css-loader??ref--6-1!../../../../node_modules/postcss-loader/src??ref--6-2!./editor.css */ "./node_modules/css-loader/index.js?!./node_modules/postcss-loader/src/index.js?!./resources/js/helpers/styles/editor.css");

if(typeof content === 'string') content = [[module.i, content, '']];

var transform;
var insertInto;



var options = {"hmr":true}

options.transform = transform
options.insertInto = undefined;

var update = __webpack_require__(/*! ../../../../node_modules/style-loader/lib/addStyles.js */ "./node_modules/style-loader/lib/addStyles.js")(content, options);

if(content.locals) module.exports = content.locals;

if(false) {}

/***/ })

}]);