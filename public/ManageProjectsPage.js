(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["ManageProjectsPage"],{

/***/ "./resources/js/pages/ManageProjectsPage.js":
/*!**************************************************!*\
  !*** ./resources/js/pages/ManageProjectsPage.js ***!
  \**************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _layouts_users_UserLayout__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../layouts/users/UserLayout */ "./resources/js/layouts/users/UserLayout.js");
/* harmony import */ var _components_base__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../components/_base */ "./resources/js/components/_base/index.js");
/* harmony import */ var _components_base__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(_components_base__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _states_userGlobal__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../states/userGlobal */ "./resources/js/states/userGlobal.js");
/* harmony import */ var _components_SequenceAddingModal__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../components/SequenceAddingModal */ "./resources/js/components/SequenceAddingModal.js");
/* harmony import */ var _components_SequenceEditingModal__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../components/SequenceEditingModal */ "./resources/js/components/SequenceEditingModal.js");
/* harmony import */ var _fortawesome_react_fontawesome__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @fortawesome/react-fontawesome */ "./node_modules/@fortawesome/react-fontawesome/index.es.js");
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! lodash */ "./node_modules/lodash/lodash.js");
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_7___default = /*#__PURE__*/__webpack_require__.n(lodash__WEBPACK_IMPORTED_MODULE_7__);
/* harmony import */ var react_device_detect__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! react-device-detect */ "./node_modules/react-device-detect/main.js");
/* harmony import */ var react_device_detect__WEBPACK_IMPORTED_MODULE_8___default = /*#__PURE__*/__webpack_require__.n(react_device_detect__WEBPACK_IMPORTED_MODULE_8__);
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! moment */ "./node_modules/moment/moment.js");
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_9___default = /*#__PURE__*/__webpack_require__.n(moment__WEBPACK_IMPORTED_MODULE_9__);
/* harmony import */ var _fortawesome_free_solid_svg_icons__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @fortawesome/free-solid-svg-icons */ "./node_modules/@fortawesome/free-solid-svg-icons/index.es.js");
/* harmony import */ var _helpers_addressHelper__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ../helpers/addressHelper */ "./resources/js/helpers/addressHelper.js");
/* harmony import */ var _helpers_deviceSizeHelper__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ../helpers/deviceSizeHelper */ "./resources/js/helpers/deviceSizeHelper.js");
/* harmony import */ var react_helmet__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! react-helmet */ "./node_modules/react-helmet/es/Helmet.js");
/* harmony import */ var _helpers_metaHelper_metaHelper__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ../helpers/metaHelper/metaHelper */ "./resources/js/helpers/metaHelper/metaHelper.js");
/* harmony import */ var _components_base_alerts_sweetAlert__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! ../components/_base/alerts/sweetAlert */ "./resources/js/components/_base/alerts/sweetAlert.js");
function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _unsupportedIterableToArray(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

function _iterableToArrayLimit(arr, i) { if (typeof Symbol === "undefined" || !(Symbol.iterator in Object(arr))) return; var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }


















var ManageProjects = function ManageProjects() {
  var _UserGlobal = Object(_states_userGlobal__WEBPACK_IMPORTED_MODULE_3__["default"])(),
      _UserGlobal2 = _slicedToArray(_UserGlobal, 2),
      userState = _UserGlobal2[0],
      userAction = _UserGlobal2[1];

  var _useState = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])(""),
      _useState2 = _slicedToArray(_useState, 2),
      keyWord = _useState2[0],
      setKeyWord = _useState2[1];

  var _useState3 = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])(false),
      _useState4 = _slicedToArray(_useState3, 2),
      toggleFetch = _useState4[0],
      setToggleFetch = _useState4[1];

  var _useState5 = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])([]),
      _useState6 = _slicedToArray(_useState5, 2),
      projects = _useState6[0],
      setProjects = _useState6[1];

  var _useState7 = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])({
    sortBy: {
      value: "deals.created_at",
      order: "desc"
    }
  }),
      _useState8 = _slicedToArray(_useState7, 2),
      filters = _useState8[0],
      setFilters = _useState8[1];

  var _useState9 = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])(false),
      _useState10 = _slicedToArray(_useState9, 2),
      toggleOrder = _useState10[0],
      setToggleOrder = _useState10[1];

  var _useState11 = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])(false),
      _useState12 = _slicedToArray(_useState11, 2),
      toggleEditProject = _useState12[0],
      setToggleEditProject = _useState12[1];

  var _useState13 = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])(false),
      _useState14 = _slicedToArray(_useState13, 2),
      toggleAddProject = _useState14[0],
      setToggleAddProject = _useState14[1];

  var _useState15 = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])(false),
      _useState16 = _slicedToArray(_useState15, 2),
      toggleSequenceAdding = _useState16[0],
      setToggleSequenceAdding = _useState16[1];

  var _useState17 = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])(false),
      _useState18 = _slicedToArray(_useState17, 2),
      toggleSequenceEdit = _useState18[0],
      setToggleSequenceEdit = _useState18[1];

  var _useState19 = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])([]),
      _useState20 = _slicedToArray(_useState19, 2),
      project = _useState20[0],
      setProject = _useState20[1];

  var _useState21 = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])(false),
      _useState22 = _slicedToArray(_useState21, 2),
      toggleInfoModal = _useState22[0],
      setToggleInfoModal = _useState22[1];

  Object(react__WEBPACK_IMPORTED_MODULE_0__["useEffect"])(function () {
    if (!toggleAddProject) {
      setToggleFetch(!toggleFetch);
    }

    return function () {};
  }, [toggleAddProject]);
  Object(react__WEBPACK_IMPORTED_MODULE_0__["useEffect"])(function () {
    if (!toggleEditProject) {
      setToggleFetch(!toggleFetch);
    }

    return function () {};
  }, [toggleEditProject]);

  var formatFilter = function formatFilter(column) {
    if (column) {
      setToggleOrder(!toggleOrder);
      setFilters({
        sortBy: {
          value: column,
          order: toggleOrder ? "desc" : "asc"
        }
      });
      setToggleFetch(!toggleFetch);
    }
  };

  var tableHeaders = [{
    label: "Deal Id",
    width: 80,
    show: react_device_detect__WEBPACK_IMPORTED_MODULE_8__["isMobile"] ? false : true
  }, {
    label: "Project Name",
    column: "deals.name",
    sortable: true,
    style: "",
    width: 100,
    show: react_device_detect__WEBPACK_IMPORTED_MODULE_8__["isMobile"] ? true : false
  }, {
    label: "Property Address",
    column: "deals.address",
    sortable: true,
    style: "",
    width: 200,
    show: react_device_detect__WEBPACK_IMPORTED_MODULE_8__["isMobile"] ? false : true
  }, {
    label: "Property Type",
    column: "deals.sub_property_id",
    sortable: true,
    width: react_device_detect__WEBPACK_IMPORTED_MODULE_8__["isMobile"] ? 120 : 150,
    show: react_device_detect__WEBPACK_IMPORTED_MODULE_8__["isMobile"] ? true : false
  }, {
    label: "Proposed Settlement",
    column: "deals.proposed_settlement",
    sortable: true,
    width: 150,
    show: react_device_detect__WEBPACK_IMPORTED_MODULE_8__["isMobile"] ? false : true
  }, {
    label: "Project Time Limit",
    column: "deals.deal_time_limit",
    width: 150,
    show: react_device_detect__WEBPACK_IMPORTED_MODULE_8__["isMobile"] ? false : true
  }, {
    label: "Property Count",
    column: "",
    sortable: false,
    style: "text-center",
    width: 150,
    show: true
  }, {
    label: "Approved",
    column: "deals.is_deal_approved",
    sortable: true,
    style: "text-center",
    width: 60,
    show: react_device_detect__WEBPACK_IMPORTED_MODULE_8__["isMobile"] ? false : true
  }, {
    label: react_device_detect__WEBPACK_IMPORTED_MODULE_8__["isMobile"] ? "View" : "Edit",
    column: "",
    sortable: false,
    width: 60,
    show: true
  }];
  var handleSearch = Object(lodash__WEBPACK_IMPORTED_MODULE_7__["debounce"])(function (text) {
    setKeyWord(text);
  }, 800);

  var _toggleSequenceAddingModal = function toggleSequenceAddingModal() {
    setToggleSequenceAdding(!toggleSequenceAdding);
  };

  var _toggleSequenceEditModal = function toggleSequenceEditModal() {
    setToggleSequenceEdit(!toggleSequenceEdit);
  };

  var renderHeaders = function renderHeaders() {
    if (!react_device_detect__WEBPACK_IMPORTED_MODULE_8__["isMobile"]) {
      return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("thead", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("tr", null, tableHeaders.map(function (th, index) {
        return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("th", {
          onClick: function onClick() {
            return formatFilter(th.column);
          },
          style: {
            width: th.width
          },
          className: "".concat(th.style, " border-b font-bold px-4 py-2 text-palette-gray px-4 py-2  ").concat(th.sortable ? "cursor-pointer hover:text-palette-gray-dark" : " ", "\n              ").concat(th.label === "Approved" ? "text-left" : "text-left", "\n\n              "),
          key: index
        }, th.label);
      })));
    } else {
      return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("thead", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("tr", {
        className: "bg-gray-200"
      }, tableHeaders.map(function (th, index) {
        if (th.show) {
          return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("th", {
            onClick: function onClick() {
              return formatFilter(th.column);
            },
            style: {
              width: th.width
            },
            className: "".concat(th.style, " border-b font-bold px-4 py-2 text-palette-gray px-4 py-2  ").concat(th.sortable ? "cursor-pointer hover:text-palette-gray-dark" : " ", "\n              ").concat(th.label === "View" ? "text-center" : "text-left", "\n              ").concat(th.label === "Property Count" && userState.windowSize <= _helpers_deviceSizeHelper__WEBPACK_IMPORTED_MODULE_12__["baseSmall"] ? "hidden" : "", "\n              "),
            key: index
          }, th.label);
        }
      })));
    }
  };

  var projectsContent = function projectsContent() {
    if (!react_device_detect__WEBPACK_IMPORTED_MODULE_8__["isMobile"]) {
      return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react__WEBPACK_IMPORTED_MODULE_0___default.a.Fragment, null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("tbody", null, projects.map(function (project) {
        var timeLimit = project.expires_at ? JSON.parse(project.expires_at) : project.created_at;
        return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("tr", {
          key: project.id,
          className: "hover:bg-gray-100 border-b text-base text-center"
        }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("td", {
          className: "pl-4 text-left"
        }, project.id), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("td", {
          onClick: function onClick() {
            window.location = "/profile/manage-properties/".concat(project.id, "/").concat(project.sub_property_id);
          },
          className: "pl-4 text-left transform font-bold hover:text-palette-blue-light cursor-pointer text-palette-purple"
        }, project.name), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("td", {
          className: "p-4 text-left"
        }, Object(_helpers_addressHelper__WEBPACK_IMPORTED_MODULE_11__["addressHelper"])(project.address)), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("td", {
          className: "p-4 text-left text-left"
        }, project.deal_type), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("td", {
          className: "p-4 text-left text-left"
        }, " ", project.sub_property_id != 4 ? moment__WEBPACK_IMPORTED_MODULE_9___default()(project.proposed_settlement).format("DD-MM-YYYY") : moment__WEBPACK_IMPORTED_MODULE_9___default()(project.land_registration).format("DD-MM-YYYY")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("td", {
          className: "p-4 text-left"
        }, project.expires_at ? moment__WEBPACK_IMPORTED_MODULE_9___default()(timeLimit.time_limit).format("DD-MM-YYYY") : moment__WEBPACK_IMPORTED_MODULE_9___default()(project.deal_time_limit).format("DD-MM-YYYY")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("td", {
          className: "p-4 text-center"
        }, project.properties ? "".concat(project.properties.length, " / 5") : "0 / 5"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("td", {
          className: "p-4 text-center"
        }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("svg", {
          className: "feather-icon ml-4 ".concat(project.is_deal_approved ? "text-palette-purple h-6 w-6" : "text-gray-500 h-5 w-5")
        }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("use", {
          xlinkHref: "/assets/svg/feather-sprite.svg#check"
        }))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("td", {
          className: "p-4"
        }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_base__WEBPACK_IMPORTED_MODULE_2__["Tooltip"], {
          title: "Edit Project"
        }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
          onClick: function onClick() {
            _toggleSequenceEditModal();

            setProject(project);
          },
          className: "cursor-pointer bg-white border border-gray-400 duration-300 hover:bg-palette-purple hover:border-palette-purple hover:text-white inline-flex px-3 py-1 rounded transition-all"
        }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("svg", {
          className: "feather-icon h-5 w-5 opacity-50"
        }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("use", {
          xlinkHref: "/assets/svg/feather-sprite.svg#edit"
        }))))));
      })));
    } else {
      return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react__WEBPACK_IMPORTED_MODULE_0___default.a.Fragment, null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("tbody", null, projects.map(function (project) {
        return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("tr", {
          key: project.id,
          className: "hover:bg-gray-100 border-b text-base text-center"
        }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("td", {
          onClick: function onClick() {
            window.location = "/profile/manage-properties/".concat(project.id);
          },
          className: "pl-4 text-left transform font-bold hover:text-palette-blue-light cursor-pointer text-palette-purple"
        }, " ", project.name), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("td", {
          className: "p-4 text-left text-left"
        }, " ", project.deal_type, " "), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("td", {
          className: "p-4 text-center  ".concat(userState.windowSize <= _helpers_deviceSizeHelper__WEBPACK_IMPORTED_MODULE_12__["baseSmall"] ? "hidden" : "")
        }, project.properties ? "".concat(project.properties.length, " / 5") : "0 / 5"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("td", {
          className: "p-4"
        }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_base__WEBPACK_IMPORTED_MODULE_2__["Tooltip"], {
          title: "Edit Project"
        }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
          onClick: function onClick() {
            setProject(project);

            _toggleSequenceEditModal();
          },
          className: "cursor-pointer bg-white border border-gray-400 duration-300 hover:bg-palette-purple hover:border-palette-purple hover:text-white inline-flex px-3 py-1 rounded transition-all"
        }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("svg", {
          className: "feather-icon h-5 w-5 opacity-50"
        }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("use", {
          xlinkHref: "/assets/svg/feather-sprite.svg#eye"
        }))))));
      })));
    }
  };

  var dislayText = function dislayText() {
    return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: "my-10",
      key: 1
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
      className: "block font-extrabold mt-2 text-2xl text-center text-gray-500 mb-5"
    }, "Welcome to GroupBuyer"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
      className: "block mt-2 text-base text-gray-500 text-left"
    }, "When adding a project ensure you upload all 5 properties/lots and fill in all required fields. Your project will be approved by a GroupBuyer Admin Agent prior to being displayed online. Should your project not meet the requirements of GroupBuyer, a team member will be in contact with you. Click the 'View Agency Agreement' button to learn more about our terms and conditions."));
  };

  var callEditToggleFetch = function callEditToggleFetch() {
    setToggleFetch(!toggleFetch);
  };

  var callAddToggleFetch = function callAddToggleFetch() {
    setToggleFetch(!toggleFetch);
  };

  var addProject = function addProject() {
    return react_device_detect__WEBPACK_IMPORTED_MODULE_8__["isMobile"] ? Object(_components_base_alerts_sweetAlert__WEBPACK_IMPORTED_MODULE_15__["sweetAlert"])("error", "In order to upload a project this must be done via desktop.") : _toggleSequenceAddingModal();
  };

  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react__WEBPACK_IMPORTED_MODULE_0___default.a.Fragment, null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_layouts_users_UserLayout__WEBPACK_IMPORTED_MODULE_1__["default"], null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_helmet__WEBPACK_IMPORTED_MODULE_13__["Helmet"], null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("meta", {
    name: "description",
    content: _helpers_metaHelper_metaHelper__WEBPACK_IMPORTED_MODULE_14__["metaHelper"].desc
  })), toggleSequenceEdit && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_SequenceEditingModal__WEBPACK_IMPORTED_MODULE_5__["default"], {
    project: project,
    editToggleFetch: callEditToggleFetch,
    toggleSequenceEditModal: function toggleSequenceEditModal() {
      return _toggleSequenceEditModal();
    }
  }), toggleSequenceAdding && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_SequenceAddingModal__WEBPACK_IMPORTED_MODULE_4__["default"], {
    addToggleFetch: callAddToggleFetch,
    toggleSequenceAddingModal: function toggleSequenceAddingModal() {
      return _toggleSequenceAddingModal();
    }
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("section", {
    className: "bg-palette-blue-dark",
    style: {
      marginTop: -130,
      paddingTop: 160
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("section", {
    className: "relative bg-white"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "pb-16 px-6 lg:px-20 mx-auto",
    style: {
      maxWidth: 1600
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("h1", {
    className: "font-bold leading-tight text-4xl pt-10 pb-8 lg:pb-16 text-center"
  }, "Manage projects"), !react_device_detect__WEBPACK_IMPORTED_MODULE_8__["isMobile"] && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "flex justify-between mb-8"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "flex justify-center lg:justify-end"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "w-64 relative "
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("svg", {
    className: "absolute feather-icon h-8 left-0 mx-3 text-gray-500"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("use", {
    xlinkHref: "/assets/svg/feather-sprite.svg#search"
  })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_base__WEBPACK_IMPORTED_MODULE_2__["TextInput"], {
    className: "pl-8",
    type: "text",
    placeholder: "Search",
    border: false,
    appearance: true,
    onChange: function onChange(e) {
      return handleSearch(e.target.value);
    }
  }))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "flex justify-center lg:justify-end"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("a", {
    href: "/assets/pdf/group-buyer-agency-agreement-2020-10-08-draft.pdf",
    target: "_blank",
    className: "border border-gray-400 button flex items-center justify-around mr-2"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
    className: "text-base text-gray-600"
  }, "View Agency Agreement")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_base__WEBPACK_IMPORTED_MODULE_2__["Tooltip"], {
    title: "Click to add a Project"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "w-auto"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_base__WEBPACK_IMPORTED_MODULE_2__["Button"], {
    className: "flex justify-around items-center",
    onClick: function onClick() {
      _toggleSequenceAddingModal();
    }
  }, "Add Project"))))), react_device_detect__WEBPACK_IMPORTED_MODULE_8__["isMobile"] && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "flex justify-between mb-8"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "flex justify-center md:flex-1 md:justify-start xs:w-48"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "w-64 relative "
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("svg", {
    className: "absolute feather-icon h-8 left-0 mx-3 text-gray-500"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("use", {
    xlinkHref: "/assets/svg/feather-sprite.svg#search"
  })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_base__WEBPACK_IMPORTED_MODULE_2__["TextInput"], {
    className: "pl-8",
    type: "text",
    placeholder: "Search",
    border: false,
    appearance: true,
    onChange: function onChange(e) {
      return handleSearch(e.target.value);
    }
  }))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "flex items-center justify-center md:flex-1 md:justify-end md:mr-12"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "flex justify-end ml-10"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("a", {
    href: "/assets/pdf/group-buyer-agency-agreement-2020-10-08-draft.pdf",
    target: "_blank",
    className: "border border-gray-400 button flex items-center justify-around mr-2"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
    className: "text-base text-gray-600"
  }, "View Agency Agreement")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
    className: "font-semibold text-palette-purple cursor-pointer",
    onClick: function onClick() {
      return addProject();
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_fortawesome_react_fontawesome__WEBPACK_IMPORTED_MODULE_6__["FontAwesomeIcon"], {
    icon: _fortawesome_free_solid_svg_icons__WEBPACK_IMPORTED_MODULE_10__["faPlus"],
    className: "fa-plus mr-2"
  }), "Add Project")))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "lg:mb-12"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
    className: "font-bold text-lg"
  }, "Note:\xA0"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
    className: "font-semibold text-red-600 text-sm"
  }, "A project cannot go live unless all 5 properties are approved.")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_base__WEBPACK_IMPORTED_MODULE_2__["Table"], {
    query: "/api/project",
    toggleFetch: toggleFetch,
    keyword: keyWord,
    getData: setProjects,
    content: projectsContent(),
    sort: filters.sortBy.value.replace(/asc_|desc_/g, "") || "",
    order: filters.sortBy.order || "",
    header: renderHeaders(),
    emptyComponent: dislayText()
  }))))));
};

/* harmony default export */ __webpack_exports__["default"] = (ManageProjects);

/***/ })

}]);