(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["/js/app"],{

/***/ "./resources/js/app.js":
/*!*****************************!*\
  !*** ./resources/js/app.js ***!
  \*****************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

/**
 * First we will load all of this project's JavaScript dependencies which
 * includes React and other helpers. It's a great starting point while
 * building robust, powerful web applications using React + Laravel.
 */
__webpack_require__(/*! ./bootstrap */ "./resources/js/bootstrap.js");
/**
 * Next, we will create a fresh React component instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */


__webpack_require__(/*! ./index */ "./resources/js/index.js");

/***/ }),

/***/ "./resources/js/bootstrap.js":
/*!***********************************!*\
  !*** ./resources/js/bootstrap.js ***!
  \***********************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

window._ = __webpack_require__(/*! lodash */ "./node_modules/lodash/lodash.js");
/**
 * We'll load jQuery and the Bootstrap jQuery plugin which provides support
 * for JavaScript based Bootstrap features such as modals and tabs. This
 * code may be modified to fit the specific needs of your application.
 */

try {
  window.Popper = __webpack_require__(/*! popper.js */ "./node_modules/popper.js/dist/esm/popper.js")["default"];
  window.$ = window.jQuery = __webpack_require__(/*! jquery */ "./node_modules/jquery/dist/jquery.js");

  __webpack_require__(/*! bootstrap */ "./node_modules/bootstrap/dist/js/bootstrap.js");
} catch (e) {}
/**
 * We'll load the axios HTTP library which allows us to easily issue requests
 * to our Laravel back-end. This library automatically handles sending the
 * CSRF token as a header based on the value of the "XSRF" token cookie.
 */


window.axios = __webpack_require__(/*! axios */ "./node_modules/axios/index.js");
window.axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';
/**
 * Next we will register the CSRF Token as a common header with Axios so that
 * all outgoing HTTP requests automatically have it attached. This is just
 * a simple convenience so we don't have to attach every token manually.
 */

var token = document.head.querySelector('meta[name="csrf-token"]');

if (token) {
  window.axios.defaults.headers.common['X-CSRF-TOKEN'] = token.content;
} else {
  console.error('CSRF token not found: https://laravel.com/docs/csrf#csrf-x-csrf-token');
}
/**
 * Echo exposes an expressive API for subscribing to channels and listening
 * for events that are broadcast by Laravel. Echo and event broadcasting
 * allows your team to easily build robust real-time web applications.
 */
// import Echo from 'laravel-echo';
// window.Pusher = require('pusher-js');
// window.Echo = new Echo({
//     broadcaster: 'pusher',
//     key: process.env.MIX_PUSHER_APP_KEY,
//     cluster: process.env.MIX_PUSHER_APP_CLUSTER,
//     forceTLS: true
// });

/***/ }),

/***/ "./resources/js/index.js":
/*!*******************************!*\
  !*** ./resources/js/index.js ***!
  \*******************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react_dom__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react-dom */ "./node_modules/react-dom/index.js");
/* harmony import */ var react_dom__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react_dom__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var hookrouter__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! hookrouter */ "./node_modules/hookrouter/dist/index.js");
/* harmony import */ var hookrouter__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(hookrouter__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var react_redux__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! react-redux */ "./node_modules/react-redux/es/index.js");
/* harmony import */ var _store__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./store */ "./resources/js/store.js");
/* harmony import */ var _fortawesome_fontawesome_svg_core__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @fortawesome/fontawesome-svg-core */ "./node_modules/@fortawesome/fontawesome-svg-core/index.es.js");
/* harmony import */ var _fortawesome_free_brands_svg_icons__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @fortawesome/free-brands-svg-icons */ "./node_modules/@fortawesome/free-brands-svg-icons/index.es.js");
/* harmony import */ var _fortawesome_free_solid_svg_icons__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @fortawesome/free-solid-svg-icons */ "./node_modules/@fortawesome/free-solid-svg-icons/index.es.js");
/* harmony import */ var _states_userGlobal__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./states/userGlobal */ "./resources/js/states/userGlobal.js");
/* harmony import */ var _routes__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./routes */ "./resources/js/routes.js");
function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _unsupportedIterableToArray(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

function _iterableToArrayLimit(arr, i) { if (typeof Symbol === "undefined" || !(Symbol.iterator in Object(arr))) return; var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }











_fortawesome_fontawesome_svg_core__WEBPACK_IMPORTED_MODULE_5__["library"].add(_fortawesome_free_brands_svg_icons__WEBPACK_IMPORTED_MODULE_6__["fab"], _fortawesome_free_solid_svg_icons__WEBPACK_IMPORTED_MODULE_7__["fas"]);

var App = function App() {
  var _UserGlobal = Object(_states_userGlobal__WEBPACK_IMPORTED_MODULE_8__["default"])(),
      _UserGlobal2 = _slicedToArray(_UserGlobal, 2),
      userState = _UserGlobal2[0],
      userAction = _UserGlobal2[1];

  var routeResult = Object(hookrouter__WEBPACK_IMPORTED_MODULE_2__["useRoutes"])(_routes__WEBPACK_IMPORTED_MODULE_9__["default"]);
  Object(react__WEBPACK_IMPORTED_MODULE_0__["useEffect"])(function () {
    userAction.setState({
      windowSize: window.screen.width
    });
  }, []);
  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react__WEBPACK_IMPORTED_MODULE_0___default.a.Fragment, null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_redux__WEBPACK_IMPORTED_MODULE_3__["Provider"], {
    store: _store__WEBPACK_IMPORTED_MODULE_4__["default"]
  }, routeResult));
};

react_dom__WEBPACK_IMPORTED_MODULE_1___default.a.render( /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(App, null), document.getElementById("app"));

/***/ }),

/***/ "./resources/js/pages/UnderConstruction.js":
/*!*************************************************!*\
  !*** ./resources/js/pages/UnderConstruction.js ***!
  \*************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _material_ui_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @material-ui/core */ "./node_modules/@material-ui/core/esm/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _components__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../components */ "./resources/js/components/index.js");
/* harmony import */ var _components__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(_components__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _layouts_users_UserLayout__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../layouts/users/UserLayout */ "./resources/js/layouts/users/UserLayout.js");





var UnderConstruction = function UnderConstruction() {
  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_layouts_users_UserLayout__WEBPACK_IMPORTED_MODULE_3__["default"], null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "bg-white flex flex-col items-center lg:flex-row mx-auto mx-10",
    style: {
      height: "100%"
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "flex flex-1 flex-col h-full items-center justify-center",
    style: {
      minHeight: "500px"
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("img", {
    className: "lg:mt-0 mt-32",
    src: "/assets/images/LOGO.png" // className={`object-cover`}
    ,
    style: {
      height: 120,
      width: 'auto'
    }
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("h1", {
    className: "font-bold text-3xl text-center text-gray-800 text-palette-purple"
  }, "Coming Soon"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "flex justify-center"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("img", {
    className: "lg:my-10 my-6 px-6",
    src: "/assets/svg/undraw_under_construction_46pa.svg" // className={`object-cover`}
    ,
    style: {
      height: 250
    }
  })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("p", {
    className: "font-semibold lg:mx-24 lg:mx-4 mx-8 text-base text-center text-gray-800"
  }, "GroupBuyer is currently under construction and will return very soon with more of Australia's best property deals. Join us for free and stay in the loop with our upcoming launch.")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "flex flex-1 flex-col h-full items-center justify-center w-full pt-16 px-5 sm:pt-0 w-full",
    style: {
      minHeight: "700px"
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "flex-col pt-16 px-5 sm:pt-0 w-full"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("p", {
    className: "font-semibold  px-6 text-2xl text-center text-gray-800"
  }, "Questions? Send us an enquiry"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components__WEBPACK_IMPORTED_MODULE_2__["ContactForm"], {
    backgroundColor: "#000024"
  })))));
};

/* harmony default export */ __webpack_exports__["default"] = (UnderConstruction);

/***/ }),

/***/ "./resources/js/routes.js":
/*!********************************!*\
  !*** ./resources/js/routes.js ***!
  \********************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react_loadable__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react-loadable */ "./node_modules/react-loadable/lib/index.js");
/* harmony import */ var react_loadable__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react_loadable__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _components_LoaderComponent__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./components/LoaderComponent */ "./resources/js/components/LoaderComponent.js");
/* harmony import */ var _services_auth__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./services/auth */ "./resources/js/services/auth.js");
/* harmony import */ var _pages_UnderConstruction__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./pages/UnderConstruction */ "./resources/js/pages/UnderConstruction.js");





var Home = react_loadable__WEBPACK_IMPORTED_MODULE_1___default()({
  loader: function loader() {
    return Promise.all(/*! import() | Home */[__webpack_require__.e("/js/vendor"), __webpack_require__.e("Home")]).then(__webpack_require__.bind(null, /*! ./pages/Home */ "./resources/js/pages/Home.js"));
  },
  loading: _components_LoaderComponent__WEBPACK_IMPORTED_MODULE_2__["default"]
});
var NewHome = react_loadable__WEBPACK_IMPORTED_MODULE_1___default()({
  loader: function loader() {
    return Promise.all(/*! import() | Home */[__webpack_require__.e("/js/vendor"), __webpack_require__.e("Home")]).then(__webpack_require__.bind(null, /*! ./pages/NewHome */ "./resources/js/pages/NewHome.js"));
  },
  loading: _components_LoaderComponent__WEBPACK_IMPORTED_MODULE_2__["default"]
});
var SiteUnderConstruction = react_loadable__WEBPACK_IMPORTED_MODULE_1___default()({
  loader: function loader() {
    return Promise.all(/*! import() | Home */[__webpack_require__.e("/js/vendor"), __webpack_require__.e("Home")]).then(__webpack_require__.bind(null, /*! ./pages/SiteUnderConstruction */ "./resources/js/pages/SiteUnderConstruction.js"));
  },
  loading: _components_LoaderComponent__WEBPACK_IMPORTED_MODULE_2__["default"]
});
var TermsAndConditions = react_loadable__WEBPACK_IMPORTED_MODULE_1___default()({
  loader: function loader() {
    return Promise.all(/*! import() | TermsAndConditions */[__webpack_require__.e("/js/vendor"), __webpack_require__.e("TermsAndConditions")]).then(__webpack_require__.bind(null, /*! ./pages/TermsAndConditions */ "./resources/js/pages/TermsAndConditions.js"));
  },
  loading: _components_LoaderComponent__WEBPACK_IMPORTED_MODULE_2__["default"]
});
var PrivacyPolicy = react_loadable__WEBPACK_IMPORTED_MODULE_1___default()({
  loader: function loader() {
    return Promise.all(/*! import() | PrivacyPolicy */[__webpack_require__.e("/js/vendor"), __webpack_require__.e("PrivacyPolicy")]).then(__webpack_require__.bind(null, /*! ./pages/PrivacyPolicy */ "./resources/js/pages/PrivacyPolicy.js"));
  },
  loading: _components_LoaderComponent__WEBPACK_IMPORTED_MODULE_2__["default"]
});
var AboutPage = react_loadable__WEBPACK_IMPORTED_MODULE_1___default()({
  loader: function loader() {
    return Promise.all(/*! import() | AboutPage */[__webpack_require__.e("/js/vendor"), __webpack_require__.e("AboutPage")]).then(__webpack_require__.bind(null, /*! ./pages/AboutPage */ "./resources/js/pages/AboutPage.js"));
  },
  loading: _components_LoaderComponent__WEBPACK_IMPORTED_MODULE_2__["default"]
});
var ContactPage = react_loadable__WEBPACK_IMPORTED_MODULE_1___default()({
  loader: function loader() {
    return Promise.all(/*! import() | ContactPage */[__webpack_require__.e("/js/vendor"), __webpack_require__.e("ContactPage")]).then(__webpack_require__.bind(null, /*! ./pages/ContactPage */ "./resources/js/pages/ContactPage.js"));
  },
  loading: _components_LoaderComponent__WEBPACK_IMPORTED_MODULE_2__["default"]
});
var SellingPage = react_loadable__WEBPACK_IMPORTED_MODULE_1___default()({
  loader: function loader() {
    return Promise.all(/*! import() | SellingPage */[__webpack_require__.e("/js/vendor"), __webpack_require__.e("SellingPage")]).then(__webpack_require__.bind(null, /*! ./pages/SellingPage */ "./resources/js/pages/SellingPage.js"));
  },
  loading: _components_LoaderComponent__WEBPACK_IMPORTED_MODULE_2__["default"]
});
var HowItWorksPage = react_loadable__WEBPACK_IMPORTED_MODULE_1___default()({
  loader: function loader() {
    return Promise.all(/*! import() | HowItWorksPage */[__webpack_require__.e("/js/vendor"), __webpack_require__.e("HowItWorksPage")]).then(__webpack_require__.bind(null, /*! ./pages/HowItWorksPage */ "./resources/js/pages/HowItWorksPage.js"));
  },
  loading: _components_LoaderComponent__WEBPACK_IMPORTED_MODULE_2__["default"]
});
var BuyingListPage = react_loadable__WEBPACK_IMPORTED_MODULE_1___default()({
  loader: function loader() {
    return Promise.all(/*! import() | BuyingListPage */[__webpack_require__.e("/js/vendor"), __webpack_require__.e("BuyingListPage")]).then(__webpack_require__.bind(null, /*! ./pages/BuyingListPage */ "./resources/js/pages/BuyingListPage.js"));
  },
  loading: _components_LoaderComponent__WEBPACK_IMPORTED_MODULE_2__["default"]
}); // const BuyingProjectDetailsPage = Loadable({
//   loader: () =>
//     import(
//       /* webpackChunkName: "BuyingProjectDetailsPage" */ "./pages/BuyingProjectDetailsPage"
//     ),
//   loading: LoaderComponent
// });

var BuyingProductPage = react_loadable__WEBPACK_IMPORTED_MODULE_1___default()({
  loader: function loader() {
    return Promise.all(/*! import() | BuyingProductPage */[__webpack_require__.e("/js/vendor"), __webpack_require__.e("BuyingProductPage")]).then(__webpack_require__.bind(null, /*! ./pages/BuyingProductPage */ "./resources/js/pages/BuyingProductPage.js"));
  },
  loading: _components_LoaderComponent__WEBPACK_IMPORTED_MODULE_2__["default"]
});
var BuyingApartmentDetailsPage = react_loadable__WEBPACK_IMPORTED_MODULE_1___default()({
  loader: function loader() {
    return Promise.all(/*! import() | BuyingApartmentDetailsPage */[__webpack_require__.e("/js/vendor"), __webpack_require__.e("BuyingApartmentDetailsPage")]).then(__webpack_require__.bind(null, /*! ./pages/BuyingApartmentDetailsPage */ "./resources/js/pages/BuyingApartmentDetailsPage.js"));
  },
  loading: _components_LoaderComponent__WEBPACK_IMPORTED_MODULE_2__["default"]
});
var BuyingConfirmationPage = react_loadable__WEBPACK_IMPORTED_MODULE_1___default()({
  loader: function loader() {
    return __webpack_require__.e(/*! import() | BuyingConfirmationPage */ "BuyingConfirmationPage").then(__webpack_require__.bind(null, /*! ./pages/BuyingConfirmationPage */ "./resources/js/pages/BuyingConfirmationPage.js"));
  },
  loading: _components_LoaderComponent__WEBPACK_IMPORTED_MODULE_2__["default"]
});
var SecuredDealsListPage = react_loadable__WEBPACK_IMPORTED_MODULE_1___default()({
  loader: function loader() {
    return __webpack_require__.e(/*! import() | SecuredDealsListPage */ "/js/vendor").then(__webpack_require__.bind(null, /*! ./pages/SecuredDealsListPage */ "./resources/js/pages/SecuredDealsListPage.js"));
  },
  loading: _components_LoaderComponent__WEBPACK_IMPORTED_MODULE_2__["default"]
});
var UserAccountSettings = react_loadable__WEBPACK_IMPORTED_MODULE_1___default()({
  loader: function loader() {
    return Promise.all(/*! import() | UserAccountSettings */[__webpack_require__.e("/js/vendor"), __webpack_require__.e("UserAccountSettings")]).then(__webpack_require__.bind(null, /*! ./pages/UserAccountSettings */ "./resources/js/pages/UserAccountSettings.js"));
  },
  loading: _components_LoaderComponent__WEBPACK_IMPORTED_MODULE_2__["default"]
});
var UserWishListPage = react_loadable__WEBPACK_IMPORTED_MODULE_1___default()({
  loader: function loader() {
    return Promise.all(/*! import() | UserWishListPage */[__webpack_require__.e("/js/vendor"), __webpack_require__.e("UserWishListPage")]).then(__webpack_require__.bind(null, /*! ./pages/UserWishListPage */ "./resources/js/pages/UserWishListPage.js"));
  },
  loading: _components_LoaderComponent__WEBPACK_IMPORTED_MODULE_2__["default"]
});
var FrequentlyAskedQuestions = react_loadable__WEBPACK_IMPORTED_MODULE_1___default()({
  loader: function loader() {
    return Promise.all(/*! import() | FrequentlyAskedQuestions */[__webpack_require__.e("/js/vendor"), __webpack_require__.e("FrequentlyAskedQuestions")]).then(__webpack_require__.bind(null, /*! ./pages/FrequentlyAskedQuestions */ "./resources/js/pages/FrequentlyAskedQuestions.js"));
  },
  loading: _components_LoaderComponent__WEBPACK_IMPORTED_MODULE_2__["default"]
});
var ManageProjectsPage = react_loadable__WEBPACK_IMPORTED_MODULE_1___default()({
  loader: function loader() {
    return Promise.all(/*! import() | ManageProjectsPage */[__webpack_require__.e("/js/vendor"), __webpack_require__.e("ManageProjectsPage")]).then(__webpack_require__.bind(null, /*! ./pages/ManageProjectsPage */ "./resources/js/pages/ManageProjectsPage.js"));
  },
  loading: _components_LoaderComponent__WEBPACK_IMPORTED_MODULE_2__["default"]
});
var ManagePropertiesPage = react_loadable__WEBPACK_IMPORTED_MODULE_1___default()({
  loader: function loader() {
    return Promise.all(/*! import() | ManagePropertiesPage */[__webpack_require__.e("/js/vendor"), __webpack_require__.e("ManagePropertiesPage")]).then(__webpack_require__.bind(null, /*! ./pages/ManagePropertiesPage */ "./resources/js/pages/ManagePropertiesPage.js"));
  },
  loading: _components_LoaderComponent__WEBPACK_IMPORTED_MODULE_2__["default"]
});
var CarsListPage = react_loadable__WEBPACK_IMPORTED_MODULE_1___default()({
  loader: function loader() {
    return Promise.all(/*! import() | CarsListPage */[__webpack_require__.e("/js/vendor"), __webpack_require__.e("CarsListPage")]).then(__webpack_require__.bind(null, /*! ./pages/CarsListPage */ "./resources/js/pages/CarsListPage.js"));
  },
  loading: _components_LoaderComponent__WEBPACK_IMPORTED_MODULE_2__["default"]
});
var ContactThankYouPage = react_loadable__WEBPACK_IMPORTED_MODULE_1___default()({
  loader: function loader() {
    return Promise.all(/*! import() | ContactThankYouPage */[__webpack_require__.e("/js/vendor"), __webpack_require__.e("ContactThankYouPage")]).then(__webpack_require__.bind(null, /*! ./pages/ContactThankYouPage */ "./resources/js/pages/ContactThankYouPage.js"));
  },
  loading: _components_LoaderComponent__WEBPACK_IMPORTED_MODULE_2__["default"]
});
var ForgotPassword = react_loadable__WEBPACK_IMPORTED_MODULE_1___default()({
  loader: function loader() {
    return Promise.all(/*! import() | ForgotPassword */[__webpack_require__.e("/js/vendor"), __webpack_require__.e("ForgotPassword")]).then(__webpack_require__.bind(null, /*! ./pages/ForgotPassword */ "./resources/js/pages/ForgotPassword.js"));
  },
  loading: _components_LoaderComponent__WEBPACK_IMPORTED_MODULE_2__["default"]
});
var ResetPassword = react_loadable__WEBPACK_IMPORTED_MODULE_1___default()({
  loader: function loader() {
    return Promise.all(/*! import() | ResetPassword */[__webpack_require__.e("/js/vendor"), __webpack_require__.e("ResetPassword")]).then(__webpack_require__.bind(null, /*! ./pages/ResetPassword */ "./resources/js/pages/ResetPassword.js"));
  },
  loading: _components_LoaderComponent__WEBPACK_IMPORTED_MODULE_2__["default"]
});
var UserLayout = react_loadable__WEBPACK_IMPORTED_MODULE_1___default()({
  loader: function loader() {
    return Object(_services_auth__WEBPACK_IMPORTED_MODULE_3__["isLoggedIn"])() ? Promise.resolve(/*! import() */).then(__webpack_require__.bind(null, /*! ./layouts/users/UserLayout */ "./resources/js/layouts/users/UserLayout.js")) : window.location.href = "/";
  },
  loading: _components_LoaderComponent__WEBPACK_IMPORTED_MODULE_2__["default"]
});
var Developers = react_loadable__WEBPACK_IMPORTED_MODULE_1___default()({
  loader: function loader() {
    return Promise.all(/*! import() | DeveloperPage */[__webpack_require__.e("/js/vendor"), __webpack_require__.e("DeveloperPage")]).then(__webpack_require__.bind(null, /*! ./pages/DeveloperPage */ "./resources/js/pages/DeveloperPage.js"));
  },
  loading: _components_LoaderComponent__WEBPACK_IMPORTED_MODULE_2__["default"]
});
var NewBuyingApartmentDetailsPage = react_loadable__WEBPACK_IMPORTED_MODULE_1___default()({
  loader: function loader() {
    return Promise.all(/*! import() | DeveloperPage */[__webpack_require__.e("/js/vendor"), __webpack_require__.e("DeveloperPage")]).then(__webpack_require__.bind(null, /*! ./pages/NewBuyingApartmentDetailsPage */ "./resources/js/pages/NewBuyingApartmentDetailsPage.js"));
  },
  loading: _components_LoaderComponent__WEBPACK_IMPORTED_MODULE_2__["default"]
});
var routes = {
  "/": function _() {
    return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(SiteUnderConstruction, null);
  } // "/under-construction": () => <SiteUnderConstruction />,
  // "/how-it-works": () => <HowItWorksPage />,
  // "/about": () => <AboutPage />,
  // "/developers": () => <Developers />,
  // "/weekly-deals": () => <BuyingListPage />,
  // "/cars-inventory": () => <CarsListPage />,
  // "/weekly-deals/:dealId": ({ dealId }) => (
  //   <BuyingProductPage dealId={parseInt(dealId)}/>
  // ),
  // "/weekly-deals/:dealId/apartment/:propertyId": ({ propertyId, dealId }) => 
  // <NewBuyingApartmentDetailsPage  propertyId={parseInt(propertyId)} dealId={parseInt(dealId)}/>,
  // "/weekly-deals/:dealId/apartment/:propertyId/": ({
  //   dealId,
  //   propertyId
  // }) => (
  //   <BuyingApartmentDetailsPage
  //     dealId={parseInt(dealId)}
  //     propertyId={parseInt(propertyId)}
  //   />
  // ),
  // "/weekly-deals/:dealId/apartment/:propertyId/confirm": ({
  //   dealId,
  //   propertyId
  // }) => (
  //   <BuyingConfirmationPage
  //     dealId={parseInt(dealId)}
  //     propertyId={parseInt(propertyId)}
  //   />
  // ),
  // "/contact": () => <ContactPage />,
  // "/selling": () => <SellingPage />,
  // "/terms-and-conditions": () => <TermsAndConditions />,
  // "/frequently-asked-questions": () => <FrequentlyAskedQuestions />,
  // "/contact-thank-you": () => <ContactThankYouPage />,
  // "/privacy-policy": () => <PrivacyPolicy />,
  // "/profile/account-settings": () => (
  //   <UserLayout>
  //     <UserAccountSettings />
  //   </UserLayout>
  // ),
  // "/profile/secured-deals": () => <SecuredDealsListPage isAdmin={false} />,
  // "/profile/wish-list": () => <UserWishListPage />,
  // "/profile/manage-projects": () => <ManageProjectsPage />,
  // "/profile/manage-properties/:dealId/:dealSubPropertyId": ({
  //   dealId,
  //   dealSubPropertyId
  // }) => (
  //   <ManagePropertiesPage
  //     dealId={parseInt(dealId)}
  //     dealSubPropertyId={parseInt(dealSubPropertyId)}
  //   />
  // ),
  // "/request-password-reset": () => <ForgotPassword />,
  // "/reset-password": () => <ResetPassword />,

};
/* harmony default export */ __webpack_exports__["default"] = (routes);

/***/ }),

/***/ 1:
/*!***********************************!*\
  !*** multi ./resources/js/app.js ***!
  \***********************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! C:\Users\Fligno-Laptop-099\Documents\projects\group-buyer-web\resources\js\app.js */"./resources/js/app.js");


/***/ })

},[[1,"/js/manifest","/js/vendor"]]]);