(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["/js/admin/app"],{

/***/ "./resources/js/admin/app.js":
/*!***********************************!*\
  !*** ./resources/js/admin/app.js ***!
  \***********************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return App; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react_dom__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react-dom */ "./node_modules/react-dom/index.js");
/* harmony import */ var react_dom__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react_dom__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var react_router_dom__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! react-router-dom */ "./node_modules/react-router-dom/esm/react-router-dom.js");
/* harmony import */ var react_redux__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! react-redux */ "./node_modules/react-redux/es/index.js");
/* harmony import */ var _store__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../store */ "./resources/js/store.js");
/* harmony import */ var _components_Content__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./components/Content */ "./resources/js/admin/components/Content.js");
/* harmony import */ var clsx__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! clsx */ "./node_modules/clsx/dist/clsx.m.js");
/* harmony import */ var _material_ui_core_styles__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @material-ui/core/styles */ "./node_modules/@material-ui/core/esm/styles/index.js");
/* harmony import */ var _material_ui_core_Drawer__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @material-ui/core/Drawer */ "./node_modules/@material-ui/core/esm/Drawer/index.js");
/* harmony import */ var _material_ui_core_CssBaseline__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @material-ui/core/CssBaseline */ "./node_modules/@material-ui/core/esm/CssBaseline/index.js");
/* harmony import */ var _material_ui_core_AppBar__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @material-ui/core/AppBar */ "./node_modules/@material-ui/core/esm/AppBar/index.js");
/* harmony import */ var _material_ui_core_Toolbar__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! @material-ui/core/Toolbar */ "./node_modules/@material-ui/core/esm/Toolbar/index.js");
/* harmony import */ var _material_ui_core_List__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! @material-ui/core/List */ "./node_modules/@material-ui/core/esm/List/index.js");
/* harmony import */ var _material_ui_core_Typography__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! @material-ui/core/Typography */ "./node_modules/@material-ui/core/esm/Typography/index.js");
/* harmony import */ var _material_ui_core_Divider__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! @material-ui/core/Divider */ "./node_modules/@material-ui/core/esm/Divider/index.js");
/* harmony import */ var _material_ui_core_IconButton__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! @material-ui/core/IconButton */ "./node_modules/@material-ui/core/esm/IconButton/index.js");
/* harmony import */ var _material_ui_icons_Menu__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! @material-ui/icons/Menu */ "./node_modules/@material-ui/icons/Menu.js");
/* harmony import */ var _material_ui_icons_Menu__WEBPACK_IMPORTED_MODULE_16___default = /*#__PURE__*/__webpack_require__.n(_material_ui_icons_Menu__WEBPACK_IMPORTED_MODULE_16__);
/* harmony import */ var _material_ui_icons_ChevronLeft__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! @material-ui/icons/ChevronLeft */ "./node_modules/@material-ui/icons/ChevronLeft.js");
/* harmony import */ var _material_ui_icons_ChevronLeft__WEBPACK_IMPORTED_MODULE_17___default = /*#__PURE__*/__webpack_require__.n(_material_ui_icons_ChevronLeft__WEBPACK_IMPORTED_MODULE_17__);
/* harmony import */ var _material_ui_icons_ChevronRight__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(/*! @material-ui/icons/ChevronRight */ "./node_modules/@material-ui/icons/ChevronRight.js");
/* harmony import */ var _material_ui_icons_ChevronRight__WEBPACK_IMPORTED_MODULE_18___default = /*#__PURE__*/__webpack_require__.n(_material_ui_icons_ChevronRight__WEBPACK_IMPORTED_MODULE_18__);
/* harmony import */ var _material_ui_core_ListItem__WEBPACK_IMPORTED_MODULE_19__ = __webpack_require__(/*! @material-ui/core/ListItem */ "./node_modules/@material-ui/core/esm/ListItem/index.js");
/* harmony import */ var _material_ui_core_ListItemIcon__WEBPACK_IMPORTED_MODULE_20__ = __webpack_require__(/*! @material-ui/core/ListItemIcon */ "./node_modules/@material-ui/core/esm/ListItemIcon/index.js");
/* harmony import */ var _material_ui_core_ListItemText__WEBPACK_IMPORTED_MODULE_21__ = __webpack_require__(/*! @material-ui/core/ListItemText */ "./node_modules/@material-ui/core/esm/ListItemText/index.js");
/* harmony import */ var _material_ui_icons_HomeWork__WEBPACK_IMPORTED_MODULE_22__ = __webpack_require__(/*! @material-ui/icons/HomeWork */ "./node_modules/@material-ui/icons/HomeWork.js");
/* harmony import */ var _material_ui_icons_HomeWork__WEBPACK_IMPORTED_MODULE_22___default = /*#__PURE__*/__webpack_require__.n(_material_ui_icons_HomeWork__WEBPACK_IMPORTED_MODULE_22__);
/* harmony import */ var _material_ui_icons_MonetizationOn__WEBPACK_IMPORTED_MODULE_23__ = __webpack_require__(/*! @material-ui/icons/MonetizationOn */ "./node_modules/@material-ui/icons/MonetizationOn.js");
/* harmony import */ var _material_ui_icons_MonetizationOn__WEBPACK_IMPORTED_MODULE_23___default = /*#__PURE__*/__webpack_require__.n(_material_ui_icons_MonetizationOn__WEBPACK_IMPORTED_MODULE_23__);
/* harmony import */ var _material_ui_icons_House__WEBPACK_IMPORTED_MODULE_24__ = __webpack_require__(/*! @material-ui/icons/House */ "./node_modules/@material-ui/icons/House.js");
/* harmony import */ var _material_ui_icons_House__WEBPACK_IMPORTED_MODULE_24___default = /*#__PURE__*/__webpack_require__.n(_material_ui_icons_House__WEBPACK_IMPORTED_MODULE_24__);
/* harmony import */ var _material_ui_icons_PermContactCalendar__WEBPACK_IMPORTED_MODULE_25__ = __webpack_require__(/*! @material-ui/icons/PermContactCalendar */ "./node_modules/@material-ui/icons/PermContactCalendar.js");
/* harmony import */ var _material_ui_icons_PermContactCalendar__WEBPACK_IMPORTED_MODULE_25___default = /*#__PURE__*/__webpack_require__.n(_material_ui_icons_PermContactCalendar__WEBPACK_IMPORTED_MODULE_25__);
/* harmony import */ var _fortawesome_fontawesome_svg_core__WEBPACK_IMPORTED_MODULE_26__ = __webpack_require__(/*! @fortawesome/fontawesome-svg-core */ "./node_modules/@fortawesome/fontawesome-svg-core/index.es.js");
/* harmony import */ var _fortawesome_free_brands_svg_icons__WEBPACK_IMPORTED_MODULE_27__ = __webpack_require__(/*! @fortawesome/free-brands-svg-icons */ "./node_modules/@fortawesome/free-brands-svg-icons/index.es.js");
/* harmony import */ var _fortawesome_free_solid_svg_icons__WEBPACK_IMPORTED_MODULE_28__ = __webpack_require__(/*! @fortawesome/free-solid-svg-icons */ "./node_modules/@fortawesome/free-solid-svg-icons/index.es.js");
/* harmony import */ var _states_userGlobal__WEBPACK_IMPORTED_MODULE_29__ = __webpack_require__(/*! ../states/userGlobal */ "./resources/js/states/userGlobal.js");
/* harmony import */ var _services_auth__WEBPACK_IMPORTED_MODULE_30__ = __webpack_require__(/*! ../services/auth */ "./resources/js/services/auth.js");
function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _unsupportedIterableToArray(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

function _iterableToArrayLimit(arr, i) { if (typeof Symbol === "undefined" || !(Symbol.iterator in Object(arr))) return; var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }






 // const App = () => {
//   return (
//     <Router basename="/admin">
//       <div className="h-screen flex">
//         <Sidebar />
//         <Content />
//       </div>
//     </Router>
//   );
// };
// if (document.getElementById("app")) {
//   ReactDOM.render(
//     <Provider store={store}>
//       <App />
//     </Provider>,
//     document.getElementById("app")
//   );
// }
// import React from 'react';


























_fortawesome_fontawesome_svg_core__WEBPACK_IMPORTED_MODULE_26__["library"].add(_fortawesome_free_brands_svg_icons__WEBPACK_IMPORTED_MODULE_27__["fab"], _fortawesome_free_solid_svg_icons__WEBPACK_IMPORTED_MODULE_28__["fas"]);
var drawerWidth = 240;
var useStyles = Object(_material_ui_core_styles__WEBPACK_IMPORTED_MODULE_7__["makeStyles"])(function (theme) {
  return {
    root: {
      display: "flex"
    },
    appBar: {
      transition: theme.transitions.create(["margin", "width"], {
        easing: theme.transitions.easing.sharp,
        duration: theme.transitions.duration.leavingScreen
      })
    },
    appBarShift: {
      width: "calc(100% - ".concat(drawerWidth, "px)"),
      marginLeft: drawerWidth,
      transition: theme.transitions.create(["margin", "width"], {
        easing: theme.transitions.easing.easeOut,
        duration: theme.transitions.duration.enteringScreen
      })
    },
    menuButton: {
      marginRight: theme.spacing(2)
    },
    hide: {
      display: "none"
    },
    drawer: {
      width: drawerWidth,
      flexShrink: 0
    },
    drawerPaper: {
      width: drawerWidth
    },
    drawerHeader: _objectSpread(_objectSpread({
      display: "flex",
      alignrouteItems: "center",
      padding: theme.spacing(0, 1)
    }, theme.mixins.toolbar), {}, {
      justifyContent: "flex-end",
      backgroundColor: "inherit"
    }),
    content: {
      flexGrow: 1,
      padding: theme.spacing(3),
      transition: theme.transitions.create("margin", {
        easing: theme.transitions.easing.sharp,
        duration: theme.transitions.duration.leavingScreen
      }),
      marginLeft: -drawerWidth
    },
    contentShift: {
      transition: theme.transitions.create("margin", {
        easing: theme.transitions.easing.easeOut,
        duration: theme.transitions.duration.enteringScreen
      }),
      marginLeft: 0
    }
  };
});
function App() {
  var classes = useStyles();
  var theme = Object(_material_ui_core_styles__WEBPACK_IMPORTED_MODULE_7__["useTheme"])();

  var _React$useState = react__WEBPACK_IMPORTED_MODULE_0___default.a.useState(true),
      _React$useState2 = _slicedToArray(_React$useState, 2),
      open = _React$useState2[0],
      setOpen = _React$useState2[1];

  var _UserGlobal = Object(_states_userGlobal__WEBPACK_IMPORTED_MODULE_29__["default"])(),
      _UserGlobal2 = _slicedToArray(_UserGlobal, 2),
      userState = _UserGlobal2[0],
      userAction = _UserGlobal2[1];

  var handleDrawerOpen = function handleDrawerOpen() {
    setOpen(true);
    userAction.setState({
      adminDrawerOpen: true
    });
  };

  var handleDrawerClose = function handleDrawerClose() {
    setOpen(false);
    userAction.setState({
      adminDrawerOpen: false
    });
  };

  var routeItems = [{
    label: "Projects",
    route: "/projects",
    icon: /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_material_ui_icons_HomeWork__WEBPACK_IMPORTED_MODULE_22___default.a, null)
  }, {
    label: "Properties",
    route: "/properties",
    icon: /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_material_ui_icons_House__WEBPACK_IMPORTED_MODULE_24___default.a, null)
  }, {
    label: "Secured Deals",
    route: "/secured-deals",
    icon: /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_material_ui_icons_MonetizationOn__WEBPACK_IMPORTED_MODULE_23___default.a, null)
  }, {
    label: "Developers",
    route: "/developers",
    icon: /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_material_ui_icons_PermContactCalendar__WEBPACK_IMPORTED_MODULE_25___default.a, null)
  }];
  return Object(_services_auth__WEBPACK_IMPORTED_MODULE_30__["isLoggedIn"])() && Object(_services_auth__WEBPACK_IMPORTED_MODULE_30__["userRole"])() === "admin" ? /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_router_dom__WEBPACK_IMPORTED_MODULE_2__["BrowserRouter"], {
    basename: "/admin"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: classes.root
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_material_ui_core_CssBaseline__WEBPACK_IMPORTED_MODULE_9__["default"], null), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_material_ui_core_AppBar__WEBPACK_IMPORTED_MODULE_10__["default"], {
    position: "fixed",
    className: Object(clsx__WEBPACK_IMPORTED_MODULE_6__["default"])(classes.appBar, _defineProperty({}, classes.appBarShift, open))
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_material_ui_core_Toolbar__WEBPACK_IMPORTED_MODULE_11__["default"], null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_material_ui_core_IconButton__WEBPACK_IMPORTED_MODULE_15__["default"], {
    color: "inherit",
    "aria-label": "open drawer",
    onClick: handleDrawerOpen,
    edge: "start",
    className: Object(clsx__WEBPACK_IMPORTED_MODULE_6__["default"])(classes.menuButton, open && classes.hide)
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_material_ui_icons_Menu__WEBPACK_IMPORTED_MODULE_16___default.a, null)), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_material_ui_core_Typography__WEBPACK_IMPORTED_MODULE_13__["default"], {
    variant: "h6",
    noWrap: true
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("a", {
    href: "/"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
    src: "/assets/svg/upload_logo_min_negative.svg",
    style: {
      height: 70
    }
  }))))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_material_ui_core_Drawer__WEBPACK_IMPORTED_MODULE_8__["default"], {
    className: classes.drawer,
    variant: "persistent",
    anchor: "left",
    open: open,
    classes: {
      paper: classes.drawerPaper
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: classes.drawerHeader
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_material_ui_core_IconButton__WEBPACK_IMPORTED_MODULE_15__["default"], {
    onClick: handleDrawerClose
  }, theme.direction === "ltr" ? /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_material_ui_icons_ChevronLeft__WEBPACK_IMPORTED_MODULE_17___default.a, null) : /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_material_ui_icons_ChevronRight__WEBPACK_IMPORTED_MODULE_18___default.a, null))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_material_ui_core_Divider__WEBPACK_IMPORTED_MODULE_14__["default"], null), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_material_ui_core_List__WEBPACK_IMPORTED_MODULE_12__["default"], null, routeItems.map(function (item, index) {
    return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_router_dom__WEBPACK_IMPORTED_MODULE_2__["Link"], {
      to: "".concat(item.route),
      key: item.label
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_material_ui_core_ListItem__WEBPACK_IMPORTED_MODULE_19__["default"], {
      button: true
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_material_ui_core_ListItemIcon__WEBPACK_IMPORTED_MODULE_20__["default"], null, item.icon), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_material_ui_core_ListItemText__WEBPACK_IMPORTED_MODULE_21__["default"], {
      primary: item.label
    })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_material_ui_core_Divider__WEBPACK_IMPORTED_MODULE_14__["default"], null));
  }))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: Object(clsx__WEBPACK_IMPORTED_MODULE_6__["default"])(classes.content, _defineProperty({}, classes.contentShift, open))
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_Content__WEBPACK_IMPORTED_MODULE_5__["default"], null)))) : window.location.href = "/";
}

if (document.getElementById("app")) {
  react_dom__WEBPACK_IMPORTED_MODULE_1___default.a.render( /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_redux__WEBPACK_IMPORTED_MODULE_3__["Provider"], {
    store: _store__WEBPACK_IMPORTED_MODULE_4__["default"]
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(App, null)), document.getElementById("app"));
}

/***/ }),

/***/ "./resources/js/admin/components/AddDeveloperModal.js":
/*!************************************************************!*\
  !*** ./resources/js/admin/components/AddDeveloperModal.js ***!
  \************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime/regenerator */ "./node_modules/@babel/runtime/regenerator/index.js");
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _helpers_styles_dropdown_css__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../helpers/styles/dropdown.css */ "./resources/js/helpers/styles/dropdown.css");
/* harmony import */ var _helpers_styles_dropdown_css__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(_helpers_styles_dropdown_css__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _components_base__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../components/_base */ "./resources/js/components/_base/index.js");
/* harmony import */ var _components_base__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(_components_base__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var _helpers_styles_toast_css__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../helpers/styles/toast.css */ "./resources/js/helpers/styles/toast.css");
/* harmony import */ var _helpers_styles_toast_css__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(_helpers_styles_toast_css__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! lodash */ "./node_modules/lodash/lodash.js");
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(lodash__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var _states_userGlobal__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../states/userGlobal */ "./resources/js/states/userGlobal.js");
/* harmony import */ var _fortawesome_react_fontawesome__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @fortawesome/react-fontawesome */ "./node_modules/@fortawesome/react-fontawesome/index.es.js");
/* harmony import */ var _fortawesome_free_solid_svg_icons__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @fortawesome/free-solid-svg-icons */ "./node_modules/@fortawesome/free-solid-svg-icons/index.es.js");
/* harmony import */ var _material_ui_core_Collapse__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @material-ui/core/Collapse */ "./node_modules/@material-ui/core/esm/Collapse/index.js");
/* harmony import */ var _components_base_alerts_sweetAlert__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ../../components/_base/alerts/sweetAlert */ "./resources/js/components/_base/alerts/sweetAlert.js");
/* harmony import */ var react_select__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! react-select */ "./node_modules/react-select/dist/react-select.browser.esm.js");
/* harmony import */ var _helpers_userAccountSettingsHelper_userAccountHelper__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ../../helpers/userAccountSettingsHelper/userAccountHelper */ "./resources/js/helpers/userAccountSettingsHelper/userAccountHelper.js");
/* harmony import */ var _helpers_countries__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ../../helpers/countries */ "./resources/js/helpers/countries.js");
/* harmony import */ var _helpers_propertyHelper_propertyHelper__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ../../helpers/propertyHelper/propertyHelper */ "./resources/js/helpers/propertyHelper/propertyHelper.js");
/* harmony import */ var _data_index__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! ../../data/index */ "./resources/js/data/index.js");


function _toConsumableArray(arr) { return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _unsupportedIterableToArray(arr) || _nonIterableSpread(); }

function _nonIterableSpread() { throw new TypeError("Invalid attempt to spread non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _iterableToArray(iter) { if (typeof Symbol !== "undefined" && Symbol.iterator in Object(iter)) return Array.from(iter); }

function _arrayWithoutHoles(arr) { if (Array.isArray(arr)) return _arrayLikeToArray(arr); }

function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _unsupportedIterableToArray(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

function _iterableToArrayLimit(arr, i) { if (typeof Symbol === "undefined" || !(Symbol.iterator in Object(arr))) return; var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }


















var AddDeveloperModal = function AddDeveloperModal(_ref) {
  var toogleAddDeveloper = _ref.toogleAddDeveloper,
      fetchDevelopers = _ref.fetchDevelopers;

  var _UserGlobal = Object(_states_userGlobal__WEBPACK_IMPORTED_MODULE_6__["default"])(),
      _UserGlobal2 = _slicedToArray(_UserGlobal, 2),
      userState = _UserGlobal2[0],
      userAction = _UserGlobal2[1];

  var _useState = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(false),
      _useState2 = _slicedToArray(_useState, 2),
      loading = _useState2[0],
      setLoading = _useState2[1];

  var _useState3 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])({}),
      _useState4 = _slicedToArray(_useState3, 2),
      errors = _useState4[0],
      setErrors = _useState4[1];

  var _useState5 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(true),
      _useState6 = _slicedToArray(_useState5, 2),
      isOpen = _useState6[0],
      setIsOpen = _useState6[1];

  var _useState7 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])([]),
      _useState8 = _slicedToArray(_useState7, 2),
      selectedState = _useState8[0],
      setSelectedState = _useState8[1];

  var _useState9 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])([]),
      _useState10 = _slicedToArray(_useState9, 2),
      selectedCountry = _useState10[0],
      setSelectedCountry = _useState10[1];

  var _useState11 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])("/assets/images/profile_default.jpg"),
      _useState12 = _slicedToArray(_useState11, 2),
      profilePic = _useState12[0],
      setProfilePic = _useState12[1];

  var _useState13 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])("/assets/images/no_logo.png"),
      _useState14 = _slicedToArray(_useState13, 2),
      companyPic = _useState14[0],
      setCompanyPic = _useState14[1];

  var form = /*#__PURE__*/Object(react__WEBPACK_IMPORTED_MODULE_1__["createRef"])();
  var profileInput = Object(react__WEBPACK_IMPORTED_MODULE_1__["useRef"])(null);
  var companyInput = Object(react__WEBPACK_IMPORTED_MODULE_1__["useRef"])(null);

  var _useState15 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(""),
      _useState16 = _slicedToArray(_useState15, 2),
      selectedBank = _useState16[0],
      setSelectedBank = _useState16[1];

  var _useState17 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])([]),
      _useState18 = _slicedToArray(_useState17, 2),
      bankList = _useState18[0],
      setBankList = _useState18[1];

  var _useState19 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(false),
      _useState20 = _slicedToArray(_useState19, 2),
      showOtherBankText = _useState20[0],
      setShowOtherBankText = _useState20[1];

  Object.keys(_helpers_userAccountSettingsHelper_userAccountHelper__WEBPACK_IMPORTED_MODULE_12__["fields"]).forEach(function (key) {
    _helpers_userAccountSettingsHelper_userAccountHelper__WEBPACK_IMPORTED_MODULE_12__["fields"][key].value = "";
  });
  Object.keys(_helpers_userAccountSettingsHelper_userAccountHelper__WEBPACK_IMPORTED_MODULE_12__["developerFields"]).forEach(function (key) {
    _helpers_userAccountSettingsHelper_userAccountHelper__WEBPACK_IMPORTED_MODULE_12__["developerFields"][key].value = "";
  });
  Object(react__WEBPACK_IMPORTED_MODULE_1__["useEffect"])(function () {
    getBanks();
  }, []);

  var getBanks = /*#__PURE__*/function () {
    var _ref2 = _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee() {
      var url;
      return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee$(_context) {
        while (1) {
          switch (_context.prev = _context.next) {
            case 0:
              url = "/api/bank";
              _context.next = 3;
              return Object(_data_index__WEBPACK_IMPORTED_MODULE_15__["fetchBanks"])({
                url: url
              }).then(function (res) {
                var newBankList = _toConsumableArray(res.data.map(function (bank) {
                  return {
                    label: "".concat(bank.name),
                    value: String(bank.id)
                  };
                }));

                newBankList.push({
                  label: "Other",
                  value: "Other"
                });
                setBankList(newBankList);
              });

            case 3:
            case "end":
              return _context.stop();
          }
        }
      }, _callee);
    }));

    return function getBanks() {
      return _ref2.apply(this, arguments);
    };
  }();

  var handleClick = function handleClick(isProfile) {
    if (isProfile) {
      profileInput.current.click();
    } else {
      companyInput.current.click();
    }
  };

  var handleFileChange = function handleFileChange(e, isProfile) {
    var imageFile = e.target.files[0];

    if (!imageFile.name.match(/\.(jpg|jpeg|png|gif)$/)) {
      return Object(_components_base_alerts_sweetAlert__WEBPACK_IMPORTED_MODULE_10__["sweetAlert"])("error", "Please select valid image.");
    }

    if (isProfile) {
      setProfilePic(URL.createObjectURL(imageFile));
    } else {
      setCompanyPic(URL.createObjectURL(imageFile));
    }
  };

  var handleSubmit = /*#__PURE__*/function () {
    var _ref3 = _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee2(e) {
      var formData, phone, url;
      return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee2$(_context2) {
        while (1) {
          switch (_context2.prev = _context2.next) {
            case 0:
              e.preventDefault();

              if (!(profilePic.substring(0, 8) === "/assets/")) {
                _context2.next = 3;
                break;
              }

              return _context2.abrupt("return", Object(_components_base_alerts_sweetAlert__WEBPACK_IMPORTED_MODULE_10__["sweetAlert"])("warning", "Developer profile picture is required."));

            case 3:
              if (!(companyPic.substring(0, 8) === "/assets/")) {
                _context2.next = 5;
                break;
              }

              return _context2.abrupt("return", Object(_components_base_alerts_sweetAlert__WEBPACK_IMPORTED_MODULE_10__["sweetAlert"])("warning", "Company profile picture is required."));

            case 5:
              formData = new FormData(form.current);
              phone = formData.get("phone") ? "+61" + formData.get("phone") : "";
              formData.set("phone", phone);
              formData.append("user_type", "project_developer"); // let email = formData.get("userEmail");
              // formData.append("email", email);

              formData.append("dev_state", selectedState.value || "");
              formData.append("dev_country", selectedCountry.value || "");

              if (!Object(lodash__WEBPACK_IMPORTED_MODULE_5__["isEmpty"])(selectedBank) && selectedBank.value !== "Other") {
                formData.set("bank_id", selectedBank.value);
              }

              setLoading(true);
              url = "/api/user";
              _context2.next = 16;
              return axios.post(url, formData).then(function (res) {
                if (res.status == 200) {
                  Object(_components_base_alerts_sweetAlert__WEBPACK_IMPORTED_MODULE_10__["sweetAlert"])("success", "Developer successfully saved.");
                  userAction.setState({
                    fetch: !userState.fetch
                  });
                  fetchDevelopers();
                  setLoading(false);
                }
              })["catch"](function (error) {
                var errors = {};
                var data = error.response.data;

                if (data.errors.length == 1 && data.errors[0].phone) {
                  errors = [];
                }

                errors = data.errors;
                setLoading(false);

                if (errors && errors.avatar_path) {
                  Object(_components_base_alerts_sweetAlert__WEBPACK_IMPORTED_MODULE_10__["sweetAlert"])("error", errors.avatar_path[0]);
                  setProfilePic("/assets/images/profile_default.jpg");
                }

                setErrors(errors || {});
              });

            case 16:
            case "end":
              return _context2.stop();
          }
        }
      }, _callee2);
    }));

    return function handleSubmit(_x) {
      return _ref3.apply(this, arguments);
    };
  }();

  var handleChange = function handleChange(key, e) {
    if (key === "dev_country") {
      setSelectedCountry(e);
    } else if (key === "dev_state") {
      setSelectedState(e);
    } else {
      if (e.value === "Other") {
        setShowOtherBankText(true);
      } else {
        setShowOtherBankText(false);
      }

      setSelectedBank(e);
    }
  };

  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components_base__WEBPACK_IMPORTED_MODULE_3__["Modal"], {
    show: true,
    title: "Add Developer",
    disableBackdropClick: true,
    maxWidth: "md",
    topOff: true,
    scroll: "body",
    onClose: function onClose() {
      return toogleAddDeveloper();
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "bg-white rounded-lg py-5 px-8 m-5"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("section", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("section", {
    className: "relative bg-white"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "pb-16 mx-auto",
    style: {
      maxWidth: 1366
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("h1", {
    className: "font-bold leading-tight text-4xl pt-10 pb-8 lg:pb-16 text-center"
  }, "Member Profile"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("form", {
    ref: form,
    onSubmit: handleSubmit
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "flex flex-col-reverse lg:flex-row-reverse"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "lg:mt-0 lg:w-8/12 ml-16 mt-8"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components_base__WEBPACK_IMPORTED_MODULE_3__["Form"], {
    errors: errors,
    formFields: _helpers_userAccountSettingsHelper_userAccountHelper__WEBPACK_IMPORTED_MODULE_12__["fields"]
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "mb-5 text-base flex"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("label", {
    className: "mr-3 w-32 mt-2 capitalize pr-3"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
    className: "font-bold"
  }, "Password")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "w-full"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    onClick: function onClick() {
      return setIsOpen(!isOpen);
    },
    className: "\n                          border border-palette-gray text-palette-gray hover:bg-palette-gray hover:text-white transition-all duration-300\n                          cursor-pointer font-bold inline-block px-6 text-sm py-2 rounded shadow\n                        "
  }, "Change Password"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_material_ui_core_Collapse__WEBPACK_IMPORTED_MODULE_9__["default"], {
    "in": isOpen
  }, isOpen && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "mt-5 text-sm"
  }, "New Password"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components_base__WEBPACK_IMPORTED_MODULE_3__["TextInput"], {
    type: "password",
    placeholder: "New Password",
    name: "password",
    border: false,
    appearance: true
  }), errors.password && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
    className: "text-red-500 text-xs"
  }, errors.password[0]), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "mt-5 text-sm"
  }, "Confirm Password"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components_base__WEBPACK_IMPORTED_MODULE_3__["TextInput"], {
    type: "password",
    placeholder: "Confirm Password",
    name: "password_confirmation",
    border: false,
    appearance: true
  }))))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(react__WEBPACK_IMPORTED_MODULE_1___default.a.Fragment, null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "mx-auto -ml-20",
    style: {
      maxWidth: 1366
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("h1", {
    className: "font-bold leading-tight ml-3 lg:pb-16 pb-8 pt-10 text-4xl"
  }, "Seller Profile")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components_base__WEBPACK_IMPORTED_MODULE_3__["Form"], {
    errors: errors,
    formFields: _helpers_userAccountSettingsHelper_userAccountHelper__WEBPACK_IMPORTED_MODULE_12__["developerFields"]
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "mb-5 text-base flex"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("label", {
    className: "mr-3 w-32 mt-2 capitalize pr-3"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
    className: "font-bold"
  }, "Bank")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "w-full"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "flex"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(react_select__WEBPACK_IMPORTED_MODULE_11__["default"], {
    isOptionSelected: true,
    name: "bank",
    classNamePrefix: "input-select",
    onChange: function onChange(e) {
      return handleChange("bank", e);
    },
    className: "w-full",
    placeholder: "Select Bank",
    value: selectedBank,
    options: bankList
  })))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_material_ui_core_Collapse__WEBPACK_IMPORTED_MODULE_9__["default"], {
    "in": showOtherBankText
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "mb-5 text-base flex"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("label", {
    className: "mr-3 w-32 mt-2 capitalize pr-3"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
    className: "font-bold"
  })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "w-full"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "flex"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components_base__WEBPACK_IMPORTED_MODULE_3__["TextInput"], {
    type: "text",
    placeholder: "Enter Bank Name",
    name: "new_bank_name",
    border: false,
    appearance: true
  }))))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "mb-5 text-base flex"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("label", {
    className: "mr-3 w-32 mt-2 capitalize pr-3"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
    className: "font-bold"
  }, "Acct. Name")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "w-full"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "flex"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components_base__WEBPACK_IMPORTED_MODULE_3__["TextInput"], {
    type: "text",
    placeholder: "Account Name",
    name: "bank_account_name",
    border: false,
    appearance: true
  }), errors.bank_account_name && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
    className: "text-red-500 text-xs"
  }, errors.bank_account_name[0])))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "mb-5 text-base flex"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("label", {
    className: "mr-3 w-32 mt-2 capitalize pr-3"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
    className: "font-bold"
  }, "BSB")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "w-full"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "flex"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components_base__WEBPACK_IMPORTED_MODULE_3__["TextInput"], {
    type: "text",
    placeholder: "Enter BSB",
    name: "bank_bsb",
    border: false,
    appearance: true
  }), errors.bank_bsb && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
    className: "text-red-500 text-xs"
  }, errors.bank_bsb[0])))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "mb-5 text-base flex"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("label", {
    className: "mr-3 w-32 mt-2 capitalize pr-3"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
    className: "font-bold"
  }, "Acct. No.")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "w-full"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "flex"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components_base__WEBPACK_IMPORTED_MODULE_3__["TextInput"], {
    type: "text",
    placeholder: "Enter Account Number",
    name: "bank_account_number",
    border: false,
    appearance: true
  }), errors.bank_account_number && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
    className: "text-red-500 text-xs"
  }, errors.bank_account_number[0])))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "mb-5 text-base flex"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("label", {
    className: "mr-3 w-32 mt-2 capitalize pr-3"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
    className: "font-bold"
  }, "State")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "w-full"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "flex"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(react_select__WEBPACK_IMPORTED_MODULE_11__["default"], {
    isOptionSelected: true,
    name: "dev_state",
    classNamePrefix: "input-select",
    onChange: function onChange(e) {
      return handleChange("dev_state", e);
    },
    className: "w-full",
    placeholder: "Select State",
    value: selectedState,
    options: _helpers_propertyHelper_propertyHelper__WEBPACK_IMPORTED_MODULE_14__["states"]
  })), errors["value"] && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
    className: "text-red-500 text-sm"
  }, errors[value]))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "mb-5 text-base flex"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("label", {
    className: "mr-3 w-32 mt-2 capitalize pr-3"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
    className: "font-bold"
  }, "Country")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "w-full"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "flex"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(react_select__WEBPACK_IMPORTED_MODULE_11__["default"], {
    isOptionSelected: true,
    name: "dev_country",
    classNamePrefix: "input-select",
    onChange: function onChange(e) {
      return handleChange("dev_country", e);
    },
    className: "w-full",
    placeholder: "Select Country",
    value: selectedCountry,
    options: _helpers_countries__WEBPACK_IMPORTED_MODULE_13__["countries"]
  })), errors["value"] && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
    className: "text-red-500 text-sm"
  }, errors[value]))))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "flex flex-col w-1/2"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "flex-1" //className={`lg:w-4/12`}

  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("input", {
    type: "file",
    name: "avatar_path",
    className: "hidden",
    ref: profileInput,
    onChange: function onChange(e) {
      return handleFileChange(e, true);
    }
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "relative mx-auto",
    style: {
      width: "fit-content"
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("img", {
    className: "border-4 border-palette-gray h-48 lg:h-56 rounded-full shadow-lg w-48 lg:w-56 object-cover",
    src: profilePic
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
    className: "\n                        absolute bg-white border border-gray-300 bottom-0 cursor-pointer flex h-10 items-center justify-center mb-3 mr-3 right-0\n                        rounded-full shadow-md w-10 transition-all duration-300 hover:bg-palette-purple hover:text-white hover:border-palette-purple\n                      ",
    onClick: function onClick() {
      return handleClick(true);
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("svg", {
    className: "feather-icon"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("use", {
    xlinkHref: "/assets/svg/feather-sprite.svg#edit-2"
  }))))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "flex-1 mb-24" //className={`lg:w-4/12`}

  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("input", {
    type: "file",
    name: "company_logo_path",
    className: "hidden",
    ref: companyInput,
    onChange: function onChange(e) {
      return handleFileChange(e, false);
    }
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "relative mx-auto",
    style: {
      width: "fit-content"
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("img", {
    className: "border-4 border-palette-gray h-48 lg:h-56 shadow-lg w-48 lg:w-56 object-cover",
    src: companyPic
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
    className: "\n                        absolute bg-white border border-gray-300 bottom-0 cursor-pointer flex h-10 items-center justify-center mb-3 mr-3 right-0\n                        rounded-full shadow-md w-10 transition-all duration-300 hover:bg-palette-purple hover:text-white hover:border-palette-purple\n                      ",
    onClick: function onClick() {
      return handleClick(false);
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("svg", {
    className: "feather-icon"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("use", {
    xlinkHref: "/assets/svg/feather-sprite.svg#edit-2"
  }))))))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "flex justify-center lg:justify-end mt-8"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components_base__WEBPACK_IMPORTED_MODULE_3__["Button"], {
    onClick: function onClick() {
      return toogleAddDeveloper();
    },
    className: "bg-red-700 button font-bold mr-2 rounded text-gray-100",
    disabled: loading
  }, "Cancel"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components_base__WEBPACK_IMPORTED_MODULE_3__["Button"], {
    className: "font-bold rounded",
    disabled: loading
  }, loading && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_fortawesome_react_fontawesome__WEBPACK_IMPORTED_MODULE_7__["FontAwesomeIcon"], {
    icon: _fortawesome_free_solid_svg_icons__WEBPACK_IMPORTED_MODULE_8__["faSpinner"],
    className: "fa-spin mr-2"
  }), "Save")))))))));
};

/* harmony default export */ __webpack_exports__["default"] = (AddDeveloperModal);

/***/ }),

/***/ "./resources/js/admin/components/AddLandProperty.js":
/*!**********************************************************!*\
  !*** ./resources/js/admin/components/AddLandProperty.js ***!
  \**********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react_dropdown__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react-dropdown */ "./node_modules/react-dropdown/dist/index.js");
/* harmony import */ var react_dropdown__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react_dropdown__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _helpers_styles_dropdown_css__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../helpers/styles/dropdown.css */ "./resources/js/helpers/styles/dropdown.css");
/* harmony import */ var _helpers_styles_dropdown_css__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(_helpers_styles_dropdown_css__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _components_base__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../components/_base */ "./resources/js/components/_base/index.js");
/* harmony import */ var _components_base__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(_components_base__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var _RichTextEditor__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./RichTextEditor */ "./resources/js/admin/components/RichTextEditor.js");
/* harmony import */ var draft_js__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! draft-js */ "./node_modules/draft-js/lib/Draft.js");
/* harmony import */ var draft_js__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(draft_js__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var _helpers_styles_datepicker_css__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../helpers/styles/datepicker.css */ "./resources/js/helpers/styles/datepicker.css");
/* harmony import */ var _helpers_styles_datepicker_css__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(_helpers_styles_datepicker_css__WEBPACK_IMPORTED_MODULE_6__);
/* harmony import */ var draft_convert__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! draft-convert */ "./node_modules/draft-convert/esm/index.js");
/* harmony import */ var react_currency_format__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! react-currency-format */ "./node_modules/react-currency-format/lib/currency-format.js");
/* harmony import */ var react_currency_format__WEBPACK_IMPORTED_MODULE_8___default = /*#__PURE__*/__webpack_require__.n(react_currency_format__WEBPACK_IMPORTED_MODULE_8__);
/* harmony import */ var _states_userGlobal__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../../states/userGlobal */ "./resources/js/states/userGlobal.js");
/* harmony import */ var _helpers_styles_toast_css__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ../../helpers/styles/toast.css */ "./resources/js/helpers/styles/toast.css");
/* harmony import */ var _helpers_styles_toast_css__WEBPACK_IMPORTED_MODULE_10___default = /*#__PURE__*/__webpack_require__.n(_helpers_styles_toast_css__WEBPACK_IMPORTED_MODULE_10__);
/* harmony import */ var _fortawesome_react_fontawesome__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! @fortawesome/react-fontawesome */ "./node_modules/@fortawesome/react-fontawesome/index.es.js");
/* harmony import */ var _fortawesome_free_solid_svg_icons__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! @fortawesome/free-solid-svg-icons */ "./node_modules/@fortawesome/free-solid-svg-icons/index.es.js");
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! lodash */ "./node_modules/lodash/lodash.js");
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_13___default = /*#__PURE__*/__webpack_require__.n(lodash__WEBPACK_IMPORTED_MODULE_13__);
/* harmony import */ var _components_base_alerts_sweetAlert__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ../../components/_base/alerts/sweetAlert */ "./resources/js/components/_base/alerts/sweetAlert.js");
/* harmony import */ var _helpers_propertyHelper_propertyHelper__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! ../../helpers/propertyHelper/propertyHelper */ "./resources/js/helpers/propertyHelper/propertyHelper.js");
/* harmony import */ var _helpers_fileHelper_fileHelper__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! ../../helpers/fileHelper/fileHelper */ "./resources/js/helpers/fileHelper/fileHelper.js");
function _toConsumableArray(arr) { return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _unsupportedIterableToArray(arr) || _nonIterableSpread(); }

function _nonIterableSpread() { throw new TypeError("Invalid attempt to spread non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _iterableToArray(iter) { if (typeof Symbol !== "undefined" && Symbol.iterator in Object(iter)) return Array.from(iter); }

function _arrayWithoutHoles(arr) { if (Array.isArray(arr)) return _arrayLikeToArray(arr); }

function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _unsupportedIterableToArray(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

function _iterableToArrayLimit(arr, i) { if (typeof Symbol === "undefined" || !(Symbol.iterator in Object(arr))) return; var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }



















/* harmony default export */ __webpack_exports__["default"] = (function (_ref) {
  var projectName = _ref.projectName,
      projectId = _ref.projectId,
      toggleAddPropertyModal = _ref.toggleAddPropertyModal,
      addToggleFetch = _ref.addToggleFetch;

  var _UserGlobal = Object(_states_userGlobal__WEBPACK_IMPORTED_MODULE_9__["default"])(),
      _UserGlobal2 = _slicedToArray(_UserGlobal, 2),
      userState = _UserGlobal2[0],
      userActions = _UserGlobal2[1];

  var _useState = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])([]),
      _useState2 = _slicedToArray(_useState, 2),
      featuredImages = _useState2[0],
      setFeaturedImages = _useState2[1];

  var _useState3 = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])("4"),
      _useState4 = _slicedToArray(_useState3, 2),
      propertyType = _useState4[0],
      setPropertyType = _useState4[1];

  var _useState5 = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])([]),
      _useState6 = _slicedToArray(_useState5, 2),
      errors = _useState6[0],
      setErrors = _useState6[1];

  var _useState7 = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])(function () {
    return draft_js__WEBPACK_IMPORTED_MODULE_5__["EditorState"].createEmpty();
  }),
      _useState8 = _slicedToArray(_useState7, 2),
      editorState = _useState8[0],
      setEditorState = _useState8[1];

  var _useState9 = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])([]),
      _useState10 = _slicedToArray(_useState9, 2),
      floorPlan = _useState10[0],
      setFLoorPlan = _useState10[1];

  var _useState11 = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])(""),
      _useState12 = _slicedToArray(_useState11, 2),
      filename = _useState12[0],
      setFilename = _useState12[1];

  var _useState13 = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])(""),
      _useState14 = _slicedToArray(_useState13, 2),
      prevFilename = _useState14[0],
      setPrevFilename = _useState14[1];

  var _useState15 = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])([]),
      _useState16 = _slicedToArray(_useState15, 2),
      previewImage = _useState16[0],
      setPreviewImage = _useState16[1];

  var _useState17 = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])([]),
      _useState18 = _slicedToArray(_useState17, 2),
      fmPlaceHolder = _useState18[0],
      setFmPlaceHolder = _useState18[1];

  var _useState19 = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])(false),
      _useState20 = _slicedToArray(_useState19, 2),
      limitError = _useState20[0],
      setLimitError = _useState20[1];

  var addPropertyForm = Object(react__WEBPACK_IMPORTED_MODULE_0__["useRef"])(null);

  var _useState21 = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])(false),
      _useState22 = _slicedToArray(_useState21, 2),
      loading = _useState22[0],
      setLoading = _useState22[1];

  var previewImageRef = Object(react__WEBPACK_IMPORTED_MODULE_0__["useRef"])(null);
  var floorPlanRef = Object(react__WEBPACK_IMPORTED_MODULE_0__["useRef"])(null);
  Object(react__WEBPACK_IMPORTED_MODULE_0__["useEffect"])(function () {
    var phs = [];

    for (var index = 0; index < 6 - featuredImages.length; index++) {
      var ph = {
        classes: "cursor-pointer m-2 inline-flex bg-transparent text-palette-gray"
      };
      phs.push(ph);
    }

    setFmPlaceHolder(phs);
  }, [featuredImages.length]);

  var handleDeletePrevFile = function handleDeletePrevFile(e) {
    e.preventDefault();
    setPreviewImage([]);
    previewImageRef.current.type = "";
    previewImageRef.current.type = "file";
    setPrevFilename("");
    e.target.value = null;
  };

  var handlePreviewImageDisplay = function handlePreviewImageDisplay(e) {
    e.preventDefault();

    if (e.target.files.length > 1) {
      Object(_components_base_alerts_sweetAlert__WEBPACK_IMPORTED_MODULE_14__["sweetAlert"])("error", "Multiple upload is not allowed!");
      return;
    }

    if (!Object(_helpers_fileHelper_fileHelper__WEBPACK_IMPORTED_MODULE_16__["imageFileTypeFilter"])(e.target.files[0]["name"])) {
      Object(_components_base_alerts_sweetAlert__WEBPACK_IMPORTED_MODULE_14__["sweetAlert"])("error", "Filetype is not supported!");
      e.target.value = null;
      return;
    }

    setPreviewImage(previewImage.concat(_toConsumableArray(_toConsumableArray(e.target.files).map(function (file) {
      var newFilename = Object(_helpers_fileHelper_fileHelper__WEBPACK_IMPORTED_MODULE_16__["sanitizeFilename"])(file.name);
      var newFileObj = new File([file], newFilename);
      setPrevFilename(newFilename);
      return {
        image: newFileObj,
        preview: URL.createObjectURL(newFileObj)
      };
    }))));
  };

  var saveProperty = function saveProperty() {
    var _userState$projectDat;

    setLoading(true);
    var formData = new FormData(addPropertyForm.current);
    formData.append("role", "project_developer");

    if (floorPlan && !Object(lodash__WEBPACK_IMPORTED_MODULE_13__["isEmpty"])(floorPlan)) {
      formData.append("floorPlan[file]", floorPlan[0]);
    }

    if (previewImage.length) {
      formData.append("preview_img[".concat(0, "]"), previewImage[0].image);
    } else {
      formData.append("preview_img[".concat(0, "]"), "");
    } // formData.append("projectName", projectName);


    formData.append("projectId", (_userState$projectDat = userState.projectData.id) !== null && _userState$projectDat !== void 0 ? _userState$projectDat : projectId); // formData.append("ownershipType", ownershipType);
    // formData.append("subPropertyType", subPropertyType);

    formData.append("propertyType", propertyType); // formData.append("water", water);
    // formData.append("council", council);
    // formData.append("strata", strata);
    // formData.append("state", state);

    formData.append("description", editorState.getCurrentContent().getPlainText());
    formData.append("descriptionHTML", Object(draft_convert__WEBPACK_IMPORTED_MODULE_7__["convertToHTML"])(editorState.getCurrentContent()));
    featuredImages.map(function (image, key) {
      formData.append("featuredImages[".concat(key, "]"), image.image);
    });
    var url = "/api/property/";
    axios({
      method: "post",
      url: url,
      data: formData,
      name: "project",
      headers: {
        "content-type": "multipart/form-data"
      }
    }).then(function (r) {
      if (r.status === 200) {
        Object(_components_base_alerts_sweetAlert__WEBPACK_IMPORTED_MODULE_14__["sweetAlert"])("success", "Property successfully added.");
        setLoading(false);
        userActions.setState({
          propertiesData: r.data
        });
        addToggleFetch();
        toggleAddPropertyModal();
      }
    })["catch"](function (error) {
      if (error.response.data.errors) {
        Object(_components_base_alerts_sweetAlert__WEBPACK_IMPORTED_MODULE_14__["sweetAlert"])("error", "Some field are missing.");
      }

      setLoading(false);
      setErrors(error.response.data.errors);
    }).then(function () {});
  };

  var handleSubmit = function handleSubmit(e) {
    e.preventDefault();
    setLimitError(false);
    saveProperty(); // window.location.href = window.location.href;
  };

  var handleDrop = function handleDrop(e) {
    e.preventDefault();
    e.stopPropagation();
    var fmCount = featuredImages.length;
    var onCount = e.dataTransfer.files.length;

    if (fmCount + onCount > 6) {
      e.target.value = null;
      setLimitError(true);
      return;
    } else {
      setLimitError(false);
    }

    setFeaturedImages(featuredImages.concat(_toConsumableArray(_toConsumableArray(e.dataTransfer.files).map(function (file) {
      var newFilename = Object(_helpers_fileHelper_fileHelper__WEBPACK_IMPORTED_MODULE_16__["sanitizeFilename"])(file.name);
      var newFileObj = new File([file], newFilename);
      return {
        image: newFileObj,
        preview: URL.createObjectURL(newFileObj)
      };
    }))));
    e.target.value = null;
  };

  var handleChange = function handleChange(e) {
    var fmCount = featuredImages.length;
    var onCount = e.target.files.length;

    if (fmCount + onCount > 6) {
      setLimitError(true);
      e.target.value = null;
      return;
    } else {
      setLimitError(false);
    }

    setFeaturedImages(featuredImages.concat(_toConsumableArray(_toConsumableArray(e.target.files).map(function (file) {
      return {
        image: file,
        preview: URL.createObjectURL(file)
      };
    }))));
    e.target.value = null;
  };

  var handleRemove = function handleRemove(e, i) {
    e.preventDefault();
    e.stopPropagation();
    setFeaturedImages(_toConsumableArray(featuredImages.filter(function (image, k) {
      if (i !== k) {
        return image;
      }
    })));

    if (featuredImages.length > 6) {
      setLimitError(true);
    } else {
      setLimitError(false);
    }
  };

  var handleFloorPlanChange = function handleFloorPlanChange(e) {
    e.preventDefault();

    if (!Object(_helpers_fileHelper_fileHelper__WEBPACK_IMPORTED_MODULE_16__["PDFFileTypeFilter"])(e.target.files[0]["name"])) {
      Object(_components_base_alerts_sweetAlert__WEBPACK_IMPORTED_MODULE_14__["sweetAlert"])("error", "Filetype is not supported!");
      e.target.value = null;
      return;
    }

    setFLoorPlan(Object(_helpers_propertyHelper_propertyHelper__WEBPACK_IMPORTED_MODULE_15__["floorPlanHelper"])(e.target.files));
    var newFilename = Object(_helpers_fileHelper_fileHelper__WEBPACK_IMPORTED_MODULE_16__["sanitizeFilename"])(e.target.files[0]["name"]);
    setFilename(newFilename); // e.target.value = null;
  };

  var handleDeleteFile = function handleDeleteFile(e) {
    e.preventDefault();
    setFLoorPlan({});
    floorPlanRef.current.type = "";
    floorPlanRef.current.type = "file";
    setFilename("");
    e.target.value = null;
  };

  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_base__WEBPACK_IMPORTED_MODULE_3__["Modal"], {
    show: true,
    title: "Add Land Property",
    disableBackdropClick: true,
    maxWidth: "md",
    topOff: true,
    onClose: function onClose() {
      return toggleAddPropertyModal();
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "bg-white rounded-lg py-5 px-8 m-5"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("form", {
    ref: addPropertyForm,
    onSubmit: handleSubmit
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "text-black"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "py-3 flex justify-start "
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "flex justify-center items-center"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("label", {
    className: "font-semibold w-24"
  }, "Property Type"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_dropdown__WEBPACK_IMPORTED_MODULE_1___default.a, {
    id: "property_type",
    value: propertyType,
    options: _helpers_propertyHelper_propertyHelper__WEBPACK_IMPORTED_MODULE_15__["property_types"],
    onChange: function onChange(option) {
      setPropertyType(option.value);
    },
    placeholder: "Please select..." // name="property_type"
    ,
    className: "w-48 ml-4",
    disabled: true
  }), errors["propertyType"] && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
    className: "px-1 text-red-400 tracking-widest  text-xs "
  }, errors["propertyType"][0]))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "ml-10 flex-1 flex justify-end items-center"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("label", {
    className: "font-semibold w-32"
  }, "Lot Number"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_base__WEBPACK_IMPORTED_MODULE_3__["TextInput"], {
    className: "border-gray-400 border pl-2 py-1",
    border: false,
    name: "unit_name",
    width: "w-24 ml-4"
  }), errors["unit_name"] && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
    className: "px-1 text-red-400 tracking-widest  text-xs "
  }, errors["unit_name"][0]))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "ml-4 flex-1 flex justify-end items-center"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("label", {
    className: "font-semibold w-12"
  }, "Size"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_base__WEBPACK_IMPORTED_MODULE_3__["TextInput"], {
    className: "border-gray-400 border pl-2 py-1",
    border: false,
    name: "size",
    width: "w-24"
  }), errors["size"] && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
    className: "px-1 text-red-400 tracking-widest  text-xs "
  }, errors["size"][0])))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "flex justify-start my-3"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "flex items-center justify-start py-3"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("label", {
    className: "w-12"
  }, "Price"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "relative"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_currency_format__WEBPACK_IMPORTED_MODULE_8___default.a, {
    allowNegative: false,
    className: "border text-sm border-gray-400 px-2 py-1 w-48",
    thousandSeparator: true,
    name: "price",
    id: "price",
    prefix: "$"
  })), errors["price"] && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
    className: "absolute px-1 text-red-400 tracking-widest text-xs "
  }, errors["price"][0]))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "py-3 flex justify-start items-center ml-10"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("label", {
    className: ""
  }, "Frontage"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "relative"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "ml-4 w-24"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_base__WEBPACK_IMPORTED_MODULE_3__["TextInput"], {
    className: "pl-2 text-sm border-gray-400 w-32 border py-1",
    border: false // type={`number`}
    ,
    name: "frontage"
  })), errors["frontage"] && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
    className: "absolute px-1 text-red-400 tracking-widest  text-xs "
  }, errors["frontage"][0]))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "py-3 flex justify-start items-center ml-8"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("label", {
    className: ""
  }, "Width"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "relative"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "ml-4 w-24"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_base__WEBPACK_IMPORTED_MODULE_3__["TextInput"], {
    className: "pl-2 text-sm border-gray-400 w-32 border py-1",
    border: false // type={`number`}
    ,
    name: "width"
  })), errors["width"] && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
    className: "absolute px-1 text-red-400 tracking-widest  text-xs "
  }, errors["width"][0]))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "py-3 flex justify-start items-center ml-8"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("label", {
    className: ""
  }, "Depth "), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "relative"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "ml-4 w-24"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_base__WEBPACK_IMPORTED_MODULE_3__["TextInput"], {
    className: "pl-2 text-sm border-gray-400 w-32 border py-1 mr-1",
    border: false // type={`number`}
    ,
    name: "depth"
  })), errors["depth"] && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
    className: "absolute px-1 text-red-400 tracking-widest  text-xs "
  }, errors["depth"][0])))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "py-3 "
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("label", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "w-full text-sm bg-white text-black "
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_RichTextEditor__WEBPACK_IMPORTED_MODULE_4__["default"], {
    title: "Property Description",
    richEditorState: editorState,
    richEditorStateContent: function richEditorStateContent(content) {
      return setEditorState(content);
    },
    characterLimit: 2000
  }))), errors["description"] && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
    className: "px-1 text-red-400 tracking-widest  text-xs "
  }, errors["description"][0])), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "py-3"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("h1", {
    className: "font-semibold"
  }, " ", "Featured Images (min. 3 images)", " "), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("label", {
    className: "button ",
    htmlFor: "upload-photos"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: ""
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "w-full flex flex-wrap  cursor-pointer",
    style: {
      border: "2px dashed #ccc",
      borderRadius: "20px",
      padding: "20px",
      textAlign: "center",
      marginBottom: "0px"
    },
    onDrop: function onDrop(e) {
      return handleDrop(e);
    },
    onDragOver: function onDragOver(e) {
      e.preventDefault();
      e.stopPropagation();
    },
    onDragEnter: function onDragEnter(e) {
      e.preventDefault();
      e.stopPropagation();
    },
    onDragLeave: function onDragLeave(e) {
      e.preventDefault();
      e.stopPropagation();
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("input", {
    type: "file",
    id: "upload-photos",
    accept: "image/*",
    multiple: true,
    className: "hidden",
    name: "image[]",
    onChange: function onChange(e) {
      return handleChange(e);
    }
  }), featuredImages.map(function (image, index) {
    return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      key: index,
      className: "rounded-sm border m-2 relative",
      style: {
        backgroundImage: "url('".concat(image.preview, "')"),
        backgroundPositionX: "center",
        width: "6.9em",
        height: "8.9rem",
        backgroundSize: "cover"
      }
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_base__WEBPACK_IMPORTED_MODULE_3__["Tooltip"], {
      title: "Remove this item"
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
      onClick: function onClick(e) {
        return handleRemove(e, index);
      },
      className: "absolute bottom-0 right-0 p-2 -mb-2 "
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_fortawesome_react_fontawesome__WEBPACK_IMPORTED_MODULE_11__["FontAwesomeIcon"], {
      className: "text-red-500 shadow-xl hover:font-bold hover:text-red-700",
      icon: _fortawesome_free_solid_svg_icons__WEBPACK_IMPORTED_MODULE_12__["faTimes"]
    }))));
  }), fmPlaceHolder.map(function (p, i) {
    return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      key: i,
      className: "".concat(p.classes, " cursor-pointer flex justify-center items-center"),
      style: {
        width: "6.9rem",
        height: "8.9rem",
        border: "2px solid #ccc"
      }
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_fortawesome_react_fontawesome__WEBPACK_IMPORTED_MODULE_11__["FontAwesomeIcon"], {
      className: "text-xl",
      icon: _fortawesome_free_solid_svg_icons__WEBPACK_IMPORTED_MODULE_12__["faCamera"]
    }));
  })), limitError && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
    className: "px-1 text-red-400 tracking-widest  text-xs "
  }, "Please limit your photos to six items only."))), errors["featuredImages"] && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
    className: "px-1 text-red-400 tracking-widest  text-xs "
  }, errors["featuredImages"][0])), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "flex flex-row"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "w-1/2 ml-2"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("h1", {
    className: "font-semibold"
  }, "Rich Text Preview "), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "flex flex-col justify-between py-3"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "flex"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("label", {
    className: "w-full"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "items-center cursor-pointer w-full p-3 rounded-lg flex",
    style: {
      border: "2px dashed #ccc",
      borderRadius: "20px"
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("input", {
    id: "rtp",
    ref: previewImageRef,
    type: "file" // name="prev_image"
    ,
    onChange: function onChange(e) {
      return handlePreviewImageDisplay(e);
    },
    className: "hidden border text-sm border-gray-400 px-2 py-1 w-full py-1"
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("label", {
    htmlFor: "rtp",
    className: " cursor-pointer hover:text-palette-blue-light ".concat(prevFilename ? "text-palette-blue-light" : "")
  }, prevFilename || "Select File"), prevFilename && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "text-red-500 text-lg cursor-pointer ml-3"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_fortawesome_react_fontawesome__WEBPACK_IMPORTED_MODULE_11__["FontAwesomeIcon"], {
    onClick: function onClick(e) {
      return handleDeletePrevFile(e);
    },
    icon: _fortawesome_free_solid_svg_icons__WEBPACK_IMPORTED_MODULE_12__["faTimes"]
  })))))))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "flex justify-end my-5"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_base__WEBPACK_IMPORTED_MODULE_3__["Button"], {
    className: "font-bold rounded-full",
    disabled: loading
  }, loading && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_fortawesome_react_fontawesome__WEBPACK_IMPORTED_MODULE_11__["FontAwesomeIcon"], {
    icon: _fortawesome_free_solid_svg_icons__WEBPACK_IMPORTED_MODULE_12__["faSpinner"],
    className: "fa-spin mr-2"
  }), "Save"))))));
});

/***/ }),

/***/ "./resources/js/admin/components/AddLandPropertyModal.js":
/*!***************************************************************!*\
  !*** ./resources/js/admin/components/AddLandPropertyModal.js ***!
  \***************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime/regenerator */ "./node_modules/@babel/runtime/regenerator/index.js");
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var react_dropdown__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! react-dropdown */ "./node_modules/react-dropdown/dist/index.js");
/* harmony import */ var react_dropdown__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(react_dropdown__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _helpers_styles_dropdown_css__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../helpers/styles/dropdown.css */ "./resources/js/helpers/styles/dropdown.css");
/* harmony import */ var _helpers_styles_dropdown_css__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(_helpers_styles_dropdown_css__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var _components_base__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../components/_base */ "./resources/js/components/_base/index.js");
/* harmony import */ var _components_base__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(_components_base__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var _fortawesome_free_solid_svg_icons__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @fortawesome/free-solid-svg-icons */ "./node_modules/@fortawesome/free-solid-svg-icons/index.es.js");
/* harmony import */ var _helpers_propertyHelper_propertyHelper__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../helpers/propertyHelper/propertyHelper */ "./resources/js/helpers/propertyHelper/propertyHelper.js");
/* harmony import */ var _RichTextEditor__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./RichTextEditor */ "./resources/js/admin/components/RichTextEditor.js");
/* harmony import */ var draft_js__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! draft-js */ "./node_modules/draft-js/lib/Draft.js");
/* harmony import */ var draft_js__WEBPACK_IMPORTED_MODULE_8___default = /*#__PURE__*/__webpack_require__.n(draft_js__WEBPACK_IMPORTED_MODULE_8__);
/* harmony import */ var _helpers_styles_datepicker_css__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../../helpers/styles/datepicker.css */ "./resources/js/helpers/styles/datepicker.css");
/* harmony import */ var _helpers_styles_datepicker_css__WEBPACK_IMPORTED_MODULE_9___default = /*#__PURE__*/__webpack_require__.n(_helpers_styles_datepicker_css__WEBPACK_IMPORTED_MODULE_9__);
/* harmony import */ var draft_convert__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! draft-convert */ "./node_modules/draft-convert/esm/index.js");
/* harmony import */ var react_currency_format__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! react-currency-format */ "./node_modules/react-currency-format/lib/currency-format.js");
/* harmony import */ var react_currency_format__WEBPACK_IMPORTED_MODULE_11___default = /*#__PURE__*/__webpack_require__.n(react_currency_format__WEBPACK_IMPORTED_MODULE_11__);
/* harmony import */ var _fortawesome_react_fontawesome__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! @fortawesome/react-fontawesome */ "./node_modules/@fortawesome/react-fontawesome/index.es.js");
/* harmony import */ var _states_userGlobal__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ../../states/userGlobal */ "./resources/js/states/userGlobal.js");
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! lodash */ "./node_modules/lodash/lodash.js");
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_14___default = /*#__PURE__*/__webpack_require__.n(lodash__WEBPACK_IMPORTED_MODULE_14__);
/* harmony import */ var _components_base_alerts_sweetAlert__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! ../../components/_base/alerts/sweetAlert */ "./resources/js/components/_base/alerts/sweetAlert.js");
/* harmony import */ var _components_base_alerts_BaseAlert__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! ../../components/_base/alerts/BaseAlert */ "./resources/js/components/_base/alerts/BaseAlert.js");
/* harmony import */ var _helpers_fileHelper_fileHelper__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! ../../helpers/fileHelper/fileHelper */ "./resources/js/helpers/fileHelper/fileHelper.js");


function _toConsumableArray(arr) { return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _unsupportedIterableToArray(arr) || _nonIterableSpread(); }

function _nonIterableSpread() { throw new TypeError("Invalid attempt to spread non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _iterableToArray(iter) { if (typeof Symbol !== "undefined" && Symbol.iterator in Object(iter)) return Array.from(iter); }

function _arrayWithoutHoles(arr) { if (Array.isArray(arr)) return _arrayLikeToArray(arr); }

function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _unsupportedIterableToArray(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

function _iterableToArrayLimit(arr, i) { if (typeof Symbol === "undefined" || !(Symbol.iterator in Object(arr))) return; var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }



















/* harmony default export */ __webpack_exports__["default"] = (function (_ref) {
  var projectName = _ref.projectName,
      projectId = _ref.projectId,
      toggleAddLandPropertyModal = _ref.toggleAddLandPropertyModal,
      addToggleFetch = _ref.addToggleFetch;

  var _UserGlobal = Object(_states_userGlobal__WEBPACK_IMPORTED_MODULE_13__["default"])(),
      _UserGlobal2 = _slicedToArray(_UserGlobal, 2),
      userState = _UserGlobal2[0],
      userActions = _UserGlobal2[1];

  var _useState = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])([]),
      _useState2 = _slicedToArray(_useState, 2),
      featuredImages = _useState2[0],
      setFeaturedImages = _useState2[1];

  var _useState3 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])("4"),
      _useState4 = _slicedToArray(_useState3, 2),
      propertyType = _useState4[0],
      setPropertyType = _useState4[1];

  var _useState5 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])([]),
      _useState6 = _slicedToArray(_useState5, 2),
      errors = _useState6[0],
      setErrors = _useState6[1];

  var _useState7 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(function () {
    return draft_js__WEBPACK_IMPORTED_MODULE_8__["EditorState"].createEmpty();
  }),
      _useState8 = _slicedToArray(_useState7, 2),
      editorState = _useState8[0],
      setEditorState = _useState8[1];

  var _useState9 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])([]),
      _useState10 = _slicedToArray(_useState9, 2),
      floorPlan = _useState10[0],
      setFLoorPlan = _useState10[1];

  var _useState11 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(""),
      _useState12 = _slicedToArray(_useState11, 2),
      filename = _useState12[0],
      setFilename = _useState12[1];

  var _useState13 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])([]),
      _useState14 = _slicedToArray(_useState13, 2),
      previewImage = _useState14[0],
      setPreviewImage = _useState14[1];

  var _useState15 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(""),
      _useState16 = _slicedToArray(_useState15, 2),
      prevFilename = _useState16[0],
      setPrevFilename = _useState16[1];

  var _useState17 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])([]),
      _useState18 = _slicedToArray(_useState17, 2),
      fmPlaceHolder = _useState18[0],
      setFmPlaceHolder = _useState18[1];

  var _useState19 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(false),
      _useState20 = _slicedToArray(_useState19, 2),
      limitError = _useState20[0],
      setLimitError = _useState20[1];

  var _useState21 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])({}),
      _useState22 = _slicedToArray(_useState21, 2),
      deals = _useState22[0],
      setDeals = _useState22[1];

  var _useState23 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(""),
      _useState24 = _slicedToArray(_useState23, 2),
      selectedDeal = _useState24[0],
      setSelectedDeal = _useState24[1];

  var addPropertyForm = Object(react__WEBPACK_IMPORTED_MODULE_1__["useRef"])(null);

  var _useState25 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(true),
      _useState26 = _slicedToArray(_useState25, 2),
      loading = _useState26[0],
      setLoading = _useState26[1];

  var _useState27 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(_helpers_propertyHelper_propertyHelper__WEBPACK_IMPORTED_MODULE_6__["costDueDates"]),
      _useState28 = _slicedToArray(_useState27, 2),
      dates = _useState28[0],
      setDates = _useState28[1];

  var previewImageRef = Object(react__WEBPACK_IMPORTED_MODULE_1__["useRef"])(null);
  var floorPlanRef = Object(react__WEBPACK_IMPORTED_MODULE_1__["useRef"])(null);
  Object(react__WEBPACK_IMPORTED_MODULE_1__["useEffect"])(function () {
    var phs = [];

    for (var index = 0; index < 6 - featuredImages.length; index++) {
      var ph = {
        classes: "cursor-pointer m-2 inline-flex bg-transparent text-palette-gray"
      };
      phs.push(ph);
    }

    setFmPlaceHolder(phs);
  }, [featuredImages.length]);
  Object(react__WEBPACK_IMPORTED_MODULE_1__["useEffect"])(function () {
    getDeals();
  }, []);

  var getDeals = /*#__PURE__*/function () {
    var _ref2 = _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee() {
      var url;
      return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee$(_context) {
        while (1) {
          switch (_context.prev = _context.next) {
            case 0:
              url = "".concat(window.location.origin, "/api/deal");
              _context.next = 3;
              return axios({
                method: "get",
                url: url,
                params: {
                  intent: "listings",
                  isProjectLand: true
                }
              }).then(function (result) {
                var array = Object(lodash__WEBPACK_IMPORTED_MODULE_14__["values"])(result.data);
                setLoading(false);
                setDeals(_toConsumableArray(array.map(function (deal) {
                  return {
                    label: "".concat(deal.name),
                    value: String(deal.id)
                  };
                })));
              })["catch"](function (error) {});

            case 3:
            case "end":
              return _context.stop();
          }
        }
      }, _callee);
    }));

    return function getDeals() {
      return _ref2.apply(this, arguments);
    };
  }();

  var handlePreviewImageDisplay = function handlePreviewImageDisplay(e) {
    e.preventDefault();

    if (e.target.files.length > 1) {
      Object(_components_base_alerts_sweetAlert__WEBPACK_IMPORTED_MODULE_15__["sweetAlert"])("error", "Multiple upload is not allowed!");
      return;
    }

    if (!Object(_helpers_fileHelper_fileHelper__WEBPACK_IMPORTED_MODULE_17__["imageFileTypeFilter"])(e.target.files[0]["name"])) {
      Object(_components_base_alerts_sweetAlert__WEBPACK_IMPORTED_MODULE_15__["sweetAlert"])("error", "Filetype is not supported!");
      e.target.value = null;
      return;
    }

    setPreviewImage(previewImage.concat(_toConsumableArray(_toConsumableArray(e.target.files).map(function (file) {
      var newFilename = Object(_helpers_fileHelper_fileHelper__WEBPACK_IMPORTED_MODULE_17__["sanitizeFilename"])(file.name);
      var newFileObj = new File([file], newFilename);
      setPrevFilename(newFilename);
      return {
        image: newFileObj,
        preview: URL.createObjectURL(newFileObj)
      };
    }))));
  };

  var handleSubmit = function handleSubmit(e) {
    e.preventDefault();
    setLimitError(false);
    setLoading(true);
    var formData = new FormData(addPropertyForm.current);
    formData.append("role", "admin");
    formData.append("project", selectedDeal);

    if (floorPlan && !Object(lodash__WEBPACK_IMPORTED_MODULE_14__["isEmpty"])(floorPlan)) {
      formData.append("floorPlan[file]", floorPlan[0]);
    }

    formData.append("projectName", projectName);
    formData.append("projectId", selectedDeal);
    formData.append("propertyType", propertyType); // formData.append("state", state);

    formData.append("description", editorState.getCurrentContent().getPlainText());
    formData.append("descriptionHTML", Object(draft_convert__WEBPACK_IMPORTED_MODULE_10__["convertToHTML"])(editorState.getCurrentContent()));
    featuredImages.map(function (image, key) {
      formData.append("featuredImages[".concat(key, "]"), image.image);
    });

    if (previewImage.length) {
      formData.append("preview_img[".concat(0, "]"), previewImage[0].image);
    } else {
      formData.append("preview_img[".concat(0, "]"), "");
    }

    var url = "/api/property/";
    axios({
      method: "post",
      url: url,
      data: formData,
      name: "project",
      headers: {
        "content-type": "multipart/form-data"
      }
    }).then(function (r) {
      if (r.status === 200) {
        Object(_components_base_alerts_sweetAlert__WEBPACK_IMPORTED_MODULE_15__["sweetAlert"])("success", "Property added successfully.");
        setLoading(false);
        setErrors([]);
        addToggleFetch();
        toggleAddLandPropertyModal();
      }
    })["catch"](function (error) {
      setErrors(error.response.data.errors);
      setLoading(false);
    }).then(function () {});
  };

  var handleDrop = function handleDrop(e) {
    e.preventDefault();
    e.stopPropagation();
    var fmCount = featuredImages.length;
    var onCount = e.dataTransfer.files.length;

    if (fmCount + onCount > 6) {
      e.target.value = null;
      setLimitError(true);
      return;
    } else {
      setLimitError(false);
    }

    setFeaturedImages(featuredImages.concat(_toConsumableArray(_toConsumableArray(e.dataTransfer.files).map(function (file) {
      var newFilename = Object(_helpers_fileHelper_fileHelper__WEBPACK_IMPORTED_MODULE_17__["sanitizeFilename"])(file.name);
      var newFileObj = new File([file], newFilename);
      return {
        image: newFileObj,
        preview: URL.createObjectURL(newFileObj)
      };
    }))));
    e.target.value = null;
  };

  var handleChange = function handleChange(e) {
    var fmCount = featuredImages.length;
    var onCount = e.target.files.length;

    if (fmCount + onCount > 6) {
      setLimitError(true);
      e.target.value = null;
      return;
    } else {
      setLimitError(false);
    }

    setFeaturedImages(featuredImages.concat(_toConsumableArray(_toConsumableArray(e.target.files).map(function (file) {
      return {
        image: file,
        preview: URL.createObjectURL(file)
      };
    }))));
    e.target.value = null;
  };

  var handleRemove = function handleRemove(e, i) {
    e.preventDefault();
    e.stopPropagation();
    setFeaturedImages(_toConsumableArray(featuredImages.filter(function (image, k) {
      if (i !== k) {
        return image;
      }
    })));

    if (featuredImages.length > 6) {
      setLimitError(true);
    } else {
      setLimitError(false);
    }
  };

  var handleFloorPlanChange = function handleFloorPlanChange(e) {
    e.preventDefault();

    if (!Object(_helpers_fileHelper_fileHelper__WEBPACK_IMPORTED_MODULE_17__["PDFFileTypeFilter"])(e.target.files[0]["name"])) {
      Object(_components_base_alerts_sweetAlert__WEBPACK_IMPORTED_MODULE_15__["sweetAlert"])("error", "Filetype is not supported!");
      e.target.value = null;
      return;
    }

    var file = Object.values(e.target.files).map(function (file) {
      var newFilename = file.name.replace(/[^.a-zA-Z0-9]/g, "");
      var newFileObj = new File([file], newFilename);
      return Object.assign(newFileObj, {
        resource: URL.createObjectURL(newFileObj)
      });
    });
    setFLoorPlan(Object(_helpers_propertyHelper_propertyHelper__WEBPACK_IMPORTED_MODULE_6__["floorPlanHelper"])(e.target.files));
    var newFilename = Object(_helpers_fileHelper_fileHelper__WEBPACK_IMPORTED_MODULE_17__["sanitizeFilename"])(e.target.files[0]["name"]);
    setFilename(newFilename); // e.target.value = null;
  };

  var handleDeletePrevFile = function handleDeletePrevFile(e) {
    e.preventDefault();
    setPreviewImage([]);
    previewImageRef.current.type = "";
    previewImageRef.current.type = "file";
    setPrevFilename("");
    e.target.value = null;
  };

  var handleDeleteFile = function handleDeleteFile(e) {
    e.preventDefault();
    setFLoorPlan([]);
    floorPlanRef.current.type = "";
    floorPlanRef.current.type = "file";
    setFilename("");
    e.target.value = null;
  };

  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components_base__WEBPACK_IMPORTED_MODULE_4__["Modal"], {
    show: true,
    maxWidth: "md",
    title: "Add Land Property",
    onClose: function onClose() {
      return toggleAddLandPropertyModal();
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("form", {
    ref: addPropertyForm,
    className: "bg-white rounded-lg py-5 px-8 m-5",
    onSubmit: handleSubmit
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "text-black"
  }, loading && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "bg-palette-purple flex flex-1 items-center mb-4 px-2 rounded text-white"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("label", {
    className: "block py-3 font-semibold w-32 mr-2"
  }, "Loading Projects"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_fortawesome_react_fontawesome__WEBPACK_IMPORTED_MODULE_12__["FontAwesomeIcon"], {
    icon: _fortawesome_free_solid_svg_icons__WEBPACK_IMPORTED_MODULE_5__["faSpinner"],
    className: "fa-spin mr-2"
  })), Object(lodash__WEBPACK_IMPORTED_MODULE_14__["isEmpty"])(deals) && !loading && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components_base_alerts_BaseAlert__WEBPACK_IMPORTED_MODULE_16__["default"], {
    message: "You cannot add property at the moment. All projects already have five properties.",
    icon: "x",
    type: "error"
  }), !Object(lodash__WEBPACK_IMPORTED_MODULE_14__["isEmpty"])(deals) && !loading && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "my-4 flex items-center"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("label", {
    className: "block py-3 font-semibold w-32"
  }, "Project Name"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "relative"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(react_dropdown__WEBPACK_IMPORTED_MODULE_2___default.a, {
    id: "deal",
    value: selectedDeal,
    options: deals,
    onChange: function onChange(option) {
      setSelectedDeal(option.value);
    },
    placeholder: "Please select...",
    name: "deal",
    disabled: true
  }), errors["project"] && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
    className: "absolute px-1 text-red-400 tracking-widest  text-xs "
  }, errors["project"][0]))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "text-black"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "py-3 flex justify-start "
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "flex justify-center items-center"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("label", {
    className: "font-semibold w-24"
  }, "Property Type"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(react_dropdown__WEBPACK_IMPORTED_MODULE_2___default.a, {
    id: "property_type",
    value: propertyType,
    options: _helpers_propertyHelper_propertyHelper__WEBPACK_IMPORTED_MODULE_6__["property_types"],
    onChange: function onChange(option) {
      setPropertyType(option.value);
    },
    placeholder: "Please select..." // name="property_type"
    ,
    className: "w-48 ml-4",
    disabled: true
  }), errors["propertyType"] && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
    className: "px-1 text-red-400 tracking-widest  text-xs "
  }, errors["propertyType"][0]))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "ml-10 flex-1 flex justify-end items-center"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("label", {
    className: "font-semibold w-32 mr-4"
  }, "Lot Number"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components_base__WEBPACK_IMPORTED_MODULE_4__["TextInput"], {
    className: "border-gray-400 border pl-2 py-1",
    border: false,
    name: "unit_name",
    width: "w-24"
  }), errors["unit_name"] && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
    className: "px-1 text-red-400 tracking-widest  text-xs "
  }, errors["unit_name"][0]))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "ml-4 flex-1 flex justify-end items-center"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("label", {
    className: "font-semibold w-12"
  }, "Size"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components_base__WEBPACK_IMPORTED_MODULE_4__["TextInput"], {
    className: "border-gray-400 border pl-2 py-1",
    border: false,
    name: "size",
    width: "w-24"
  }), errors["size"] && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
    className: "px-1 text-red-400 tracking-widest  text-xs "
  }, errors["size"][0])))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "flex justify-start my-3"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "flex items-center justify-start py-3"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("label", {
    className: "w-12"
  }, "Price"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "relative"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(react_currency_format__WEBPACK_IMPORTED_MODULE_11___default.a, {
    allowNegative: false,
    className: "border text-sm border-gray-400 px-2 py-1 w-48",
    thousandSeparator: true,
    name: "price",
    id: "price",
    prefix: "$"
  })), errors["price"] && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
    className: "absolute px-1 text-red-400 tracking-widest text-xxs "
  }, errors["price"][0]))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "py-3 flex justify-start items-center ml-10"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("label", {
    className: ""
  }, "Frontage"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "relative"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "ml-4 w-24"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components_base__WEBPACK_IMPORTED_MODULE_4__["TextInput"], {
    className: "pl-2 text-sm border-gray-400 w-32 border py-1",
    border: false // type={`number`}
    ,
    name: "frontage"
  })), errors["frontage"] && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
    className: "absolute px-1 text-red-400 tracking-widest  text-xs "
  }, errors["frontage"][0]))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "py-3 flex justify-start items-center ml-8"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("label", {
    className: ""
  }, "Width"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "relative"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "ml-4 w-24"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components_base__WEBPACK_IMPORTED_MODULE_4__["TextInput"], {
    className: "pl-2 text-sm border-gray-400 w-32 border py-1",
    border: false // type={`number`}
    ,
    name: "width"
  })), errors["width"] && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
    className: "absolute px-1 text-red-400 tracking-widest  text-xs "
  }, errors["width"][0]))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "py-3 flex justify-start items-center ml-8"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("label", {
    className: ""
  }, "Depth "), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "relative"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "ml-4 w-24"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components_base__WEBPACK_IMPORTED_MODULE_4__["TextInput"], {
    className: "pl-2 text-sm border-gray-400 w-32 border py-1 mr-1",
    border: false // type={`number`}
    ,
    name: "depth"
  })), errors["depth"] && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
    className: "absolute px-1 text-red-400 tracking-widest  text-xs "
  }, errors["depth"][0]))))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "py-3"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("label", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "w-full text-sm bg-white text-black "
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_RichTextEditor__WEBPACK_IMPORTED_MODULE_7__["default"], {
    title: "Property Description",
    richEditorState: editorState,
    richEditorStateContent: function richEditorStateContent(content) {
      return setEditorState(content);
    },
    characterLimit: 2000
  }))), errors["description"] && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
    className: "px-1 text-red-400 tracking-widest  text-xs "
  }, errors["description"][0])), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "py-3"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("h1", {
    className: "font-semibold"
  }, " Featured Images (min. 3 images) "), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("label", {
    className: "button ",
    htmlFor: "upload-photos"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: ""
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "w-full flex flex-wrap  cursor-pointer",
    style: {
      border: "2px dashed #ccc",
      borderRadius: "20px",
      padding: "20px",
      textAlign: "center",
      marginBottom: "0px"
    },
    onDrop: function onDrop(e) {
      return handleDrop(e);
    },
    onDragOver: function onDragOver(e) {
      e.preventDefault();
      e.stopPropagation();
    },
    onDragEnter: function onDragEnter(e) {
      e.preventDefault();
      e.stopPropagation();
    },
    onDragLeave: function onDragLeave(e) {
      e.preventDefault();
      e.stopPropagation();
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("input", {
    type: "file",
    id: "upload-photos",
    accept: "image/*",
    multiple: true,
    className: "hidden",
    name: "image[]",
    onChange: function onChange(e) {
      return handleChange(e);
    }
  }), featuredImages.map(function (image, index) {
    return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
      key: index,
      className: "rounded-sm border m-2 relative",
      style: {
        backgroundImage: "url('".concat(image.preview, "')"),
        backgroundPositionX: "center",
        width: "6.9em",
        height: "8.9rem",
        backgroundSize: "cover"
      }
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components_base__WEBPACK_IMPORTED_MODULE_4__["Tooltip"], {
      title: "Remove this item"
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
      onClick: function onClick(e) {
        return handleRemove(e, index);
      },
      className: "absolute bottom-0 right-0 p-2 -mb-2 "
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_fortawesome_react_fontawesome__WEBPACK_IMPORTED_MODULE_12__["FontAwesomeIcon"], {
      className: "text-red-500 shadow-xl hover:font-bold hover:text-red-700",
      icon: _fortawesome_free_solid_svg_icons__WEBPACK_IMPORTED_MODULE_5__["faTimes"]
    }))));
  }), fmPlaceHolder.map(function (p, i) {
    return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
      key: i,
      className: "".concat(p.classes, " cursor-pointer flex justify-center items-center"),
      style: {
        width: "6.9rem",
        height: "8.9rem",
        border: "2px solid #ccc"
      }
    }, " ", /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_fortawesome_react_fontawesome__WEBPACK_IMPORTED_MODULE_12__["FontAwesomeIcon"], {
      className: "text-xl",
      icon: _fortawesome_free_solid_svg_icons__WEBPACK_IMPORTED_MODULE_5__["faCamera"]
    }), " ");
  })), limitError && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
    className: "px-1 text-red-400 tracking-widest  text-xs "
  }, "Please limit your photos to six items only."))), errors["featuredImages"] && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
    className: "px-1 text-red-400 tracking-widest  text-xs "
  }, errors["featuredImages"][0])), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "flex flex-row"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "w-1/2 ml-2"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("h1", {
    className: "font-semibold"
  }, "Rich Text Preview "), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "flex flex-col justify-between py-3"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "flex"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("label", {
    className: "w-full"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "items-center cursor-pointer w-full p-3 rounded-lg flex",
    style: {
      border: "2px dashed #ccc",
      borderRadius: "20px"
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("input", {
    id: "rtp",
    ref: previewImageRef,
    type: "file",
    name: "prev_image",
    onChange: function onChange(e) {
      return handlePreviewImageDisplay(e);
    },
    className: "hidden border text-sm border-gray-400 px-2 py-1 w-full py-1"
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("label", {
    htmlFor: "rtp",
    className: " cursor-pointer hover:text-palette-blue-light ".concat(prevFilename ? "text-palette-blue-light" : "")
  }, prevFilename || "Select File"), prevFilename && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "text-red-500 text-lg cursor-pointer ml-3"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_fortawesome_react_fontawesome__WEBPACK_IMPORTED_MODULE_12__["FontAwesomeIcon"], {
    onClick: function onClick(e) {
      return handleDeletePrevFile(e);
    },
    icon: _fortawesome_free_solid_svg_icons__WEBPACK_IMPORTED_MODULE_5__["faTimes"]
  })))))))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "flex justify-end my-5"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components_base__WEBPACK_IMPORTED_MODULE_4__["Button"], {
    className: "font-bold rounded-full",
    disabled: loading
  }, loading && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_fortawesome_react_fontawesome__WEBPACK_IMPORTED_MODULE_12__["FontAwesomeIcon"], {
    icon: _fortawesome_free_solid_svg_icons__WEBPACK_IMPORTED_MODULE_5__["faSpinner"],
    className: "fa-spin mr-2"
  }), "Save")))));
});

/***/ }),

/***/ "./resources/js/admin/components/AddProjectPart.js":
/*!*********************************************************!*\
  !*** ./resources/js/admin/components/AddProjectPart.js ***!
  \*********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime/regenerator */ "./node_modules/@babel/runtime/regenerator/index.js");
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var react_dropdown__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! react-dropdown */ "./node_modules/react-dropdown/dist/index.js");
/* harmony import */ var react_dropdown__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(react_dropdown__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _helpers_styles_dropdown_css__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../helpers/styles/dropdown.css */ "./resources/js/helpers/styles/dropdown.css");
/* harmony import */ var _helpers_styles_dropdown_css__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(_helpers_styles_dropdown_css__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var _components_base__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../components/_base */ "./resources/js/components/_base/index.js");
/* harmony import */ var _components_base__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(_components_base__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var _RichTextEditor__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./RichTextEditor */ "./resources/js/admin/components/RichTextEditor.js");
/* harmony import */ var draft_js__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! draft-js */ "./node_modules/draft-js/lib/Draft.js");
/* harmony import */ var draft_js__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(draft_js__WEBPACK_IMPORTED_MODULE_6__);
/* harmony import */ var react_datepicker__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! react-datepicker */ "./node_modules/react-datepicker/dist/react-datepicker.min.js");
/* harmony import */ var react_datepicker__WEBPACK_IMPORTED_MODULE_7___default = /*#__PURE__*/__webpack_require__.n(react_datepicker__WEBPACK_IMPORTED_MODULE_7__);
/* harmony import */ var _helpers_styles_datepicker_css__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../../helpers/styles/datepicker.css */ "./resources/js/helpers/styles/datepicker.css");
/* harmony import */ var _helpers_styles_datepicker_css__WEBPACK_IMPORTED_MODULE_8___default = /*#__PURE__*/__webpack_require__.n(_helpers_styles_datepicker_css__WEBPACK_IMPORTED_MODULE_8__);
/* harmony import */ var draft_convert__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! draft-convert */ "./node_modules/draft-convert/esm/index.js");
/* harmony import */ var _helpers_propertyHelper_propertyHelper__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ../../helpers/propertyHelper/propertyHelper */ "./resources/js/helpers/propertyHelper/propertyHelper.js");
/* harmony import */ var _fortawesome_react_fontawesome__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! @fortawesome/react-fontawesome */ "./node_modules/@fortawesome/react-fontawesome/index.es.js");
/* harmony import */ var _fortawesome_free_regular_svg_icons__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! @fortawesome/free-regular-svg-icons */ "./node_modules/@fortawesome/free-regular-svg-icons/index.es.js");
/* harmony import */ var _fortawesome_free_solid_svg_icons__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! @fortawesome/free-solid-svg-icons */ "./node_modules/@fortawesome/free-solid-svg-icons/index.es.js");
/* harmony import */ var _states_userGlobal__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ../../states/userGlobal */ "./resources/js/states/userGlobal.js");
/* harmony import */ var _data_index__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! ../../data/index */ "./resources/js/data/index.js");
/* harmony import */ var _components_base_alerts_sweetAlert__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! ../../components/_base/alerts/sweetAlert */ "./resources/js/components/_base/alerts/sweetAlert.js");
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! lodash */ "./node_modules/lodash/lodash.js");
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_17___default = /*#__PURE__*/__webpack_require__.n(lodash__WEBPACK_IMPORTED_MODULE_17__);
/* harmony import */ var _helpers_numberInput__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(/*! ../../helpers/numberInput */ "./resources/js/helpers/numberInput.js");
/* harmony import */ var _helpers_mapCoordinateHelper__WEBPACK_IMPORTED_MODULE_19__ = __webpack_require__(/*! ../../helpers/mapCoordinateHelper */ "./resources/js/helpers/mapCoordinateHelper.js");
/* harmony import */ var _components_AddDeveloperModal__WEBPACK_IMPORTED_MODULE_20__ = __webpack_require__(/*! ../components/AddDeveloperModal */ "./resources/js/admin/components/AddDeveloperModal.js");
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_21__ = __webpack_require__(/*! moment */ "./node_modules/moment/moment.js");
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_21___default = /*#__PURE__*/__webpack_require__.n(moment__WEBPACK_IMPORTED_MODULE_21__);
/* harmony import */ var _helpers_fileHelper_fileHelper__WEBPACK_IMPORTED_MODULE_22__ = __webpack_require__(/*! ../../helpers/fileHelper/fileHelper */ "./resources/js/helpers/fileHelper/fileHelper.js");


function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

function _toConsumableArray(arr) { return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _unsupportedIterableToArray(arr) || _nonIterableSpread(); }

function _nonIterableSpread() { throw new TypeError("Invalid attempt to spread non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _iterableToArray(iter) { if (typeof Symbol !== "undefined" && Symbol.iterator in Object(iter)) return Array.from(iter); }

function _arrayWithoutHoles(arr) { if (Array.isArray(arr)) return _arrayLikeToArray(arr); }

function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _unsupportedIterableToArray(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

function _iterableToArrayLimit(arr, i) { if (typeof Symbol === "undefined" || !(Symbol.iterator in Object(arr))) return; var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }



























/* harmony default export */ __webpack_exports__["default"] = (function (_ref) {
  var toggleAddProjectModal = _ref.toggleAddProjectModal;

  var _UserGlobal = Object(_states_userGlobal__WEBPACK_IMPORTED_MODULE_14__["default"])(),
      _UserGlobal2 = _slicedToArray(_UserGlobal, 2),
      userState = _UserGlobal2[0],
      userActions = _UserGlobal2[1];

  var _useState = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(new Date(Date.now())),
      _useState2 = _slicedToArray(_useState, 2),
      date = _useState2[0],
      setDate = _useState2[1];

  var _useState3 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])([]),
      _useState4 = _slicedToArray(_useState3, 2),
      featuredImages = _useState4[0],
      setFeaturedImages = _useState4[1];

  var _useState5 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(""),
      _useState6 = _slicedToArray(_useState5, 2),
      projectState = _useState6[0],
      setProjectState = _useState6[1];

  var _useState7 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(""),
      _useState8 = _slicedToArray(_useState7, 2),
      displaySuiteState = _useState8[0],
      setDisplaySuiteState = _useState8[1];

  var _useState9 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(""),
      _useState10 = _slicedToArray(_useState9, 2),
      propertyType = _useState10[0],
      setPropertyType = _useState10[1];

  var _useState11 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(0),
      _useState12 = _slicedToArray(_useState11, 2),
      deposit = _useState12[0],
      setDeposit = _useState12[1];

  var _useState13 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(false),
      _useState14 = _slicedToArray(_useState13, 2),
      toogleAddEditDeveloper = _useState14[0],
      setToogleAddEditDeveloper = _useState14[1];

  var _useState15 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])([]),
      _useState16 = _slicedToArray(_useState15, 2),
      errors = _useState16[0],
      setErrors = _useState16[1];

  var _useState17 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(function () {
    return draft_js__WEBPACK_IMPORTED_MODULE_6__["EditorState"].createEmpty();
  }),
      _useState18 = _slicedToArray(_useState17, 2),
      editorState = _useState18[0],
      setEditorState = _useState18[1];

  var _useState19 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])("Australia"),
      _useState20 = _slicedToArray(_useState19, 2),
      projectCountry = _useState20[0],
      setProjectCountry = _useState20[1];

  var _useState21 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])("Australia"),
      _useState22 = _slicedToArray(_useState21, 2),
      displaySuiteCountry = _useState22[0],
      setDisplaySuiteCountry = _useState22[1];

  var _useState23 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])([]),
      _useState24 = _slicedToArray(_useState23, 2),
      fmPlaceHolder = _useState24[0],
      setFmPlaceHolder = _useState24[1];

  var _useState25 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(false),
      _useState26 = _slicedToArray(_useState25, 2),
      limitError = _useState26[0],
      setLimitError = _useState26[1];

  var addProjectForm = Object(react__WEBPACK_IMPORTED_MODULE_1__["useRef"])(null);
  var countries = [{
    label: "Australia",
    value: "Australia"
  }];

  var _useState27 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(userState.projectData),
      _useState28 = _slicedToArray(_useState27, 2),
      project = _useState28[0],
      setProject = _useState28[1];

  var _useState29 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(false),
      _useState30 = _slicedToArray(_useState29, 2),
      sameAddress = _useState30[0],
      setSame = _useState30[1];

  var _useState31 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(""),
      _useState32 = _slicedToArray(_useState31, 2),
      developerValue = _useState32[0],
      setDeveloperValue = _useState32[1];

  var _useState33 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])({}),
      _useState34 = _slicedToArray(_useState33, 2),
      projectDevelopers = _useState34[0],
      setProjectDevelopers = _useState34[1];

  var _useState35 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(false),
      _useState36 = _slicedToArray(_useState35, 2),
      loading = _useState36[0],
      setLoading = _useState36[1];

  var _useState37 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(""),
      _useState38 = _slicedToArray(_useState37, 2),
      discount = _useState38[0],
      setDiscount = _useState38[1];

  var _useState39 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(0),
      _useState40 = _slicedToArray(_useState39, 2),
      timeLimitOptionValue = _useState40[0],
      setTimeLimitOptionValue = _useState40[1];

  var _useState41 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(0),
      _useState42 = _slicedToArray(_useState41, 2),
      timeLimitValue = _useState42[0],
      setTimeLimitValue = _useState42[1];

  var _useState43 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(""),
      _useState44 = _slicedToArray(_useState43, 2),
      lat = _useState44[0],
      setLat = _useState44[1];

  var _useState45 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(""),
      _useState46 = _slicedToArray(_useState45, 2),
      lng = _useState46[0],
      setLng = _useState46[1];

  var _useState47 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(""),
      _useState48 = _slicedToArray(_useState47, 2),
      filename = _useState48[0],
      setFilename = _useState48[1];

  var _useState49 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])([]),
      _useState50 = _slicedToArray(_useState49, 2),
      previewImage = _useState50[0],
      setPreviewImage = _useState50[1];

  var _useState51 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(false),
      _useState52 = _slicedToArray(_useState51, 2),
      isProjectLand = _useState52[0],
      setIsProjectLand = _useState52[1];

  var previewImageRef = Object(react__WEBPACK_IMPORTED_MODULE_1__["useRef"])(null);
  var time_limit_options = [{
    label: "Please select...",
    value: 0
  }, {
    label: "Hour/s",
    value: "1"
  }, {
    label: "Day/s",
    value: "2"
  }, {
    label: "Week/s",
    value: "3"
  }, {
    label: "Month/s",
    value: "4"
  }, {
    label: "Year/s",
    value: "5"
  }];

  var _useState53 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(false),
      _useState54 = _slicedToArray(_useState53, 2),
      completedNow = _useState54[0],
      setCompletedNow = _useState54[1];

  Object(react__WEBPACK_IMPORTED_MODULE_1__["useEffect"])(function () {
    var phs = [];

    for (var index = 0; index < 12 - featuredImages.length; index++) {
      var ph = {
        classes: "cursor-pointer m-2 inline-flex bg-transparent text-palette-gray"
      };
      phs.push(ph);
    }

    setFmPlaceHolder(phs);
  }, [featuredImages.length]);
  Object(react__WEBPACK_IMPORTED_MODULE_1__["useEffect"])(function () {
    setEditorState(function () {
      return draft_js__WEBPACK_IMPORTED_MODULE_6__["EditorState"].push(editorState, Object(draft_convert__WEBPACK_IMPORTED_MODULE_9__["convertFromHTML"])(userState.projectData.description ? userState.projectData.description : ""));
    });
  }, []);
  Object(react__WEBPACK_IMPORTED_MODULE_1__["useEffect"])(function () {
    _getDevelopers();
  }, []);

  var _getDevelopers = function getDevelopers() {
    var url = "".concat(window.location.origin, "/api/user");
    Object(_data_index__WEBPACK_IMPORTED_MODULE_15__["developers"])(url).then(function (result) {
      setProjectDevelopers(_toConsumableArray(result.data.map(function (developer) {
        return {
          label: "".concat(developer.first_name, " ").concat(developer.last_name),
          value: String(developer.id)
        };
      })));
    })["catch"](function (error) {});
  };

  var toogleAddUpdateDeveloper = function toogleAddUpdateDeveloper() {
    setToogleAddEditDeveloper(!toogleAddEditDeveloper);
  };

  var handleSubmit = /*#__PURE__*/function () {
    var _ref2 = _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee(e) {
      return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee$(_context) {
        while (1) {
          switch (_context.prev = _context.next) {
            case 0:
              e.preventDefault();

              if (!(discount && discount < 10)) {
                _context.next = 3;
                break;
              }

              return _context.abrupt("return", Object(_components_base_alerts_sweetAlert__WEBPACK_IMPORTED_MODULE_16__["sweetAlert"])("error", "Project discount should not be less than 10 percent."));

            case 3:
              if (!(discount && discount > 100)) {
                _context.next = 5;
                break;
              }

              return _context.abrupt("return", Object(_components_base_alerts_sweetAlert__WEBPACK_IMPORTED_MODULE_16__["sweetAlert"])("error", "Project discount should not be more than 100 percent."));

            case 5:
              setLimitError(false);
              saveProject();

            case 7:
            case "end":
              return _context.stop();
          }
        }
      }, _callee);
    }));

    return function handleSubmit(_x) {
      return _ref2.apply(this, arguments);
    };
  }();

  var saveProject = /*#__PURE__*/function () {
    var _ref3 = _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee2() {
      var formData, url;
      return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee2$(_context2) {
        while (1) {
          switch (_context2.prev = _context2.next) {
            case 0:
              setLoading(true);
              formData = new FormData(addProjectForm.current);
              formData.append("role", "project_developer");
              formData.append("propertyType", propertyType);
              formData.append("project_address_state", projectState);
              formData.append("project_address_country", projectCountry);
              formData.append("display_suite_address_state", displaySuiteState);
              formData.append("display_suite_address_country", displaySuiteCountry);
              formData.append("time_limit[optionVal]", timeLimitValue);
              formData.append("time_limit[option]", timeLimitOptionValue);
              formData.append("time_limit[createdAt]", 0);
              formData.append("developer", developerValue);
              formData.append("lat", lat);
              formData.append("long", lng);
              formData.append("deposit", deposit);
              formData.append("project_discount", discount || 0);
              formData.append("same_address", sameAddress);
              formData.append("isCompleted", completedNow);
              formData.append("isPropertyTypeLand", isProjectLand);
              formData.append("description", editorState.getCurrentContent().getPlainText());
              formData.append("descriptionHTML", Object(draft_convert__WEBPACK_IMPORTED_MODULE_9__["convertToHTML"])(editorState.getCurrentContent()));
              featuredImages.map(function (image, key) {
                formData.append("featuredImages[".concat(key, "]"), image.image);
              });

              if (previewImage.length) {
                formData.append("preview_img[".concat(0, "]"), previewImage[0].image);
              }

              url = "/api/deal/";
              _context2.next = 26;
              return saveDeal({
                formData: formData,
                url: url
              });

            case 26:
            case "end":
              return _context2.stop();
          }
        }
      }, _callee2);
    }));

    return function saveProject() {
      return _ref3.apply(this, arguments);
    };
  }();

  var saveDeal = /*#__PURE__*/function () {
    var _ref5 = _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee3(_ref4) {
      var formData, url;
      return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee3$(_context3) {
        while (1) {
          switch (_context3.prev = _context3.next) {
            case 0:
              formData = _ref4.formData, url = _ref4.url;
              _context3.next = 3;
              return getDealCoordinates({
                formData: formData
              });

            case 3:
              _context3.next = 5;
              return Object(_data_index__WEBPACK_IMPORTED_MODULE_15__["saveUpdateDeal"])({
                formData: formData,
                url: url
              }).then(function (r) {
                if (r.status === 200) {
                  Object(_components_base_alerts_sweetAlert__WEBPACK_IMPORTED_MODULE_16__["sweetAlert"])("success", "Project successfully saved.");
                  setLoading(false);
                  userActions.setState({
                    projectData: r.data,
                    currentProjStep: 2
                  });
                }
              })["catch"](function (error) {
                if (error.response.data.errors) {
                  Object(_components_base_alerts_sweetAlert__WEBPACK_IMPORTED_MODULE_16__["sweetAlert"])("error", "Some fields are missing!");
                }

                setLoading(false);
                setErrors(error.response.data.errors);
              }).then(function () {});

            case 5:
            case "end":
              return _context3.stop();
          }
        }
      }, _callee3);
    }));

    return function saveDeal(_x2) {
      return _ref5.apply(this, arguments);
    };
  }();

  var getDealCoordinates = /*#__PURE__*/function () {
    var _ref7 = _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee4(_ref6) {
      var formData, address, suburb, addressParam, data, _address, _suburb, _addressParam, _data;

      return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee4$(_context4) {
        while (1) {
          switch (_context4.prev = _context4.next) {
            case 0:
              formData = _ref6.formData;
              address = formData.get("project_address_line_1");
              suburb = formData.get("project_address_suburb");
              addressParam = address.concat(" ", suburb).concat(" ", projectState).concat(" ", projectCountry);
              _context4.next = 6;
              return Object(_helpers_mapCoordinateHelper__WEBPACK_IMPORTED_MODULE_19__["getMapCoordinates"])(addressParam);

            case 6:
              data = _context4.sent;
              formData.append("long", data["long"]);
              formData.append("lat", data.lat);

              if (sameAddress) {
                _context4.next = 18;
                break;
              }

              _address = formData.get("display_suite_address_line_1");
              _suburb = formData.get("display_suite_address_suburb");
              _addressParam = _address.concat(" ", _suburb).concat(" ", displaySuiteState).concat(" ", displaySuiteCountry);
              _context4.next = 15;
              return Object(_helpers_mapCoordinateHelper__WEBPACK_IMPORTED_MODULE_19__["getMapCoordinates"])(_addressParam);

            case 15:
              _data = _context4.sent;
              formData.append("display_suite_long", _data["long"]);
              formData.append("display_suite_lat", _data.lat);

            case 18:
            case "end":
              return _context4.stop();
          }
        }
      }, _callee4);
    }));

    return function getDealCoordinates(_x3) {
      return _ref7.apply(this, arguments);
    };
  }();

  var handleDrop = function handleDrop(e) {
    e.preventDefault();
    e.stopPropagation();
    var fmCount = featuredImages.length;
    var onCount = e.dataTransfer.files.length;

    if (fmCount + onCount > 12) {
      e.target.value = null;
      setLimitError(true);
      return;
    } else {
      setLimitError(false);
    }

    setFeaturedImages(featuredImages.concat(_toConsumableArray(_toConsumableArray(e.dataTransfer.files).map(function (file) {
      var newFilename = Object(_helpers_fileHelper_fileHelper__WEBPACK_IMPORTED_MODULE_22__["sanitizeFilename"])(file.name);
      var newFileObj = new File([file], newFilename);
      return {
        image: newFileObj,
        preview: URL.createObjectURL(newFileObj)
      };
    }))));
    e.target.value = null;
  };

  var handleChange = function handleChange(e) {
    var fmCount = featuredImages.length;
    var onCount = e.target.files.length;

    if (fmCount + onCount > 12) {
      setLimitError(true);
      e.target.value = null;
      return;
    } else {
      setLimitError(false);
    }

    setFeaturedImages(featuredImages.concat(_toConsumableArray(_toConsumableArray(e.target.files).map(function (file) {
      var newFilename = Object(_helpers_fileHelper_fileHelper__WEBPACK_IMPORTED_MODULE_22__["sanitizeFilename"])(file.name);
      var newFileObj = new File([file], newFilename);
      return {
        image: newFileObj,
        preview: URL.createObjectURL(newFileObj)
      };
    }))));
    e.target.value = null;
  };

  var handlePreviewImageDisplay = function handlePreviewImageDisplay(e) {
    if (e.target.files.length > 1) {
      Object(_components_base_alerts_sweetAlert__WEBPACK_IMPORTED_MODULE_16__["sweetAlert"])("error", "Multiple upload is not allowed!");
      return;
    }

    if (!Object(_helpers_fileHelper_fileHelper__WEBPACK_IMPORTED_MODULE_22__["imageFileTypeFilter"])(e.target.files[0]["name"])) {
      Object(_components_base_alerts_sweetAlert__WEBPACK_IMPORTED_MODULE_16__["sweetAlert"])("error", "Filetype is not supported!");
      e.target.value = null;
      return;
    }

    setPreviewImage(previewImage.concat(_toConsumableArray(_toConsumableArray(e.target.files).map(function (file) {
      var newFilename = Object(_helpers_fileHelper_fileHelper__WEBPACK_IMPORTED_MODULE_22__["sanitizeFilename"])(file.name);
      var newFileObj = new File([file], newFilename);
      setFilename(newFilename);
      return {
        image: newFileObj,
        preview: URL.createObjectURL(newFileObj)
      };
    }))));
  };

  var handleDeleteFile = function handleDeleteFile(e) {
    e.preventDefault();
    setPreviewImage([]);
    previewImageRef.current.type = "";
    previewImageRef.current.type = "file";
    setFilename("");
    e.target.value = null;
  };

  var handleRemove = function handleRemove(e, i) {
    e.preventDefault();
    e.stopPropagation();
    setFeaturedImages(_toConsumableArray(featuredImages.filter(function (image, k) {
      if (i !== k) {
        return image;
      }
    })));

    if (featuredImages.length > 12) {
      setLimitError(true);
    } else {
      setLimitError(false);
    }
  };

  var compareDate = function compareDate(selectedDate) {
    var date = moment__WEBPACK_IMPORTED_MODULE_21___default()(selectedDate, "DD-MM-YYYY"); //Date format

    var currDate = moment__WEBPACK_IMPORTED_MODULE_21___default()();
    var duration = date.diff(currDate, "hours");

    if (duration < 0) {
      return false;
    }

    return true;
  };

  var setSelctedSettlementDate = function setSelctedSettlementDate(date) {
    if (isProjectLand) {
      setLandRegistrationDate(date);
    } else {
      if (!compareDate(date)) {
        Object(_components_base_alerts_sweetAlert__WEBPACK_IMPORTED_MODULE_16__["sweetAlert"])("error", "Please select future date!");
      } else {
        setDate(date);
      }
    }
  };

  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "bg-white rounded-lg py-5 px-8 m-5"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("form", {
    ref: addProjectForm,
    onSubmit: handleSubmit
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "text-black"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "flex items-center"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "flex items-center"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("label", {
    className: "block py-3 font-semibold w-40"
  }, "Project Developer"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "relative"
  }, !Object(lodash__WEBPACK_IMPORTED_MODULE_17__["isEmpty"])(projectDevelopers) ? /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(react_dropdown__WEBPACK_IMPORTED_MODULE_2___default.a, {
    id: "project_developer",
    value: developerValue,
    options: projectDevelopers,
    onChange: function onChange(option) {
      setDeveloperValue(option.value);
    },
    placeholder: "Please select...",
    name: "project_developer"
  }) : /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_fortawesome_react_fontawesome__WEBPACK_IMPORTED_MODULE_11__["FontAwesomeIcon"], {
    icon: _fortawesome_free_solid_svg_icons__WEBPACK_IMPORTED_MODULE_13__["faSpinner"],
    className: "fa-spin mr-2"
  })), errors["developer"] && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
    className: "absolute px-1 text-red-400 tracking-widest  text-xs "
  }, errors["developer"][0])), !loading && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "flex"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "flex justify-end ml-10"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
    className: "font-semibold text-palette-purple cursor-pointer",
    disabled: loading,
    onClick: function onClick() {
      return toogleAddUpdateDeveloper();
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_fortawesome_react_fontawesome__WEBPACK_IMPORTED_MODULE_11__["FontAwesomeIcon"], {
    icon: _fortawesome_free_solid_svg_icons__WEBPACK_IMPORTED_MODULE_13__["faPlus"],
    className: "fa-plus mr-2"
  }), "Add Project Developer")))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "py-3 flex items-center "
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("label", {
    className: "font-semibold w-1/5",
    htmlFor: "name"
  }, "Project Name"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "flex-1 relative"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components_base__WEBPACK_IMPORTED_MODULE_4__["TextInput"], {
    id: "name",
    className: "pl-2 text-sm border-gray-400 border py-1",
    border: false,
    name: "name"
  }), errors["name"] && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
    className: "absolute px-1 text-red-400 tracking-widest  text-xs "
  }, errors["name"][0]))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "py-3"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("label", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "w-full text-sm bg-white text-black text-sm "
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_RichTextEditor__WEBPACK_IMPORTED_MODULE_5__["default"], {
    title: "Project Description",
    richEditorState: editorState,
    richEditorStateContent: function richEditorStateContent(content) {
      return setEditorState(content);
    },
    characterLimit: 2000
  }))), errors["description"] && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
    className: "px-1 text-red-400 tracking-widest  text-xs "
  }, errors["description"][0])), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "flex justify-between"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "flex flex-start items-center"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("label", {
    className: "block py-3 w-32"
  }, "Project Type"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "relative"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(react_dropdown__WEBPACK_IMPORTED_MODULE_2___default.a, {
    id: "property_type",
    value: propertyType,
    options: _helpers_propertyHelper_propertyHelper__WEBPACK_IMPORTED_MODULE_10__["property_types"],
    onChange: function onChange(option) {
      if (option.label === "Land") {
        setIsProjectLand(true);
      } else {
        setIsProjectLand(false);
      }

      setPropertyType(option.value);
    },
    placeholder: "Please select...",
    name: "property_type"
  }), errors["propertyType"] && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
    className: "absolute px-1 text-red-400 tracking-widest  text-xs "
  }, errors["propertyType"][0]))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "px-10 flex flex-start items-center "
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("label", {
    className: "block pr-5 py-3"
  }, "Discount"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "relative"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "flex items-center"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components_base__WEBPACK_IMPORTED_MODULE_4__["TextInput"], {
    className: "pl-2 text-sm border-gray-400 w-32 border py-1",
    border: false,
    type: "number",
    value: discount,
    onChange: function onChange(e) {
      return Object(_helpers_numberInput__WEBPACK_IMPORTED_MODULE_18__["inputRegex"])(e.target.value) ? setDiscount(e.target.value) : null;
    }
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
    className: "font-extrabold pl-3"
  }, " % ")), errors["project_discount"] && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
    className: "absolute px-1 text-red-400 tracking-widest  text-xs "
  }, errors["project_discount"][0])))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "flex flex-start items-center mt-3"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("label", {
    className: "block py-3 w-32"
  }, "Deposit"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "relative flex items-center"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(react_dropdown__WEBPACK_IMPORTED_MODULE_2___default.a, {
    id: "deposit",
    value: deposit,
    options: [{
      label: "5",
      value: "5"
    }, {
      label: "10",
      value: "10"
    }],
    onChange: function onChange(option) {
      setDeposit(option.value);
    },
    placeholder: "Please select...",
    name: "deposit"
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
    className: "font-extrabold pl-3"
  }, " % "), errors["deposit"] && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
    className: "absolute px-1 text-red-400 tracking-widest  text-xs "
  }, errors["deposit"][0]))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("h1", {
    className: "font-semibold mt-5"
  }, "Project Address"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "w-full"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "py-3 flex w-full items-center"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("label", {
    className: "w-32"
  }, "Address Line 1"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: " flex-1 relative"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components_base__WEBPACK_IMPORTED_MODULE_4__["TextInput"], {
    className: "border-gray-400 border pl-2 text-sm py-1",
    border: false,
    name: "project_address_line_1"
  }), errors["project_address_line_1"] && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
    className: "absolute px-1 text-red-400 tracking-widest  text-xs "
  }, errors["project_address_line_1"][0]))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "py-3 flex justify-between"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "flex items-center"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("label", {
    className: "w-32 block py-3"
  }, "Suburb "), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    style: {
      width: "320px"
    },
    className: "flex-1 relative"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components_base__WEBPACK_IMPORTED_MODULE_4__["TextInput"], {
    className: " border-gray-400 border py-1 pl-2",
    border: false,
    name: "project_address_suburb"
  }), errors["project_address_suburb"] && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
    className: "absolute px-1 text-red-400 tracking-widest  text-xs "
  }, errors["project_address_suburb"][0]))), !isProjectLand && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "ml-5 flex items-center"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("label", {
    className: "w-32 block py-3"
  }, "City "), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "relative"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components_base__WEBPACK_IMPORTED_MODULE_4__["TextInput"], {
    className: " border-gray-400 border pl-2 py-1",
    border: false,
    name: "project_address_city"
  }), errors["project_address_city"] && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
    className: "absolute px-1 text-red-400 tracking-widest  text-xs "
  }, errors["project_address_city"][0])))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "py-3 flex justify-between"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "py-3 flex items-center"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("label", {
    className: "w-32 block py-3"
  }, "State "), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "relative"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(react_dropdown__WEBPACK_IMPORTED_MODULE_2___default.a, {
    id: "project_address_state",
    value: projectState,
    options: _helpers_propertyHelper_propertyHelper__WEBPACK_IMPORTED_MODULE_10__["states"],
    onChange: function onChange(option) {
      setProjectState(option.value);
    },
    placeholder: "Select state",
    name: "project_address_state"
  }), errors["project_address_state"] && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
    className: "absolute px-1 text-red-400 tracking-widest  text-xs "
  }, errors["project_address_state"][0]))), !isProjectLand && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "ml-5 flex items-center"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("label", {
    className: "w-32 block py-3"
  }, "Postcode/ZIP "), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "relative"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components_base__WEBPACK_IMPORTED_MODULE_4__["TextInput"], {
    className: " border-gray-400 border pl-2 py-1",
    border: false,
    name: "project_address_zip"
  }), errors["project_address_zip"] && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
    className: "absolute px-1 text-red-400 tracking-widest  text-xs "
  }, errors["project_address_zip"][0])))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "py-3 flex items-center"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("label", {
    className: "w-32 block py-3"
  }, "Country "), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "relative"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(react_dropdown__WEBPACK_IMPORTED_MODULE_2___default.a, {
    id: "project_address_country",
    value: projectCountry,
    options: countries,
    onChange: function onChange(option) {
      setProjectCountry(option.value);
    },
    name: "project_address_country"
  }), errors["project_address_country"] && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
    className: "absolute px-1 text-red-400 tracking-widest  text-xs "
  }, errors["project_address_country"][0])))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "flex justify-between mt-5 items-center"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("h1", {
    className: "font-semibold"
  }, isProjectLand ? "Office" : "Display Suite", " Address"), " ", /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "flex items-center"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("input", {
    className: "",
    type: "checkbox",
    name: "same_address_check",
    id: "same_address",
    onChange: function onChange(e) {
      e.target.checked ? setSame(true) : setSame(false);
    }
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
    className: "p-1"
  }, isProjectLand ? "Office" : "Display Suite", " Address is same as Project Address"))), !sameAddress && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "py-3 flex w-full items-center"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("label", {
    className: "w-32"
  }, "Address Line 1"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "flex-1 relativ"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components_base__WEBPACK_IMPORTED_MODULE_4__["TextInput"], {
    className: "pl-2 text-sm border-gray-400 border py-1",
    border: false,
    name: "display_suite_address_line_1"
  }), errors["display_suite_address_line_1"] && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
    className: "absolute px-1 text-red-400 tracking-widest  text-xs "
  }, errors["display_suite_address_line_1"][0]))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "py-3 flex justify-between"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "flex items-center"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("label", {
    className: "w-32 block py-3"
  }, "Suburb "), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    style: {
      width: "320px"
    },
    className: "flex-1 relative"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components_base__WEBPACK_IMPORTED_MODULE_4__["TextInput"], {
    className: " border-gray-400 border py-1 pl-2",
    border: false,
    name: "display_suite_address_suburb"
  }), errors["display_suite_address_suburb"] && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
    className: "absolute px-1 text-red-400 tracking-widest  text-xs "
  }, errors["display_suite_address_suburb"][0]))), !isProjectLand && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "ml-5 flex items-center"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("label", {
    className: "w-32 block py-3"
  }, "City "), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "relative"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components_base__WEBPACK_IMPORTED_MODULE_4__["TextInput"], {
    className: " border-gray-400 border pl-2 py-1",
    border: false,
    name: "display_suite_address_city"
  }), errors["display_suite_address_city"] && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
    className: "absolute px-1 text-red-400 tracking-widest  text-xs "
  }, errors["display_suite_address_city"][0])))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "py-3 flex justify-between"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "py-3 flex items-center"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("label", {
    className: "w-32 block py-3"
  }, "State "), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "relative"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(react_dropdown__WEBPACK_IMPORTED_MODULE_2___default.a, {
    id: "display_suite_address_state",
    value: displaySuiteState,
    options: _helpers_propertyHelper_propertyHelper__WEBPACK_IMPORTED_MODULE_10__["states"],
    onChange: function onChange(option) {
      setDisplaySuiteState(option.value);
    },
    placeholder: "Select state",
    name: "display_suite_address_state"
  }), errors["display_suite_address_state"] && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
    className: "absolute px-1 text-red-400 tracking-widest  text-xs "
  }, errors["display_suite_address_state"][0]))), !isProjectLand && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "ml-5 flex items-center"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("label", {
    className: "w-32 block py-3"
  }, "Postcode/ZIP "), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "relative"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components_base__WEBPACK_IMPORTED_MODULE_4__["TextInput"], {
    className: " border-gray-400 border pl-2 py-1",
    border: false,
    name: "display_suite_address_zip"
  }), errors["display_suite_address_zip"] && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
    className: "absolute px-1 text-red-400 tracking-widest  text-xs "
  }, errors["display_suite_address_zip"][0])))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "py-3 flex items-center"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("label", {
    className: "w-32 block py-3"
  }, "Country "), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "relative"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(react_dropdown__WEBPACK_IMPORTED_MODULE_2___default.a, {
    id: "display_suite_address_country",
    value: displaySuiteCountry,
    options: countries,
    onChange: function onChange(option) {
      setDisplaySuiteCountry(option.value);
    },
    name: "display_suite_address_country"
  }), errors["display_suite_address_country"] && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
    className: "absolute px-1 text-red-400 tracking-widest  text-xs "
  }, errors["display_suite_address_country"][0])))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "py-3 flex"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "flex-1"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "cursor-pointer flex-1"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("h1", {
    className: "font-semibold mb-4"
  }, "Estimated Completion/", isProjectLand ? "Land Registration" : "Settlement"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_fortawesome_react_fontawesome__WEBPACK_IMPORTED_MODULE_11__["FontAwesomeIcon"], {
    className: "mr-2",
    icon: _fortawesome_free_regular_svg_icons__WEBPACK_IMPORTED_MODULE_12__["faCalendarCheck"]
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(react_datepicker__WEBPACK_IMPORTED_MODULE_7___default.a, {
    className: "py-1 px-2",
    id: "proposed_settlement",
    name: "proposed_settlement",
    showMonthDropdown: true,
    showYearDropdown: true,
    dateFormat: "PP",
    selected: date,
    disabled: completedNow,
    onSelect: function onSelect(date) {
      setSelctedSettlementDate(date);
    },
    onChange: function onChange(date) {
      setSelctedSettlementDate(date);
    }
  })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "p-1 flex items-center mt-3"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("input", {
    type: "checkbox",
    id: "completed_now",
    onChange: function onChange(e) {
      setCompletedNow(e.target.checked);
    }
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
    className: "p-1"
  }, "Completed Now"))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "cursor-pointer flex-1"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("h1", {
    className: "font-semibold mb-4"
  }, "Project Time Limit"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "relative flex"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "pr-6"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(react_dropdown__WEBPACK_IMPORTED_MODULE_2___default.a, {
    className: "project_time_limit",
    id: "time_limit_option",
    value: timeLimitOptionValue,
    options: time_limit_options,
    placeholder: "Please select...",
    onChange: function onChange(option) {
      setTimeLimitOptionValue(option.value);
    },
    name: "time_limit_option_value"
  }), errors["timeLimitValue"] && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
    className: "absolute px-1 text-red-400 tracking-widest  text-xs "
  }, errors["timeLimitValue"][0])), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components_base__WEBPACK_IMPORTED_MODULE_4__["TextInput"], {
    className: "pl-2 text-sm border-gray-400 w-32 border py-1",
    border: false,
    value: timeLimitValue,
    onChange: function onChange(e) {
      return Object(_helpers_numberInput__WEBPACK_IMPORTED_MODULE_18__["inputRegex"])(e.target.value) ? setTimeLimitValue(e.target.value) : null;
    } //name="time_limit_value"

  }), errors["time_limit_value"] && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
    className: "absolute px-1 text-red-400 tracking-widest  text-xs "
  }, errors["time_limit_value"][0]))))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "py-3"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("h1", {
    className: "font-semibold"
  }, " Featured Images (min. 3 images) "), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("label", {
    className: "button ",
    htmlFor: "upload-photos"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: ""
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "w-full flex flex-wrap  cursor-pointer",
    style: {
      border: "2px dashed #ccc",
      borderRadius: "20px",
      padding: "20px",
      textAlign: "center",
      marginBottom: "0px"
    },
    onDrop: function onDrop(e) {
      return handleDrop(e);
    },
    onDragOver: function onDragOver(e) {
      e.preventDefault();
      e.stopPropagation();
    },
    onDragEnter: function onDragEnter(e) {
      e.preventDefault();
      e.stopPropagation();
    },
    onDragLeave: function onDragLeave(e) {
      e.preventDefault();
      e.stopPropagation();
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("input", {
    type: "file",
    id: "upload-photos",
    accept: "image/*",
    multiple: true,
    className: "hidden",
    name: "image[]",
    onChange: function onChange(e) {
      return handleChange(e);
    }
  }), featuredImages.map(function (image, index) {
    return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
      key: index,
      className: "rounded-sm border m-2 relative",
      style: {
        backgroundImage: "url('".concat(image.preview, "')"),
        backgroundPositionX: "center",
        width: "6.9em",
        height: "8.9rem",
        backgroundSize: "cover"
      }
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
      className: "absolute bg-palette-blue-light font-bold leading-none ml-1 mt-1 rounded-full text-white w-1/4"
    }, index + 1), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components_base__WEBPACK_IMPORTED_MODULE_4__["Tooltip"], {
      title: "Remove this item"
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
      onClick: function onClick(e) {
        return handleRemove(e, index);
      },
      className: "absolute bottom-0 right-0 p-2 -mb-2 "
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_fortawesome_react_fontawesome__WEBPACK_IMPORTED_MODULE_11__["FontAwesomeIcon"], {
      className: "text-red-500 shadow-xl hover:font-bold hover:text-red-700",
      icon: _fortawesome_free_solid_svg_icons__WEBPACK_IMPORTED_MODULE_13__["faTimes"]
    }))));
  }), fmPlaceHolder.map(function (p, index) {
    return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
      key: index,
      className: "".concat(p.classes, " relative cursor-pointer flex justify-center items-center"),
      style: {
        width: "6.9rem",
        height: "8.9rem",
        border: "2px solid #ccc"
      }
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
      className: "absolute font-bold leading-none w-1/4"
    }, index + featuredImages.length + 1));
  })), limitError && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
    className: "px-1 text-red-400 tracking-widest  text-xs "
  }, "Please limit your photos to twelve(12) items only."))), errors["featuredImages"] && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
    className: "px-1 text-red-400 tracking-widest  text-xs "
  }, errors["featuredImages"][0])), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "flex flex-col mb-10"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "flex-1"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("h1", {
    className: "font-semibold"
  }, "Rich Preview Image "), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "py-3 flex flex-col"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "flex"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("label", {
    className: "w-full"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "cursor-pointer w-full p-3 rounded-lg flex",
    style: {
      border: "2px dashed #ccc",
      borderRadius: "20px"
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("input", {
    id: "rtp",
    ref: previewImageRef,
    type: "file",
    name: "prev_image",
    onChange: function onChange(e) {
      return handlePreviewImageDisplay(e);
    },
    className: "hidden border text-sm border-gray-400 px-2 py-1 w-full py-1"
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("label", {
    htmlFor: "rtp",
    className: " cursor-pointer hover:text-palette-blue-light ".concat(filename ? "text-palette-blue-light" : "")
  }, filename || "Select File"), filename && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "cursor-pointer ml-4 mr-8 text-lg text-red-500"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_fortawesome_react_fontawesome__WEBPACK_IMPORTED_MODULE_11__["FontAwesomeIcon"], {
    onClick: function onClick(e) {
      return handleDeleteFile(e);
    },
    icon: _fortawesome_free_solid_svg_icons__WEBPACK_IMPORTED_MODULE_13__["faTimes"]
  })))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "flex items-center justify-end w-1/2"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components_base__WEBPACK_IMPORTED_MODULE_4__["Button"], {
    className: "font-bold rounded-full",
    disabled: loading
  }, loading && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_fortawesome_react_fontawesome__WEBPACK_IMPORTED_MODULE_11__["FontAwesomeIcon"], {
    icon: _fortawesome_free_solid_svg_icons__WEBPACK_IMPORTED_MODULE_13__["faSpinner"],
    className: "fa-spin mr-2"
  }), "Continue")))))))), toogleAddEditDeveloper && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components_AddDeveloperModal__WEBPACK_IMPORTED_MODULE_20__["default"], {
    title: "Add Developer",
    developer: {},
    getDevelopers: function getDevelopers() {
      return _getDevelopers();
    },
    toogleAddDeveloper: function toogleAddDeveloper() {
      return toogleAddUpdateDeveloper();
    }
  }));
});

/***/ }),

/***/ "./resources/js/admin/components/AddProjectResources.js":
/*!**************************************************************!*\
  !*** ./resources/js/admin/components/AddProjectResources.js ***!
  \**************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime/regenerator */ "./node_modules/@babel/runtime/regenerator/index.js");
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _fortawesome_react_fontawesome__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @fortawesome/react-fontawesome */ "./node_modules/@fortawesome/react-fontawesome/index.es.js");
/* harmony import */ var _states_userGlobal__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../states/userGlobal */ "./resources/js/states/userGlobal.js");
/* harmony import */ var _helpers_resourceHelper_index__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../helpers/resourceHelper/index */ "./resources/js/helpers/resourceHelper/index.js");
/* harmony import */ var _data_index__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../data/index */ "./resources/js/data/index.js");
/* harmony import */ var _components_base__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../components/_base */ "./resources/js/components/_base/index.js");
/* harmony import */ var _components_base__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(_components_base__WEBPACK_IMPORTED_MODULE_6__);
/* harmony import */ var _components_resources_ResourcesData__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../components/resources/ResourcesData */ "./resources/js/components/resources/ResourcesData.js");
/* harmony import */ var _components_resources_LandResourcesData__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../../components/resources/LandResourcesData */ "./resources/js/components/resources/LandResourcesData.js");
/* harmony import */ var _components_base_alerts_sweetAlert__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../../components/_base/alerts/sweetAlert */ "./resources/js/components/_base/alerts/sweetAlert.js");


function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _unsupportedIterableToArray(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

function _iterableToArrayLimit(arr, i) { if (typeof Symbol === "undefined" || !(Symbol.iterator in Object(arr))) return; var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }











var AddProjectResources = function AddProjectResources() {
  var _UserGlobal = Object(_states_userGlobal__WEBPACK_IMPORTED_MODULE_3__["default"])(),
      _UserGlobal2 = _slicedToArray(_UserGlobal, 2),
      userState = _UserGlobal2[0],
      userActions = _UserGlobal2[1];

  var _useState = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])([]),
      _useState2 = _slicedToArray(_useState, 2),
      projectResources = _useState2[0],
      setProjectResources = _useState2[1];

  var _useState3 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(false),
      _useState4 = _slicedToArray(_useState3, 2),
      loading = _useState4[0],
      setLoading = _useState4[1];

  var resourceForm = Object(react__WEBPACK_IMPORTED_MODULE_1__["useRef"])(null);

  var _useState5 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])("file"),
      _useState6 = _slicedToArray(_useState5, 2),
      builderProfile = _useState6[0],
      setBuilderProfile = _useState6[1];

  var _useState7 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(""),
      _useState8 = _slicedToArray(_useState7, 2),
      builderProfileLink = _useState8[0],
      setBuilderProfileLink = _useState8[1];

  var _useState9 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])("file"),
      _useState10 = _slicedToArray(_useState9, 2),
      arcProfile = _useState10[0],
      setArcProfile = _useState10[1];

  var _useState11 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(""),
      _useState12 = _slicedToArray(_useState11, 2),
      arcProfileLink = _useState12[0],
      setArcProfileLink = _useState12[1];

  var _useState13 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])("file"),
      _useState14 = _slicedToArray(_useState13, 2),
      devProfile = _useState14[0],
      setDevProfile = _useState14[1];

  var _useState15 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(""),
      _useState16 = _slicedToArray(_useState15, 2),
      devProfileLink = _useState16[0],
      setDevProfileLink = _useState16[1];

  var _useState17 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(""),
      _useState18 = _slicedToArray(_useState17, 2),
      projectVideo = _useState18[0],
      setProjectVideo = _useState18[1];

  var tableHeaders = [{
    label: "Project Resources",
    style: "",
    width: "w-1/2"
  }, {
    label: "Document Name",
    style: "text-center",
    width: "w-4/12"
  }, {
    label: "Delete",
    style: "text-center",
    width: "w-1/12"
  }];

  var handleSubmit = function handleSubmit(e) {
    e.preventDefault();
    saveProjectResources();
  };

  var saveProjectResources = /*#__PURE__*/function () {
    var _ref = _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee() {
      var formData, url;
      return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee$(_context) {
        while (1) {
          switch (_context.prev = _context.next) {
            case 0:
              formData = new FormData(resourceForm.current);
              projectResources.forEach(function (resource, key) {
                if (arcProfile === "text" && resource.name === "architect_profile") {
                  return;
                }

                if (builderProfile === "text" && resource.name === "builder_profile") {
                  return;
                }

                if (devProfile === "text" && resource.name === "developer_profile") {
                  return;
                }

                formData.append("resources[".concat(key, "][file]"), resource.file);
                formData.append("resources[".concat(key, "][name]"), resource.name);
              });

              if (devProfileLink) {
                formData.append("devProfileLink", _helpers_resourceHelper_index__WEBPACK_IMPORTED_MODULE_4__["resources"].appendHttpsToResource(devProfileLink));
              }

              if (arcProfileLink) {
                formData.append("arcProfileLink", _helpers_resourceHelper_index__WEBPACK_IMPORTED_MODULE_4__["resources"].appendHttpsToResource(arcProfileLink));
              }

              if (builderProfileLink) {
                formData.append("builderProfileLink", _helpers_resourceHelper_index__WEBPACK_IMPORTED_MODULE_4__["resources"].appendHttpsToResource(builderProfileLink));
              }

              setLoading(true);
              formData.append("id", userState.projectData.id);
              formData.append("name", userState.projectData.name);
              formData.append("projectVideo", projectVideo);
              url = "/api/project-resource/upload";
              _context.next = 12;
              return Object(_data_index__WEBPACK_IMPORTED_MODULE_5__["saveUpdateResources"])({
                formData: formData,
                url: url
              }).then(function (r) {
                if (r.status === 200) {
                  setLoading(false);
                  Object(_components_base_alerts_sweetAlert__WEBPACK_IMPORTED_MODULE_9__["sweetAlert"])("success", "Project resources successfully added.");
                  userActions.setState({
                    projectData: r.data
                  });
                  userActions.setState({
                    currentProjStep: 3
                  });
                }
              })["catch"](function (err) {
                setLoading(false);
              }).then(function () {});

            case 12:
            case "end":
              return _context.stop();
          }
        }
      }, _callee);
    }));

    return function saveProjectResources() {
      return _ref.apply(this, arguments);
    };
  }();

  var handleFileChange = function handleFileChange(e, resource) {
    e.preventDefault();
    var c = projectResources.filter(function (file) {
      if (file["name"] !== resource) {
        return file;
      }
    });

    if (e.target.files.length > 0) {
      setProjectResources(c.concat(_helpers_resourceHelper_index__WEBPACK_IMPORTED_MODULE_4__["resources"].trimFilename(e.target.files[0], resource)));
      e.target.value = null;
    }
  };

  var handleDelete = function handleDelete(e, resource) {
    e.preventDefault();
    setProjectResources(projectResources.filter(function (file) {
      return file["name"] !== resource;
    }));
    e.target.value = null;
  };

  var getFileName = function getFileName(resource) {
    var filename = "";
    projectResources.forEach(function (file) {
      if (file["name"] === resource) {
        if (typeof file["file"].name == "string") {
          filename = file["file"]["name"];
        } else {
          var first = file["file"].split("/".concat(resource, "/"))[1];
          filename = first.split("?")[0];
        }
      }
    });
    return filename;
  };

  var changeInputType = function changeInputType(rowName, checked) {
    if (rowName === "Builder Profile") {
      if (checked) {
        setBuilderProfile("text");
      } else {
        setBuilderProfileLink("");
        setBuilderProfile("file");
      }
    }

    if (rowName === "Developer Profile") {
      if (checked) {
        setDevProfile("text");
      } else {
        setDevProfileLink("");
        setDevProfile("file");
      }
    }

    if (rowName === "Architect Profile") {
      if (checked) {
        setArcProfile("text");
      } else {
        setArcProfileLink("");
        setArcProfile("file");
      }
    }
  };

  var setCheckedState = function setCheckedState(row) {
    var retval = false;

    if (row.label === "Builder Profile") {
      if (builderProfile === "text") {
        retval = true;
      }
    }

    if (row.label === "Developer Profile") {
      if (devProfile === "text") {
        retval = true;
      }
    }

    if (row.label === "Architect Profile") {
      if (arcProfile === "text") {
        retval = true;
      }
    }

    return retval;
  };

  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "bg-white rounded-lg py-5 px-8 m-5"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("form", {
    ref: resourceForm,
    onSubmit: handleSubmit
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("table", {
    className: "w-full"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("thead", {
    className: "w-full"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("tr", null, tableHeaders.map(function (header, key) {
    return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("th", {
      key: key,
      className: " ".concat(header.style, " border-b font-bold px-4 py-2 text-palette-gray px-4 py-2 ").concat(header.width, " ")
    }, header.label);
  }))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("tbody", {
    className: "w-full"
  }, userState.projectData && userState.projectData.sub_property_id == "4" ? /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components_resources_LandResourcesData__WEBPACK_IMPORTED_MODULE_8__["default"], {
    devProfile: devProfile,
    getFileName: getFileName,
    setCheckedState: setCheckedState,
    changeInputType: changeInputType,
    handleFileChange: handleFileChange,
    handleDelete: handleDelete,
    setProjectVideo: setProjectVideo,
    setDevProfileLink: setDevProfileLink
  }) : /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components_resources_ResourcesData__WEBPACK_IMPORTED_MODULE_7__["default"], {
    builderProfile: builderProfile,
    devProfile: devProfile,
    arcProfile: arcProfile,
    getFileName: getFileName,
    setCheckedState: setCheckedState,
    changeInputType: changeInputType,
    handleFileChange: handleFileChange,
    handleDelete: handleDelete,
    setProjectVideo: setProjectVideo,
    setDevProfileLink: setDevProfileLink
  }))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "flex justify-end my-5"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components_base__WEBPACK_IMPORTED_MODULE_6__["Button"], {
    className: "font-bold rounded-full",
    disabled: loading
  }, loading && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_fortawesome_react_fontawesome__WEBPACK_IMPORTED_MODULE_2__["FontAwesomeIcon"], {
    icon: ["fas", "spinner"],
    className: "fa-spin mr-2"
  }), "Continue"))));
};

/* harmony default export */ __webpack_exports__["default"] = (AddProjectResources);

/***/ }),

/***/ "./resources/js/admin/components/AddProperty.js":
/*!******************************************************!*\
  !*** ./resources/js/admin/components/AddProperty.js ***!
  \******************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react_dropdown__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react-dropdown */ "./node_modules/react-dropdown/dist/index.js");
/* harmony import */ var react_dropdown__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react_dropdown__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _helpers_styles_dropdown_css__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../helpers/styles/dropdown.css */ "./resources/js/helpers/styles/dropdown.css");
/* harmony import */ var _helpers_styles_dropdown_css__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(_helpers_styles_dropdown_css__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _components_base__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../components/_base */ "./resources/js/components/_base/index.js");
/* harmony import */ var _components_base__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(_components_base__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var _components_RichTextEditor__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../components/RichTextEditor */ "./resources/js/admin/components/RichTextEditor.js");
/* harmony import */ var draft_js__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! draft-js */ "./node_modules/draft-js/lib/Draft.js");
/* harmony import */ var draft_js__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(draft_js__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var _helpers_styles_datepicker_css__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../helpers/styles/datepicker.css */ "./resources/js/helpers/styles/datepicker.css");
/* harmony import */ var _helpers_styles_datepicker_css__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(_helpers_styles_datepicker_css__WEBPACK_IMPORTED_MODULE_6__);
/* harmony import */ var draft_convert__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! draft-convert */ "./node_modules/draft-convert/esm/index.js");
/* harmony import */ var react_currency_format__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! react-currency-format */ "./node_modules/react-currency-format/lib/currency-format.js");
/* harmony import */ var react_currency_format__WEBPACK_IMPORTED_MODULE_8___default = /*#__PURE__*/__webpack_require__.n(react_currency_format__WEBPACK_IMPORTED_MODULE_8__);
/* harmony import */ var _states_userGlobal__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../../states/userGlobal */ "./resources/js/states/userGlobal.js");
/* harmony import */ var _helpers_styles_toast_css__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ../../helpers/styles/toast.css */ "./resources/js/helpers/styles/toast.css");
/* harmony import */ var _helpers_styles_toast_css__WEBPACK_IMPORTED_MODULE_10___default = /*#__PURE__*/__webpack_require__.n(_helpers_styles_toast_css__WEBPACK_IMPORTED_MODULE_10__);
/* harmony import */ var _material_ui_icons__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! @material-ui/icons */ "./node_modules/@material-ui/icons/esm/index.js");
/* harmony import */ var _fortawesome_react_fontawesome__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! @fortawesome/react-fontawesome */ "./node_modules/@fortawesome/react-fontawesome/index.es.js");
/* harmony import */ var _fortawesome_free_solid_svg_icons__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! @fortawesome/free-solid-svg-icons */ "./node_modules/@fortawesome/free-solid-svg-icons/index.es.js");
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! lodash */ "./node_modules/lodash/lodash.js");
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_14___default = /*#__PURE__*/__webpack_require__.n(lodash__WEBPACK_IMPORTED_MODULE_14__);
/* harmony import */ var _components_base_alerts_sweetAlert__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! ../../components/_base/alerts/sweetAlert */ "./resources/js/components/_base/alerts/sweetAlert.js");
/* harmony import */ var _helpers_propertyHelper_propertyHelper__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! ../../helpers/propertyHelper/propertyHelper */ "./resources/js/helpers/propertyHelper/propertyHelper.js");
/* harmony import */ var _helpers_fileHelper_fileHelper__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! ../../helpers/fileHelper/fileHelper */ "./resources/js/helpers/fileHelper/fileHelper.js");
function _toConsumableArray(arr) { return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _unsupportedIterableToArray(arr) || _nonIterableSpread(); }

function _nonIterableSpread() { throw new TypeError("Invalid attempt to spread non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _iterableToArray(iter) { if (typeof Symbol !== "undefined" && Symbol.iterator in Object(iter)) return Array.from(iter); }

function _arrayWithoutHoles(arr) { if (Array.isArray(arr)) return _arrayLikeToArray(arr); }

function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _unsupportedIterableToArray(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

function _iterableToArrayLimit(arr, i) { if (typeof Symbol === "undefined" || !(Symbol.iterator in Object(arr))) return; var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }




















/* harmony default export */ __webpack_exports__["default"] = (function (_ref) {
  var projectName = _ref.projectName,
      projectId = _ref.projectId,
      toggleAddPropertyModal = _ref.toggleAddPropertyModal,
      addToggleFetch = _ref.addToggleFetch;

  var _UserGlobal = Object(_states_userGlobal__WEBPACK_IMPORTED_MODULE_9__["default"])(),
      _UserGlobal2 = _slicedToArray(_UserGlobal, 2),
      userState = _UserGlobal2[0],
      userActions = _UserGlobal2[1];

  var _useState = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])([]),
      _useState2 = _slicedToArray(_useState, 2),
      featuredImages = _useState2[0],
      setFeaturedImages = _useState2[1];

  var _useState3 = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])(""),
      _useState4 = _slicedToArray(_useState3, 2),
      state = _useState4[0],
      setState = _useState4[1];

  var _useState5 = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])(""),
      _useState6 = _slicedToArray(_useState5, 2),
      propertyType = _useState6[0],
      setPropertyType = _useState6[1];

  var _useState7 = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])([]),
      _useState8 = _slicedToArray(_useState7, 2),
      errors = _useState8[0],
      setErrors = _useState8[1];

  var _useState9 = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])(function () {
    return draft_js__WEBPACK_IMPORTED_MODULE_5__["EditorState"].createEmpty();
  }),
      _useState10 = _slicedToArray(_useState9, 2),
      editorState = _useState10[0],
      setEditorState = _useState10[1];

  var _useState11 = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])([]),
      _useState12 = _slicedToArray(_useState11, 2),
      floorPlan = _useState12[0],
      setFLoorPlan = _useState12[1];

  var _useState13 = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])(""),
      _useState14 = _slicedToArray(_useState13, 2),
      filename = _useState14[0],
      setFilename = _useState14[1];

  var _useState15 = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])(""),
      _useState16 = _slicedToArray(_useState15, 2),
      prevFilename = _useState16[0],
      setPrevFilename = _useState16[1];

  var _useState17 = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])([]),
      _useState18 = _slicedToArray(_useState17, 2),
      previewImage = _useState18[0],
      setPreviewImage = _useState18[1];

  var _useState19 = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])([]),
      _useState20 = _slicedToArray(_useState19, 2),
      fmPlaceHolder = _useState20[0],
      setFmPlaceHolder = _useState20[1];

  var _useState21 = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])(false),
      _useState22 = _slicedToArray(_useState21, 2),
      limitError = _useState22[0],
      setLimitError = _useState22[1];

  var addPropertyForm = Object(react__WEBPACK_IMPORTED_MODULE_0__["useRef"])(null);

  var _useState23 = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])(false),
      _useState24 = _slicedToArray(_useState23, 2),
      loading = _useState24[0],
      setLoading = _useState24[1];

  var _useState25 = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])(""),
      _useState26 = _slicedToArray(_useState25, 2),
      strata = _useState26[0],
      setStrata = _useState26[1];

  var _useState27 = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])(""),
      _useState28 = _slicedToArray(_useState27, 2),
      water = _useState28[0],
      setWater = _useState28[1];

  var _useState29 = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])(""),
      _useState30 = _slicedToArray(_useState29, 2),
      council = _useState30[0],
      setCouncil = _useState30[1];

  var _useState31 = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])(_helpers_propertyHelper_propertyHelper__WEBPACK_IMPORTED_MODULE_16__["costDueDates"]),
      _useState32 = _slicedToArray(_useState31, 2),
      dates = _useState32[0],
      setDates = _useState32[1];

  var previewImageRef = Object(react__WEBPACK_IMPORTED_MODULE_0__["useRef"])(null);
  var floorPlanRef = Object(react__WEBPACK_IMPORTED_MODULE_0__["useRef"])(null);
  Object(react__WEBPACK_IMPORTED_MODULE_0__["useEffect"])(function () {
    var phs = [];

    for (var index = 0; index < 6 - featuredImages.length; index++) {
      var ph = {
        classes: "cursor-pointer m-2 inline-flex bg-transparent text-palette-gray"
      };
      phs.push(ph);
    }

    setFmPlaceHolder(phs);
  }, [featuredImages.length]);

  var handleDeletePrevFile = function handleDeletePrevFile(e) {
    e.preventDefault();
    setPreviewImage([]);
    previewImageRef.current.type = "";
    previewImageRef.current.type = "file";
    setPrevFilename("");
    e.target.value = null;
  };

  var handlePreviewImageDisplay = function handlePreviewImageDisplay(e) {
    e.preventDefault();

    if (e.target.files.length > 1) {
      Object(_components_base_alerts_sweetAlert__WEBPACK_IMPORTED_MODULE_15__["sweetAlert"])("error", "Multiple upload is not allowed!");
      return;
    }

    if (!Object(_helpers_fileHelper_fileHelper__WEBPACK_IMPORTED_MODULE_17__["imageFileTypeFilter"])(e.target.files[0]["name"])) {
      Object(_components_base_alerts_sweetAlert__WEBPACK_IMPORTED_MODULE_15__["sweetAlert"])("error", "Filetype is not supported!");
      e.target.value = null;
      return;
    }

    setPreviewImage(previewImage.concat(_toConsumableArray(_toConsumableArray(e.target.files).map(function (file) {
      var newFilename = Object(_helpers_fileHelper_fileHelper__WEBPACK_IMPORTED_MODULE_17__["sanitizeFilename"])(file.name);
      var newFileObj = new File([file], newFilename);
      setPrevFilename(newFilename);
      return {
        image: newFileObj,
        preview: URL.createObjectURL(newFileObj)
      };
    }))));
  };

  var saveProperty = function saveProperty() {
    var _userState$projectDat;

    setLoading(true);
    var formData = new FormData(addPropertyForm.current);
    formData.append("role", "project_developer");

    if (floorPlan && !Object(lodash__WEBPACK_IMPORTED_MODULE_14__["isEmpty"])(floorPlan)) {
      formData.append("floorPlan[file]", floorPlan[0]);
    }

    if (previewImage.length) {
      formData.append("preview_img[".concat(0, "]"), previewImage[0].image);
    } else {
      formData.append("preview_img[".concat(0, "]"), "");
    } // formData.append("projectName", projectName);


    formData.append("projectId", (_userState$projectDat = userState.projectData.id) !== null && _userState$projectDat !== void 0 ? _userState$projectDat : projectId); // formData.append("ownershipType", ownershipType);
    // formData.append("subPropertyType", subPropertyType);

    formData.append("propertyType", propertyType);
    formData.append("water", water);
    formData.append("council", council);
    formData.append("strata", strata); // formData.append("state", state);

    formData.append("description", editorState.getCurrentContent().getPlainText());
    formData.append("descriptionHTML", Object(draft_convert__WEBPACK_IMPORTED_MODULE_7__["convertToHTML"])(editorState.getCurrentContent()));
    featuredImages.map(function (image, key) {
      formData.append("featuredImages[".concat(key, "]"), image.image);
    });
    var url = "/api/property/";
    axios({
      method: "post",
      url: url,
      data: formData,
      name: "project",
      headers: {
        "content-type": "multipart/form-data"
      }
    }).then(function (r) {
      if (r.status === 200) {
        Object(_components_base_alerts_sweetAlert__WEBPACK_IMPORTED_MODULE_15__["sweetAlert"])("success", "Property successfully added.");
        setLoading(false);
        userActions.setState({
          propertiesData: r.data
        });
        addToggleFetch();
        toggleAddPropertyModal();
      }
    })["catch"](function (error) {
      if (error.response.data.errors) {
        Object(_components_base_alerts_sweetAlert__WEBPACK_IMPORTED_MODULE_15__["sweetAlert"])("error", "Some field are missing.");
      }

      setLoading(false);
      setErrors(error.response.data.errors);
    }).then(function () {});
  };

  var handleSubmit = function handleSubmit(e) {
    e.preventDefault();
    setLimitError(false);
    saveProperty(); // window.location.href = window.location.href;
  };

  var handleDrop = function handleDrop(e) {
    e.preventDefault();
    e.stopPropagation();
    var fmCount = featuredImages.length;
    var onCount = e.dataTransfer.files.length;

    if (fmCount + onCount > 6) {
      e.target.value = null;
      setLimitError(true);
      return;
    } else {
      setLimitError(false);
    }

    setFeaturedImages(featuredImages.concat(_toConsumableArray(_toConsumableArray(e.dataTransfer.files).map(function (file) {
      var newFilename = Object(_helpers_fileHelper_fileHelper__WEBPACK_IMPORTED_MODULE_17__["sanitizeFilename"])(file.name);
      var newFileObj = new File([file], newFilename);
      return {
        image: newFileObj,
        preview: URL.createObjectURL(newFileObj)
      };
    }))));
    e.target.value = null;
  };

  var handleChange = function handleChange(e) {
    var fmCount = featuredImages.length;
    var onCount = e.target.files.length;

    if (fmCount + onCount > 6) {
      setLimitError(true);
      e.target.value = null;
      return;
    } else {
      setLimitError(false);
    }

    setFeaturedImages(featuredImages.concat(_toConsumableArray(_toConsumableArray(e.target.files).map(function (file) {
      return {
        image: file,
        preview: URL.createObjectURL(file)
      };
    }))));
    e.target.value = null;
  };

  var handleRemove = function handleRemove(e, i) {
    e.preventDefault();
    e.stopPropagation();
    setFeaturedImages(_toConsumableArray(featuredImages.filter(function (image, k) {
      if (i !== k) {
        return image;
      }
    })));

    if (featuredImages.length > 6) {
      setLimitError(true);
    } else {
      setLimitError(false);
    }
  };

  var handleFloorPlanChange = function handleFloorPlanChange(e) {
    e.preventDefault();

    if (!Object(_helpers_fileHelper_fileHelper__WEBPACK_IMPORTED_MODULE_17__["PDFFileTypeFilter"])(e.target.files[0]["name"])) {
      Object(_components_base_alerts_sweetAlert__WEBPACK_IMPORTED_MODULE_15__["sweetAlert"])("error", "Filetype is not supported!");
      e.target.value = null;
      return;
    }

    setFLoorPlan(Object(_helpers_propertyHelper_propertyHelper__WEBPACK_IMPORTED_MODULE_16__["floorPlanHelper"])(e.target.files));
    var newFilename = Object(_helpers_fileHelper_fileHelper__WEBPACK_IMPORTED_MODULE_17__["sanitizeFilename"])(e.target.files[0]["name"]);
    setFilename(newFilename); // e.target.value = null;
  };

  var handleDeleteFile = function handleDeleteFile(e) {
    e.preventDefault();
    setFLoorPlan({});
    floorPlanRef.current.type = "";
    floorPlanRef.current.type = "file";
    setFilename("");
    e.target.value = null;
  };

  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_base__WEBPACK_IMPORTED_MODULE_3__["Modal"], {
    show: true,
    title: "Add Property",
    disableBackdropClick: true,
    maxWidth: "md",
    topOff: true,
    onClose: function onClose() {
      return toggleAddPropertyModal();
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "bg-white rounded-lg py-5 px-8 m-5"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("form", {
    ref: addPropertyForm,
    onSubmit: handleSubmit
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "text-black"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "py-3 flex justify-start "
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "flex justify-center items-center"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("label", {
    className: "font-semibold w-24"
  }, "Property Type"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_dropdown__WEBPACK_IMPORTED_MODULE_1___default.a, {
    id: "property_type",
    value: propertyType,
    options: _helpers_propertyHelper_propertyHelper__WEBPACK_IMPORTED_MODULE_16__["property_types"],
    onChange: function onChange(option) {
      setPropertyType(option.value);
    },
    placeholder: "Please select..." // name="property_type"
    ,
    className: "w-48 ml-4"
  }), errors["propertyType"] && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
    className: "px-1 text-red-400 tracking-widest  text-xs "
  }, errors["propertyType"][0]))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "ml-10 flex-1 flex justify-end items-center"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("label", {
    className: "font-semibold w-32"
  }, "Property Number"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_base__WEBPACK_IMPORTED_MODULE_3__["TextInput"], {
    className: "border-gray-400 border pl-2 py-1",
    border: false,
    name: "unit_name",
    width: "w-24"
  }), errors["unit_name"] && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
    className: "px-1 text-red-400 tracking-widest  text-xs "
  }, errors["unit_name"][0]))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "ml-4 flex-1 flex justify-end items-center"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("label", {
    className: "font-semibold w-24 mr-4"
  }, "Lot Number"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_base__WEBPACK_IMPORTED_MODULE_3__["TextInput"], {
    className: "border-gray-400 border pl-2 py-1",
    border: false,
    name: "unit_no",
    width: "w-24"
  }), errors["unit_no"] && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
    className: "px-1 text-red-400 tracking-widest  text-xs "
  }, errors["unit_no"][0])))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "flex justify-start my-3"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "flex items-center justify-start py-3"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("label", {
    className: "w-12"
  }, "Price"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "relative"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_currency_format__WEBPACK_IMPORTED_MODULE_8___default.a, {
    allowNegative: false,
    className: "border text-sm border-gray-400 px-2 py-1 w-48",
    thousandSeparator: true,
    name: "price",
    id: "price",
    prefix: "$"
  })), errors["price"] && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
    className: "absolute px-1 text-red-400 tracking-widest text-xs "
  }, errors["price"][0]))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "py-3 flex items-center ml-12"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("label", {
    title: "Number of bedrooms",
    className: "cursor-pointer mr-4"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_material_ui_icons__WEBPACK_IMPORTED_MODULE_11__["LocalHotel"], null)), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "flex flex-col w-24"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_base__WEBPACK_IMPORTED_MODULE_3__["TextInput"], {
    className: "pl-2 text-sm border-gray-400 w-32 border py-1",
    border: false,
    type: "number",
    name: "number_of_bedrooms"
  }), errors["number_of_bedrooms"] && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
    className: "px-1 text-red-400 tracking-widest  text-xs "
  }, errors["number_of_bedrooms"][0]))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "py-3 flex items-center ml-16"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("label", {
    title: "Number of bathrooms",
    className: "cursor-pointer mr-4"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_material_ui_icons__WEBPACK_IMPORTED_MODULE_11__["Bathtub"], null)), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "flex flex-col w-24"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_base__WEBPACK_IMPORTED_MODULE_3__["TextInput"], {
    className: "pl-2 text-sm border-gray-400 w-32 border py-1",
    border: false,
    type: "number",
    name: "number_of_bathrooms"
  }), errors["number_of_bathrooms"] && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
    className: "px-1 text-red-400 tracking-widest  text-xs "
  }, errors["number_of_bathrooms"][0]))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "py-3 flex items-center ml-12"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("label", {
    title: "Number of garages",
    className: "cursor-pointer mr-4 "
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_material_ui_icons__WEBPACK_IMPORTED_MODULE_11__["DirectionsCar"], null)), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "flex flex-col w-24"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_base__WEBPACK_IMPORTED_MODULE_3__["TextInput"], {
    className: "pl-2 text-sm border-gray-400 w-32 border py-1",
    border: false,
    type: "number",
    name: "number_of_garages",
    min: "1"
  }), errors["number_of_garages"] && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
    className: "px-1 text-red-400 tracking-widest  text-xs "
  }, errors["number_of_garages"][0])))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "flex"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "py-3 flex justify-start items-center"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("label", {
    className: "w-24"
  }, "Internal SQM"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "relative"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "ml-4 w-32"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_base__WEBPACK_IMPORTED_MODULE_3__["TextInput"], {
    className: "pl-2 text-sm border-gray-400 w-32 border py-1",
    border: false,
    type: "number",
    name: "internal_size"
  })), errors["internal_size"] && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
    className: "absolute px-1 text-red-400 tracking-widest  text-xs "
  }, errors["internal_size"][0]))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "py-3 flex justify-start items-center mx-10"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("label", {
    className: "w-24"
  }, "External SQM"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "relative"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "ml-4 w-32"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_base__WEBPACK_IMPORTED_MODULE_3__["TextInput"], {
    className: "pl-2 text-sm border-gray-400 w-32 border py-1",
    border: false,
    type: "number",
    name: "external_size"
  })), errors["external_size"] && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
    className: "absolute px-1 text-red-400 tracking-widest  text-xs "
  }, errors["external_size"][0]))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "py-3 flex justify-start items-center ml-3"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("label", {
    className: "w-24"
  }, "Parking SQM"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "relative"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "ml-4 w-32"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_base__WEBPACK_IMPORTED_MODULE_3__["TextInput"], {
    className: "pl-2 text-sm border-gray-400 w-32 border py-1 mr-1",
    border: false,
    type: "number",
    name: "parking_size"
  })), errors["parking_size"] && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
    className: "absolute px-1 text-red-400 tracking-widest  text-xs "
  }, errors["parking_size"][0])))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "flex justify-center my-3"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "py-3 flex flex-1 items-center"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("label", {
    title: "Strata",
    className: "cursor-pointer mr-4 ".concat(errors["strata"] ? "mb-4" : "mb-0")
  }, "Strata"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "flex ml-2"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_currency_format__WEBPACK_IMPORTED_MODULE_8___default.a, {
    allowNegative: false,
    className: "border text-sm border-gray-400 px-2 py-1 w-32",
    thousandSeparator: true,
    name: "strata_cost",
    id: "strata_cost",
    prefix: "$"
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_dropdown__WEBPACK_IMPORTED_MODULE_1___default.a, {
    id: "strata",
    value: strata,
    options: dates,
    onChange: function onChange(option) {
      setStrata(option.value);
    },
    placeholder: "Please select...",
    name: "strata",
    className: "w-48 ml-2"
  })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", null, errors["strata"] && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
    className: "px-1 text-red-400 tracking-widest  text-xs "
  }, errors["strata"][0]), errors["strata_cost"] && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
    className: "px-1 text-red-400 tracking-widest  text-xs "
  }, errors["strata_cost"][0])))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "flex flex-1 items-center justify-end py-3"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("label", {
    title: "Water",
    className: "cursor-pointer mr-4 ".concat(errors["water"] ? "mb-4" : "mb-0")
  }, "Water"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "flex ml-2"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_currency_format__WEBPACK_IMPORTED_MODULE_8___default.a, {
    allowNegative: false,
    className: "border text-sm border-gray-400 px-2 py-1 w-32",
    thousandSeparator: true,
    name: "water_cost",
    id: "water_cost",
    prefix: "$"
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_dropdown__WEBPACK_IMPORTED_MODULE_1___default.a, {
    id: "water",
    value: water,
    options: dates,
    onChange: function onChange(option) {
      setWater(option.value);
    },
    placeholder: "Please select...",
    name: "water",
    className: "w-48 ml-2"
  })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", null, errors["water"] && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
    className: "px-1 text-red-400 tracking-widest  text-xs "
  }, errors["water"][0]), errors["water_cost"] && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
    className: "px-1 text-red-400 tracking-widest  text-xs "
  }, errors["water_cost"][0]))))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "flex justify-center my-3"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "py-3 flex flex-1 items-center"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("label", {
    title: "Council",
    className: "cursor-pointer mr-3 ".concat(errors["council"] ? "mb-4" : "mb-0")
  }, "Council"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "flex ml-1"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_currency_format__WEBPACK_IMPORTED_MODULE_8___default.a, {
    allowNegative: false,
    className: "border text-sm border-gray-400 px-2 py-1 w-32",
    thousandSeparator: true,
    name: "council_cost",
    id: "council_cost",
    prefix: "$"
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_dropdown__WEBPACK_IMPORTED_MODULE_1___default.a, {
    id: "council",
    value: council,
    options: dates,
    onChange: function onChange(option) {
      setCouncil(option.value);
    },
    placeholder: "Please select...",
    name: "council",
    className: "w-48 ml-2"
  })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", null, errors["council"] && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
    className: "px-1 text-red-400 tracking-widest text-xs "
  }, errors["council"][0]), errors["council_cost"] && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
    className: "px-1 text-red-400 tracking-widest text-xs "
  }, errors["council_cost"][0]))))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "py-3 "
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("label", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "w-full text-sm bg-white text-black "
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_RichTextEditor__WEBPACK_IMPORTED_MODULE_4__["default"], {
    title: "Property Description",
    richEditorState: editorState,
    richEditorStateContent: function richEditorStateContent(content) {
      return setEditorState(content);
    },
    characterLimit: 2000
  }))), errors["description"] && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
    className: "px-1 text-red-400 tracking-widest  text-xs "
  }, errors["description"][0])), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "py-3"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("h1", {
    className: "font-semibold"
  }, " ", "Featured Images (min. 3 images)", " "), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("label", {
    className: "button ",
    htmlFor: "upload-photos"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: ""
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "w-full flex flex-wrap  cursor-pointer",
    style: {
      border: "2px dashed #ccc",
      borderRadius: "20px",
      padding: "20px",
      textAlign: "center",
      marginBottom: "0px"
    },
    onDrop: function onDrop(e) {
      return handleDrop(e);
    },
    onDragOver: function onDragOver(e) {
      e.preventDefault();
      e.stopPropagation();
    },
    onDragEnter: function onDragEnter(e) {
      e.preventDefault();
      e.stopPropagation();
    },
    onDragLeave: function onDragLeave(e) {
      e.preventDefault();
      e.stopPropagation();
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("input", {
    type: "file",
    id: "upload-photos",
    accept: "image/*",
    multiple: true,
    className: "hidden",
    name: "image[]",
    onChange: function onChange(e) {
      return handleChange(e);
    }
  }), featuredImages.map(function (image, index) {
    return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      key: index,
      className: "rounded-sm border m-2 relative",
      style: {
        backgroundImage: "url('".concat(image.preview, "')"),
        backgroundPositionX: "center",
        width: "6.9em",
        height: "8.9rem",
        backgroundSize: "cover"
      }
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_base__WEBPACK_IMPORTED_MODULE_3__["Tooltip"], {
      title: "Remove this item"
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
      onClick: function onClick(e) {
        return handleRemove(e, index);
      },
      className: "absolute bottom-0 right-0 p-2 -mb-2 "
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_fortawesome_react_fontawesome__WEBPACK_IMPORTED_MODULE_12__["FontAwesomeIcon"], {
      className: "text-red-500 shadow-xl hover:font-bold hover:text-red-700",
      icon: _fortawesome_free_solid_svg_icons__WEBPACK_IMPORTED_MODULE_13__["faTimes"]
    }))));
  }), fmPlaceHolder.map(function (p, i) {
    return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      key: i,
      className: "".concat(p.classes, " cursor-pointer flex justify-center items-center"),
      style: {
        width: "6.9rem",
        height: "8.9rem",
        border: "2px solid #ccc"
      }
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_fortawesome_react_fontawesome__WEBPACK_IMPORTED_MODULE_12__["FontAwesomeIcon"], {
      className: "text-xl",
      icon: _fortawesome_free_solid_svg_icons__WEBPACK_IMPORTED_MODULE_13__["faCamera"]
    }));
  })), limitError && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
    className: "px-1 text-red-400 tracking-widest  text-xs "
  }, "Please limit your photos to six items only."))), errors["featuredImages"] && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
    className: "px-1 text-red-400 tracking-widest  text-xs "
  }, errors["featuredImages"][0])), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "flex flex-row"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "w-1/2 mr-2"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("h1", {
    className: "font-semibold"
  }, "Floor Plan "), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "flex flex-col justify-between py-3"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("label", {
    className: "w-full"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "items-center cursor-pointer w-full p-3 rounded-lg flex",
    style: {
      border: "2px dashed #ccc",
      borderRadius: "20px"
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("input", {
    id: "floorPlan",
    ref: floorPlanRef,
    type: "file" // name="floor_plan"
    ,
    onChange: function onChange(e) {
      return handleFloorPlanChange(e);
    },
    className: "hidden border text-sm border-gray-400 px-2 py-1 w-full py-1"
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("label", {
    htmlFor: "floorPlan",
    className: " cursor-pointer hover:text-palette-blue-light ".concat(filename ? "text-palette-blue-light" : "")
  }, filename || "Select File"), filename && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "text-red-500 text-lg cursor-pointer ml-3"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_fortawesome_react_fontawesome__WEBPACK_IMPORTED_MODULE_12__["FontAwesomeIcon"], {
    onClick: function onClick(e) {
      return handleDeleteFile(e);
    },
    icon: _fortawesome_free_solid_svg_icons__WEBPACK_IMPORTED_MODULE_13__["faTimes"]
  })))), errors["floorPlan"] && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
    className: "px-1 text-red-400 tracking-widest  text-xs "
  }, errors["floorPlan"][0])))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "w-1/2 ml-2"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("h1", {
    className: "font-semibold"
  }, "Rich Text Preview "), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "flex flex-col justify-between py-3"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "flex"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("label", {
    className: "w-full"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "items-center cursor-pointer w-full p-3 rounded-lg flex",
    style: {
      border: "2px dashed #ccc",
      borderRadius: "20px"
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("input", {
    id: "rtp",
    ref: previewImageRef,
    type: "file" // name="prev_image"
    ,
    onChange: function onChange(e) {
      return handlePreviewImageDisplay(e);
    },
    className: "hidden border text-sm border-gray-400 px-2 py-1 w-full py-1"
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("label", {
    htmlFor: "rtp",
    className: " cursor-pointer hover:text-palette-blue-light ".concat(prevFilename ? "text-palette-blue-light" : "")
  }, prevFilename || "Select File"), prevFilename && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "text-red-500 text-lg cursor-pointer ml-3"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_fortawesome_react_fontawesome__WEBPACK_IMPORTED_MODULE_12__["FontAwesomeIcon"], {
    onClick: function onClick(e) {
      return handleDeletePrevFile(e);
    },
    icon: _fortawesome_free_solid_svg_icons__WEBPACK_IMPORTED_MODULE_13__["faTimes"]
  })))))))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "flex justify-end my-5"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_base__WEBPACK_IMPORTED_MODULE_3__["Button"], {
    className: "font-bold rounded-full",
    disabled: loading
  }, loading && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_fortawesome_react_fontawesome__WEBPACK_IMPORTED_MODULE_12__["FontAwesomeIcon"], {
    icon: _fortawesome_free_solid_svg_icons__WEBPACK_IMPORTED_MODULE_13__["faSpinner"],
    className: "fa-spin mr-2"
  }), "Save"))))));
});

/***/ }),

/***/ "./resources/js/admin/components/AddPropertyModal.js":
/*!***********************************************************!*\
  !*** ./resources/js/admin/components/AddPropertyModal.js ***!
  \***********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime/regenerator */ "./node_modules/@babel/runtime/regenerator/index.js");
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var react_dropdown__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! react-dropdown */ "./node_modules/react-dropdown/dist/index.js");
/* harmony import */ var react_dropdown__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(react_dropdown__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _helpers_styles_dropdown_css__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../helpers/styles/dropdown.css */ "./resources/js/helpers/styles/dropdown.css");
/* harmony import */ var _helpers_styles_dropdown_css__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(_helpers_styles_dropdown_css__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var _components_base__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../components/_base */ "./resources/js/components/_base/index.js");
/* harmony import */ var _components_base__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(_components_base__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var _fortawesome_free_solid_svg_icons__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @fortawesome/free-solid-svg-icons */ "./node_modules/@fortawesome/free-solid-svg-icons/index.es.js");
/* harmony import */ var _helpers_propertyHelper_propertyHelper__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../helpers/propertyHelper/propertyHelper */ "./resources/js/helpers/propertyHelper/propertyHelper.js");
/* harmony import */ var _components_RichTextEditor__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../components/RichTextEditor */ "./resources/js/admin/components/RichTextEditor.js");
/* harmony import */ var draft_js__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! draft-js */ "./node_modules/draft-js/lib/Draft.js");
/* harmony import */ var draft_js__WEBPACK_IMPORTED_MODULE_8___default = /*#__PURE__*/__webpack_require__.n(draft_js__WEBPACK_IMPORTED_MODULE_8__);
/* harmony import */ var react_datepicker__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! react-datepicker */ "./node_modules/react-datepicker/dist/react-datepicker.min.js");
/* harmony import */ var react_datepicker__WEBPACK_IMPORTED_MODULE_9___default = /*#__PURE__*/__webpack_require__.n(react_datepicker__WEBPACK_IMPORTED_MODULE_9__);
/* harmony import */ var _helpers_styles_datepicker_css__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ../../helpers/styles/datepicker.css */ "./resources/js/helpers/styles/datepicker.css");
/* harmony import */ var _helpers_styles_datepicker_css__WEBPACK_IMPORTED_MODULE_10___default = /*#__PURE__*/__webpack_require__.n(_helpers_styles_datepicker_css__WEBPACK_IMPORTED_MODULE_10__);
/* harmony import */ var draft_convert__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! draft-convert */ "./node_modules/draft-convert/esm/index.js");
/* harmony import */ var react_currency_format__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! react-currency-format */ "./node_modules/react-currency-format/lib/currency-format.js");
/* harmony import */ var react_currency_format__WEBPACK_IMPORTED_MODULE_12___default = /*#__PURE__*/__webpack_require__.n(react_currency_format__WEBPACK_IMPORTED_MODULE_12__);
/* harmony import */ var _fortawesome_react_fontawesome__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! @fortawesome/react-fontawesome */ "./node_modules/@fortawesome/react-fontawesome/index.es.js");
/* harmony import */ var _states_userGlobal__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ../../states/userGlobal */ "./resources/js/states/userGlobal.js");
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! lodash */ "./node_modules/lodash/lodash.js");
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_15___default = /*#__PURE__*/__webpack_require__.n(lodash__WEBPACK_IMPORTED_MODULE_15__);
/* harmony import */ var _material_ui_icons__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! @material-ui/icons */ "./node_modules/@material-ui/icons/esm/index.js");
/* harmony import */ var _components_base_alerts_sweetAlert__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! ../../components/_base/alerts/sweetAlert */ "./resources/js/components/_base/alerts/sweetAlert.js");
/* harmony import */ var _components_base_alerts_BaseAlert__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(/*! ../../components/_base/alerts/BaseAlert */ "./resources/js/components/_base/alerts/BaseAlert.js");
/* harmony import */ var _helpers_fileHelper_fileHelper__WEBPACK_IMPORTED_MODULE_19__ = __webpack_require__(/*! ../../helpers/fileHelper/fileHelper */ "./resources/js/helpers/fileHelper/fileHelper.js");
/* harmony import */ var _helpers_resourceHelper_index__WEBPACK_IMPORTED_MODULE_20__ = __webpack_require__(/*! ../../helpers/resourceHelper/index */ "./resources/js/helpers/resourceHelper/index.js");


function _toConsumableArray(arr) { return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _unsupportedIterableToArray(arr) || _nonIterableSpread(); }

function _nonIterableSpread() { throw new TypeError("Invalid attempt to spread non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _iterableToArray(iter) { if (typeof Symbol !== "undefined" && Symbol.iterator in Object(iter)) return Array.from(iter); }

function _arrayWithoutHoles(arr) { if (Array.isArray(arr)) return _arrayLikeToArray(arr); }

function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _unsupportedIterableToArray(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

function _iterableToArrayLimit(arr, i) { if (typeof Symbol === "undefined" || !(Symbol.iterator in Object(arr))) return; var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }






















/* harmony default export */ __webpack_exports__["default"] = (function (_ref) {
  var projectName = _ref.projectName,
      projectId = _ref.projectId,
      toggleAddPropertyModal = _ref.toggleAddPropertyModal,
      addToggleFetch = _ref.addToggleFetch;

  var _UserGlobal = Object(_states_userGlobal__WEBPACK_IMPORTED_MODULE_14__["default"])(),
      _UserGlobal2 = _slicedToArray(_UserGlobal, 2),
      userState = _UserGlobal2[0],
      userActions = _UserGlobal2[1];

  var _useState = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])([]),
      _useState2 = _slicedToArray(_useState, 2),
      featuredImages = _useState2[0],
      setFeaturedImages = _useState2[1];

  var _useState3 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(""),
      _useState4 = _slicedToArray(_useState3, 2),
      state = _useState4[0],
      setState = _useState4[1];

  var _useState5 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(""),
      _useState6 = _slicedToArray(_useState5, 2),
      propertyType = _useState6[0],
      setPropertyType = _useState6[1];

  var _useState7 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(""),
      _useState8 = _slicedToArray(_useState7, 2),
      subPropertyType = _useState8[0],
      setSubPropertyType = _useState8[1];

  var _useState9 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(""),
      _useState10 = _slicedToArray(_useState9, 2),
      ownershipType = _useState10[0],
      setOwnershipType = _useState10[1];

  var _useState11 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])([]),
      _useState12 = _slicedToArray(_useState11, 2),
      errors = _useState12[0],
      setErrors = _useState12[1];

  var _useState13 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(function () {
    return draft_js__WEBPACK_IMPORTED_MODULE_8__["EditorState"].createEmpty();
  }),
      _useState14 = _slicedToArray(_useState13, 2),
      editorState = _useState14[0],
      setEditorState = _useState14[1];

  var _useState15 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])([]),
      _useState16 = _slicedToArray(_useState15, 2),
      floorPlan = _useState16[0],
      setFLoorPlan = _useState16[1];

  var _useState17 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(""),
      _useState18 = _slicedToArray(_useState17, 2),
      filename = _useState18[0],
      setFilename = _useState18[1];

  var _useState19 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])([]),
      _useState20 = _slicedToArray(_useState19, 2),
      previewImage = _useState20[0],
      setPreviewImage = _useState20[1];

  var _useState21 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(""),
      _useState22 = _slicedToArray(_useState21, 2),
      prevFilename = _useState22[0],
      setPrevFilename = _useState22[1];

  var _useState23 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])([]),
      _useState24 = _slicedToArray(_useState23, 2),
      fmPlaceHolder = _useState24[0],
      setFmPlaceHolder = _useState24[1];

  var _useState25 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(false),
      _useState26 = _slicedToArray(_useState25, 2),
      limitError = _useState26[0],
      setLimitError = _useState26[1];

  var _useState27 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])({}),
      _useState28 = _slicedToArray(_useState27, 2),
      deals = _useState28[0],
      setDeals = _useState28[1];

  var _useState29 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(""),
      _useState30 = _slicedToArray(_useState29, 2),
      selectedDeal = _useState30[0],
      setSelectedDeal = _useState30[1];

  var addPropertyForm = Object(react__WEBPACK_IMPORTED_MODULE_1__["useRef"])(null);

  var _useState31 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(true),
      _useState32 = _slicedToArray(_useState31, 2),
      loading = _useState32[0],
      setLoading = _useState32[1];

  var _useState33 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(""),
      _useState34 = _slicedToArray(_useState33, 2),
      strata = _useState34[0],
      setStrata = _useState34[1];

  var _useState35 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(""),
      _useState36 = _slicedToArray(_useState35, 2),
      water = _useState36[0],
      setWater = _useState36[1];

  var _useState37 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(""),
      _useState38 = _slicedToArray(_useState37, 2),
      council = _useState38[0],
      setCouncil = _useState38[1];

  var _useState39 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(_helpers_propertyHelper_propertyHelper__WEBPACK_IMPORTED_MODULE_6__["costDueDates"]),
      _useState40 = _slicedToArray(_useState39, 2),
      dates = _useState40[0],
      setDates = _useState40[1];

  var previewImageRef = Object(react__WEBPACK_IMPORTED_MODULE_1__["useRef"])(null);
  var floorPlanRef = Object(react__WEBPACK_IMPORTED_MODULE_1__["useRef"])(null);
  Object(react__WEBPACK_IMPORTED_MODULE_1__["useEffect"])(function () {
    var phs = [];

    for (var index = 0; index < 6 - featuredImages.length; index++) {
      var ph = {
        classes: "cursor-pointer m-2 inline-flex bg-transparent text-palette-gray"
      };
      phs.push(ph);
    }

    setFmPlaceHolder(phs);
  }, [featuredImages.length]);
  Object(react__WEBPACK_IMPORTED_MODULE_1__["useEffect"])(function () {
    getDeals();
  }, []);

  var getDeals = /*#__PURE__*/function () {
    var _ref2 = _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee() {
      var url;
      return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee$(_context) {
        while (1) {
          switch (_context.prev = _context.next) {
            case 0:
              url = "".concat(window.location.origin, "/api/deal");
              _context.next = 3;
              return axios({
                method: "get",
                url: url,
                params: {
                  intent: "listings",
                  isProjectLand: false
                }
              }).then(function (result) {
                var array = Object(lodash__WEBPACK_IMPORTED_MODULE_15__["values"])(result.data);
                setLoading(false);
                setDeals(_toConsumableArray(array.map(function (deal) {
                  return {
                    label: "".concat(deal.name),
                    value: String(deal.id)
                  };
                })));
              })["catch"](function (error) {});

            case 3:
            case "end":
              return _context.stop();
          }
        }
      }, _callee);
    }));

    return function getDeals() {
      return _ref2.apply(this, arguments);
    };
  }();

  var handlePreviewImageDisplay = function handlePreviewImageDisplay(e) {
    e.preventDefault();

    if (e.target.files.length > 1) {
      Object(_components_base_alerts_sweetAlert__WEBPACK_IMPORTED_MODULE_17__["sweetAlert"])("error", "Multiple upload is not allowed!");
      return;
    }

    if (!Object(_helpers_fileHelper_fileHelper__WEBPACK_IMPORTED_MODULE_19__["imageFileTypeFilter"])(e.target.files[0]["name"])) {
      Object(_components_base_alerts_sweetAlert__WEBPACK_IMPORTED_MODULE_17__["sweetAlert"])("error", "Filetype is not supported!");
      e.target.value = null;
      return;
    }

    setPreviewImage(previewImage.concat(_toConsumableArray(_toConsumableArray(e.target.files).map(function (file) {
      var newFilename = Object(_helpers_fileHelper_fileHelper__WEBPACK_IMPORTED_MODULE_19__["sanitizeFilename"])(file.name);
      var newFileObj = new File([file], newFilename);
      setPrevFilename(newFilename);
      return {
        image: newFileObj,
        preview: URL.createObjectURL(newFileObj)
      };
    }))));
  };

  var handleSubmit = function handleSubmit(e) {
    e.preventDefault();
    setLimitError(false);
    setLoading(true);
    var formData = new FormData(addPropertyForm.current);
    formData.append("role", "admin");
    formData.append("project", selectedDeal);

    if (floorPlan && !Object(lodash__WEBPACK_IMPORTED_MODULE_15__["isEmpty"])(floorPlan)) {
      formData.append("floorPlan[file]", floorPlan[0]);
    }

    formData.append("projectName", projectName);
    formData.append("projectId", selectedDeal);
    formData.append("propertyType", propertyType);
    formData.append("water", water);
    formData.append("council", council);
    formData.append("strata", strata); // formData.append("state", state);

    formData.append("description", editorState.getCurrentContent().getPlainText());
    formData.append("descriptionHTML", Object(draft_convert__WEBPACK_IMPORTED_MODULE_11__["convertToHTML"])(editorState.getCurrentContent()));
    featuredImages.map(function (image, key) {
      formData.append("featuredImages[".concat(key, "]"), image.image);
    });

    if (previewImage.length) {
      formData.append("preview_img[".concat(0, "]"), previewImage[0].image);
    } else {
      formData.append("preview_img[".concat(0, "]"), "");
    }

    var url = "/api/property/";
    axios({
      method: "post",
      url: url,
      data: formData,
      name: "project",
      headers: {
        "content-type": "multipart/form-data"
      }
    }).then(function (r) {
      if (r.status === 200) {
        Object(_components_base_alerts_sweetAlert__WEBPACK_IMPORTED_MODULE_17__["sweetAlert"])("success", "Property added successfully.");
        setLoading(false);
        setErrors([]);
        addToggleFetch();
        toggleAddPropertyModal();
      }
    })["catch"](function (error) {
      setErrors(error.response.data.errors);
      setLoading(false);
    }).then(function () {});
  };

  var handleDrop = function handleDrop(e) {
    e.preventDefault();
    e.stopPropagation();
    var fmCount = featuredImages.length;
    var onCount = e.dataTransfer.files.length;

    if (fmCount + onCount > 6) {
      e.target.value = null;
      setLimitError(true);
      return;
    } else {
      setLimitError(false);
    }

    setFeaturedImages(featuredImages.concat(_toConsumableArray(_toConsumableArray(e.dataTransfer.files).map(function (file) {
      var newFilename = Object(_helpers_fileHelper_fileHelper__WEBPACK_IMPORTED_MODULE_19__["sanitizeFilename"])(file.name);
      var newFileObj = new File([file], newFilename);
      return {
        image: newFileObj,
        preview: URL.createObjectURL(newFileObj)
      };
    }))));
    e.target.value = null;
  };

  var handleChange = function handleChange(e) {
    var fmCount = featuredImages.length;
    var onCount = e.target.files.length;

    if (fmCount + onCount > 6) {
      setLimitError(true);
      e.target.value = null;
      return;
    } else {
      setLimitError(false);
    }

    setFeaturedImages(featuredImages.concat(_toConsumableArray(_toConsumableArray(e.target.files).map(function (file) {
      return {
        image: file,
        preview: URL.createObjectURL(file)
      };
    }))));
    e.target.value = null;
  };

  var handleRemove = function handleRemove(e, i) {
    e.preventDefault();
    e.stopPropagation();
    setFeaturedImages(_toConsumableArray(featuredImages.filter(function (image, k) {
      if (i !== k) {
        return image;
      }
    })));

    if (featuredImages.length > 6) {
      setLimitError(true);
    } else {
      setLimitError(false);
    }
  };

  var handleFloorPlanChange = function handleFloorPlanChange(e) {
    e.preventDefault();

    if (!Object(_helpers_fileHelper_fileHelper__WEBPACK_IMPORTED_MODULE_19__["PDFFileTypeFilter"])(e.target.files[0]["name"])) {
      Object(_components_base_alerts_sweetAlert__WEBPACK_IMPORTED_MODULE_17__["sweetAlert"])("error", "Filetype is not supported!");
      e.target.value = null;
      return;
    }

    var file = Object.values(e.target.files).map(function (file) {
      var newFilename = file.name.replace(/[^.a-zA-Z0-9]/g, "");
      var newFileObj = new File([file], newFilename);
      return Object.assign(newFileObj, {
        resource: URL.createObjectURL(newFileObj)
      });
    });
    setFLoorPlan(Object(_helpers_propertyHelper_propertyHelper__WEBPACK_IMPORTED_MODULE_6__["floorPlanHelper"])(e.target.files));
    var newFilename = Object(_helpers_fileHelper_fileHelper__WEBPACK_IMPORTED_MODULE_19__["sanitizeFilename"])(e.target.files[0]["name"]);
    setFilename(newFilename); // e.target.value = null;
  };

  var handleDeletePrevFile = function handleDeletePrevFile(e) {
    e.preventDefault();
    setPreviewImage([]);
    previewImageRef.current.type = "";
    previewImageRef.current.type = "file";
    setPrevFilename("");
    e.target.value = null;
  };

  var handleDeleteFile = function handleDeleteFile(e) {
    e.preventDefault();
    setFLoorPlan([]);
    floorPlanRef.current.type = "";
    floorPlanRef.current.type = "file";
    setFilename("");
    e.target.value = null;
  };

  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components_base__WEBPACK_IMPORTED_MODULE_4__["Modal"], {
    show: true,
    maxWidth: "md",
    title: "Add Property",
    onClose: function onClose() {
      return toggleAddPropertyModal();
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("form", {
    ref: addPropertyForm,
    className: "bg-white rounded-lg py-5 px-8 m-5",
    onSubmit: handleSubmit
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "text-black"
  }, loading && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "bg-palette-purple flex flex-1 items-center mb-4 px-2 rounded text-white"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("label", {
    className: "block py-3 font-semibold w-32 mr-2"
  }, "Loading Projects"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_fortawesome_react_fontawesome__WEBPACK_IMPORTED_MODULE_13__["FontAwesomeIcon"], {
    icon: _fortawesome_free_solid_svg_icons__WEBPACK_IMPORTED_MODULE_5__["faSpinner"],
    className: "fa-spin mr-2"
  })), Object(lodash__WEBPACK_IMPORTED_MODULE_15__["isEmpty"])(deals) && !loading && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components_base_alerts_BaseAlert__WEBPACK_IMPORTED_MODULE_18__["default"], {
    message: "You cannot add property at the moment. All projects already have five properties.",
    icon: "x",
    type: "error"
  }), !Object(lodash__WEBPACK_IMPORTED_MODULE_15__["isEmpty"])(deals) && !loading && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "my-4 flex items-center"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("label", {
    className: "block py-3 font-semibold w-32"
  }, "Project Name"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "relative"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(react_dropdown__WEBPACK_IMPORTED_MODULE_2___default.a, {
    id: "deal",
    value: selectedDeal,
    options: deals,
    onChange: function onChange(option) {
      setSelectedDeal(option.value);
    },
    placeholder: "Please select...",
    name: "deal"
  }), errors["project"] && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
    className: "absolute px-1 text-red-400 tracking-widest  text-xs "
  }, errors["project"][0]))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "text-black"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "py-3 flex justify-start "
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "flex justify-center items-center"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("label", {
    className: "font-semibold w-24"
  }, "Property Type"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(react_dropdown__WEBPACK_IMPORTED_MODULE_2___default.a, {
    id: "property_type",
    value: propertyType,
    options: _helpers_propertyHelper_propertyHelper__WEBPACK_IMPORTED_MODULE_6__["property_types"],
    onChange: function onChange(option) {
      setPropertyType(option.value);
    },
    placeholder: "Please select..." // name="property_type"
    ,
    className: "w-48 ml-4"
  }), errors["propertyType"] && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
    className: "px-1 text-red-400 tracking-widest  text-xs "
  }, errors["propertyType"][0]))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "ml-10 flex-1 flex justify-end items-center"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("label", {
    className: "font-semibold w-32 mr-4"
  }, "Property Number"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components_base__WEBPACK_IMPORTED_MODULE_4__["TextInput"], {
    className: "border-gray-400 border pl-2 py-1",
    border: false,
    name: "unit_name",
    width: "w-24"
  }), errors["unit_name"] && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
    className: "px-1 text-red-400 tracking-widest  text-xs "
  }, errors["unit_name"][0]))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "ml-4 flex-1 flex justify-end items-center"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("label", {
    className: "font-semibold w-24 mr-4"
  }, "Lot Number"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components_base__WEBPACK_IMPORTED_MODULE_4__["TextInput"], {
    className: "border-gray-400 border pl-2 py-1",
    border: false,
    name: "unit_no",
    width: "w-24"
  }), errors["unit_no"] && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
    className: "px-1 text-red-400 tracking-widest  text-xs "
  }, errors["unit_no"][0])))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "flex justify-start my-3"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "flex items-center justify-start py-3"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("label", {
    className: "w-12"
  }, "Price"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "relative"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(react_currency_format__WEBPACK_IMPORTED_MODULE_12___default.a, {
    allowNegative: false,
    className: "border text-sm border-gray-400 px-2 py-1 w-48",
    thousandSeparator: true,
    name: "price",
    id: "price",
    prefix: "$"
  })), errors["price"] && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
    className: "absolute px-1 text-red-400 tracking-widest text-xxs "
  }, errors["price"][0]))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "py-3 flex items-center ml-12"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("label", {
    title: "Number of bedrooms",
    className: "cursor-pointer mr-4"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_material_ui_icons__WEBPACK_IMPORTED_MODULE_16__["LocalHotel"], null)), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "flex flex-col w-24"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components_base__WEBPACK_IMPORTED_MODULE_4__["TextInput"], {
    className: "pl-2 text-sm border-gray-400 w-32 border py-1",
    border: false,
    type: "number",
    name: "number_of_bedrooms"
  }), errors["number_of_bedrooms"] && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
    className: "px-1 text-red-400 tracking-widest  text-xxs "
  }, errors["number_of_bedrooms"][0]))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "py-3 flex items-center ml-16"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("label", {
    title: "Number of bathrooms",
    className: "cursor-pointer mr-4"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_material_ui_icons__WEBPACK_IMPORTED_MODULE_16__["Bathtub"], null)), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "flex flex-col w-24"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components_base__WEBPACK_IMPORTED_MODULE_4__["TextInput"], {
    className: "pl-2 text-sm border-gray-400 w-32 border py-1",
    border: false,
    type: "number",
    name: "number_of_bathrooms"
  }), errors["number_of_bathrooms"] && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
    className: "px-1 text-red-400 tracking-widest  text-xxs "
  }, errors["number_of_bathrooms"][0]))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "py-3 flex items-center ml-12"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("label", {
    title: "Number of garages",
    className: "cursor-pointer mr-4 "
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_material_ui_icons__WEBPACK_IMPORTED_MODULE_16__["DirectionsCar"], null)), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "flex flex-col w-24"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components_base__WEBPACK_IMPORTED_MODULE_4__["TextInput"], {
    className: "pl-2 text-sm border-gray-400 w-32 border py-1",
    border: false,
    type: "number",
    name: "number_of_garages",
    min: "1"
  }), errors["number_of_garages"] && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
    className: "px-1 text-red-400 tracking-widest  text-xxs "
  }, errors["number_of_garages"][0])))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "flex"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "py-3 flex justify-start items-center"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("label", {
    className: "w-24"
  }, "Internal SQM"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "relative"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "ml-4 w-32"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components_base__WEBPACK_IMPORTED_MODULE_4__["TextInput"], {
    className: "pl-2 text-sm border-gray-400 w-32 border py-1",
    border: false,
    type: "number",
    name: "internal_size"
  })), errors["internal_size"] && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
    className: "absolute px-1 text-red-400 tracking-widest  text-xxs "
  }, errors["internal_size"][0]))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "py-3 flex justify-start items-center mx-10"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("label", {
    className: "w-24"
  }, "External SQM"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "relative"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "ml-4 w-32"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components_base__WEBPACK_IMPORTED_MODULE_4__["TextInput"], {
    className: "pl-2 text-sm border-gray-400 w-32 border py-1",
    border: false,
    type: "number",
    name: "external_size"
  })), errors["external_size"] && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
    className: "absolute px-1 text-red-400 tracking-widest  text-xxs "
  }, errors["external_size"][0]))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "py-3 flex justify-start items-center ml-3"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("label", {
    className: "w-24"
  }, "Parking SQM"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "relative"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "ml-4 w-32"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components_base__WEBPACK_IMPORTED_MODULE_4__["TextInput"], {
    className: "pl-2 text-sm border-gray-400 w-32 border py-1 mr-1",
    border: false,
    type: "number",
    name: "parking_size"
  })), errors["parking_size"] && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
    className: "absolute px-1 text-red-400 tracking-widest  text-xxs "
  }, errors["parking_size"][0])))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "flex justify-center my-3"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "py-3 flex flex-1 items-center"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("label", {
    title: "Strata",
    className: "cursor-pointer mr-4 ".concat(errors["strata"] ? "mb-4" : "mb-0")
  }, "Strata"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "ml-1"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "flex ml-1"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(react_currency_format__WEBPACK_IMPORTED_MODULE_12___default.a, {
    allowNegative: false,
    className: "border text-sm border-gray-400 px-2 py-1 w-32",
    thousandSeparator: true,
    name: "strata_cost",
    id: "strata_cost",
    prefix: "$"
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(react_dropdown__WEBPACK_IMPORTED_MODULE_2___default.a, {
    id: "strata",
    value: strata,
    options: dates,
    onChange: function onChange(option) {
      setStrata(option.value);
    },
    placeholder: "Please select...",
    name: "strata",
    className: "w-48 ml-2"
  })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", null, errors["strata"] && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
    className: "px-1 text-red-400 tracking-widest  text-xxs "
  }, errors["strata"][0]), errors["strata_cost"] && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
    className: "px-1 text-red-400 tracking-widest  text-xxs "
  }, errors["strata_cost"][0])))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "flex flex-1 items-center justify-end py-3"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("label", {
    title: "Water",
    className: "cursor-pointer mr-4 ".concat(errors["water"] ? "mb-4" : "mb-0")
  }, "Water"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "flex ml-2"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(react_currency_format__WEBPACK_IMPORTED_MODULE_12___default.a, {
    allowNegative: false,
    className: "border text-sm border-gray-400 px-2 py-1 w-32",
    thousandSeparator: true,
    name: "water_cost",
    id: "water_cost",
    prefix: "$"
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(react_dropdown__WEBPACK_IMPORTED_MODULE_2___default.a, {
    id: "water",
    value: water,
    options: dates,
    onChange: function onChange(option) {
      setWater(option.value);
    },
    placeholder: "Please select...",
    name: "water",
    className: "w-48 ml-2"
  })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", null, errors["water"] && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
    className: "px-1 text-red-400 tracking-widest  text-xxs "
  }, errors["water"][0]), errors["water_cost"] && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
    className: "px-1 text-red-400 tracking-widest  text-xxs "
  }, errors["water_cost"][0]))))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "flex justify-center my-3"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "py-3 flex flex-1 items-center"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("label", {
    title: "Council",
    className: "cursor-pointer mr-3 ".concat(errors["council"] ? "mb-4" : "mb-0")
  }, "Council"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "flex ml-1"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(react_currency_format__WEBPACK_IMPORTED_MODULE_12___default.a, {
    allowNegative: false,
    className: "border text-sm border-gray-400 px-2 py-1 w-32",
    thousandSeparator: true,
    name: "council_cost",
    id: "council_cost",
    prefix: "$"
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(react_dropdown__WEBPACK_IMPORTED_MODULE_2___default.a, {
    id: "council",
    value: council,
    options: dates,
    onChange: function onChange(option) {
      setCouncil(option.value);
    },
    placeholder: "Please select...",
    name: "council",
    className: "w-48 ml-2"
  })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", null, errors["council"] && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
    className: "px-1 text-red-400 tracking-widest text-xxs "
  }, errors["council"][0]), errors["council_cost"] && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
    className: "px-1 text-red-400 tracking-widest text-xxs "
  }, errors["council_cost"][0])))))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "py-3"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("label", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "w-full text-sm bg-white text-black "
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components_RichTextEditor__WEBPACK_IMPORTED_MODULE_7__["default"], {
    title: "Property Description",
    richEditorState: editorState,
    richEditorStateContent: function richEditorStateContent(content) {
      return setEditorState(content);
    },
    characterLimit: 2000
  }))), errors["description"] && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
    className: "px-1 text-red-400 tracking-widest  text-xs "
  }, errors["description"][0])), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "py-3"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("h1", {
    className: "font-semibold"
  }, " Featured Images (min. 3 images) "), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("label", {
    className: "button ",
    htmlFor: "upload-photos"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: ""
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "w-full flex flex-wrap  cursor-pointer",
    style: {
      border: "2px dashed #ccc",
      borderRadius: "20px",
      padding: "20px",
      textAlign: "center",
      marginBottom: "0px"
    },
    onDrop: function onDrop(e) {
      return handleDrop(e);
    },
    onDragOver: function onDragOver(e) {
      e.preventDefault();
      e.stopPropagation();
    },
    onDragEnter: function onDragEnter(e) {
      e.preventDefault();
      e.stopPropagation();
    },
    onDragLeave: function onDragLeave(e) {
      e.preventDefault();
      e.stopPropagation();
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("input", {
    type: "file",
    id: "upload-photos",
    accept: "image/*",
    multiple: true,
    className: "hidden",
    name: "image[]",
    onChange: function onChange(e) {
      return handleChange(e);
    }
  }), featuredImages.map(function (image, index) {
    return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
      key: index,
      className: "rounded-sm border m-2 relative",
      style: {
        backgroundImage: "url('".concat(image.preview, "')"),
        backgroundPositionX: "center",
        width: "6.9em",
        height: "8.9rem",
        backgroundSize: "cover"
      }
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components_base__WEBPACK_IMPORTED_MODULE_4__["Tooltip"], {
      title: "Remove this item"
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
      onClick: function onClick(e) {
        return handleRemove(e, index);
      },
      className: "absolute bottom-0 right-0 p-2 -mb-2 "
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_fortawesome_react_fontawesome__WEBPACK_IMPORTED_MODULE_13__["FontAwesomeIcon"], {
      className: "text-red-500 shadow-xl hover:font-bold hover:text-red-700",
      icon: _fortawesome_free_solid_svg_icons__WEBPACK_IMPORTED_MODULE_5__["faTimes"]
    }))));
  }), fmPlaceHolder.map(function (p, i) {
    return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
      key: i,
      className: "".concat(p.classes, " cursor-pointer flex justify-center items-center"),
      style: {
        width: "6.9rem",
        height: "8.9rem",
        border: "2px solid #ccc"
      }
    }, " ", /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_fortawesome_react_fontawesome__WEBPACK_IMPORTED_MODULE_13__["FontAwesomeIcon"], {
      className: "text-xl",
      icon: _fortawesome_free_solid_svg_icons__WEBPACK_IMPORTED_MODULE_5__["faCamera"]
    }), " ");
  })), limitError && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
    className: "px-1 text-red-400 tracking-widest  text-xs "
  }, "Please limit your photos to six items only."))), errors["featuredImages"] && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
    className: "px-1 text-red-400 tracking-widest  text-xs "
  }, errors["featuredImages"][0])), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "flex flex-row"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "w-1/2 mr-2"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("h1", {
    className: "font-semibold"
  }, "Floor Plan "), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "flex flex-col justify-between py-3"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("label", {
    className: "w-full"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "items-center cursor-pointer w-full p-3 rounded-lg flex",
    style: {
      border: "2px dashed #ccc",
      borderRadius: "20px"
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("input", {
    id: "floorPlan",
    ref: floorPlanRef,
    type: "file",
    name: "floor_plan",
    onChange: function onChange(e) {
      return handleFloorPlanChange(e);
    },
    className: "hidden border text-sm border-gray-400 px-2 py-1 w-full py-1"
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("label", {
    htmlFor: "floorPlan",
    className: " cursor-pointer hover:text-palette-blue-light ".concat(filename ? "text-palette-blue-light" : "")
  }, filename || "Select File"), filename && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "text-red-500 text-lg cursor-pointer ml-3"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_fortawesome_react_fontawesome__WEBPACK_IMPORTED_MODULE_13__["FontAwesomeIcon"], {
    onClick: function onClick(e) {
      return handleDeleteFile(e);
    },
    icon: _fortawesome_free_solid_svg_icons__WEBPACK_IMPORTED_MODULE_5__["faTimes"]
  })))), errors["floorPlan"] && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
    className: "px-1 text-red-400 tracking-widest  text-xs "
  }, errors["floorPlan"][0])))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "w-1/2 ml-2"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("h1", {
    className: "font-semibold"
  }, "Rich Text Preview "), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "flex flex-col justify-between py-3"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "flex"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("label", {
    className: "w-full"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "items-center cursor-pointer w-full p-3 rounded-lg flex",
    style: {
      border: "2px dashed #ccc",
      borderRadius: "20px"
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("input", {
    id: "rtp",
    ref: previewImageRef,
    type: "file",
    name: "prev_image",
    onChange: function onChange(e) {
      return handlePreviewImageDisplay(e);
    },
    className: "hidden border text-sm border-gray-400 px-2 py-1 w-full py-1"
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("label", {
    htmlFor: "rtp",
    className: " cursor-pointer hover:text-palette-blue-light ".concat(prevFilename ? "text-palette-blue-light" : "")
  }, prevFilename || "Select File"), prevFilename && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "text-red-500 text-lg cursor-pointer ml-3"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_fortawesome_react_fontawesome__WEBPACK_IMPORTED_MODULE_13__["FontAwesomeIcon"], {
    onClick: function onClick(e) {
      return handleDeletePrevFile(e);
    },
    icon: _fortawesome_free_solid_svg_icons__WEBPACK_IMPORTED_MODULE_5__["faTimes"]
  })))))))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "flex justify-end my-5"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components_base__WEBPACK_IMPORTED_MODULE_4__["Button"], {
    className: "font-bold rounded-full",
    disabled: loading
  }, loading && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_fortawesome_react_fontawesome__WEBPACK_IMPORTED_MODULE_13__["FontAwesomeIcon"], {
    icon: _fortawesome_free_solid_svg_icons__WEBPACK_IMPORTED_MODULE_5__["faSpinner"],
    className: "fa-spin mr-2"
  }), "Save")))));
});

/***/ }),

/***/ "./resources/js/admin/components/AdminPropertyManager.js":
/*!***************************************************************!*\
  !*** ./resources/js/admin/components/AdminPropertyManager.js ***!
  \***************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _EditPropertyModal__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./EditPropertyModal */ "./resources/js/admin/components/EditPropertyModal.js");
/* harmony import */ var _AddProperty__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./AddProperty */ "./resources/js/admin/components/AddProperty.js");
/* harmony import */ var _AddLandProperty__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./AddLandProperty */ "./resources/js/admin/components/AddLandProperty.js");
/* harmony import */ var _EditLandPropertyModal__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./EditLandPropertyModal */ "./resources/js/admin/components/EditLandPropertyModal.js");
/* harmony import */ var _states_userGlobal__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../states/userGlobal */ "./resources/js/states/userGlobal.js");
/* harmony import */ var _components_base_alerts_sweetAlert__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../components/_base/alerts/sweetAlert */ "./resources/js/components/_base/alerts/sweetAlert.js");
/* harmony import */ var _components_base_alerts_BaseAlert__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../components/_base/alerts/BaseAlert */ "./resources/js/components/_base/alerts/BaseAlert.js");
/* harmony import */ var _components_base__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../../components/_base */ "./resources/js/components/_base/index.js");
/* harmony import */ var _components_base__WEBPACK_IMPORTED_MODULE_8___default = /*#__PURE__*/__webpack_require__.n(_components_base__WEBPACK_IMPORTED_MODULE_8__);
function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _unsupportedIterableToArray(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

function _iterableToArrayLimit(arr, i) { if (typeof Symbol === "undefined" || !(Symbol.iterator in Object(arr))) return; var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }











var AdminPropertyManager = function AdminPropertyManager(_ref) {
  var toggleSEM = _ref.toggleSEM,
      toggleFetch = _ref.toggleFetch,
      toggleAgreementView = _ref.toggleAgreementView;

  var _UserGlobal = Object(_states_userGlobal__WEBPACK_IMPORTED_MODULE_5__["default"])(),
      _UserGlobal2 = _slicedToArray(_UserGlobal, 2),
      userState = _UserGlobal2[0],
      userActions = _UserGlobal2[1];

  var _useState = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])([]),
      _useState2 = _slicedToArray(_useState, 2),
      properties = _useState2[0],
      setProperties = _useState2[1];

  var _useState3 = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])([]),
      _useState4 = _slicedToArray(_useState3, 2),
      property = _useState4[0],
      setProperty = _useState4[1];

  var _useState5 = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])(false),
      _useState6 = _slicedToArray(_useState5, 2),
      toggleEditProperty = _useState6[0],
      setToggleEditProperty = _useState6[1];

  var _useState7 = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])(false),
      _useState8 = _slicedToArray(_useState7, 2),
      toggleAP = _useState8[0],
      setToggleAP = _useState8[1];

  var _useState9 = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])(false),
      _useState10 = _slicedToArray(_useState9, 2),
      toogleLandProperty = _useState10[0],
      setToogleLandProperty = _useState10[1];

  var _useState11 = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])(false),
      _useState12 = _slicedToArray(_useState11, 2),
      toogleEditLandProperty = _useState12[0],
      setToogleEditLandProperty = _useState12[1];

  var _useState13 = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])(userState.projectData.sub_property_id == 4 ? true : false),
      _useState14 = _slicedToArray(_useState13, 2),
      isProjectLand = _useState14[0],
      setIsprojectLand = _useState14[1];

  Object(react__WEBPACK_IMPORTED_MODULE_0__["useEffect"])(function () {
    setProperties(userState.projectData.properties);
    userActions.setState({
      propertiesData: userState.projectData.properties
    });
  }, []);

  var nextStep = function nextStep() {
    userActions.setState({
      currentProjStep: 4
    });
    toggleAgreementView();
  };

  var _toggleEditPropertyModal = function toggleEditPropertyModal() {
    if (!isProjectLand) {
      setToggleEditProperty(!toggleEditProperty);
    } else {
      setToogleEditLandProperty(!toogleEditLandProperty);
    }
  };

  var _toggleAddPropertyModal = function toggleAddPropertyModal() {
    if (userState.propertiesData.length >= 5) {
      return Object(_components_base_alerts_sweetAlert__WEBPACK_IMPORTED_MODULE_6__["sweetAlert"])("error", "You can only add up to 5 properties");
    }

    if (!isProjectLand) {
      setToggleAP(!toggleAP);
    } else {
      setToogleLandProperty(!toogleLandProperty);
    }
  };

  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "relative bg-white py-5 m-5 rounded-lg"
  }, toggleEditProperty && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_EditPropertyModal__WEBPACK_IMPORTED_MODULE_1__["default"], {
    projectName: userState.projectData.name,
    projectId: userState.projectData.id,
    property: property,
    toggleEditPropertyModal: function toggleEditPropertyModal() {
      return _toggleEditPropertyModal();
    },
    editToggleFetch: toggleFetch
  }), toogleEditLandProperty && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_EditLandPropertyModal__WEBPACK_IMPORTED_MODULE_4__["default"], {
    projectName: userState.projectData.name,
    projectId: userState.projectData.id,
    property: property,
    toggleEditPropertyModal: function toggleEditPropertyModal() {
      return _toggleEditPropertyModal();
    },
    editToggleFetch: toggleFetch
  }), toggleAP && userState.propertiesData.length < 5 && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_AddProperty__WEBPACK_IMPORTED_MODULE_2__["default"], {
    projectName: userState.projectData.name,
    projectId: userState.projectData.id,
    toggleAddPropertyModal: function toggleAddPropertyModal() {
      return _toggleAddPropertyModal();
    },
    addToggleFetch: toggleFetch
  }), toogleLandProperty && userState.propertiesData.length < 5 && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_AddLandProperty__WEBPACK_IMPORTED_MODULE_3__["default"], {
    projectName: userState.projectData.name,
    projectId: userState.projectData.id,
    toggleAddPropertyModal: function toggleAddPropertyModal() {
      return _toggleAddPropertyModal();
    },
    addToggleFetch: toggleFetch
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "flex justify-around flex-wrap "
  }, userState.propertiesData && userState.propertiesData.map(function (property, k) {
    return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: "cursor-pointer text-palette-gray hover:bg-gray-200 hover:text-gray-800 ",
      onClick: function onClick() {
        setProperty(property);

        _toggleEditPropertyModal();
      },
      key: property.id
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(PropertyInfo, {
      pId: k + 1,
      property: property
    }));
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    onClick: function onClick() {
      return _toggleAddPropertyModal();
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(PropertyPlaceholder, {
    properties: properties
  }))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "flex flex-1 px-3 ".concat(userState.propertiesData.length < 5 ? "justify-end mr-16" : "")
  }, userState.propertiesData.length == 5 && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_base_alerts_BaseAlert__WEBPACK_IMPORTED_MODULE_7__["default"], {
    message: "You have reached the maximum 5 properties per project. Once all 5 properties are sold you can upload another 5 properties.",
    icon: "check",
    type: "success",
    iconHeight: "h-6",
    iconWidth: "w-8",
    className: "flex items-center mb-2 px-6 py-2 rounded w-9/12"
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_base__WEBPACK_IMPORTED_MODULE_8__["Button"], {
    className: "button button-primary font-bold h-10 ml-10 mt-3 rounded rounded-full",
    onClick: function onClick() {
      return nextStep();
    },
    disabled: userState.propertiesData.length < 5
  }, "Next Step")));
};

var PropertyPlaceholder = function PropertyPlaceholder(_ref2) {
  var properties = _ref2.properties,
      toggleAPM = _ref2.toggleAPM;

  var _UserGlobal3 = Object(_states_userGlobal__WEBPACK_IMPORTED_MODULE_5__["default"])(),
      _UserGlobal4 = _slicedToArray(_UserGlobal3, 2),
      userState = _UserGlobal4[0],
      userActions = _UserGlobal4[1];

  var _useState15 = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])([]),
      _useState16 = _slicedToArray(_useState15, 2),
      placeHolder = _useState16[0],
      setPlaceHolder = _useState16[1];

  Object(react__WEBPACK_IMPORTED_MODULE_0__["useEffect"])(function () {
    var phs = [];

    for (var index = 0; index < 5 - userState.propertiesData.length; index++) {
      var ph = /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        key: index,
        className: "cursor-pointer m-5 inline-flex bg-transparent text-palette-blue-light",
        style: {
          width: "7rem",
          height: "7rem",
          border: "2px dashed #ccc",
          borderRadius: "5px"
        }
      });
      phs.push(ph);
    }

    setPlaceHolder(phs);
  }, [userState.propertiesData.length]);
  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", null, placeHolder);
};

var PropertyInfo = function PropertyInfo(_ref3) {
  var pId = _ref3.pId,
      property = _ref3.property;
  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: " m-5"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: " bg-transparent rounded-lg border border-gray-400",
    style: {
      backgroundImage: "url('".concat(property.featured_images[0], "')"),
      backgroundPositionX: "center",
      backgroundSize: "cover",
      width: "7rem",
      height: "7rem"
    }
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
    className: "p-2  text-xs"
  }, "Property #".concat(pId, " - ").concat(property.unit_name))));
};

/* harmony default export */ __webpack_exports__["default"] = (AdminPropertyManager);

/***/ }),

/***/ "./resources/js/admin/components/AgencyAgreementPart.js":
/*!**************************************************************!*\
  !*** ./resources/js/admin/components/AgencyAgreementPart.js ***!
  \**************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);


var AgencyAgreementPart = function AgencyAgreementPart(_ref) {
  var toggleSEM = _ref.toggleSEM,
      toggleFetch = _ref.toggleFetch;
  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "relative bg-white py-5 px-5 flex flex-col items-center m-5 rounded-lg"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
    src: "/assets/images/undraw_fill_forms_yltj.svg",
    height: "250px",
    width: "420px",
    style: {
      marginTop: '24px'
    }
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("h4", {
    className: "font-bold lg:w-3/4 w-full px-1 py-3 rounded-t text-2xl text-center"
  }, "Your project will be approved by a GroupBuyer Admin Agent prior to being displayed online. Should your project not meet the requirements of GroupBuyer, a team member will be in contact with you. Click the 'View Agency Agreement' button to learn more about our terms and conditions."), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("a", {
    href: "/assets/pdf/group-buyer-agency-agreement-2020-10-08-draft.pdf",
    target: "_blank",
    className: "border border-gray-900 button py-3 flex items-center justify-around mr-2 mt-2 mb-2"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
    className: "text-base text-gray-900"
  }, "View Agency Agreement")));
};

/* harmony default export */ __webpack_exports__["default"] = (AgencyAgreementPart);

/***/ }),

/***/ "./resources/js/admin/components/Content.js":
/*!**************************************************!*\
  !*** ./resources/js/admin/components/Content.js ***!
  \**************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react_router_dom__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react-router-dom */ "./node_modules/react-router-dom/esm/react-router-dom.js");
/* harmony import */ var react_loadable__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! react-loadable */ "./node_modules/react-loadable/lib/index.js");
/* harmony import */ var react_loadable__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(react_loadable__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _pages_Projects__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../pages/Projects */ "./resources/js/admin/pages/Projects.js");
/* harmony import */ var _pages_Properties__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../pages/Properties */ "./resources/js/admin/pages/Properties.js");
/* harmony import */ var _pages_ManageProjectProperties__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../pages/ManageProjectProperties */ "./resources/js/admin/pages/ManageProjectProperties.js");
/* harmony import */ var _pages_SecuredDealsListPage__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../pages/SecuredDealsListPage */ "./resources/js/pages/SecuredDealsListPage.js");
/* harmony import */ var _pages_DeveloperList__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../pages/DeveloperList */ "./resources/js/admin/pages/DeveloperList.js");









var Content = function Content() {
  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "h-full flex-1 p-8 bg-gray-200"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_router_dom__WEBPACK_IMPORTED_MODULE_1__["Switch"], null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_router_dom__WEBPACK_IMPORTED_MODULE_1__["Route"], {
    exact: true,
    path: "/projects",
    component: function component() {
      return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_pages_Projects__WEBPACK_IMPORTED_MODULE_3__["default"], null);
    }
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_router_dom__WEBPACK_IMPORTED_MODULE_1__["Route"], {
    exact: true,
    path: "/properties",
    component: function component() {
      return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_pages_Properties__WEBPACK_IMPORTED_MODULE_4__["default"], null);
    }
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_router_dom__WEBPACK_IMPORTED_MODULE_1__["Route"], {
    exact: true,
    path: "/manage-properties/:dealId/:dealSubPropertyId",
    component: function component(routeParams) {
      return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_pages_ManageProjectProperties__WEBPACK_IMPORTED_MODULE_5__["default"], {
        dealId: parseInt(routeParams.match.params.dealId),
        dealSubPropertyId: parseInt(routeParams.match.params.dealSubPropertyId)
      });
    }
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_router_dom__WEBPACK_IMPORTED_MODULE_1__["Route"], {
    exact: true,
    path: "/secured-deals",
    component: function component(routeParams) {
      return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_pages_SecuredDealsListPage__WEBPACK_IMPORTED_MODULE_6__["default"], {
        isAdmin: true
      });
    }
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_router_dom__WEBPACK_IMPORTED_MODULE_1__["Route"], {
    exact: true,
    path: "/developers",
    component: function component(routeParams) {
      return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_pages_DeveloperList__WEBPACK_IMPORTED_MODULE_7__["default"], null);
    }
  })));
};

/* harmony default export */ __webpack_exports__["default"] = (/*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.memo(Content));

/***/ }),

/***/ "./resources/js/admin/components/EditLandPropertyModal.js":
/*!****************************************************************!*\
  !*** ./resources/js/admin/components/EditLandPropertyModal.js ***!
  \****************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime/regenerator */ "./node_modules/@babel/runtime/regenerator/index.js");
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var react_dropdown__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! react-dropdown */ "./node_modules/react-dropdown/dist/index.js");
/* harmony import */ var react_dropdown__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(react_dropdown__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _helpers_styles_dropdown_css__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../helpers/styles/dropdown.css */ "./resources/js/helpers/styles/dropdown.css");
/* harmony import */ var _helpers_styles_dropdown_css__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(_helpers_styles_dropdown_css__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var _components_base__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../components/_base */ "./resources/js/components/_base/index.js");
/* harmony import */ var _components_base__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(_components_base__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var draft_js__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! draft-js */ "./node_modules/draft-js/lib/Draft.js");
/* harmony import */ var draft_js__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(draft_js__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var _helpers_styles_datepicker_css__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../helpers/styles/datepicker.css */ "./resources/js/helpers/styles/datepicker.css");
/* harmony import */ var _helpers_styles_datepicker_css__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(_helpers_styles_datepicker_css__WEBPACK_IMPORTED_MODULE_6__);
/* harmony import */ var draft_convert__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! draft-convert */ "./node_modules/draft-convert/esm/index.js");
/* harmony import */ var react_currency_format__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! react-currency-format */ "./node_modules/react-currency-format/lib/currency-format.js");
/* harmony import */ var react_currency_format__WEBPACK_IMPORTED_MODULE_8___default = /*#__PURE__*/__webpack_require__.n(react_currency_format__WEBPACK_IMPORTED_MODULE_8__);
/* harmony import */ var _fortawesome_react_fontawesome__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @fortawesome/react-fontawesome */ "./node_modules/@fortawesome/react-fontawesome/index.es.js");
/* harmony import */ var _fortawesome_free_solid_svg_icons__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @fortawesome/free-solid-svg-icons */ "./node_modules/@fortawesome/free-solid-svg-icons/index.es.js");
/* harmony import */ var _states_userGlobal__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ../../states/userGlobal */ "./resources/js/states/userGlobal.js");
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! lodash */ "./node_modules/lodash/lodash.js");
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_12___default = /*#__PURE__*/__webpack_require__.n(lodash__WEBPACK_IMPORTED_MODULE_12__);
/* harmony import */ var _components_base_alerts_sweetAlert__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ../../components/_base/alerts/sweetAlert */ "./resources/js/components/_base/alerts/sweetAlert.js");
/* harmony import */ var _helpers_propertyHelper_propertyHelper__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ../../helpers/propertyHelper/propertyHelper */ "./resources/js/helpers/propertyHelper/propertyHelper.js");
/* harmony import */ var _data_index__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! ../../data/index */ "./resources/js/data/index.js");
/* harmony import */ var _helpers_fileHelper_fileHelper__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! ../../helpers/fileHelper/fileHelper */ "./resources/js/helpers/fileHelper/fileHelper.js");
/* harmony import */ var _helpers_resourceHelper_index__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! ../../helpers/resourceHelper/index */ "./resources/js/helpers/resourceHelper/index.js");


function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

function _toConsumableArray(arr) { return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _unsupportedIterableToArray(arr) || _nonIterableSpread(); }

function _nonIterableSpread() { throw new TypeError("Invalid attempt to spread non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _iterableToArray(iter) { if (typeof Symbol !== "undefined" && Symbol.iterator in Object(iter)) return Array.from(iter); }

function _arrayWithoutHoles(arr) { if (Array.isArray(arr)) return _arrayLikeToArray(arr); }

function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _unsupportedIterableToArray(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

function _iterableToArrayLimit(arr, i) { if (typeof Symbol === "undefined" || !(Symbol.iterator in Object(arr))) return; var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }



















/* harmony default export */ __webpack_exports__["default"] = (function (_ref) {
  var projectName = _ref.projectName,
      projectId = _ref.projectId,
      property = _ref.property,
      toggleEditPropertyModal = _ref.toggleEditPropertyModal,
      editToggleFetch = _ref.editToggleFetch;

  var _UserGlobal = Object(_states_userGlobal__WEBPACK_IMPORTED_MODULE_11__["default"])(),
      _UserGlobal2 = _slicedToArray(_UserGlobal, 2),
      userState = _UserGlobal2[0],
      userActions = _UserGlobal2[1];

  var _useState = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(_toConsumableArray(property.featured_images.map(function (image) {
    return {
      image: image,
      preview: image
    };
  }))),
      _useState2 = _slicedToArray(_useState, 2),
      featuredImages = _useState2[0],
      setFeaturedImages = _useState2[1];

  var previewImageRef = Object(react__WEBPACK_IMPORTED_MODULE_1__["useRef"])(null);
  var floorPlanRef = Object(react__WEBPACK_IMPORTED_MODULE_1__["useRef"])(null);

  var _useState3 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(""),
      _useState4 = _slicedToArray(_useState3, 2),
      state = _useState4[0],
      setState = _useState4[1];

  var _useState5 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(String(property.property_type_id)),
      _useState6 = _slicedToArray(_useState5, 2),
      propertyType = _useState6[0],
      setPropertyType = _useState6[1];

  var _useState7 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(false),
      _useState8 = _slicedToArray(_useState7, 2),
      loading = _useState8[0],
      setLoading = _useState8[1];

  var _useState9 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(""),
      _useState10 = _slicedToArray(_useState9, 2),
      subPropertyType = _useState10[0],
      setSubPropertyType = _useState10[1];

  var _useState11 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(""),
      _useState12 = _slicedToArray(_useState11, 2),
      ownershipType = _useState12[0],
      setOwnershipType = _useState12[1];

  var _useState13 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])([]),
      _useState14 = _slicedToArray(_useState13, 2),
      errors = _useState14[0],
      setErrors = _useState14[1];

  var _useState15 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(function () {
    return draft_js__WEBPACK_IMPORTED_MODULE_5__["EditorState"].createEmpty();
  }),
      _useState16 = _slicedToArray(_useState15, 2),
      editorState = _useState16[0],
      setEditorState = _useState16[1];

  var _useState17 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])({}),
      _useState18 = _slicedToArray(_useState17, 2),
      floorPlan = _useState18[0],
      setFLoorPlan = _useState18[1];

  var _useState19 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(""),
      _useState20 = _slicedToArray(_useState19, 2),
      filename = _useState20[0],
      setFilename = _useState20[1];

  var _useState21 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(""),
      _useState22 = _slicedToArray(_useState21, 2),
      prevFilename = _useState22[0],
      setPrevFilename = _useState22[1];

  var _useState23 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])([]),
      _useState24 = _slicedToArray(_useState23, 2),
      previewImage = _useState24[0],
      setPreviewImage = _useState24[1];

  var _useState25 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(""),
      _useState26 = _slicedToArray(_useState25, 2),
      propertyPrice = _useState26[0],
      setPropertyPrice = _useState26[1];

  var _useState27 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(false),
      _useState28 = _slicedToArray(_useState27, 2),
      limitError = _useState28[0],
      setLimitError = _useState28[1];

  var _useState29 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])([]),
      _useState30 = _slicedToArray(_useState29, 2),
      fmPlaceHolder = _useState30[0],
      setFmPlaceHolder = _useState30[1];

  var _useState31 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(_helpers_propertyHelper_propertyHelper__WEBPACK_IMPORTED_MODULE_14__["costDueDates"]),
      _useState32 = _slicedToArray(_useState31, 2),
      dates = _useState32[0],
      setDates = _useState32[1];

  var addPropertyForm = Object(react__WEBPACK_IMPORTED_MODULE_1__["useRef"])(null);
  Object(react__WEBPACK_IMPORTED_MODULE_1__["useEffect"])(function () {
    var phs = [];

    for (var index = 0; index < 6 - featuredImages.length; index++) {
      var ph = {
        classes: "cursor-pointer m-2 inline-flex bg-transparent text-palette-gray"
      };
      phs.push(ph);
    }

    setFmPlaceHolder(phs);
  }, [featuredImages.length]);
  Object(react__WEBPACK_IMPORTED_MODULE_1__["useEffect"])(function () {
    setEditorState(function () {
      return draft_js__WEBPACK_IMPORTED_MODULE_5__["EditorState"].push(editorState, Object(draft_convert__WEBPACK_IMPORTED_MODULE_7__["convertFromHTML"])(property.description));
    });
  }, []);
  Object(react__WEBPACK_IMPORTED_MODULE_1__["useEffect"])(function () {
    if (property.rich_preview_image) {
      var img = [];
      img.push({
        image: property.rich_preview_image,
        preview: property.rich_preview_image
      });
      var newFilename = property.rich_preview_image.split("/imgPreview/")[1];
      setPrevFilename(newFilename);
      setPreviewImage(img);
    }

    if (property.price) {
      setPropertyPrice(property.price);
    }
  }, []);

  var handleSubmit = function handleSubmit(e) {
    e.preventDefault();
    setLimitError(false);
    saveProperty();
  };

  var saveProperty = /*#__PURE__*/function () {
    var _ref2 = _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee() {
      var formData, url;
      return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee$(_context) {
        while (1) {
          switch (_context.prev = _context.next) {
            case 0:
              setLoading(true);
              formData = new FormData(addPropertyForm.current);
              formData.append("role", "project_developer");

              if (floorPlan && !Object(lodash__WEBPACK_IMPORTED_MODULE_12__["isEmpty"])(floorPlan)) {
                formData.append("floorPlan[file]", floorPlan.file);
                formData.append("floorPlan[name]", "floorplan");
              }

              formData.append("projectId", projectId);
              formData.append("propertyType", propertyType);
              formData.append("description", editorState.getCurrentContent().getPlainText());
              formData.append("descriptionHTML", Object(draft_convert__WEBPACK_IMPORTED_MODULE_7__["convertToHTML"])(editorState.getCurrentContent()));
              featuredImages.forEach(function (image, key) {
                formData.append("featuredImages[".concat(key, "]"), image.image);
              });

              if (previewImage.length) {
                formData.append("preview_img[".concat(0, "]"), previewImage[0].image);
              } else {
                formData.append("preview_img[".concat(0, "]"), "");
              }

              formData.append("_method", "PATCH");
              url = "/api/property/".concat(property.id);
              _context.next = 14;
              return Object(_data_index__WEBPACK_IMPORTED_MODULE_15__["saveUpdateProperty"])({
                formData: formData,
                url: url
              }).then(function (retVal) {
                if (retVal.status === 200) {
                  userActions.setState({
                    propertiesData: retVal.data
                  });
                  setLoading(false);
                  Object(_components_base_alerts_sweetAlert__WEBPACK_IMPORTED_MODULE_13__["sweetAlert"])("success", "Property successfully updated.");
                  editToggleFetch(); // toggleEditPropertyModal();
                }
              })["catch"](function (error) {
                setErrors(error.response.data.errors);
                setLoading(false);
              }).then(function () {});

            case 14:
            case "end":
              return _context.stop();
          }
        }
      }, _callee);
    }));

    return function saveProperty() {
      return _ref2.apply(this, arguments);
    };
  }();

  var handlePreviewImageDisplay = function handlePreviewImageDisplay(e) {
    e.preventDefault();

    if (e.target.files.length > 1) {
      Object(_components_base_alerts_sweetAlert__WEBPACK_IMPORTED_MODULE_13__["sweetAlert"])("error", "Multiple upload is not allowed!");
      return;
    }

    if (!Object(_helpers_fileHelper_fileHelper__WEBPACK_IMPORTED_MODULE_16__["imageFileTypeFilter"])(e.target.files[0]["name"])) {
      Object(_components_base_alerts_sweetAlert__WEBPACK_IMPORTED_MODULE_13__["sweetAlert"])("error", "Filetype is not supported!");
      e.target.value = null;
      return;
    }

    var imgArr = [];
    setPreviewImage(imgArr.concat(_toConsumableArray(_toConsumableArray(e.target.files).map(function (file) {
      var newFilename = Object(_helpers_fileHelper_fileHelper__WEBPACK_IMPORTED_MODULE_16__["sanitizeFilename"])(file.name);
      var newFileObj = new File([file], newFilename);
      setPrevFilename(newFilename);
      return {
        image: newFileObj,
        preview: URL.createObjectURL(newFileObj)
      };
    }))));
  };

  var handleDrop = function handleDrop(e) {
    e.preventDefault();
    e.stopPropagation();
    var fmCount = featuredImages.length;
    var onCount = e.dataTransfer.files.length;

    if (fmCount + onCount > 6) {
      e.target.value = null;
      setLimitError(true);
      return;
    } else {
      setLimitError(false);
    }

    setFeaturedImages(featuredImages.concat(_toConsumableArray(_toConsumableArray(e.dataTransfer.files).map(function (file) {
      var newFilename = Object(_helpers_fileHelper_fileHelper__WEBPACK_IMPORTED_MODULE_16__["sanitizeFilename"])(file.name);
      var newFileObj = new File([file], newFilename);
      return {
        image: newFileObj,
        preview: URL.createObjectURL(newFileObj)
      };
    }))));
    e.target.value = null;
  };

  var handleChange = function handleChange(e) {
    var fmCount = featuredImages.length;
    var onCount = e.target.files.length;

    if (fmCount + onCount > 6) {
      setLimitError(true);
      e.target.value = null;
      return;
    } else {
      setLimitError(false);
    }

    setFeaturedImages(featuredImages.concat(_toConsumableArray(_toConsumableArray(e.target.files).map(function (file) {
      var newFilename = Object(_helpers_fileHelper_fileHelper__WEBPACK_IMPORTED_MODULE_16__["sanitizeFilename"])(file.name);
      var newFileObj = new File([file], newFilename);
      return {
        image: newFileObj,
        preview: URL.createObjectURL(newFileObj)
      };
    }))));
    e.target.value = null;
  };

  var handleRemove = function handleRemove(e, i) {
    e.preventDefault();
    e.stopPropagation();
    setFeaturedImages(_toConsumableArray(featuredImages.filter(function (image, k) {
      if (i !== k) {
        return image;
      }
    })));

    if (featuredImages.length > 6) {
      setLimitError(true);
    } else {
      setLimitError(false);
    }
  };

  var handleFloorPlanChange = function handleFloorPlanChange(e) {
    e.preventDefault();

    if (!Object(_helpers_fileHelper_fileHelper__WEBPACK_IMPORTED_MODULE_16__["PDFFileTypeFilter"])(e.target.files[0]["name"])) {
      Object(_components_base_alerts_sweetAlert__WEBPACK_IMPORTED_MODULE_13__["sweetAlert"])("error", "Filetype is not supported!");
      e.target.value = null;
      return;
    }

    setFLoorPlan({
      file: _helpers_resourceHelper_index__WEBPACK_IMPORTED_MODULE_17__["resources"].trimFloorPlan(e.target.files[0]),
      name: "floor_plan"
    });
    var newFilename = Object(_helpers_fileHelper_fileHelper__WEBPACK_IMPORTED_MODULE_16__["sanitizeFilename"])(e.target.files[0]["name"]);
    setFilename(newFilename); // e.target.value = null;
  };

  var handleDeleteFile = function handleDeleteFile(e) {
    e.preventDefault();
    setFLoorPlan({});
    floorPlanRef.current.type = "";
    floorPlanRef.current.type = "file";
    setFilename("");
    e.target.value = null;
  };

  var handleDeletePrevFile = function handleDeletePrevFile(e) {
    e.preventDefault();
    setPreviewImage([]);
    previewImageRef.current.type = "";
    previewImageRef.current.type = "file";
    setPrevFilename("");
    e.target.value = null;
  };

  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components_base__WEBPACK_IMPORTED_MODULE_4__["Modal"], {
    show: true,
    maxWidth: "md",
    title: "Edit Land Property",
    onClose: function onClose() {
      return toggleEditPropertyModal();
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("form", {
    ref: addPropertyForm,
    className: "bg-white rounded-lg py-5 px-8 m-5",
    onSubmit: handleSubmit
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "text-black "
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "py-3 flex justify-start "
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "flex justify-center items-center"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("label", {
    className: "font-semibold w-24"
  }, "Property Type"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(react_dropdown__WEBPACK_IMPORTED_MODULE_2___default.a, {
    id: "property_type",
    value: propertyType,
    options: _helpers_propertyHelper_propertyHelper__WEBPACK_IMPORTED_MODULE_14__["property_types"],
    onChange: function onChange(option) {
      setPropertyType(option.value);
    },
    placeholder: "Please select...",
    name: "property_type",
    className: "w-48 ml-4",
    disabled: true
  }), errors["propertyType"] && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
    className: "px-1 text-red-400 tracking-widest  text-xs "
  }, errors["propertyType"][0]))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "ml-10 flex-1 flex justify-end items-center"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("label", {
    className: "font-semibold w-32"
  }, "Lot Number"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components_base__WEBPACK_IMPORTED_MODULE_4__["TextInput"], {
    defaultValue: property.unit_name,
    className: "border-gray-400 border pl-2 py-1",
    border: false,
    name: "unit_name",
    width: "w-24 ml-4"
  }), errors["unit_name"] && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
    className: "px-1 text-red-400 tracking-widest  text-xs "
  }, errors["unit_name"][0]))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "ml-4 flex-1 flex justify-end items-center"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("label", {
    className: "font-semibold w-12"
  }, "Size"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components_base__WEBPACK_IMPORTED_MODULE_4__["TextInput"], {
    className: "border-gray-400 border pl-2 py-1",
    border: false,
    name: "size",
    defaultValue: property.size,
    width: "w-24"
  }), errors["size"] && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
    className: "px-1 text-red-400 tracking-widest  text-xs "
  }, errors["size"][0])))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "flex justify-start my-3"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "flex items-center justify-start py-3"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("label", {
    className: "w-12"
  }, "Price"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "relative"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(react_currency_format__WEBPACK_IMPORTED_MODULE_8___default.a, {
    allowNegative: false,
    className: "border text-sm border-gray-400 px-2 py-1 w-48 ".concat(property.is_property_approved ? "bg-gray-300 cursor-not-allowed" : ""),
    thousandSeparator: true,
    value: propertyPrice,
    onValueChange: function onValueChange(e) {
      setPropertyPrice(e.value);
    },
    name: "price",
    id: "price",
    prefix: "$",
    readOnly: property.is_property_approved
  })), errors["price"] && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
    className: "absolute px-1 text-red-400 tracking-widest text-xs "
  }, errors["price"][0]))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "py-3 flex justify-start items-center ml-10"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("label", {
    className: ""
  }, "Frontage"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "relative"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "ml-4 w-24"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components_base__WEBPACK_IMPORTED_MODULE_4__["TextInput"], {
    className: "pl-2 text-sm border-gray-400 w-32 border py-1",
    border: false // type={`number`}
    ,
    name: "frontage",
    defaultValue: property.frontage
  })), errors["frontage"] && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
    className: "absolute px-1 text-red-400 tracking-widest  text-xs "
  }, errors["frontage"][0]))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "py-3 flex justify-start items-center ml-8"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("label", {
    className: ""
  }, "Width"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "relative"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "ml-4 w-24"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components_base__WEBPACK_IMPORTED_MODULE_4__["TextInput"], {
    className: "pl-2 text-sm border-gray-400 w-32 border py-1",
    border: false // type={`number`}
    ,
    name: "width",
    defaultValue: property.width
  })), errors["width"] && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
    className: "absolute px-1 text-red-400 tracking-widest  text-xs "
  }, errors["width"][0]))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "py-3 flex justify-start items-center ml-8"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("label", {
    className: ""
  }, "Depth "), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "relative"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "ml-4 w-24"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components_base__WEBPACK_IMPORTED_MODULE_4__["TextInput"], {
    className: "pl-2 text-sm border-gray-400 w-32 border py-1 mr-1",
    border: false // type={`number`}
    ,
    name: "depth",
    defaultValue: property.depth
  })), errors["depth"] && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
    className: "absolute px-1 text-red-400 tracking-widest  text-xs "
  }, errors["depth"][0])))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "py-3"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "w-full text-sm bg-white text-black "
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components_base__WEBPACK_IMPORTED_MODULE_4__["RichTextEditor"], {
    key: property.id,
    title: "Property Description",
    richEditorState: editorState,
    richEditorStateContent: function richEditorStateContent(content) {
      return setEditorState(content);
    },
    characterLimit: 2000
  })), errors["description"] && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
    className: "px-1 text-red-400 tracking-widest  text-xs "
  }, errors["description"][0])), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "py-3"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("h1", {
    className: "font-semibold"
  }, " Featured Images (min. 3 images) "), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("label", {
    className: "button ",
    htmlFor: "upload-photos"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: ""
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "w-full flex flex-wrap  cursor-pointer",
    style: {
      border: "2px dashed #ccc",
      borderRadius: "20px",
      padding: "20px",
      textAlign: "center",
      marginBottom: "0px"
    },
    onDrop: function onDrop(e) {
      return handleDrop(e);
    },
    onDragOver: function onDragOver(e) {
      e.preventDefault();
      e.stopPropagation();
    },
    onDragEnter: function onDragEnter(e) {
      e.preventDefault();
      e.stopPropagation();
    },
    onDragLeave: function onDragLeave(e) {
      e.preventDefault();
      e.stopPropagation();
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("input", {
    type: "file",
    id: "upload-photos",
    accept: "image/*",
    multiple: true,
    className: "hidden",
    name: "image[]",
    onChange: function onChange(e) {
      return handleChange(e);
    }
  }), featuredImages.map(function (image, index) {
    return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
      key: index,
      className: "rounded-sm border m-2 relative",
      style: {
        backgroundImage: "url('".concat(image.preview, "')"),
        backgroundPositionX: "center",
        width: "6.9em",
        height: "8.9rem",
        backgroundSize: "cover"
      }
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components_base__WEBPACK_IMPORTED_MODULE_4__["Tooltip"], {
      title: "Remove this item"
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
      onClick: function onClick(e) {
        return handleRemove(e, index);
      },
      className: "absolute bottom-0 right-0 p-2 -mb-2 "
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_fortawesome_react_fontawesome__WEBPACK_IMPORTED_MODULE_9__["FontAwesomeIcon"], {
      className: "text-red-500 shadow-xl hover:font-bold hover:text-red-700",
      icon: _fortawesome_free_solid_svg_icons__WEBPACK_IMPORTED_MODULE_10__["faTimes"]
    }))));
  }), fmPlaceHolder.map(function (p, i) {
    return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
      key: i,
      className: "".concat(p.classes, " cursor-pointer flex justify-center items-center"),
      style: {
        width: "6.9rem",
        height: "8.9rem",
        border: "2px solid #ccc"
      }
    }, " ", /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_fortawesome_react_fontawesome__WEBPACK_IMPORTED_MODULE_9__["FontAwesomeIcon"], {
      className: "text-xl",
      icon: _fortawesome_free_solid_svg_icons__WEBPACK_IMPORTED_MODULE_10__["faCamera"]
    }), " ");
  })), limitError && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
    className: "px-1 text-red-400 tracking-widest  text-xs "
  }, "Please limit your photos to six items only."))), errors["featuredImages"] && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
    className: "px-1 text-red-400 tracking-widest  text-xs "
  }, errors["featuredImages"][0])), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "flex flex-row"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "w-1/2 ml-2"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("h1", {
    className: "font-semibold"
  }, "Rich Text Preview "), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "flex flex-col justify-between py-3"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "flex"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("label", {
    className: "w-full"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "items-center cursor-pointer w-full p-3 rounded-lg flex",
    style: {
      border: "2px dashed #ccc",
      borderRadius: "20px"
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("input", {
    id: "rtp",
    ref: previewImageRef,
    type: "file",
    name: "prev_image",
    onChange: function onChange(e) {
      return handlePreviewImageDisplay(e);
    },
    className: "hidden border text-sm border-gray-400 px-2 py-1 w-full py-1"
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("label", {
    htmlFor: "rtp",
    className: " cursor-pointer hover:text-palette-blue-light ".concat(prevFilename ? "text-palette-blue-light" : "")
  }, prevFilename || "Select File"), prevFilename && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "text-red-500 text-lg cursor-pointer ml-3"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_fortawesome_react_fontawesome__WEBPACK_IMPORTED_MODULE_9__["FontAwesomeIcon"], {
    onClick: function onClick(e) {
      return handleDeletePrevFile(e);
    },
    icon: _fortawesome_free_solid_svg_icons__WEBPACK_IMPORTED_MODULE_10__["faTimes"]
  })))))))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "flex justify-end my-5"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components_base__WEBPACK_IMPORTED_MODULE_4__["Button"], {
    className: "font-bold rounded-full",
    disabled: loading
  }, loading && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_fortawesome_react_fontawesome__WEBPACK_IMPORTED_MODULE_9__["FontAwesomeIcon"], {
    icon: _fortawesome_free_solid_svg_icons__WEBPACK_IMPORTED_MODULE_10__["faSpinner"],
    className: "fa-spin mr-2"
  }), "Update")))));
});

/***/ }),

/***/ "./resources/js/admin/components/EditProjectPart.js":
/*!**********************************************************!*\
  !*** ./resources/js/admin/components/EditProjectPart.js ***!
  \**********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime/regenerator */ "./node_modules/@babel/runtime/regenerator/index.js");
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var react_dropdown__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! react-dropdown */ "./node_modules/react-dropdown/dist/index.js");
/* harmony import */ var react_dropdown__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(react_dropdown__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _helpers_styles_dropdown_css__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../helpers/styles/dropdown.css */ "./resources/js/helpers/styles/dropdown.css");
/* harmony import */ var _helpers_styles_dropdown_css__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(_helpers_styles_dropdown_css__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var react_toastify_dist_ReactToastify_css__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! react-toastify/dist/ReactToastify.css */ "./node_modules/react-toastify/dist/ReactToastify.css");
/* harmony import */ var react_toastify_dist_ReactToastify_css__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(react_toastify_dist_ReactToastify_css__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! moment */ "./node_modules/moment/moment.js");
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(moment__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var _data_index__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../data/index */ "./resources/js/data/index.js");
/* harmony import */ var _components_base__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../components/_base */ "./resources/js/components/_base/index.js");
/* harmony import */ var _components_base__WEBPACK_IMPORTED_MODULE_7___default = /*#__PURE__*/__webpack_require__.n(_components_base__WEBPACK_IMPORTED_MODULE_7__);
/* harmony import */ var _RichTextEditor__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./RichTextEditor */ "./resources/js/admin/components/RichTextEditor.js");
/* harmony import */ var draft_js__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! draft-js */ "./node_modules/draft-js/lib/Draft.js");
/* harmony import */ var draft_js__WEBPACK_IMPORTED_MODULE_9___default = /*#__PURE__*/__webpack_require__.n(draft_js__WEBPACK_IMPORTED_MODULE_9__);
/* harmony import */ var react_datepicker__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! react-datepicker */ "./node_modules/react-datepicker/dist/react-datepicker.min.js");
/* harmony import */ var react_datepicker__WEBPACK_IMPORTED_MODULE_10___default = /*#__PURE__*/__webpack_require__.n(react_datepicker__WEBPACK_IMPORTED_MODULE_10__);
/* harmony import */ var _helpers_styles_datepicker_css__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ../../helpers/styles/datepicker.css */ "./resources/js/helpers/styles/datepicker.css");
/* harmony import */ var _helpers_styles_datepicker_css__WEBPACK_IMPORTED_MODULE_11___default = /*#__PURE__*/__webpack_require__.n(_helpers_styles_datepicker_css__WEBPACK_IMPORTED_MODULE_11__);
/* harmony import */ var draft_convert__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! draft-convert */ "./node_modules/draft-convert/esm/index.js");
/* harmony import */ var _helpers_propertyHelper_propertyHelper__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ../../helpers/propertyHelper/propertyHelper */ "./resources/js/helpers/propertyHelper/propertyHelper.js");
/* harmony import */ var _fortawesome_react_fontawesome__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! @fortawesome/react-fontawesome */ "./node_modules/@fortawesome/react-fontawesome/index.es.js");
/* harmony import */ var _fortawesome_free_solid_svg_icons__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! @fortawesome/free-solid-svg-icons */ "./node_modules/@fortawesome/free-solid-svg-icons/index.es.js");
/* harmony import */ var _fortawesome_free_regular_svg_icons__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! @fortawesome/free-regular-svg-icons */ "./node_modules/@fortawesome/free-regular-svg-icons/index.es.js");
/* harmony import */ var _states_userGlobal__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! ../../states/userGlobal */ "./resources/js/states/userGlobal.js");
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(/*! lodash */ "./node_modules/lodash/lodash.js");
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_18___default = /*#__PURE__*/__webpack_require__.n(lodash__WEBPACK_IMPORTED_MODULE_18__);
/* harmony import */ var react_geocode__WEBPACK_IMPORTED_MODULE_19__ = __webpack_require__(/*! react-geocode */ "./node_modules/react-geocode/lib/index.js");
/* harmony import */ var react_geocode__WEBPACK_IMPORTED_MODULE_19___default = /*#__PURE__*/__webpack_require__.n(react_geocode__WEBPACK_IMPORTED_MODULE_19__);
/* harmony import */ var _helpers_numberInput__WEBPACK_IMPORTED_MODULE_20__ = __webpack_require__(/*! ../../helpers/numberInput */ "./resources/js/helpers/numberInput.js");
/* harmony import */ var _components_AddDeveloperModal__WEBPACK_IMPORTED_MODULE_21__ = __webpack_require__(/*! ../components/AddDeveloperModal */ "./resources/js/admin/components/AddDeveloperModal.js");
/* harmony import */ var _helpers_mapCoordinateHelper__WEBPACK_IMPORTED_MODULE_22__ = __webpack_require__(/*! ../../helpers/mapCoordinateHelper */ "./resources/js/helpers/mapCoordinateHelper.js");
/* harmony import */ var _helpers_fileHelper_fileHelper__WEBPACK_IMPORTED_MODULE_23__ = __webpack_require__(/*! ../../helpers/fileHelper/fileHelper */ "./resources/js/helpers/fileHelper/fileHelper.js");
/* harmony import */ var _components_base_alerts_sweetAlert__WEBPACK_IMPORTED_MODULE_24__ = __webpack_require__(/*! ../../components/_base/alerts/sweetAlert */ "./resources/js/components/_base/alerts/sweetAlert.js");


function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

function _toConsumableArray(arr) { return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _unsupportedIterableToArray(arr) || _nonIterableSpread(); }

function _nonIterableSpread() { throw new TypeError("Invalid attempt to spread non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _iterableToArray(iter) { if (typeof Symbol !== "undefined" && Symbol.iterator in Object(iter)) return Array.from(iter); }

function _arrayWithoutHoles(arr) { if (Array.isArray(arr)) return _arrayLikeToArray(arr); }

function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _unsupportedIterableToArray(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

function _iterableToArrayLimit(arr, i) { if (typeof Symbol === "undefined" || !(Symbol.iterator in Object(arr))) return; var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }




























var mapKey = "".concat("AIzaSyB2angcqMN5xDkZHAzghZ5hlg-QldC02ww");

var EditProjectPart = function EditProjectPart(_ref) {
  var toggleFetch = _ref.toggleFetch;
  react_geocode__WEBPACK_IMPORTED_MODULE_19___default.a.setApiKey(mapKey);

  var _UserGlobal = Object(_states_userGlobal__WEBPACK_IMPORTED_MODULE_17__["default"])(),
      _UserGlobal2 = _slicedToArray(_UserGlobal, 2),
      userState = _UserGlobal2[0],
      userActions = _UserGlobal2[1];

  var timeLimitData = userState.projectData.expires_at ? JSON.parse(userState.projectData.expires_at) : null;

  var _useState = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(userState.projectData.land_registration ? new Date(moment__WEBPACK_IMPORTED_MODULE_5___default()(userState.projectData.land_registration).format("YYYY-MM-DD")) : new Date(Date.now())),
      _useState2 = _slicedToArray(_useState, 2),
      land_registration = _useState2[0],
      setLandRegistrationDate = _useState2[1];

  var _useState3 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(userState.projectData.proposed_settlement ? new Date(moment__WEBPACK_IMPORTED_MODULE_5___default()(userState.projectData.proposed_settlement).format("YYYY-MM-DD")) : new Date(Date.now())),
      _useState4 = _slicedToArray(_useState3, 2),
      proposed_settlement = _useState4[0],
      setDate = _useState4[1];

  var _useState5 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(userState.projectData.proposed_settlement ? new Date(moment__WEBPACK_IMPORTED_MODULE_5___default()(userState.projectData.proposed_settlement).format("YYYY-MM-DD")) : new Date(Date.now())),
      _useState6 = _slicedToArray(_useState5, 2),
      default_proposed_settlement = _useState6[0],
      setDefaultDate = _useState6[1];

  var _useState7 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(timeLimitData && timeLimitData.option ? parseInt(timeLimitData.option) : 0),
      _useState8 = _slicedToArray(_useState7, 2),
      timeLimitOptionValue = _useState8[0],
      setTimeLimitOptionValue = _useState8[1];

  var _useState9 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(timeLimitData && timeLimitData.option ? parseInt(timeLimitData.option) : 0),
      _useState10 = _slicedToArray(_useState9, 2),
      timeLimitSelectValue = _useState10[0],
      setTimeLimitSelectValue = _useState10[1];

  var _useState11 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(timeLimitData && timeLimitData.optionVal ? parseInt(timeLimitData.optionVal) : 0),
      _useState12 = _slicedToArray(_useState11, 2),
      timeLimitValue = _useState12[0],
      setTimeLimitValue = _useState12[1];

  var _useState13 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(""),
      _useState14 = _slicedToArray(_useState13, 2),
      discount = _useState14[0],
      setDiscount = _useState14[1];

  var _useState15 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(userState.projectData.deposit ? userState.projectData.deposit.toString() : null),
      _useState16 = _slicedToArray(_useState15, 2),
      deposit = _useState16[0],
      setDeposit = _useState16[1];

  var _useState17 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(_toConsumableArray(userState.projectData.featured_images.map(function (image) {
    return {
      image: image,
      preview: image
    };
  }))),
      _useState18 = _slicedToArray(_useState17, 2),
      featuredImages = _useState18[0],
      setFeaturedImages = _useState18[1];

  var toogleAddUpdateDeveloper = function toogleAddUpdateDeveloper() {
    setToogleAddEditDeveloper(!toogleAddEditDeveloper);
  };

  var _useState19 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(false),
      _useState20 = _slicedToArray(_useState19, 2),
      toogleAddEditDeveloper = _useState20[0],
      setToogleAddEditDeveloper = _useState20[1];

  var _useState21 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(false),
      _useState22 = _slicedToArray(_useState21, 2),
      loading = _useState22[0],
      setLoading = _useState22[1];

  var _useState23 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])([]),
      _useState24 = _slicedToArray(_useState23, 2),
      fmPlaceHolder = _useState24[0],
      setFmPlaceHolder = _useState24[1];

  var _useState25 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(false),
      _useState26 = _slicedToArray(_useState25, 2),
      limitError = _useState26[0],
      setLimitError = _useState26[1];

  var _useState27 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(userState.projectData.address.state),
      _useState28 = _slicedToArray(_useState27, 2),
      projectState = _useState28[0],
      setProjectState = _useState28[1];

  var _useState29 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(!Object(lodash__WEBPACK_IMPORTED_MODULE_18__["isEmpty"])(userState.projectData.display_suite_address) && userState.projectData.display_suite_address.state ? userState.projectData.display_suite_address.state : ""),
      _useState30 = _slicedToArray(_useState29, 2),
      displaySuiteState = _useState30[0],
      setDisplaySuiteState = _useState30[1];

  var _useState31 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(String(userState.projectData.sub_property_id)),
      _useState32 = _slicedToArray(_useState31, 2),
      propertyType = _useState32[0],
      setPropertyType = _useState32[1];

  var _useState33 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])([]),
      _useState34 = _slicedToArray(_useState33, 2),
      errors = _useState34[0],
      setErrors = _useState34[1];

  var _useState35 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(function () {
    return draft_js__WEBPACK_IMPORTED_MODULE_9__["EditorState"].createEmpty();
  }),
      _useState36 = _slicedToArray(_useState35, 2),
      editorState = _useState36[0],
      setEditorState = _useState36[1];

  var _useState37 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(function () {
    return draft_js__WEBPACK_IMPORTED_MODULE_9__["EditorState"].createEmpty();
  }),
      _useState38 = _slicedToArray(_useState37, 2),
      editorStateBackup = _useState38[0],
      setEditorStateBackup = _useState38[1];

  var _useState39 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])("Australia"),
      _useState40 = _slicedToArray(_useState39, 2),
      projectCountry = _useState40[0],
      setProjectCountry = _useState40[1];

  var _useState41 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])("Australia"),
      _useState42 = _slicedToArray(_useState41, 2),
      displaySuiteCountry = _useState42[0],
      setDisplaySuiteCountry = _useState42[1];

  var addProjectForm = Object(react__WEBPACK_IMPORTED_MODULE_1__["useRef"])(null);
  var countries = [{
    label: "Australia",
    value: "Australia"
  }];

  var _useState43 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(userState.projectData),
      _useState44 = _slicedToArray(_useState43, 2),
      project = _useState44[0],
      setProject = _useState44[1];

  var _useState45 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(userState.projectData.is_address_same == 1 ? true : false),
      _useState46 = _slicedToArray(_useState45, 2),
      sameAddress = _useState46[0],
      setSame = _useState46[1];

  var _useState47 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])("".concat(userState.projectData.user_id)),
      _useState48 = _slicedToArray(_useState47, 2),
      developerValue = _useState48[0],
      setDeveloperValue = _useState48[1];

  var _useState49 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])({}),
      _useState50 = _slicedToArray(_useState49, 2),
      projectDevelopers = _useState50[0],
      setProjectDevelopers = _useState50[1];

  var _useState51 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(""),
      _useState52 = _slicedToArray(_useState51, 2),
      filename = _useState52[0],
      setFilename = _useState52[1];

  var _useState53 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])([]),
      _useState54 = _slicedToArray(_useState53, 2),
      previewImage = _useState54[0],
      setPreviewImage = _useState54[1];

  var _useState55 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(userState.projectData && userState.projectData.sub_property_id == 4),
      _useState56 = _slicedToArray(_useState55, 2),
      isProjectLand = _useState56[0],
      setIsProjectLand = _useState56[1];

  var previewImageRef = Object(react__WEBPACK_IMPORTED_MODULE_1__["useRef"])(null);
  var time_limit_options = [{
    label: "Please select...",
    value: 0
  }, {
    label: "Hour/s",
    value: "1"
  }, {
    label: "Day/s",
    value: "2"
  }, {
    label: "Week/s",
    value: "3"
  }, {
    label: "Month/s",
    value: "4"
  }, {
    label: "Year/s",
    value: "5"
  }];

  var _useState57 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(userState.projectData.is_completed),
      _useState58 = _slicedToArray(_useState57, 2),
      completedNow = _useState58[0],
      setCompletedNow = _useState58[1];

  Object(react__WEBPACK_IMPORTED_MODULE_1__["useEffect"])(function () {
    var phs = [];

    for (var index = 0; index < 12 - featuredImages.length; index++) {
      var ph = {
        classes: "cursor-pointer m-2 inline-flex bg-transparent text-palette-gray"
      };
      phs.push(ph);
    }

    setFmPlaceHolder(phs);
  }, [featuredImages.length]);
  Object(react__WEBPACK_IMPORTED_MODULE_1__["useEffect"])(function () {
    var data = userState.projectData.description;
    setEditorState(function () {
      return draft_js__WEBPACK_IMPORTED_MODULE_9__["EditorState"].push(editorState, Object(draft_convert__WEBPACK_IMPORTED_MODULE_12__["convertFromHTML"])(data));
    });
    setEditorStateBackup(function () {
      return draft_js__WEBPACK_IMPORTED_MODULE_9__["EditorState"].push(editorStateBackup, Object(draft_convert__WEBPACK_IMPORTED_MODULE_12__["convertFromHTML"])(data));
    });

    if (timeLimitData) {
      var currentTimeLimit = time_limit_options.filter(function (val, i) {
        return val.value === timeLimitData.option;
      });

      if (currentTimeLimit[0]) {
        setTimeLimitOptionValue(currentTimeLimit[0].value);
      }

      setTimeLimitSelectValue(currentTimeLimit[0]);
    }
  }, []);
  Object(react__WEBPACK_IMPORTED_MODULE_1__["useEffect"])(function () {
    getDevelopers();
    setDiscount(userState.projectData.discount || "");

    if (userState.projectData.rich_preview_image) {
      var img = [];
      img.push({
        image: userState.projectData.rich_preview_image,
        preview: userState.projectData.rich_preview_image
      });
      var first = "";

      if (userState.projectData.rich_preview_image.includes("featured-images")) {
        first = userState.projectData.rich_preview_image.split("/featured-images/")[1];
      }

      if (userState.projectData.rich_preview_image.includes("imgPreview")) {
        first = userState.projectData.rich_preview_image.split("/imgPreview/")[1];
      }

      if (first) {
        var newFilename = first.split("?")[0];
        setFilename(newFilename);
        setPreviewImage(img);
      }
    }
  }, []);

  var getDevelopers = function getDevelopers() {
    var url = "".concat(window.location.origin, "/api/user");
    Object(_data_index__WEBPACK_IMPORTED_MODULE_6__["developers"])(url).then(function (result) {
      setProjectDevelopers(_toConsumableArray(result.data.map(function (developer) {
        return {
          label: "".concat(developer.first_name, " ").concat(developer.last_name),
          value: String(developer.id)
        };
      })));
    })["catch"](function (error) {});
  };

  var getDealCoordinates = /*#__PURE__*/function () {
    var _ref3 = _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee(_ref2) {
      var formData, url, address, suburb, addressParam, data, _address, _suburb, _addressParam, _data;

      return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee$(_context) {
        while (1) {
          switch (_context.prev = _context.next) {
            case 0:
              formData = _ref2.formData, url = _ref2.url;
              address = formData.get("project_address_line_1");
              suburb = formData.get("project_address_suburb");
              addressParam = address.concat(" ", suburb).concat(" ", projectState).concat(" ", projectCountry);
              _context.next = 6;
              return Object(_helpers_mapCoordinateHelper__WEBPACK_IMPORTED_MODULE_22__["getMapCoordinates"])(addressParam);

            case 6:
              data = _context.sent;
              formData.append("long", data["long"]);
              formData.append("lat", data.lat);

              if (sameAddress) {
                _context.next = 18;
                break;
              }

              _address = formData.get("display_suite_address_line_1");
              _suburb = formData.get("display_suite_address_suburb");
              _addressParam = _address.concat(" ", _suburb).concat(" ", displaySuiteState).concat(" ", displaySuiteCountry);
              _context.next = 15;
              return Object(_helpers_mapCoordinateHelper__WEBPACK_IMPORTED_MODULE_22__["getMapCoordinates"])(_addressParam);

            case 15:
              _data = _context.sent;
              formData.append("display_suite_long", _data["long"]);
              formData.append("display_suite_lat", _data.lat);

            case 18:
            case "end":
              return _context.stop();
          }
        }
      }, _callee);
    }));

    return function getDealCoordinates(_x) {
      return _ref3.apply(this, arguments);
    };
  }();

  var saveDeal = /*#__PURE__*/function () {
    var _ref5 = _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee2(_ref4) {
      var formData, url;
      return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee2$(_context2) {
        while (1) {
          switch (_context2.prev = _context2.next) {
            case 0:
              formData = _ref4.formData, url = _ref4.url;
              _context2.next = 3;
              return getDealCoordinates({
                formData: formData
              });

            case 3:
              _context2.next = 5;
              return Object(_data_index__WEBPACK_IMPORTED_MODULE_6__["saveUpdateDeal"])({
                formData: formData,
                url: url
              }).then(function (r) {
                if (r.status === 200) {
                  setLoading(false);
                  Object(_components_base_alerts_sweetAlert__WEBPACK_IMPORTED_MODULE_24__["sweetAlert"])("success", "Project successfully updated!");
                  setDefaultDate(proposed_settlement);
                  toggleFetch();
                }
              })["catch"](function (error) {
                if (error.response.data.errors) {
                  Object(_components_base_alerts_sweetAlert__WEBPACK_IMPORTED_MODULE_24__["sweetAlert"])("error", "Some fields are missing");
                }

                setLoading(false);
                setErrors(error.response.data.errors);
              }).then(function () {});

            case 5:
            case "end":
              return _context2.stop();
          }
        }
      }, _callee2);
    }));

    return function saveDeal(_x2) {
      return _ref5.apply(this, arguments);
    };
  }();

  var handleSubmit = function handleSubmit(e) {
    e.preventDefault();
    setLimitError(false);
    saveProject();
  };

  var saveProject = function saveProject() {
    if (discount && discount < 10) {
      return Object(_components_base_alerts_sweetAlert__WEBPACK_IMPORTED_MODULE_24__["sweetAlert"])("error", "Project discount should not be less than 10 percent.");
    }

    if (discount && discount > 100) {
      return Object(_components_base_alerts_sweetAlert__WEBPACK_IMPORTED_MODULE_24__["sweetAlert"])("error", "Project discount should not be more than 100 percent.");
    }

    setLoading(true);
    var newDate = new Date(moment__WEBPACK_IMPORTED_MODULE_5___default()(userState.projectData.created_at).format("YYYY-MM-DD"));
    var formData = new FormData(addProjectForm.current);
    formData.append("role", "project_developer");
    formData.append("propertyType", propertyType);
    formData.append("project_address_state", projectState);
    formData.append("project_address_country", projectCountry);
    formData.append("display_suite_address_state", displaySuiteState);
    formData.append("display_suite_address_country", displaySuiteCountry);
    formData.append("time_limit[optionVal]", timeLimitValue);
    formData.append("time_limit[option]", timeLimitOptionValue);
    formData.append("time_limit[createdAt]", newDate);
    formData.append("developer", developerValue);
    formData.append("project_discount", discount || 0);
    formData.append("deposit", deposit);
    formData.append("isCompleted", completedNow);
    formData.append("description", editorState.getCurrentContent().getPlainText());
    formData.append("descriptionHTML", Object(draft_convert__WEBPACK_IMPORTED_MODULE_12__["convertToHTML"])(editorState.getCurrentContent()));
    featuredImages.forEach(function (image, key) {
      formData.append("featuredImages[".concat(key, "]"), image.image);
    });

    if (previewImage.length) {
      formData.append("preview_img[".concat(0, "]"), previewImage[0].image);
    } else {
      formData.append("preview_img[".concat(0, "]"), "");
    }

    formData.append("_method", "PATCH");
    formData.append("same_address", sameAddress);
    formData.append("isPropertyTypeLand", isProjectLand);
    var url = "/api/deal/".concat(userState.projectData.id);
    saveDeal({
      formData: formData,
      url: url
    });
  };

  var handleDrop = function handleDrop(e) {
    e.preventDefault();
    e.stopPropagation();
    var fmCount = featuredImages.length;
    var onCount = e.dataTransfer.files.length;

    if (fmCount + onCount > 12) {
      e.target.value = null;
      setLimitError(true);
      return;
    } else {
      setLimitError(false);
    }

    setFeaturedImages(featuredImages.concat(_toConsumableArray(_toConsumableArray(e.dataTransfer.files).map(function (file) {
      var newFilename = Object(_helpers_fileHelper_fileHelper__WEBPACK_IMPORTED_MODULE_23__["sanitizeFilename"])(file.name);
      var newFileObj = new File([file], newFilename);
      return {
        image: newFileObj,
        preview: URL.createObjectURL(newFileObj)
      };
    }))));
    e.target.value = null;
  };

  var handleChange = function handleChange(e) {
    var fmCount = featuredImages.length;
    var onCount = e.target.files.length;

    if (fmCount + onCount > 12) {
      setLimitError(true);
      e.target.value = null;
      return;
    } else {
      setLimitError(false);
    }

    setFeaturedImages(featuredImages.concat(_toConsumableArray(_toConsumableArray(e.target.files).map(function (file) {
      var newFilename = Object(_helpers_fileHelper_fileHelper__WEBPACK_IMPORTED_MODULE_23__["sanitizeFilename"])(file.name);
      var newFileObj = new File([file], newFilename);
      return {
        image: newFileObj,
        preview: URL.createObjectURL(newFileObj)
      };
    }))));
    e.target.value = null;
  };

  var handlePreviewImageDisplay = function handlePreviewImageDisplay(e) {
    if (e.target.files.length > 1) {
      Object(_components_base_alerts_sweetAlert__WEBPACK_IMPORTED_MODULE_24__["sweetAlert"])("error", "Multiple upload is not allowed!");
      return;
    }

    if (!Object(_helpers_fileHelper_fileHelper__WEBPACK_IMPORTED_MODULE_23__["imageFileTypeFilter"])(e.target.files[0]["name"])) {
      Object(_components_base_alerts_sweetAlert__WEBPACK_IMPORTED_MODULE_24__["sweetAlert"])("error", "Filetype is not supported!");
      e.target.value = null;
      return;
    }

    var imgArr = [];
    setPreviewImage(imgArr.concat(_toConsumableArray(_toConsumableArray(e.target.files).map(function (file) {
      var newFilename = Object(_helpers_fileHelper_fileHelper__WEBPACK_IMPORTED_MODULE_23__["sanitizeFilename"])(file.name);
      var newFileObj = new File([file], newFilename);
      setFilename(newFilename);
      return {
        image: newFileObj,
        preview: URL.createObjectURL(newFileObj)
      };
    }))));
  };

  var handleDeleteFile = function handleDeleteFile(e) {
    e.preventDefault();
    setPreviewImage([]);
    previewImageRef.current.type = "";
    previewImageRef.current.type = "file";
    setFilename("");
    e.target.value = null;
  };

  var handleRemove = function handleRemove(e, i) {
    e.preventDefault();
    e.stopPropagation();
    setFeaturedImages(_toConsumableArray(featuredImages.filter(function (image, k) {
      if (i !== k) {
        return image;
      }
    })));

    if (featuredImages.length > 12) {
      setLimitError(true);
    } else {
      setLimitError(false);
    }
  };

  var compareDate = function compareDate(selectedDate) {
    var date = moment__WEBPACK_IMPORTED_MODULE_5___default()(selectedDate, "DD-MM-YYYY"); //Date format

    var currDate = moment__WEBPACK_IMPORTED_MODULE_5___default()().format("YYYY-MM-DD");
    var durationHours = date.diff(currDate, "hours");
    var defaultDate = moment__WEBPACK_IMPORTED_MODULE_5___default()(default_proposed_settlement, "DD-MM-YYYY");
    var durationDays = date.diff(defaultDate, "days");

    if (durationDays < 0 && durationHours < 0) {
      return false;
    }

    return true;
  };

  var setSelctedSettlementDate = function setSelctedSettlementDate(date) {
    if (isProjectLand) {
      setLandRegistrationDate(date);
    } else {
      if (!compareDate(date)) {
        Object(_components_base_alerts_sweetAlert__WEBPACK_IMPORTED_MODULE_24__["sweetAlert"])("error", "Please select future date!");
      } else {
        setDate(date);
      }
    }
  };

  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "bg-white rounded-lg py-5 px-8 m-5"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("form", {
    ref: addProjectForm,
    onSubmit: handleSubmit
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "text-black"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "flex items-center"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "flex items-center"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("label", {
    className: "block py-3 font-semibold w-40"
  }, "Project Developer"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "relative"
  }, !Object(lodash__WEBPACK_IMPORTED_MODULE_18__["isEmpty"])(projectDevelopers) ? /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(react_dropdown__WEBPACK_IMPORTED_MODULE_2___default.a, {
    id: "project_developer",
    value: developerValue,
    options: projectDevelopers,
    onChange: function onChange(option) {
      setDeveloperValue(option.value);
    },
    placeholder: "Please select...",
    name: "project_developer"
  }) : /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_fortawesome_react_fontawesome__WEBPACK_IMPORTED_MODULE_14__["FontAwesomeIcon"], {
    icon: _fortawesome_free_solid_svg_icons__WEBPACK_IMPORTED_MODULE_15__["faSpinner"],
    className: "fa-spin mr-2"
  })), errors["developer"] && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
    className: "absolute px-1 text-red-400 tracking-widest  text-xs "
  }, errors["developer"][0])), !loading && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "flex"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "flex justify-end ml-10"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
    className: "font-semibold text-palette-purple cursor-pointer",
    disabled: loading,
    onClick: function onClick() {
      return toogleAddUpdateDeveloper();
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_fortawesome_react_fontawesome__WEBPACK_IMPORTED_MODULE_14__["FontAwesomeIcon"], {
    icon: _fortawesome_free_solid_svg_icons__WEBPACK_IMPORTED_MODULE_15__["faPlus"],
    className: "fa-plus mr-2"
  }), "Add Project Developer")))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "py-3 flex items-center "
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("label", {
    className: "font-semibold w-1/5",
    htmlFor: "name"
  }, "Project Name"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "flex-1 "
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components_base__WEBPACK_IMPORTED_MODULE_7__["TextInput"], {
    id: "name",
    defaultValue: userState.projectData.name,
    className: "border border-gray-400 px-2 py-1 w-20 py-1",
    border: false,
    name: "name"
  }), errors["name"] && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
    className: "px-1 text-red-400 tracking-widest  text-xs "
  }, errors["name"][0]))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "py-3"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "w-full text-sm bg-white text-black "
  }, editorStateBackup.getCurrentContent().getPlainText() && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_RichTextEditor__WEBPACK_IMPORTED_MODULE_8__["default"], {
    title: "Project Description",
    richEditorState: editorState,
    richEditorStateContent: function richEditorStateContent(content) {
      return setEditorState(content);
    },
    characterLimit: 2000
  })), errors["description"] && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
    className: "px-1 text-red-400 tracking-widest  text-xs "
  }, errors["description"][0])), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "flex justify-between"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "flex flex-start items-center"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("label", {
    className: "block py-3 w-32"
  }, "Project Type"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "relative"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(react_dropdown__WEBPACK_IMPORTED_MODULE_2___default.a, {
    id: "property_type",
    value: propertyType,
    options: _helpers_propertyHelper_propertyHelper__WEBPACK_IMPORTED_MODULE_13__["property_types"],
    onChange: function onChange(option) {
      if (option.label === "Land") {
        setIsProjectLand(true);
      } else {
        setIsProjectLand(false);
      }

      setPropertyType(option.value);
    },
    placeholder: "Please select...",
    name: "property_type"
  }), errors["propertyType"] && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
    className: "absolute px-1 text-red-400 tracking-widest  text-xs "
  }, errors["propertyType"][0]))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "px-10 flex flex-start items-center "
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("label", {
    className: "block pr-5 py-3"
  }, "Discount"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "relative"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "flex items-center"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components_base__WEBPACK_IMPORTED_MODULE_7__["TextInput"], {
    className: "pl-2 text-sm border-gray-400 w-32 border py-1",
    border: false,
    type: "number" // name={`project_discount`}
    ,
    value: discount,
    onChange: function onChange(e) {
      return Object(_helpers_numberInput__WEBPACK_IMPORTED_MODULE_20__["inputRegex"])(e.target.value) ? setDiscount(e.target.value) : null;
    } //name="time_limit_value"

  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
    className: "font-extrabold pl-3"
  }, " % ")), errors["project_discount"] && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
    className: "absolute px-1 text-red-400 tracking-widest  text-xs "
  }, errors["project_discount"][0])))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "flex flex-start items-center mt-3"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("label", {
    className: "block py-3 w-32"
  }, "Deposit"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "relative flex items-center"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(react_dropdown__WEBPACK_IMPORTED_MODULE_2___default.a, {
    id: "deposit",
    value: deposit,
    options: [{
      label: "5",
      value: "5"
    }, {
      label: "10",
      value: "10"
    }],
    onChange: function onChange(option) {
      setDeposit(option.value);
    },
    placeholder: "Please select...",
    name: "deposit"
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
    className: "font-extrabold pl-3"
  }, " % "), errors["deposit"] && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
    className: "absolute px-1 text-red-400 tracking-widest  text-xs "
  }, errors["deposit"][0]))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("h1", {
    className: "font-semibold mt-5"
  }, "Project Address"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "w-full"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "py-3 flex w-full items-center"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("label", {
    className: "w-32"
  }, "Address Line 1"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: " flex-1 relative"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components_base__WEBPACK_IMPORTED_MODULE_7__["TextInput"], {
    defaultValue: userState.projectData.address.line_1,
    className: "border text-sm border-gray-400 px-2 py-1 w-20",
    border: false,
    name: "project_address_line_1"
  }), errors["project_address_line_1"] && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
    className: "absolute px-1 text-red-400 tracking-widest  text-xs "
  }, errors["project_address_line_1"][0]))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "py-3 flex justify-between"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "flex items-center"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("label", {
    className: "w-32 block py-3"
  }, "Suburb "), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    style: {
      width: "320px"
    },
    className: "flex-1 relative"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components_base__WEBPACK_IMPORTED_MODULE_7__["TextInput"], {
    className: " border-gray-400 border py-1 pl-2",
    border: false,
    name: "project_address_suburb",
    defaultValue: userState.projectData.address.suburb
  }), errors["project_address_suburb"] && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
    className: "absolute px-1 text-red-400 tracking-widest  text-xs "
  }, errors["project_address_suburb"][0]))), !isProjectLand && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "ml-5 flex items-center"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("label", {
    className: "w-32 block py-3"
  }, "City "), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "relative"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components_base__WEBPACK_IMPORTED_MODULE_7__["TextInput"], {
    className: " border-gray-400 border pl-2 py-1",
    border: false,
    name: "project_address_city",
    defaultValue: userState.projectData.address.city
  }), errors["project_address_city"] && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
    className: "absolute px-1 text-red-400 tracking-widest  text-xs "
  }, errors["project_address_city"][0])))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "py-3 flex justify-between"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "py-3 flex items-center"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("label", {
    className: "w-32 block py-3"
  }, "State "), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "relative"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(react_dropdown__WEBPACK_IMPORTED_MODULE_2___default.a, {
    id: "project_address_state",
    value: projectState,
    options: _helpers_propertyHelper_propertyHelper__WEBPACK_IMPORTED_MODULE_13__["states"],
    onChange: function onChange(option) {
      setProjectState(option.value);
    },
    placeholder: "Select state",
    name: "project_address_state"
  }), errors["project_address_state"] && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
    className: "absolute px-1 text-red-400 tracking-widest  text-xs "
  }, errors["project_address_state"][0]))), !isProjectLand && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "ml-5 flex items-center"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("label", {
    className: "w-32 block py-3"
  }, "Postcode/ZIP "), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "relative"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components_base__WEBPACK_IMPORTED_MODULE_7__["TextInput"], {
    className: " border-gray-400 border pl-2 py-1",
    border: false,
    name: "project_address_zip",
    defaultValue: userState.projectData.address.postal
  }), errors["project_address_zip"] && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
    className: "absolute px-1 text-red-400 tracking-widest  text-xs "
  }, errors["project_address_zip"][0])))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "py-3 flex items-center"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("label", {
    className: "w-32 block py-3"
  }, "Country "), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "relative"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(react_dropdown__WEBPACK_IMPORTED_MODULE_2___default.a, {
    id: "project_address_country",
    value: projectCountry,
    options: countries,
    onChange: function onChange(option) {
      setProjectCountry(option.value);
    },
    name: "project_address_country"
  }), errors["project_address_country"] && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
    className: "absolute px-1 text-red-400 tracking-widest  text-xs "
  }, errors["project_address_country"][0])))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "flex justify-between mt-5"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("h1", {
    className: "font-semibold"
  }, isProjectLand ? "Office" : "Display Suite", " Address"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "flex justify-between items-center"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("input", {
    type: "checkbox",
    name: "same_address_check",
    id: "same_address",
    onChange: function onChange(e) {
      e.target.checked ? setSame(true) : setSame(false);
    },
    checked: sameAddress
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
    className: "p-1"
  }, isProjectLand ? "Office" : "Display Suite", " Address is same as Project Address"))), !sameAddress && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "py-3 flex w-full items-center"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("label", {
    className: "w-32"
  }, "Address Line 1"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "flex-1 relative"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components_base__WEBPACK_IMPORTED_MODULE_7__["TextInput"], {
    defaultValue: userState.projectData.display_suite_address.line_1,
    className: "border text-sm border-gray-400 px-2 py-1 w-20",
    border: false,
    name: "display_suite_address_line_1"
  }), errors["display_suite_address_line_1"] && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
    className: "absolute px-1 text-red-400 tracking-widest  text-xs "
  }, errors["display_suite_address_line_1"][0]))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "py-3 flex justify-between"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "flex items-center"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("label", {
    className: "w-32 block py-3"
  }, "Suburb "), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    style: {
      width: "320px"
    },
    className: "flex-1 relative"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components_base__WEBPACK_IMPORTED_MODULE_7__["TextInput"], {
    className: " border-gray-400 border py-1 pl-2",
    border: false,
    name: "display_suite_address_suburb",
    defaultValue: userState.projectData.display_suite_address.suburb
  }), errors["display_suite_address_suburb"] && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
    className: "absolute px-1 text-red-400 tracking-widest  text-xs "
  }, errors["display_suite_address_suburb"][0]))), !isProjectLand && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "ml-5 flex items-center"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("label", {
    className: "w-32 block py-3"
  }, "City "), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "relative"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components_base__WEBPACK_IMPORTED_MODULE_7__["TextInput"], {
    className: " border-gray-400 border pl-2 py-1",
    border: false,
    name: "display_suite_address_city",
    defaultValue: userState.projectData.display_suite_address.city
  }), errors["display_suite_address_city"] && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
    className: "absolute px-1 text-red-400 tracking-widest  text-xs "
  }, errors["display_suite_address_city"][0])))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "py-3 flex justify-between"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "py-3 flex items-center"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("label", {
    className: "w-32 block py-3"
  }, "State "), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "relative"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(react_dropdown__WEBPACK_IMPORTED_MODULE_2___default.a, {
    id: "display_suite_address_state",
    value: displaySuiteState,
    options: _helpers_propertyHelper_propertyHelper__WEBPACK_IMPORTED_MODULE_13__["states"],
    onChange: function onChange(option) {
      setDisplaySuiteState(option.value);
    },
    placeholder: "Select state",
    name: "display_suite_address_state"
  }), errors["display_suite_address_state"] && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
    className: "absolute px-1 text-red-400 tracking-widest  text-xs "
  }, errors["display_suite_address_state"][0]))), !isProjectLand && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "ml-5 flex items-center"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("label", {
    className: "w-32 block py-3"
  }, "Postcode/ZIP "), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "relative"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components_base__WEBPACK_IMPORTED_MODULE_7__["TextInput"], {
    className: " border-gray-400 border pl-2 py-1",
    border: false,
    name: "display_suite_address_zip",
    defaultValue: userState.projectData.display_suite_address.postal
  }), errors["display_suite_address_zip"] && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
    className: "absolute px-1 text-red-400 tracking-widest  text-xs "
  }, errors["display_suite_address_zip"][0])))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "py-3 flex items-center"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("label", {
    className: "w-32 block py-3"
  }, "Country "), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "relative"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(react_dropdown__WEBPACK_IMPORTED_MODULE_2___default.a, {
    id: "display_suite_address_country",
    value: displaySuiteCountry,
    options: countries,
    onChange: function onChange(option) {
      setDisplaySuiteCountry(option.value);
    },
    name: "display_suite_address_country"
  }), errors["display_suite_address_country"] && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
    className: "absolute px-1 text-red-400 tracking-widest  text-xs "
  }, errors["display_suite_address_country"][0])))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "py-3 flex"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "flex-1"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "cursor-pointer flex-1"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("h1", {
    className: "font-semibold mb-4"
  }, "Estimated Completion/", isProjectLand ? "Land Registration" : "Settlement"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_fortawesome_react_fontawesome__WEBPACK_IMPORTED_MODULE_14__["FontAwesomeIcon"], {
    className: "mr-2",
    icon: _fortawesome_free_regular_svg_icons__WEBPACK_IMPORTED_MODULE_16__["faCalendarCheck"]
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(react_datepicker__WEBPACK_IMPORTED_MODULE_10___default.a, {
    className: "py-1 px-2",
    id: "proposed_settlement" // name={`proposed_settlement`}
    ,
    showMonthDropdown: true,
    showYearDropdown: true,
    dateFormat: "PP",
    selected: isProjectLand ? land_registration : proposed_settlement,
    disabled: completedNow,
    onSelect: function onSelect(date) {
      setSelctedSettlementDate(date);
    },
    onChange: function onChange(date) {
      setSelctedSettlementDate(date);
    }
  })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "p-1 flex items-center mt-3"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("input", {
    type: "checkbox",
    id: "completed_now",
    checked: completedNow,
    onChange: function onChange(e) {
      setCompletedNow(e.target.checked);
    }
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
    className: "p-1"
  }, "Completed Now"))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "cursor-pointer flex-1"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("h1", {
    className: "font-semibold mb-4"
  }, "Project Time Limit"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "relative flex"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "pr-6"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(react_dropdown__WEBPACK_IMPORTED_MODULE_2___default.a, {
    className: "project_time_limit",
    id: "time_limit_option",
    value: timeLimitOptionValue,
    options: time_limit_options,
    placeholder: "Please select...",
    onChange: function onChange(option) {
      setTimeLimitOptionValue(option.value);
    },
    name: "time_limit_option_value"
  }), errors["timeLimitValue"] && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
    className: "absolute px-1 text-red-400 tracking-widest  text-xs "
  }, errors["timeLimitValue"][0])), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components_base__WEBPACK_IMPORTED_MODULE_7__["TextInput"], {
    className: "pl-2 text-sm border-gray-400 w-32 border py-1",
    border: false,
    value: timeLimitValue,
    onChange: function onChange(e) {
      return Object(_helpers_numberInput__WEBPACK_IMPORTED_MODULE_20__["inputRegex"])(e.target.value) ? setTimeLimitValue(e.target.value) : null;
    } //name="time_limit_value"

  }), errors["time_limit_value"] && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
    className: "absolute px-1 text-red-400 tracking-widest  text-xs "
  }, errors["time_limit_value"][0]))))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "py-3"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("h1", {
    className: "font-semibold"
  }, " Featured Images (min. 3 images) "), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("label", {
    className: "button ",
    htmlFor: "upload-photos"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: ""
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "w-full flex flex-wrap  cursor-pointer",
    style: {
      border: "2px dashed #ccc",
      borderRadius: "20px",
      padding: "20px",
      textAlign: "center",
      marginBottom: "0px"
    },
    onDrop: function onDrop(e) {
      return handleDrop(e);
    },
    onDragOver: function onDragOver(e) {
      e.preventDefault();
      e.stopPropagation();
    },
    onDragEnter: function onDragEnter(e) {
      e.preventDefault();
      e.stopPropagation();
    },
    onDragLeave: function onDragLeave(e) {
      e.preventDefault();
      e.stopPropagation();
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("input", {
    type: "file",
    id: "upload-photos",
    accept: "image/*",
    multiple: true,
    className: "hidden",
    name: "image[]",
    onChange: function onChange(e) {
      return handleChange(e);
    }
  }), featuredImages.map(function (image, index) {
    return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
      key: index,
      className: "rounded-sm border m-2 relative",
      style: {
        backgroundImage: "url('".concat(image.preview, "')"),
        backgroundPositionX: "center",
        width: "6.9em",
        height: "8.9rem",
        backgroundSize: "cover"
      }
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
      className: "absolute bg-palette-blue-light font-bold leading-none ml-1 mt-1 rounded-full text-white w-1/4"
    }, index + 1), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components_base__WEBPACK_IMPORTED_MODULE_7__["Tooltip"], {
      title: "Remove this item"
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
      onClick: function onClick(e) {
        return handleRemove(e, index);
      },
      className: "absolute bottom-0 right-0 p-2 -mb-2 "
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_fortawesome_react_fontawesome__WEBPACK_IMPORTED_MODULE_14__["FontAwesomeIcon"], {
      className: "text-red-500 shadow-xl hover:font-bold hover:text-red-700",
      icon: _fortawesome_free_solid_svg_icons__WEBPACK_IMPORTED_MODULE_15__["faTimes"]
    }))));
  }), fmPlaceHolder.map(function (p, index) {
    return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
      key: index,
      className: "".concat(p.classes, " cursor-pointer flex justify-center items-center"),
      style: {
        width: "6.9rem",
        height: "8.9rem",
        border: "2px solid #ccc"
      }
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
      className: "absolute font-bold leading-none w-1/4"
    }, index + featuredImages.length + 1));
  })), limitError && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
    className: "px-1 text-red-400 tracking-widest  text-xs "
  }, "Please limit your photos to twelve(12) items only."))), errors["featuredImages"] && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
    className: "px-1 text-red-400 tracking-widest  text-xs "
  }, errors["featuredImages"][0])), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "flex flex-col mb-10"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "flex-1"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("h1", {
    className: "font-semibold"
  }, "Rich Preview Image "), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "py-3 flex flex-col"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "flex"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("label", {
    className: "w-full"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "cursor-pointer w-full p-3 rounded-lg flex",
    style: {
      border: "2px dashed #ccc",
      borderRadius: "20px"
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("input", {
    id: "rtp",
    ref: previewImageRef,
    type: "file",
    name: "prev_image",
    onChange: function onChange(e) {
      return handlePreviewImageDisplay(e);
    },
    className: "border text-sm border-gray-400 px-2 py-1 w-full py-1 hidden"
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("label", {
    htmlFor: "rtp",
    className: "cursor-pointer hover:text-palette-blue-light ".concat(filename ? "text-palette-blue-light" : "")
  }, filename || "Select File"), filename && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "cursor-pointer ml-4 mr-8 text-lg text-red-500"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_fortawesome_react_fontawesome__WEBPACK_IMPORTED_MODULE_14__["FontAwesomeIcon"], {
    onClick: function onClick(e) {
      return handleDeleteFile(e);
    },
    icon: _fortawesome_free_solid_svg_icons__WEBPACK_IMPORTED_MODULE_15__["faTimes"]
  })))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "flex items-center justify-end w-1/2"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components_base__WEBPACK_IMPORTED_MODULE_7__["Button"], {
    className: "font-bold rounded-full",
    disabled: loading
  }, loading && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_fortawesome_react_fontawesome__WEBPACK_IMPORTED_MODULE_14__["FontAwesomeIcon"], {
    icon: _fortawesome_free_solid_svg_icons__WEBPACK_IMPORTED_MODULE_15__["faSpinner"],
    className: "fa-spin mr-2"
  }), "Continue")))))))), toogleAddEditDeveloper && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components_AddDeveloperModal__WEBPACK_IMPORTED_MODULE_21__["default"], {
    title: "Add Developer",
    developer: {},
    toogleAddDeveloper: function toogleAddDeveloper() {
      return toogleAddUpdateDeveloper();
    }
  }));
};

/* harmony default export */ __webpack_exports__["default"] = (EditProjectPart);

/***/ }),

/***/ "./resources/js/admin/components/EditPropertyModal.js":
/*!************************************************************!*\
  !*** ./resources/js/admin/components/EditPropertyModal.js ***!
  \************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime/regenerator */ "./node_modules/@babel/runtime/regenerator/index.js");
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var react_dropdown__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! react-dropdown */ "./node_modules/react-dropdown/dist/index.js");
/* harmony import */ var react_dropdown__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(react_dropdown__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _helpers_styles_dropdown_css__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../helpers/styles/dropdown.css */ "./resources/js/helpers/styles/dropdown.css");
/* harmony import */ var _helpers_styles_dropdown_css__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(_helpers_styles_dropdown_css__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var _components_base__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../components/_base */ "./resources/js/components/_base/index.js");
/* harmony import */ var _components_base__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(_components_base__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var draft_js__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! draft-js */ "./node_modules/draft-js/lib/Draft.js");
/* harmony import */ var draft_js__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(draft_js__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var react_datepicker__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! react-datepicker */ "./node_modules/react-datepicker/dist/react-datepicker.min.js");
/* harmony import */ var react_datepicker__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(react_datepicker__WEBPACK_IMPORTED_MODULE_6__);
/* harmony import */ var _helpers_styles_datepicker_css__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../helpers/styles/datepicker.css */ "./resources/js/helpers/styles/datepicker.css");
/* harmony import */ var _helpers_styles_datepicker_css__WEBPACK_IMPORTED_MODULE_7___default = /*#__PURE__*/__webpack_require__.n(_helpers_styles_datepicker_css__WEBPACK_IMPORTED_MODULE_7__);
/* harmony import */ var draft_convert__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! draft-convert */ "./node_modules/draft-convert/esm/index.js");
/* harmony import */ var react_currency_format__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! react-currency-format */ "./node_modules/react-currency-format/lib/currency-format.js");
/* harmony import */ var react_currency_format__WEBPACK_IMPORTED_MODULE_9___default = /*#__PURE__*/__webpack_require__.n(react_currency_format__WEBPACK_IMPORTED_MODULE_9__);
/* harmony import */ var _fortawesome_react_fontawesome__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @fortawesome/react-fontawesome */ "./node_modules/@fortawesome/react-fontawesome/index.es.js");
/* harmony import */ var _fortawesome_free_solid_svg_icons__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! @fortawesome/free-solid-svg-icons */ "./node_modules/@fortawesome/free-solid-svg-icons/index.es.js");
/* harmony import */ var _states_userGlobal__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ../../states/userGlobal */ "./resources/js/states/userGlobal.js");
/* harmony import */ var _material_ui_icons__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! @material-ui/icons */ "./node_modules/@material-ui/icons/esm/index.js");
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! lodash */ "./node_modules/lodash/lodash.js");
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_14___default = /*#__PURE__*/__webpack_require__.n(lodash__WEBPACK_IMPORTED_MODULE_14__);
/* harmony import */ var _components_base_alerts_sweetAlert__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! ../../components/_base/alerts/sweetAlert */ "./resources/js/components/_base/alerts/sweetAlert.js");
/* harmony import */ var _helpers_propertyHelper_propertyHelper__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! ../../helpers/propertyHelper/propertyHelper */ "./resources/js/helpers/propertyHelper/propertyHelper.js");
/* harmony import */ var _data_index__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! ../../data/index */ "./resources/js/data/index.js");
/* harmony import */ var _helpers_fileHelper_fileHelper__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(/*! ../../helpers/fileHelper/fileHelper */ "./resources/js/helpers/fileHelper/fileHelper.js");
/* harmony import */ var _helpers_resourceHelper_index__WEBPACK_IMPORTED_MODULE_19__ = __webpack_require__(/*! ../../helpers/resourceHelper/index */ "./resources/js/helpers/resourceHelper/index.js");


function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

function _toConsumableArray(arr) { return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _unsupportedIterableToArray(arr) || _nonIterableSpread(); }

function _nonIterableSpread() { throw new TypeError("Invalid attempt to spread non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _iterableToArray(iter) { if (typeof Symbol !== "undefined" && Symbol.iterator in Object(iter)) return Array.from(iter); }

function _arrayWithoutHoles(arr) { if (Array.isArray(arr)) return _arrayLikeToArray(arr); }

function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _unsupportedIterableToArray(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

function _iterableToArrayLimit(arr, i) { if (typeof Symbol === "undefined" || !(Symbol.iterator in Object(arr))) return; var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }





















/* harmony default export */ __webpack_exports__["default"] = (function (_ref) {
  var projectName = _ref.projectName,
      projectId = _ref.projectId,
      property = _ref.property,
      toggleEditPropertyModal = _ref.toggleEditPropertyModal,
      editToggleFetch = _ref.editToggleFetch;

  var _UserGlobal = Object(_states_userGlobal__WEBPACK_IMPORTED_MODULE_12__["default"])(),
      _UserGlobal2 = _slicedToArray(_UserGlobal, 2),
      userState = _UserGlobal2[0],
      userActions = _UserGlobal2[1];

  var _useState = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(_toConsumableArray(property.featured_images.map(function (image) {
    return {
      image: image,
      preview: image
    };
  }))),
      _useState2 = _slicedToArray(_useState, 2),
      featuredImages = _useState2[0],
      setFeaturedImages = _useState2[1];

  var previewImageRef = Object(react__WEBPACK_IMPORTED_MODULE_1__["useRef"])(null);
  var floorPlanRef = Object(react__WEBPACK_IMPORTED_MODULE_1__["useRef"])(null);

  var _useState3 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(""),
      _useState4 = _slicedToArray(_useState3, 2),
      state = _useState4[0],
      setState = _useState4[1];

  var _useState5 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(String(property.property_type_id)),
      _useState6 = _slicedToArray(_useState5, 2),
      propertyType = _useState6[0],
      setPropertyType = _useState6[1];

  var _useState7 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(false),
      _useState8 = _slicedToArray(_useState7, 2),
      loading = _useState8[0],
      setLoading = _useState8[1];

  var _useState9 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(""),
      _useState10 = _slicedToArray(_useState9, 2),
      subPropertyType = _useState10[0],
      setSubPropertyType = _useState10[1];

  var _useState11 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(""),
      _useState12 = _slicedToArray(_useState11, 2),
      ownershipType = _useState12[0],
      setOwnershipType = _useState12[1];

  var _useState13 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])([]),
      _useState14 = _slicedToArray(_useState13, 2),
      errors = _useState14[0],
      setErrors = _useState14[1];

  var _useState15 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(function () {
    return draft_js__WEBPACK_IMPORTED_MODULE_5__["EditorState"].createEmpty();
  }),
      _useState16 = _slicedToArray(_useState15, 2),
      editorState = _useState16[0],
      setEditorState = _useState16[1];

  var _useState17 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])({}),
      _useState18 = _slicedToArray(_useState17, 2),
      floorPlan = _useState18[0],
      setFLoorPlan = _useState18[1];

  var _useState19 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(""),
      _useState20 = _slicedToArray(_useState19, 2),
      filename = _useState20[0],
      setFilename = _useState20[1];

  var _useState21 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(""),
      _useState22 = _slicedToArray(_useState21, 2),
      prevFilename = _useState22[0],
      setPrevFilename = _useState22[1];

  var _useState23 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])([]),
      _useState24 = _slicedToArray(_useState23, 2),
      previewImage = _useState24[0],
      setPreviewImage = _useState24[1];

  var _useState25 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(""),
      _useState26 = _slicedToArray(_useState25, 2),
      propertyPrice = _useState26[0],
      setPropertyPrice = _useState26[1];

  var _useState27 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(false),
      _useState28 = _slicedToArray(_useState27, 2),
      limitError = _useState28[0],
      setLimitError = _useState28[1];

  var _useState29 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])([]),
      _useState30 = _slicedToArray(_useState29, 2),
      fmPlaceHolder = _useState30[0],
      setFmPlaceHolder = _useState30[1];

  var _useState31 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(_helpers_propertyHelper_propertyHelper__WEBPACK_IMPORTED_MODULE_16__["costDueDates"]),
      _useState32 = _slicedToArray(_useState31, 2),
      dates = _useState32[0],
      setDates = _useState32[1];

  var _useState33 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(property.strata_cost || ""),
      _useState34 = _slicedToArray(_useState33, 2),
      strataCost = _useState34[0],
      setStrataCost = _useState34[1];

  var _useState35 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(property.water_cost || ""),
      _useState36 = _slicedToArray(_useState35, 2),
      waterCost = _useState36[0],
      setWaterCost = _useState36[1];

  var _useState37 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(property.council_cost || ""),
      _useState38 = _slicedToArray(_useState37, 2),
      councilCost = _useState38[0],
      setCouncilCost = _useState38[1];

  var _useState39 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(property.strata || ""),
      _useState40 = _slicedToArray(_useState39, 2),
      strata = _useState40[0],
      setStrata = _useState40[1];

  var _useState41 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(property.water || ""),
      _useState42 = _slicedToArray(_useState41, 2),
      water = _useState42[0],
      setWater = _useState42[1];

  var _useState43 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(property.council || ""),
      _useState44 = _slicedToArray(_useState43, 2),
      council = _useState44[0],
      setCouncil = _useState44[1];

  var addPropertyForm = Object(react__WEBPACK_IMPORTED_MODULE_1__["useRef"])(null);
  Object(react__WEBPACK_IMPORTED_MODULE_1__["useEffect"])(function () {
    var phs = [];

    for (var index = 0; index < 6 - featuredImages.length; index++) {
      var ph = {
        classes: "cursor-pointer m-2 inline-flex bg-transparent text-palette-gray"
      };
      phs.push(ph);
    }

    setFmPlaceHolder(phs);
  }, [featuredImages.length]);
  Object(react__WEBPACK_IMPORTED_MODULE_1__["useEffect"])(function () {
    setEditorState(function () {
      return draft_js__WEBPACK_IMPORTED_MODULE_5__["EditorState"].push(editorState, Object(draft_convert__WEBPACK_IMPORTED_MODULE_8__["convertFromHTML"])(property.description));
    });
  }, []);
  Object(react__WEBPACK_IMPORTED_MODULE_1__["useEffect"])(function () {
    if (property.floor_plan && property.floor_plan.file) {
      var first = property.floor_plan.file.split("/floorplan/")[1];
      var newFilename = first.split("?")[0];
      setFilename(newFilename);
      setFLoorPlan(property.floor_plan);
    }

    if (property.rich_preview_image) {
      var img = [];
      img.push({
        image: property.rich_preview_image,
        preview: property.rich_preview_image
      });
      var _first = "";

      if (property.rich_preview_image.includes("featured-images")) {
        _first = property.rich_preview_image.split("/featured-images/")[1];
      }

      if (property.rich_preview_image.includes("imgPreview")) {
        _first = property.rich_preview_image.split("/imgPreview/")[1];
      }

      if (_first) {
        var _newFilename = _first.split("?")[0];

        setPrevFilename(_newFilename);
        setPreviewImage(img);
        setPrevFilename(_newFilename);
        setPreviewImage(img);
      }
    }

    if (property.price) {
      setPropertyPrice(property.price);
    }
  }, []);

  var getFileName = function getFileName(resource) {
    var filename = "";

    if (typeof resource["file"].name == "string") {
      filename = resource["file"]["name"];
    } else {
      var first = resource["file"].split("/floorplan/")[1];
      filename = first.split("?")[0];
    }

    return filename;
  };

  var handleSubmit = function handleSubmit(e) {
    e.preventDefault();
    setLimitError(false);
    saveProperty();
  };

  var saveProperty = /*#__PURE__*/function () {
    var _ref2 = _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee() {
      var formData, url;
      return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee$(_context) {
        while (1) {
          switch (_context.prev = _context.next) {
            case 0:
              setLoading(true);
              formData = new FormData(addPropertyForm.current);
              formData.append("role", "project_developer");

              if (floorPlan && !Object(lodash__WEBPACK_IMPORTED_MODULE_14__["isEmpty"])(floorPlan)) {
                formData.append("floorPlan[file]", floorPlan.file);
                formData.append("floorPlan[name]", "floorplan");
              }

              formData.append("projectId", projectId);
              formData.append("water", water);
              formData.append("council", council);
              formData.append("strata", strata);
              formData.append("propertyType", propertyType);
              formData.append("description", editorState.getCurrentContent().getPlainText());
              formData.append("descriptionHTML", Object(draft_convert__WEBPACK_IMPORTED_MODULE_8__["convertToHTML"])(editorState.getCurrentContent()));
              featuredImages.forEach(function (image, key) {
                formData.append("featuredImages[".concat(key, "]"), image.image);
              });

              if (previewImage.length) {
                formData.append("preview_img[".concat(0, "]"), previewImage[0].image);
              } else {
                formData.append("preview_img[".concat(0, "]"), "");
              }

              formData.append("_method", "PATCH");
              url = "/api/property/".concat(property.id);
              _context.next = 17;
              return Object(_data_index__WEBPACK_IMPORTED_MODULE_17__["saveUpdateProperty"])({
                formData: formData,
                url: url
              }).then(function (retVal) {
                if (retVal.status === 200) {
                  userActions.setState({
                    propertiesData: retVal.data
                  });
                  setLoading(false);
                  Object(_components_base_alerts_sweetAlert__WEBPACK_IMPORTED_MODULE_15__["sweetAlert"])("success", "Property successfully updated.");
                  editToggleFetch(); // toggleEditPropertyModal();
                }
              })["catch"](function (error) {
                setErrors(error.response.data.errors);
                setLoading(false);
              }).then(function () {});

            case 17:
            case "end":
              return _context.stop();
          }
        }
      }, _callee);
    }));

    return function saveProperty() {
      return _ref2.apply(this, arguments);
    };
  }();

  var handlePreviewImageDisplay = function handlePreviewImageDisplay(e) {
    e.preventDefault();

    if (e.target.files.length > 1) {
      Object(_components_base_alerts_sweetAlert__WEBPACK_IMPORTED_MODULE_15__["sweetAlert"])("error", "Multiple upload is not allowed!");
      return;
    }

    if (!Object(_helpers_fileHelper_fileHelper__WEBPACK_IMPORTED_MODULE_18__["imageFileTypeFilter"])(e.target.files[0]["name"])) {
      Object(_components_base_alerts_sweetAlert__WEBPACK_IMPORTED_MODULE_15__["sweetAlert"])("error", "Filetype is not supported!");
      e.target.value = null;
      return;
    }

    var imgArr = [];
    setPreviewImage(imgArr.concat(_toConsumableArray(_toConsumableArray(e.target.files).map(function (file) {
      var newFilename = Object(_helpers_fileHelper_fileHelper__WEBPACK_IMPORTED_MODULE_18__["sanitizeFilename"])(file.name);
      var newFileObj = new File([file], newFilename);
      setPrevFilename(newFilename);
      return {
        image: newFileObj,
        preview: URL.createObjectURL(newFileObj)
      };
    }))));
  };

  var handleDrop = function handleDrop(e) {
    e.preventDefault();
    e.stopPropagation();
    var fmCount = featuredImages.length;
    var onCount = e.dataTransfer.files.length;

    if (fmCount + onCount > 6) {
      e.target.value = null;
      setLimitError(true);
      return;
    } else {
      setLimitError(false);
    }

    setFeaturedImages(featuredImages.concat(_toConsumableArray(_toConsumableArray(e.dataTransfer.files).map(function (file) {
      var newFilename = Object(_helpers_fileHelper_fileHelper__WEBPACK_IMPORTED_MODULE_18__["sanitizeFilename"])(file.name);
      var newFileObj = new File([file], newFilename);
      return {
        image: newFileObj,
        preview: URL.createObjectURL(newFileObj)
      };
    }))));
    e.target.value = null;
  };

  var handleChange = function handleChange(e) {
    var fmCount = featuredImages.length;
    var onCount = e.target.files.length;

    if (fmCount + onCount > 6) {
      setLimitError(true);
      e.target.value = null;
      return;
    } else {
      setLimitError(false);
    }

    setFeaturedImages(featuredImages.concat(_toConsumableArray(_toConsumableArray(e.target.files).map(function (file) {
      var newFilename = Object(_helpers_fileHelper_fileHelper__WEBPACK_IMPORTED_MODULE_18__["sanitizeFilename"])(file.name);
      var newFileObj = new File([file], newFilename);
      return {
        image: newFileObj,
        preview: URL.createObjectURL(newFileObj)
      };
    }))));
    e.target.value = null;
  };

  var handleRemove = function handleRemove(e, i) {
    e.preventDefault();
    e.stopPropagation();
    setFeaturedImages(_toConsumableArray(featuredImages.filter(function (image, k) {
      if (i !== k) {
        return image;
      }
    })));

    if (featuredImages.length > 6) {
      setLimitError(true);
    } else {
      setLimitError(false);
    }
  };

  var handleFloorPlanChange = function handleFloorPlanChange(e) {
    e.preventDefault();

    if (!Object(_helpers_fileHelper_fileHelper__WEBPACK_IMPORTED_MODULE_18__["PDFFileTypeFilter"])(e.target.files[0]["name"])) {
      Object(_components_base_alerts_sweetAlert__WEBPACK_IMPORTED_MODULE_15__["sweetAlert"])("error", "Filetype is not supported!");
      e.target.value = null;
      return;
    }

    setFLoorPlan({
      file: _helpers_resourceHelper_index__WEBPACK_IMPORTED_MODULE_19__["resources"].trimFloorPlan(e.target.files[0]),
      name: "floor_plan"
    });
    var newFilename = Object(_helpers_fileHelper_fileHelper__WEBPACK_IMPORTED_MODULE_18__["sanitizeFilename"])(e.target.files[0]["name"]);
    setFilename(newFilename); // e.target.value = null;
  };

  var handleDeleteFile = function handleDeleteFile(e) {
    e.preventDefault();
    setFLoorPlan({});
    floorPlanRef.current.type = "";
    floorPlanRef.current.type = "file";
    setFilename("");
    e.target.value = null;
  };

  var handleDeletePrevFile = function handleDeletePrevFile(e) {
    e.preventDefault();
    setPreviewImage([]);
    previewImageRef.current.type = "";
    previewImageRef.current.type = "file";
    setPrevFilename("");
    e.target.value = null;
  };

  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components_base__WEBPACK_IMPORTED_MODULE_4__["Modal"], {
    show: true,
    maxWidth: "md",
    title: "Edit Property",
    onClose: function onClose() {
      return toggleEditPropertyModal();
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("form", {
    ref: addPropertyForm,
    className: "bg-white rounded-lg py-5 px-8 m-5",
    onSubmit: handleSubmit
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "text-black "
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "py-3 flex justify-start "
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "flex justify-center items-center"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("label", {
    className: "font-semibold w-24"
  }, "Property Type"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(react_dropdown__WEBPACK_IMPORTED_MODULE_2___default.a, {
    id: "property_type",
    value: propertyType,
    options: _helpers_propertyHelper_propertyHelper__WEBPACK_IMPORTED_MODULE_16__["property_types"],
    onChange: function onChange(option) {
      setPropertyType(option.value);
    },
    placeholder: "Please select...",
    name: "property_type",
    className: "w-48 ml-4"
  }), errors["propertyType"] && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
    className: "px-1 text-red-400 tracking-widest  text-xs "
  }, errors["propertyType"][0]))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "flex flex-1 items-center justify-end ml-10"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("label", {
    className: "font-semibold w-32 mr-4"
  }, "Property Number"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components_base__WEBPACK_IMPORTED_MODULE_4__["TextInput"], {
    defaultValue: property.unit_name,
    className: "border-gray-400 border pl-2 py-1 w-24",
    border: false,
    name: "unit_name",
    width: "w-24"
  }), errors["unit_name"] && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
    className: "px-1 text-red-400 tracking-widest  text-xs "
  }, errors["unit_name"][0]))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "flex flex-1 items-center justify-end ml-4"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("label", {
    className: "font-semibold w-24 mr-4"
  }, "Lot Number"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components_base__WEBPACK_IMPORTED_MODULE_4__["TextInput"], {
    defaultValue: property.unit_no,
    className: "border-gray-400 border pl-2 py-1 w-24",
    border: false,
    name: "unit_no",
    width: "w-24"
  }), errors["unit_no"] && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
    className: "px-1 text-red-400 tracking-widest  text-xs "
  }, errors["unit_no"][0])))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "flex justify-start my-3"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "flex items-center justify-start py-3"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("label", {
    className: "w-12"
  }, "Price"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "relative"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(react_currency_format__WEBPACK_IMPORTED_MODULE_9___default.a, {
    allowNegative: false,
    className: "border text-sm border-gray-400 px-2 py-1 w-48 ".concat(property.is_property_approved ? "bg-gray-300 cursor-not-allowed" : ""),
    thousandSeparator: true,
    value: propertyPrice,
    onValueChange: function onValueChange(e) {
      setPropertyPrice(e.value);
    },
    name: "price",
    id: "price",
    prefix: "$",
    readOnly: property.is_property_approved
  })), errors["price"] && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
    className: "absolute px-1 text-red-400 tracking-widest text-xs "
  }, errors["price"][0]))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "py-3 flex items-center ml-12"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("label", {
    title: "Number of bedrooms",
    className: "cursor-pointer mr-4"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_material_ui_icons__WEBPACK_IMPORTED_MODULE_13__["LocalHotel"], null)), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "flex flex-col w-24"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components_base__WEBPACK_IMPORTED_MODULE_4__["TextInput"], {
    className: "pl-2 text-sm border-gray-400 w-32 border py-1",
    border: false,
    type: "number",
    name: "number_of_bedrooms",
    defaultValue: property.no_of_bedrooms
  }), errors["number_of_bedrooms"] && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
    className: "px-1 text-red-400 tracking-widest  text-xs "
  }, errors["number_of_bedrooms"][0]))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "py-3 flex items-center ml-12"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("label", {
    title: "Number of bathrooms",
    className: "cursor-pointer mr-4"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_material_ui_icons__WEBPACK_IMPORTED_MODULE_13__["Bathtub"], null)), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "flex flex-col w-24"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components_base__WEBPACK_IMPORTED_MODULE_4__["TextInput"], {
    className: "pl-2 text-sm border-gray-400 w-32 border py-1",
    border: false,
    type: "number",
    name: "number_of_bathrooms",
    defaultValue: property.no_of_bathrooms
  }), errors["number_of_bathrooms"] && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
    className: "px-1 text-red-400 tracking-widest  text-xs "
  }, errors["number_of_bathrooms"][0]))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "py-3 flex items-center ml-12"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("label", {
    title: "Number of garages",
    className: "cursor-pointer mr-4 "
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_material_ui_icons__WEBPACK_IMPORTED_MODULE_13__["DirectionsCar"], null)), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "flex flex-col w-24"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components_base__WEBPACK_IMPORTED_MODULE_4__["TextInput"], {
    className: "pl-2 text-sm border-gray-400 w-32 border py-1",
    border: false,
    type: "number",
    name: "number_of_garages",
    min: "1",
    defaultValue: property.no_of_garages
  }), errors["number_of_garages"] && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
    className: "px-1 text-red-400 tracking-widest  text-xs "
  }, errors["number_of_garages"][0])))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "flex"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "py-3 flex justify-start items-center"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("label", {
    className: "w-24"
  }, "Internal SQM"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "relative"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "ml-4 w-32"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components_base__WEBPACK_IMPORTED_MODULE_4__["TextInput"], {
    className: "pl-2 text-sm border-gray-400 w-32 border py-1",
    border: false,
    type: "number",
    name: "internal_size",
    defaultValue: property.internal_size
  })), errors["internal_size"] && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
    className: "absolute px-1 text-red-400 tracking-widest  text-xs "
  }, errors["internal_size"][0]))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "py-3 flex justify-start items-center mx-10"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("label", {
    className: "w-24"
  }, "External SQM"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "relative"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "ml-4 w-32"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components_base__WEBPACK_IMPORTED_MODULE_4__["TextInput"], {
    className: "pl-2 text-sm border-gray-400 w-32 border py-1",
    border: false,
    type: "number",
    name: "external_size",
    defaultValue: property.external_size
  })), errors["external_size"] && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
    className: "absolute px-1 text-red-400 tracking-widest  text-xs "
  }, errors["external_size"][0]))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "py-3 flex justify-start items-center ml-3"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("label", {
    className: "w-24"
  }, "Parking SQM"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "relative"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "ml-4 w-32"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components_base__WEBPACK_IMPORTED_MODULE_4__["TextInput"], {
    className: "pl-2 text-sm border-gray-400 w-32 border py-1 mr-1",
    border: false,
    type: "number",
    name: "parking_size",
    defaultValue: property.parking_size
  })), errors["parking_size"] && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
    className: "absolute px-1 text-red-400 tracking-widest  text-xs "
  }, errors["parking_size"][0])))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "flex justify-center my-3"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "py-3 flex flex-1 items-center"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("label", {
    title: "Strata",
    className: "cursor-pointer mr-4 ".concat(errors["strata"] ? "mb-4" : "mb-0")
  }, "Strata"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "flex ml-2"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(react_currency_format__WEBPACK_IMPORTED_MODULE_9___default.a, {
    allowNegative: false,
    className: "border text-sm border-gray-400 px-2 py-1 w-32",
    thousandSeparator: true,
    name: "strata_cost",
    id: "strata_cost",
    prefix: "$",
    value: strataCost,
    onValueChange: function onValueChange(e) {
      setStrataCost(e.value);
    }
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(react_dropdown__WEBPACK_IMPORTED_MODULE_2___default.a, {
    id: "strata",
    value: strata,
    options: dates,
    onChange: function onChange(option) {
      setStrata(option.value);
    },
    placeholder: "Please select...",
    name: "strata",
    className: "w-48 ml-2"
  })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", null, errors["strata"] && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
    className: "px-1 text-red-400 tracking-widest  text-xs "
  }, errors["strata"][0]), errors["strata_cost"] && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
    className: "px-1 text-red-400 tracking-widest  text-xs "
  }, errors["strata_cost"][0])))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "flex flex-1 items-center justify-end py-3"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("label", {
    title: "Water",
    className: "cursor-pointer mr-4 ".concat(errors["water"] ? "mb-4" : "mb-0")
  }, "Water"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "flex ml-2"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(react_currency_format__WEBPACK_IMPORTED_MODULE_9___default.a, {
    allowNegative: false,
    className: "border text-sm border-gray-400 px-2 py-1 w-32",
    thousandSeparator: true,
    name: "water_cost",
    id: "water_cost",
    prefix: "$",
    value: waterCost,
    onValueChange: function onValueChange(e) {
      setWaterCost(e.value);
    }
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(react_dropdown__WEBPACK_IMPORTED_MODULE_2___default.a, {
    id: "water",
    value: water,
    options: dates,
    onChange: function onChange(option) {
      setWater(option.value);
    },
    placeholder: "Please select...",
    name: "water",
    className: "w-48 ml-2"
  })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", null, errors["water"] && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
    className: "px-1 text-red-400 tracking-widest  text-xs "
  }, errors["water"][0]), errors["water_cost"] && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
    className: "px-1 text-red-400 tracking-widest  text-xs "
  }, errors["water_cost"][0]))))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "flex justify-center my-3"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "py-3 flex flex-1 items-center"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("label", {
    title: "Council",
    className: "cursor-pointer mr-3 ".concat(errors["council"] ? "mb-4" : "mb-0")
  }, "Council"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "flex ml-1"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(react_currency_format__WEBPACK_IMPORTED_MODULE_9___default.a, {
    allowNegative: false,
    className: "border text-sm border-gray-400 px-2 py-1 w-32",
    thousandSeparator: true,
    name: "council_cost",
    id: "council_cost",
    prefix: "$",
    value: councilCost,
    onValueChange: function onValueChange(e) {
      setCouncilCost(e.value);
    }
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(react_dropdown__WEBPACK_IMPORTED_MODULE_2___default.a, {
    id: "council",
    value: council,
    options: dates,
    onChange: function onChange(option) {
      setCouncil(option.value);
    },
    placeholder: "Please select...",
    name: "council",
    className: "w-48 ml-2"
  })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", null, errors["council"] && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
    className: "px-1 text-red-400 tracking-widest  text-xs "
  }, errors["council"][0]), errors["council_cost"] && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
    className: "px-1 text-red-400 tracking-widest  text-xs "
  }, errors["council_cost"][0]))))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "py-3"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "w-full text-sm bg-white text-black "
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components_base__WEBPACK_IMPORTED_MODULE_4__["RichTextEditor"], {
    key: property.id,
    title: "Property Description",
    richEditorState: editorState,
    richEditorStateContent: function richEditorStateContent(content) {
      return setEditorState(content);
    },
    characterLimit: 2000
  })), errors["description"] && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
    className: "px-1 text-red-400 tracking-widest  text-xs "
  }, errors["description"][0])), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "py-3"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("h1", {
    className: "font-semibold"
  }, " Featured Images (min. 3 images) "), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("label", {
    className: "button ",
    htmlFor: "upload-photos"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: ""
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "w-full flex flex-wrap  cursor-pointer",
    style: {
      border: "2px dashed #ccc",
      borderRadius: "20px",
      padding: "20px",
      textAlign: "center",
      marginBottom: "0px"
    },
    onDrop: function onDrop(e) {
      return handleDrop(e);
    },
    onDragOver: function onDragOver(e) {
      e.preventDefault();
      e.stopPropagation();
    },
    onDragEnter: function onDragEnter(e) {
      e.preventDefault();
      e.stopPropagation();
    },
    onDragLeave: function onDragLeave(e) {
      e.preventDefault();
      e.stopPropagation();
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("input", {
    type: "file",
    id: "upload-photos",
    accept: "image/*",
    multiple: true,
    className: "hidden",
    name: "image[]",
    onChange: function onChange(e) {
      return handleChange(e);
    }
  }), featuredImages.map(function (image, index) {
    return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
      key: index,
      className: "rounded-sm border m-2 relative",
      style: {
        backgroundImage: "url('".concat(image.preview, "')"),
        backgroundPositionX: "center",
        width: "6.9em",
        height: "8.9rem",
        backgroundSize: "cover"
      }
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components_base__WEBPACK_IMPORTED_MODULE_4__["Tooltip"], {
      title: "Remove this item"
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
      onClick: function onClick(e) {
        return handleRemove(e, index);
      },
      className: "absolute bottom-0 right-0 p-2 -mb-2 "
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_fortawesome_react_fontawesome__WEBPACK_IMPORTED_MODULE_10__["FontAwesomeIcon"], {
      className: "text-red-500 shadow-xl hover:font-bold hover:text-red-700",
      icon: _fortawesome_free_solid_svg_icons__WEBPACK_IMPORTED_MODULE_11__["faTimes"]
    }))));
  }), fmPlaceHolder.map(function (p, i) {
    return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
      key: i,
      className: "".concat(p.classes, " cursor-pointer flex justify-center items-center"),
      style: {
        width: "6.9rem",
        height: "8.9rem",
        border: "2px solid #ccc"
      }
    }, " ", /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_fortawesome_react_fontawesome__WEBPACK_IMPORTED_MODULE_10__["FontAwesomeIcon"], {
      className: "text-xl",
      icon: _fortawesome_free_solid_svg_icons__WEBPACK_IMPORTED_MODULE_11__["faCamera"]
    }), " ");
  })), limitError && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
    className: "px-1 text-red-400 tracking-widest  text-xs "
  }, "Please limit your photos to six items only."))), errors["featuredImages"] && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
    className: "px-1 text-red-400 tracking-widest  text-xs "
  }, errors["featuredImages"][0])), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "flex flex-row"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "w-1/2 mr-2"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("h1", {
    className: "font-semibold"
  }, "Floor Plan "), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "flex flex-col justify-between py-3"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("label", {
    className: "w-full"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "items-center cursor-pointer w-full p-3 rounded-lg flex",
    style: {
      border: "2px dashed #ccc",
      borderRadius: "20px"
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("input", {
    id: "floorPlan",
    ref: floorPlanRef,
    type: "file",
    name: "floor_plan",
    onChange: function onChange(e) {
      return handleFloorPlanChange(e);
    },
    className: "hidden border text-sm border-gray-400 px-2 py-1 w-full py-1"
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("label", {
    htmlFor: "floorPlan",
    className: " cursor-pointer hover:text-palette-blue-light ".concat(filename ? "text-palette-blue-light" : "")
  }, filename || "Select File"), filename && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "text-red-500 text-lg cursor-pointer ml-3"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_fortawesome_react_fontawesome__WEBPACK_IMPORTED_MODULE_10__["FontAwesomeIcon"], {
    onClick: function onClick(e) {
      return handleDeleteFile(e);
    },
    icon: _fortawesome_free_solid_svg_icons__WEBPACK_IMPORTED_MODULE_11__["faTimes"]
  })))), errors["floorPlan"] && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
    className: "px-1 text-red-400 tracking-widest  text-xs "
  }, errors["floorPlan"][0])))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "w-1/2 ml-2"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("h1", {
    className: "font-semibold"
  }, "Rich Text Preview "), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "flex flex-col justify-between py-3"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "flex"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("label", {
    className: "w-full"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "items-center cursor-pointer w-full p-3 rounded-lg flex",
    style: {
      border: "2px dashed #ccc",
      borderRadius: "20px"
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("input", {
    id: "rtp",
    ref: previewImageRef,
    type: "file",
    name: "prev_image",
    onChange: function onChange(e) {
      return handlePreviewImageDisplay(e);
    },
    className: "hidden border text-sm border-gray-400 px-2 py-1 w-full py-1"
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("label", {
    htmlFor: "rtp",
    className: " cursor-pointer hover:text-palette-blue-light ".concat(prevFilename ? "text-palette-blue-light" : "")
  }, prevFilename || "Select File"), prevFilename && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "text-red-500 text-lg cursor-pointer ml-3"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_fortawesome_react_fontawesome__WEBPACK_IMPORTED_MODULE_10__["FontAwesomeIcon"], {
    onClick: function onClick(e) {
      return handleDeletePrevFile(e);
    },
    icon: _fortawesome_free_solid_svg_icons__WEBPACK_IMPORTED_MODULE_11__["faTimes"]
  })))))))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "flex justify-end my-5"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components_base__WEBPACK_IMPORTED_MODULE_4__["Button"], {
    className: "font-bold rounded-full",
    disabled: loading
  }, loading && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_fortawesome_react_fontawesome__WEBPACK_IMPORTED_MODULE_10__["FontAwesomeIcon"], {
    icon: _fortawesome_free_solid_svg_icons__WEBPACK_IMPORTED_MODULE_11__["faSpinner"],
    className: "fa-spin mr-2"
  }), "Update")))));
});

/***/ }),

/***/ "./resources/js/admin/components/RichTextEditor.js":
/*!*********************************************************!*\
  !*** ./resources/js/admin/components/RichTextEditor.js ***!
  \*********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var draft_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! draft-js */ "./node_modules/draft-js/lib/Draft.js");
/* harmony import */ var draft_js__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(draft_js__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var draft_convert__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! draft-convert */ "./node_modules/draft-convert/esm/index.js");
/* harmony import */ var _helpers_styles_editor_css__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../helpers/styles/editor.css */ "./resources/js/helpers/styles/editor.css");
/* harmony import */ var _helpers_styles_editor_css__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(_helpers_styles_editor_css__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var draft_js_dist_Draft_css__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! draft-js/dist/Draft.css */ "./node_modules/draft-js/dist/Draft.css");
/* harmony import */ var draft_js_dist_Draft_css__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(draft_js_dist_Draft_css__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var _fortawesome_react_fontawesome__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @fortawesome/react-fontawesome */ "./node_modules/@fortawesome/react-fontawesome/index.es.js");
/* harmony import */ var _fortawesome_free_solid_svg_icons__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @fortawesome/free-solid-svg-icons */ "./node_modules/@fortawesome/free-solid-svg-icons/index.es.js");
/* harmony import */ var _components_base_alerts_sweetAlert__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../components/_base/alerts/sweetAlert */ "./resources/js/components/_base/alerts/sweetAlert.js");
function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _unsupportedIterableToArray(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

function _iterableToArrayLimit(arr, i) { if (typeof Symbol === "undefined" || !(Symbol.iterator in Object(arr))) return; var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }









/* harmony default export */ __webpack_exports__["default"] = (function (_ref) {
  var _ref$title = _ref.title,
      title = _ref$title === void 0 ? "" : _ref$title,
      richEditorState = _ref.richEditorState,
      richEditorStateContent = _ref.richEditorStateContent,
      _ref$characterLimit = _ref.characterLimit,
      characterLimit = _ref$characterLimit === void 0 ? -1 : _ref$characterLimit;

  var _useState = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])(richEditorState),
      _useState2 = _slicedToArray(_useState, 2),
      editorState = _useState2[0],
      setEditorState = _useState2[1];

  var editor = Object(react__WEBPACK_IMPORTED_MODULE_0__["useRef"])(null);

  var onChange = function onChange(editorState1) {
    setEditorState(editorState1);
    richEditorStateContent(editorState1);
  };

  var handleKeyCommand = function handleKeyCommand(command) {
    return _handleKeyCommand(command);
  };

  var toggleBlockType = function toggleBlockType(type) {
    return _toggleBlockType(type);
  };

  var toggleInlineStyle = function toggleInlineStyle(style) {
    return _toggleInlineStyle(style);
  };

  var _handleKeyCommand = function _handleKeyCommand(command) {
    var newState = draft_js__WEBPACK_IMPORTED_MODULE_1__["RichUtils"].handleKeyCommand(editorState, command);

    if (newState) {
      onChange(newState);
      return true;
    }

    return false;
  };

  var _toggleBlockType = function _toggleBlockType(blockType) {
    onChange(draft_js__WEBPACK_IMPORTED_MODULE_1__["RichUtils"].toggleBlockType(editorState, blockType));
  };

  var _toggleInlineStyle = function _toggleInlineStyle(inlineStyle) {
    onChange(draft_js__WEBPACK_IMPORTED_MODULE_1__["RichUtils"].toggleInlineStyle(editorState, inlineStyle));
  };

  var className = "RichEditor-editor";
  var contentState = editorState.getCurrentContent();

  if (!contentState.hasText()) {
    if (contentState.getBlockMap().first().getType() !== "unstyled") {
      className += " RichEditor-hidePlaceholder";
    }
  }

  var getLengthOfSelectedText = function getLengthOfSelectedText() {
    var currentSelection = editorState.getSelection();
    var isCollapsed = currentSelection.isCollapsed();
    var length = 0;

    if (!isCollapsed) {
      var currentContent = editorState.getCurrentContent();
      var startKey = currentSelection.getStartKey();
      var endKey = currentSelection.getEndKey();
      var startBlock = currentContent.getBlockForKey(startKey);
      var isStartAndEndBlockAreTheSame = startKey === endKey;
      var startBlockTextLength = startBlock.getLength();
      var startSelectedTextLength = startBlockTextLength - currentSelection.getStartOffset();
      var endSelectedTextLength = currentSelection.getEndOffset();
      var keyAfterEnd = currentContent.getKeyAfter(endKey);

      if (isStartAndEndBlockAreTheSame) {
        length += currentSelection.getEndOffset() - currentSelection.getStartOffset();
      } else {
        var currentKey = startKey;

        while (currentKey && currentKey !== keyAfterEnd) {
          if (currentKey === startKey) {
            length += startSelectedTextLength + 1;
          } else if (currentKey === endKey) {
            length += endSelectedTextLength;
          } else {
            length += currentContent.getBlockForKey(currentKey).getLength() + 1;
          }

          currentKey = currentContent.getKeyAfter(currentKey);
        }
      }
    }

    return length;
  };

  var handleBeforeInput = function handleBeforeInput() {
    var currentContent = editorState.getCurrentContent();
    var currentContentLength = currentContent.getPlainText("").length;
    var selectedTextLength = getLengthOfSelectedText(editorState);

    if (characterLimit !== -1) {
      if (currentContentLength - selectedTextLength > characterLimit - 1) {
        return "handled";
      }
    }
  };

  var handlePastedText = function handlePastedText(pastedText) {
    var currentContent = editorState.getCurrentContent();
    var currentContentLength = currentContent.getPlainText("").length;
    var selectedTextLength = getLengthOfSelectedText();

    if (characterLimit !== -1) {
      if (currentContentLength + pastedText.length - selectedTextLength > characterLimit) {
        Object(_components_base_alerts_sweetAlert__WEBPACK_IMPORTED_MODULE_7__["sweetAlert"])('error', 'Input should be less than or equal to 2000 characters!');
        return "handled";
      }
    }
  };

  var myKeyBindingFn = function myKeyBindingFn(e) {
    var hasCommandModifier = draft_js__WEBPACK_IMPORTED_MODULE_1__["KeyBindingUtil"].hasCommandModifier;

    if (e.keyCode === 90
    /* `Z` key */
    && hasCommandModifier(e) && !e.shiftKey) {
      setEditorState(draft_js__WEBPACK_IMPORTED_MODULE_1__["EditorState"].undo(editorState));
    }
  };

  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react__WEBPACK_IMPORTED_MODULE_0___default.a.Fragment, null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "flex"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "w-full font-bold"
  }, title, " "), characterLimit !== -1 && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: " Richeditor-counter text-right "
  }, "Characters remaining: ", /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
    className: "w-10"
  }, characterLimit - editorState.getCurrentContent().getPlainText().length))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "RichEditor-root"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(InlineStyleControls, {
    editorState: editorState,
    onToggle: toggleInlineStyle
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(BlockStyleControls, {
    editorState: editorState,
    onToggle: toggleBlockType
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: className,
    onClick: focus
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(draft_js__WEBPACK_IMPORTED_MODULE_1__["Editor"], {
    blockStyleFn: getBlockStyle,
    customStyleMap: styleMap,
    editorState: editorState,
    handleKeyCommand: handleKeyCommand,
    handleBeforeInput: handleBeforeInput,
    handlePastedText: handlePastedText,
    onChange: onChange,
    ref: editor,
    spellCheck: true
  }))));
});
var styleMap = {
  CODE: {
    backgroundColor: "rgba(0, 0, 0, 0.05)",
    fontFamily: '"Inconsolata", "Menlo", "Consolas", monospace',
    fontSize: 16,
    padding: 2
  }
};

var getBlockStyle = function getBlockStyle(block) {
  switch (block.getType()) {
    case "blockquote":
      return "RichEditor-blockquote";

    default:
      return null;
  }
};

var BLOCK_TYPES = [//  { label: "H1", style: "header-one" },
//  { label: "H2", style: "header-two" },
//  { label: "H3", style: "header-three" },
//  { label: "H4", style: "header-four" },
//  { label: "H5", style: "header-five" },
//  { label: "H6", style: "header-six" },
// { label: "Blockquote", style: "blockquote" },
{
  label: "list-ul",
  style: "unordered-list-item"
}, {
  label: "list-ol",
  style: "ordered-list-item"
} //  { label: "Code Block", style: "code-block" }
];

var BlockStyleControls = function BlockStyleControls(props) {
  var editorState = props.editorState;
  var selection = editorState.getSelection();
  var blockType = editorState.getCurrentContent().getBlockForKey(selection.getStartKey()).getType();
  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "RichEditor-controls inline-flex"
  }, BLOCK_TYPES.map(function (type) {
    return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(StyleButton, {
      key: type.label,
      active: type.style === blockType,
      label: type.label,
      onToggle: props.onToggle,
      style: type.style
    });
  }));
};

var INLINE_STYLES = [{
  label: "bold",
  style: "BOLD"
}, {
  label: "italic",
  style: "ITALIC"
}, {
  label: "underline",
  style: "UNDERLINE"
} //  { label: "Monospace", style: "CODE" }
];

var InlineStyleControls = function InlineStyleControls(props) {
  var currentStyle = props.editorState.getCurrentInlineStyle();
  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "RichEditor-controls inline-flex"
  }, INLINE_STYLES.map(function (type) {
    return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(StyleButton, {
      key: type.label,
      active: currentStyle.has(type.style),
      label: type.label,
      onToggle: props.onToggle,
      style: type.style
    });
  }));
};

var StyleButton = function StyleButton(props) {
  var onToggle = function onToggle(e) {
    e.preventDefault();
    props.onToggle(props.style);
  };

  var className = "RichEditor-styleButton";

  if (props.active) {
    className += " RichEditor-activeButton";
  }

  var styleIcon = function styleIcon(icon) {
    switch (icon) {
      case "bold":
        return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_fortawesome_react_fontawesome__WEBPACK_IMPORTED_MODULE_5__["FontAwesomeIcon"], {
          icon: _fortawesome_free_solid_svg_icons__WEBPACK_IMPORTED_MODULE_6__["faBold"]
        });
        break;

      case "italic":
        return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_fortawesome_react_fontawesome__WEBPACK_IMPORTED_MODULE_5__["FontAwesomeIcon"], {
          icon: _fortawesome_free_solid_svg_icons__WEBPACK_IMPORTED_MODULE_6__["faItalic"]
        });
        break;

      case "underline":
        return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_fortawesome_react_fontawesome__WEBPACK_IMPORTED_MODULE_5__["FontAwesomeIcon"], {
          icon: _fortawesome_free_solid_svg_icons__WEBPACK_IMPORTED_MODULE_6__["faUnderline"]
        });
        break;

      case "list-ul":
        return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_fortawesome_react_fontawesome__WEBPACK_IMPORTED_MODULE_5__["FontAwesomeIcon"], {
          icon: _fortawesome_free_solid_svg_icons__WEBPACK_IMPORTED_MODULE_6__["faListUl"]
        });
        break;

      case "list-ol":
        return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_fortawesome_react_fontawesome__WEBPACK_IMPORTED_MODULE_5__["FontAwesomeIcon"], {
          icon: _fortawesome_free_solid_svg_icons__WEBPACK_IMPORTED_MODULE_6__["faListOl"]
        });
        break;

      default:
        break;
    }
  };

  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
    className: className,
    onMouseDown: onToggle
  }, styleIcon(props.label));
};

/***/ }),

/***/ "./resources/js/admin/components/SequenceAddingModal.js":
/*!**************************************************************!*\
  !*** ./resources/js/admin/components/SequenceAddingModal.js ***!
  \**************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _components_AddingSteps__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../components/AddingSteps */ "./resources/js/components/AddingSteps.js");
/* harmony import */ var _AddProjectPart__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./AddProjectPart */ "./resources/js/admin/components/AddProjectPart.js");
/* harmony import */ var _AddProjectResources__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./AddProjectResources */ "./resources/js/admin/components/AddProjectResources.js");
/* harmony import */ var _AdminPropertyManager__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./AdminPropertyManager */ "./resources/js/admin/components/AdminPropertyManager.js");
/* harmony import */ var _AgencyAgreementPart__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./AgencyAgreementPart */ "./resources/js/admin/components/AgencyAgreementPart.js");
/* harmony import */ var _components_base__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../components/_base */ "./resources/js/components/_base/index.js");
/* harmony import */ var _components_base__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(_components_base__WEBPACK_IMPORTED_MODULE_6__);
/* harmony import */ var _states_userGlobal__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../states/userGlobal */ "./resources/js/states/userGlobal.js");
function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _unsupportedIterableToArray(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

function _iterableToArrayLimit(arr, i) { if (typeof Symbol === "undefined" || !(Symbol.iterator in Object(arr))) return; var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }










var SequenceAddingModal = function SequenceAddingModal(_ref) {
  var toggleSequenceAddingModal = _ref.toggleSequenceAddingModal,
      addToggleFetch = _ref.addToggleFetch;

  var _UserGlobal = Object(_states_userGlobal__WEBPACK_IMPORTED_MODULE_7__["default"])(),
      _UserGlobal2 = _slicedToArray(_UserGlobal, 2),
      userState = _UserGlobal2[0],
      userActions = _UserGlobal2[1];

  Object(react__WEBPACK_IMPORTED_MODULE_0__["useEffect"])(function () {
    userActions.setState({
      currentProjStep: 1
    });
    userActions.setState({
      projectData: []
    });
  }, []);
  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_base__WEBPACK_IMPORTED_MODULE_6__["Modal"], {
    show: true,
    disableBackdropClick: true,
    topOff: true,
    maxWidth: "md",
    title: "Add Project",
    onClose: function onClose() {
      return toggleSequenceAddingModal();
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_AddingSteps__WEBPACK_IMPORTED_MODULE_1__["default"], null), userState.currentProjStep === 1 && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_AddProjectPart__WEBPACK_IMPORTED_MODULE_2__["default"], null), userState.currentProjStep === 2 && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_AddProjectResources__WEBPACK_IMPORTED_MODULE_3__["default"], null), userState.currentProjStep === 3 && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_AdminPropertyManager__WEBPACK_IMPORTED_MODULE_4__["default"], {
    toggleFetch: addToggleFetch
  }), userState.currentProjStep === 4 && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_AgencyAgreementPart__WEBPACK_IMPORTED_MODULE_5__["default"], null));
};

/* harmony default export */ __webpack_exports__["default"] = (SequenceAddingModal);

/***/ }),

/***/ "./resources/js/admin/components/SequenceEditingModal.js":
/*!***************************************************************!*\
  !*** ./resources/js/admin/components/SequenceEditingModal.js ***!
  \***************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _components_EditSteps__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../components/EditSteps */ "./resources/js/components/EditSteps.js");
/* harmony import */ var _EditProjectPart__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./EditProjectPart */ "./resources/js/admin/components/EditProjectPart.js");
/* harmony import */ var _EditProjectResources__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./EditProjectResources */ "./resources/js/admin/components/EditProjectResources.js");
/* harmony import */ var _AdminPropertyManager__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./AdminPropertyManager */ "./resources/js/admin/components/AdminPropertyManager.js");
/* harmony import */ var _AgencyAgreementPart__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./AgencyAgreementPart */ "./resources/js/admin/components/AgencyAgreementPart.js");
/* harmony import */ var _components_base__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../components/_base */ "./resources/js/components/_base/index.js");
/* harmony import */ var _components_base__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(_components_base__WEBPACK_IMPORTED_MODULE_6__);
/* harmony import */ var _states_userGlobal__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../states/userGlobal */ "./resources/js/states/userGlobal.js");
function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _unsupportedIterableToArray(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

function _iterableToArrayLimit(arr, i) { if (typeof Symbol === "undefined" || !(Symbol.iterator in Object(arr))) return; var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }










var SequenceEditingModal = function SequenceEditingModal(_ref) {
  var project = _ref.project,
      toggleSequenceEditModal = _ref.toggleSequenceEditModal,
      editToggleFetch = _ref.editToggleFetch;

  var _UserGlobal = Object(_states_userGlobal__WEBPACK_IMPORTED_MODULE_7__["default"])(),
      _UserGlobal2 = _slicedToArray(_UserGlobal, 2),
      userState = _UserGlobal2[0],
      userActions = _UserGlobal2[1];

  var _useState = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])(true),
      _useState2 = _slicedToArray(_useState, 2),
      projectView = _useState2[0],
      setProjectView = _useState2[1];

  var _useState3 = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])(false),
      _useState4 = _slicedToArray(_useState3, 2),
      resourcesView = _useState4[0],
      setResourcesView = _useState4[1];

  var _useState5 = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])(false),
      _useState6 = _slicedToArray(_useState5, 2),
      propertyView = _useState6[0],
      setPropertyView = _useState6[1];

  var _useState7 = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])(false),
      _useState8 = _slicedToArray(_useState7, 2),
      agreementView = _useState8[0],
      setAgreementView = _useState8[1];

  Object(react__WEBPACK_IMPORTED_MODULE_0__["useEffect"])(function () {
    userActions.setState({
      projectData: project
    });
    userActions.setState({
      currentProjStep: 1
    });
  }, []);

  var toggleProjV = function toggleProjV() {
    setProjectView(true);
    setResourcesView(false);
    setPropertyView(false);
    setAgreementView(false);
  };

  var toggleResV = function toggleResV() {
    setResourcesView(true);
    setPropertyView(false);
    setProjectView(false);
    setAgreementView(false);
  };

  var togglePropView = function togglePropView() {
    setPropertyView(true);
    setProjectView(false);
    setResourcesView(false);
    setAgreementView(false);
  };

  var toggleAgreeView = function toggleAgreeView() {
    setPropertyView(false);
    setProjectView(false);
    setResourcesView(false);
    setAgreementView(true);
  };

  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_base__WEBPACK_IMPORTED_MODULE_6__["Modal"], {
    show: true,
    disableBackdropClick: true,
    topOff: true,
    maxWidth: "md",
    title: "Edit Project",
    onClose: function onClose() {
      return toggleSequenceEditModal();
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_EditSteps__WEBPACK_IMPORTED_MODULE_1__["default"], {
    activeProjectView: projectView,
    activeResourcesView: resourcesView,
    activePropertyView: propertyView,
    activeAgreementView: agreementView,
    toggleProjectView: function toggleProjectView() {
      return toggleProjV();
    },
    toggleResourcesView: function toggleResourcesView() {
      return toggleResV();
    },
    togglePropertyView: function togglePropertyView() {
      return togglePropView();
    },
    toggleAgreementView: function toggleAgreementView() {
      return toggleAgreeView();
    }
  }), projectView && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_EditProjectPart__WEBPACK_IMPORTED_MODULE_2__["default"], {
    toggleFetch: editToggleFetch
  }), resourcesView && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_EditProjectResources__WEBPACK_IMPORTED_MODULE_3__["default"], {
    toggleFetch: editToggleFetch
  }), propertyView && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_AdminPropertyManager__WEBPACK_IMPORTED_MODULE_4__["default"], {
    toggleSEM: function toggleSEM() {
      return toggleSequenceEditModal();
    },
    toggleFetch: editToggleFetch,
    toggleAgreementView: function toggleAgreementView() {
      return toggleAgreeView();
    }
  }), agreementView && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_AgencyAgreementPart__WEBPACK_IMPORTED_MODULE_5__["default"], {
    toggleFetch: editToggleFetch
  }));
};

/* harmony default export */ __webpack_exports__["default"] = (SequenceEditingModal);

/***/ }),

/***/ "./resources/js/admin/components/UpdateDeveloperModal.js":
/*!***************************************************************!*\
  !*** ./resources/js/admin/components/UpdateDeveloperModal.js ***!
  \***************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime/regenerator */ "./node_modules/@babel/runtime/regenerator/index.js");
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _helpers_styles_dropdown_css__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../helpers/styles/dropdown.css */ "./resources/js/helpers/styles/dropdown.css");
/* harmony import */ var _helpers_styles_dropdown_css__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(_helpers_styles_dropdown_css__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _components_base__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../components/_base */ "./resources/js/components/_base/index.js");
/* harmony import */ var _components_base__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(_components_base__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var _helpers_styles_toast_css__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../helpers/styles/toast.css */ "./resources/js/helpers/styles/toast.css");
/* harmony import */ var _helpers_styles_toast_css__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(_helpers_styles_toast_css__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var _states_userGlobal__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../states/userGlobal */ "./resources/js/states/userGlobal.js");
/* harmony import */ var _helpers_bootstrap__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../helpers/bootstrap */ "./resources/js/helpers/bootstrap.js");
/* harmony import */ var _fortawesome_react_fontawesome__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @fortawesome/react-fontawesome */ "./node_modules/@fortawesome/react-fontawesome/index.es.js");
/* harmony import */ var _fortawesome_free_solid_svg_icons__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @fortawesome/free-solid-svg-icons */ "./node_modules/@fortawesome/free-solid-svg-icons/index.es.js");
/* harmony import */ var _material_ui_core_Collapse__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @material-ui/core/Collapse */ "./node_modules/@material-ui/core/esm/Collapse/index.js");
/* harmony import */ var _components_base_alerts_sweetAlert__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ../../components/_base/alerts/sweetAlert */ "./resources/js/components/_base/alerts/sweetAlert.js");
/* harmony import */ var react_select__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! react-select */ "./node_modules/react-select/dist/react-select.browser.esm.js");
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! lodash */ "./node_modules/lodash/lodash.js");
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_12___default = /*#__PURE__*/__webpack_require__.n(lodash__WEBPACK_IMPORTED_MODULE_12__);
/* harmony import */ var _helpers_userAccountSettingsHelper_userAccountHelper__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ../../helpers/userAccountSettingsHelper/userAccountHelper */ "./resources/js/helpers/userAccountSettingsHelper/userAccountHelper.js");
/* harmony import */ var _helpers_countries__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ../../helpers/countries */ "./resources/js/helpers/countries.js");
/* harmony import */ var _helpers_propertyHelper_propertyHelper__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! ../../helpers/propertyHelper/propertyHelper */ "./resources/js/helpers/propertyHelper/propertyHelper.js");
/* harmony import */ var _data_index__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! ../../data/index */ "./resources/js/data/index.js");
/* harmony import */ var _helpers_fileHelper_fileHelper__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! ../../helpers/fileHelper/fileHelper */ "./resources/js/helpers/fileHelper/fileHelper.js");


function _toConsumableArray(arr) { return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _unsupportedIterableToArray(arr) || _nonIterableSpread(); }

function _nonIterableSpread() { throw new TypeError("Invalid attempt to spread non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _iterableToArray(iter) { if (typeof Symbol !== "undefined" && Symbol.iterator in Object(iter)) return Array.from(iter); }

function _arrayWithoutHoles(arr) { if (Array.isArray(arr)) return _arrayLikeToArray(arr); }

function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _unsupportedIterableToArray(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

function _iterableToArrayLimit(arr, i) { if (typeof Symbol === "undefined" || !(Symbol.iterator in Object(arr))) return; var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }




















var UpdateDeveloperModal = function UpdateDeveloperModal(_ref) {
  var developer = _ref.developer,
      toogleUpdateDeveloper = _ref.toogleUpdateDeveloper,
      fetchDevelopers = _ref.fetchDevelopers;

  var _UserGlobal = Object(_states_userGlobal__WEBPACK_IMPORTED_MODULE_5__["default"])(),
      _UserGlobal2 = _slicedToArray(_UserGlobal, 2),
      userState = _UserGlobal2[0],
      userAction = _UserGlobal2[1];

  var _useState = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(false),
      _useState2 = _slicedToArray(_useState, 2),
      loading = _useState2[0],
      setLoading = _useState2[1];

  var _useState3 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])({}),
      _useState4 = _slicedToArray(_useState3, 2),
      errors = _useState4[0],
      setErrors = _useState4[1];

  var _useState5 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(!Object(lodash__WEBPACK_IMPORTED_MODULE_12__["isEmpty"])(developer) ? false : true),
      _useState6 = _slicedToArray(_useState5, 2),
      isOpen = _useState6[0],
      setIsOpen = _useState6[1];

  var _useState7 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])([]),
      _useState8 = _slicedToArray(_useState7, 2),
      propertyTypes = _useState8[0],
      setPropertyTypes = _useState8[1];

  var _useState9 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(false),
      _useState10 = _slicedToArray(_useState9, 2),
      showOtherBankText = _useState10[0],
      setShowOtherBankText = _useState10[1];

  var _useState11 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(""),
      _useState12 = _slicedToArray(_useState11, 2),
      bankAccountName = _useState12[0],
      setBankAccountName = _useState12[1];

  var _useState13 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(""),
      _useState14 = _slicedToArray(_useState13, 2),
      bankBSB = _useState14[0],
      setBankBSB = _useState14[1];

  var _useState15 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(""),
      _useState16 = _slicedToArray(_useState15, 2),
      bankAccountNumber = _useState16[0],
      setBankAccountNumber = _useState16[1];

  var _useState17 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(!Object(lodash__WEBPACK_IMPORTED_MODULE_12__["isEmpty"])(developer.developer) && developer.developer.id ? developer.developer.id : 0),
      _useState18 = _slicedToArray(_useState17, 2),
      devId = _useState18[0],
      setDevId = _useState18[1];

  var _useState19 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])([]),
      _useState20 = _slicedToArray(_useState19, 2),
      selectedState = _useState20[0],
      setSelectedState = _useState20[1];

  var _useState21 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])([]),
      _useState22 = _slicedToArray(_useState21, 2),
      selectedCountry = _useState22[0],
      setSelectedCountry = _useState22[1];

  var _useState23 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(!Object(lodash__WEBPACK_IMPORTED_MODULE_12__["isEmpty"])(developer) && developer.avatar_path ? developer.avatar_path : "/assets/images/profile_default.jpg"),
      _useState24 = _slicedToArray(_useState23, 2),
      profilePic = _useState24[0],
      setProfilePic = _useState24[1];

  var _useState25 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(!Object(lodash__WEBPACK_IMPORTED_MODULE_12__["isEmpty"])(developer.developer) && developer.developer.company_logo_path ? developer.developer.company_logo_path : "/assets/images/no_logo.png"),
      _useState26 = _slicedToArray(_useState25, 2),
      companyPic = _useState26[0],
      setCompanyPic = _useState26[1];

  var form = /*#__PURE__*/Object(react__WEBPACK_IMPORTED_MODULE_1__["createRef"])();
  var profileInput = Object(react__WEBPACK_IMPORTED_MODULE_1__["useRef"])(null);
  var companyInput = Object(react__WEBPACK_IMPORTED_MODULE_1__["useRef"])(null);

  var _useState27 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(""),
      _useState28 = _slicedToArray(_useState27, 2),
      selectedBank = _useState28[0],
      setSelectedBank = _useState28[1];

  var _useState29 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])([]),
      _useState30 = _slicedToArray(_useState29, 2),
      bankList = _useState30[0],
      setBankList = _useState30[1];

  Object(react__WEBPACK_IMPORTED_MODULE_1__["useEffect"])(function () {
    getBanks();

    if (!Object(lodash__WEBPACK_IMPORTED_MODULE_12__["isEmpty"])(developer) && developer.developer) {
      mapDeveloperFields(developer.developer);
    }

    if (developer && developer.avatar_path) {
      setProfilePic("".concat(developer.avatar_path));

      if (developer.developer.company_logo_path) {
        setCompanyPic(developer.developer.company_logo_path);
      }
    }
  }, [userState.user]);

  var getBanks = /*#__PURE__*/function () {
    var _ref2 = _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee() {
      var url;
      return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee$(_context) {
        while (1) {
          switch (_context.prev = _context.next) {
            case 0:
              url = "/api/bank";
              _context.next = 3;
              return Object(_data_index__WEBPACK_IMPORTED_MODULE_16__["fetchBanks"])({
                url: url
              }).then(function (res) {
                var newBankList = _toConsumableArray(res.data.map(function (bank) {
                  return {
                    label: "".concat(bank.name),
                    value: String(bank.id)
                  };
                }));

                newBankList.push({
                  label: "Other",
                  value: "Other"
                });

                if (!Object(lodash__WEBPACK_IMPORTED_MODULE_12__["isEmpty"])(developer.developer) && developer.developer.bank_id) {
                  var bank = newBankList.filter(function (bank) {
                    return bank.value == developer.developer.bank_id.toString();
                  });
                  setSelectedBank(bank[0]);
                }

                setBankList(newBankList);
              });

            case 3:
            case "end":
              return _context.stop();
          }
        }
      }, _callee);
    }));

    return function getBanks() {
      return _ref2.apply(this, arguments);
    };
  }();

  var handleClick = function handleClick(isProfile) {
    if (isProfile) {
      profileInput.current.click();
    } else {
      companyInput.current.click();
    }
  };

  var handleFileChange = function handleFileChange(e, isProfile) {
    var imageFile = e.target.files[0];

    if (!Object(_helpers_fileHelper_fileHelper__WEBPACK_IMPORTED_MODULE_17__["imageFileTypeFilter"])(imageFile.name)) {
      return Object(_components_base_alerts_sweetAlert__WEBPACK_IMPORTED_MODULE_10__["sweetAlert"])("error", "Please select valid image.");
    }

    if (isProfile) {
      setProfilePic(URL.createObjectURL(imageFile));
    } else {
      setCompanyPic(URL.createObjectURL(imageFile));
    }
  };

  var mapDeveloperFields = function mapDeveloperFields(data) {
    if (Object(lodash__WEBPACK_IMPORTED_MODULE_12__["isEmpty"])(data)) return;

    if (data.country) {
      setSelectedCountry({
        value: data.country,
        label: data.country
      });
    }

    if (data.state) {
      setSelectedState({
        value: data.state,
        label: data.state
      });
    }

    if (data.bank_account_name) {
      setBankAccountName(data.bank_account_name);
    }

    if (data.bank_bsb) {
      setBankBSB(data.bank_bsb);
    }

    if (data.bank_account_number) {
      setBankAccountNumber(data.bank_account_number);
    }

    Object.keys(_helpers_userAccountSettingsHelper_userAccountHelper__WEBPACK_IMPORTED_MODULE_13__["developerFields"]).forEach(function (key) {
      var k = key.replace("dev_", "");

      if (data.hasOwnProperty(k)) {
        if (k === "phone") {
          _helpers_userAccountSettingsHelper_userAccountHelper__WEBPACK_IMPORTED_MODULE_13__["developerFields"][key].value = data[k] ? data[k].replace("+61", "") : "";
        } else {
          _helpers_userAccountSettingsHelper_userAccountHelper__WEBPACK_IMPORTED_MODULE_13__["developerFields"][key].value = data[k];
        }
      }
    });
  };

  var handleSubmit = /*#__PURE__*/function () {
    var _ref3 = _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee2(e) {
      var errors, formData, phone, url, data;
      return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee2$(_context2) {
        while (1) {
          switch (_context2.prev = _context2.next) {
            case 0:
              e.preventDefault();

              if (!(profilePic.substring(0, 8) === '/assets/')) {
                _context2.next = 3;
                break;
              }

              return _context2.abrupt("return", Object(_components_base_alerts_sweetAlert__WEBPACK_IMPORTED_MODULE_10__["sweetAlert"])("warning", "Developer profile picture is required."));

            case 3:
              if (!(companyPic.substring(0, 8) === '/assets/')) {
                _context2.next = 5;
                break;
              }

              return _context2.abrupt("return", Object(_components_base_alerts_sweetAlert__WEBPACK_IMPORTED_MODULE_10__["sweetAlert"])("warning", "Company profile picture is required."));

            case 5:
              errors = {};
              formData = new FormData(form.current);
              phone = formData.get("phone") ? "+61" + formData.get("phone") : "";
              formData.set("phone", phone);
              formData.append("user_type", "project_developer"); // let email = formData.get("userEmail");
              // formData.append("email", email);

              formData.append("_method", "PATCH");
              formData.append("dev_state", selectedState.value || "");
              formData.append("dev_country", selectedCountry.value || "");
              formData.append("user_id", developer.id);
              formData.append("devId", devId);

              if (!Object(lodash__WEBPACK_IMPORTED_MODULE_12__["isEmpty"])(selectedBank) && selectedBank.value !== "Other") {
                formData.set("bank_id", selectedBank.value);
              }

              setLoading(true);
              url = "/api/user/".concat(developer.id);
              _context2.prev = 18;
              _context2.next = 21;
              return _helpers_bootstrap__WEBPACK_IMPORTED_MODULE_6__["axios"].post(url, formData);

            case 21:
              Object(_components_base_alerts_sweetAlert__WEBPACK_IMPORTED_MODULE_10__["sweetAlert"])("success", "Profile successfully updated.");
              userAction.setState({
                fetch: !userState.fetch
              });
              fetchDevelopers();
              setLoading(false);
              _context2.next = 34;
              break;

            case 27:
              _context2.prev = 27;
              _context2.t0 = _context2["catch"](18);
              data = _context2.t0.response.data;

              if (data.errors.length == 1 && data.errors[0].phone) {
                errors = [];
              }

              errors = data.errors;
              setLoading(false);

              if (errors && errors.avatar_path) {
                Object(_components_base_alerts_sweetAlert__WEBPACK_IMPORTED_MODULE_10__["sweetAlert"])("error", errors.avatar_path[0]);
                developer.avatar_path ? setProfilePic("".concat(developer.avatar_path)) : setProfilePic("/assets/images/profile_default.jpg");
              }

            case 34:
              setErrors(errors || {});

            case 35:
            case "end":
              return _context2.stop();
          }
        }
      }, _callee2, null, [[18, 27]]);
    }));

    return function handleSubmit(_x) {
      return _ref3.apply(this, arguments);
    };
  }();

  Object.keys(_helpers_userAccountSettingsHelper_userAccountHelper__WEBPACK_IMPORTED_MODULE_13__["fields"]).forEach(function (key) {
    if (key === "phone") {
      _helpers_userAccountSettingsHelper_userAccountHelper__WEBPACK_IMPORTED_MODULE_13__["fields"][key].value = developer["phone"] ? developer["phone"].replace("+61", "") : "";
    } else if (key === "userEmail") {
      _helpers_userAccountSettingsHelper_userAccountHelper__WEBPACK_IMPORTED_MODULE_13__["fields"][key].value = developer["email"] ? developer["email"] : "";
    } else {
      _helpers_userAccountSettingsHelper_userAccountHelper__WEBPACK_IMPORTED_MODULE_13__["fields"][key].value = developer[key] ? developer[key] : "";
    }

    _helpers_userAccountSettingsHelper_userAccountHelper__WEBPACK_IMPORTED_MODULE_13__["fields"][key].id = developer[key] ? developer[key] : null;
  });

  var handleChange = function handleChange(key, e) {
    if (key === "dev_country") {
      setSelectedCountry(e);
    } else if (key === "dev_state") {
      setSelectedState(e);
    } else {
      if (e.value === "Other") {
        setShowOtherBankText(true);
      } else {
        setShowOtherBankText(false);
      }

      setSelectedBank(e);
    }
  };

  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components_base__WEBPACK_IMPORTED_MODULE_3__["Modal"], {
    show: true,
    title: "Edit Developer",
    disableBackdropClick: true,
    maxWidth: "md",
    topOff: true,
    scroll: "body",
    onClose: function onClose() {
      return toogleUpdateDeveloper();
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "bg-white rounded-lg py-5 px-8 m-5"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("section", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("section", {
    className: "relative bg-white"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "pb-16 mx-auto",
    style: {
      maxWidth: 1366
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("h1", {
    className: "font-bold leading-tight text-4xl pt-10 pb-8 lg:pb-16 text-center"
  }, "Member Profile"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("form", {
    ref: form,
    onSubmit: handleSubmit
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "flex flex-col-reverse lg:flex-row-reverse"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "lg:mt-0 lg:w-8/12 ml-16 mt-8"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components_base__WEBPACK_IMPORTED_MODULE_3__["Form"], {
    errors: errors,
    formFields: _helpers_userAccountSettingsHelper_userAccountHelper__WEBPACK_IMPORTED_MODULE_13__["fields"]
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "mb-5 text-base flex"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("label", {
    className: "mr-3 w-32 mt-2 capitalize pr-3"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
    className: "font-bold"
  }, "Password")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "w-full"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    onClick: function onClick() {
      return setIsOpen(!isOpen);
    },
    className: "\n                          border border-palette-gray text-palette-gray hover:bg-palette-gray hover:text-white transition-all duration-300\n                          cursor-pointer font-bold inline-block px-6 text-sm py-2 rounded shadow\n                        "
  }, "Change Password"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_material_ui_core_Collapse__WEBPACK_IMPORTED_MODULE_9__["default"], {
    "in": isOpen
  }, isOpen && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "mt-5 text-sm"
  }, "New Password"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components_base__WEBPACK_IMPORTED_MODULE_3__["TextInput"], {
    type: "password",
    placeholder: "New Password",
    name: "password",
    border: false,
    appearance: true
  }), errors.password && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
    className: "text-red-500 text-xs"
  }, errors.password[0]), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "mt-5 text-sm"
  }, "Confirm Password"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components_base__WEBPACK_IMPORTED_MODULE_3__["TextInput"], {
    type: "password",
    placeholder: "Confirm Password",
    name: "password_confirmation",
    border: false,
    appearance: true
  }))))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(react__WEBPACK_IMPORTED_MODULE_1___default.a.Fragment, null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "mx-auto -ml-20",
    style: {
      maxWidth: 1366
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("h1", {
    className: "font-bold leading-tight ml-3 lg:pb-16 pb-8 pt-10 text-4xl"
  }, "Seller Profile")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components_base__WEBPACK_IMPORTED_MODULE_3__["Form"], {
    errors: errors,
    formFields: _helpers_userAccountSettingsHelper_userAccountHelper__WEBPACK_IMPORTED_MODULE_13__["developerFields"]
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "mb-5 text-base flex"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("label", {
    className: "mr-3 w-32 mt-2 capitalize pr-3"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
    className: "font-bold"
  }, "Bank")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "w-full"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "flex"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(react_select__WEBPACK_IMPORTED_MODULE_11__["default"], {
    isOptionSelected: true,
    name: "bank",
    classNamePrefix: "input-select",
    onChange: function onChange(e) {
      return handleChange("bank", e);
    },
    className: "w-full",
    placeholder: "Select Bank",
    value: selectedBank,
    options: bankList
  })))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_material_ui_core_Collapse__WEBPACK_IMPORTED_MODULE_9__["default"], {
    "in": showOtherBankText
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "mb-5 text-base flex"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("label", {
    className: "mr-3 w-32 mt-2 capitalize pr-3"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
    className: "font-bold"
  })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "w-full"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "flex"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components_base__WEBPACK_IMPORTED_MODULE_3__["TextInput"], {
    type: "text",
    placeholder: "Enter Bank Name",
    name: "new_bank_name",
    border: false,
    appearance: true
  }))))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "mb-5 text-base flex"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("label", {
    className: "mr-3 w-32 mt-2 capitalize pr-3"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
    className: "font-bold"
  }, "Acct. Name")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "w-full"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "flex"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components_base__WEBPACK_IMPORTED_MODULE_3__["TextInput"], {
    type: "text",
    placeholder: "Account Name",
    name: "bank_account_name",
    border: false,
    defaultValue: bankAccountName,
    appearance: true
  }), errors.bank_account_name && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
    className: "text-red-500 text-xs"
  }, errors.bank_account_name[0])))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "mb-5 text-base flex"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("label", {
    className: "mr-3 w-32 mt-2 capitalize pr-3"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
    className: "font-bold"
  }, "BSB")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "w-full"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "flex"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components_base__WEBPACK_IMPORTED_MODULE_3__["TextInput"], {
    type: "text",
    placeholder: "Enter BSB",
    name: "bank_bsb",
    defaultValue: bankBSB,
    border: false,
    appearance: true
  }), errors.bank_bsb && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
    className: "text-red-500 text-xs"
  }, errors.bank_bsb[0])))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "mb-5 text-base flex"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("label", {
    className: "mr-3 w-32 mt-2 capitalize pr-3"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
    className: "font-bold"
  }, "Acct. No.")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "w-full"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "flex"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components_base__WEBPACK_IMPORTED_MODULE_3__["TextInput"], {
    type: "text",
    placeholder: "Enter Account Number",
    name: "bank_account_number",
    defaultValue: bankAccountNumber,
    border: false,
    appearance: true
  }), errors.bank_account_number && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
    className: "text-red-500 text-xs"
  }, errors.bank_account_number[0])))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "mb-5 text-base flex"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("label", {
    className: "mr-3 w-32 mt-2 capitalize pr-3"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
    className: "font-bold"
  }, "State")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "w-full"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "flex"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(react_select__WEBPACK_IMPORTED_MODULE_11__["default"], {
    isOptionSelected: true,
    name: "dev_state",
    classNamePrefix: "input-select",
    onChange: function onChange(e) {
      return handleChange("dev_state", e);
    },
    className: "w-full",
    placeholder: "Select State",
    value: selectedState,
    options: _helpers_propertyHelper_propertyHelper__WEBPACK_IMPORTED_MODULE_15__["states"]
  })), errors["value"] && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
    className: "text-red-500 text-sm"
  }, errors[value]))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "mb-5 text-base flex"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("label", {
    className: "mr-3 w-32 mt-2 capitalize pr-3"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
    className: "font-bold"
  }, "Country")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "w-full"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "flex"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(react_select__WEBPACK_IMPORTED_MODULE_11__["default"], {
    isOptionSelected: true,
    name: "dev_country",
    classNamePrefix: "input-select",
    onChange: function onChange(e) {
      return handleChange("dev_country", e);
    },
    className: "w-full",
    placeholder: "Select Country",
    value: selectedCountry,
    options: _helpers_countries__WEBPACK_IMPORTED_MODULE_14__["countries"]
  })), errors["value"] && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
    className: "text-red-500 text-sm"
  }, errors[value]))))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "flex flex-col w-1/2"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "flex justify-center" //className={`lg:w-4/12`}

  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("input", {
    type: "file",
    name: "avatar_path",
    className: "hidden",
    ref: profileInput,
    onChange: function onChange(e) {
      return handleFileChange(e, true);
    }
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "relative flex justify-center"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "mx-auto flex items-center p-4 overflow-hidden border-4 border-palette-gray h-48 lg:h-56 rounded-full shadow-lg w-48 lg:w-56 ",
    style: {
      width: "fit-content"
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("img", {
    className: "",
    src: profilePic
  })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
    className: "\n                            absolute bg-white border border-gray-300 bottom-0 cursor-pointer flex h-10 items-center justify-center mb-3 mr-3 right-0\n                            rounded-full shadow-md w-10 transition-all duration-300 hover:bg-palette-purple hover:text-white hover:border-palette-purple\n                          ",
    onClick: function onClick() {
      return handleClick(true);
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("svg", {
    className: "feather-icon"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("use", {
    xlinkHref: "/assets/svg/feather-sprite.svg#edit-2"
  }))))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "flex justify-center mt-48" //className={`lg:w-4/12`}

  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("input", {
    type: "file",
    name: "company_logo_path",
    className: "hidden",
    ref: companyInput,
    onChange: function onChange(e) {
      return handleFileChange(e, false);
    }
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "relative flex justify-center"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "relative mx-auto flex items-center p-4 overflow-hidden border-4 border-palette-gray h-48 lg:h-56 shadow-lg w-48 lg:w-56 ",
    style: {
      width: "fit-content"
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("img", {
    className: "",
    src: companyPic
  })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
    className: "\n                              absolute bg-white border border-gray-300 bottom-0 cursor-pointer flex h-10 items-center justify-center mb-3 mr-3 right-0\n                              rounded-full shadow-md w-10 transition-all duration-300 hover:bg-palette-purple hover:text-white hover:border-palette-purple\n                            ",
    onClick: function onClick() {
      return handleClick(false);
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("svg", {
    className: "feather-icon"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("use", {
    xlinkHref: "/assets/svg/feather-sprite.svg#edit-2"
  }))))))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "flex justify-center lg:justify-end mt-8"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components_base__WEBPACK_IMPORTED_MODULE_3__["Button"], {
    onClick: function onClick() {
      return toogleUpdateDeveloper();
    },
    className: "bg-red-700 button font-bold mr-2 rounded text-gray-100",
    disabled: loading
  }, "Cancel"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components_base__WEBPACK_IMPORTED_MODULE_3__["Button"], {
    className: "font-bold rounded",
    disabled: loading
  }, loading && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_fortawesome_react_fontawesome__WEBPACK_IMPORTED_MODULE_7__["FontAwesomeIcon"], {
    icon: _fortawesome_free_solid_svg_icons__WEBPACK_IMPORTED_MODULE_8__["faSpinner"],
    className: "fa-spin mr-2"
  }), !Object(lodash__WEBPACK_IMPORTED_MODULE_12__["isEmpty"])(developer) ? "Update" : "Save")))))))));
};

/* harmony default export */ __webpack_exports__["default"] = (UpdateDeveloperModal);

/***/ }),

/***/ "./resources/js/admin/pages/DeveloperList.js":
/*!***************************************************!*\
  !*** ./resources/js/admin/pages/DeveloperList.js ***!
  \***************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime/regenerator */ "./node_modules/@babel/runtime/regenerator/index.js");
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! lodash */ "./node_modules/lodash/lodash.js");
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(lodash__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _states_userGlobal__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../states/userGlobal */ "./resources/js/states/userGlobal.js");
/* harmony import */ var react_device_detect__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! react-device-detect */ "./node_modules/react-device-detect/main.js");
/* harmony import */ var react_device_detect__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(react_device_detect__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var _components__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../components */ "./resources/js/components/index.js");
/* harmony import */ var _components__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(_components__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var _components_AddDeveloperModal__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../components/AddDeveloperModal */ "./resources/js/admin/components/AddDeveloperModal.js");
/* harmony import */ var _components_UpdateDeveloperModal__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../components/UpdateDeveloperModal */ "./resources/js/admin/components/UpdateDeveloperModal.js");
/* harmony import */ var _components_base__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../../components/_base */ "./resources/js/components/_base/index.js");
/* harmony import */ var _components_base__WEBPACK_IMPORTED_MODULE_8___default = /*#__PURE__*/__webpack_require__.n(_components_base__WEBPACK_IMPORTED_MODULE_8__);


function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _unsupportedIterableToArray(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

function _iterableToArrayLimit(arr, i) { if (typeof Symbol === "undefined" || !(Symbol.iterator in Object(arr))) return; var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }











var DeveloperList = function DeveloperList() {
  var _UserGlobal = Object(_states_userGlobal__WEBPACK_IMPORTED_MODULE_3__["default"])(),
      _UserGlobal2 = _slicedToArray(_UserGlobal, 2),
      userState = _UserGlobal2[0],
      userAction = _UserGlobal2[1];

  var _useState = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])({
    sortBy: {
      value: "developers.created_at",
      order: "desc"
    }
  }),
      _useState2 = _slicedToArray(_useState, 2),
      filters = _useState2[0],
      setFilters = _useState2[1];

  var _useState3 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(false),
      _useState4 = _slicedToArray(_useState3, 2),
      toggleFetch = _useState4[0],
      setToggleFetch = _useState4[1];

  var _useState5 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(""),
      _useState6 = _slicedToArray(_useState5, 2),
      keyword = _useState6[0],
      setKeyword = _useState6[1];

  var _useState7 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])([]),
      _useState8 = _slicedToArray(_useState7, 2),
      developers = _useState8[0],
      setDevelopers = _useState8[1];

  var _useState9 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])({}),
      _useState10 = _slicedToArray(_useState9, 2),
      developer = _useState10[0],
      setSelectedDeveloper = _useState10[1];

  var _useState11 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(true),
      _useState12 = _slicedToArray(_useState11, 2),
      loading = _useState12[0],
      setLoading = _useState12[1];

  var _useState13 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(false),
      _useState14 = _slicedToArray(_useState13, 2),
      toggleOrder = _useState14[0],
      setToggleOrder = _useState14[1];

  var _useState15 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(false),
      _useState16 = _slicedToArray(_useState15, 2),
      toogleAddDeveloper = _useState16[0],
      setToogleAddDeveloper = _useState16[1];

  var _useState17 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(false),
      _useState18 = _slicedToArray(_useState17, 2),
      toogleUpdateDeveloper = _useState18[0],
      setToogleUpdateDeveloper = _useState18[1];

  var _useState19 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])("Add Developer"),
      _useState20 = _slicedToArray(_useState19, 2),
      title = _useState20[0],
      setTitle = _useState20[1];

  var _useState21 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(false),
      _useState22 = _slicedToArray(_useState21, 2),
      isAdd = _useState22[0],
      setIsAdd = _useState22[1];

  var tableHeaders = [{
    label: "Company Name",
    column: "developers.name",
    width: 230
  }, {
    label: "Contact",
    // column: "developers.name",
    width: 230
  }, {
    label: "Phone",
    //column: "developers.phone",
    width: 150
  }, {
    label: "Email",
    column: "developers.email",
    width: 150
  }, {
    label: "Edit",
    width: 50
  }];

  var handleSaveUpdateDeveloper = /*#__PURE__*/function () {
    var _ref = _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee(developer) {
      return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee$(_context) {
        while (1) {
          switch (_context.prev = _context.next) {
            case 0:
              setLoading(true);

            case 1:
            case "end":
              return _context.stop();
          }
        }
      }, _callee);
    }));

    return function handleSaveUpdateDeveloper(_x) {
      return _ref.apply(this, arguments);
    };
  }();

  var _onClose = function onClose() {
    userAction.setState({
      showSaveUpdateModal: false
    });
  };

  var _fetchDevelopers = function fetchDevelopers() {
    setToggleFetch(!toggleFetch);
  };

  var renderDeveloperList = function renderDeveloperList() {
    return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(react__WEBPACK_IMPORTED_MODULE_1___default.a.Fragment, null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("tbody", null, developers && developers.map(function (developer, index) {
      return !react_device_detect__WEBPACK_IMPORTED_MODULE_4__["isMobile"] ? /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("tr", {
        key: index,
        className: "hover:bg-gray-100 border-b text-base"
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("td", {
        className: "p-4"
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
        className: "flex items-center"
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("img", {
        src: !Object(lodash__WEBPACK_IMPORTED_MODULE_2__["isEmpty"])(developer.developer) && developer.developer.company_logo_path ? developer.developer.company_logo_path : "/assets/images/profile_default.jpg",
        className: "w-12 h-12 mr-3"
      }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
        className: "font-bold text-palette-purple"
      }, "".concat(!Object(lodash__WEBPACK_IMPORTED_MODULE_2__["isEmpty"])(developer.developer) && developer.developer.name ? developer.developer.name : ""))))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("td", {
        className: "p-4"
      }, "".concat(developer.first_name, " ").concat(developer.last_name)), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("td", {
        className: "p-4"
      }, "".concat(developer.phone ? developer.phone : developer.developer && developer.developer.phone ? developer.developer.phone : "")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("td", {
        className: "p-4 text-palette-blue-light"
      }, developer.email ? developer.email : ""), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("td", {
        className: "p-4"
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components_base__WEBPACK_IMPORTED_MODULE_8__["Tooltip"], {
        title: "Edit Developer"
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("button", {
        onClick: function onClick() {
          setTitle("Edit Developer");
          setIsAdd(false);
          toogleUpdateDeveloperModal();
          setSelectedDeveloper(developer);
        },
        className: "cursor-pointer bg-white border border-gray-400 duration-300 hover:bg-palette-purple hover:border-palette-purple hover:text-white inline-flex px-3 py-1 rounded transition-all"
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("svg", {
        className: "feather-icon h-5 w-5 opacity-50"
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("use", {
        xlinkHref: "/assets/svg/feather-sprite.svg#edit"
      })))))) : /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("tr", {
        key: index,
        className: "flex flex-col"
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("td", {
        className: "p-4 rounded",
        style: {
          boxShadow: "0 5px 28px 4px rgba(0,0,0,0.1)"
        }
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
        className: "flex items-center justify-between"
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
        className: "flex items-center"
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("img", {
        src: "/storage/projects/".concat(developer.developer.property_images[0]),
        className: "rounded-full w-12 h-12 mr-3"
      }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
        className: "font-bold text-base text-palette-purple"
      }, developer.developer.developer_name), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
        className: "opacity-50 text-sm"
      }, "Apartment " + developer.property_unit_name))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
        onClick: function onClick() {
          return handleSaveUpdateDeveloper(developer);
        },
        className: "cursor-pointer bg-white border border-gray-400 duration-300 hover:bg-palette-purple hover:border-palette-purple hover:text-white inline-flex px-3 py-1 rounded transition-all"
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("svg", {
        className: "feather-icon h-5 w-5 opacity-50"
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("use", {
        xlinkHref: "/assets/svg/feather-sprite.svg#edit"
      }))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("a", {
        href: "/weekly-deals/".concat(developer.developer_id, "/apartment/").concat(developer.property_id)
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
        className: "cursor-pointer bg-white border border-gray-400 duration-300 hover:bg-palette-purple hover:border-palette-purple hover:text-white inline-flex px-3 py-1 rounded transition-all"
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("svg", {
        className: "feather-icon h-5 w-5 opacity-50"
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("use", {
        xlinkHref: "/assets/svg/feather-sprite.svg#eye"
      }))))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("hr", {
        className: "block border border-gray-200 my-2"
      }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", null), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
        className: "flex items-center justify-between mt-2"
      })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("td", {
        className: "p-3"
      }));
    })));
  };

  var formatFilter = function formatFilter(column) {
    if (column) {
      setToggleOrder(!toggleOrder);
      setFilters({
        sortBy: {
          value: column,
          order: toggleOrder ? "desc" : "asc"
        }
      });
      setToggleFetch(!toggleFetch);
    }
  };

  var renderHeaders = function renderHeaders() {
    return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("thead", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("tr", null, tableHeaders.map(function (th, index) {
      return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("th", {
        onClick: function onClick() {
          return formatFilter(th.column);
        },
        style: {
          width: th.width
        },
        className: "border-b font-bold \n                px-4 py-2 text-palette-gray \n                px-4 py-2 cursor-pointer \n                hover:text-palette-gray-dark ",
        key: index
      }, th.label);
    })));
  };

  var handleSearch = Object(lodash__WEBPACK_IMPORTED_MODULE_2__["debounce"])(function (value) {
    setKeyword(value);
  }, 800);

  var toogleAddDeveloperModal = function toogleAddDeveloperModal() {
    setToogleAddDeveloper(!toogleAddDeveloper);
  };

  var toogleUpdateDeveloperModal = function toogleUpdateDeveloperModal() {
    setToogleUpdateDeveloper(!toogleUpdateDeveloper);
  };

  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "border mt-10"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "bg-white"
  }, toogleAddDeveloper && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components_AddDeveloperModal__WEBPACK_IMPORTED_MODULE_6__["default"], {
    title: title,
    developer: isAdd ? {} : developer,
    toogleAddDeveloper: function toogleAddDeveloper() {
      return toogleAddDeveloperModal();
    },
    fetchDevelopers: function fetchDevelopers() {
      return _fetchDevelopers();
    }
  }), toogleUpdateDeveloper && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components_UpdateDeveloperModal__WEBPACK_IMPORTED_MODULE_7__["default"], {
    title: title,
    developer: isAdd ? {} : developer,
    toogleUpdateDeveloper: function toogleUpdateDeveloper() {
      return toogleUpdateDeveloperModal();
    },
    fetchDevelopers: function fetchDevelopers() {
      return _fetchDevelopers();
    }
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("section", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("section", {
    className: "relative bg-white"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "pb-16 px-6 mx-auto"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("h1", {
    className: "font-bold leading-tight text-4xl pt-10 pb-8 lg:pb-16 text-center"
  }, "Developers"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "flex justify-between"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "flex justify-center lg:justify-end mb-8"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "w-64 relative"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("svg", {
    className: "absolute feather-icon h-8 left-0 mx-3 text-gray-500"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("use", {
    xlinkHref: "/assets/svg/feather-sprite.svg#search"
  })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components_base__WEBPACK_IMPORTED_MODULE_8__["TextInput"], {
    className: "pl-8",
    type: "text",
    placeholder: "Search",
    border: false,
    appearance: true,
    onChange: function onChange(e) {
      return handleSearch(e.target.value);
    }
  }))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: " flex justify-center lg:justify-end mb-8"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components_base__WEBPACK_IMPORTED_MODULE_8__["Tooltip"], {
    title: "Click to add Developer"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "w-64 relative "
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("svg", {
    className: "absolute feather-icon pt-2 h-full left-0 ml-5 text-white pb-2"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("use", {
    xlinkHref: "/assets/svg/feather-sprite.svg#plus-circle"
  })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components_base__WEBPACK_IMPORTED_MODULE_8__["Button"], {
    onClick: function onClick() {
      setTitle("Add Developer");
      setIsAdd(true);
      toogleAddDeveloperModal();
    }
  }, "Add Developer"))))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components_base__WEBPACK_IMPORTED_MODULE_8__["Table"], {
    query: "/api/developer",
    toggleFetch: toggleFetch,
    keyword: keyword,
    getData: setDevelopers,
    content: renderDeveloperList(),
    header: react_device_detect__WEBPACK_IMPORTED_MODULE_4__["isMobile"] ? null : renderHeaders()
  })))), !loading && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components__WEBPACK_IMPORTED_MODULE_5__["SaveUpdateModal"], {
    show: userState.showSaveUpdateModal,
    onClose: function onClose() {
      return _onClose();
    },
    developer: selecteddeveloper,
    apartment: selecteddeveloperProperty,
    purchasersParam: purchasers
  })));
};

/* harmony default export */ __webpack_exports__["default"] = (DeveloperList);

/***/ }),

/***/ "./resources/js/admin/pages/ManageProjectProperties.js":
/*!*************************************************************!*\
  !*** ./resources/js/admin/pages/ManageProjectProperties.js ***!
  \*************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _components_base__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../components/_base */ "./resources/js/components/_base/index.js");
/* harmony import */ var _components_base__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_components_base__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _components_EditPropertyModal__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../components/EditPropertyModal */ "./resources/js/admin/components/EditPropertyModal.js");
/* harmony import */ var _components_AddProperty__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../components/AddProperty */ "./resources/js/admin/components/AddProperty.js");
/* harmony import */ var _components_EditLandPropertyModal__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../components/EditLandPropertyModal */ "./resources/js/admin/components/EditLandPropertyModal.js");
/* harmony import */ var _components_AddLandProperty__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../components/AddLandProperty */ "./resources/js/admin/components/AddLandProperty.js");
/* harmony import */ var react_device_detect__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! react-device-detect */ "./node_modules/react-device-detect/main.js");
/* harmony import */ var react_device_detect__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(react_device_detect__WEBPACK_IMPORTED_MODULE_6__);
/* harmony import */ var react_currency_format__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! react-currency-format */ "./node_modules/react-currency-format/lib/currency-format.js");
/* harmony import */ var react_currency_format__WEBPACK_IMPORTED_MODULE_7___default = /*#__PURE__*/__webpack_require__.n(react_currency_format__WEBPACK_IMPORTED_MODULE_7__);
/* harmony import */ var _material_ui_core_Checkbox__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @material-ui/core/Checkbox */ "./node_modules/@material-ui/core/esm/Checkbox/index.js");
/* harmony import */ var _material_ui_core_FormControlLabel__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @material-ui/core/FormControlLabel */ "./node_modules/@material-ui/core/esm/FormControlLabel/index.js");
/* harmony import */ var _material_ui_core_styles__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @material-ui/core/styles */ "./node_modules/@material-ui/core/esm/styles/index.js");
/* harmony import */ var _material_ui_core_colors__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! @material-ui/core/colors */ "./node_modules/@material-ui/core/esm/colors/index.js");
/* harmony import */ var _components_base_alerts_sweetAlert__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ../../components/_base/alerts/sweetAlert */ "./resources/js/components/_base/alerts/sweetAlert.js");
/* harmony import */ var _helpers_addressHelper__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ../../helpers/addressHelper */ "./resources/js/helpers/addressHelper.js");
function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _unsupportedIterableToArray(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

function _iterableToArrayLimit(arr, i) { if (typeof Symbol === "undefined" || !(Symbol.iterator in Object(arr))) return; var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }

function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }















var GreenCheckbox = Object(_material_ui_core_styles__WEBPACK_IMPORTED_MODULE_10__["withStyles"])({
  root: {
    color: _material_ui_core_colors__WEBPACK_IMPORTED_MODULE_11__["green"][400],
    "&$checked": {
      color: _material_ui_core_colors__WEBPACK_IMPORTED_MODULE_11__["green"][600]
    }
  },
  checked: {}
})(function (props) {
  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_material_ui_core_Checkbox__WEBPACK_IMPORTED_MODULE_8__["default"], _extends({
    color: "default"
  }, props));
});
window.axios = __webpack_require__(/*! axios */ "./node_modules/axios/index.js");

var ManageProjectProperties = function ManageProjectProperties(_ref) {
  var _ref2;

  var dealId = _ref.dealId,
      dealSubPropertyId = _ref.dealSubPropertyId;
  var isProjectLand = dealSubPropertyId == 4 ? true : false;

  var _useState = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])(""),
      _useState2 = _slicedToArray(_useState, 2),
      keyWord = _useState2[0],
      setKeyWord = _useState2[1];

  var _useState3 = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])(false),
      _useState4 = _slicedToArray(_useState3, 2),
      toggleFetch = _useState4[0],
      setToggleFetch = _useState4[1];

  var _useState5 = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])([]),
      _useState6 = _slicedToArray(_useState5, 2),
      currentProject = _useState6[0],
      setCurrentProject = _useState6[1];

  var _useState7 = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])({
    sortBy: {
      value: "deals.name",
      order: "desc"
    }
  }),
      _useState8 = _slicedToArray(_useState7, 2),
      filters = _useState8[0],
      setFilters = _useState8[1];

  var _useState9 = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])(false),
      _useState10 = _slicedToArray(_useState9, 2),
      toggleOrder = _useState10[0],
      setToggleOrder = _useState10[1];

  var _useState11 = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])(false),
      _useState12 = _slicedToArray(_useState11, 2),
      toggleEditProperty = _useState12[0],
      setToggleEditProperty = _useState12[1];

  var _useState13 = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])(false),
      _useState14 = _slicedToArray(_useState13, 2),
      toggleAddProperty = _useState14[0],
      setToggleAddProperty = _useState14[1];

  var _useState15 = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])([]),
      _useState16 = _slicedToArray(_useState15, 2),
      property = _useState16[0],
      setProperty = _useState16[1];

  var _useState17 = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])(false),
      _useState18 = _slicedToArray(_useState17, 2),
      toogleLandProperty = _useState18[0],
      setToogleLandProperty = _useState18[1];

  var _useState19 = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])(false),
      _useState20 = _slicedToArray(_useState19, 2),
      toogleEditLandProperty = _useState20[0],
      setToogleEditLandProperty = _useState20[1];

  var formatFilter = function formatFilter(column) {
    if (column) {
      setToggleOrder(!toggleOrder);
      setFilters({
        sortBy: {
          value: column,
          order: toggleOrder ? "desc" : "asc"
        }
      });
      setToggleFetch(!toggleFetch);
    }
  };

  Object(react__WEBPACK_IMPORTED_MODULE_0__["useEffect"])(function () {
    if (!toggleAddProperty) {
      setToggleFetch(!toggleFetch);
    }

    return function () {};
  }, [toggleAddProperty]);
  Object(react__WEBPACK_IMPORTED_MODULE_0__["useEffect"])(function () {
    if (!toggleEditProperty) {
      setToggleFetch(!toggleFetch);
    }

    return function () {};
  }, [toggleEditProperty]);
  var tableHeaders = [{
    label: "Id",
    column: "properties.id",
    width: 40
  }, {
    label: "Property Number",
    column: "properties.unit_name",
    width: 150
  }, {
    label: "Property Address",
    column: "property.address",
    width: 150
  }, {
    label: "Developer Price",
    column: "properties.price",
    width: 150
  }, {
    label: "Discount",
    column: "properties.price",
    width: 80
  }, {
    label: "GroupBuyer Price",
    column: "properties.price",
    width: 150
  }, (_ref2 = {
    label: "Approved",
    column: ""
  }, _defineProperty(_ref2, "column", "properties.is_property_approved"), _defineProperty(_ref2, "width", 150), _ref2), {
    label: "Edit",
    column: "",
    width: 40
  }];

  var renderHeaders = function renderHeaders() {
    return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("thead", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("tr", null, tableHeaders.map(function (th, index) {
      return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("th", {
        onClick: function onClick() {
          return formatFilter(th.column);
        },
        style: {
          width: th.width
        },
        className: "border-b font-bold px-4 py-2 text-palette-gray px-4 py-2",
        key: index
      }, th.label);
    })));
  };

  var _toggleEditPropertyModal = function toggleEditPropertyModal() {
    if (!isProjectLand) {
      setToggleEditProperty(!toggleEditProperty);
    } else {
      setToogleEditLandProperty(!toogleEditLandProperty);
    }
  };

  var _toggleAddPropertyModal = function toggleAddPropertyModal() {
    if (!isProjectLand) {
      setToggleAddProperty(!toggleAddProperty);
    } else {
      setToogleLandProperty(!toogleLandProperty);
    }
  };

  var handleApproval = function handleApproval(event, propId, name) {
    var status = event.target.checked;
    axios({
      method: "post",
      url: "/api/approve-property",
      data: {
        value: event.target.checked,
        propertyId: propId
      }
    }).then(function (res) {
      if (res.status === 200) {
        var msg = status ? "".concat(name, " is now added to approved properties!") : "".concat(name, " is now removed on the approved properties!");
        Object(_components_base_alerts_sweetAlert__WEBPACK_IMPORTED_MODULE_12__["sweetAlert"])("success", msg);
        setToggleFetch(!toggleFetch);
      }
    });
  };

  var propertiesContent = function propertiesContent() {
    return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react__WEBPACK_IMPORTED_MODULE_0___default.a.Fragment, null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("tbody", null, currentProject.properties && currentProject.properties.map(function (property) {
      return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("tr", {
        key: property.id,
        className: "hover:bg-gray-100 border-b text-base"
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("td", {
        className: "p-4"
      }, property.id), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("td", {
        className: "p-4 font-bold text-palette-purple"
      }, property.unit_name), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("td", {
        className: "p-4"
      }, Object(_helpers_addressHelper__WEBPACK_IMPORTED_MODULE_13__["addressHelper"])(property.address)), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("td", {
        className: "p-4"
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_currency_format__WEBPACK_IMPORTED_MODULE_7___default.a, {
        value: property.price,
        displayType: "text",
        thousandSeparator: true,
        prefix: "$"
      })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("td", {
        className: "p-4"
      }, "".concat(currentProject.discount ? currentProject.discount + "%" : "N/A", " ")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("td", {
        className: "p-4 text-left font-semibold text-red-700"
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_base__WEBPACK_IMPORTED_MODULE_1__["Tooltip"], {
        title: "Price discount: ".concat(currentProject.discount, "%")
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_currency_format__WEBPACK_IMPORTED_MODULE_7___default.a, {
        value: Math.ceil(property.price - property.price * (currentProject.discount / 100)),
        displayType: "text",
        thousandSeparator: true,
        prefix: "$"
      }))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("td", {
        className: "p-4 pl-10"
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_material_ui_core_FormControlLabel__WEBPACK_IMPORTED_MODULE_9__["default"], {
        control: /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(GreenCheckbox, {
          checked: property.is_property_approved ? true : false,
          onChange: function onChange(e) {
            return handleApproval(e, property.id, property.unit_name);
          },
          name: property.name
        }),
        label: ""
      })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("td", {
        className: "p-4"
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_base__WEBPACK_IMPORTED_MODULE_1__["Tooltip"], {
        title: "Edit Property"
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("button", {
        onClick: function onClick() {
          _toggleEditPropertyModal();

          setProperty(property);
        },
        className: "cursor-pointer bg-white border border-gray-400 duration-300 hover:bg-palette-purple hover:border-palette-purple hover:text-white inline-flex px-3 py-1 rounded transition-all"
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("svg", {
        className: "feather-icon h-5 w-5 opacity-50"
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("use", {
        xlinkHref: "/assets/svg/feather-sprite.svg#edit"
      }))))));
    })));
  };

  var editToggleFetch = function editToggleFetch() {
    setToggleFetch(!toggleFetch);
  };

  var addToggleFetch = function addToggleFetch() {
    setToggleFetch(!toggleFetch);
  };

  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "border mt-10"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "bg-white"
  }, toggleEditProperty && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_EditPropertyModal__WEBPACK_IMPORTED_MODULE_2__["default"], {
    project: currentProject,
    projectName: currentProject.name,
    projectId: dealId,
    property: property,
    toggleEditPropertyModal: function toggleEditPropertyModal() {
      return _toggleEditPropertyModal();
    },
    editToggleFetch: editToggleFetch
  }), toggleAddProperty && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_AddProperty__WEBPACK_IMPORTED_MODULE_3__["default"], {
    projectName: currentProject.name,
    projectId: dealId,
    toggleAddPropertyModal: function toggleAddPropertyModal() {
      return _toggleAddPropertyModal();
    },
    addToggleFetch: addToggleFetch
  }), toogleEditLandProperty && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_EditLandPropertyModal__WEBPACK_IMPORTED_MODULE_4__["default"], {
    projectName: currentProject.name,
    projectId: currentProject.id,
    property: property,
    toggleEditPropertyModal: function toggleEditPropertyModal() {
      return _toggleEditPropertyModal();
    },
    editToggleFetch: toggleFetch
  }), toogleLandProperty && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_AddLandProperty__WEBPACK_IMPORTED_MODULE_5__["default"], {
    projectName: currentProject.name,
    projectId: currentProject.id,
    toggleAddPropertyModal: function toggleAddPropertyModal() {
      return _toggleAddPropertyModal();
    },
    addToggleFetch: toggleFetch
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "pb-16 px-6 lg:px-10 mx-auto" // style={{ maxWidth: 1366 }}

  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "flex items-center flex items-center lg:pb-16 pb-10 pt-10"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
    className: "bg-white cursor-pointer duration-300 font-bold hover:text-palette-blue inline-flex items-center pr-3 relative text-palette-blue-light text-sm transition-all z-10",
    onClick: function onClick() {
      return window.location = "/admin/projects";
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("svg", {
    className: "feather-icon h-4 w-4 mr-1 inline"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("use", {
    xlinkHref: "/assets/svg/feather-sprite.svg#chevron-left"
  })), "Back to Projects Page"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("h1", {
    className: "flex-1 font-bold leading-tight text-4xl text-center"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
    className: "mr-40 text-center"
  }, currentProject.name))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "flex justify-end"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: " flex justify-center lg:justify-end mb-8"
  }, currentProject.properties && currentProject.properties.length < 5 ? /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_base__WEBPACK_IMPORTED_MODULE_1__["Tooltip"], {
    title: "Click to add a Project"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "w-64 relative "
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("svg", {
    className: "absolute feather-icon h-full left-0 ml-5 text-white"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("use", {
    xlinkHref: "/assets/svg/feather-sprite.svg#plus-circle"
  })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_base__WEBPACK_IMPORTED_MODULE_1__["Button"], {
    onClick: function onClick() {
      return _toggleAddPropertyModal();
    }
  }, "Add Property"))) : /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_base__WEBPACK_IMPORTED_MODULE_1__["Tooltip"], {
    title: "You have reached the maximum 5 properties per project. Once all 5 properties are sold you can upload another 5 properties."
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "w-64 relative "
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("svg", {
    className: "absolute feather-icon h-full left-0 ml-5 text-gray-500"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("use", {
    xlinkHref: "/assets/svg/feather-sprite.svg#plus-circle"
  })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_base__WEBPACK_IMPORTED_MODULE_1__["Button"], {
    disabled: true,
    onClick: function onClick() {
      return _toggleAddPropertyModal();
    }
  }, "Add a Property"))))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_base__WEBPACK_IMPORTED_MODULE_1__["Table"], {
    query: "/api/deal/".concat(dealId, "?paginated=true"),
    toggleFetch: toggleFetch,
    keyword: keyWord,
    getData: function getData(data) {
      return setCurrentProject(data[0]);
    },
    content: propertiesContent(),
    sort: filters.sortBy.value.replace(/asc_|desc_/g, "") || "",
    order: filters.sortBy.order || "",
    header: react_device_detect__WEBPACK_IMPORTED_MODULE_6__["isMobile"] ? null : renderHeaders()
  }))));
};

/* harmony default export */ __webpack_exports__["default"] = (ManageProjectProperties);

/***/ }),

/***/ "./resources/js/admin/pages/Projects.js":
/*!**********************************************!*\
  !*** ./resources/js/admin/pages/Projects.js ***!
  \**********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _components_base__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../components/_base */ "./resources/js/components/_base/index.js");
/* harmony import */ var _components_base__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_components_base__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _components_SequenceAddingModal__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../components/SequenceAddingModal */ "./resources/js/admin/components/SequenceAddingModal.js");
/* harmony import */ var _components_SequenceEditingModal__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../components/SequenceEditingModal */ "./resources/js/admin/components/SequenceEditingModal.js");
/* harmony import */ var _states_userGlobal__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../states/userGlobal */ "./resources/js/states/userGlobal.js");
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! lodash */ "./node_modules/lodash/lodash.js");
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(lodash__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var react_device_detect__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! react-device-detect */ "./node_modules/react-device-detect/main.js");
/* harmony import */ var react_device_detect__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(react_device_detect__WEBPACK_IMPORTED_MODULE_6__);
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! moment */ "./node_modules/moment/moment.js");
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_7___default = /*#__PURE__*/__webpack_require__.n(moment__WEBPACK_IMPORTED_MODULE_7__);
/* harmony import */ var _material_ui_core_Checkbox__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @material-ui/core/Checkbox */ "./node_modules/@material-ui/core/esm/Checkbox/index.js");
/* harmony import */ var _material_ui_core_FormControlLabel__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @material-ui/core/FormControlLabel */ "./node_modules/@material-ui/core/esm/FormControlLabel/index.js");
/* harmony import */ var _material_ui_core_styles__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @material-ui/core/styles */ "./node_modules/@material-ui/core/esm/styles/index.js");
/* harmony import */ var _material_ui_core_colors__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! @material-ui/core/colors */ "./node_modules/@material-ui/core/esm/colors/index.js");
/* harmony import */ var _components_base_alerts_sweetAlert__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ../../components/_base/alerts/sweetAlert */ "./resources/js/components/_base/alerts/sweetAlert.js");
/* harmony import */ var _helpers_addressHelper__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ../../helpers/addressHelper */ "./resources/js/helpers/addressHelper.js");
function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _unsupportedIterableToArray(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

function _iterableToArrayLimit(arr, i) { if (typeof Symbol === "undefined" || !(Symbol.iterator in Object(arr))) return; var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }

















var Projects = function Projects() {
  var _UserGlobal = Object(_states_userGlobal__WEBPACK_IMPORTED_MODULE_4__["default"])(),
      _UserGlobal2 = _slicedToArray(_UserGlobal, 2),
      userState = _UserGlobal2[0],
      userAction = _UserGlobal2[1];

  var _useState = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])(""),
      _useState2 = _slicedToArray(_useState, 2),
      keyWord = _useState2[0],
      setKeyWord = _useState2[1];

  var _useState3 = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])(false),
      _useState4 = _slicedToArray(_useState3, 2),
      toggleFetch = _useState4[0],
      setToggleFetch = _useState4[1];

  var _useState5 = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])([]),
      _useState6 = _slicedToArray(_useState5, 2),
      projects = _useState6[0],
      setProjects = _useState6[1];

  var _useState7 = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])({
    sortBy: {
      value: "deals.created_at",
      order: "desc"
    }
  }),
      _useState8 = _slicedToArray(_useState7, 2),
      filters = _useState8[0],
      setFilters = _useState8[1];

  var _useState9 = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])(false),
      _useState10 = _slicedToArray(_useState9, 2),
      toggleOrder = _useState10[0],
      setToggleOrder = _useState10[1];

  var _useState11 = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])(false),
      _useState12 = _slicedToArray(_useState11, 2),
      toggleSequenceAddingModal = _useState12[0],
      setToggleSequenceAddingModal = _useState12[1];

  var _useState13 = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])(false),
      _useState14 = _slicedToArray(_useState13, 2),
      toggleSequenceEditModal = _useState14[0],
      setToggleSequenceEditModal = _useState14[1];

  var _useState15 = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])([]),
      _useState16 = _slicedToArray(_useState15, 2),
      proj = _useState16[0],
      setProj = _useState16[1];

  var _useState17 = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])([]),
      _useState18 = _slicedToArray(_useState17, 2),
      agents = _useState18[0],
      setAgents = _useState18[1];

  var GreenCheckbox = Object(_material_ui_core_styles__WEBPACK_IMPORTED_MODULE_10__["withStyles"])({
    root: {
      color: _material_ui_core_colors__WEBPACK_IMPORTED_MODULE_11__["green"][400],
      "&$checked": {
        color: _material_ui_core_colors__WEBPACK_IMPORTED_MODULE_11__["green"][600]
      }
    },
    checked: {}
  })(function (props) {
    return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_material_ui_core_Checkbox__WEBPACK_IMPORTED_MODULE_8__["default"], _extends({
      color: "default"
    }, props));
  });
  Object(react__WEBPACK_IMPORTED_MODULE_0__["useEffect"])(function () {
    axios.get("/api/agents").then(function (response) {
      setAgents(response.data);
    });
  }, []);
  Object(react__WEBPACK_IMPORTED_MODULE_0__["useEffect"])(function () {
    if (!toggleSequenceAddingModal) {
      setToggleFetch(!toggleFetch);
    }

    return function () {};
  }, [toggleSequenceAddingModal]);
  Object(react__WEBPACK_IMPORTED_MODULE_0__["useEffect"])(function () {
    if (!toggleSequenceEditModal) {
      setToggleFetch(!toggleFetch);
    }

    return function () {};
  }, [toggleSequenceEditModal]);

  var formatFilter = function formatFilter(column) {
    if (column) {
      setToggleOrder(!toggleOrder);
      setFilters({
        sortBy: {
          value: column,
          order: toggleOrder ? "desc" : "asc"
        }
      });
      setToggleFetch(!toggleFetch);
    }
  };

  var tableHeaders = [{
    label: "Id",
    column: "deals.id",
    width: 40
  }, {
    label: "Name",
    column: "deals.name",
    width: 180
  }, {
    label: "Property Address",
    column: "deals.address",
    width: 200
  }, {
    label: "Property Type",
    column: "deals.sub_property_id",
    width: 150
  }, {
    label: "Agent Name",
    column: "deals.agent_name",
    width: 150
  }, {
    label: "Proposed Settlement",
    column: "deals.proposed_settlement",
    width: 150
  }, {
    label: "Project Time Limit",
    column: "deals.deal_time_limit",
    width: 150
  }, {
    label: "Featured",
    column: "is_deal_approved",
    width: 80
  }, {
    label: "Weekly",
    column: "is_weekly",
    width: 80
  }, {
    label: "Approved",
    column: "is_featured",
    width: 80
  }, {
    label: "Preview",
    column: "",
    width: 60
  }, {
    label: "Edit",
    column: "",
    width: 60
  }];
  var handleSearch = Object(lodash__WEBPACK_IMPORTED_MODULE_5__["debounce"])(function (text) {
    setKeyWord(text);
  }, 800);

  var renderHeaders = function renderHeaders() {
    return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("thead", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("tr", null, tableHeaders.map(function (th, index) {
      return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("th", {
        onClick: function onClick() {
          return formatFilter(th.column);
        },
        style: {
          width: th.width
        },
        className: "border-b font-bold px-4 py-2 text-palette-gray px-4 py-2 cursor-pointer hover:text-palette-gray-dark ",
        key: index
      }, th.label);
    })));
  };

  var toggleSequenceAM = function toggleSequenceAM() {
    setToggleSequenceAddingModal(!toggleSequenceAddingModal);
  };

  var toggleSequenceEM = function toggleSequenceEM() {
    setToggleSequenceEditModal(!toggleSequenceEditModal);
  };

  var handleApproval = function handleApproval(event, pId) {
    axios({
      method: "post",
      url: "/api/approve-deal",
      data: {
        value: event.target.checked,
        projectId: pId
      }
    }).then(function (res) {
      if (res.status === 200) {
        var data = res.data;

        if (!data.success) {
          Object(_components_base_alerts_sweetAlert__WEBPACK_IMPORTED_MODULE_12__["sweetAlert"])("error", data.message);
        } else {
          setToggleFetch(!toggleFetch);
          Object(_components_base_alerts_sweetAlert__WEBPACK_IMPORTED_MODULE_12__["sweetAlert"])("success", data.message);
        }
      }
    });
  };

  var handleFeatured = function handleFeatured(event, pId, dealApproved, name) {
    if (!dealApproved) {
      return Object(_components_base_alerts_sweetAlert__WEBPACK_IMPORTED_MODULE_12__["sweetAlert"])("error", "".concat(name, " is not yet approved!"));
    }

    var status = event.target.checked;
    axios({
      method: "post",
      url: "/api/featured-deal",
      data: {
        value: status,
        id: pId,
        field: "is_featured"
      }
    }).then(function (res) {
      if (res.status === 200) {
        setToggleFetch(!toggleFetch);
        var msg = status ? "".concat(name, " is now added to the featured projects!") : "".concat(name, " is now removed on the featured projects!");
        Object(_components_base_alerts_sweetAlert__WEBPACK_IMPORTED_MODULE_12__["sweetAlert"])("success", msg);
      }
    });
  };

  var handleWeekly = function handleWeekly(event, pId, dealApproved, name) {
    if (!dealApproved) {
      return Object(_components_base_alerts_sweetAlert__WEBPACK_IMPORTED_MODULE_12__["sweetAlert"])("error", "".concat(name, " is not yet approved!"));
    }

    var status = event.target.checked;
    axios({
      method: "post",
      url: "/api/weekly-deal",
      data: {
        value: status,
        id: pId,
        field: "is_weekly"
      }
    }).then(function (res) {
      if (res.status === 200) {
        setToggleFetch(!toggleFetch);
        var msg = status ? "".concat(name, " is now set as deal of the week!") : "".concat(name, " is now removed as deal of the week!");
        Object(_components_base_alerts_sweetAlert__WEBPACK_IMPORTED_MODULE_12__["sweetAlert"])("success", msg);
      }
    });
  };

  var projectsContent = function projectsContent() {
    return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react__WEBPACK_IMPORTED_MODULE_0___default.a.Fragment, null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("tbody", null, !Object(lodash__WEBPACK_IMPORTED_MODULE_5__["isEmpty"])(projects) && projects.map(function (project, index) {
      var timeLimit = project.expires_at ? JSON.parse(project.expires_at) : project.created_at;
      return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("tr", {
        key: project.id,
        className: "hover:bg-gray-100 border-b text-base"
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("td", {
        className: "p-4"
      }, project.id), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("td", {
        onClick: function onClick() {
          window.location = "/admin/manage-properties/".concat(project.id, "/").concat(project.sub_property_id);
        },
        className: "p-4 transform font-bold text-palette-purple hover:text-palette-blue-light cursor-pointer"
      }, project.name), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("td", {
        className: "p-4"
      }, Object(_helpers_addressHelper__WEBPACK_IMPORTED_MODULE_13__["addressHelper"])(project.address)), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("td", {
        className: "p-4"
      }, project.deal_type), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("td", {
        className: "p-4"
      }, project.agent_name), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("td", {
        className: "p-4"
      }, " ", project.sub_property_id != 4 ? moment__WEBPACK_IMPORTED_MODULE_7___default()(project.proposed_settlement).format("DD-MM-YYYY") : moment__WEBPACK_IMPORTED_MODULE_7___default()(project.land_registration).format("DD-MM-YYYY")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("td", {
        className: "p-4"
      }, " ", project.expires_at ? moment__WEBPACK_IMPORTED_MODULE_7___default()(timeLimit.time_limit).format("DD-MM-YYYY") : moment__WEBPACK_IMPORTED_MODULE_7___default()(project.deal_time_limit).format("DD-MM-YYYY")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("td", {
        className: "p-4 pl-10"
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_material_ui_core_FormControlLabel__WEBPACK_IMPORTED_MODULE_9__["default"], {
        control: /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(GreenCheckbox, {
          checked: project.is_featured,
          onChange: function onChange(e) {
            return handleFeatured(e, project.id, project.is_deal_approved, project.name);
          },
          name: project.name
        }),
        label: ""
      })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("td", {
        className: "p-4 pl-10"
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_material_ui_core_FormControlLabel__WEBPACK_IMPORTED_MODULE_9__["default"], {
        control: /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(GreenCheckbox, {
          checked: project.is_weekly,
          onChange: function onChange(e) {
            return handleWeekly(e, project.id, project.is_deal_approved, project.name);
          },
          name: project.name
        }),
        label: ""
      })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("td", {
        className: "p-4 pl-10"
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_material_ui_core_FormControlLabel__WEBPACK_IMPORTED_MODULE_9__["default"], {
        control: /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(GreenCheckbox, {
          checked: project.is_deal_approved,
          onChange: function onChange(e) {
            return handleApproval(e, project.id);
          },
          name: project.name
        }),
        label: ""
      })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("td", {
        className: "p-4"
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_base__WEBPACK_IMPORTED_MODULE_1__["Tooltip"], {
        title: "Preview"
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("button", {
        onClick: function onClick() {
          window.location = "/weekly-deals/".concat(project.id, "/", true);
        },
        className: "cursor-pointer bg-white border border-gray-400 duration-300 hover:bg-palette-purple hover:border-palette-purple hover:text-white inline-flex px-3 py-1 rounded transition-all"
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("svg", {
        className: "feather-icon h-5 w-5 opacity-50"
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("use", {
        xlinkHref: "/assets/svg/feather-sprite.svg#eye"
      }))))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("td", {
        className: "p-4"
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_base__WEBPACK_IMPORTED_MODULE_1__["Tooltip"], {
        title: "Edit"
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("button", {
        onClick: function onClick() {
          setProj(project);
          toggleSequenceEM(project);
        },
        className: "cursor-pointer bg-white border border-gray-400 duration-300 hover:bg-palette-purple hover:border-palette-purple hover:text-white inline-flex px-3 py-1 rounded transition-all"
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("svg", {
        className: "feather-icon h-5 w-5 opacity-50"
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("use", {
        xlinkHref: "/assets/svg/feather-sprite.svg#edit"
      }))))));
    })));
  };

  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "border mt-10"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "bg-white"
  }, toggleSequenceEditModal && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_SequenceEditingModal__WEBPACK_IMPORTED_MODULE_3__["default"], {
    project: proj,
    editToggleFetch: function editToggleFetch() {
      setToggleFetch(!toggleFetch);
    },
    toggleSequenceEditModal: function toggleSequenceEditModal() {
      return toggleSequenceEM();
    }
  }), toggleSequenceAddingModal && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_SequenceAddingModal__WEBPACK_IMPORTED_MODULE_2__["default"], {
    addToggleFetch: function addToggleFetch() {
      setToggleFetch(!toggleFetch);
    },
    toggleSequenceAddingModal: function toggleSequenceAddingModal() {
      return toggleSequenceAM();
    }
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "pb-16 px-6 lg:px-10 mx-auto"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("h1", {
    className: "font-bold leading-tight text-4xl pt-10 pb-8 lg:pb-16 text-center"
  }, "Projects"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "flex justify-between"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "flex justify-center lg:justify-end mb-8"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "w-64 relative "
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("svg", {
    className: "absolute feather-icon h-8 left-0 mx-3 text-gray-500"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("use", {
    xlinkHref: "/assets/svg/feather-sprite.svg#search"
  })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_base__WEBPACK_IMPORTED_MODULE_1__["TextInput"], {
    className: "pl-8",
    type: "text",
    placeholder: "Search",
    border: false,
    appearance: true,
    onChange: function onChange(e) {
      return handleSearch(e.target.value);
    }
  }))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: " flex justify-center lg:justify-end mb-8"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_base__WEBPACK_IMPORTED_MODULE_1__["Tooltip"], {
    title: "Click to add Project"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "w-64 relative "
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("svg", {
    className: "absolute feather-icon h-full pt-2 left-0 ml-5 text-white pb-2"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("use", {
    xlinkHref: "/assets/svg/feather-sprite.svg#plus-circle"
  })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_base__WEBPACK_IMPORTED_MODULE_1__["Button"], {
    onClick: function onClick() {
      return toggleSequenceAM();
    }
  }, "Add Project"))))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_base__WEBPACK_IMPORTED_MODULE_1__["Table"], {
    query: "/api/deal",
    toggleFetch: toggleFetch,
    keyword: keyWord,
    getData: setProjects,
    content: projectsContent(),
    sort: filters.sortBy.value.replace(/asc_|desc_/g, "") || "",
    order: filters.sortBy.order || "",
    header: react_device_detect__WEBPACK_IMPORTED_MODULE_6__["isMobile"] ? null : renderHeaders()
  }))));
};

/* harmony default export */ __webpack_exports__["default"] = (Projects);

/***/ }),

/***/ "./resources/js/admin/pages/Properties.js":
/*!************************************************!*\
  !*** ./resources/js/admin/pages/Properties.js ***!
  \************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime/regenerator */ "./node_modules/@babel/runtime/regenerator/index.js");
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _components_base__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../components/_base */ "./resources/js/components/_base/index.js");
/* harmony import */ var _components_base__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(_components_base__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _components_EditPropertyModal__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../components/EditPropertyModal */ "./resources/js/admin/components/EditPropertyModal.js");
/* harmony import */ var _components_AddPropertyModal__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../components/AddPropertyModal */ "./resources/js/admin/components/AddPropertyModal.js");
/* harmony import */ var _components_EditLandPropertyModal__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../components/EditLandPropertyModal */ "./resources/js/admin/components/EditLandPropertyModal.js");
/* harmony import */ var _components_AddLandPropertyModal__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../components/AddLandPropertyModal */ "./resources/js/admin/components/AddLandPropertyModal.js");
/* harmony import */ var react_currency_format__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! react-currency-format */ "./node_modules/react-currency-format/lib/currency-format.js");
/* harmony import */ var react_currency_format__WEBPACK_IMPORTED_MODULE_7___default = /*#__PURE__*/__webpack_require__.n(react_currency_format__WEBPACK_IMPORTED_MODULE_7__);
/* harmony import */ var _states_userGlobal__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../../states/userGlobal */ "./resources/js/states/userGlobal.js");
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! lodash */ "./node_modules/lodash/lodash.js");
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_9___default = /*#__PURE__*/__webpack_require__.n(lodash__WEBPACK_IMPORTED_MODULE_9__);
/* harmony import */ var react_device_detect__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! react-device-detect */ "./node_modules/react-device-detect/main.js");
/* harmony import */ var react_device_detect__WEBPACK_IMPORTED_MODULE_10___default = /*#__PURE__*/__webpack_require__.n(react_device_detect__WEBPACK_IMPORTED_MODULE_10__);
/* harmony import */ var _material_ui_core_Checkbox__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! @material-ui/core/Checkbox */ "./node_modules/@material-ui/core/esm/Checkbox/index.js");
/* harmony import */ var _material_ui_core_FormControlLabel__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! @material-ui/core/FormControlLabel */ "./node_modules/@material-ui/core/esm/FormControlLabel/index.js");
/* harmony import */ var _material_ui_core_styles__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! @material-ui/core/styles */ "./node_modules/@material-ui/core/esm/styles/index.js");
/* harmony import */ var _material_ui_core_colors__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! @material-ui/core/colors */ "./node_modules/@material-ui/core/esm/colors/index.js");
/* harmony import */ var _components_base_alerts_sweetAlert__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! ../../components/_base/alerts/sweetAlert */ "./resources/js/components/_base/alerts/sweetAlert.js");
/* harmony import */ var _helpers_addressHelper__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! ../../helpers/addressHelper */ "./resources/js/helpers/addressHelper.js");


function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _unsupportedIterableToArray(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

function _iterableToArrayLimit(arr, i) { if (typeof Symbol === "undefined" || !(Symbol.iterator in Object(arr))) return; var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }


















var Properties = function Properties() {
  var _useState = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(""),
      _useState2 = _slicedToArray(_useState, 2),
      keyWord = _useState2[0],
      setKeyWord = _useState2[1];

  var _useState3 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(false),
      _useState4 = _slicedToArray(_useState3, 2),
      toggleFetch = _useState4[0],
      setToggleFetch = _useState4[1];

  var _useState5 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])([]),
      _useState6 = _slicedToArray(_useState5, 2),
      properties = _useState6[0],
      setProperties = _useState6[1];

  var _useState7 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])({
    sortBy: {
      value: "properties.created_at",
      order: "desc"
    }
  }),
      _useState8 = _slicedToArray(_useState7, 2),
      filters = _useState8[0],
      setFilters = _useState8[1];

  var _useState9 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(false),
      _useState10 = _slicedToArray(_useState9, 2),
      toggleOrder = _useState10[0],
      setToggleOrder = _useState10[1];

  var _useState11 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(false),
      _useState12 = _slicedToArray(_useState11, 2),
      toggleEditProperty = _useState12[0],
      setToggleEditProperty = _useState12[1];

  var _useState13 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(false),
      _useState14 = _slicedToArray(_useState13, 2),
      toggleAddProperty = _useState14[0],
      setToggleAddProperty = _useState14[1];

  var _useState15 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])([]),
      _useState16 = _slicedToArray(_useState15, 2),
      property = _useState16[0],
      setProperty = _useState16[1];

  var _useState17 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])([]),
      _useState18 = _slicedToArray(_useState17, 2),
      currentProject = _useState18[0],
      setCurrentProject = _useState18[1];

  var _useState19 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(false),
      _useState20 = _slicedToArray(_useState19, 2),
      isProjectLand = _useState20[0],
      setIsProjectLand = _useState20[1];

  var _useState21 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(false),
      _useState22 = _slicedToArray(_useState21, 2),
      toogleLandProperty = _useState22[0],
      setToogleLandProperty = _useState22[1];

  var _useState23 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(false),
      _useState24 = _slicedToArray(_useState23, 2),
      toogleEditLandProperty = _useState24[0],
      setToogleEditLandProperty = _useState24[1];

  var formatFilter = function formatFilter(column) {
    if (column) {
      setToggleOrder(!toggleOrder);
      setFilters({
        sortBy: {
          value: column,
          order: toggleOrder ? "desc" : "asc"
        }
      });
      setToggleFetch(!toggleFetch);
    }
  };

  Object(react__WEBPACK_IMPORTED_MODULE_1__["useEffect"])(function () {
    if (!toggleAddProperty) {
      setToggleFetch(!toggleFetch);
    }

    return function () {};
  }, [toggleAddProperty]);
  Object(react__WEBPACK_IMPORTED_MODULE_1__["useEffect"])(function () {
    if (!toggleEditProperty) {
      setToggleFetch(!toggleFetch);
    }

    return function () {};
  }, [toggleEditProperty]);
  var GreenCheckbox = Object(_material_ui_core_styles__WEBPACK_IMPORTED_MODULE_13__["withStyles"])({
    root: {
      color: _material_ui_core_colors__WEBPACK_IMPORTED_MODULE_14__["green"][400],
      "&$checked": {
        color: _material_ui_core_colors__WEBPACK_IMPORTED_MODULE_14__["green"][600]
      }
    },
    checked: {}
  })(function (props) {
    return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_material_ui_core_Checkbox__WEBPACK_IMPORTED_MODULE_11__["default"], _extends({
      color: "default"
    }, props));
  });
  var tableHeaders = [{
    label: "Id",
    column: "properties.id",
    width: 40
  }, {
    label: "Project Name",
    column: "properties.deal_id",
    width: 100
  }, {
    label: "Property Number",
    column: "properties.unit_name",
    width: 100
  }, {
    label: "Property Address",
    column: "properties.address",
    width: 200
  }, {
    label: "Developer Price",
    column: "properties.price",
    width: 150
  }, {
    label: "Discount",
    column: "properties.price",
    width: 80
  }, {
    label: "GroupBuyer Price",
    column: "properties.price",
    width: 150
  }, // {
  //   label: "Floor Area",
  //   column: "properties.floor_area",
  //   width: 150
  // },
  {
    label: "Approved",
    column: "is_property_approved",
    width: 150
  }, {
    label: "Edit",
    column: "",
    width: 40
  }];
  var handleSearch = Object(lodash__WEBPACK_IMPORTED_MODULE_9__["debounce"])(function (text) {
    setKeyWord(text);
  }, 800);

  var renderHeaders = function renderHeaders() {
    return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("thead", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("tr", null, tableHeaders.map(function (th, index) {
      return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("th", {
        onClick: function onClick() {
          return formatFilter(th.column);
        },
        style: {
          width: th.width
        },
        className: "border-b font-bold px-4 py-2 text-palette-gray px-4 py-2\n                cursor-pointer hover:text-palette-gray-dark\n                ".concat(th === "Edit" ? "text-center" : "text-left"),
        key: index
      }, th.label);
    })));
  };

  var getProperty = /*#__PURE__*/function () {
    var _ref = _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee(propId) {
      var url;
      return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee$(_context) {
        while (1) {
          switch (_context.prev = _context.next) {
            case 0:
              url = "/api/property/".concat(propId);
              _context.next = 3;
              return axios.get(url).then(function (res) {
                if (res.data.property_type_id == 4) {
                  setIsProjectLand(true);
                } else {
                  setIsProjectLand(false);
                }

                setProperty(res.data);
                showModal(res.data.property_type_id == 4);
              });

            case 3:
            case "end":
              return _context.stop();
          }
        }
      }, _callee);
    }));

    return function getProperty(_x) {
      return _ref.apply(this, arguments);
    };
  }();

  var showModal = function showModal(show) {
    if (!show) {
      setToggleEditProperty(!toggleEditProperty);
    } else {
      setToogleEditLandProperty(!toogleEditLandProperty);
    }
  };

  var _toggleEditPropertyModal = function toggleEditPropertyModal() {
    if (!isProjectLand) {
      setToggleEditProperty(!toggleEditProperty);
    } else {
      setToogleEditLandProperty(!toogleEditLandProperty);
    }
  };

  var _toggleAddPropertyModal = function toggleAddPropertyModal() {
    setToggleAddProperty(!toggleAddProperty);
  };

  var _toggleAddLandPropertyModal = function toggleAddLandPropertyModal() {
    setToogleLandProperty(!toogleLandProperty);
  };

  var handleApproval = function handleApproval(event, propId, property) {
    var name = "".concat(property.deal_name, " ").concat(property.unit_name);
    var status = event.target.checked;
    axios({
      method: "post",
      url: "/api/approve-property",
      data: {
        value: status,
        propertyId: propId
      }
    }).then(function (res) {
      if (res.status === 200) {
        var msg = status ? "".concat(name, " is now added to the approved properties!") : "".concat(name, " is now removed from approved properties!");
        Object(_components_base_alerts_sweetAlert__WEBPACK_IMPORTED_MODULE_15__["sweetAlert"])("success", msg);
        setToggleFetch(!toggleFetch);
      }
    });
  };

  var propertiesContent = function propertiesContent() {
    return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(react__WEBPACK_IMPORTED_MODULE_1___default.a.Fragment, null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("tbody", null, properties.map(function (property) {
      return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("tr", {
        key: property.id,
        className: "hover:bg-gray-100 border-b text-base"
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("td", {
        className: "p-4"
      }, property.id), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("td", {
        className: "p-4 font-bold text-palette-purple"
      }, property.deal_name), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("td", {
        className: "p-4"
      }, property.unit_name), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("td", {
        className: "p-4"
      }, Object(_helpers_addressHelper__WEBPACK_IMPORTED_MODULE_16__["addressHelper"])(property.address)), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("td", {
        className: "p-4"
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(react_currency_format__WEBPACK_IMPORTED_MODULE_7___default.a, {
        value: property.price,
        displayType: "text",
        thousandSeparator: true,
        prefix: "$"
      })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("td", {
        className: "p-4"
      }, "".concat(property.discount ? property.discount + "%" : "N/A", " ")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("td", {
        className: "p-4 font-semibold text-red-700 pl-10"
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components_base__WEBPACK_IMPORTED_MODULE_2__["Tooltip"], {
        title: "Price discount: ".concat(property.discount, "%")
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(react_currency_format__WEBPACK_IMPORTED_MODULE_7___default.a, {
        value: Math.ceil(property.price - property.price * (property.discount / 100)),
        displayType: "text",
        thousandSeparator: true,
        prefix: "$"
      }))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("td", {
        className: "p-4 pl-10"
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_material_ui_core_FormControlLabel__WEBPACK_IMPORTED_MODULE_12__["default"], {
        control: /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(GreenCheckbox, {
          checked: property.is_property_approved ? true : false,
          onChange: function onChange(e) {
            return handleApproval(e, property.id, property);
          },
          name: property.name
        }),
        label: ""
      })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("td", {
        className: "p-4"
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components_base__WEBPACK_IMPORTED_MODULE_2__["Tooltip"], {
        title: "Edit Property"
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
        onClick: function onClick() {
          return getProperty(property.id);
        },
        className: "cursor-pointer bg-white border border-gray-400 duration-300 hover:bg-palette-purple hover:border-palette-purple hover:text-white inline-flex px-3 py-1 rounded transition-all"
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("svg", {
        className: "feather-icon h-5 w-5 opacity-50"
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("use", {
        xlinkHref: "/assets/svg/feather-sprite.svg#edit"
      }))))));
    })));
  };

  var editToggleFetch = function editToggleFetch() {
    setToggleFetch(!toggleFetch);
  };

  var addToggleFetch = function addToggleFetch() {
    setToggleFetch(!toggleFetch);
  };

  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "border mt-10"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "bg-white"
  }, toggleEditProperty && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components_EditPropertyModal__WEBPACK_IMPORTED_MODULE_3__["default"], {
    projectName: currentProject.name,
    projectId: currentProject.id,
    property: property,
    toggleEditPropertyModal: function toggleEditPropertyModal() {
      return _toggleEditPropertyModal();
    },
    editToggleFetch: editToggleFetch
  }), toggleAddProperty && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components_AddPropertyModal__WEBPACK_IMPORTED_MODULE_4__["default"], {
    projectName: currentProject.name,
    projectId: currentProject.id,
    toggleAddPropertyModal: function toggleAddPropertyModal() {
      return _toggleAddPropertyModal();
    },
    addToggleFetch: addToggleFetch
  }), toogleLandProperty && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components_AddLandPropertyModal__WEBPACK_IMPORTED_MODULE_6__["default"], {
    projectName: currentProject.name,
    projectId: currentProject.id,
    toggleAddLandPropertyModal: function toggleAddLandPropertyModal() {
      return _toggleAddLandPropertyModal();
    },
    addToggleFetch: toggleFetch
  }), toogleEditLandProperty && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components_EditLandPropertyModal__WEBPACK_IMPORTED_MODULE_5__["default"], {
    projectName: currentProject.name,
    projectId: currentProject.id,
    property: property,
    toggleEditPropertyModal: function toggleEditPropertyModal() {
      return _toggleEditPropertyModal();
    },
    editToggleFetch: toggleFetch
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "pb-16 px-6 lg:px-10 mx-auto"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("h1", {
    className: "font-bold leading-tight text-4xl pt-10 pb-8 lg:pb-16 text-center"
  }, "Properties"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "flex justify-between"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "flex justify-center lg:justify-end mb-8"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "w-64 relative "
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("svg", {
    className: "absolute feather-icon h-8 left-0 mx-3 text-gray-500"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("use", {
    xlinkHref: "/assets/svg/feather-sprite.svg#search"
  })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components_base__WEBPACK_IMPORTED_MODULE_2__["TextInput"], {
    className: "pl-8",
    type: "text",
    placeholder: "Search",
    border: false,
    appearance: true,
    onChange: function onChange(e) {
      return handleSearch(e.target.value);
    }
  }))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: " flex justify-center lg:justify-end mb-8"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components_base__WEBPACK_IMPORTED_MODULE_2__["Tooltip"], {
    title: "Click to add a Project"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "mr-3 relative "
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("svg", {
    className: "absolute feather-icon pt-2 h-full left-0 ml-5 text-white pb-2"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("use", {
    xlinkHref: "/assets/svg/feather-sprite.svg#plus-circle"
  })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components_base__WEBPACK_IMPORTED_MODULE_2__["Button"], {
    onClick: function onClick() {
      return _toggleAddPropertyModal();
    }
  }, "Add Property"))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components_base__WEBPACK_IMPORTED_MODULE_2__["Tooltip"], {
    title: "Click to add a Project"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "w-64 relative "
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("svg", {
    className: "absolute feather-icon pt-2 h-full left-0 ml-5 text-white pb-2"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("use", {
    xlinkHref: "/assets/svg/feather-sprite.svg#plus-circle"
  })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components_base__WEBPACK_IMPORTED_MODULE_2__["Button"], {
    onClick: function onClick() {
      return _toggleAddLandPropertyModal();
    }
  }, "Add Land Property"))))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components_base__WEBPACK_IMPORTED_MODULE_2__["Table"], {
    query: "/api/property",
    toggleFetch: toggleFetch,
    keyword: keyWord,
    getData: setProperties,
    content: propertiesContent(),
    sort: filters.sortBy.value.replace(/asc_|desc_/g, "") || "",
    order: filters.sortBy.order || "",
    header: react_device_detect__WEBPACK_IMPORTED_MODULE_10__["isMobile"] ? null : renderHeaders()
  }))));
};

/* harmony default export */ __webpack_exports__["default"] = (Properties);

/***/ }),

/***/ 0:
/*!*****************************************!*\
  !*** multi ./resources/js/admin/app.js ***!
  \*****************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! C:\Users\Fligno-Laptop-099\Documents\projects\group-buyer-web\resources\js\admin\app.js */"./resources/js/admin/app.js");


/***/ })

},[[0,"/js/manifest","/js/vendor"]]]);