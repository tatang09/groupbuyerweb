(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["ManagePropertiesPage"],{

/***/ "./resources/js/pages/ManagePropertiesPage.js":
/*!****************************************************!*\
  !*** ./resources/js/pages/ManagePropertiesPage.js ***!
  \****************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime/regenerator */ "./node_modules/@babel/runtime/regenerator/index.js");
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _layouts_users_UserLayout__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../layouts/users/UserLayout */ "./resources/js/layouts/users/UserLayout.js");
/* harmony import */ var _components_base__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../components/_base */ "./resources/js/components/_base/index.js");
/* harmony import */ var _components_base__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(_components_base__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var _components_EditPropertyModal__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../components/EditPropertyModal */ "./resources/js/components/EditPropertyModal.js");
/* harmony import */ var _components_AddPropertyModal__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../components/AddPropertyModal */ "./resources/js/components/AddPropertyModal.js");
/* harmony import */ var _components_AddLandProperty__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../components/AddLandProperty */ "./resources/js/components/AddLandProperty.js");
/* harmony import */ var _components_EditLandPropertyModal__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../components/EditLandPropertyModal */ "./resources/js/components/EditLandPropertyModal.js");
/* harmony import */ var react_currency_format__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! react-currency-format */ "./node_modules/react-currency-format/lib/currency-format.js");
/* harmony import */ var react_currency_format__WEBPACK_IMPORTED_MODULE_8___default = /*#__PURE__*/__webpack_require__.n(react_currency_format__WEBPACK_IMPORTED_MODULE_8__);
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! lodash */ "./node_modules/lodash/lodash.js");
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_9___default = /*#__PURE__*/__webpack_require__.n(lodash__WEBPACK_IMPORTED_MODULE_9__);
/* harmony import */ var react_device_detect__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! react-device-detect */ "./node_modules/react-device-detect/main.js");
/* harmony import */ var react_device_detect__WEBPACK_IMPORTED_MODULE_10___default = /*#__PURE__*/__webpack_require__.n(react_device_detect__WEBPACK_IMPORTED_MODULE_10__);
/* harmony import */ var _helpers_addressHelper__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ../helpers/addressHelper */ "./resources/js/helpers/addressHelper.js");
/* harmony import */ var _fortawesome_free_solid_svg_icons__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! @fortawesome/free-solid-svg-icons */ "./node_modules/@fortawesome/free-solid-svg-icons/index.es.js");
/* harmony import */ var _fortawesome_react_fontawesome__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! @fortawesome/react-fontawesome */ "./node_modules/@fortawesome/react-fontawesome/index.es.js");
/* harmony import */ var react_helmet__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! react-helmet */ "./node_modules/react-helmet/es/Helmet.js");
/* harmony import */ var _helpers_metaHelper_metaHelper__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! ../helpers/metaHelper/metaHelper */ "./resources/js/helpers/metaHelper/metaHelper.js");


function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _unsupportedIterableToArray(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

function _iterableToArrayLimit(arr, i) { if (typeof Symbol === "undefined" || !(Symbol.iterator in Object(arr))) return; var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }

















var ManageProperties = function ManageProperties(_ref) {
  var dealId = _ref.dealId,
      dealSubPropertyId = _ref.dealSubPropertyId;

  var _useState = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(""),
      _useState2 = _slicedToArray(_useState, 2),
      keyWord = _useState2[0],
      setKeyWord = _useState2[1];

  var _useState3 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(false),
      _useState4 = _slicedToArray(_useState3, 2),
      toggleFetch = _useState4[0],
      setToggleFetch = _useState4[1];

  var _useState5 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])([]),
      _useState6 = _slicedToArray(_useState5, 2),
      currentProject = _useState6[0],
      setCurrentProject = _useState6[1];

  var _useState7 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])({
    sortBy: {
      value: "deals.name",
      order: "desc"
    }
  }),
      _useState8 = _slicedToArray(_useState7, 2),
      filters = _useState8[0],
      setFilters = _useState8[1];

  var _useState9 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(false),
      _useState10 = _slicedToArray(_useState9, 2),
      toggleOrder = _useState10[0],
      setToggleOrder = _useState10[1];

  var _useState11 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(false),
      _useState12 = _slicedToArray(_useState11, 2),
      toggleEditProperty = _useState12[0],
      setToggleEditProperty = _useState12[1];

  var _useState13 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(false),
      _useState14 = _slicedToArray(_useState13, 2),
      toggleAddProperty = _useState14[0],
      setToggleAddProperty = _useState14[1];

  var _useState15 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(false),
      _useState16 = _slicedToArray(_useState15, 2),
      toogleLandProperty = _useState16[0],
      setToogleLandProperty = _useState16[1];

  var _useState17 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(false),
      _useState18 = _slicedToArray(_useState17, 2),
      toogleEditLandProperty = _useState18[0],
      setToogleEditLandProperty = _useState18[1];

  var _useState19 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])([]),
      _useState20 = _slicedToArray(_useState19, 2),
      property = _useState20[0],
      setProperty = _useState20[1];

  var _useState21 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(dealSubPropertyId == 4 ? true : false),
      _useState22 = _slicedToArray(_useState21, 2),
      isProjectLand = _useState22[0],
      setIsProjectLand = _useState22[1];

  var formatFilter = function formatFilter(column) {
    if (column) {
      setToggleOrder(!toggleOrder);
      setFilters({
        sortBy: {
          value: column,
          order: toggleOrder ? "desc" : "asc"
        }
      });
      setToggleFetch(!toggleFetch);
    }
  };

  Object(react__WEBPACK_IMPORTED_MODULE_1__["useEffect"])(function () {
    if (!toggleAddProperty) {
      setToggleFetch(!toggleFetch);
    }

    return function () {};
  }, [toggleAddProperty]);
  Object(react__WEBPACK_IMPORTED_MODULE_1__["useEffect"])(function () {
    if (!toggleEditProperty) {
      setToggleFetch(!toggleFetch);
    }

    return function () {};
  }, [toggleEditProperty]);
  var tableHeaders = [{
    label: "Property Id",
    width: 80,
    show: react_device_detect__WEBPACK_IMPORTED_MODULE_10__["isMobile"] ? false : true
  }, {
    label: react_device_detect__WEBPACK_IMPORTED_MODULE_10__["isMobile"] ? "Property#" : "Property Number",
    column: "properties.unit_name",
    width: react_device_detect__WEBPACK_IMPORTED_MODULE_10__["isMobile"] ? 70 : 100,
    show: true
  }, {
    label: "Property Address",
    column: "property.address",
    width: 200,
    show: react_device_detect__WEBPACK_IMPORTED_MODULE_10__["isMobile"] ? false : true
  }, {
    label: "Property Price",
    column: "property.price",
    width: react_device_detect__WEBPACK_IMPORTED_MODULE_10__["isMobile"] ? 100 : 150,
    show: true
  }, {
    label: "Approved",
    column: "property.is_property_approved",
    width: react_device_detect__WEBPACK_IMPORTED_MODULE_10__["isMobile"] ? 80 : 150,
    show: true
  }, {
    label: "Edit",
    column: "",
    width: 40,
    show: true
  }];
  var handleSearch = Object(lodash__WEBPACK_IMPORTED_MODULE_9__["debounce"])(function (text) {
    setKeyWord(text);
  }, 800);

  var renderHeaders = function renderHeaders() {
    return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("thead", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("tr", null, tableHeaders.map(function (th, index) {
      if (th.show) {
        return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("th", {
          style: {
            width: th.width
          },
          className: "sm:text-base border-b font-bold md:px-4 md:py-2 text-palette-gray",
          key: index
        }, th.label);
      }
    })));
  };

  var getProperty = /*#__PURE__*/function () {
    var _ref2 = _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee(propId) {
      var url;
      return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee$(_context) {
        while (1) {
          switch (_context.prev = _context.next) {
            case 0:
              url = "/api/property/".concat(propId);
              _context.next = 3;
              return axios.get(url).then(function (res) {
                if (res.data.property_type_id == 4) {
                  setIsProjectLand(true);
                } else {
                  setIsProjectLand(false);
                }

                setProperty(res.data);
                showModal(res.data.property_type_id == 4);
              });

            case 3:
            case "end":
              return _context.stop();
          }
        }
      }, _callee);
    }));

    return function getProperty(_x) {
      return _ref2.apply(this, arguments);
    };
  }();

  var showModal = function showModal(show) {
    if (!show) {
      setToggleEditProperty(!toggleEditProperty);
    } else {
      setToogleEditLandProperty(!toogleEditLandProperty);
    }
  };

  var _toggleEditPropertyModal = function toggleEditPropertyModal() {
    if (!isProjectLand) {
      setToggleEditProperty(!toggleEditProperty);
    } else {
      setToogleEditLandProperty(!toogleEditLandProperty);
    }
  };

  var _toggleAddPropertyModal = function toggleAddPropertyModal() {
    if (!isProjectLand) {
      setToggleAddProperty(!toggleAddProperty);
    } else {
      setToogleLandProperty(!toogleLandProperty);
    }
  };

  var propertiesContent = function propertiesContent() {
    return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(react__WEBPACK_IMPORTED_MODULE_1___default.a.Fragment, null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("tbody", null, currentProject.properties && currentProject.properties.length > 0 && currentProject.properties.map(function (property) {
      return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("tr", {
        key: property.id,
        className: "hover:bg-gray-100 border-b text-base"
      }, !react_device_detect__WEBPACK_IMPORTED_MODULE_10__["isMobile"] && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("td", {
        className: "pl-10 text-left"
      }, property.id), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("td", {
        className: "cursor-pointer font-bold lg:pl-10 pl-5 text-left text-palette-purple transform"
      }, property.unit_name), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("td", {
        className: "p-4"
      }, Object(_helpers_addressHelper__WEBPACK_IMPORTED_MODULE_11__["addressHelper"])(property.address)), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("td", {
        className: "lg:p-4 py-4"
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(react_currency_format__WEBPACK_IMPORTED_MODULE_8___default.a, {
        value: property.price,
        displayType: "text",
        thousandSeparator: true,
        prefix: "$"
      })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("td", {
        className: "lg:p-4 py-4"
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("svg", {
        className: "feather-icon ml-4 ".concat(property.is_property_approved ? "text-palette-purple h-6 w-6" : "text-gray-500 h-5 w-5")
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("use", {
        xlinkHref: "/assets/svg/feather-sprite.svg#check"
      }))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("td", {
        className: "flex md:p-4 py-4"
      }, !property.is_property_approved ? /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components_base__WEBPACK_IMPORTED_MODULE_3__["Tooltip"], {
        title: "Edit Property"
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("button", {
        onClick: function onClick() {
          return getProperty(property.id);
        },
        className: "bg-white border-gray-400 cursor-pointer duration-300 hover:bg-palette-purple hover:border-palette-purple hover:text-white inline-flex md:border md:px-4 pr-4 py-1 rounded transition-all"
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("svg", {
        className: "feather-icon h-5 w-5 opacity-50"
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("use", {
        xlinkHref: "/assets/svg/feather-sprite.svg#edit"
      })))) : /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components_base__WEBPACK_IMPORTED_MODULE_3__["Tooltip"], {
        title: "Property is already approved. Changes are not allowed."
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("svg", {
        className: "feather-icon h-5 w-5 opacity-50"
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("use", {
        xlinkHref: "/assets/svg/feather-sprite.svg#lock"
      })))));
    })));
  };

  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(react__WEBPACK_IMPORTED_MODULE_1___default.a.Fragment, null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_layouts_users_UserLayout__WEBPACK_IMPORTED_MODULE_2__["default"], null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(react_helmet__WEBPACK_IMPORTED_MODULE_14__["Helmet"], null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("meta", {
    name: "description",
    content: _helpers_metaHelper_metaHelper__WEBPACK_IMPORTED_MODULE_15__["metaHelper"].desc
  })), toggleEditProperty && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components_EditPropertyModal__WEBPACK_IMPORTED_MODULE_4__["default"], {
    project: currentProject,
    projectName: currentProject.name,
    projectId: currentProject.id,
    property: property,
    toggleEditPropertyModal: function toggleEditPropertyModal() {
      return _toggleEditPropertyModal();
    },
    editToggleFetch: function editToggleFetch() {
      setToggleFetch(!toggleFetch);
    }
  }), toggleAddProperty && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components_AddPropertyModal__WEBPACK_IMPORTED_MODULE_5__["default"], {
    projectName: currentProject.name,
    projectId: currentProject.id,
    toggleAddPropertyModal: function toggleAddPropertyModal() {
      return _toggleAddPropertyModal();
    },
    addToggleFetch: function addToggleFetch() {
      setToggleFetch(!toggleFetch);
    }
  }), toogleLandProperty && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components_AddLandProperty__WEBPACK_IMPORTED_MODULE_6__["default"], {
    projectName: currentProject.name,
    projectId: currentProject.id,
    toggleAddPropertyModal: function toggleAddPropertyModal() {
      return _toggleAddPropertyModal();
    },
    addToggleFetch: toggleFetch
  }), toogleEditLandProperty && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components_EditLandPropertyModal__WEBPACK_IMPORTED_MODULE_7__["default"], {
    projectName: currentProject.name,
    projectId: currentProject.id,
    property: property,
    toggleEditPropertyModal: function toggleEditPropertyModal() {
      return _toggleEditPropertyModal();
    },
    editToggleFetch: toggleFetch
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("section", {
    className: "bg-palette-blue-dark",
    style: {
      marginTop: -130,
      paddingTop: 160
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("section", {
    className: "relative bg-white"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "pb-16 px-6 lg:px-20 mx-auto",
    style: {
      maxWidth: 1600
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "flex items-center flex items-center lg:pb-16 pb-10 pt-10"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
    className: "bg-white cursor-pointer duration-300 font-bold hover:text-palette-blue inline-flex items-center pr-3 relative text-palette-blue-light text-sm transition-all z-10",
    onClick: function onClick() {
      return window.location = "/profile/manage-projects";
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("svg", {
    className: "feather-icon h-4 w-4 mr-1 inline"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("use", {
    xlinkHref: "/assets/svg/feather-sprite.svg#chevron-left"
  })), "Back to Projects page"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("h1", {
    className: "flex-1 font-bold leading-tight text-4xl text-center"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
    className: "text-center xs:text-3xl"
  }, " ", "Manage properties"))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "flex justify-between mb-8"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "ml-5 rounded-lg text-base"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("h1", null, "Project:", /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
    className: "font-semibold"
  }, "\xA0", currentProject.name && currentProject.name)), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("h2", null, "Property Count:\xA0", /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
    className: "font-semibold"
  }, currentProject.properties ? "".concat(currentProject.properties.length, " / 5") : 0 / 5))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "flex justify-center lg:justify-end"
  }, currentProject.properties && currentProject.properties.length < 5 ? /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components_base__WEBPACK_IMPORTED_MODULE_3__["Tooltip"], {
    title: "Click to add a Project"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "w-auto"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components_base__WEBPACK_IMPORTED_MODULE_3__["Button"], {
    className: "flex justify-around items-center",
    onClick: function onClick() {
      return _toggleAddPropertyModal();
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_fortawesome_react_fontawesome__WEBPACK_IMPORTED_MODULE_13__["FontAwesomeIcon"], {
    className: "mr-2 ",
    icon: _fortawesome_free_solid_svg_icons__WEBPACK_IMPORTED_MODULE_12__["faPlusCircle"]
  }), "Add Property"))) : /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components_base__WEBPACK_IMPORTED_MODULE_3__["Tooltip"], {
    title: "You have reached the maximum 5 properties per project. Once all 5 properties are sold you can upload another 5 properties.."
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "w-64 relative "
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("svg", {
    className: "absolute feather-icon h-full left-0 ml-5 text-gray-500"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("use", {
    xlinkHref: "/assets/svg/feather-sprite.svg#plus-circle"
  })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components_base__WEBPACK_IMPORTED_MODULE_3__["Button"], {
    disabled: true,
    onClick: function onClick() {
      return _toggleAddPropertyModal();
    }
  }, "Add Property"))))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components_base__WEBPACK_IMPORTED_MODULE_3__["Table"], {
    query: "/api/deal/".concat(dealId, "?paginated=true"),
    toggleFetch: toggleFetch,
    keyword: keyWord,
    getData: function getData(data) {
      return setCurrentProject(data[0]);
    },
    content: propertiesContent(),
    sort: filters.sortBy.value.replace(/asc_|desc_/g, "") || "",
    order: filters.sortBy.order || "",
    header: renderHeaders()
  }))))));
};

/* harmony default export */ __webpack_exports__["default"] = (ManageProperties);

/***/ })

}]);