(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["HowItWorksPage"],{

/***/ "./resources/js/pages/HowItWorksPage.js":
/*!**********************************************!*\
  !*** ./resources/js/pages/HowItWorksPage.js ***!
  \**********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _layouts_users_UserLayout__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../layouts/users/UserLayout */ "./resources/js/layouts/users/UserLayout.js");
/* harmony import */ var _states_userGlobal__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../states/userGlobal */ "./resources/js/states/userGlobal.js");
/* harmony import */ var hero_slider__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! hero-slider */ "./node_modules/hero-slider/dist/index.es.js");
/* harmony import */ var react_helmet__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! react-helmet */ "./node_modules/react-helmet/es/Helmet.js");
/* harmony import */ var _helpers_metaHelper_metaHelper__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../helpers/metaHelper/metaHelper */ "./resources/js/helpers/metaHelper/metaHelper.js");
/* harmony import */ var _material_ui_core_Button__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @material-ui/core/Button */ "./node_modules/@material-ui/core/esm/Button/index.js");
/* harmony import */ var _components__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../components */ "./resources/js/components/index.js");
/* harmony import */ var _components__WEBPACK_IMPORTED_MODULE_7___default = /*#__PURE__*/__webpack_require__.n(_components__WEBPACK_IMPORTED_MODULE_7__);
function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _unsupportedIterableToArray(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

function _iterableToArrayLimit(arr, i) { if (typeof Symbol === "undefined" || !(Symbol.iterator in Object(arr))) return; var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }











var HowItWorksPage = function HowItWorksPage() {
  var _UserGlobal = Object(_states_userGlobal__WEBPACK_IMPORTED_MODULE_2__["default"])(),
      _UserGlobal2 = _slicedToArray(_UserGlobal, 2),
      userState = _UserGlobal2[0],
      userAction = _UserGlobal2[1];

  var _useState = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])(false),
      _useState2 = _slicedToArray(_useState, 2),
      showWork = _useState2[0],
      setShowWork = _useState2[1];

  var nextSlideHandler = react__WEBPACK_IMPORTED_MODULE_0___default.a.useRef();
  var previousSlideHandler = react__WEBPACK_IMPORTED_MODULE_0___default.a.useRef();

  var handleClose = function handleClose() {
    setShowWork(false);
  };

  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_layouts_users_UserLayout__WEBPACK_IMPORTED_MODULE_1__["default"], null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_helmet__WEBPACK_IMPORTED_MODULE_4__["Helmet"], null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("meta", {
    name: "description",
    content: _helpers_metaHelper_metaHelper__WEBPACK_IMPORTED_MODULE_5__["metaHelper"].desc
  })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("section", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "lg:inline-flex",
    id: "HowItWorksHero"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "bg-cover lg:bg-center md:bg-center bg-no-repeat relative lg:w-full",
    style: {
      background: "url('/assets/images/howitworks-hero.png')"
    },
    id: "imageSize-HIW-main"
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "flex-1 flex text-white flex-col bg-palette-violet-main-dark",
    id: "heroSectionContent"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "lg:ml-24 lg:mt-48 lg:mr-48 lg:mb-32 xs:mt-8 xs:ml-8 xs:mr-8 xs:mb-12 2xl:ml-32",
    id: "heroSectionPosition"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("p", {
    className: "font-bold tracking-wider uppercase xs:text-sm text-palette-purple-main"
  }, "Ben Daniel's Vision"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
    className: "pb-4 mb-4 font-extrabold leading-none lg:align-bottom lg:text-6xl xs:text-5xl xs:mr-4",
    id: "heroSectionMainHeading"
  }, "We believe in ", /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("br", null), "a fair-go for Australians"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("p", {
    className: "mt-6 text-base font-light 2xl:font-hairline",
    id: "heroSectionSubHeading"
  }, "We believe everyone deserves to own their own home and benefit from it. We wanted property to be more affordable and accessible to everyday Aussies, not just those with higher incomes. GroupBuyer is a win/win solution where vendors can sell faster in bulk and buyers can purchase below market value.")))))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components__WEBPACK_IMPORTED_MODULE_7__["TheMore"], {
    bgColor: "#ffffff",
    txtColor: "text-black"
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("section", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components__WEBPACK_IMPORTED_MODULE_7__["HowItWorksComponent"], null)), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("section", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "xs:hidden py-10",
    style: {
      background: "linear-gradient(rgb(255 255 255 / 98%), rgb(255 255 255 / 98%)) 0% 0% / cover, url(/assets/images/benefits_for_buyer.png) 0% 0% no-repeat padding-box padding-box rgba(255, 255, 255, 0)",
      backgroundSize: "cover",
      backgroundPosition: "center"
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "w-100 text-center flex flex-col w-full items-center mt-4"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
    className: "font-bold text-palette-purple-main lg:text-7xl"
  }, "How we proceed"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("p", {
    className: "text-base xs:pt-4 xs:pb-4 text-palette-violet-main-light"
  }, "We are transparent with our process because we believe great relationships are formed on the foundation of trust."), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
    src: "assets/images/how-it-works_how-we-proceed.png",
    className: "px-48 py-16"
  })))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("section", {
    id: "howitworks-how-we-proceed",
    className: "lg:hidden md:hidden",
    style: {
      background: "linear-gradient(rgb(255 255 255 / 98%), rgb(255 255 255 / 98%)) 0% 0% / cover, url(/assets/images/benefits_for_buyer.png) 0% 0% no-repeat padding-box padding-box rgba(255, 255, 255, 0)",
      backgroundSize: "cover",
      backgroundPosition: "center"
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "w-100 text-center flex flex-col w-full items-center xs:py-6 w-100 xs:px-8 xs:pb-4"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("h2", {
    className: "text-base font-bold leading-none text-palette-purple-main xs:text-5xl"
  }, "How we proceed"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("p", {
    className: "text-base xs:pt-4 xs:pb-4 text-palette-violet-main-light"
  }, "We are transparent with our process because we believe great relationships are formed on the foundation of trust.")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(hero_slider__WEBPACK_IMPORTED_MODULE_3__["default"], {
    nextSlide: nextSlideHandler,
    orientation: "horizontal",
    initialSlide: 1,
    style: {},
    settings: {
      shouldAutoplay: false,
      shouldDisplayButtons: false,
      height: "70vh",
      slidingDuration: 250,
      slidingDelay: 100
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(hero_slider__WEBPACK_IMPORTED_MODULE_3__["Slide"], null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "text-white ml-10 mr-10 mb-10 rounded-md",
    style: {
      backgroundColor: '#443466'
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("h2", {
    className: "font-extrabold text-base xs:text-4xl text-center pt-4"
  }, "Stage 1"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "text-white pb-2 ml-8 mr-4 justify-center"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("p", {
    className: "font-normal pt-4 text-palette-purple-main text-base"
  }, "GroupBuyer selects properties"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
    className: "font-extralight"
  }, "GroupBuyer does its due diligence and considers the following factors before listing a property online:")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "justify-center pt-2 ml-4 text-base"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("ul", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("li", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
    className: "px-2 font-light xs:ml-2 text-palette-purple-main"
  }, "-"), "Developes"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("li", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
    className: "px-2 font-light xs:ml-2 text-palette-purple-main"
  }, "-"), " Architect/Builder"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("li", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
    className: "px-2 font-light xs:ml-2 text-palette-purple-main"
  }, "-"), "Location/Neighbourhood"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("li", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
    className: "px-2 font-light xs:ml-2 text-palette-purple-main"
  }, "-"), "Local Council"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("li", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
    className: "px-2 font-light xs:ml-2 text-palette-purple-main"
  }, "-"), "Infrastructure (eg. Nearby Transport, Schools, CBD, etc)"))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "py-4"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_material_ui_core_Button__WEBPACK_IMPORTED_MODULE_6__["default"], {
    style: {
      marginLeft: "13.5rem"
    },
    onClick: function onClick() {
      return nextSlideHandler.current();
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
    src: "assets/svg/Group 179.svg",
    style: {
      width: "60%"
    }
  }))))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(hero_slider__WEBPACK_IMPORTED_MODULE_3__["Slide"], null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "text-white ml-10 mr-10 mb-10 rounded-md",
    style: {
      backgroundColor: '#443466'
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("h2", {
    className: "font-extrabold text-base xs:text-4xl text-center pt-4"
  }, "Stage 2"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "text-white pb-2 ml-8 mr-4 justify-center"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("p", {
    className: "font-normal pt-4 text-palette-purple-main text-base"
  }, "GroupBuyer set-up Deals"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
    className: "font-extralight"
  }, "Each property is qualified by price, size & future demand. We tick all the boxes to ensure you're buying a quality property that will increase in value over time.")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "justify-center pt-2 ml-4 text-base"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("ul", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("li", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
    className: "px-2 font-light xs:ml-2 text-palette-purple-main"
  }, "-"), "Developes"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("li", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
    className: "px-2 font-light xs:ml-2 text-palette-purple-main"
  }, "-"), " Architect/Builder"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("li", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
    className: "px-2 font-light xs:ml-2 text-palette-purple-main"
  }, "-"), "Location/Neighbourhood"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("li", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
    className: "px-2 font-light xs:ml-2 text-palette-purple-main"
  }, "-"), "Local Council"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("li", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
    className: "px-2 font-light xs:ml-2 text-palette-purple-main"
  }, "-"), "Infrastructure (eg. Nearby Transport, Schools, CBD, etc)"))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "py-4"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_material_ui_core_Button__WEBPACK_IMPORTED_MODULE_6__["default"], {
    style: {
      marginLeft: "13.5rem"
    },
    onClick: function onClick() {
      return nextSlideHandler.current();
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
    src: "assets/svg/Group 179.svg",
    style: {
      width: "60%"
    }
  }))))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(hero_slider__WEBPACK_IMPORTED_MODULE_3__["Slide"], null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "text-white ml-10 mr-10 mb-10 rounded-md",
    style: {
      backgroundColor: '#443466'
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("h2", {
    className: "font-extrabold text-base xs:text-4xl text-center pt-4"
  }, "Stage 3"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "text-white pb-2 ml-8 mr-4 justify-center"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("p", {
    className: "font-normal pt-4 text-palette-purple-main text-base"
  }, "GroupBuyer gives you access to deals"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
    className: "font-extralight"
  }, "Once a project is live online it's over to you. Available property deals are on a \"first-in-best-dressed\" basis and can not be held without a $1000 deposit.")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "justify-center pt-2 ml-4 text-base"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("ul", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("li", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
    className: "px-2 font-light xs:ml-2 text-palette-purple-main"
  }, "-"), "Developes"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("li", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
    className: "px-2 font-light xs:ml-2 text-palette-purple-main"
  }, "-"), " Architect/Builder"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("li", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
    className: "px-2 font-light xs:ml-2 text-palette-purple-main"
  }, "-"), "Location/Neighbourhood"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("li", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
    className: "px-2 font-light xs:ml-2 text-palette-purple-main"
  }, "-"), "Local Council"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("li", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
    className: "px-2 font-light xs:ml-2 text-palette-purple-main"
  }, "-"), "Infrastructure (eg. Nearby Transport, Schools, CBD, etc)"))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "py-4"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_material_ui_core_Button__WEBPACK_IMPORTED_MODULE_6__["default"], {
    style: {
      marginLeft: "13.5rem"
    },
    onClick: function onClick() {
      return nextSlideHandler.current();
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
    src: "assets/svg/Group 179.svg",
    style: {
      width: "60%"
    }
  }))))))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components__WEBPACK_IMPORTED_MODULE_7__["JoinUs"], null), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components__WEBPACK_IMPORTED_MODULE_7__["CurrentPopularDeals"], null));
};

/* harmony default export */ __webpack_exports__["default"] = (HowItWorksPage);

/***/ })

}]);