(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["BuyingProjectDetailsPage"],{

/***/ "./resources/js/pages/BuyingProjectDetailsPage.js":
/*!********************************************************!*\
  !*** ./resources/js/pages/BuyingProjectDetailsPage.js ***!
  \********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime/regenerator */ "./node_modules/@babel/runtime/regenerator/index.js");
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! lodash */ "./node_modules/lodash/lodash.js");
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(lodash__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _layouts_users_UserLayout__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../layouts/users/UserLayout */ "./resources/js/layouts/users/UserLayout.js");
/* harmony import */ var _states_userGlobal__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../states/userGlobal */ "./resources/js/states/userGlobal.js");
/* harmony import */ var _components__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../components */ "./resources/js/components/index.js");
/* harmony import */ var _components__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(_components__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var _services_auth__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../services/auth */ "./resources/js/services/auth.js");
/* harmony import */ var _components_adComponents_AdContainerComponent__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../components/adComponents/AdContainerComponent */ "./resources/js/components/adComponents/AdContainerComponent.js");
/* harmony import */ var _fortawesome_react_fontawesome__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @fortawesome/react-fontawesome */ "./node_modules/@fortawesome/react-fontawesome/index.es.js");
/* harmony import */ var react_device_detect__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! react-device-detect */ "./node_modules/react-device-detect/main.js");
/* harmony import */ var react_device_detect__WEBPACK_IMPORTED_MODULE_9___default = /*#__PURE__*/__webpack_require__.n(react_device_detect__WEBPACK_IMPORTED_MODULE_9__);
/* harmony import */ var react_currency_format__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! react-currency-format */ "./node_modules/react-currency-format/lib/currency-format.js");
/* harmony import */ var react_currency_format__WEBPACK_IMPORTED_MODULE_10___default = /*#__PURE__*/__webpack_require__.n(react_currency_format__WEBPACK_IMPORTED_MODULE_10__);
/* harmony import */ var react_date_countdown_timer__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! react-date-countdown-timer */ "./node_modules/react-date-countdown-timer/build/index.js");
/* harmony import */ var react_date_countdown_timer__WEBPACK_IMPORTED_MODULE_11___default = /*#__PURE__*/__webpack_require__.n(react_date_countdown_timer__WEBPACK_IMPORTED_MODULE_11__);
/* harmony import */ var react_slick__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! react-slick */ "./node_modules/react-slick/lib/index.js");
/* harmony import */ var react_slick__WEBPACK_IMPORTED_MODULE_12___default = /*#__PURE__*/__webpack_require__.n(react_slick__WEBPACK_IMPORTED_MODULE_12__);
/* harmony import */ var _components_base__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ../components/_base */ "./resources/js/components/_base/index.js");
/* harmony import */ var _components_base__WEBPACK_IMPORTED_MODULE_13___default = /*#__PURE__*/__webpack_require__.n(_components_base__WEBPACK_IMPORTED_MODULE_13__);
/* harmony import */ var _helpers_pricingHelper_discountHelper__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ../helpers/pricingHelper/discountHelper */ "./resources/js/helpers/pricingHelper/discountHelper.js");
/* harmony import */ var _data_developersData_developersData__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! ../data/developersData/developersData */ "./resources/js/data/developersData/developersData.js");


function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _unsupportedIterableToArray(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

function _iterableToArrayLimit(arr, i) { if (typeof Symbol === "undefined" || !(Symbol.iterator in Object(arr))) return; var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }

















var BuyingProjectDetailsPage = function BuyingProjectDetailsPage(_ref) {
  var dealId = _ref.dealId,
      isPreview = _ref.isPreview;

  var _UserGlobal = Object(_states_userGlobal__WEBPACK_IMPORTED_MODULE_4__["default"])(),
      _UserGlobal2 = _slicedToArray(_UserGlobal, 2),
      userState = _UserGlobal2[0],
      userAction = _UserGlobal2[1];

  var _useState = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(true),
      _useState2 = _slicedToArray(_useState, 2),
      loading = _useState2[0],
      setLoading = _useState2[1];

  var _useState3 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(),
      _useState4 = _slicedToArray(_useState3, 2),
      project = _useState4[0],
      setProject = _useState4[1];

  var _useState5 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(""),
      _useState6 = _slicedToArray(_useState5, 2),
      timeleft = _useState6[0],
      setTimeLeft = _useState6[1];

  var _useState7 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(isPreview === "true" ? true : false),
      _useState8 = _slicedToArray(_useState7, 2),
      preview = _useState8[0],
      setPreview = _useState8[1];

  var _useState9 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(""),
      _useState10 = _slicedToArray(_useState9, 2),
      developerLogo = _useState10[0],
      setDeveloperLogo = _useState10[1];

  var _useState11 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(""),
      _useState12 = _slicedToArray(_useState11, 2),
      developerURL = _useState12[0],
      setDeveloperURl = _useState12[1];

  var _useState13 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(false),
      _useState14 = _slicedToArray(_useState13, 2),
      isProjectLand = _useState14[0],
      setIsProjectLand = _useState14[1];

  var _useState15 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(false),
      _useState16 = _slicedToArray(_useState15, 2),
      showBookingModal = _useState16[0],
      setShowBookingModal = _useState16[1];

  var toogleShowBookingModal = function toogleShowBookingModal() {
    setShowBookingModal(!showBookingModal);
  };

  var getDeal = /*#__PURE__*/function () {
    var _ref2 = _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee() {
      var deal, exp, _yield$axios$get, data, newProject;

      return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee$(_context) {
        while (1) {
          switch (_context.prev = _context.next) {
            case 0:
              _context.next = 2;
              return axios.get("/api/deal/".concat(dealId));

            case 2:
              deal = _context.sent;

              if (deal.data.sub_property_id == 4) {
                setIsProjectLand(true);
              }

              if (deal.data && deal.data.expires_at) {
                exp = JSON.parse(deal.data.expires_at);
                setTimeLeft(exp.time_limit);
              }

              Object(_data_developersData_developersData__WEBPACK_IMPORTED_MODULE_15__["getDeveloperById"])(deal.data.user_id).then(function (res) {
                if (res.data.company_logo_path) {
                  setDeveloperLogo(res.data.company_logo_path);
                  setDeveloperURl(res.data.company_url);
                }
              });

              if (!(Object(_services_auth__WEBPACK_IMPORTED_MODULE_6__["isLoggedIn"])() && !Object(lodash__WEBPACK_IMPORTED_MODULE_2__["isEmpty"])(userState.user))) {
                _context.next = 16;
                break;
              }

              _context.next = 9;
              return axios.get("/api/wish-list/".concat(dealId), {
                headers: {
                  Authorization: "Bearer " + Object(_services_auth__WEBPACK_IMPORTED_MODULE_6__["isLoggedIn"])()
                }
              });

            case 9:
              _yield$axios$get = _context.sent;
              data = _yield$axios$get.data;
              newProject = Object.assign({}, deal.data);

              if (data && !Object(lodash__WEBPACK_IMPORTED_MODULE_2__["isEmpty"])(newProject)) {
                data.map(function (d) {
                  newProject.properties.map(function (p) {
                    if (p.id == d.property_id) {
                      p.hasWishlist = true;
                      p.wishListId = d.id;
                    }
                  });
                });
              }

              setProject(newProject);
              _context.next = 17;
              break;

            case 16:
              setProject(deal.data);

            case 17:
              // setProject(deal.data);
              setLoading(false);

            case 18:
            case "end":
              return _context.stop();
          }
        }
      }, _callee);
    }));

    return function getDeal() {
      return _ref2.apply(this, arguments);
    };
  }();

  Object(react__WEBPACK_IMPORTED_MODULE_1__["useEffect"])(function () {
    // if (!isLoggedIn()) {
    //   window.location = `/weekly-deals`;
    // }
    getDeal();
  }, [userState.user]);

  var handleWishlist = /*#__PURE__*/function () {
    var _ref3 = _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee2(property) {
      var formData;
      return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee2$(_context2) {
        while (1) {
          switch (_context2.prev = _context2.next) {
            case 0:
              if (Object(_services_auth__WEBPACK_IMPORTED_MODULE_6__["isLoggedIn"])()) {
                _context2.next = 3;
                break;
              }

              userAction.setState({
                showSignIn: true
              });
              return _context2.abrupt("return");

            case 3:
              formData = new FormData();
              formData.append("property_id", property.id);
              formData.append("deal_id", dealId);
              _context2.prev = 6;

              if (property.hasWishlist) {
                _context2.next = 12;
                break;
              }

              _context2.next = 10;
              return axios.post("/api/wish-list", formData, {
                headers: {
                  Authorization: "Bearer " + Object(_services_auth__WEBPACK_IMPORTED_MODULE_6__["isLoggedIn"])()
                }
              });

            case 10:
              _context2.next = 14;
              break;

            case 12:
              _context2.next = 14;
              return axios["delete"]("/api/wish-list/".concat(property.wishListId), {
                headers: {
                  Authorization: "Bearer " + Object(_services_auth__WEBPACK_IMPORTED_MODULE_6__["isLoggedIn"])()
                }
              });

            case 14:
              getDeal();
              _context2.next = 19;
              break;

            case 17:
              _context2.prev = 17;
              _context2.t0 = _context2["catch"](6);

            case 19:
            case "end":
              return _context2.stop();
          }
        }
      }, _callee2, null, [[6, 17]]);
    }));

    return function handleWishlist(_x) {
      return _ref3.apply(this, arguments);
    };
  }();

  var renderApartments = function renderApartments() {
    return !loading && Object.keys(project).length !== 0 && project.properties.map(function (property, key) {
      // let images = property.featured_images.length > 0 ?  JSON.parse(property.featured_images) : [];
      return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("li", {
        key: key,
        className: "lg:px-3 lg:w-4/12 mb-6 relative w-full"
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
        className: "relative bg-white rounded-md duration-300 transform transition-all ".concat(!property.is_secured ? "lg:hover:scale-105" : ""),
        style: {
          boxShadow: "0 5px 28px 4px rgba(0,0,0,0.1)"
        }
      }, property.is_secured && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
        className: "rounded-md absolute inset-0 z-20 bg-gray-900 bg-opacity-75 flex items-center justify-center"
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
        className: "uppercase font-bold text-4xl text-white flex items-center"
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
        className: "w-12 h-12 rounded-full mr-4 flex items-center justify-center",
        style: {
          background: "#89b036"
        }
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("svg", {
        className: "feather-icon h-8 w-8",
        style: {
          strokeWidth: 4
        }
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("use", {
        xlinkHref: "/assets/svg/feather-sprite.svg#check"
      }))), "Deal Secured")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("a", {
        href: "/weekly-deals/".concat(dealId, "/apartment/").concat(property.id, "/").concat(isPreview || false)
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
        className: "flex items-center justify-between lg:px-4 px-2 py-2"
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
        className: "text-2xl font-bold"
      }, isProjectLand ? "Lot No." : " Apartment", " ", property.unit_name), !isProjectLand && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
        className: "flex font-semibold items-center lg:font-bold my-3"
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
        className: "mr-3 text-lg"
      }, property.no_of_bedrooms, " ", /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_fortawesome_react_fontawesome__WEBPACK_IMPORTED_MODULE_8__["FontAwesomeIcon"], {
        icon: ["fas", "bed"]
      })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
        className: "mr-3 text-lg"
      }, property.no_of_bathrooms, " ", /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_fortawesome_react_fontawesome__WEBPACK_IMPORTED_MODULE_8__["FontAwesomeIcon"], {
        icon: ["fas", "bath"]
      })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
        className: "text-lg"
      }, property.no_of_garages, " ", /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_fortawesome_react_fontawesome__WEBPACK_IMPORTED_MODULE_8__["FontAwesomeIcon"], {
        icon: ["fas", "car"]
      })))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
        className: "relative"
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(react_slick__WEBPACK_IMPORTED_MODULE_12___default.a, apartmentSettings, !loading && property.featured_images.map(function (image, key) {
        return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("img", {
          key: key,
          src: image,
          className: "object-cover carousel-apartment"
        });
      })))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
        className: "p-4"
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
        className: "flex items-center justify-between"
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
        className: "lg:text-3xl text-2xl font-bold py-1 px-6 bg-red-700 text-white -ml-4"
      }, "Save", " ", /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(react_currency_format__WEBPACK_IMPORTED_MODULE_10___default.a, {
        value: property.price * project.discount / 100,
        displayType: "text",
        thousandSeparator: true,
        prefix: "$"
      })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
        className: "text-base flex",
        style: {
          position: "relative"
        }
      }, !preview && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components_base__WEBPACK_IMPORTED_MODULE_13__["Tooltip"], {
        title: "Share",
        placement: "top"
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("svg", {
        className: "feather-icon h-5 w-5 cursor-pointer mr-3",
        onClick: function onClick() {
          return userAction.setState({
            showSocialMedia: true
          });
        }
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("use", {
        xlinkHref: "/assets/svg/feather-sprite.svg#share"
      }))), !preview && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components_base__WEBPACK_IMPORTED_MODULE_13__["Tooltip"], {
        title: "".concat(property.hasWishlist ? "Already on wishlist" : "Add to wishlist"),
        placement: "top"
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("svg", {
        className: "".concat(property.hasWishlist ? "text-red-600" : "text-black", " feather-icon h-5 w-5 cursor-pointer"),
        onClick: function onClick() {
          return handleWishlist(property);
        }
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("use", {
        xlinkHref: "/assets/svg/feather-sprite.svg#heart",
        className: "".concat(property.hasWishlist ? "fill-current" : null)
      }))))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
        className: "flex items-center justify-between mt-3"
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
        className: "opacity-75 line-through font-bold text-xl lg:mr-6"
      }, "WAS", " ", /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(react_currency_format__WEBPACK_IMPORTED_MODULE_10___default.a, {
        value: property.price || 0,
        displayType: "text",
        thousandSeparator: true,
        prefix: "$"
      })), !isProjectLand && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
        className: "opacity-75"
      }, "Internal size: ", property.internal_size, "sqm")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
        className: "flex items-center justify-between"
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
        className: "text-red-700 font-bold text-xl leading-tight"
      }, "NOW", " ", /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(react_currency_format__WEBPACK_IMPORTED_MODULE_10___default.a, {
        value: property.price - property.price * (project.discount / 100),
        displayType: "text",
        thousandSeparator: true,
        prefix: "$"
      })), !isProjectLand && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
        className: "text-palette-blue-light font-bold"
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("a", {
        href: property.floor_plan.file,
        className: "flex items-center ".concat(property.floor_plan.file ? "" : "cursor-not-allowed"),
        target: "_blank"
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("img", {
        src: "/assets/svg/floor-plan.svg",
        className: "mr-2"
      }), "Floor plan"))))));
    });
  };

  var handleBackToSearch = function handleBackToSearch() {
    localStorage.setItem("__use_projects_filters", true);

    if (isPreview) {
      window.location = "/admin/projects";
    } else {
      window.location = "/weekly-deals/".concat(isPreview);
    }
  };

  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_layouts_users_UserLayout__WEBPACK_IMPORTED_MODULE_3__["default"], null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("section", {
    className: "bg-palette-blue-dark",
    style: {
      marginTop: -130,
      paddingTop: 160
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("section", {
    className: "relative bg-white"
  }, !preview && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components__WEBPACK_IMPORTED_MODULE_5__["BuyingProgress"], {
    current: 2,
    isPreview: preview
  }), preview && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "w-full bg-palette-purple text-white font-bold text-center p-4 text-xl"
  }, "You are viewing a preview of the project."), !react_device_detect__WEBPACK_IMPORTED_MODULE_9__["isMobile"] && !loading && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("section", {
    className: "lg:mt-0 mt-16 relative z-20"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(react_slick__WEBPACK_IMPORTED_MODULE_12___default.a, featuredSettings, !loading && project.featured_images.length > 0 && project.featured_images.map(function (image, key) {
    return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("img", {
      key: key,
      src: image,
      className: "object-cover carousel-featured"
    });
  }))), !react_device_detect__WEBPACK_IMPORTED_MODULE_9__["isMobile"] && !loading && project.discount && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("section", {
    className: "mt-8 lg:-mt-8 relative text-white z-20 inline-block w-full lg:w-auto"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "flex lg:inline-flex shadow-lg"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "w-6/12 lg:w-auto bg-red-700 flex items-center lg:pl-12 lg:pr-24 px-3 py-1",
    style: {
      clipPath: "polygon(0 0, 100% 0, 80% 100%, 0 100%)"
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
    className: "font-bold lg:text-4xl mr-4 text-2xl"
  }, "".concat(project.discount, "%")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
    className: "font-bold lg:text-2xl text-lg uppercase"
  }, "Discount")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "w-6/12 lg:w-auto lg:-ml-20 bg-red-800 flex items-center lg:pl-20 px-3 py-1",
    style: {
      clipPath: "polygon(18% 0, 100% 0, 100% 100%, 0 100%)"
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "font-bold leading-none lg:mr-4 mr-1 text-center uppercasee"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "text-sm lg:text-2xl"
  }, "SAVE"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "text-sm lg:text-2xl"
  }, "UP TO")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "ml-2"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(react_currency_format__WEBPACK_IMPORTED_MODULE_10___default.a, {
    value: project.properties.length && Math.max.apply(Math, project.properties.map(function (max) {
      return max.price * project.discount / 100;
    })) || 0,
    displayType: "text",
    thousandSeparator: true,
    prefix: "$",
    className: "font-bold lg:text-4xl text-lg"
  }))))), react_device_detect__WEBPACK_IMPORTED_MODULE_9__["isMobile"] && !loading && project.discount && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("section", {
    className: "mt-8 lg:-mt-8 relative text-white z-20 inline-block w-full lg:w-auto"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "flex lg:inline-flex shadow-lg"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    id: "discountBanner",
    className: "lg:-mr-20 items-center sm:justify-center -mr-16 sm:-mr-32 md:justify-center bg-red-700 flex lg:pl-12 lg:pr-24 lg:w-auto w-full xs:pl-8",
    style: {
      clipPath: "polygon(0 0, 100% 0, 80% 100%, 0 100%)"
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
    className: "font-bold lg:text-4xl mr-4 text-base"
  }, "".concat(project.discount, "%")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
    className: "font-bold lg:text-2xl text-base uppercase"
  }, "Discount")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "bg-red-800 flex items-center lg:-ml-20 lg:pl-20 lg:w-auto pl-12 px-3 py-1 w-full justify-center",
    style: {
      clipPath: "polygon(18% 0, 100% 0, 100% 100%, 0 100%)"
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "font-bold leading-none lg:mr-4 mr-1 text-center uppercasee"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "text-sm lg:text-2xl"
  }, "SAVE"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "text-sm lg:text-2xl"
  }, "UP TO")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "ml-2"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(react_currency_format__WEBPACK_IMPORTED_MODULE_10___default.a, {
    value: project.properties.length && Math.max.apply(Math, project.properties.map(function (max) {
      return max.price * project.discount / 100;
    })) || 0,
    displayType: "text",
    thousandSeparator: true,
    prefix: "$",
    className: "font-bold lg:text-4xl text-lg"
  }))))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("section", {
    className: "lg:px-10 mt-6 pb-16 lg:pb-24 mx-auto relative",
    style: {
      maxWidth: 1600
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("section", {
    className: "flex flex-col lg:flex-row",
    style: {
      minHeight: 440
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "lg:w-3/12 px-4 lg:px-0"
  }, preview && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "relative"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
    className: "bg-white cursor-pointer duration-300 font-bold hover:text-palette-blue inline-flex items-center pr-3 relative text-palette-blue-light text-sm transition-all z-10",
    onClick: handleBackToSearch
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("svg", {
    className: "feather-icon h-4 w-4 mr-1 inline"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("use", {
    xlinkHref: "/assets/svg/feather-sprite.svg#chevron-left"
  })), isPreview ? "Back to Admin Projects" : "Back to search results"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "absolute border-gray-200 border-t w-full",
    style: {
      top: 10
    }
  })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("h1", {
    className: "font-bold lg:mb-2 lg:text-5xl mb-3 mt-2 text-4xl leading-none"
  }, !loading && project.name), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "lg:font-bold text-base"
  }, !loading && project.address && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(react__WEBPACK_IMPORTED_MODULE_1___default.a.Fragment, null, project.address.line_1 ? project.address.line_1 + ", " : "", project.address.line_2 ? project.address.line_2 + ", " : "", project.address.suburbs ? project.address.suburbs + ", " : "", project.address.suburb ? project.address.suburb + ", " : "", project.address.state ? project.address.state + " " : "", project.address.postal ? project.address.postal : "")), !loading && !isProjectLand && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "px-4 py-1 shadow-lg mt-4 bg-red-700 font-bold text-white rounded-lg",
    style: {
      width: react_device_detect__WEBPACK_IMPORTED_MODULE_9__["isMobile"] ? "fit-content" : "100%"
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
    className: "text-white"
  }, "Time left: "), !loading && timeleft && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(react_date_countdown_timer__WEBPACK_IMPORTED_MODULE_11___default.a, {
    dateTo: timeleft ? timeleft : project.created_at,
    locales: ["Year, ", "Month, ", "Day, ", "hr, ", "min, ", "sec"],
    locales_plural: ["Years, ", "Months, ", "Days, ", "hrs, ", "min, ", "sec"]
  }) || "-"), !react_device_detect__WEBPACK_IMPORTED_MODULE_9__["isMobile"] && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "flex"
  }, !Object(lodash__WEBPACK_IMPORTED_MODULE_2__["isEmpty"])(project) && !isProjectLand && !project.is_completed && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components__WEBPACK_IMPORTED_MODULE_5__["EstimatedCompletionDate"], {
    loading: loading,
    project: project
  }), !Object(lodash__WEBPACK_IMPORTED_MODULE_2__["isEmpty"])(project) && !isProjectLand && project.is_completed && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components__WEBPACK_IMPORTED_MODULE_5__["ReadyForOccupancy"], null), !Object(lodash__WEBPACK_IMPORTED_MODULE_2__["isEmpty"])(project) && project.deposit > 0 && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components__WEBPACK_IMPORTED_MODULE_5__["Deposit"], {
    deposit: project.deposit
  })), react_device_detect__WEBPACK_IMPORTED_MODULE_9__["isMobile"] && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "flex xs:flex-col"
  }, !Object(lodash__WEBPACK_IMPORTED_MODULE_2__["isEmpty"])(project) && isProjectLand && !project.is_completed && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components__WEBPACK_IMPORTED_MODULE_5__["EstimatedCompletionDate"], {
    loading: loading,
    project: project
  }), !Object(lodash__WEBPACK_IMPORTED_MODULE_2__["isEmpty"])(project) && project.is_completed && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components__WEBPACK_IMPORTED_MODULE_5__["ReadyForOccupancy"], null), !Object(lodash__WEBPACK_IMPORTED_MODULE_2__["isEmpty"])(project) && project.deposit > 0 && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components__WEBPACK_IMPORTED_MODULE_5__["Deposit"], {
    deposit: project.deposit
  })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "lg:mt-6"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "flex flex-col items-center lg:block"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
    className: "text-gray-600 font-bold text-base w-full"
  }, "DEVELOPER"), developerURL && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("a", {
    href: developerURL,
    target: "_blank"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("img", {
    className: "my-3",
    src: developerLogo
  })), !developerURL && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("img", {
    className: "my-3",
    src: developerLogo
  })), !loading && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "my-4 p-1 rounded text-center"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("button", {
    disabled: isPreview === "true" ? true : false,
    onClick: function onClick() {
      return setShowBookingModal(true);
    },
    className: "rounded text-base font-bold p-2 ".concat(!isPreview ? "text-white" : "text-gray-400", " w-1/2 text-center hover:bg-gray-900 bg-gray-700 transition-all duration-300")
  }, "Book Inspection")))), react_device_detect__WEBPACK_IMPORTED_MODULE_9__["isMobile"] && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("section", {
    className: "lg:mt-0 mt-8 relative z-20"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(react_slick__WEBPACK_IMPORTED_MODULE_12___default.a, featuredSettings, !loading && project.featured_images.length > 0 && project.featured_images.map(function (image, key) {
    return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
      key: key,
      className: "px-1"
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("img", {
      src: image,
      className: "object-cover carousel-featured rounded-lg"
    }));
  }))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "px-6 mt-10 lg:mt-0 lg:px-16 lg:w-7/12"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", null, !loading && !Object(lodash__WEBPACK_IMPORTED_MODULE_2__["isEmpty"])(project) && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(react__WEBPACK_IMPORTED_MODULE_1___default.a.Fragment, null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "border-b-2 pb-4 mb-4 flex items-center justify-between"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "font-bold text-xl lg:text-3xl opacity-50"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
    className: "mr-2"
  }, "From"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(react_currency_format__WEBPACK_IMPORTED_MODULE_10___default.a, {
    value: project && Object(_helpers_pricingHelper_discountHelper__WEBPACK_IMPORTED_MODULE_14__["discountedPriceRange"])(project, false),
    displayType: "text",
    thousandSeparator: true,
    prefix: "$"
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", null, " - "), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(react_currency_format__WEBPACK_IMPORTED_MODULE_10___default.a, {
    value: project && Object(_helpers_pricingHelper_discountHelper__WEBPACK_IMPORTED_MODULE_14__["discountedPriceRange"])(project, true),
    displayType: "text",
    thousandSeparator: true,
    prefix: "$"
  })), !react_device_detect__WEBPACK_IMPORTED_MODULE_9__["isMobile"] && !preview && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "text-base flex"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components_base__WEBPACK_IMPORTED_MODULE_13__["Tooltip"], {
    title: "Share",
    placement: "top"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("svg", {
    className: "feather-icon h-5 w-5 cursor-pointer mr-5",
    onClick: function onClick() {
      return userAction.setState({
        showSocialMedia: true
      });
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("use", {
    xlinkHref: "/assets/svg/feather-sprite.svg#share"
  }))))))), !loading && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(react__WEBPACK_IMPORTED_MODULE_1___default.a.Fragment, null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "text-base",
    dangerouslySetInnerHTML: {
      __html: project.description
    }
  }), !isProjectLand && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("p", {
    className: "italic text-sm mt-10"
  }, "*Property images are for illustration purposes only and may not be specific to your particular property. Property sizes and outgoing costs are estimates only."))), !react_device_detect__WEBPACK_IMPORTED_MODULE_9__["isMobile"] && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "lg:w-2/12"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "text-palette-gray uppercase font-bold text-base mb-4"
  }, "Resources"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("ul", null, !loading && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components__WEBPACK_IMPORTED_MODULE_5__["ResourcesComponent"], {
    project: project
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
    className: "cursor-pointer flex items-center bg-gray-200 block font-bold px-3 py-1 leading-relaxed text-base rounded text-palette-blue-light mt-3",
    onClick: function onClick() {
      return userAction.setState({
        showSocialMedia: true
      });
    }
  }, "Share this project", /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("svg", {
    className: "feather-icon h-4 w-4 ml-2"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("use", {
    xlinkHref: "/assets/svg/feather-sprite.svg#share"
  })))))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("section", {
    className: "border-t-2 lg:mt-8 lg:px-0 mt-4 pt-4 px-6"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "text-3xl font-bold mt-6"
  }, "Available properties"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("ul", {
    className: "flex flex-wrap mt-6"
  }, renderApartments()), react_device_detect__WEBPACK_IMPORTED_MODULE_9__["isMobile"] && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "px-16 mt-6"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "text-palette-gray uppercase font-bold text-center text-base mb-4"
  }, "Resources"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("ul", null, !Object(lodash__WEBPACK_IMPORTED_MODULE_2__["isEmpty"])(project) && project.resources.length > 0 && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components__WEBPACK_IMPORTED_MODULE_5__["ResourcesComponent"], {
    project: project
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
    className: "".concat(react_device_detect__WEBPACK_IMPORTED_MODULE_9__["isMobile"] ? "justify-center" : "", " mt-3 cursor-pointer flex items-center bg-gray-200 block font-bold px-3 py-1 leading-relaxed text-base rounded text-palette-blue-light"),
    onClick: function onClick() {
      return userAction.setState({
        showSocialMedia: true
      });
    }
  }, "Share this project", /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("svg", {
    className: "feather-icon h-4 w-4 ml-2"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("use", {
    xlinkHref: "/assets/svg/feather-sprite.svg#share"
  })))))))), !loading && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components__WEBPACK_IMPORTED_MODULE_5__["AgentDetailsSection"], {
    showAdd: true,
    project: project,
    isProjectLand: isProjectLand
  }), !loading && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components__WEBPACK_IMPORTED_MODULE_5__["BookInspection"], {
    show: showBookingModal,
    toogleShowBookingModal: toogleShowBookingModal,
    dealId: dealId
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components__WEBPACK_IMPORTED_MODULE_5__["SocialMediaShareModal"], null))));
};

var featuredSettings = {
  dots: true,
  arrows: false,
  infinite: true,
  autoplay: true,
  autoplaySpeed: 5000,
  slidesToShow: 3,
  responsive: [{
    breakpoint: 1024,
    settings: {
      slidesToShow: 3,
      slidesToScroll: 3,
      infinite: true,
      dots: true
    }
  }, {
    breakpoint: 600,
    settings: {
      slidesToShow: 2,
      slidesToScroll: 2
    }
  }, {
    breakpoint: 480,
    settings: {
      centerMode: true,
      slidesToShow: 1,
      slidesToScroll: 1
    }
  }]
};
var apartmentSettings = {
  dots: false,
  arrows: false,
  infinite: true,
  autoplay: true,
  autoplaySpeed: 5000,
  slidesToShow: 1,
  responsive: [{
    breakpoint: 1024,
    settings: {
      slidesToShow: 3,
      slidesToScroll: 3,
      infinite: true,
      dots: true
    }
  }, {
    breakpoint: 600,
    settings: {
      slidesToShow: 2,
      slidesToScroll: 2
    }
  }, {
    breakpoint: 480,
    settings: {
      slidesToShow: 1,
      slidesToScroll: 1
    }
  } // You can unslick at a given breakpoint now by adding:
  // settings: "unslick"
  // instead of a settings object
  ]
};
/* harmony default export */ __webpack_exports__["default"] = (BuyingProjectDetailsPage);

/***/ })

}]);