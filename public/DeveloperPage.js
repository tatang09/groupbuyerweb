(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["DeveloperPage"],{

/***/ "./resources/js/helpers/html/htmlHelper.js":
/*!*************************************************!*\
  !*** ./resources/js/helpers/html/htmlHelper.js ***!
  \*************************************************/
/*! exports provided: extracDescription, extractFeatureOrInclusion */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "extracDescription", function() { return extracDescription; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "extractFeatureOrInclusion", function() { return extractFeatureOrInclusion; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);

var extracDescription = function extracDescription(node) {
  var nodes = node;
  var htmlObject = document.createElement("div");
  htmlObject.innerHTML = nodes;
  var features = htmlObject.getElementsByTagName("ul")[0];
  var inclusions = htmlObject.getElementsByTagName("ul")[1];
  var featureHeader = htmlObject.getElementsByTagName("p");

  for (var index = 0; index <= featureHeader.length; index++) {
    var p = featureHeader[index];

    if (p && (p.innerHTML === "<strong>Features:</strong>" || p.innerHTML === "<strong>Inclusions:</strong>")) {
      p.setAttribute("hidden", "true");
    }
  }

  features.remove();
  inclusions.remove();
  return htmlObject.innerHTML;
};
var extractFeatureOrInclusion = function extractFeatureOrInclusion(node, isFeature) {
  var nodes = node;
  var htmlObject = document.createElement("div");
  htmlObject.innerHTML = nodes;
  var ul = htmlObject.getElementsByTagName("ul")[isFeature ? 0 : 1];
  var li = ul.getElementsByTagName("li");
  var arr = [];
  Array.from(li).forEach(function (element) {
    arr.push(element.innerHTML);
  });
  return arr.map(function (feature, index) {
    return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: "flex text-base font-normal",
      key: index
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
      src: "/assets/images/chevron-right.svg",
      className: "w-2 mr-2 mt-1"
    })), feature);
  });
};

/***/ }),

/***/ "./resources/js/pages/DeveloperPage.js":
/*!*********************************************!*\
  !*** ./resources/js/pages/DeveloperPage.js ***!
  \*********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react_device_detect__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react-device-detect */ "./node_modules/react-device-detect/main.js");
/* harmony import */ var react_device_detect__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react_device_detect__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _layouts_users_UserLayout__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../layouts/users/UserLayout */ "./resources/js/layouts/users/UserLayout.js");
/* harmony import */ var _components_base__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../components/_base */ "./resources/js/components/_base/index.js");
/* harmony import */ var _components_base__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(_components_base__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var _components__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../components */ "./resources/js/components/index.js");
/* harmony import */ var _components__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(_components__WEBPACK_IMPORTED_MODULE_4__);
function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _unsupportedIterableToArray(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

function _iterableToArrayLimit(arr, i) { if (typeof Symbol === "undefined" || !(Symbol.iterator in Object(arr))) return; var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }







var DeveloperPage = function DeveloperPage() {
  var _useState = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])(window.innerWidth),
      _useState2 = _slicedToArray(_useState, 2),
      width = _useState2[0],
      setWidth = _useState2[1];

  Object(react__WEBPACK_IMPORTED_MODULE_0__["useEffect"])(function () {
    var handleWindowResize = function handleWindowResize() {
      return setWidth(window.innerWidth);
    };

    window.addEventListener("resize", handleWindowResize);
    return function () {
      return window.removeEventListener("resize", handleWindowResize);
    };
  }, []);
  var devBenefitsArr = [{
    title: "Display current stock",
    paragh: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi ut turpis in velit ultricies dignissim. Morbi viverra."
  }, {
    title: "Raise brand awareness",
    paragh: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi ut turpis in velit ultricies dignissim. Morbi viverra."
  }, {
    title: "Promote opportunities",
    paragh: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi ut turpis in velit ultricies dignissim. Morbi viverra."
  }, {
    title: "Secure cash flow",
    paragh: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi ut turpis in velit ultricies dignissim. Morbi viverra."
  }];
  var buyerBenefitsArr = [{
    title: "Exclusive properties",
    paragh: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi ut turpis in velit ultricies dignissim. Morbi viverra."
  }, {
    title: "Unbeatable price",
    paragh: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi ut turpis in velit ultricies dignissim. Morbi viverra."
  }, {
    title: "Full transparency",
    paragh: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi ut turpis in velit ultricies dignissim. Morbi viverra."
  }, {
    title: "Power of knowledge",
    paragh: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi ut turpis in velit ultricies dignissim. Morbi viverra."
  }];
  var logoPathArr1 = ["/assets/images/static_logos/CompanyLogos1@2x.png", "/assets/images/static_logos/SH_Official_Logo_BOX_RGB@2x.png", "/assets/images/static_logos/CompanyLogos2@2x.png", "/assets/images/static_logos/CompanyLogos4@2x.png"];
  var logoPathArr2 = ["/assets/images/static_logos/CompanyLogos6@2x.png", "/assets/images/static_logos/CompanyLogos7@2x.png", "/assets/images/static_logos/CompanyLogos8@2x.png", "/assets/images/static_logos/CompanyLogos5@2x.png"];

  var displayBenefits = function displayBenefits(arr, titleColor, pColor) {
    return arr.map(function (item, key) {
      return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        key: key
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
        className: "font-bold leading-7 sm:text-2xl text-base ".concat(titleColor)
      }, item.title), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("p", {
        className: "pt-2 ".concat(pColor)
      }, item.paragh));
    });
  };

  var displayLogos = function displayLogos(arr) {
    return arr.map(function (item, key) {
      return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        key: key,
        className: "sm:flex sm:flex-1 sm:items-center sm:justify-center"
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
        src: "".concat(item),
        className: "h-24"
      }));
    });
  };

  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_layouts_users_UserLayout__WEBPACK_IMPORTED_MODULE_2__["default"], null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("section", {
    className: "text-white"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("section", {
    className: "flex bg-palette-black-main flex-col lg:flex-row"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "flex lg:w-2/5",
    style: rowStyles.developerBanner
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "flex hidden items-center justify-center lg:block"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
    src: "/assets/images/_directors/Ben_Daniel_large.png",
    className: "flex lg:ml-12 mt-10 py-16 xl:ml-24"
  }))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "flex flex-col justify-center lg:w-3/4 xl:pl-40 lg:pl-16 p-8 pt-10 ".concat(width >= 1024 ? "2xl:pl-32" : "")
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "lg:pr-20 lg:pt-0 pt-10 xl:pr-32"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "lg:mt-8 w-auto xl:my-8 mt-4"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("p", {
    className: "font-bold leading-7 lg:w-auto text-4xl text-white w-2/3 xl:text-5xl"
  }, "Why we exist"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("p", {
    className: " pt-1 text-white xl:text-xs"
  }, "We believe\u2026 dolor sit amet, consectetur adipiscing elit. Morbi ut turpis in velit ultricies dignissim. Morbi viverra libero quis est porttitor tincidunt. Nunc in metus sit amet felis feugiat tincidunt sit amet quis lectus.")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "lg:mb-3 w-auto xl:mb-8"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("p", {
    className: "leading-6 lg:w-2/5 mb-0 text-base text-palette-purple-main w-3/5 xl:text-2xl xl:w-3/5"
  }, "Twenty years of experience in property"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("p", {
    className: "lg:w-34 mb-0 mb-2 pt-1 sm:mb-0 text-white text-xs"
  }, "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi ut turpis in velit ultricies dignissim. Morbi viverra libero quis est porttitor tincidunt. Nunc in metus sit amet felis feugiat tincidunt sit amet")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "lg:mb-3 w-auto 2xl:mb-8"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("p", {
    className: "leading-6 mb-0 text-base text-palette-purple-main w-10/12 xl:text-2xl"
  }, "Making great business with both agents and developers"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("p", {
    className: "lg:w-34 pt-1 text-white text-xs mb-0"
  }, "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi ut turpis in velit ultricies dignissim. Morbi viverra libero quis est porttitor tincidunt. Nunc in metus sit amet felis feugiat tincidunt sit amet")))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    style: rowStyles.founder,
    className: "lg:w-2/6 rounded-lg lg:hidden block"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "flex flex-col items-center p-8"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "flex sm:justify-center w-full"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("p", {
    className: "mb-2 text-palette-purple-main text-xs"
  }, "BEN DANIEL - FOUNDER")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
    src: "/assets/images/_directors/Ben_Daniel_founder.png",
    className: "h-64 my-3 rounded sm:w-1/2 w-full"
  })))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("section", {
    className: "relative bg-cover",
    style: rowStyles.developerBenefits
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "p-8"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "sm:flex sm:flex-col sm:items-center sm:justify-center"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "flex flex-col hidden items-center justify-center sm:block sm:mb-12 lg:mt-10"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
    className: "font-bold leading-7 text-5xl text-gray-900"
  }, "Benefits for Developers"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("p", {
    className: "leading-3 m-0 pt-2 text-base text-center text-palette-purple-main"
  }, "An alternative to the existing property flatforms.")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "block sm:hidden w-10/12"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
    className: "font-bold leading-7 sm:text-6xl text-4xl text-gray-800"
  }, "Benefits")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "block sm:hidden w-10/12"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
    className: "font-bold leading-7 sm:text-6xl text-4xl text-gray-800"
  }, "for Developers"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("p", {
    className: "pt-2 text-palette-purple-main whitespace-no-wrap"
  }, "An alternative to the existing property flatforms."))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "sm:flex md:mb-16 xl:px-48"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "flex flex-1 hidden sm:block"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
    src: "/assets/images/benefits_for_developers2.png",
    className: "h-auto rounded-lg w-11/12"
  })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "flex flex-1 flex-col md:justify-center sm:mt-4"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "flex flex-col items-center sm:justify-center "
  }, displayBenefits(devBenefitsArr, "text-purple-800", "text-gray-800")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_base__WEBPACK_IMPORTED_MODULE_3__["Button"], {
    className: "bg-palette-purple-main button button-primary font-sans md:w-1/2 rounded undefined w-full md:w-3/4 xl:w-1/2"
  }, "Join Us Now"))))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("section", {
    className: "relative bg-cover",
    style: rowStyles.buyerBenefits
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "p-8"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "sm:mt-10 sm:flex sm:flex-col sm:justify-center sm:items-center"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "flex flex-col hidden items-center justify-center sm:block sm:mb-12"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
    className: "font-bold leading-7 sm:text-6xl text-4xl text-white"
  }, "Benefits for Buyers"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("p", {
    className: "leading-3 m-0 pt-4 text-center text-palette-purple-main text-xs"
  }, "A new and innovative platform to access properties from trusted developers.")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "w-10/12 block sm:hidden"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
    className: "font-bold leading-7 text-4xl text-white"
  }, "Benefits")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "w-10/12 block sm:hidden"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
    className: "font-bold leading-7 text-4xl text-white"
  }, "for Buyers"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("p", {
    className: "pt-2 text-palette-purple-main"
  }, "A new and innovative platform to access properties from trusted developers."))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "sm:flex md:mb-16 xl:px-48"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "flex-1 flex"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "flex flex-col items-center justify-end sm:pb-8"
  }, displayBenefits(buyerBenefitsArr, "text-palette-purple-main", "text-white"))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "flex flex-1 hidden sm:block"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
    src: "/assets/images/benefits_for_buyers2.png",
    className: "h-auto rounded-lg w-10/11"
  }))))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("section", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "flex flex-col items-center justify-center p-8 w-full sm:py-16"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "sm:flex sm:flex-col sm:items-center"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "flex items-center justify-center mb-4 w-full"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
    className: "leading-7 leading-none text-base text-center text-gray-800 w-1/2 mb-3"
  }, "People we are working with")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("p", {
    className: "sm:w-2/5 pt-2 text-center text-gray-800"
  }, "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi ut turpis in velit ultricies dignissim. Morbi viverra libero quis est porttitor tincidunt. Nunc in")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "flex sm:flex-col sm:w-3/4 w-full"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "sm:flex-row flex flex-1 flex-col items-center justify-center w-full sm:mb-12"
  }, displayLogos(logoPathArr1)), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "sm:flex-row flex flex-1 flex-col items-center justify-center w-full"
  }, displayLogos(logoPathArr2))))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components__WEBPACK_IMPORTED_MODULE_4__["Testimonials"], null), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("section", {
    style: rowStyles.letsWorkTogether
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "p-8 pb-0"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "text-center w-full"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
    className: "font-bold leading-7 text-palette-purple-main md:text-base text-xxs"
  }, "LET'S WORK TOGETHER")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "flex items-center justify-center mb-4 w-full"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
    className: "leading-7 leading-none mb-3 md:text-4xl md:w-1/2 text-base text-center text-white w-7/12 xl:w-1/3"
  }, "Join and let's discuss on how we can collaborate"))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "md:flex md:flex-col md:px-48 p-8 pt-0 pb-2 sm:px-16 items-center"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components__WEBPACK_IMPORTED_MODULE_4__["AgentDetails"], null)), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "flex md:py-12 px-8 py-8 sm:items-center sm:justify-center"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_base__WEBPACK_IMPORTED_MODULE_3__["Button"], {
    className: "bg-palette-purple-main button button-primary font-sans lg:w-1/5 rounded sm:w-1/2 text-xs undefined w-full"
  }, "Let's work together")))));
};

var rowStyles = {
  developerBanner: {
    background: "url('/assets/images/developers_banner_3.png')",
    // marginTop: isMobile ? -162 : -130,
    minHeight: "100%",
    backgroundBlendMode: "multiply",
    backgroundSize: "cover"
  },
  buyerBenefits: _defineProperty({
    background: "url('/assets/images/buyer_benefits.png')",
    backgroundSize: "100%",
    backgroundPosition: "center",
    backgroundRepeat: "no-repeat",
    minHeight: "100%"
  }, "backgroundSize", "contain"),
  founder: {
    background: "url('/assets/images/developers_banner_3.png')",
    backgroundSize: "cover",
    backgroundPosition: "center",
    backgroundRepeat: "no-repeat"
  },
  letsWorkTogether: {
    background: "url('/assets/images/lets_work_together.png')",
    minHeight: "100%",
    backgroundBlendMode: "multiply",
    backgroundSize: "cover"
  },
  developerBenefits: {
    background: "url('/assets/images/developer_benefits.png')",
    // marginTop: isMobile ? -162 : -130,
    minHeight: "100%",
    backgroundBlendMode: "multiply",
    backgroundSize: "cover",
    backgroundRepeat: "no-repeat"
  },
  developerBenefits2: {
    background: "url('/assets/images/benefits_for_developers2.png')",
    // marginTop: isMobile ? -162 : -130,
    minHeight: "100%",
    backgroundSize: "cover",
    backgroundRepeat: "no-repeat"
  }
};
/* harmony default export */ __webpack_exports__["default"] = (DeveloperPage);

/***/ }),

/***/ "./resources/js/pages/NewBuyingApartmentDetailsPage.js":
/*!*************************************************************!*\
  !*** ./resources/js/pages/NewBuyingApartmentDetailsPage.js ***!
  \*************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime/regenerator */ "./node_modules/@babel/runtime/regenerator/index.js");
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _layouts_users_UserLayout__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../layouts/users/UserLayout */ "./resources/js/layouts/users/UserLayout.js");
/* harmony import */ var _states_userGlobal__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../states/userGlobal */ "./resources/js/states/userGlobal.js");
/* harmony import */ var _components__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../components */ "./resources/js/components/index.js");
/* harmony import */ var _components__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(_components__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var _services_auth__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../services/auth */ "./resources/js/services/auth.js");
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! lodash */ "./node_modules/lodash/lodash.js");
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(lodash__WEBPACK_IMPORTED_MODULE_6__);
/* harmony import */ var react_helmet__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! react-helmet */ "./node_modules/react-helmet/es/Helmet.js");
/* harmony import */ var _helpers_metaHelper_metaHelper__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../helpers/metaHelper/metaHelper */ "./resources/js/helpers/metaHelper/metaHelper.js");
/* harmony import */ var react_currency_format__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! react-currency-format */ "./node_modules/react-currency-format/lib/currency-format.js");
/* harmony import */ var react_currency_format__WEBPACK_IMPORTED_MODULE_9___default = /*#__PURE__*/__webpack_require__.n(react_currency_format__WEBPACK_IMPORTED_MODULE_9__);
/* harmony import */ var _helpers_html_htmlHelper__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ../helpers/html/htmlHelper */ "./resources/js/helpers/html/htmlHelper.js");


function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _unsupportedIterableToArray(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

function _iterableToArrayLimit(arr, i) { if (typeof Symbol === "undefined" || !(Symbol.iterator in Object(arr))) return; var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }












var NewBuyingApartmentDetailsPage = function NewBuyingApartmentDetailsPage(_ref) {
  var dealId = _ref.dealId,
      propertyId = _ref.propertyId;

  var _useState = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(true),
      _useState2 = _slicedToArray(_useState, 2),
      loading = _useState2[0],
      setLoading = _useState2[1];

  var _UserGlobal = Object(_states_userGlobal__WEBPACK_IMPORTED_MODULE_3__["default"])(),
      _UserGlobal2 = _slicedToArray(_UserGlobal, 2),
      userState = _UserGlobal2[0],
      userAction = _UserGlobal2[1];

  var _useState3 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(),
      _useState4 = _slicedToArray(_useState3, 2),
      apartment = _useState4[0],
      setApartment = _useState4[1];

  var _useState5 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(false),
      _useState6 = _slicedToArray(_useState5, 2),
      showBookingModal = _useState6[0],
      setShowBookingModal = _useState6[1];

  var _useState7 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(),
      _useState8 = _slicedToArray(_useState7, 2),
      project = _useState8[0],
      setProject = _useState8[1];

  var getDeal = /*#__PURE__*/function () {
    var _ref2 = _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee() {
      var deal, exp, newApartment, _yield$axios$get, data, newProject;

      return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee$(_context) {
        while (1) {
          switch (_context.prev = _context.next) {
            case 0:
              _context.next = 2;
              return axios.get("/api/deal/".concat(dealId));

            case 2:
              deal = _context.sent;

              if (deal.data.sub_property_id == 4) {
                setIsProjectLand(true);
              }

              if (deal.data && deal.data.expires_at) {
                exp = JSON.parse(deal.data.expires_at);
                setTimeLeft(exp.time_limit);
              }

              newApartment = null;

              if (!Object(_services_auth__WEBPACK_IMPORTED_MODULE_5__["isLoggedIn"])()) {
                _context.next = 19;
                break;
              }

              _context.next = 9;
              return axios.get("/api/wish-list/".concat(dealId), {
                headers: {
                  Authorization: "Bearer " + Object(_services_auth__WEBPACK_IMPORTED_MODULE_5__["isLoggedIn"])()
                }
              });

            case 9:
              _yield$axios$get = _context.sent;
              data = _yield$axios$get.data;
              newProject = Object.assign({}, deal.data);

              if (data && !Object(lodash__WEBPACK_IMPORTED_MODULE_6__["isEmpty"])(newProject)) {
                data.map(function (d) {
                  newProject.properties.map(function (p) {
                    if (p.id == d.property_id) {
                      p.hasWishlist = true;
                      p.wishListId = d.id;
                    }
                  });
                });
              }

              newApartment = newProject.properties.find(function (p) {
                return p.id == propertyId;
              });

              if (!Object(lodash__WEBPACK_IMPORTED_MODULE_6__["isEmpty"])(newApartment)) {
                setApartment(newApartment);
              }

              setProject(newProject);
              setLoading(false);
              _context.next = 23;
              break;

            case 19:
              newApartment = deal.data.properties.find(function (p) {
                return p.id == propertyId;
              });
              setApartment(newApartment);
              setProject(deal.data);
              setLoading(false);

            case 23:
            case "end":
              return _context.stop();
          }
        }
      }, _callee);
    }));

    return function getDeal() {
      return _ref2.apply(this, arguments);
    };
  }();

  var handleWishlist = /*#__PURE__*/function () {
    var _ref3 = _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee2(property) {
      var formData;
      return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee2$(_context2) {
        while (1) {
          switch (_context2.prev = _context2.next) {
            case 0:
              if (Object(_services_auth__WEBPACK_IMPORTED_MODULE_5__["isLoggedIn"])()) {
                _context2.next = 3;
                break;
              }

              userAction.setState({
                showSignIn: true
              });
              return _context2.abrupt("return");

            case 3:
              formData = new FormData();
              formData.append("property_id", property.id);
              formData.append("deal_id", dealId);
              _context2.prev = 6;

              if (property.hasWishlist) {
                _context2.next = 12;
                break;
              }

              _context2.next = 10;
              return axios.post("/api/wish-list", formData, {
                headers: {
                  Authorization: "Bearer " + Object(_services_auth__WEBPACK_IMPORTED_MODULE_5__["isLoggedIn"])()
                }
              });

            case 10:
              _context2.next = 14;
              break;

            case 12:
              _context2.next = 14;
              return axios["delete"]("/api/wish-list/".concat(property.wishListId), {
                headers: {
                  Authorization: "Bearer " + Object(_services_auth__WEBPACK_IMPORTED_MODULE_5__["isLoggedIn"])()
                }
              });

            case 14:
              getDeal();
              _context2.next = 19;
              break;

            case 17:
              _context2.prev = 17;
              _context2.t0 = _context2["catch"](6);

            case 19:
            case "end":
              return _context2.stop();
          }
        }
      }, _callee2, null, [[6, 17]]);
    }));

    return function handleWishlist(_x) {
      return _ref3.apply(this, arguments);
    };
  }();

  Object(react__WEBPACK_IMPORTED_MODULE_1__["useEffect"])(function () {
    getDeal();
  }, [userState.user]);

  var handleClose = function handleClose() {
    setShow(false);
  };

  var featuredImagesMapper = function featuredImagesMapper() {
    if (loading) return;
  };

  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_layouts_users_UserLayout__WEBPACK_IMPORTED_MODULE_2__["default"], null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(react_helmet__WEBPACK_IMPORTED_MODULE_7__["Helmet"], null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("meta", {
    name: "description",
    content: _helpers_metaHelper_metaHelper__WEBPACK_IMPORTED_MODULE_8__["metaHelper"].desc
  })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("section", {
    className: "text-black bg-white"
  }, " ", /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "block"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    style: {
      backgroundImage: "linear-gradient(rgba(0, 0, 0, 0), rgba(0, 0, 0, 0.7)), url('/assets/images/Interior 4.jpg')"
    },
    className: "flex items-end w-full text-white bg-left bg-no-repeat bg-cover lg:h-full xs:h-screen"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "block w-full"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "w-full block lg:pt-56"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "w-full flex justify-between lg:pt-32 lg:pb-24 xs:pt-16"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("button", {
    className: "rounded w-auto py-5 lg:px-12 xs:px-6"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("img", {
    src: "/assets/images/left-chevron-white.svg",
    className: "transform -rotate-90 lg:w-10 xs:w-6"
  }))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("button", {
    className: "rounded w-auto py-5 lg:px-12 xs:px-6"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("img", {
    src: "/assets/images/right-chevron-white.svg",
    className: "transform -rotate-90 lg:w-10 xs:w-6"
  }))))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "w-full lg:px-16 lg:pt-32 xs:pt-56 lg:pb-2 xs:px-8 xs:pb-8"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
    className: "font-bold leading-none text-6xl xs:text-5xl"
  }, "Save $111,111"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("p", {
    className: "font-semibold leading-none text-palette-purple-main text-3xl xs:text-2xl"
  }, !loading && project.discount, "% off")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "xs:hidden lg:block w-full block pb-6"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "w-full flex justify-center space-x-2"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("button", {
    className: "rounded w-auto p-2"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("img", {
    src: "/assets/images/checkbox-circle-purple.svg",
    className: "transform -rotate-90 lg:w-4"
  }))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("button", {
    className: "rounded w-auto p-2"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("img", {
    src: "/assets/images/checkbox-circle-gray.svg",
    className: "transform -rotate-90 lg:w-4"
  }))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("button", {
    className: "rounded w-auto p-2"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("img", {
    src: "/assets/images/checkbox-circle-gray.svg",
    className: "transform -rotate-90 lg:w-4"
  }))))))))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("section", {
    className: "text-black bg-white"
  }, " ", /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "block lg:pt-24 lg:pb-8 lg:w-1/2 lg:mx-auto xs:w-full xs:px-12 xs:pt-8 xs:pb-2 space-y-8"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "flex w-full"
  }, " ", /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "block w-3/4 space-y-1"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
    className: "font-bold leading-none text-palette-purple-main lg:text-4xl xs:text-xl"
  }, !loading && project.name, " / Unit ", !loading && apartment.unit_name), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("p", {
    className: "italic font-light leading-none lg:text-lg xs:text-sm text-gray-500"
  }, "Estimated completion: November 2021")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "block w-1/4"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "flex w-full justify-end items-center space-x-3"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "flex items-center text-3xl font-semibold"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("img", {
    src: "/assets/images/heart.svg",
    className: "lg:w-10 xs:w-6"
  }))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "flex items-center text-3xl font-semibold"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("img", {
    src: "/assets/images/share.svg",
    className: "lg:w-10 xs:w-6"
  })))))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "flex w-full"
  }, " ", /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "block w-2/5"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
    className: "font-bold leading-none lg:text-5xl xs:text-2xl"
  }, !loading && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(react_currency_format__WEBPACK_IMPORTED_MODULE_9___default.a, {
    value: apartment.price,
    displayType: "text",
    thousandSeparator: true,
    prefix: "$"
  }))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "block w-3/5"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "flex w-full justify-end items-center"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "flex items-center font-bold border-r lg:pr-6 lg:text-3xl xs:pr-2 xs:text-lg border-palette-purple-main"
  }, !loading && apartment.no_of_bedrooms, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("img", {
    src: "/assets/images/bed.svg",
    className: "lg:w-10 xs:w-6 lg:ml-4 xs:ml-2"
  }))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "flex items-center font-bold border-r lg:px-6 lg:text-3xl xs:px-2 xs:text-lg border-palette-purple-main"
  }, !loading && apartment.no_of_bathrooms, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("img", {
    src: "/assets/images/shower.svg",
    className: "lg:w-10 xs:w-6 lg:ml-4 xs:ml-2"
  }))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "flex items-center font-bold lg:pl-6 lg:text-3xl xs:pl-2 xs:text-lg"
  }, !loading && apartment.no_of_garages, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("img", {
    src: "/assets/images/car.svg",
    className: "lg:w-10 xs:w-6 lg:ml-4 xs:ml-2"
  })))))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "block w-full space-y-4"
  }, " ", /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "flex w-auto space-x-4"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "font-normal leading-none text-base"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
    className: "font-bold"
  }, "Internal:"), " ", "50m", /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
    className: "align-top text-xxs"
  }, "2")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "font-normal leading-none text-base"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
    className: "font-bold"
  }, "External:"), " ", "10m", /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
    className: "align-top text-xxs"
  }, "2"))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "block w-full space-y-4"
  }, !loading && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "text-base description-list pb-4",
    dangerouslySetInnerHTML: {
      __html: !loading && Object(_helpers_html_htmlHelper__WEBPACK_IMPORTED_MODULE_10__["extracDescription"])(apartment.description)
    }
  }))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "lg:flex xs:block w-full lg:space-x-4 xs:space-y-8"
  }, " ", /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "block lg:w-1/2 xs:w-full lg:space-y-3 xs:space-y-2"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("p", {
    className: "font-bold lg:pb-2 leading-none lg:text-4xl xs:text-2xl"
  }, "Features"), !loading && Object(_helpers_html_htmlHelper__WEBPACK_IMPORTED_MODULE_10__["extractFeatureOrInclusion"])(apartment.description, true)), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "block lg:w-1/2 xs:w-full lg:space-y-3 xs:space-y-2"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("p", {
    className: "font-bold lg:pb-2 leading-none lg:text-4xl xs:text-2xl"
  }, "Inclusions"), !loading && Object(_helpers_html_htmlHelper__WEBPACK_IMPORTED_MODULE_10__["extractFeatureOrInclusion"])(apartment.description, false))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "block w-full space-y-4"
  }, " ", /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "block w-full space-y-4"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "lg:flex xs:block w-full lg:space-x-4 xs:space-y-4"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "h-auto lg:w-1/2 xs:w-full"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    style: {
      backgroundImage: "url('/assets/images/Interior 1.jpg')"
    },
    className: "items-end bg-cover bg-center bg-no-repeat"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "lg:py-32 xs:py-24"
  }))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "h-auto lg:w-1/2 xs:w-full"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    style: {
      backgroundImage: "url('/assets/images/Interior 2.jpg')"
    },
    className: "items-end bg-cover bg-center bg-no-repeat"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "lg:py-32 xs:py-24"
  })))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "lg:flex xs:block w-full lg:space-x-4 xs:space-y-4"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "h-auto lg:w-1/2 xs:w-full"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    style: {
      backgroundImage: "url('/assets/images/Interior 3.jpg')"
    },
    className: "items-end bg-cover bg-center bg-no-repeat"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "lg:py-32 xs:py-24"
  }))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "h-auto lg:w-1/2 xs:w-full"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    style: {
      backgroundImage: "url('/assets/images/Interior 4.jpg')"
    },
    className: "items-end bg-cover bg-center bg-no-repeat"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "lg:py-32 xs:py-24"
  })))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "lg:flex xs:block w-full lg:space-x-4 xs:space-y-4"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "h-auto lg:w-1/2 xs:w-full"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    style: {
      backgroundImage: "url('/assets/images/Interior 5.jpg')"
    },
    className: "items-end bg-cover bg-center bg-no-repeat"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "lg:py-32 xs:py-24"
  }))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "h-auto lg:w-1/2 xs:w-full"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    style: {
      backgroundImage: "url('/assets/images/Interior 6.jpg')"
    },
    className: "items-end bg-cover bg-center bg-no-repeat"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "lg:py-32 xs:py-24"
  }))))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "block w-full"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("p", {
    className: "italic font-light lg:text-base xs:text-xs text-gray-700"
  }, "*Property images are for illustration purposes only and may not be specific to your particular property. Property sizes and outdoing costs are estimates only."))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "block w-full space-y-2"
  }, " ", /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "block w-full"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("button", {
    className: "rounded text-white bg-palette-purple-main lg:text-2xl xs:text-sm w-full font-bold py-5 px-3"
  }, "Secure this deal now for only $1,000")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "block w-full"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("button", {
    className: "rounded text-palette-purple-main lg:text-2xl xs:text-sm w-full font-bold py-5 px-3"
  }, "Back to project details"))))), !loading && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components__WEBPACK_IMPORTED_MODULE_4__["GoogleMapsNew"], {
    project: project
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("section", {
    className: "text-black bg-white"
  }, " ", /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "lg:flex xs:block w-full h-auto"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components__WEBPACK_IMPORTED_MODULE_4__["InspectProperty"], null), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components__WEBPACK_IMPORTED_MODULE_4__["TalkExpert"], null))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components__WEBPACK_IMPORTED_MODULE_4__["TheMore"], {
    bgColor: "#4D0B98"
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components__WEBPACK_IMPORTED_MODULE_4__["JoinUs"], null), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components__WEBPACK_IMPORTED_MODULE_4__["SocialMediaShareModal"], null));
};

/* harmony default export */ __webpack_exports__["default"] = (NewBuyingApartmentDetailsPage);

/***/ })

}]);