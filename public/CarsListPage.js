(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["CarsListPage"],{

/***/ "./resources/js/pages/CarsListPage.js":
/*!********************************************!*\
  !*** ./resources/js/pages/CarsListPage.js ***!
  \********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime/regenerator */ "./node_modules/@babel/runtime/regenerator/index.js");
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! lodash */ "./node_modules/lodash/lodash.js");
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(lodash__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _layouts_users_UserLayout__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../layouts/users/UserLayout */ "./resources/js/layouts/users/UserLayout.js");
/* harmony import */ var _states_userGlobal__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../states/userGlobal */ "./resources/js/states/userGlobal.js");
/* harmony import */ var _components__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../components */ "./resources/js/components/index.js");
/* harmony import */ var _components__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(_components__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var _components_base__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../components/_base */ "./resources/js/components/_base/index.js");
/* harmony import */ var _components_base__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(_components_base__WEBPACK_IMPORTED_MODULE_6__);
/* harmony import */ var _services_auth__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../services/auth */ "./resources/js/services/auth.js");
/* harmony import */ var _material_ui_core__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @material-ui/core */ "./node_modules/@material-ui/core/esm/index.js");
/* harmony import */ var react_device_detect__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! react-device-detect */ "./node_modules/react-device-detect/main.js");
/* harmony import */ var react_device_detect__WEBPACK_IMPORTED_MODULE_9___default = /*#__PURE__*/__webpack_require__.n(react_device_detect__WEBPACK_IMPORTED_MODULE_9__);
/* harmony import */ var react_currency_format__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! react-currency-format */ "./node_modules/react-currency-format/lib/currency-format.js");
/* harmony import */ var react_currency_format__WEBPACK_IMPORTED_MODULE_10___default = /*#__PURE__*/__webpack_require__.n(react_currency_format__WEBPACK_IMPORTED_MODULE_10__);
/* harmony import */ var react_select__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! react-select */ "./node_modules/react-select/dist/react-select.browser.esm.js");
/* harmony import */ var react_helmet__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! react-helmet */ "./node_modules/react-helmet/es/Helmet.js");
/* harmony import */ var _helpers_metaHelper_metaHelper__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ../helpers/metaHelper/metaHelper */ "./resources/js/helpers/metaHelper/metaHelper.js");
function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }



function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _unsupportedIterableToArray(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

function _iterableToArrayLimit(arr, i) { if (typeof Symbol === "undefined" || !(Symbol.iterator in Object(arr))) return; var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }















var CarsListPage = function CarsListPage() {
  var _React$createElement;

  var _UserGlobal = Object(_states_userGlobal__WEBPACK_IMPORTED_MODULE_4__["default"])(),
      _UserGlobal2 = _slicedToArray(_UserGlobal, 2),
      userState = _UserGlobal2[0],
      userAction = _UserGlobal2[1];

  var _useState = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(""),
      _useState2 = _slicedToArray(_useState, 2),
      keyword = _useState2[0],
      setKeyword = _useState2[1];

  var _useState3 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(null),
      _useState4 = _slicedToArray(_useState3, 2),
      popper = _useState4[0],
      setPopper = _useState4[1];

  var _useState5 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(false),
      _useState6 = _slicedToArray(_useState5, 2),
      openSearch = _useState6[0],
      setOpenSearch = _useState6[1];

  var _useState7 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(false),
      _useState8 = _slicedToArray(_useState7, 2),
      toggleFetch = _useState8[0],
      setToggleFetch = _useState8[1];

  var _useState9 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(false),
      _useState10 = _slicedToArray(_useState9, 2),
      priceDropdown = _useState10[0],
      setPriceDropdown = _useState10[1];

  var _useState11 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])([]),
      _useState12 = _slicedToArray(_useState11, 2),
      projectData = _useState12[0],
      setProjectData = _useState12[1];

  var _useState13 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])([]),
      _useState14 = _slicedToArray(_useState13, 2),
      options = _useState14[0],
      setOptions = _useState14[1];

  var _useState15 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])([]),
      _useState16 = _slicedToArray(_useState15, 2),
      priceOptions = _useState16[0],
      setPriceOptions = _useState16[1];

  var _useState17 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])({
    sortBy: {
      value: "deals.created_at",
      order: "desc",
      label: "Most Recent"
    },
    minPrice: {
      value: 0,
      label: "Any"
    },
    maxPrice: {
      value: 15000000,
      label: "Any"
    },
    types: "All",
    selectedTypes: {
      value: "All",
      label: "All"
    },
    locations: "All",
    selectedLocations: {
      value: "All",
      label: "All"
    }
  }),
      _useState18 = _slicedToArray(_useState17, 2),
      filters = _useState18[0],
      setFilters = _useState18[1];

  var anchorRef = Object(react__WEBPACK_IMPORTED_MODULE_1__["useRef"])(null);
  var prices = [50000, 100000, 150000, 200000, 250000, 300000, 350000, 400000, 450000, 500000, 550000, 600000, 650000, 700000, 750000, 800000, 850000, 900000, 950000, 1000000, 1100000, 1200000, 1300000, 1400000, 1500000, 1600000, 1700000, 1800000, 1900000, 2000000, 2100000, 2200000, 2300000, 2400000, 2500000, 2600000, 2700000, 2800000, 2900000, 3000000, 4000000, 5000000, 6000000, 7000000, 8000000, 9000000, 10000000, 11000000, 12000000, 13000000, 14000000, 15000000];
  Object(react__WEBPACK_IMPORTED_MODULE_1__["useEffect"])(function () {
    if (localStorage.getItem("__use_projects_filters")) {
      var savedFilters = JSON.parse(localStorage.getItem("__projects_filters"));
      setFilters(savedFilters);
      setToggleFetch(!toggleFetch);
      localStorage.setItem("__use_projects_filters", "");
    }

    var getType = /*#__PURE__*/function () {
      var _ref = _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee() {
        var _yield$axios$get, data, $typeOptions;

        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                _context.next = 2;
                return axios.get("/api/sub-property-type");

              case 2:
                _yield$axios$get = _context.sent;
                data = _yield$axios$get.data;
                $typeOptions = data.map(function (type) {
                  return {
                    value: type.id,
                    label: type.name
                  };
                });
                $typeOptions.unshift({
                  value: "All",
                  label: "All"
                });
                setOptions($typeOptions);

              case 7:
              case "end":
                return _context.stop();
            }
          }
        }, _callee);
      }));

      return function getType() {
        return _ref.apply(this, arguments);
      };
    }();

    var getPriceOptions = function getPriceOptions() {
      var options = [];
      prices.map(function (price) {
        options.push({
          value: price,
          label: new Intl.NumberFormat("en-AU", {
            style: "currency",
            currency: "AUD",
            minimumFractionDigits: 0
          }).format(price || 0)
        });
      });
      setPriceOptions(options);
    };

    getType();
    getPriceOptions();
  }, []);

  var handleSortBy = function handleSortBy(selectedItem) {
    setFilters(_objectSpread(_objectSpread({}, filters), {}, {
      sortBy: selectedItem
    }));
    setToggleFetch(!toggleFetch);
  };

  var handleMinPrice = function handleMinPrice(selectedItem) {
    if (!selectedItem) {
      setFilters(_objectSpread(_objectSpread({}, filters), {}, {
        minPrice: {
          value: 0,
          label: "Any"
        }
      }));
      setToggleFetch(!toggleFetch);
      return;
    }

    setFilters(_objectSpread(_objectSpread({}, filters), {}, {
      minPrice: selectedItem
    }));
    setToggleFetch(!toggleFetch);
  };

  var handleMaxPrice = function handleMaxPrice(selectedItem) {
    if (!selectedItem) {
      setFilters(_objectSpread(_objectSpread({}, filters), {}, {
        maxPrice: {
          value: 15000000,
          label: "Any"
        }
      }));
      setToggleFetch(!toggleFetch);
      return;
    }

    setFilters(_objectSpread(_objectSpread({}, filters), {}, {
      maxPrice: selectedItem
    }));
    setToggleFetch(!toggleFetch);
  };

  var handleSelectType = function handleSelectType(selectedItem) {
    if (!selectedItem || selectedItem.length === 0) {
      setFilters(_objectSpread(_objectSpread({}, filters), {}, {
        types: "All",
        selectedTypes: {
          value: "All",
          label: "All"
        }
      }));
      setToggleFetch(!toggleFetch);
      return;
    }

    var types = [];
    var lastSelected = selectedItem[selectedItem.length - 1];
    var filteredTypes = selectedItem.filter(function (item) {
      return item.value !== "All";
    });

    if (lastSelected.value === "All") {
      setFilters(_objectSpread(_objectSpread({}, filters), {}, {
        types: lastSelected.value,
        selectedTypes: lastSelected
      }));
      setToggleFetch(!toggleFetch);
      return;
    }

    filteredTypes.map(function (type) {
      types.push(type.value);
    });
    setFilters(_objectSpread(_objectSpread({}, filters), {}, {
      types: types,
      selectedTypes: filteredTypes
    }));
    setToggleFetch(!toggleFetch);
  };

  var handleSelectLocation = function handleSelectLocation(selectedItem) {
    if (!selectedItem || selectedItem.length === 0) {
      setFilters(_objectSpread(_objectSpread({}, filters), {}, {
        locations: "All",
        selectedLocations: {
          value: "All",
          label: "All"
        }
      }));
      setToggleFetch(!toggleFetch);
      return;
    }

    var locations = [];
    var lastSelected = selectedItem[selectedItem.length - 1];
    var filteredLocations = selectedItem.filter(function (item) {
      return item.value !== "All";
    });

    if (lastSelected.value === "All") {
      setFilters(_objectSpread(_objectSpread({}, filters), {}, {
        locations: lastSelected.value,
        selectedLocations: lastSelected
      }));
      setToggleFetch(!toggleFetch);
      return;
    }

    filteredLocations.map(function (city) {
      locations.push(city.value);
    });
    setFilters(_objectSpread(_objectSpread({}, filters), {}, {
      locations: locations,
      selectedLocations: filteredLocations
    }));
    setToggleFetch(!toggleFetch);
  };

  var handleSearch = Object(lodash__WEBPACK_IMPORTED_MODULE_2__["debounce"])(function (value) {
    setKeyword(value);
  }, 800);

  var handleSelectProject = function handleSelectProject(url) {
    if (!Object(_services_auth__WEBPACK_IMPORTED_MODULE_7__["isLoggedIn"])()) {
      userAction.setState({
        showSignIn: true
      });
      return;
    }

    localStorage.setItem("__projects_filters", JSON.stringify(filters));
    window.location = url;
  };

  var renderProjects = function renderProjects() {
    return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("tbody", {
      className: "flex flex-wrap"
    }, projectData && projectData.map(function (project, key) {
      return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("tr", {
        key: key,
        className: "lg:w-4/12 mb-8 lg:mb-4 lg:px-3 w-full"
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("td", {
        style: {
          boxShadow: "0 5px 28px 4px rgba(0,0,0,0.1)"
        },
        className: "block bg-white rounded-md lg:hover:scale-105 duration-300 transform transition-all p-0"
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
        className: "cursor-pointer",
        onClick: function onClick() {
          return handleSelectProject("/weekly-deals/".concat(project.id));
        }
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
        className: "bg-cover bg-center rounded-t-md relative text-white",
        style: {
          minHeight: react_device_detect__WEBPACK_IMPORTED_MODULE_9__["isMobile"] ? 250 : 300,
          background: "linear-gradient(rgba(0, 0, 0, 0.1) 0%, rgba(0, 0, 0, 0.2) 80%, rgb(0, 0, 0) 100%), url(\"/storage/projects/".concat(project.featured_images[0], "\")")
        }
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
        className: "absolute top-0 left-0 mt-6"
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
        className: "bg-red-700 flex items-center px-5"
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
        className: "font-semibold text-2xl mr-3"
      }, "".concat(project.discount, "%")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
        className: "font-semibold text-base uppercase"
      }, "Discount"))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
        className: "text-2xl absolute bottom-0 left-0 mb-3 ml-4 font-bold w-56 leading-none"
      }, project.name), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
        className: "absolute bottom-0 right-0 mb-4 mr-4 flex items-center"
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("svg", {
        className: "feather-icon text-palette-purple mr-1"
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("use", {
        xlinkHref: "/assets/svg/feather-sprite.svg#map-pin"
      })), project.address.suburbs + " " + project.address.state))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
        className: "pt-3 pb-4 px-5"
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
        className: "flex items-center justify-between border-b pb-2 mb-2"
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
        className: "text-base font-semibold"
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(react_currency_format__WEBPACK_IMPORTED_MODULE_10___default.a, {
        value: project.properties.reduce(function (a, b) {
          return b["price"] < a ? b["price"] : a;
        }, project.properties[0].price),
        displayType: "text",
        thousandSeparator: true,
        prefix: "$"
      }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", null, " - "), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(react_currency_format__WEBPACK_IMPORTED_MODULE_10___default.a, {
        value: project.properties.reduce(function (a, b) {
          return b["price"] > a ? b["price"] : a;
        }, project.properties[0].price),
        displayType: "text",
        thousandSeparator: true,
        prefix: "$"
      })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
        className: "\n                      text-base font-semibold\n                      ".concat(project.properties.filter(function (property) {
          return property.is_secured === false;
        }).length < 2 ? "text-red-700" : "", "\n                    ")
      }, project.properties.filter(function (property) {
        return property.is_secured === false;
      }).length !== 0 ? project.properties.filter(function (property) {
        return property.is_secured === false;
      }).length : "", project.properties.filter(function (property) {
        return property.is_secured === false;
      }).length > 1 ? " Available" : project.properties.filter(function (property) {
        return property.is_secured === false;
      }).length === 0 ? "Sold out" : " Remaining")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
        className: "flex items-center justify-between"
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
        className: "text-base"
      }, project.deal_type), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
        className: "text-base flex"
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
        className: "cursor-pointer",
        onClick: function onClick() {
          return userAction.setState({
            showSocialMedia: true
          });
        }
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("svg", {
        className: "feather-icon h-5 w-5"
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("use", {
        xlinkHref: "/assets/svg/feather-sprite.svg#share"
      }))))))));
    }));
  };

  var renderFilters = function renderFilters() {
    return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
      className: "\n        lg:flex-row lg:px-16 lg:m-0\n        flex-col flex mt-8 mb-10 text-gray-600 px-8 items-center justify-center relative\n      "
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
      className: "flex-1 mb-4 lg:mb-0 w-full",
      style: {
        maxWidth: react_device_detect__WEBPACK_IMPORTED_MODULE_9__["isMobile"] ? "100%" : "25%"
      }
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
      className: "lg:block uppercase font-bold px-3 text-xs lg:absolute",
      style: {
        top: "-24px"
      }
    }, "Location"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
      className: "border-2 w-full h-full flex items-center"
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(react_select__WEBPACK_IMPORTED_MODULE_11__["default"], {
      isMulti: true,
      isClearable: false,
      closeMenuOnSelect: false,
      hideSelectedOptions: false,
      isSearchable: false,
      options: [{
        value: "All",
        label: "All"
      }, {
        value: "Adelaide",
        label: "Adelaide"
      }, {
        value: "Brisbane",
        label: "Brisbane"
      }, {
        value: "Gold Coast",
        label: "Gold Coast"
      }, {
        value: "Melbourne",
        label: "Melbourne"
      }, {
        value: "Newcastle",
        label: "Newcastle"
      }, {
        value: "Sydney",
        label: "Sydney"
      }],
      classNamePrefix: "input-select",
      className: "gb-multi-select h-full w-full",
      placeholder: "Select Location",
      value: filters.selectedLocations,
      onChange: handleSelectLocation
    }))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
      className: "flex-1 mb-4 lg:mb-0 w-full",
      style: {
        maxWidth: react_device_detect__WEBPACK_IMPORTED_MODULE_9__["isMobile"] ? "100%" : "25%"
      }
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
      className: "lg:block uppercase font-bold px-3 text-xs lg:absolute",
      style: {
        top: "-24px"
      }
    }, "Type"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
      className: "border-2 lg:border-l-0 w-full h-full flex items-center"
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(react_select__WEBPACK_IMPORTED_MODULE_11__["default"], {
      isMulti: true,
      isClearable: false,
      closeMenuOnSelect: false,
      hideSelectedOptions: false,
      isSearchable: false,
      options: options,
      classNamePrefix: "input-select",
      className: "gb-multi-select h-full w-full",
      placeholder: "Select Type",
      value: filters.selectedTypes,
      onChange: handleSelectType
    }))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
      className: "relative flex-1 mb-4 lg:mb-0 w-full",
      style: {
        maxWidth: react_device_detect__WEBPACK_IMPORTED_MODULE_9__["isMobile"] ? "100%" : "25%"
      }
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
      className: "lg:block uppercase font-bold px-3 text-xs lg:absolute",
      style: {
        top: "-24px"
      }
    }, "Price"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
      className: "border-2 lg:border-l-0 w-full h-full flex items-center cursor-pointer relative",
      onClick: function onClick() {
        return setPriceDropdown(!priceDropdown);
      }
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
      className: "px-3 py-2 text-base text-gray-500 w-full flex items-center justify-between"
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
      className: "flex items-center",
      style: {
        height: 36
      }
    }, filters.minPrice.label, " ~ ", filters.maxPrice.label), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("svg", {
      className: "feather-icon opacity-50 hover:opacity-100 ".concat(priceDropdown ? "opacity-100" : ""),
      style: {
        color: "hsl(0, 0%, 60%)",
        strokeWidth: "3px",
        width: 18,
        height: 18
      }
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("use", {
      xlinkHref: "/assets/svg/feather-sprite.svg#chevron-down"
    })))), priceDropdown && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_material_ui_core__WEBPACK_IMPORTED_MODULE_8__["ClickAwayListener"], {
      onClickAway: function onClickAway() {
        return setPriceDropdown(!priceDropdown);
      }
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
      className: "absolute bg-white border-2 p-2 z-50",
      style: {
        top: "calc(100% - 2px)",
        width: react_device_detect__WEBPACK_IMPORTED_MODULE_9__["isMobile"] ? "100%" : "calc(100% + 2px)",
        left: react_device_detect__WEBPACK_IMPORTED_MODULE_9__["isMobile"] ? 0 : "-2px"
      }
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
      className: "flex items-center mb-2"
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
      className: "font-bold mr-1 w-16"
    }, "Min Price"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(react_select__WEBPACK_IMPORTED_MODULE_11__["default"], {
      hideSelectedOptions: false,
      isSearchable: false,
      isClearable: true,
      options: priceOptions,
      classNamePrefix: "input-select",
      className: "gb-price-select flex-1 ml-2",
      placeholder: "Min Price",
      defaultValue: filters.minPrice,
      onChange: handleMinPrice
    })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
      className: "flex items-center"
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
      className: "font-bold mr-1 w-16"
    }, "Max Price"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(react_select__WEBPACK_IMPORTED_MODULE_11__["default"], {
      hideSelectedOptions: false,
      isSearchable: false,
      isClearable: true,
      options: priceOptions,
      classNamePrefix: "input-select",
      className: "gb-price-select flex-1 ml-2",
      placeholder: "Max Price",
      defaultValue: filters.maxPrice,
      onChange: handleMaxPrice
    }))))), !react_device_detect__WEBPACK_IMPORTED_MODULE_9__["isMobile"] && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
      className: "flex-1 w-full",
      style: {
        maxWidth: react_device_detect__WEBPACK_IMPORTED_MODULE_9__["isMobile"] ? "100%" : "25%"
      }
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
      className: "hidden lg:block uppercase font-bold px-3 text-xs absolute",
      style: {
        top: "-24px"
      }
    }, "Sort By"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
      className: "border-2 lg:border-l-0 w-full h-full flex items-center"
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(react_select__WEBPACK_IMPORTED_MODULE_11__["default"], {
      hideSelectedOptions: false,
      isSearchable: false,
      options: [{
        value: "deals.created_at",
        order: "desc",
        label: "Most Recent"
      }, {
        value: "incentives.discount",
        order: "desc",
        label: "Biggest Discount"
      }, {
        value: "deals.expires_at",
        order: "asc",
        label: "Closing Soon"
      }, {
        value: "asc_properties_total_price",
        order: "asc",
        label: "Price (low-high)"
      }, {
        value: "desc_properties_total_price",
        order: "desc",
        label: "Price (high-low)"
      }],
      classNamePrefix: "input-select",
      className: "gb-multi-select h-full w-full",
      placeholder: "Sort By",
      value: filters.sortBy,
      onChange: handleSortBy
    }))));
  };

  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_layouts_users_UserLayout__WEBPACK_IMPORTED_MODULE_3__["default"], null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(react_helmet__WEBPACK_IMPORTED_MODULE_12__["Helmet"], null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("meta", {
    name: "description",
    content: _helpers_metaHelper_metaHelper__WEBPACK_IMPORTED_MODULE_13__["metaHelper"].desc
  })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("section", {
    className: "bg-palette-blue-dark",
    style: {
      marginTop: -130,
      paddingTop: 160
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("section", {
    className: "relative bg-white"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components__WEBPACK_IMPORTED_MODULE_5__["BuyingProgress"], null), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "pb-16 px-6 lg:px-0 mx-auto",
    style: {
      maxWidth: 1366
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("h1", {
    className: "text-left lg:text-center font-bold leading-tight text-4xl mt-10 mb-8 lg:mb-16"
  }, "Cars Inventory List"), react_device_detect__WEBPACK_IMPORTED_MODULE_9__["isMobile"] && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "flex items-center justify-between"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    onClick: function onClick() {
      return setOpenSearch(!openSearch);
    },
    className: "uppercase font-bold text-palette-gray flex items-center"
  }, "Refine Search", /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("svg", {
    className: "\n                    ml-1 transition-all duration-300 feather-icon text-palette-gray transform\n                    ".concat(openSearch ? "rotate-45" : "", "\n                  ")
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("use", {
    xlinkHref: "/assets/svg/feather-sprite.svg#plus"
  }))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    ref: anchorRef,
    onClick: function onClick(e) {
      return setPopper(popper ? null : e.currentTarget);
    },
    className: "uppercase font-bold text-palette-gray flex items-center"
  }, filters.sortBy.label, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("svg", {
    className: "\n                    ml-1 transition-all duration-300 feather-icon text-palette-gray transform\n                    ".concat(popper ? "rotate-180" : "", "\n                  ")
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("use", {
    xlinkHref: "/assets/svg/feather-sprite.svg#chevron-down"
  }))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_material_ui_core__WEBPACK_IMPORTED_MODULE_8__["Popper"], {
    open: Boolean(popper),
    anchorEl: popper,
    placement: "bottom-end",
    transition: true,
    disablePortal: true,
    className: "z-50 menu-list"
  }, function (_ref2) {
    var TransitionProps = _ref2.TransitionProps;
    return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_material_ui_core__WEBPACK_IMPORTED_MODULE_8__["Grow"], _extends({}, TransitionProps, {
      style: {
        transformOrigin: "right top"
      }
    }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_material_ui_core__WEBPACK_IMPORTED_MODULE_8__["Paper"], {
      elevation: 2,
      className: "relative"
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_material_ui_core__WEBPACK_IMPORTED_MODULE_8__["MenuList"], null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_material_ui_core__WEBPACK_IMPORTED_MODULE_8__["MenuItem"], {
      onClick: function onClick() {
        setFilters(_objectSpread(_objectSpread({}, filters), {}, {
          sortBy: {
            value: "deals.created_at",
            order: "desc",
            label: "Most Recent"
          }
        }));
        setToggleFetch(!toggleFetch);
        setPopper(null);
      }
    }, "Most Recent"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_material_ui_core__WEBPACK_IMPORTED_MODULE_8__["MenuItem"], {
      onClick: function onClick() {
        setFilters(_objectSpread(_objectSpread({}, filters), {}, {
          sortBy: {
            value: "incentives.discount",
            order: "desc",
            label: "Biggest Discount"
          }
        }));
        setToggleFetch(!toggleFetch);
        setPopper(null);
      }
    }, "Biggest Discount"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_material_ui_core__WEBPACK_IMPORTED_MODULE_8__["MenuItem"], {
      onClick: function onClick() {
        setFilters(_objectSpread(_objectSpread({}, filters), {}, {
          sortBy: {
            value: "deals.expires_at",
            order: "asc",
            label: "Closing Soon"
          }
        }));
        setToggleFetch(!toggleFetch);
        setPopper(null);
      }
    }, "Closing Soon"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_material_ui_core__WEBPACK_IMPORTED_MODULE_8__["MenuItem"], {
      onClick: function onClick() {
        setFilters(_objectSpread(_objectSpread({}, filters), {}, {
          sortBy: {
            value: "properties_total_price",
            order: "asc",
            label: "Price (low-high)"
          }
        }));
        setToggleFetch(!toggleFetch);
        setPopper(null);
      }
    }, "Price (low-high)"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_material_ui_core__WEBPACK_IMPORTED_MODULE_8__["MenuItem"], {
      onClick: function onClick() {
        setFilters(_objectSpread(_objectSpread({}, filters), {}, {
          sortBy: {
            value: "properties_total_price",
            order: "desc",
            label: "Price (high-low)"
          }
        }));
        setToggleFetch(!toggleFetch);
        setPopper(null);
      }
    }, "Price (high-low)"))));
  })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_material_ui_core__WEBPACK_IMPORTED_MODULE_8__["Collapse"], {
    "in": react_device_detect__WEBPACK_IMPORTED_MODULE_9__["isMobile"] ? openSearch : true
  }, renderFilters()), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "lg:px-16 mt-4 lg:mt-24"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components_base__WEBPACK_IMPORTED_MODULE_6__["Table"], (_React$createElement = {
    query: "/api/deal",
    queryParams: "&projects=true&types=".concat(filters.types, "&locations=").concat(filters.locations, "&min_price=").concat(filters.minPrice.value, "&max_price=").concat(filters.maxPrice.value) // queryParams={`&projects=true&types=${filters.types}`}
    ,
    toggleFetch: toggleFetch
  }, _defineProperty(_React$createElement, "toggleFetch", toggleFetch), _defineProperty(_React$createElement, "keyword", keyword), _defineProperty(_React$createElement, "getData", setProjectData), _defineProperty(_React$createElement, "content", renderProjects()), _defineProperty(_React$createElement, "sort", filters.sortBy.value.replace(/asc_|desc_/g, "") || ""), _defineProperty(_React$createElement, "order", filters.sortBy.order || ""), _React$createElement)))))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components__WEBPACK_IMPORTED_MODULE_5__["SocialMediaShareModal"], null));
};

/* harmony default export */ __webpack_exports__["default"] = (CarsListPage);

/***/ })

}]);