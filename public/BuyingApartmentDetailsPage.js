(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["BuyingApartmentDetailsPage"],{

/***/ "./resources/js/components/adComponents/AdContainerComponent.js":
/*!**********************************************************************!*\
  !*** ./resources/js/components/adComponents/AdContainerComponent.js ***!
  \**********************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _states_userGlobal__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../states/userGlobal */ "./resources/js/states/userGlobal.js");
function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _unsupportedIterableToArray(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

function _iterableToArrayLimit(arr, i) { if (typeof Symbol === "undefined" || !(Symbol.iterator in Object(arr))) return; var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }

function _objectWithoutProperties(source, excluded) { if (source == null) return {}; var target = _objectWithoutPropertiesLoose(source, excluded); var key, i; if (Object.getOwnPropertySymbols) { var sourceSymbolKeys = Object.getOwnPropertySymbols(source); for (i = 0; i < sourceSymbolKeys.length; i++) { key = sourceSymbolKeys[i]; if (excluded.indexOf(key) >= 0) continue; if (!Object.prototype.propertyIsEnumerable.call(source, key)) continue; target[key] = source[key]; } } return target; }

function _objectWithoutPropertiesLoose(source, excluded) { if (source == null) return {}; var target = {}; var sourceKeys = Object.keys(source); var key, i; for (i = 0; i < sourceKeys.length; i++) { key = sourceKeys[i]; if (excluded.indexOf(key) >= 0) continue; target[key] = source[key]; } return target; }




var AdContainerComponent = function AdContainerComponent(_ref) {
  var src = _ref.src,
      className = _ref.className,
      rest = _objectWithoutProperties(_ref, ["src", "className"]);

  var _useState = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])(""),
      _useState2 = _slicedToArray(_useState, 2),
      img = _useState2[0],
      setImage = _useState2[1];

  var _UserGlobal = Object(_states_userGlobal__WEBPACK_IMPORTED_MODULE_1__["default"])(),
      _UserGlobal2 = _slicedToArray(_UserGlobal, 2),
      userState = _UserGlobal2[0],
      seState = _UserGlobal2[1]; // useEffect(() => {
  //   if (userState.windowSize > 0) {
  //     mapAddImage();
  //   }
  // }, []);
  // const mapAddImage = () => {
  //   if (isMobile) {
  //     setImage(`/assets/images/Fenero_MREC.gif`);
  //   } else {
  //     if (userState.windowSize <= deviceSize.standardScreen) {
  //       setImage(`/assets/images/Fenero_Leaderboard.gif`);
  //     } else {
  //       setImage(`/assets/images/Fenero_Half_Page.gif`);
  //     }
  //   }
  // };


  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
    className: "".concat(className, " w-full"),
    src: src
  }));
};

/* harmony default export */ __webpack_exports__["default"] = (AdContainerComponent);

/***/ }),

/***/ "./resources/js/pages/BuyingApartmentDetailsPage.js":
/*!**********************************************************!*\
  !*** ./resources/js/pages/BuyingApartmentDetailsPage.js ***!
  \**********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime/regenerator */ "./node_modules/@babel/runtime/regenerator/index.js");
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _layouts_users_UserLayout__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../layouts/users/UserLayout */ "./resources/js/layouts/users/UserLayout.js");
/* harmony import */ var _states_userGlobal__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../states/userGlobal */ "./resources/js/states/userGlobal.js");
/* harmony import */ var _components__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../components */ "./resources/js/components/index.js");
/* harmony import */ var _components__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(_components__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var _services_auth__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../services/auth */ "./resources/js/services/auth.js");
/* harmony import */ var _fortawesome_react_fontawesome__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @fortawesome/react-fontawesome */ "./node_modules/@fortawesome/react-fontawesome/index.es.js");
/* harmony import */ var react_device_detect__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! react-device-detect */ "./node_modules/react-device-detect/main.js");
/* harmony import */ var react_device_detect__WEBPACK_IMPORTED_MODULE_7___default = /*#__PURE__*/__webpack_require__.n(react_device_detect__WEBPACK_IMPORTED_MODULE_7__);
/* harmony import */ var react_currency_format__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! react-currency-format */ "./node_modules/react-currency-format/lib/currency-format.js");
/* harmony import */ var react_currency_format__WEBPACK_IMPORTED_MODULE_8___default = /*#__PURE__*/__webpack_require__.n(react_currency_format__WEBPACK_IMPORTED_MODULE_8__);
/* harmony import */ var react_date_countdown_timer__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! react-date-countdown-timer */ "./node_modules/react-date-countdown-timer/build/index.js");
/* harmony import */ var react_date_countdown_timer__WEBPACK_IMPORTED_MODULE_9___default = /*#__PURE__*/__webpack_require__.n(react_date_countdown_timer__WEBPACK_IMPORTED_MODULE_9__);
/* harmony import */ var react_slick__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! react-slick */ "./node_modules/react-slick/lib/index.js");
/* harmony import */ var react_slick__WEBPACK_IMPORTED_MODULE_10___default = /*#__PURE__*/__webpack_require__.n(react_slick__WEBPACK_IMPORTED_MODULE_10__);
/* harmony import */ var _components_base__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ../components/_base */ "./resources/js/components/_base/index.js");
/* harmony import */ var _components_base__WEBPACK_IMPORTED_MODULE_11___default = /*#__PURE__*/__webpack_require__.n(_components_base__WEBPACK_IMPORTED_MODULE_11__);
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! lodash */ "./node_modules/lodash/lodash.js");
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_12___default = /*#__PURE__*/__webpack_require__.n(lodash__WEBPACK_IMPORTED_MODULE_12__);
/* harmony import */ var _components_adComponents_AdContainerComponent__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ../components/adComponents/AdContainerComponent */ "./resources/js/components/adComponents/AdContainerComponent.js");
/* harmony import */ var _components_OutgoingCosts__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ../components/OutgoingCosts */ "./resources/js/components/OutgoingCosts.js");
/* harmony import */ var _components_TotalSize__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! ../components/TotalSize */ "./resources/js/components/TotalSize.js");
/* harmony import */ var _components_LandProjectSize__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! ../components/LandProjectSize */ "./resources/js/components/LandProjectSize.js");


function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _unsupportedIterableToArray(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

function _iterableToArrayLimit(arr, i) { if (typeof Symbol === "undefined" || !(Symbol.iterator in Object(arr))) return; var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }


















var BuyingApartmentDetailsPage = function BuyingApartmentDetailsPage(_ref) {
  var dealId = _ref.dealId,
      propertyId = _ref.propertyId,
      isPreview = _ref.isPreview;

  var _UserGlobal = Object(_states_userGlobal__WEBPACK_IMPORTED_MODULE_3__["default"])(),
      _UserGlobal2 = _slicedToArray(_UserGlobal, 2),
      userState = _UserGlobal2[0],
      userAction = _UserGlobal2[1];

  var _useState = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(false),
      _useState2 = _slicedToArray(_useState, 2),
      show = _useState2[0],
      setShow = _useState2[1];

  var _useState3 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(true),
      _useState4 = _slicedToArray(_useState3, 2),
      loading = _useState4[0],
      setLoading = _useState4[1];

  var _useState5 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(),
      _useState6 = _slicedToArray(_useState5, 2),
      project = _useState6[0],
      setProject = _useState6[1];

  var _useState7 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(),
      _useState8 = _slicedToArray(_useState7, 2),
      apartment = _useState8[0],
      setApartment = _useState8[1];

  var _useState9 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(isPreview === "true"),
      _useState10 = _slicedToArray(_useState9, 2),
      isPrev = _useState10[0],
      seIsPrev = _useState10[1];

  var _useState11 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(""),
      _useState12 = _slicedToArray(_useState11, 2),
      timeleft = _useState12[0],
      setTimeLeft = _useState12[1];

  var _useState13 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(false),
      _useState14 = _slicedToArray(_useState13, 2),
      showBookingModal = _useState14[0],
      setShowBookingModal = _useState14[1];

  var _useState15 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(false),
      _useState16 = _slicedToArray(_useState15, 2),
      isProjectLand = _useState16[0],
      setIsProjectLand = _useState16[1];

  var toogleShowBookingModal = function toogleShowBookingModal() {
    setShowBookingModal(!showBookingModal);
  };

  var getDeal = /*#__PURE__*/function () {
    var _ref2 = _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee() {
      var deal, exp, newApartment, _yield$axios$get, data, newProject;

      return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee$(_context) {
        while (1) {
          switch (_context.prev = _context.next) {
            case 0:
              _context.next = 2;
              return axios.get("/api/deal/".concat(dealId));

            case 2:
              deal = _context.sent;

              if (deal.data.sub_property_id == 4) {
                setIsProjectLand(true);
              }

              if (deal.data && deal.data.expires_at) {
                exp = JSON.parse(deal.data.expires_at);
                setTimeLeft(exp.time_limit);
              }

              newApartment = null;

              if (!(Object(_services_auth__WEBPACK_IMPORTED_MODULE_5__["isLoggedIn"])() && !Object(lodash__WEBPACK_IMPORTED_MODULE_12__["isEmpty"])(userState.user))) {
                _context.next = 19;
                break;
              }

              _context.next = 9;
              return axios.get("/api/wish-list/".concat(dealId), {
                headers: {
                  Authorization: "Bearer " + Object(_services_auth__WEBPACK_IMPORTED_MODULE_5__["isLoggedIn"])()
                }
              });

            case 9:
              _yield$axios$get = _context.sent;
              data = _yield$axios$get.data;
              newProject = Object.assign({}, deal.data);

              if (data && !Object(lodash__WEBPACK_IMPORTED_MODULE_12__["isEmpty"])(newProject)) {
                data.map(function (d) {
                  newProject.properties.map(function (p) {
                    if (p.id == d.property_id) {
                      p.hasWishlist = true;
                      p.wishListId = d.id;
                    }
                  });
                });
              }

              newApartment = newProject.properties.find(function (p) {
                return p.id == propertyId;
              });

              if (!Object(lodash__WEBPACK_IMPORTED_MODULE_12__["isEmpty"])(newApartment)) {
                setApartment(newApartment);
              }

              setProject(newProject);
              setLoading(false);
              _context.next = 23;
              break;

            case 19:
              newApartment = deal.data.properties.find(function (p) {
                return p.id == propertyId;
              });
              setApartment(newApartment);
              setProject(deal.data);
              setLoading(false);

            case 23:
            case "end":
              return _context.stop();
          }
        }
      }, _callee);
    }));

    return function getDeal() {
      return _ref2.apply(this, arguments);
    };
  }();

  var handleWishlist = /*#__PURE__*/function () {
    var _ref3 = _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee2(property) {
      var formData;
      return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee2$(_context2) {
        while (1) {
          switch (_context2.prev = _context2.next) {
            case 0:
              if (Object(_services_auth__WEBPACK_IMPORTED_MODULE_5__["isLoggedIn"])()) {
                _context2.next = 3;
                break;
              }

              userAction.setState({
                showSignIn: true
              });
              return _context2.abrupt("return");

            case 3:
              formData = new FormData();
              formData.append("property_id", property.id);
              formData.append("deal_id", dealId);
              _context2.prev = 6;

              if (property.hasWishlist) {
                _context2.next = 12;
                break;
              }

              _context2.next = 10;
              return axios.post("/api/wish-list", formData, {
                headers: {
                  Authorization: "Bearer " + Object(_services_auth__WEBPACK_IMPORTED_MODULE_5__["isLoggedIn"])()
                }
              });

            case 10:
              _context2.next = 14;
              break;

            case 12:
              _context2.next = 14;
              return axios["delete"]("/api/wish-list/".concat(property.wishListId), {
                headers: {
                  Authorization: "Bearer " + Object(_services_auth__WEBPACK_IMPORTED_MODULE_5__["isLoggedIn"])()
                }
              });

            case 14:
              getDeal();
              _context2.next = 19;
              break;

            case 17:
              _context2.prev = 17;
              _context2.t0 = _context2["catch"](6);

            case 19:
            case "end":
              return _context2.stop();
          }
        }
      }, _callee2, null, [[6, 17]]);
    }));

    return function handleWishlist(_x) {
      return _ref3.apply(this, arguments);
    };
  }();

  var getDescription = function getDescription(node) {
    var nodes = node;
    var htmlObject = document.createElement("div");
    htmlObject.innerHTML = nodes;
    var features = htmlObject.getElementsByTagName("ul")[0];
    var inclusions = htmlObject.getElementsByTagName("ul")[1];
    var featureHeader = htmlObject.getElementsByTagName("p");

    for (var index = 0; index <= featureHeader.length; index++) {
      var p = featureHeader[index];

      if (p && (p.innerHTML === "<strong>Features:</strong>" || p.innerHTML === "<strong>Inclusions:</strong>")) {
        p.setAttribute("hidden", "true");
      }
    }

    features.remove();
    inclusions.remove();
    return htmlObject.innerHTML;
  };

  var getFeatures = function getFeatures(node) {
    var nodes = node;
    var htmlObject = document.createElement("div");
    htmlObject.innerHTML = nodes;
    var features = htmlObject.getElementsByTagName("ul")[0].innerHTML;
    var inclusions = htmlObject.getElementsByTagName("ul")[1].innerHTML;
    return {
      features: features,
      inclusions: inclusions
    };
  };

  Object(react__WEBPACK_IMPORTED_MODULE_1__["useEffect"])(function () {
    // if (!isLoggedIn()) {
    //   window.location = `/weekly-deals`;
    // }
    getDeal();
  }, [userState.user]);

  var _handleClose = function handleClose() {
    setShow(false);
  };

  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_layouts_users_UserLayout__WEBPACK_IMPORTED_MODULE_2__["default"], null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("section", {
    className: "bg-palette-blue-dark",
    style: {
      marginTop: -130,
      paddingTop: 160
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("section", {
    className: "relative bg-white"
  }, !loading && apartment && !apartment.is_secured && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components__WEBPACK_IMPORTED_MODULE_4__["BuyingProgress"], {
    current: 3,
    project_id: dealId,
    isPreview: isPrev
  }), loading && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "w-full bg-palette-purple text-white font-bold text-center p-4 text-xl"
  }, "Loading property details..."), !loading && apartment && apartment.is_secured && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "w-full bg-palette-purple text-white font-bold text-center p-4 text-xl"
  }, "This deal has already been secured."), !react_device_detect__WEBPACK_IMPORTED_MODULE_7__["isMobile"] && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("section", {
    className: "relative z-20"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(react_slick__WEBPACK_IMPORTED_MODULE_10___default.a, featuredSettings, !loading && apartment.featured_images && apartment.featured_images.map(function (image, key) {
    return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("img", {
      key: key,
      src: "".concat(image),
      className: "object-cover carousel-featured"
    });
  }))), !react_device_detect__WEBPACK_IMPORTED_MODULE_7__["isMobile"] && !loading && project.discount && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("section", {
    className: "mt-8 lg:-mt-8 relative text-white z-20 inline-block w-full lg:w-auto"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "flex lg:inline-flex shadow-lg"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "w-1/2 lg:w-auto bg-red-700 flex items-center lg:pl-12 lg:pr-24 px-3 py-1",
    style: {
      clipPath: "polygon(0 0, 100% 0, 80% 100%, 0 100%)"
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
    className: "font-bold lg:text-4xl mr-4 text-2xl"
  }, !loading && "".concat(project.discount, "%")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
    className: "font-bold lg:text-2xl text-lg uppercase"
  }, "Discount")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "w-6/12 lg:w-auto lg:-ml-20 bg-red-800 flex items-center lg:pl-16 px-3 py-1",
    style: {
      clipPath: "polygon(18% 0, 100% 0, 100% 100%, 0 100%)"
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "font-bold leading-none lg:text-4xl mr-4 text-center text-lg uppercase"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", null, "Save")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "font-bold lg:text-4xl text-2xl"
  }, !loading && apartment && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(react_currency_format__WEBPACK_IMPORTED_MODULE_8___default.a, {
    value: apartment.price * project.discount / 100,
    displayType: "text",
    thousandSeparator: true,
    prefix: "$"
  }))))), react_device_detect__WEBPACK_IMPORTED_MODULE_7__["isMobile"] && !loading && project.discount && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("section", {
    className: "mt-8 lg:-mt-8 relative text-white z-20 inline-block w-full lg:w-auto"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "flex lg:inline-flex shadow-lg"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    id: "discountBanner",
    className: "lg:-mr-20 items-center sm:justify-center -mr-16 sm:-mr-32 md:justify-center bg-red-700 flex lg:pl-12 lg:pr-24 lg:w-auto w-full xs:pl-8",
    style: {
      clipPath: "polygon(0 0, 100% 0, 80% 100%, 0 100%)"
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
    className: "font-bold lg:text-4xl mr-4 text-base"
  }, "".concat(project.discount, "%")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
    className: "font-bold lg:text-2xl text-base uppercase"
  }, "Discount")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "bg-red-800 flex items-center lg:-ml-20 lg:pl-20 lg:w-auto pl-12 px-3 py-1 w-full justify-center",
    style: {
      clipPath: "polygon(18% 0, 100% 0, 100% 100%, 0 100%)"
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "font-bold leading-none lg:mr-4 mr-1 text-center uppercasee"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "text-sm lg:text-2xl"
  }, "SAVE"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "text-sm lg:text-2xl"
  }, "UP TO")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "ml-2"
  }, !loading && apartment && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(react_currency_format__WEBPACK_IMPORTED_MODULE_8___default.a, {
    value: apartment.price * project.discount / 100,
    displayType: "text",
    thousandSeparator: true,
    prefix: "$",
    className: "font-bold lg:text-4xl text-lg"
  }))))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("section", {
    className: "flex flex-col lg:flex-row lg:px-10 mt-6 pb-16 lg:pb-24 mx-auto relative",
    style: {
      maxWidth: 1600
    }
  }, !react_device_detect__WEBPACK_IMPORTED_MODULE_7__["isMobile"] && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "lg:w-3/12 w-full"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "relative"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("a", {
    href: "/weekly-deals/".concat(dealId, "/").concat(isPreview),
    className: "bg-white cursor-pointer duration-300 font-bold hover:text-palette-blue inline-flex items-center pr-3 relative text-palette-blue-light text-sm transition-all z-10"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("svg", {
    className: "feather-icon h-4 w-4 mr-1 inline"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("use", {
    xlinkHref: "/assets/svg/feather-sprite.svg#chevron-left"
  })), "Back to project details"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "absolute border-gray-200 border-t w-full",
    style: {
      top: 10
    }
  })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("h1", {
    className: "font-bold lg:mb-2 lg:text-5xl mb-3 mt-2 text-4xl leading-none"
  }, !loading && project.name), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "font-bold text-base"
  }, !loading && project.address && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(react__WEBPACK_IMPORTED_MODULE_1___default.a.Fragment, null, project.address.line_1 ? project.address.line_1 + ", " : "", project.address.line_2 ? project.address.line_2 + ", " : "", project.address.suburbs ? project.address.suburbs + ", " : "", project.address.suburb ? project.address.suburb + ", " : "", project.address.state ? project.address.state + " " : "", project.address.postal ? project.address.postal : "")), !loading && !isProjectLand && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "px-4 py-1 shadow-lg mt-4 bg-red-700 font-bold text-white rounded-lg"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", null, "Time left: "), !loading && timeleft && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(react_date_countdown_timer__WEBPACK_IMPORTED_MODULE_9___default.a, {
    dateTo: timeleft ? timeleft : project.created_at // dateTo={project.expires_at}
    ,
    locales: ["Year, ", "Month, ", "Day, ", "hr, ", "min, ", "sec"],
    locales_plural: ["Years, ", "Months, ", "Days, ", "hrs, ", "min, ", "sec"]
  }) || "-"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "flex"
  }, !Object(lodash__WEBPACK_IMPORTED_MODULE_12__["isEmpty"])(project) && !isProjectLand && !project.is_completed && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components__WEBPACK_IMPORTED_MODULE_4__["EstimatedCompletionDate"], {
    loading: loading,
    project: project
  }), !Object(lodash__WEBPACK_IMPORTED_MODULE_12__["isEmpty"])(project) && project.is_completed && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components__WEBPACK_IMPORTED_MODULE_4__["ReadyForOccupancy"], null), !Object(lodash__WEBPACK_IMPORTED_MODULE_12__["isEmpty"])(project) && project.deposit > 0 && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components__WEBPACK_IMPORTED_MODULE_4__["Deposit"], {
    deposit: project.deposit
  })), !Object(_services_auth__WEBPACK_IMPORTED_MODULE_5__["isLoggedIn"])() ? /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(react__WEBPACK_IMPORTED_MODULE_1___default.a.Fragment, null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "rounded bg-blue-100 p-5 text-center my-4"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "mb-3 text-base"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
    onClick: function onClick() {
      return userAction.setState({
        showSignIn: true
      });
    },
    className: "cursor-pointer font-bold text-palette-purple hover:text-palette-violet"
  }, "Sign in"), "\xA0or\xA0", /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
    onClick: function onClick() {
      return userAction.setState({
        showSignUp: true
      });
    },
    className: "cursor-pointer font-bold text-palette-purple hover:text-palette-violet"
  }, "Sign up"), "\xA0to secure this deal"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
    className: "lg:text-base"
  }, "Secure this deal for only $1,000"))) : /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(react__WEBPACK_IMPORTED_MODULE_1___default.a.Fragment, null, apartment && !apartment.is_secured && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(react__WEBPACK_IMPORTED_MODULE_1___default.a.Fragment, null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "rounded ".concat(isPrev ? "bg-gray-400" : "bg-blue-100", " p-5 text-center my-4")
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "font-bold text-base"
  }, "Yes! I want to"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("button", {
    disabled: isPrev,
    onClick: function onClick() {
      return setShow(true);
    },
    className: "rounded text-base font-bold p-4 ".concat(!isPrev ? "text-white" : "text-gray-400", " w-full text-center my-3 ").concat(isPrev ? "bg-palette-violet" : "bg-palette-purple", " hover:bg-palette-violet transition-all duration-300")
  }, "Secure this deal"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
    className: "lg:text-base"
  }, "Secure this deal for only $1,000")))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "my-4 p-1 rounded text-center"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("button", {
    disabled: isPrev,
    onClick: function onClick() {
      return setShowBookingModal(true);
    },
    className: "rounded text-base font-bold p-2 ".concat(!isPrev ? "text-white" : "text-gray-400", " w-1/2 text-center hover:bg-gray-900 bg-gray-700 transition-all duration-300")
  }, "Book Inspection"))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "relative px-6 lg:px-16 lg:w-7/12",
    style: {
      minHeight: react_device_detect__WEBPACK_IMPORTED_MODULE_7__["isMobile"] ? null : 440
    }
  }, react_device_detect__WEBPACK_IMPORTED_MODULE_7__["isMobile"] && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(react__WEBPACK_IMPORTED_MODULE_1___default.a.Fragment, null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "relative mb-3"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("a", {
    href: "/weekly-deals/".concat(dealId, "/").concat(isPreview),
    className: "bg-white cursor-pointer duration-300 font-bold hover:text-palette-blue inline-flex items-center pr-3 relative text-palette-blue-light text-sm transition-all z-10"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("svg", {
    className: "feather-icon h-4 w-4 mr-1 inline"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("use", {
    xlinkHref: "/assets/svg/feather-sprite.svg#chevron-left"
  })), "Back to project details"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "absolute border-gray-200 border-t w-full",
    style: {
      top: 10
    }
  })), !isPrev && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "flex"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "flex flex-col"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("a", {
    href: "/weekly-deals/".concat(dealId, "/").concat(isPreview),
    className: "font-bold text-2xl opacity-25 underline"
  }, !loading && project.name), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
    className: "font-bold text-2xl opacity-25"
  }, "/", " ", !loading && apartment && "Lot " + (!isProjectLand ? apartment.unit_no : apartment.unit_name) || "")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "font-bold text-2xl lg:text-4xl leading-tight italic"
  }, isProjectLand ? "Lot " : "Apartment", " ", !loading && apartment && apartment.unit_name || ""), !isProjectLand && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "font-bold text-base flex items-center my-3"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
    className: "mr-4 text-lg"
  }, !loading && apartment.no_of_bedrooms || "", /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_fortawesome_react_fontawesome__WEBPACK_IMPORTED_MODULE_6__["FontAwesomeIcon"], {
    icon: ["fas", "bed"],
    className: "text-2xl ml-1"
  })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
    className: "mr-4 text-lg"
  }, !loading && apartment.no_of_bathrooms || "", /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_fortawesome_react_fontawesome__WEBPACK_IMPORTED_MODULE_6__["FontAwesomeIcon"], {
    icon: ["fas", "bath"],
    className: "text-2xl fa-w-20"
  })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
    className: "mr-4 text-lg"
  }, !loading && apartment.no_of_garages || "", /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_fortawesome_react_fontawesome__WEBPACK_IMPORTED_MODULE_6__["FontAwesomeIcon"], {
    icon: ["fas", "car"],
    className: "text-2xl fa-w-20"
  })))))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "flex flex-1 justify-end mr-4"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components_base__WEBPACK_IMPORTED_MODULE_11__["Tooltip"], {
    title: "Share",
    placement: "top"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("svg", {
    className: "feather-icon h-5 w-5 cursor-pointer mr-3",
    onClick: function onClick() {
      return userAction.setState({
        showSocialMedia: true
      });
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("use", {
    xlinkHref: "/assets/svg/feather-sprite.svg#share"
  }))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components_base__WEBPACK_IMPORTED_MODULE_11__["Tooltip"], {
    title: "".concat(!loading && apartment.hasWishlist ? "Already on wishlist" : "Add to wishlist"),
    placement: "top"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("svg", {
    className: "".concat(!loading && apartment.hasWishlist ? "text-red-600" : "text-black", " feather-icon h-5 w-5 cursor-pointer"),
    onClick: function onClick() {
      return handleWishlist(apartment);
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("use", {
    xlinkHref: "/assets/svg/feather-sprite.svg#heart",
    className: "".concat(!loading && apartment.hasWishlist ? "fill-current" : null)
  })))))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "lg:border-b-2 lg:pb-4 lg:mb-4 flex items-center justify-between"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "w-full"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", null, !react_device_detect__WEBPACK_IMPORTED_MODULE_7__["isMobile"] && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "flex flex-col"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "flex"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "flex flex-1"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("a", {
    href: "/weekly-deals/".concat(dealId, "/").concat(isPrev),
    className: "font-bold text-2xl opacity-25 underline"
  }, !loading && project.name), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
    className: "font-bold text-2xl opacity-25"
  }, "/", " ", !loading && !isProjectLand && apartment && "Lot " + apartment.unit_no || "", !loading && apartment && isProjectLand && "Lot " + apartment.unit_name || "")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "flex flex-1 justify-end mr-4"
  }, !react_device_detect__WEBPACK_IMPORTED_MODULE_7__["isMobile"] && !isPrev && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "text-base flex mt-2"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components_base__WEBPACK_IMPORTED_MODULE_11__["Tooltip"], {
    title: "Share",
    placement: "top"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("svg", {
    className: "feather-icon h-5 w-5 cursor-pointer mr-3",
    onClick: function onClick() {
      return userAction.setState({
        showSocialMedia: true
      });
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("use", {
    xlinkHref: "/assets/svg/feather-sprite.svg#share"
  }))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components_base__WEBPACK_IMPORTED_MODULE_11__["Tooltip"], {
    title: "".concat(!loading && apartment.hasWishlist ? "Already on wishlist" : "Add to wishlist"),
    placement: "top"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("svg", {
    className: "".concat(!loading && apartment.hasWishlist ? "text-red-600" : "text-black", " feather-icon h-5 w-5 cursor-pointer"),
    onClick: function onClick() {
      return handleWishlist(apartment);
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("use", {
    xlinkHref: "/assets/svg/feather-sprite.svg#heart",
    className: "".concat(!loading && apartment.hasWishlist ? "fill-current" : null)
  })))))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "flex"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "flex flex-1 font-bold italic leading-tight lg:text-4xl mt-2 text-2xl"
  }, isProjectLand ? "Lot " : "Apartment", " ", !loading && apartment && apartment.unit_name || ""), !isProjectLand && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "flex flex-1 font-bold items-center justify-end my-3 text-base"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
    className: "mr-4 text-2xl"
  }, !loading && apartment.no_of_bedrooms || "", /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_fortawesome_react_fontawesome__WEBPACK_IMPORTED_MODULE_6__["FontAwesomeIcon"], {
    icon: ["fas", "bed"],
    className: "text-2xl ml-1"
  })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
    className: " mr-4 text-2xl"
  }, !loading && apartment.no_of_bathrooms || "", /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_fortawesome_react_fontawesome__WEBPACK_IMPORTED_MODULE_6__["FontAwesomeIcon"], {
    icon: ["fas", "bath"],
    className: "text-2xl fa-w-20"
  })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
    className: "mr-4 text-2xl"
  }, !loading && apartment.no_of_garages || "", /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_fortawesome_react_fontawesome__WEBPACK_IMPORTED_MODULE_6__["FontAwesomeIcon"], {
    icon: ["fas", "car"],
    className: "text-2xl fa-w-20"
  }))))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "flex"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
    className: "opacity-75 line-through font-bold text-xl lg:text-2xl mr-3"
  }, !loading && apartment && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(react__WEBPACK_IMPORTED_MODULE_1___default.a.Fragment, null, "WAS", " ", /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(react_currency_format__WEBPACK_IMPORTED_MODULE_8___default.a, {
    value: apartment.price || 0,
    displayType: "text",
    thousandSeparator: true,
    prefix: "$"
  }))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
    className: "text-red-700 font-bold text-xl lg:text-2xl"
  }, !loading && apartment && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(react__WEBPACK_IMPORTED_MODULE_1___default.a.Fragment, null, "NOW", " ", /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(react_currency_format__WEBPACK_IMPORTED_MODULE_8___default.a, {
    value: apartment.price - apartment.price * (project.discount / 100),
    displayType: "text",
    thousandSeparator: true,
    prefix: "$"
  }))))), react_device_detect__WEBPACK_IMPORTED_MODULE_7__["isMobile"] && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "flex xs:flex-col"
  }, !Object(lodash__WEBPACK_IMPORTED_MODULE_12__["isEmpty"])(project) && !isProjectLand && !project.is_completed && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components__WEBPACK_IMPORTED_MODULE_4__["EstimatedCompletionDate"], {
    loading: loading,
    project: project
  }), !Object(lodash__WEBPACK_IMPORTED_MODULE_12__["isEmpty"])(project) && project.is_completed && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components__WEBPACK_IMPORTED_MODULE_4__["ReadyForOccupancy"], null), !Object(lodash__WEBPACK_IMPORTED_MODULE_12__["isEmpty"])(project) && project.deposit > 0 && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components__WEBPACK_IMPORTED_MODULE_4__["Deposit"], {
    deposit: project.deposit
  })))), !react_device_detect__WEBPACK_IMPORTED_MODULE_7__["isMobile"] && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(react__WEBPACK_IMPORTED_MODULE_1___default.a.Fragment, null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "text-base description-list border-gray-500 border-b-2 pb-4",
    dangerouslySetInnerHTML: {
      __html: !loading && apartment.description
    }
  }), !loading && !isProjectLand && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components_TotalSize__WEBPACK_IMPORTED_MODULE_15__["default"], {
    apartment: apartment
  }), !loading && isProjectLand && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components_LandProjectSize__WEBPACK_IMPORTED_MODULE_16__["default"], {
    apartment: apartment
  }), !loading && !isProjectLand && apartment.strata_cost > 0 && apartment.water_cost > 0 && apartment.council_cost > 0 && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components_OutgoingCosts__WEBPACK_IMPORTED_MODULE_14__["default"], {
    apartment: apartment
  }), !isProjectLand && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("p", {
    className: "text-sm italic mt-10 "
  }, "*Property images are for illustration purposes only and may not be specific to your particular property. Property sizes and outgoing costs are estimates only."))), react_device_detect__WEBPACK_IMPORTED_MODULE_7__["isMobile"] && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(react__WEBPACK_IMPORTED_MODULE_1___default.a.Fragment, null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("section", {
    className: "lg:mt-0 mt-8 relative z-20"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(react_slick__WEBPACK_IMPORTED_MODULE_10___default.a, featuredSettings, !loading && apartment.featured_images && apartment.featured_images.map(function (image, key) {
    return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
      key: key,
      className: "px-1"
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("img", {
      src: "".concat(image),
      className: "object-cover carousel-featured rounded-lg"
    }));
  }))), react_device_detect__WEBPACK_IMPORTED_MODULE_7__["isMobile"] && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(react__WEBPACK_IMPORTED_MODULE_1___default.a.Fragment, null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "lg:w-3/12 w-full px-16 mt-8"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "rounded bg-blue-100 p-5 text-center my-4"
  }, !Object(_services_auth__WEBPACK_IMPORTED_MODULE_5__["isLoggedIn"])() ? /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(react__WEBPACK_IMPORTED_MODULE_1___default.a.Fragment, null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "mb-3 text-base"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
    onClick: function onClick() {
      return userAction.setState({
        showSignIn: true
      });
    },
    className: "cursor-pointer font-bold text-palette-purple hover:text-palette-violet"
  }, "Sign in"), "\xA0or\xA0", /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
    onClick: function onClick() {
      return userAction.setState({
        showSignUp: true
      });
    },
    className: "cursor-pointer font-bold text-palette-purple hover:text-palette-violet"
  }, "Sign up"), "\xA0to secure this deal"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
    className: "lg:text-base"
  }, "Secure this deal for only $1,000")) : /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(react__WEBPACK_IMPORTED_MODULE_1___default.a.Fragment, null, !loading && apartment && apartment.is_secured ? /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(react__WEBPACK_IMPORTED_MODULE_1___default.a.Fragment, null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "font-bold text-base text-palette-gray"
  }, "Deal already secured for this apartment.")) : /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(react__WEBPACK_IMPORTED_MODULE_1___default.a.Fragment, null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "font-bold text-base"
  }, "Yes! I want to"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("button", {
    onClick: function onClick() {
      return setShow(true);
    },
    className: "rounded text-base font-bold p-4 text-white w-full text-center my-3 bg-palette-purple hover:bg-palette-violet transition-all duration-300"
  }, "Secure this deal"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
    className: "lg:text-base"
  }, "Secure this deal for only $1,000")))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "my-4 p-1 rounded text-center"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("button", {
    disabled: isPrev,
    onClick: function onClick() {
      return setShowBookingModal(true);
    },
    className: "rounded text-base font-bold p-2 ".concat(!isPrev ? "text-white" : "text-gray-400", "  text-center hover:bg-gray-900 bg-gray-700 transition-all duration-300")
  }, "Book Inspection"))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "px-6 mt-8 text-base description-list",
    dangerouslySetInnerHTML: {
      __html: !loading && apartment.description
    }
  }), !loading && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "mx-8"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components_TotalSize__WEBPACK_IMPORTED_MODULE_15__["default"], {
    apartment: apartment
  })), !loading && apartment.strata_cost > 0 && apartment.water_cost > 0 && apartment.council_cost > 0 && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "mx-8"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components_OutgoingCosts__WEBPACK_IMPORTED_MODULE_14__["default"], {
    apartment: apartment
  })), !isProjectLand && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("p", {
    className: "italic px-8 text-sm mt-10"
  }, "*Property images are for illustration purposes only and may not be specific to your particular property. Property sizes and outgoing costs are estimates only."))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "lg:w-2/12 px-16 mt-6 lg:mt-0 lg:p-0"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "text-palette-gray uppercase font-bold text-center lg:text-left text-base mb-4"
  }, "Resources"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("ul", null, !loading && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components__WEBPACK_IMPORTED_MODULE_4__["ResourcesComponent"], {
    project: project,
    floorPlan: apartment.floor_plan
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
    onClick: function onClick() {
      return userAction.setState({
        showSocialMedia: true
      });
    },
    className: "".concat(react_device_detect__WEBPACK_IMPORTED_MODULE_7__["isMobile"] ? "justify-center" : "", " mt-3 cursor-pointer flex items-center bg-gray-200 block font-bold px-3 py-1 leading-relaxed text-base rounded text-palette-blue-light")
  }, "Share this project", /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("svg", {
    className: "feather-icon h-4 w-4 ml-2"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("use", {
    xlinkHref: "/assets/svg/feather-sprite.svg#share"
  })))))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "lg:mb-24 items-center justify-center flex"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("section", {
    className: "flex-1 pt-4 mt-8 px-6 lg:px-0 h-full",
    style: {
      maxWidth: 1366
    }
  })), !loading && project && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components__WEBPACK_IMPORTED_MODULE_4__["AgentDetailsSection"], {
    showAdd: true,
    project: project,
    isProjectLand: isProjectLand
  }), !loading && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components__WEBPACK_IMPORTED_MODULE_4__["BookInspection"], {
    show: showBookingModal,
    toogleShowBookingModal: toogleShowBookingModal,
    propertyId: propertyId,
    dealId: dealId
  }))), !loading && apartment && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components__WEBPACK_IMPORTED_MODULE_4__["SecureDeal"], {
    show: show,
    handleClose: function handleClose() {
      return _handleClose();
    },
    apartment: apartment,
    project: project
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components__WEBPACK_IMPORTED_MODULE_4__["SocialMediaShareModal"], null));
};

var featuredSettings = {
  dots: true,
  arrows: false,
  infinite: true,
  autoplay: true,
  autoplaySpeed: 5000,
  slidesToShow: 3,
  responsive: [{
    breakpoint: 1024,
    settings: {
      slidesToShow: 3,
      slidesToScroll: 3,
      infinite: true,
      dots: true
    }
  }, {
    breakpoint: 600,
    settings: {
      slidesToShow: 2,
      slidesToScroll: 2
    }
  }, {
    breakpoint: 480,
    settings: {
      centerMode: true,
      slidesToShow: 1,
      slidesToScroll: 1
    }
  }]
};
/* harmony default export */ __webpack_exports__["default"] = (BuyingApartmentDetailsPage);

/***/ })

}]);