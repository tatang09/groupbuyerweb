(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["UserAccountSettings"],{

/***/ "./resources/js/pages/UserAccountSettings.js":
/*!***************************************************!*\
  !*** ./resources/js/pages/UserAccountSettings.js ***!
  \***************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime/regenerator */ "./node_modules/@babel/runtime/regenerator/index.js");
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _states_userGlobal__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../states/userGlobal */ "./resources/js/states/userGlobal.js");
/* harmony import */ var _helpers_bootstrap__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../helpers/bootstrap */ "./resources/js/helpers/bootstrap.js");
/* harmony import */ var _components_base__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../components/_base */ "./resources/js/components/_base/index.js");
/* harmony import */ var _components_base__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(_components_base__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var _fortawesome_react_fontawesome__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @fortawesome/react-fontawesome */ "./node_modules/@fortawesome/react-fontawesome/index.es.js");
/* harmony import */ var _material_ui_core_Collapse__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @material-ui/core/Collapse */ "./node_modules/@material-ui/core/esm/Collapse/index.js");
/* harmony import */ var _components_base_alerts_sweetAlert__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../components/_base/alerts/sweetAlert */ "./resources/js/components/_base/alerts/sweetAlert.js");
/* harmony import */ var react_select__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! react-select */ "./node_modules/react-select/dist/react-select.browser.esm.js");
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! lodash */ "./node_modules/lodash/lodash.js");
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_9___default = /*#__PURE__*/__webpack_require__.n(lodash__WEBPACK_IMPORTED_MODULE_9__);
/* harmony import */ var _helpers_userAccountSettingsHelper_userAccountHelper__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ../helpers/userAccountSettingsHelper/userAccountHelper */ "./resources/js/helpers/userAccountSettingsHelper/userAccountHelper.js");
/* harmony import */ var _data_index__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ../data/index */ "./resources/js/data/index.js");
/* harmony import */ var _helpers_countries__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ../helpers/countries */ "./resources/js/helpers/countries.js");
/* harmony import */ var _helpers_propertyHelper_propertyHelper__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ../helpers/propertyHelper/propertyHelper */ "./resources/js/helpers/propertyHelper/propertyHelper.js");
/* harmony import */ var react_helmet__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! react-helmet */ "./node_modules/react-helmet/es/Helmet.js");
/* harmony import */ var _helpers_metaHelper_metaHelper__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! ../helpers/metaHelper/metaHelper */ "./resources/js/helpers/metaHelper/metaHelper.js");
/* harmony import */ var _services_auth__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! ../services/auth */ "./resources/js/services/auth.js");
function _toConsumableArray(arr) { return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _unsupportedIterableToArray(arr) || _nonIterableSpread(); }

function _nonIterableSpread() { throw new TypeError("Invalid attempt to spread non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _iterableToArray(iter) { if (typeof Symbol !== "undefined" && Symbol.iterator in Object(iter)) return Array.from(iter); }

function _arrayWithoutHoles(arr) { if (Array.isArray(arr)) return _arrayLikeToArray(arr); }



function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _unsupportedIterableToArray(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

function _iterableToArrayLimit(arr, i) { if (typeof Symbol === "undefined" || !(Symbol.iterator in Object(arr))) return; var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }






 // import swal from "sweetalert2";












var UserAccountSettings = function UserAccountSettings() {
  var _UserGlobal = Object(_states_userGlobal__WEBPACK_IMPORTED_MODULE_2__["default"])(),
      _UserGlobal2 = _slicedToArray(_UserGlobal, 2),
      userState = _UserGlobal2[0],
      userAction = _UserGlobal2[1];

  var _useState = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(false),
      _useState2 = _slicedToArray(_useState, 2),
      loading = _useState2[0],
      setLoading = _useState2[1];

  var _useState3 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])({}),
      _useState4 = _slicedToArray(_useState3, 2),
      errors = _useState4[0],
      setErrors = _useState4[1];

  var _useState5 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(false),
      _useState6 = _slicedToArray(_useState5, 2),
      isOpen = _useState6[0],
      setIsOpen = _useState6[1];

  var _useState7 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])([]),
      _useState8 = _slicedToArray(_useState7, 2),
      propertyTypes = _useState8[0],
      setPropertyTypes = _useState8[1];

  var _useState9 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(""),
      _useState10 = _slicedToArray(_useState9, 2),
      devId = _useState10[0],
      setDevId = _useState10[1];

  var _useState11 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])([]),
      _useState12 = _slicedToArray(_useState11, 2),
      selectedState = _useState12[0],
      setSelectedState = _useState12[1];

  var _useState13 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])([]),
      _useState14 = _slicedToArray(_useState13, 2),
      selectedCountry = _useState14[0],
      setSelectedCountry = _useState14[1];

  var _useState15 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(false),
      _useState16 = _slicedToArray(_useState15, 2),
      showOtherBankText = _useState16[0],
      setShowOtherBankText = _useState16[1];

  var _useState17 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(""),
      _useState18 = _slicedToArray(_useState17, 2),
      bankAccountName = _useState18[0],
      setBankAccountName = _useState18[1];

  var _useState19 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(""),
      _useState20 = _slicedToArray(_useState19, 2),
      bankBSB = _useState20[0],
      setBankBSB = _useState20[1];

  var _useState21 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(""),
      _useState22 = _slicedToArray(_useState21, 2),
      bankAccountNumber = _useState22[0],
      setBankAccountNumber = _useState22[1];

  var _useState23 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(""),
      _useState24 = _slicedToArray(_useState23, 2),
      userPhone = _useState24[0],
      setUserPhone = _useState24[1];

  var _useState25 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(""),
      _useState26 = _slicedToArray(_useState25, 2),
      devPhone = _useState26[0],
      setDevPhone = _useState26[1];

  var _useState27 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])([{
    value: "All",
    label: "All"
  }]),
      _useState28 = _slicedToArray(_useState27, 2),
      selectedLocation = _useState28[0],
      setSelectedLocation = _useState28[1];

  var _useState29 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])([{
    value: 50000,
    label: 50000
  }]),
      _useState30 = _slicedToArray(_useState29, 2),
      selectedMaxPrice = _useState30[0],
      setMaxPrice = _useState30[1];

  var _useState31 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])([{
    value: 50000,
    label: 50000
  }]),
      _useState32 = _slicedToArray(_useState31, 2),
      selectedMinPrice = _useState32[0],
      setMinPrice = _useState32[1];

  var _useState33 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])([{
    value: "All",
    label: "All"
  }]),
      _useState34 = _slicedToArray(_useState33, 2),
      selectedPropertyType = _useState34[0],
      setSelectedPropertyType = _useState34[1];

  var _useState35 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])([{
    value: "All",
    label: "All"
  }]),
      _useState36 = _slicedToArray(_useState35, 2),
      selectedDescription = _useState36[0],
      setSelectedDescription = _useState36[1];

  var _useState37 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])([]),
      _useState38 = _slicedToArray(_useState37, 2),
      propertyTypeArr = _useState38[0],
      setPropertyTypeArr = _useState38[1];

  var _useState39 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])("/assets/images/profile_default.jpg"),
      _useState40 = _slicedToArray(_useState39, 2),
      profilePic = _useState40[0],
      setProfilePic = _useState40[1];

  var _useState41 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])("/assets/images/no_logo.png"),
      _useState42 = _slicedToArray(_useState41, 2),
      companyPic = _useState42[0],
      setCompanyPic = _useState42[1];

  var form = /*#__PURE__*/Object(react__WEBPACK_IMPORTED_MODULE_1__["createRef"])();
  var profileInput = Object(react__WEBPACK_IMPORTED_MODULE_1__["useRef"])(null);
  var companyInput = Object(react__WEBPACK_IMPORTED_MODULE_1__["useRef"])(null);

  var _useState43 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(""),
      _useState44 = _slicedToArray(_useState43, 2),
      selectedBank = _useState44[0],
      setSelectedBank = _useState44[1];

  var _useState45 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])([]),
      _useState46 = _slicedToArray(_useState45, 2),
      bankList = _useState46[0],
      setBankList = _useState46[1];

  var mapData = function mapData() {
    if (userState.user && userState.user.location) {
      handleDataChange(JSON.parse(userState.user.location), "location");
    }

    if (userState.user && userState.user.min_price) {
      handleDataChange(JSON.parse(userState.user.min_price), "min_price");
    }

    if (userState.user && userState.user.max_price) {
      handleDataChange(JSON.parse(userState.user.max_price), "max_price");
    }

    if (userState.user && userState.user.best_describe) {
      handleDataChange(JSON.parse(userState.user.best_describe), "best_describe");
    }

    if (userState.user && userState.user.propertyType) {
      handleDataChange(JSON.parse(userState.user.propertyType), "propertyType");
    }
  };

  Object(react__WEBPACK_IMPORTED_MODULE_1__["useEffect"])(function () {
    getBanks();
  }, []);
  Object(react__WEBPACK_IMPORTED_MODULE_1__["useEffect"])(function () {
    if (userState.user && userState.user.id) {
      getDeveloperAccount();
    }

    if (userState.user && userState.user.avatar_path) {
      setProfilePic("".concat(userState.user.avatar_path));
    }

    if (userState.user && userState.user.phone) {
      setUserPhone(userState.user["phone"].replace("+61", ""));
    }

    mapData();
  }, [userState.user]);

  var handleClick = function handleClick(isProfile) {
    if (isProfile) {
      profileInput.current.click();
    } else {
      companyInput.current.click();
    }
  };

  var handleFileChange = function handleFileChange(e, isProfile) {
    var imageFile = e.target.files[0];

    if (!imageFile.name.match(/\.(jpg|jpeg|png|gif)$/)) {
      Object(_components_base_alerts_sweetAlert__WEBPACK_IMPORTED_MODULE_7__["sweetAlert"])("error", "Please select valid image.");
      return;
    }

    if (isProfile) {
      setProfilePic(URL.createObjectURL(imageFile));
    } else {
      setCompanyPic(URL.createObjectURL(imageFile));
    }
  };

  var getDeveloperAccount = /*#__PURE__*/function () {
    var _ref = _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee() {
      var _yield$axios$get, data;

      return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee$(_context) {
        while (1) {
          switch (_context.prev = _context.next) {
            case 0:
              _context.next = 2;
              return _helpers_bootstrap__WEBPACK_IMPORTED_MODULE_3__["axios"].get("/api/developer/".concat(userState.user.id));

            case 2:
              _yield$axios$get = _context.sent;
              data = _yield$axios$get.data;
              mapDeveloperFields(data);
              setDevId(data.id || 0);

            case 6:
            case "end":
              return _context.stop();
          }
        }
      }, _callee);
    }));

    return function getDeveloperAccount() {
      return _ref.apply(this, arguments);
    };
  }();

  var getBanks = /*#__PURE__*/function () {
    var _ref2 = _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee2() {
      var url;
      return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee2$(_context2) {
        while (1) {
          switch (_context2.prev = _context2.next) {
            case 0:
              url = "/api/bank";
              _context2.next = 3;
              return Object(_data_index__WEBPACK_IMPORTED_MODULE_11__["fetchBanks"])({
                url: url
              }).then(function (res) {
                var newBankList = _toConsumableArray(res.data.map(function (bank) {
                  return {
                    label: "".concat(bank.name),
                    value: String(bank.id)
                  };
                }));

                newBankList.push({
                  label: "Other",
                  value: "Other"
                });
                setBankList(newBankList);
              });

            case 3:
            case "end":
              return _context2.stop();
          }
        }
      }, _callee2);
    }));

    return function getBanks() {
      return _ref2.apply(this, arguments);
    };
  }();

  var mapDeveloperFields = function mapDeveloperFields(data) {
    if (Object(lodash__WEBPACK_IMPORTED_MODULE_9__["isEmpty"])(data)) return;

    if (data.country) {
      setSelectedCountry({
        value: data.country,
        label: data.country
      });
    }

    if (data.bank_id) {
      var bank = bankList.filter(function (bank) {
        return bank.value == data.bank_id.toString();
      });
      setSelectedBank(bank[0]);
    }

    if (data.state) {
      setSelectedState({
        value: data.state,
        label: data.state
      });
    }

    if (data.company_logo_path) {
      setCompanyPic(data.company_logo_path);
    }

    if (data.bank_account_name) {
      setBankAccountName(data.bank_account_name);
    }

    if (data.bank_bsb) {
      setBankBSB(data.bank_bsb);
    }

    if (data.bank_account_number) {
      setBankAccountNumber(data.bank_account_number);
    }

    Object.keys(_helpers_userAccountSettingsHelper_userAccountHelper__WEBPACK_IMPORTED_MODULE_10__["developerFields"]).forEach(function (key) {
      var k = key.replace("dev_", "");

      if (data.hasOwnProperty(k)) {
        if (k === "phone") {
          _helpers_userAccountSettingsHelper_userAccountHelper__WEBPACK_IMPORTED_MODULE_10__["developerFields"][key].value = data[k] ? data[k].replace("+61", "") : "";
        } else {
          _helpers_userAccountSettingsHelper_userAccountHelper__WEBPACK_IMPORTED_MODULE_10__["developerFields"][key].value = data[k];
        }
      }
    });
  };

  var handleSubmit = /*#__PURE__*/function () {
    var _ref3 = _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee3(e) {
      var errors, formData, phone, dev_phone, url, data;
      return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee3$(_context3) {
        while (1) {
          switch (_context3.prev = _context3.next) {
            case 0:
              e.preventDefault();

              if (!(companyPic.substring(0, 8) === "/assets/" && Object(_services_auth__WEBPACK_IMPORTED_MODULE_16__["userRole"])() !== "customer")) {
                _context3.next = 3;
                break;
              }

              return _context3.abrupt("return", Object(_components_base_alerts_sweetAlert__WEBPACK_IMPORTED_MODULE_7__["sweetAlert"])("warning", "Company profile picture is required."));

            case 3:
              setLoading(true);
              errors = {};
              formData = new FormData(form.current);
              phone = userPhone ? "+61" + userPhone : "";
              formData.append("_method", "PATCH");
              formData.append("best_describe", JSON.stringify(selectedDescription));
              formData.append("location", JSON.stringify(selectedLocation));
              formData.append("max_price", JSON.stringify(selectedMaxPrice));
              formData.append("min_price", JSON.stringify(selectedMinPrice));
              formData.append("propertyType", JSON.stringify(selectedPropertyType));
              formData.append("user_id", userState.user.id);
              formData.append("user_type", userState.user.user_role);
              formData.set("phone", phone);
              formData.append("dev_state", selectedState.value || "");
              formData.append("dev_country", selectedCountry.value || "");
              dev_phone = formData.get("dev_phone") ? "+61" + formData.get("dev_phone") : "";
              formData.set("dev_phone", dev_phone);

              if (!Object(lodash__WEBPACK_IMPORTED_MODULE_9__["isEmpty"])(selectedBank) && selectedBank.value !== "Other") {
                formData.set("bank_id", selectedBank.value);
              }

              formData.append("devId", devId);
              _context3.prev = 22;
              url = "/api/user/".concat(userState.user.id);
              _context3.next = 26;
              return _helpers_bootstrap__WEBPACK_IMPORTED_MODULE_3__["axios"].post(url, formData);

            case 26:
              Object(_components_base_alerts_sweetAlert__WEBPACK_IMPORTED_MODULE_7__["sweetAlert"])("success", "Profile successfully updated.");
              userAction.setState({
                fetch: !userState.fetch
              });
              setLoading(false);
              _context3.next = 37;
              break;

            case 31:
              _context3.prev = 31;
              _context3.t0 = _context3["catch"](22);
              data = _context3.t0.response.data;
              errors = data.errors;
              setLoading(false);

              if (errors && errors.avatar_path) {
                Object(_components_base_alerts_sweetAlert__WEBPACK_IMPORTED_MODULE_7__["sweetAlert"])("error", errors.avatar_path[0]);
                userState.user.avatar_path ? setProfilePic("".concat(userState.user.avatar_path)) : setProfilePic("/assets/images/profile_default.jpg");
              }

            case 37:
              setErrors(errors || {});

            case 38:
            case "end":
              return _context3.stop();
          }
        }
      }, _callee3, null, [[22, 31]]);
    }));

    return function handleSubmit(_x) {
      return _ref3.apply(this, arguments);
    };
  }();

  if (userState.user) {
    Object.keys(_helpers_userAccountSettingsHelper_userAccountHelper__WEBPACK_IMPORTED_MODULE_10__["fields"]).forEach(function (key) {
      if (key === "phone") {
        _helpers_userAccountSettingsHelper_userAccountHelper__WEBPACK_IMPORTED_MODULE_10__["fields"][key].value = userState.user[key] ? userState.user[key].replace("+61", "") : "";
      } else {
        if (key === "userEmail") {
          _helpers_userAccountSettingsHelper_userAccountHelper__WEBPACK_IMPORTED_MODULE_10__["fields"][key].value = userState.user["email"];
        } else {
          _helpers_userAccountSettingsHelper_userAccountHelper__WEBPACK_IMPORTED_MODULE_10__["fields"][key].value = userState.user[key];
        }
      }

      _helpers_userAccountSettingsHelper_userAccountHelper__WEBPACK_IMPORTED_MODULE_10__["fields"][key].id = userState.user[key];
    });
  }

  var userPreferenceDownFields = [{
    key: "best_describe",
    placeholder: "Select Buying Purpose",
    label: "Buying Purpose",
    value: selectedDescription,
    options: _helpers_userAccountSettingsHelper_userAccountHelper__WEBPACK_IMPORTED_MODULE_10__["descriptions"]
  }, {
    label: "Location",
    placeholder: "Select Location",
    key: "location",
    value: selectedLocation,
    options: _helpers_userAccountSettingsHelper_userAccountHelper__WEBPACK_IMPORTED_MODULE_10__["locations"]
  }, {
    label: "Type",
    placeholder: "Select Property Type",
    key: "type",
    value: selectedPropertyType,
    options: _helpers_userAccountSettingsHelper_userAccountHelper__WEBPACK_IMPORTED_MODULE_10__["prop_types"]
  }, {
    label: "Min Price",
    placeholder: "Select Min Price",
    key: "min_price",
    value: selectedMinPrice,
    options: _helpers_userAccountSettingsHelper_userAccountHelper__WEBPACK_IMPORTED_MODULE_10__["prices"]
  }, {
    label: "Max Price",
    placeholder: "Select Max Price",
    key: "max_price",
    value: selectedMaxPrice,
    options: _helpers_userAccountSettingsHelper_userAccountHelper__WEBPACK_IMPORTED_MODULE_10__["prices"]
  }];

  var handleDataChange = function handleDataChange(selectedItems, key) {
    switch (key) {
      case "location":
        if (!selectedItems || selectedItems.length == 0 || selectedItems[selectedItems.length - 1].value === "All") {
          setSelectedLocation([{
            value: "All",
            label: "All"
          }]);
        } else {
          setSelectedLocation(selectedItems.filter(function (item) {
            return item.value !== "All";
          }));
        }

        break;

      case "best_describe":
        if (!selectedItems || selectedItems.length == 0 || selectedItems[selectedItems.length - 1].value === "All") {
          setSelectedDescription([{
            value: "All",
            label: "All"
          }]);
        } else {
          setSelectedDescription(selectedItems.filter(function (item) {
            return item.value !== "All";
          }));
        }

        break;

      case "max_price":
        if (!selectedItems || selectedItems.length == 0) {
          setMaxPrice([{
            value: 50000,
            label: 50000
          }]);
        } else {
          setMaxPrice(selectedItems);
        }

        break;

      case "min_price":
        if (!selectedItems || selectedItems.length == 0) {
          setMinPrice([{
            value: 50000,
            label: 50000
          }]);
        } else {
          setMinPrice(selectedItems);
        }

        break;

      default:
        if (!selectedItems || selectedItems.length == 0 || selectedItems[selectedItems.length - 1].value === "All") {
          setSelectedPropertyType([{
            value: "All",
            label: "All"
          }]);
        } else {
          setSelectedPropertyType(selectedItems.filter(function (item) {
            return item.value !== "All";
          }));
        }

        break;
    }
  };

  var handleChange = function handleChange(key, e) {
    if (key === "dev_country") {
      setSelectedCountry(e);
    } else if (key === "dev_state") {
      setSelectedState(e);
    } else {
      if (e.value === "Other") {
        setShowOtherBankText(true);
      } else {
        setShowOtherBankText(false);
      }

      setSelectedBank(e);
    }
  };

  var dropDownComponent = function dropDownComponent(options, value, placeholder, key) {
    return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(react_select__WEBPACK_IMPORTED_MODULE_8__["default"], {
      isMulti: key === "min_price" || key === "max_price" ? false : true,
      isClearable: false,
      closeMenuOnSelect: false,
      hideSelectedOptions: false,
      isSearchable: false,
      options: options,
      classNamePrefix: "input-select",
      className: "gb-multi-select h-full w-full",
      placeholder: placeholder,
      value: value,
      onChange: function onChange(e) {
        return handleDataChange(e, key);
      }
    });
  };

  var renderDropDown = function renderDropDown() {
    return userPreferenceDownFields.map(function (item) {
      return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
        className: "flex lg:flex-row flex-row mb-5 rounded text-base",
        key: item.key
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("label", {
        className: "capitalize mb-2 mr-4 mt-4 pr-3 w-40 lg:mb-0"
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
        className: "font-bold"
      }, item.label)), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
        className: "border-2 w-full flex items-center"
      }, dropDownComponent(item.options, item.value, item.placeholder, item.key)));
    });
  };

  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(react_helmet__WEBPACK_IMPORTED_MODULE_14__["Helmet"], null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("meta", {
    name: "description",
    content: _helpers_metaHelper_metaHelper__WEBPACK_IMPORTED_MODULE_15__["metaHelper"].desc
  })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("section", {
    className: "bg-palette-blue-dark",
    style: {
      marginTop: -130,
      paddingTop: 160
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("section", {
    className: "relative bg-white"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "pb-16 px-6 lg:px-10 mx-auto",
    style: {
      maxWidth: 1366
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("h1", {
    className: "font-bold leading-tight text-4xl pt-10 pb-8 lg:pb-16 text-center"
  }, "My Profile"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("form", {
    ref: form,
    onSubmit: handleSubmit
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "flex flex-col"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "w-full flex flex-col lg:flex-row lg:pr-16 mt-8 lg:mt-0"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "w-full mb-8 lg:mb-0 lg:w-1/2"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "flex justify-center" //className={`lg:w-4/12`}

  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("input", {
    type: "file",
    name: "avatar_path",
    className: "hidden",
    ref: profileInput,
    onChange: function onChange(e) {
      return handleFileChange(e, true);
    }
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "relative flex justify-center"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "mx-auto flex items-center p-0 overflow-hidden border-4 border-palette-gray h-48 lg:h-56 rounded-full shadow-lg w-48 lg:w-56 ",
    style: {
      width: "fit-content"
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("img", {
    className: "object-cover h-48 w-48 lg:w-56 lg:h-56",
    src: profilePic
  })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
    className: "\n                          absolute bg-white border border-gray-300 bottom-0 cursor-pointer flex h-10 items-center justify-center mb-3 mr-3 right-0\n                          rounded-full shadow-md w-10 transition-all duration-300 hover:bg-palette-purple hover:text-white hover:border-palette-purple\n                        ",
    onClick: function onClick() {
      return handleClick(true);
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("svg", {
    className: "feather-icon"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("use", {
    xlinkHref: "/assets/svg/feather-sprite.svg#edit-2"
  })))))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "w-full lg:w-1/2"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components_base__WEBPACK_IMPORTED_MODULE_4__["Form"], {
    errors: errors,
    formFields: _helpers_userAccountSettingsHelper_userAccountHelper__WEBPACK_IMPORTED_MODULE_10__["fields"]
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "mb-5 text-base flex"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("label", {
    className: "mr-3 w-40 mt-2 capitalize pr-3"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
    className: "font-bold"
  }, "Password")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "w-full"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    onClick: function onClick() {
      return setIsOpen(!isOpen);
    },
    className: "\n                            border border-palette-gray text-palette-gray hover:bg-palette-gray hover:text-white transition-all duration-300\n                            cursor-pointer font-bold inline-block px-6 text-sm py-2 rounded shadow\n                          "
  }, "Change Password"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_material_ui_core_Collapse__WEBPACK_IMPORTED_MODULE_6__["default"], {
    "in": isOpen
  }, isOpen && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "mt-5 text-sm"
  }, "Old Password"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components_base__WEBPACK_IMPORTED_MODULE_4__["TextInput"], {
    type: "password",
    placeholder: "Old Password",
    name: "old_password",
    border: false,
    appearance: true
  }), errors.old_password && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
    className: "text-red-500 text-xs"
  }, errors.old_password[0]), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "mt-5 text-sm"
  }, "New Password"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components_base__WEBPACK_IMPORTED_MODULE_4__["TextInput"], {
    type: "password",
    placeholder: "New Password",
    name: "password",
    border: false,
    appearance: true
  }), errors.password && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
    className: "text-red-500 text-xs"
  }, errors.password[0]), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "mt-5 text-sm"
  }, "Confirm Password"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components_base__WEBPACK_IMPORTED_MODULE_4__["TextInput"], {
    type: "password",
    placeholder: "Confirm Password",
    name: "password_confirmation",
    border: false,
    appearance: true
  }))))), !Object(lodash__WEBPACK_IMPORTED_MODULE_9__["isEmpty"])(userState.user) && userState.user.user_role !== "project_developer" && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "mx-auto",
    style: {
      maxWidth: 1366
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("h1", {
    className: "font-bold leading-tight lg:pb-16 ml-3 pb-8 pt-10 text-3xl text-center"
  }, "My Preferences"), renderDropDown()))), userState.user && userState.user.user_role !== "customer" && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "w-full flex flex-col lg:flex-row"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "w-full lg:w-1/2"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("h1", {
    className: "font-bold leading-tight lg:pb-16 ml-3 pb-8 pt-10 text-3xl text-center"
  }, "Seller Profile"))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "w-full flex flex-col lg:flex-row lg:pr-16 mt-8 lg:mt-0"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "w-full mb-8 lg:mb-0 lg:w-1/2"
  }, userState.user && userState.user.user_role !== "customer" && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "flex justify-center" //className={`lg:w-4/12`}

  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("input", {
    type: "file",
    name: "company_logo_path",
    className: "hidden",
    ref: companyInput,
    onChange: function onChange(e) {
      return handleFileChange(e, false);
    }
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "relative flex justify-center"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "relative mx-auto flex items-center p-0 overflow-hidden border-4 border-palette-gray h-48 lg:h-56 shadow-lg w-48 lg:w-56 ",
    style: {
      width: "fit-content"
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("img", {
    className: "object-cover h-48 w-48 lg:w-56 lg:h-56",
    src: companyPic
  })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
    className: "\n                              absolute bg-white border border-gray-300 bottom-0 cursor-pointer flex h-10 items-center justify-center mb-3 mr-3 right-0\n                              rounded-full shadow-md w-10 transition-all duration-300 hover:bg-palette-purple hover:text-white hover:border-palette-purple\n                            ",
    onClick: function onClick() {
      return handleClick(false);
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("svg", {
    className: "feather-icon"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("use", {
    xlinkHref: "/assets/svg/feather-sprite.svg#edit-2"
  })))))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "w-full lg:w-1/2"
  }, !Object(lodash__WEBPACK_IMPORTED_MODULE_9__["isEmpty"])(userState.user) && userState.user.user_role !== "customer" && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(react__WEBPACK_IMPORTED_MODULE_1___default.a.Fragment, null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components_base__WEBPACK_IMPORTED_MODULE_4__["Form"], {
    errors: errors,
    formFields: _helpers_userAccountSettingsHelper_userAccountHelper__WEBPACK_IMPORTED_MODULE_10__["developerFields"]
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "mb-5 text-base flex"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("label", {
    className: "mr-3 w-40 mt-2 capitalize pr-3"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
    className: "font-bold"
  }, "Bank")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "w-full"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "flex"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(react_select__WEBPACK_IMPORTED_MODULE_8__["default"], {
    isOptionSelected: true,
    name: "bank",
    classNamePrefix: "input-select",
    onChange: function onChange(e) {
      return handleChange("bank", e);
    },
    className: "w-full",
    placeholder: "Select Bank",
    value: selectedBank,
    options: bankList
  })))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_material_ui_core_Collapse__WEBPACK_IMPORTED_MODULE_6__["default"], {
    "in": showOtherBankText
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "mb-5 text-base flex"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("label", {
    className: "mr-3 w-40 mt-2 capitalize pr-3"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
    className: "font-bold"
  })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "w-full"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "flex"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components_base__WEBPACK_IMPORTED_MODULE_4__["TextInput"], {
    type: "text",
    placeholder: "Enter Bank Name",
    name: "new_bank_name",
    border: false,
    appearance: true
  }))))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "mb-5 text-base flex"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("label", {
    className: "mr-3 w-40 mt-2 capitalize pr-3"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
    className: "font-bold"
  }, "Acct. Name")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "w-full"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "flex"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components_base__WEBPACK_IMPORTED_MODULE_4__["TextInput"], {
    type: "text",
    placeholder: "Account Name",
    name: "bank_account_name",
    border: false,
    defaultValue: bankAccountName,
    appearance: true
  }), errors.bank_account_name && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
    className: "text-red-500 text-xs"
  }, errors.bank_account_name[0])))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "mb-5 text-base flex"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("label", {
    className: "mr-3 w-40 mt-2 capitalize pr-3"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
    className: "font-bold"
  }, "BSB")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "w-full"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "flex"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components_base__WEBPACK_IMPORTED_MODULE_4__["TextInput"], {
    type: "text",
    placeholder: "Enter BSB",
    name: "bank_bsb",
    defaultValue: bankBSB,
    border: false,
    appearance: true
  }), errors.bank_bsb && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
    className: "text-red-500 text-xs"
  }, errors.bank_bsb[0])))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "mb-5 text-base flex"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("label", {
    className: "mr-3 w-40 mt-2 capitalize pr-3"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
    className: "font-bold"
  }, "Acct. No.")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "w-full"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "flex"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components_base__WEBPACK_IMPORTED_MODULE_4__["TextInput"], {
    type: "text",
    placeholder: "Enter Account Number",
    name: "bank_account_number",
    defaultValue: bankAccountNumber,
    border: false,
    appearance: true
  }), errors.bank_account_number && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
    className: "text-red-500 text-xs"
  }, errors.bank_account_number[0])))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "mb-5 text-base flex"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("label", {
    className: "mr-3 w-40 mt-2 capitalize pr-3"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
    className: "font-bold"
  }, "State")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "w-full"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "flex"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(react_select__WEBPACK_IMPORTED_MODULE_8__["default"], {
    isOptionSelected: true,
    name: "dev_state",
    classNamePrefix: "input-select",
    onChange: function onChange(e) {
      return handleChange("dev_state", e);
    },
    className: "w-full",
    placeholder: "Select State",
    value: selectedState,
    options: _helpers_propertyHelper_propertyHelper__WEBPACK_IMPORTED_MODULE_13__["states"]
  })))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "mb-5 text-base flex"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("label", {
    className: "mr-3 w-40 mt-2 capitalize pr-3"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
    className: "font-bold"
  }, "Country")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "w-full"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "flex"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(react_select__WEBPACK_IMPORTED_MODULE_8__["default"], {
    isOptionSelected: true,
    name: "dev_country",
    classNamePrefix: "input-select",
    onChange: function onChange(e) {
      return handleChange("dev_country", e);
    },
    className: "w-full",
    placeholder: "Select Country",
    value: selectedCountry,
    options: _helpers_countries__WEBPACK_IMPORTED_MODULE_12__["countries"]
  })))))))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "lg:px-16 flex justify-center lg:justify-end mt-8"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components_base__WEBPACK_IMPORTED_MODULE_4__["Button"], {
    className: "font-bold rounded",
    disabled: loading
  }, loading && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_fortawesome_react_fontawesome__WEBPACK_IMPORTED_MODULE_5__["FontAwesomeIcon"], {
    icon: ["fas", "spinner"],
    className: "fa-spin mr-2"
  }), "Save")))))));
};

/* harmony default export */ __webpack_exports__["default"] = (UserAccountSettings);

/***/ })

}]);