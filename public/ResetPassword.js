(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["ResetPassword"],{

/***/ "./resources/js/pages/ResetPassword.js":
/*!*********************************************!*\
  !*** ./resources/js/pages/ResetPassword.js ***!
  \*********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _layouts_users_UserLayout__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../layouts/users/UserLayout */ "./resources/js/layouts/users/UserLayout.js");
/* harmony import */ var _components_base__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../components/_base */ "./resources/js/components/_base/index.js");
/* harmony import */ var _components_base__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(_components_base__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _fortawesome_react_fontawesome__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @fortawesome/react-fontawesome */ "./node_modules/@fortawesome/react-fontawesome/index.es.js");
/* harmony import */ var _data_resetPassword_resetPassword__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../data/resetPassword/resetPassword */ "./resources/js/data/resetPassword/resetPassword.js");
/* harmony import */ var _fortawesome_free_solid_svg_icons__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @fortawesome/free-solid-svg-icons */ "./node_modules/@fortawesome/free-solid-svg-icons/index.es.js");
/* harmony import */ var _components_base_alerts_sweetAlert__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../components/_base/alerts/sweetAlert */ "./resources/js/components/_base/alerts/sweetAlert.js");
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! lodash */ "./node_modules/lodash/lodash.js");
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_7___default = /*#__PURE__*/__webpack_require__.n(lodash__WEBPACK_IMPORTED_MODULE_7__);
function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _unsupportedIterableToArray(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

function _iterableToArrayLimit(arr, i) { if (typeof Symbol === "undefined" || !(Symbol.iterator in Object(arr))) return; var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }










var ResetPassword = function ResetPassword() {
  var _useState = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])({}),
      _useState2 = _slicedToArray(_useState, 2),
      errors = _useState2[0],
      setErrors = _useState2[1];

  var _useState3 = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])(false),
      _useState4 = _slicedToArray(_useState3, 2),
      loading = _useState4[0],
      setLoading = _useState4[1];

  var _useState5 = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])(""),
      _useState6 = _slicedToArray(_useState5, 2),
      email = _useState6[0],
      setEmail = _useState6[1];

  var _useState7 = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])(""),
      _useState8 = _slicedToArray(_useState7, 2),
      password = _useState8[0],
      setPassword = _useState8[1];

  var _useState9 = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])(""),
      _useState10 = _slicedToArray(_useState9, 2),
      confirmPassword = _useState10[0],
      setPasswordConfirm = _useState10[1];

  var _useState11 = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])(false),
      _useState12 = _slicedToArray(_useState11, 2),
      showPassword = _useState12[0],
      setShowPassword = _useState12[1];

  var _useState13 = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])(false),
      _useState14 = _slicedToArray(_useState13, 2),
      showConfirmPassword = _useState14[0],
      setShowConfirmPassword = _useState14[1];

  var _useState15 = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])(""),
      _useState16 = _slicedToArray(_useState15, 2),
      paramToken = _useState16[0],
      setToken = _useState16[1];

  var requestPasswordReset = function requestPasswordReset(e) {
    e.preventDefault();

    if (password !== confirmPassword) {
      Object(_components_base_alerts_sweetAlert__WEBPACK_IMPORTED_MODULE_6__["sweetAlert"])("error", "Password did not match!");
      return;
    }

    setLoading(true);
    var url = "/api/password/reset";
    var param = {
      email: email,
      password: password,
      password_confirmation: confirmPassword,
      token: paramToken
    };
    Object(_data_resetPassword_resetPassword__WEBPACK_IMPORTED_MODULE_4__["resetPassword"])({
      url: url,
      param: param
    }).then(function (res) {
      if (res.status == 200) {
        setLoading(false);
        Object(_components_base_alerts_sweetAlert__WEBPACK_IMPORTED_MODULE_6__["sweetAlert"])("success", res.data.message);
        window.location = "/";
      }
    })["catch"](function (error) {
      setErrors(error.response.data.errors);
      setLoading(false);
    });
  };

  var handleInputChange = function handleInputChange(field, e) {
    switch (field) {
      case "password":
        setPassword(e.target.value);
        break;

      case "confirmPassword":
        setPasswordConfirm(e.target.value);
        break;

      default:
        setEmail(e.target.value);
        break;
    }
  };

  var backToReset = function backToReset(e) {
    e.preventDefault();
    window.location = "/request-password-reset";
  };

  Object(react__WEBPACK_IMPORTED_MODULE_0__["useEffect"])(function () {
    var token = new URLSearchParams(window.location.search).get("token");
    setToken(token);
  }, []);
  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_layouts_users_UserLayout__WEBPACK_IMPORTED_MODULE_1__["default"], null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("section", {
    className: "text-white bg-palette-blue-dark",
    style: {
      marginTop: -130,
      paddingTop: 160
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "mx-auto",
    style: {
      maxWidth: 1366
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "bg-white flex flex-col items-center lg:p-16 mx-auto pb-10 pt-8 px-4 text-white"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("h1", {
    className: "bg-white font-bold leading-tight lg:w-3/4 px-1 py-8 rounded-t text-2xl text-center text-gray-900 w-full"
  }, "Reset Password"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "mb-5 w-1/2"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_base__WEBPACK_IMPORTED_MODULE_2__["TextInput"], {
    className: "".concat(errors["email"] && errors["email"][0] ? "border-red-300" : "border-gray-400", " border-gray-400 border pl-2 py-2 rounded text-base text-gray-900"),
    border: false,
    name: "email",
    placeholder: "Email",
    autoComplete: "off",
    onChange: function onChange(e) {
      return handleInputChange("email", e);
    }
  }), errors["email"] && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
    className: "px-1 text-red-400 tracking-widest text-xs "
  }, errors["email"][0])), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "".concat(errors["password"] && errors["password"][0] ? "border-red-300" : "border-gray-400 mb-5", " w-1/2 border-2 rounded")
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_base__WEBPACK_IMPORTED_MODULE_2__["TextInput"], {
    className: "appearance-none bg-transparent border-gray-400 border-r-0 font-hairline leading-relaxed pl-2 py-2 text-base text-gray-900 w-full",
    border: false,
    name: "password",
    type: "".concat(showPassword ? "text" : "password"),
    placeholder: "Password",
    autoComplete: "off",
    suffix: /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
      className: "cursor-pointer mr-2",
      onClick: function onClick() {
        return setShowPassword(!showPassword);
      }
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("svg", {
      className: "feather-icon h-6 opacity-50 text-gray-800 w-6 mt-2"
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("use", {
      xlinkHref: "/assets/svg/feather-sprite.svg#".concat(showPassword ? "eye-off" : "eye")
    }))),
    onChange: function onChange(e) {
      return handleInputChange("password", e);
    }
  })), errors["password"] && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "mb-5 w-1/2"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
    className: "px-1 text-red-400 tracking-widest text-xs"
  }, errors["password"][0])), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "".concat(errors["password_confirmation"] && errors["password_confirmation"][0] ? "border-red-300" : "border-gray-400 mb-5", " w-1/2 border-2 border-gray-400 rounded")
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_base__WEBPACK_IMPORTED_MODULE_2__["TextInput"], {
    className: "appearance-none bg-transparent  border-gray-400 border-r-0 font-hairline leading-relaxed pl-2 py-2 text-base text-gray-900 w-full",
    border: false,
    name: "confirmPassword",
    type: "".concat(showConfirmPassword ? "text" : "password"),
    placeholder: "Confirm Password",
    autoComplete: "off",
    suffix: /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
      className: "cursor-pointer mr-2",
      onClick: function onClick() {
        return setShowConfirmPassword(!showConfirmPassword);
      }
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("svg", {
      className: "feather-icon h-6 opacity-50 text-gray-800 w-6 mt-2"
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("use", {
      xlinkHref: "/assets/svg/feather-sprite.svg#".concat(showConfirmPassword ? "eye-off" : "eye")
    }))),
    onChange: function onChange(e) {
      return handleInputChange("confirmPassword", e);
    }
  })), errors["password_confirmation"] && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "mb-5 w-1/2"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
    className: "px-1 text-red-400 tracking-widest text-xs"
  }, errors["password_confirmation"][0])), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "flex items-center justify-end mb-5 w-1/2"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_base__WEBPACK_IMPORTED_MODULE_2__["Button"], {
    className: "font-bold rounded mr-3 bg-red-600",
    disabled: loading,
    onClick: function onClick(e) {
      return backToReset(e);
    }
  }, "Cancel"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_base__WEBPACK_IMPORTED_MODULE_2__["Button"], {
    className: "font-bold rounded",
    disabled: loading,
    onClick: function onClick(e) {
      return requestPasswordReset(e);
    }
  }, loading && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_fortawesome_react_fontawesome__WEBPACK_IMPORTED_MODULE_3__["FontAwesomeIcon"], {
    icon: _fortawesome_free_solid_svg_icons__WEBPACK_IMPORTED_MODULE_5__["faSpinner"],
    className: "fa-spin mr-2"
  }), "Reset Password"))))));
};

/* harmony default export */ __webpack_exports__["default"] = (ResetPassword);

/***/ })

}]);