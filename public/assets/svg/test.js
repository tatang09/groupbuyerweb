<symbol id="chevrons-up" viewBox="0 0 24 24">
    <polyline points="17 11 12 6 7 11">
    </polyline>
    <polyline points="17 18 12 13 7 18">
    </polyline>
    <polyline points="17 25 12 20 7 25">
    </polyline>
</symbol>