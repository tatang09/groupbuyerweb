(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["BuyingProductPage"],{

/***/ "./resources/js/pages/BuyingProductPage.js":
/*!*************************************************!*\
  !*** ./resources/js/pages/BuyingProductPage.js ***!
  \*************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime/regenerator */ "./node_modules/@babel/runtime/regenerator/index.js");
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _layouts_users_UserLayout__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../layouts/users/UserLayout */ "./resources/js/layouts/users/UserLayout.js");
/* harmony import */ var _states_userGlobal__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../states/userGlobal */ "./resources/js/states/userGlobal.js");
/* harmony import */ var _components__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../components */ "./resources/js/components/index.js");
/* harmony import */ var _components__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(_components__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! lodash */ "./node_modules/lodash/lodash.js");
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(lodash__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var _services_auth__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../services/auth */ "./resources/js/services/auth.js");
/* harmony import */ var react_helmet__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! react-helmet */ "./node_modules/react-helmet/es/Helmet.js");
/* harmony import */ var _helpers_metaHelper_metaHelper__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../helpers/metaHelper/metaHelper */ "./resources/js/helpers/metaHelper/metaHelper.js");
/* harmony import */ var _fortawesome_react_fontawesome__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @fortawesome/react-fontawesome */ "./node_modules/@fortawesome/react-fontawesome/index.es.js");
/* harmony import */ var react_device_detect__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! react-device-detect */ "./node_modules/react-device-detect/main.js");
/* harmony import */ var react_device_detect__WEBPACK_IMPORTED_MODULE_10___default = /*#__PURE__*/__webpack_require__.n(react_device_detect__WEBPACK_IMPORTED_MODULE_10__);
/* harmony import */ var react_currency_format__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! react-currency-format */ "./node_modules/react-currency-format/lib/currency-format.js");
/* harmony import */ var react_currency_format__WEBPACK_IMPORTED_MODULE_11___default = /*#__PURE__*/__webpack_require__.n(react_currency_format__WEBPACK_IMPORTED_MODULE_11__);
/* harmony import */ var _helpers_pricingHelper_discountHelper__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ../helpers/pricingHelper/discountHelper */ "./resources/js/helpers/pricingHelper/discountHelper.js");


function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _unsupportedIterableToArray(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

function _iterableToArrayLimit(arr, i) { if (typeof Symbol === "undefined" || !(Symbol.iterator in Object(arr))) return; var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }














var BuyingProductPage = function BuyingProductPage(_ref) {
  var dealId = _ref.dealId;

  var _UserGlobal = Object(_states_userGlobal__WEBPACK_IMPORTED_MODULE_3__["default"])(),
      _UserGlobal2 = _slicedToArray(_UserGlobal, 2),
      userState = _UserGlobal2[0],
      userAction = _UserGlobal2[1];

  var _useState = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(true),
      _useState2 = _slicedToArray(_useState, 2),
      loading = _useState2[0],
      setLoading = _useState2[1];

  var _useState3 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(false),
      _useState4 = _slicedToArray(_useState3, 2),
      loadSummary = _useState4[0],
      setLoadSummary = _useState4[1];

  var _useState5 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(),
      _useState6 = _slicedToArray(_useState5, 2),
      project = _useState6[0],
      setProject = _useState6[1];

  var anchorRef = Object(react__WEBPACK_IMPORTED_MODULE_1__["useRef"])(null);
  Object(react__WEBPACK_IMPORTED_MODULE_1__["useEffect"])(function () {});

  var truncateString = function truncateString(string, limit) {
    if (string.length > limit) {
      return string.substring(0, limit) + "...";
    } else {
      return string;
    }
  };

  var getDeal = /*#__PURE__*/function () {
    var _ref2 = _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee() {
      var deal, exp, _yield$axios$get, data, newProject;

      return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee$(_context) {
        while (1) {
          switch (_context.prev = _context.next) {
            case 0:
              _context.next = 2;
              return axios.get("/api/deal/".concat(dealId));

            case 2:
              deal = _context.sent;

              if (deal.data && deal.data.expires_at) {
                exp = JSON.parse(deal.data.expires_at);
                setTimeLeft(exp.time_limit);
              } // getDeveloperById(deal.data.user_id).then(res => {
              //     if (res.data.company_logo_path) {
              //         setDeveloperLogo(res.data.company_logo_path);
              //         setDeveloperURl(res.data.company_url);
              //     }
              // });


              if (!(Object(_services_auth__WEBPACK_IMPORTED_MODULE_6__["isLoggedIn"])() && !Object(lodash__WEBPACK_IMPORTED_MODULE_5__["isEmpty"])(userState.user))) {
                _context.next = 14;
                break;
              }

              _context.next = 7;
              return axios.get("/api/wish-list/".concat(dealId), {
                headers: {
                  Authorization: "Bearer " + Object(_services_auth__WEBPACK_IMPORTED_MODULE_6__["isLoggedIn"])()
                }
              });

            case 7:
              _yield$axios$get = _context.sent;
              data = _yield$axios$get.data;
              newProject = Object.assign({}, deal.data);

              if (data && !Object(lodash__WEBPACK_IMPORTED_MODULE_5__["isEmpty"])(newProject)) {
                data.map(function (d) {
                  newProject.properties.map(function (p) {
                    if (p.id == d.property_id) {
                      p.hasWishlist = true;
                      p.wishListId = d.id;
                    }
                  });
                });
              }

              setProject(newProject);
              _context.next = 15;
              break;

            case 14:
              setProject(deal.data);

            case 15:
              setLoading(false);

            case 16:
            case "end":
              return _context.stop();
          }
        }
      }, _callee);
    }));

    return function getDeal() {
      return _ref2.apply(this, arguments);
    };
  }();

  Object(react__WEBPACK_IMPORTED_MODULE_1__["useEffect"])(function () {
    getDeal();
  }, [userState.user]);

  var handleWishlist = /*#__PURE__*/function () {
    var _ref3 = _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee2(property) {
      var formData;
      return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee2$(_context2) {
        while (1) {
          switch (_context2.prev = _context2.next) {
            case 0:
              if (Object(_services_auth__WEBPACK_IMPORTED_MODULE_6__["isLoggedIn"])()) {
                _context2.next = 3;
                break;
              }

              userAction.setState({
                showSignIn: true
              });
              return _context2.abrupt("return");

            case 3:
              formData = new FormData();
              formData.append("property_id", property.id);
              formData.append("deal_id", dealId);
              _context2.prev = 6;

              if (property.hasWishlist) {
                _context2.next = 12;
                break;
              }

              _context2.next = 10;
              return axios.post("/api/wish-list", formData, {
                headers: {
                  Authorization: "Bearer " + Object(_services_auth__WEBPACK_IMPORTED_MODULE_6__["isLoggedIn"])()
                }
              });

            case 10:
              _context2.next = 14;
              break;

            case 12:
              _context2.next = 14;
              return axios["delete"]("/api/wish-list/".concat(property.wishListId), {
                headers: {
                  Authorization: "Bearer " + Object(_services_auth__WEBPACK_IMPORTED_MODULE_6__["isLoggedIn"])()
                }
              });

            case 14:
              getDeal();
              _context2.next = 19;
              break;

            case 17:
              _context2.prev = 17;
              _context2.t0 = _context2["catch"](6);

            case 19:
            case "end":
              return _context2.stop();
          }
        }
      }, _callee2, null, [[6, 17]]);
    }));

    return function handleWishlist(_x) {
      return _ref3.apply(this, arguments);
    };
  }();

  var renderApartments = function renderApartments() {
    return !loading && Object.keys(project).length !== 0 && project.properties.map(function (property, key) {
      return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("li", {
        key: key,
        className: "lg:px-3 lg:w-4/12 mb-6 relative w-full"
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
        className: "bg-white lg:px-5 px-3 py-4 relative rounded-md",
        style: {
          boxShadow: "0 5px 28px 4px rgba(0,0,0,0.1)"
        }
      }, property.is_secured && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
        className: "rounded-md absolute inset-0 z-20 bg-gray-900 bg-opacity-75 flex items-center justify-center"
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
        className: "uppercase font-bold text-4xl text-white flex items-center"
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
        className: "w-12 h-12 rounded-full mr-4 flex items-center justify-center",
        style: {
          background: "#89b036"
        }
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("svg", {
        className: "feather-icon h-8 w-8",
        style: {
          strokeWidth: 4
        }
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("use", {
        xlinkHref: "/assets/svg/feather-sprite.svg#check"
      }))), "Deal Secured")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
        className: "flex items-center justify-between px-2 py-2"
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
        className: "text-2xl text-palette-purple-main"
      }, "Unit ", property.unit_name)), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
        className: "bg-center bg-cover relative rounded text-white",
        style: {
          minHeight: react_device_detect__WEBPACK_IMPORTED_MODULE_10__["isMobile"] ? 250 : 300,
          background: "linear-gradient(rgba(0, 0, 0, 0.1) 0%, rgba(0, 0, 0, 0.2) 80%, rgb(0, 0, 0) 100%), url(\"".concat(property.featured_images[0], "\")")
        }
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
        className: "absolute flex px-5 py-3 right-0 top-0"
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
        className: "rounded-full items-center justify-center flex mr-3 text-gray-500"
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("img", {
        src: "/assets/images/icons/purple_heart.png"
      })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
        className: "rounded-full items-center justify-center flex text-gray-500"
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("img", {
        src: "/assets/images/icons/share.png"
      }))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
        className: "absolute bottom-0 flex py-3 px-5 w-full"
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
        className: "flex-1 flex items-end text-red-700 line-through"
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(react_currency_format__WEBPACK_IMPORTED_MODULE_11___default.a, {
        value: property.price,
        displayType: "text",
        thousandSeparator: true,
        prefix: "$"
      })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
        className: "flex flex-1 font-bold items-center justify-end text-2xl"
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(react_currency_format__WEBPACK_IMPORTED_MODULE_11___default.a, {
        value: property.discounted_price || 0,
        displayType: "text",
        thousandSeparator: true,
        prefix: "$"
      }))))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
        className: "py-4"
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
        className: "px-2 flex items-center text-gray-900 text-justify"
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
        className: "text-base"
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("p", null, "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi ut turpis in velit ultricies dignissim. Morbi viverra libero quis est porttitor tincidunt."))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
        className: " pb-4 px-2 flex items-center text-gray-900 text-justify"
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
        className: "lg:w-1/3"
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
        className: "mr-3"
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
        className: "font-semibold text-sm"
      }, "Internal:", " "), "50m2")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
        className: "lg:w-1/3"
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
        className: "mr-3"
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
        className: "font-semibold text-sm"
      }, "External:", " "), "10m2"))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
        className: " pb-4 px-2 flex items-center text-gray-900 text-justify"
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
        className: "flex items-center font-bold border-r lg:pr-6 lg:text-3xl xs:pr-2 xs:text-lg border-palette-purple-main"
      }, property.no_of_bedrooms, " ", /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("img", {
        src: "/assets/images/bed.svg",
        className: "lg:w-10 xs:w-6 lg:ml-4 xs:ml-2"
      }))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
        className: "flex items-center font-bold border-r lg:px-6 lg:text-3xl xs:px-2 xs:text-lg border-palette-purple-main"
      }, property.no_of_bathrooms, " ", /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("img", {
        src: "/assets/images/shower.svg",
        className: "lg:w-10 xs:w-6 lg:ml-4 xs:ml-2"
      }))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
        className: "flex items-center font-bold lg:pl-6 lg:text-3xl xs:pl-2 xs:text-lg"
      }, property.no_of_garages, " ", /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("img", {
        src: "/assets/images/car.svg",
        className: "lg:w-10 xs:w-6 lg:ml-4 xs:ml-2"
      })))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
        className: "w-full px-2"
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("button", {
        className: "bg-palette-purple-main lg:py-2 lg:rounded py-3 rounded-sm text-white w-full",
        onClick: function onClick() {
          window.location = "/weekly-deals/".concat(dealId, "/apartment/").concat(property.id);
        }
      }, "See More")))));
    });
  };

  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_layouts_users_UserLayout__WEBPACK_IMPORTED_MODULE_2__["default"], null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(react_helmet__WEBPACK_IMPORTED_MODULE_7__["Helmet"], null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("meta", {
    name: "description",
    content: _helpers_metaHelper_metaHelper__WEBPACK_IMPORTED_MODULE_8__["metaHelper"].desc
  })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("section", {
    className: "text-black bg-white"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("section", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "lg:grid lg:grid-cols-3 xs:block"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "lg:col-span-2 xs:block"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    style: {
      backgroundImage: "linear-gradient(rgba(0, 0, 0, 0), rgba(0, 0, 0, 0.8)), url('/assets/images/homepage_carrousel_b_4.jpg')"
    },
    className: "flex items-end w-full text-white bg-left bg-no-repeat bg-cover lg:h-full xs:h-screen"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "w-full px-8 pb-8 lg:space-y-4 xs:space-y-3 xs:pt-56 lg:pb-12 lg:px-16 lg:pt-0"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("p", {
    className: "font-bold leading-none lg:text-6xl xs:text-5xl"
  }, "Sunny Cove"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("p", {
    className: "font-bold leading-none lg:text-3xl xs:text-2xl"
  }, "13% off"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "flex items-center w-full"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "items-center block border-r lg:text-base xs:text-sm lg:pr-6 xs:pr-4 border-palette-purple-main"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "font-bold text-palette-purple-main"
  }, "Goal"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "font-normal text-white"
  }, "20 investors")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "items-center block border-r lg:text-base xs:text-sm lg:px-6 xs:px-4 border-palette-purple-main"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "font-bold text-palette-purple-main"
  }, "Current"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "font-normal text-white"
  }, "12 investors")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "items-center block lg:text-base xs:text-sm lg:pl-6 xs:pl-4"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "font-bold text-palette-purple-main"
  }, "To Go"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "font-normal text-white"
  }, "8 investors"))), loadSummary && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("p", {
    className: "font-normal lg:text-base xs:text-sm"
  }, "Dolor sit amet, consectetur adipiscing elit. Morbi ut turpis in velit ultricies dignissim. Morbi viverra libero quis est porttitor tincidunt. Nunc in metus sit amet felis feugiat tincidunt sit amet quis lectus. Vivamus tincidunt a ligula a malesuada. Aenean viverra lacus nulla, sit amet bibendum ipsum suscipit sit amet."), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "flex items-center justify-between w-full pt-2"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("button", {
    className: "py-3 text-xs font-semibold text-white whitespace-pre rounded lg:px-12 xs:px-6",
    style: {
      backgroundColor: '#E91AFC'
    },
    onClick: function onClick() {
      setLoadSummary(!loadSummary);
    }
  }, loadSummary ? "Read less" : "Read more"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "flex"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("button", {
    className: "rounded w-auto py-4 lg:px-6 xs:px-4"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("img", {
    src: "/assets/images/left-chevron-white.svg",
    className: "transform -rotate-90 lg:w-5 xs:w-4"
  }))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("button", {
    className: "rounded w-auto py-4 lg:px-6 xs:px-4"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("img", {
    src: "/assets/images/right-chevron-white.svg",
    className: "transform -rotate-90 lg:w-5 xs:w-4"
  })))))))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "lg:col-span-1 xs:block"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "h-full items-end bg-cover text-white bg-left bg-no-repeat flex w-full bg-palette-violet-main-dark"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "block lg:pr-20 lg:pl-16 lg:pt-48 lg:pb-32 xs:px-8 xs:pt-12 xs:pb-8 space-y-4"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "lg:block xs:flex xs:flex-row-reverse lg:space-y-3 xs:justify-between text-xs font-normal"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "lg:w-full xs:w-1/3"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "flex xs:justify-end"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "flex items-center font-semibold text-xs px-3 py-1 bg-palette-purple-main rounded"
  }, "Express", /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("img", {
    src: "/assets/images/running.svg",
    className: "w-5 ml-2"
  }))))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "lg:w-full xs:w-2/3"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "font-bold leading-none lg:text-5xl xs:text-3xl"
  }, "Property Features"))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("p", {
    className: "font-semibold leading-none text-xs text-palette-purple-main uppercase xs:w-3/4"
  }, "Spectacular brand new contemporary home"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "block space-y-3 text-xs font-normal"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "flex"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("img", {
    src: "/assets/images/chevron-right.svg",
    className: "w-2 mr-2"
  })), "SMEG appliances throughout"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "flex"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("img", {
    src: "/assets/images/chevron-right.svg",
    className: "w-2 mr-2"
  })), "European inspired timber floors"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "flex"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("img", {
    src: "/assets/images/chevron-right.svg",
    className: "w-2 mr-2"
  })), "Architecturally designed open spaces"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "flex"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("img", {
    src: "/assets/images/chevron-right.svg",
    className: "w-2 mr-2"
  })), "Ducted day-night air-condition"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "flex"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("img", {
    src: "/assets/images/chevron-right.svg",
    className: "w-2 mr-2"
  })), "Spacious entertainers` balcony"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "flex"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("img", {
    src: "/assets/images/chevron-right.svg",
    className: "w-2 mr-2"
  })), "1 car space and private storage"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "flex"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("img", {
    src: "/assets/images/chevron-right.svg",
    className: "w-2 mr-2"
  })), "Access to the rooftop terrace and BBQ"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "flex"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("img", {
    src: "/assets/images/chevron-right.svg",
    className: "w-2 mr-2"
  })), "Access to pool and residence lounge"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "flex"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("img", {
    src: "/assets/images/chevron-right.svg",
    className: "w-2 mr-2"
  })), "Great connectivity with local amenities"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "flex"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("img", {
    src: "/assets/images/chevron-right.svg",
    className: "w-2 mr-2"
  })), "Designed by award-winning architects"))))))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("section", {
    className: "lg:flex xs:block"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "lg:w-1/2 xs:w-full"
  }, !loading && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components__WEBPACK_IMPORTED_MODULE_4__["GoogleMapsNew"], {
    project: project
  })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components__WEBPACK_IMPORTED_MODULE_4__["NeighbourhoodInsights"], null)), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("section", {
    className: "lg:flex xs:block"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "lg:w-1/2 xs:w-full xs:hidden lg:flex lg:items-center",
    style: {
      backgroundColor: "#4D0B98"
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components__WEBPACK_IMPORTED_MODULE_4__["RegistrationForm"], {
    subHeaderColor: "text-white",
    headerColor: "text-palette-purple-main",
    isPopup: false,
    opacity: "100%",
    containerBgColor: "#4D0B98"
  })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components__WEBPACK_IMPORTED_MODULE_4__["LocalSchoolCatchments"], null)), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("section", {
    className: "text-black bg-white"
  }, " ", /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "lg:flex xs:block w-full h-auto"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components__WEBPACK_IMPORTED_MODULE_4__["InspectProperty"], null), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components__WEBPACK_IMPORTED_MODULE_4__["TalkExpert"], null))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components__WEBPACK_IMPORTED_MODULE_4__["TheMore"], {
    bgColor: "#4D0B98"
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("section", {
    className: "relative bg-white"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "pb-4 px-6 lg:px-0 mx-auto",
    style: {
      maxWidth: 1366
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("h4", {
    className: "text-center my-8 text-palette-purple-main text-base font-extrabold"
  }, !loading && project.name), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("h1", {
    className: "text-left text-center leading-none font-bold text-4xl lg:text-5xl px-8 mb-4 lg:mb-10"
  }, "Available properties"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("p", {
    className: "text-justify mb-5  lg:px-32 lg:mb-16"
  }, "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Mi proin sed libero enim sed faucibus turpis. Amet consectetur adipiscing elit ut aliquam purus sit."), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("ul", {
    className: "flex flex-wrap mt-6"
  }, renderApartments()))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components__WEBPACK_IMPORTED_MODULE_4__["PieceMeal"], null), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components__WEBPACK_IMPORTED_MODULE_4__["JoinUs"], null)), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components__WEBPACK_IMPORTED_MODULE_4__["SocialMediaShareModal"], null));
};

var apartmentSettings = {
  dots: false,
  arrows: false,
  infinite: true,
  autoplay: true,
  autoplaySpeed: 5000,
  slidesToShow: 1,
  responsive: [{
    breakpoint: 1024,
    settings: {
      slidesToShow: 3,
      slidesToScroll: 3,
      infinite: true,
      dots: true
    }
  }, {
    breakpoint: 600,
    settings: {
      slidesToShow: 2,
      slidesToScroll: 2
    }
  }, {
    breakpoint: 480,
    settings: {
      slidesToShow: 1,
      slidesToScroll: 1
    }
  } // You can unslick at a given breakpoint now by adding:
  // settings: "unslick"
  // instead of a settings object
  ]
};
/* harmony default export */ __webpack_exports__["default"] = (BuyingProductPage);

/***/ })

}]);