(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[1],{

/***/ "./resources/js/layouts/users/Header.js":
/*!**********************************************!*\
  !*** ./resources/js/layouts/users/Header.js ***!
  \**********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime/regenerator */ "./node_modules/@babel/runtime/regenerator/index.js");
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var hookrouter__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! hookrouter */ "./node_modules/hookrouter/dist/index.js");
/* harmony import */ var hookrouter__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(hookrouter__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _states_userGlobal__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../states/userGlobal */ "./resources/js/states/userGlobal.js");
/* harmony import */ var _services_auth__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../services/auth */ "./resources/js/services/auth.js");
/* harmony import */ var _material_ui_core__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @material-ui/core */ "./node_modules/@material-ui/core/esm/index.js");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! sweetalert2 */ "./node_modules/sweetalert2/dist/sweetalert2.all.js");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(sweetalert2__WEBPACK_IMPORTED_MODULE_6__);
/* harmony import */ var react_device_detect__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! react-device-detect */ "./node_modules/react-device-detect/main.js");
/* harmony import */ var react_device_detect__WEBPACK_IMPORTED_MODULE_7___default = /*#__PURE__*/__webpack_require__.n(react_device_detect__WEBPACK_IMPORTED_MODULE_7__);
/* harmony import */ var _components_base__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../../components/_base */ "./resources/js/components/_base/index.js");
/* harmony import */ var _components_base__WEBPACK_IMPORTED_MODULE_8___default = /*#__PURE__*/__webpack_require__.n(_components_base__WEBPACK_IMPORTED_MODULE_8__);
/* harmony import */ var _components__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../../components */ "./resources/js/components/index.js");
/* harmony import */ var _components__WEBPACK_IMPORTED_MODULE_9___default = /*#__PURE__*/__webpack_require__.n(_components__WEBPACK_IMPORTED_MODULE_9__);
/* harmony import */ var react_currency_format__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! react-currency-format */ "./node_modules/react-currency-format/lib/currency-format.js");
/* harmony import */ var react_currency_format__WEBPACK_IMPORTED_MODULE_10___default = /*#__PURE__*/__webpack_require__.n(react_currency_format__WEBPACK_IMPORTED_MODULE_10__);


function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _unsupportedIterableToArray(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

function _iterableToArrayLimit(arr, i) { if (typeof Symbol === "undefined" || !(Symbol.iterator in Object(arr))) return; var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }

function _objectDestructuringEmpty(obj) { if (obj == null) throw new TypeError("Cannot destructure undefined"); }












var Header = function Header(_ref) {
  _objectDestructuringEmpty(_ref);

  var path = Object(hookrouter__WEBPACK_IMPORTED_MODULE_2__["usePath"])();
  var anchorRef = Object(react__WEBPACK_IMPORTED_MODULE_1__["useRef"])(null);

  var _UserGlobal = Object(_states_userGlobal__WEBPACK_IMPORTED_MODULE_3__["default"])(),
      _UserGlobal2 = _slicedToArray(_UserGlobal, 2),
      userState = _UserGlobal2[0],
      userAction = _UserGlobal2[1];

  var _useState = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(null),
      _useState2 = _slicedToArray(_useState, 2),
      popper = _useState2[0],
      setPopper = _useState2[1];

  var _useState3 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(false),
      _useState4 = _slicedToArray(_useState3, 2),
      showMenu = _useState4[0],
      setShowMenu = _useState4[1];

  var links = [{
    name: "FAQ",
    route: "/frequently-asked-questions",
    show: true
  }, {
    name: "How it works",
    route: "/how-it-works",
    show: true
  }, // {
  //   name: "About",
  //   route: "/about"
  // },
  {
    name: "Properties",
    route: "/weekly-deals",
    show: true
  }, {
    name: "Cars",
    route: "/cars-inventory",
    show: Object(_services_auth__WEBPACK_IMPORTED_MODULE_4__["isLoggedIn"])() && Object(_services_auth__WEBPACK_IMPORTED_MODULE_4__["userRole"])() === "admin" ? true : false
  }, // {
  //   name: "Selling",
  //   route: "/selling"
  // },
  {
    name: "Contact",
    route: "/contact",
    show: true
  }];

  var handleLogout = /*#__PURE__*/function () {
    var _ref2 = _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee2() {
      return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee2$(_context2) {
        while (1) {
          switch (_context2.prev = _context2.next) {
            case 0:
              if (!react_device_detect__WEBPACK_IMPORTED_MODULE_7__["isMobile"]) {
                _context2.next = 12;
                break;
              }

              _context2.prev = 1;
              _context2.next = 4;
              return axios.post("/api/logout", null, {
                headers: {
                  Authorization: "Bearer " + Object(_services_auth__WEBPACK_IMPORTED_MODULE_4__["isLoggedIn"])()
                }
              });

            case 4:
              Object(_services_auth__WEBPACK_IMPORTED_MODULE_4__["logout"])();
              setUserRole("");
              userAction.setState({
                fetch: !userState.fetch
              });
              _context2.next = 11;
              break;

            case 9:
              _context2.prev = 9;
              _context2.t0 = _context2["catch"](1);

            case 11:
              return _context2.abrupt("return");

            case 12:
              sweetalert2__WEBPACK_IMPORTED_MODULE_6___default.a.fire({
                title: "Confirmation",
                text: "Are you sure you want to sign out?",
                showConfirmButton: true,
                showCloseButton: true,
                showCancelButton: true,
                focusConfirm: false,
                confirmButtonColor: "#BD00F2",
                confirmButtonText: "Confirm"
              }).then( /*#__PURE__*/function () {
                var _ref3 = _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee(result) {
                  return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee$(_context) {
                    while (1) {
                      switch (_context.prev = _context.next) {
                        case 0:
                          if (!result.value) {
                            _context.next = 11;
                            break;
                          }

                          _context.prev = 1;
                          _context.next = 4;
                          return axios.post("/api/logout", null, {
                            headers: {
                              Authorization: "Bearer " + Object(_services_auth__WEBPACK_IMPORTED_MODULE_4__["isLoggedIn"])()
                            }
                          });

                        case 4:
                          Object(_services_auth__WEBPACK_IMPORTED_MODULE_4__["logout"])();
                          userAction.setState({
                            fetch: !userState.fetch
                          });
                          _context.next = 11;
                          break;

                        case 8:
                          _context.prev = 8;
                          _context.t0 = _context["catch"](1);
                          console.log(_context.t0);

                        case 11:
                        case "end":
                          return _context.stop();
                      }
                    }
                  }, _callee, null, [[1, 8]]);
                }));

                return function (_x) {
                  return _ref3.apply(this, arguments);
                };
              }());

            case 13:
            case "end":
              return _context2.stop();
          }
        }
      }, _callee2, null, [[1, 9]]);
    }));

    return function handleLogout() {
      return _ref2.apply(this, arguments);
    };
  }();

  var navLinks = function navLinks() {
    return links.map(function (link, key) {
      return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("a", {
        key: key,
        href: link.route,
        className: "\n            ".concat(path !== "/" ? "opacity-50" : "opacity-100", "\n            ").concat(path.includes(link.route) ? "text-palette-teal opacity-100" : "text-white", " mb-6 lg:mb-0\n            hover:text-palette-teal px-3 tracking-wide ").concat(!react_device_detect__WEBPACK_IMPORTED_MODULE_7__["isMobile"] ? "text-base" : "text-2xl lg:text-base", " font-bold transition-all duration-300\n            ").concat(link.show ? "" : "hidden", "\n          ")
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", null, link.name));
    });
  };

  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("header", {
    style: {
      maxWidth: 1366
    },
    className: "relative z-50 flex items-center mx-auto justify-center lg:justify-between px-6 lg:px-4 lg:pt-10"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("a", {
    href: "/"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("img", {
    // src="/assets/images/group_buyer_logo_white.png"
    src: "/assets/svg/upload_logo_min_negative.svg",
    style: {
      height: 90
    }
  })), !react_device_detect__WEBPACK_IMPORTED_MODULE_7__["isMobile"] && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "border border-bac flex font-bold h-8 items-center justify-center ml-3 mt-1 px-4 rounded-full lg:text-base text-palette-teal w-auto"
  }, "Members:\xA0", /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(react_currency_format__WEBPACK_IMPORTED_MODULE_10___default.a, {
    value: userState.userCount,
    displayType: "text",
    thousandSeparator: true // prefix={`$`}

  })), !react_device_detect__WEBPACK_IMPORTED_MODULE_7__["isMobile"] ? /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "flex items-center text-white"
  }, navLinks(), !Object(_services_auth__WEBPACK_IMPORTED_MODULE_4__["isLoggedIn"])() ? /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
    onClick: function onClick() {
      return userAction.setState({
        showSignIn: true
      });
    },
    className: "cursor-pointer hover:text-palette-teal px-3 tracking-wide lg:text-base text-white font-bold transition-all duration-300"
  }, "Log in") : /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "flex items-center"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "border-l border-gray-500 mr-6 ml-3",
    style: {
      width: 1,
      height: 20
    }
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
    onMouseOver: function onMouseOver(e) {
      return setPopper(e.currentTarget);
    },
    onMouseLeave: function onMouseLeave() {
      return setPopper(null);
    }
  }, userState.user && userState.user.avatar_path ? /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
    className: "flex items-center cursor-pointer"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
    className: "tracking-wide lg:text-base font-bold"
  }, userState.user && userState.user.first_name), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_material_ui_core__WEBPACK_IMPORTED_MODULE_5__["Avatar"], {
    ref: anchorRef,
    src: "".concat(userState.user.avatar_path),
    className: "cursor-pointer ml-2"
  })) : /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
    className: "flex items-center cursor-pointer"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
    className: "tracking-wide lg:text-base font-bold"
  }, userState.user && userState.user.first_name), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("svg", {
    className: "feather-icon h-6 w-6 ml-2",
    ref: anchorRef
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("use", {
    xlinkHref: "/assets/svg/feather-sprite.svg#user"
  }))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_material_ui_core__WEBPACK_IMPORTED_MODULE_5__["Popper"], {
    open: Boolean(popper),
    anchorEl: popper,
    placement: "bottom-end",
    transition: true,
    disablePortal: true,
    className: "-mr-4 mt-3 menu-list"
  }, function (_ref4) {
    var TransitionProps = _ref4.TransitionProps;
    return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_material_ui_core__WEBPACK_IMPORTED_MODULE_5__["Grow"], _extends({}, TransitionProps, {
      style: {
        transformOrigin: "right top"
      }
    }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_material_ui_core__WEBPACK_IMPORTED_MODULE_5__["Paper"], {
      elevation: 4,
      style: {
        width: 360
      },
      className: "relative"
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
      className: "arrow absolute",
      style: {
        top: "-6px",
        right: "24px",
        borderWidth: "0 0.8em 0.8em 0.8em",
        borderColor: "transparent transparent white transparent"
      }
    }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_material_ui_core__WEBPACK_IMPORTED_MODULE_5__["MenuList"], null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
      className: "flex items-center px-4 pt-3 pb-4 mb-2 border-b"
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
      className: "flex flex-col"
    }, userState.user && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(react__WEBPACK_IMPORTED_MODULE_1___default.a.Fragment, null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
      className: "block lg:text-base font-bold text-xl leading-none"
    }, userState.user.first_name + " " + userState.user.last_name), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
      className: "lg:text-base block\n                                    "
    }, userState.user.email)))), (Object(_services_auth__WEBPACK_IMPORTED_MODULE_4__["userRole"])() === "admin" || Object(_services_auth__WEBPACK_IMPORTED_MODULE_4__["userRole"])() === "super_admin") && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_material_ui_core__WEBPACK_IMPORTED_MODULE_5__["MenuItem"], {
      onClick: function onClick() {
        return window.location = "/admin/projects";
      }
    }, "Admin"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_material_ui_core__WEBPACK_IMPORTED_MODULE_5__["MenuItem"], {
      onClick: function onClick() {
        return window.location = "/profile/account-settings";
      }
    }, "My Profile"), (Object(_services_auth__WEBPACK_IMPORTED_MODULE_4__["userRole"])() === "admin" || Object(_services_auth__WEBPACK_IMPORTED_MODULE_4__["userRole"])() === "super_admin" || Object(_services_auth__WEBPACK_IMPORTED_MODULE_4__["userRole"])() === "project_developer") && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_material_ui_core__WEBPACK_IMPORTED_MODULE_5__["MenuItem"], {
      onClick: function onClick() {
        return window.location = "/profile/manage-projects";
      }
    }, "Manage projects"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_material_ui_core__WEBPACK_IMPORTED_MODULE_5__["MenuItem"], {
      onClick: function onClick() {
        return window.location = "/profile/secured-deals";
      }
    }, "Secured deals"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_material_ui_core__WEBPACK_IMPORTED_MODULE_5__["MenuItem"], {
      onClick: function onClick() {
        return window.location = "/profile/wish-list";
      }
    }, "Wish list"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_material_ui_core__WEBPACK_IMPORTED_MODULE_5__["MenuItem"], {
      onClick: function onClick() {
        return handleLogout();
      }
    }, "Sign out"))));
  })))) : /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(react__WEBPACK_IMPORTED_MODULE_1___default.a.Fragment, null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("svg", {
    onClick: function onClick() {
      return setShowMenu(!showMenu);
    },
    className: "absolute cursor-pointer feather-icon h-8 mr-6 right-0 text-white w-8"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("use", {
    xlinkHref: "/assets/svg/feather-sprite.svg#menu"
  })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components_base__WEBPACK_IMPORTED_MODULE_8__["Modal"], {
    show: showMenu,
    onClose: function onClose() {
      return setShowMenu(!showMenu);
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "flex flex-col items-end justify-around\n                "
  }, navLinks(), !Object(_services_auth__WEBPACK_IMPORTED_MODULE_4__["isLoggedIn"])() ? /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
    onClick: function onClick() {
      return userAction.setState({
        showSignIn: true
      });
    },
    className: "cursor-pointer px-3 tracking-wide text-2xl lg:text-base text-white font-bold"
  }, "Sign in") : /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(react__WEBPACK_IMPORTED_MODULE_1___default.a.Fragment, null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("hr", {
    className: "block w-1/4 mb-6 border border-gray-300 opacity-50"
  }), (Object(_services_auth__WEBPACK_IMPORTED_MODULE_4__["userRole"])() === "admin" || Object(_services_auth__WEBPACK_IMPORTED_MODULE_4__["userRole"])() === "super_admin") && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("a", {
    href: "/admin/projects",
    className: "mb-6 cursor-pointer px-3 tracking-wide text-2xl lg:text-base text-white font-bold"
  }, "Admin"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("a", {
    href: "/profile/account-settings",
    className: "mb-6 cursor-pointer px-3 tracking-wide text-2xl lg:text-base text-white font-bold"
  }, "My Profile"), (Object(_services_auth__WEBPACK_IMPORTED_MODULE_4__["userRole"])() === "admin" || Object(_services_auth__WEBPACK_IMPORTED_MODULE_4__["userRole"])() === "super_admin" || Object(_services_auth__WEBPACK_IMPORTED_MODULE_4__["userRole"])() === "project_developer") && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("a", {
    href: "/profile/manage-projects",
    className: "mb-6 cursor-pointer px-3 tracking-wide text-2xl lg:text-base text-white font-bold"
  }, "Manage Project"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("a", {
    href: "/profile/secured-deals",
    className: "mb-6 cursor-pointer px-3 tracking-wide text-2xl lg:text-base text-white font-bold"
  }, "Secured deals"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("a", {
    href: "/profile/wish-list",
    className: "mb-6 cursor-pointer px-3 tracking-wide text-2xl lg:text-base text-white font-bold"
  }, "Wish list"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
    onClick: function onClick() {
      return handleLogout();
    },
    className: "mb-6 cursor-pointer px-3 tracking-wide text-2xl lg:text-base text-white font-bold"
  }, "Sign out")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
    className: "text-white"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components__WEBPACK_IMPORTED_MODULE_9__["SocialMedia"], {
    className: "flex flex-row mb-6 lg:justify-start justify-center mt-6",
    size: "2x"
  }))))));
};

/* harmony default export */ __webpack_exports__["default"] = (Header);

/***/ })

}]);