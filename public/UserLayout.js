(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["UserLayout"],{

/***/ "./node_modules/css-loader/index.js?!./node_modules/postcss-loader/src/index.js?!./resources/js/helpers/styles/datepicker.css":
/*!************************************************************************************************************************************!*\
  !*** ./node_modules/css-loader??ref--6-1!./node_modules/postcss-loader/src??ref--6-2!./resources/js/helpers/styles/datepicker.css ***!
  \************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(/*! ../../../../node_modules/css-loader/lib/css-base.js */ "./node_modules/css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ".react-datepicker-popper[data-placement^=\"bottom\"] .react-datepicker__triangle,\r\n.react-datepicker-popper[data-placement^=\"top\"] .react-datepicker__triangle,\r\n.react-datepicker__year-read-view--down-arrow,\r\n.react-datepicker__month-read-view--down-arrow,\r\n.react-datepicker__month-year-read-view--down-arrow {\n  margin-left: -8px;\n  position: absolute;\n}\n\n.react-datepicker-popper[data-placement^=\"bottom\"] .react-datepicker__triangle,\r\n.react-datepicker-popper[data-placement^=\"top\"] .react-datepicker__triangle,\r\n.react-datepicker__year-read-view--down-arrow,\r\n.react-datepicker__month-read-view--down-arrow,\r\n.react-datepicker__month-year-read-view--down-arrow,\r\n.react-datepicker-popper[data-placement^=\"bottom\"]\r\n    .react-datepicker__triangle::before,\r\n.react-datepicker-popper[data-placement^=\"top\"]\r\n    .react-datepicker__triangle::before,\r\n.react-datepicker__year-read-view--down-arrow::before,\r\n.react-datepicker__month-read-view--down-arrow::before,\r\n.react-datepicker__month-year-read-view--down-arrow::before {\n  box-sizing: content-box;\n  position: absolute;\n  border: 8px solid transparent;\n  height: 0;\n  width: 1px;\n}\n\n.react-datepicker-popper[data-placement^=\"bottom\"]\r\n    .react-datepicker__triangle::before,\r\n.react-datepicker-popper[data-placement^=\"top\"]\r\n    .react-datepicker__triangle::before,\r\n.react-datepicker__year-read-view--down-arrow::before,\r\n.react-datepicker__month-read-view--down-arrow::before,\r\n.react-datepicker__month-year-read-view--down-arrow::before {\n  content: \"\";\n  z-index: -1;\n  border-width: 8px;\n  left: -8px;\n  border-bottom-color: #aeaeae;\n}\n\n.react-datepicker-popper[data-placement^=\"bottom\"] .react-datepicker__triangle {\n  top: 0;\n  margin-top: -8px;\n}\n\n.react-datepicker-popper[data-placement^=\"bottom\"] .react-datepicker__triangle,\r\n.react-datepicker-popper[data-placement^=\"bottom\"]\r\n    .react-datepicker__triangle::before {\n  border-top: none;\n  border-bottom-color: #f0f0f0;\n}\n\n.react-datepicker-popper[data-placement^=\"bottom\"]\r\n    .react-datepicker__triangle::before {\n  top: -1px;\n  border-bottom-color: #aeaeae;\n}\n\n.react-datepicker-popper[data-placement^=\"top\"] .react-datepicker__triangle,\r\n.react-datepicker__year-read-view--down-arrow,\r\n.react-datepicker__month-read-view--down-arrow,\r\n.react-datepicker__month-year-read-view--down-arrow {\n  bottom: 0;\n  margin-bottom: -8px;\n}\n\n.react-datepicker-popper[data-placement^=\"top\"] .react-datepicker__triangle,\r\n.react-datepicker__year-read-view--down-arrow,\r\n.react-datepicker__month-read-view--down-arrow,\r\n.react-datepicker__month-year-read-view--down-arrow,\r\n.react-datepicker-popper[data-placement^=\"top\"]\r\n    .react-datepicker__triangle::before,\r\n.react-datepicker__year-read-view--down-arrow::before,\r\n.react-datepicker__month-read-view--down-arrow::before,\r\n.react-datepicker__month-year-read-view--down-arrow::before {\n  border-bottom: none;\n  border-top-color: #fff;\n}\n\n.react-datepicker-popper[data-placement^=\"top\"]\r\n    .react-datepicker__triangle::before,\r\n.react-datepicker__year-read-view--down-arrow::before,\r\n.react-datepicker__month-read-view--down-arrow::before,\r\n.react-datepicker__month-year-read-view--down-arrow::before {\n  bottom: -1px;\n  border-top-color: #aeaeae;\n}\n\n.react-datepicker-wrapper {\n  display: inline-block;\n  padding: 0;\n  border: 0;\n}\n\n.react-datepicker {\n  font-family: \"Helvetica Neue\", Helvetica, Arial, sans-serif;\n  font-size: 0.8rem;\n  background-color: #fff;\n  color: #000;\n  border: 1px solid #aeaeae;\n  border-radius: 0.3rem;\n  display: inline-block;\n  position: relative;\n}\n\n.react-datepicker--time-only .react-datepicker__triangle {\n  left: 35px;\n}\n\n.react-datepicker--time-only .react-datepicker__time-container {\n  border-left: 0;\n}\n\n.react-datepicker--time-only .react-datepicker__time {\n  border-radius: 0.3rem;\n}\n\n.react-datepicker--time-only .react-datepicker__time-box {\n  border-radius: 0.3rem;\n}\n\n.react-datepicker__triangle {\n  position: absolute;\n  left: 50px;\n}\n\n.react-datepicker-popper {\n  z-index: 1;\n}\n\n.react-datepicker-popper[data-placement^=\"bottom\"] {\n  margin-top: 10px;\n}\n\n.react-datepicker-popper[data-placement=\"bottom-end\"]\r\n    .react-datepicker__triangle,\r\n.react-datepicker-popper[data-placement=\"top-end\"] .react-datepicker__triangle {\n  left: auto;\n  right: 50px;\n}\n\n.react-datepicker-popper[data-placement^=\"top\"] {\n  margin-bottom: 10px;\n}\n\n.react-datepicker-popper[data-placement^=\"right\"] {\n  margin-left: 8px;\n}\n\n.react-datepicker-popper[data-placement^=\"right\"] .react-datepicker__triangle {\n  left: auto;\n  right: 42px;\n}\n\n.react-datepicker-popper[data-placement^=\"left\"] {\n  margin-right: 8px;\n}\n\n.react-datepicker-popper[data-placement^=\"left\"] .react-datepicker__triangle {\n  left: 42px;\n  right: auto;\n}\n\n.react-datepicker__header {\n  text-align: center;\n  background-color: #f0f0f0;\n  border-bottom: 1px solid #aeaeae;\n  border-top-left-radius: 0.3rem;\n  border-top-right-radius: 0.3rem;\n  padding-top: 8px;\n  position: relative;\n}\n\n.react-datepicker__header--time {\n  padding-bottom: 8px;\n  padding-left: 5px;\n  padding-right: 5px;\n}\n\n.react-datepicker__year-dropdown-container--select,\r\n.react-datepicker__month-dropdown-container--select,\r\n.react-datepicker__month-year-dropdown-container--select,\r\n.react-datepicker__year-dropdown-container--scroll,\r\n.react-datepicker__month-dropdown-container--scroll,\r\n.react-datepicker__month-year-dropdown-container--scroll {\n  display: inline-block;\n  margin: 0 2px;\n}\n\n.react-datepicker__current-month,\r\n.react-datepicker-time__header,\r\n.react-datepicker-year-header {\n  margin-top: 0;\n  color: #000;\n  font-weight: bold;\n  font-size: 0.944rem;\n}\n\n.react-datepicker-time__header {\n  text-overflow: ellipsis;\n  white-space: nowrap;\n  overflow: hidden;\n}\n\n.react-datepicker__navigation {\n  background: none;\n  line-height: 1.7rem;\n  text-align: center;\n  cursor: pointer;\n  position: absolute;\n  top: 10px;\n  width: 0;\n  padding: 0;\n  border: 0.45rem solid transparent;\n  z-index: 1;\n  height: 10px;\n  width: 10px;\n  text-indent: -999em;\n  overflow: hidden;\n}\n\n.react-datepicker__navigation--previous {\n  left: 10px;\n  border-right-color: #ccc;\n}\n\n.react-datepicker__navigation--previous:hover {\n  border-right-color: #b3b3b3;\n}\n\n.react-datepicker__navigation--previous--disabled,\r\n.react-datepicker__navigation--previous--disabled:hover {\n  border-right-color: #e6e6e6;\n  cursor: default;\n}\n\n.react-datepicker__navigation--next {\n  right: 10px;\n  border-left-color: #ccc;\n}\n\n.react-datepicker__navigation--next--with-time:not(.react-datepicker__navigation--next--with-today-button) {\n  right: 80px;\n}\n\n.react-datepicker__navigation--next:hover {\n  border-left-color: #b3b3b3;\n}\n\n.react-datepicker__navigation--next--disabled,\r\n.react-datepicker__navigation--next--disabled:hover {\n  border-left-color: #e6e6e6;\n  cursor: default;\n}\n\n.react-datepicker__navigation--years {\n  position: relative;\n  top: 0;\n  display: block;\n  margin-left: auto;\n  margin-right: auto;\n}\n\n.react-datepicker__navigation--years-previous {\n  top: 4px;\n  border-top-color: #ccc;\n}\n\n.react-datepicker__navigation--years-previous:hover {\n  border-top-color: #b3b3b3;\n}\n\n.react-datepicker__navigation--years-upcoming {\n  top: -4px;\n  border-bottom-color: #ccc;\n}\n\n.react-datepicker__navigation--years-upcoming:hover {\n  border-bottom-color: #b3b3b3;\n}\n\n.react-datepicker__month-container {\n  float: left;\n}\n\n.react-datepicker__year-container {\n  margin: 0.4rem;\n  text-align: center;\n  display: flex;\n  flex-wrap: wrap;\n}\n\n.react-datepicker__year-container-text {\n  display: inline-block;\n  cursor: pointer;\n  flex: 1 0 30%;\n  width: 12px;\n  padding: 2px;\n}\n\n.react-datepicker__month {\n  margin: 0.4rem;\n  text-align: center;\n}\n\n.react-datepicker__month .react-datepicker__month-text,\r\n.react-datepicker__month .react-datepicker__quarter-text {\n  display: inline-block;\n  width: 4rem;\n  margin: 2px;\n}\n\n.react-datepicker__input-time-container {\n  clear: both;\n  width: 100%;\n  float: left;\n  margin: 5px 0 10px 15px;\n  text-align: left;\n}\n\n.react-datepicker__input-time-container .react-datepicker-time__caption {\n  display: inline-block;\n}\n\n.react-datepicker__input-time-container\r\n    .react-datepicker-time__input-container {\n  display: inline-block;\n}\n\n.react-datepicker__input-time-container\r\n    .react-datepicker-time__input-container\r\n    .react-datepicker-time__input {\n  display: inline-block;\n  margin-left: 10px;\n}\n\n.react-datepicker__input-time-container\r\n    .react-datepicker-time__input-container\r\n    .react-datepicker-time__input\r\n    input {\n  width: 85px;\n}\n\n.react-datepicker__input-time-container\r\n    .react-datepicker-time__input-container\r\n    .react-datepicker-time__input\r\n    input[type=\"time\"]::-webkit-inner-spin-button,\r\n.react-datepicker__input-time-container\r\n    .react-datepicker-time__input-container\r\n    .react-datepicker-time__input\r\n    input[type=\"time\"]::-webkit-outer-spin-button {\n  -webkit-appearance: none;\n  margin: 0;\n}\n\n.react-datepicker__input-time-container\r\n    .react-datepicker-time__input-container\r\n    .react-datepicker-time__input\r\n    input[type=\"time\"] {\n  -moz-appearance: textfield;\n}\n\n.react-datepicker__input-time-container\r\n    .react-datepicker-time__input-container\r\n    .react-datepicker-time__delimiter {\n  margin-left: 5px;\n  display: inline-block;\n}\n\n.react-datepicker__time-container {\n  float: right;\n  border-left: 1px solid #aeaeae;\n  width: 85px;\n}\n\n.react-datepicker__time-container--with-today-button {\n  display: inline;\n  border: 1px solid #aeaeae;\n  border-radius: 0.3rem;\n  position: absolute;\n  right: -72px;\n  top: 0;\n}\n\n.react-datepicker__time-container .react-datepicker__time {\n  position: relative;\n  background: white;\n}\n\n.react-datepicker__time-container\r\n    .react-datepicker__time\r\n    .react-datepicker__time-box {\n  width: 85px;\n  overflow-x: hidden;\n  margin: 0 auto;\n  text-align: center;\n}\n\n.react-datepicker__time-container\r\n    .react-datepicker__time\r\n    .react-datepicker__time-box\r\n    ul.react-datepicker__time-list {\n  list-style: none;\n  margin: 0;\n  height: calc(195px + (1.7rem / 2));\n  overflow-y: scroll;\n  padding-right: 0px;\n  padding-left: 0px;\n  width: 100%;\n  box-sizing: content-box;\n}\n\n.react-datepicker__time-container\r\n    .react-datepicker__time\r\n    .react-datepicker__time-box\r\n    ul.react-datepicker__time-list\r\n    li.react-datepicker__time-list-item {\n  height: 30px;\n  padding: 5px 10px;\n  white-space: nowrap;\n}\n\n.react-datepicker__time-container\r\n    .react-datepicker__time\r\n    .react-datepicker__time-box\r\n    ul.react-datepicker__time-list\r\n    li.react-datepicker__time-list-item:hover {\n  cursor: pointer;\n  background-color: #f0f0f0;\n}\n\n.react-datepicker__time-container\r\n    .react-datepicker__time\r\n    .react-datepicker__time-box\r\n    ul.react-datepicker__time-list\r\n    li.react-datepicker__time-list-item--selected {\n  background-color: #216ba5;\n  color: white;\n  font-weight: bold;\n}\n\n.react-datepicker__time-container\r\n    .react-datepicker__time\r\n    .react-datepicker__time-box\r\n    ul.react-datepicker__time-list\r\n    li.react-datepicker__time-list-item--selected:hover {\n  background-color: #216ba5;\n}\n\n.react-datepicker__time-container\r\n    .react-datepicker__time\r\n    .react-datepicker__time-box\r\n    ul.react-datepicker__time-list\r\n    li.react-datepicker__time-list-item--disabled {\n  color: #ccc;\n}\n\n.react-datepicker__time-container\r\n    .react-datepicker__time\r\n    .react-datepicker__time-box\r\n    ul.react-datepicker__time-list\r\n    li.react-datepicker__time-list-item--disabled:hover {\n  cursor: default;\n  background-color: transparent;\n}\n\n.react-datepicker__week-number {\n  color: #ccc;\n  display: inline-block;\n  width: 1.7rem;\n  line-height: 1.7rem;\n  text-align: center;\n  margin: 0.166rem;\n}\n\n.react-datepicker__week-number.react-datepicker__week-number--clickable {\n  cursor: pointer;\n}\n\n.react-datepicker__week-number.react-datepicker__week-number--clickable:hover {\n  border-radius: 0.3rem;\n  background-color: #f0f0f0;\n}\n\n.react-datepicker__day-names,\r\n.react-datepicker__week {\n  white-space: nowrap;\n}\n\n.react-datepicker__day-name,\r\n.react-datepicker__day,\r\n.react-datepicker__time-name {\n  color: #000;\n  display: inline-block;\n  width: 1.7rem;\n  line-height: 1.7rem;\n  text-align: center;\n  margin: 0.166rem;\n}\n\n.react-datepicker__month--selected,\r\n.react-datepicker__month--in-selecting-range,\r\n.react-datepicker__month--in-range,\r\n.react-datepicker__quarter--selected,\r\n.react-datepicker__quarter--in-selecting-range,\r\n.react-datepicker__quarter--in-range,\r\n.react-datepicker__year-container-text--selected,\r\n.react-datepicker__year-container-text--in-selecting-range,\r\n.react-datepicker__year-container-text--in-range {\n  border-radius: 0.3rem;\n  background-color: #216ba5;\n  color: #fff;\n}\n\n.react-datepicker__month--selected:hover,\r\n.react-datepicker__month--in-selecting-range:hover,\r\n.react-datepicker__month--in-range:hover,\r\n.react-datepicker__quarter--selected:hover,\r\n.react-datepicker__quarter--in-selecting-range:hover,\r\n.react-datepicker__quarter--in-range:hover,\r\n.react-datepicker__year-container-text--selected:hover,\r\n.react-datepicker__year-container-text--in-selecting-range:hover,\r\n.react-datepicker__year-container-text--in-range:hover {\n  background-color: #1d5d90;\n}\n\n.react-datepicker__month--disabled,\r\n.react-datepicker__quarter--disabled,\r\n.react-datepicker__year-container-text--disabled {\n  color: #ccc;\n  pointer-events: none;\n}\n\n.react-datepicker__month--disabled:hover,\r\n.react-datepicker__quarter--disabled:hover,\r\n.react-datepicker__year-container-text--disabled:hover {\n  cursor: default;\n  background-color: transparent;\n}\n\n.react-datepicker__day,\r\n.react-datepicker__month-text,\r\n.react-datepicker__quarter-text {\n  cursor: pointer;\n}\n\n.react-datepicker__day:hover,\r\n.react-datepicker__month-text:hover,\r\n.react-datepicker__quarter-text:hover {\n  border-radius: 0.3rem;\n  background-color: #f0f0f0;\n}\n\n.react-datepicker__day--today,\r\n.react-datepicker__month-text--today,\r\n.react-datepicker__quarter-text--today {\n  font-weight: bold;\n}\n\n.react-datepicker__day--highlighted,\r\n.react-datepicker__month-text--highlighted,\r\n.react-datepicker__quarter-text--highlighted {\n  border-radius: 0.3rem;\n  background-color: #3dcc4a;\n  color: #fff;\n}\n\n.react-datepicker__day--highlighted:hover,\r\n.react-datepicker__month-text--highlighted:hover,\r\n.react-datepicker__quarter-text--highlighted:hover {\n  background-color: #32be3f;\n}\n\n.react-datepicker__day--highlighted-custom-1,\r\n.react-datepicker__month-text--highlighted-custom-1,\r\n.react-datepicker__quarter-text--highlighted-custom-1 {\n  color: magenta;\n}\n\n.react-datepicker__day--highlighted-custom-2,\r\n.react-datepicker__month-text--highlighted-custom-2,\r\n.react-datepicker__quarter-text--highlighted-custom-2 {\n  color: green;\n}\n\n.react-datepicker__day--selected,\r\n.react-datepicker__day--in-selecting-range,\r\n.react-datepicker__day--in-range,\r\n.react-datepicker__month-text--selected,\r\n.react-datepicker__month-text--in-selecting-range,\r\n.react-datepicker__month-text--in-range,\r\n.react-datepicker__quarter-text--selected,\r\n.react-datepicker__quarter-text--in-selecting-range,\r\n.react-datepicker__quarter-text--in-range {\n  border-radius: 0.3rem;\n  background-color: #216ba5;\n  color: #fff;\n}\n\n.react-datepicker__day--selected:hover,\r\n.react-datepicker__day--in-selecting-range:hover,\r\n.react-datepicker__day--in-range:hover,\r\n.react-datepicker__month-text--selected:hover,\r\n.react-datepicker__month-text--in-selecting-range:hover,\r\n.react-datepicker__month-text--in-range:hover,\r\n.react-datepicker__quarter-text--selected:hover,\r\n.react-datepicker__quarter-text--in-selecting-range:hover,\r\n.react-datepicker__quarter-text--in-range:hover {\n  background-color: #1d5d90;\n}\n\n.react-datepicker__day--keyboard-selected,\r\n.react-datepicker__month-text--keyboard-selected,\r\n.react-datepicker__quarter-text--keyboard-selected {\n  border-radius: 0.3rem;\n  background-color: #2a87d0;\n  color: #fff;\n}\n\n.react-datepicker__day--keyboard-selected:hover,\r\n.react-datepicker__month-text--keyboard-selected:hover,\r\n.react-datepicker__quarter-text--keyboard-selected:hover {\n  background-color: #1d5d90;\n}\n\n.react-datepicker__day--in-selecting-range,\r\n.react-datepicker__month-text--in-selecting-range,\r\n.react-datepicker__quarter-text--in-selecting-range {\n  background-color: rgba(33, 107, 165, 0.5);\n}\n\n.react-datepicker__month--selecting-range .react-datepicker__day--in-range,\r\n.react-datepicker__month--selecting-range\r\n    .react-datepicker__month-text--in-range,\r\n.react-datepicker__month--selecting-range\r\n    .react-datepicker__quarter-text--in-range {\n  background-color: #f0f0f0;\n  color: #000;\n}\n\n.react-datepicker__day--disabled,\r\n.react-datepicker__month-text--disabled,\r\n.react-datepicker__quarter-text--disabled {\n  cursor: default;\n  color: #ccc;\n}\n\n.react-datepicker__day--disabled:hover,\r\n.react-datepicker__month-text--disabled:hover,\r\n.react-datepicker__quarter-text--disabled:hover {\n  background-color: transparent;\n}\n\n.react-datepicker__month-text.react-datepicker__month--selected:hover,\r\n.react-datepicker__month-text.react-datepicker__month--in-range:hover,\r\n.react-datepicker__month-text.react-datepicker__quarter--selected:hover,\r\n.react-datepicker__month-text.react-datepicker__quarter--in-range:hover,\r\n.react-datepicker__quarter-text.react-datepicker__month--selected:hover,\r\n.react-datepicker__quarter-text.react-datepicker__month--in-range:hover,\r\n.react-datepicker__quarter-text.react-datepicker__quarter--selected:hover,\r\n.react-datepicker__quarter-text.react-datepicker__quarter--in-range:hover {\n  background-color: #216ba5;\n}\n\n.react-datepicker__month-text:hover,\r\n.react-datepicker__quarter-text:hover {\n  background-color: #f0f0f0;\n}\n\n.react-datepicker__input-container {\n  position: relative;\n  display: inline-block;\n  width: 100%;\n}\n\n.react-datepicker__year-read-view,\r\n.react-datepicker__month-read-view,\r\n.react-datepicker__month-year-read-view {\n  border: 1px solid transparent;\n  border-radius: 0.3rem;\n}\n\n.react-datepicker__year-read-view:hover,\r\n.react-datepicker__month-read-view:hover,\r\n.react-datepicker__month-year-read-view:hover {\n  cursor: pointer;\n}\n\n.react-datepicker__year-read-view:hover\r\n    .react-datepicker__year-read-view--down-arrow,\r\n.react-datepicker__year-read-view:hover\r\n    .react-datepicker__month-read-view--down-arrow,\r\n.react-datepicker__month-read-view:hover\r\n    .react-datepicker__year-read-view--down-arrow,\r\n.react-datepicker__month-read-view:hover\r\n    .react-datepicker__month-read-view--down-arrow,\r\n.react-datepicker__month-year-read-view:hover\r\n    .react-datepicker__year-read-view--down-arrow,\r\n.react-datepicker__month-year-read-view:hover\r\n    .react-datepicker__month-read-view--down-arrow {\n  border-top-color: #b3b3b3;\n}\n\n.react-datepicker__year-read-view--down-arrow,\r\n.react-datepicker__month-read-view--down-arrow,\r\n.react-datepicker__month-year-read-view--down-arrow {\n  border-top-color: #ccc;\n  float: right;\n  margin-left: 20px;\n  top: 8px;\n  position: relative;\n  border-width: 0.45rem;\n}\n\n.react-datepicker__year-dropdown,\r\n.react-datepicker__month-dropdown,\r\n.react-datepicker__month-year-dropdown {\n  background-color: #f0f0f0;\n  position: absolute;\n  width: 50%;\n  left: 25%;\n  top: 30px;\n  z-index: 1;\n  text-align: center;\n  border-radius: 0.3rem;\n  border: 1px solid #aeaeae;\n}\n\n.react-datepicker__year-dropdown:hover,\r\n.react-datepicker__month-dropdown:hover,\r\n.react-datepicker__month-year-dropdown:hover {\n  cursor: pointer;\n}\n\n.react-datepicker__year-dropdown--scrollable,\r\n.react-datepicker__month-dropdown--scrollable,\r\n.react-datepicker__month-year-dropdown--scrollable {\n  height: 150px;\n  overflow-y: scroll;\n}\n\n.react-datepicker__year-option,\r\n.react-datepicker__month-option,\r\n.react-datepicker__month-year-option {\n  line-height: 20px;\n  width: 100%;\n  display: block;\n  margin-left: auto;\n  margin-right: auto;\n}\n\n.react-datepicker__year-option:first-of-type,\r\n.react-datepicker__month-option:first-of-type,\r\n.react-datepicker__month-year-option:first-of-type {\n  border-top-left-radius: 0.3rem;\n  border-top-right-radius: 0.3rem;\n}\n\n.react-datepicker__year-option:last-of-type,\r\n.react-datepicker__month-option:last-of-type,\r\n.react-datepicker__month-year-option:last-of-type {\n  -webkit-user-select: none;\n  -moz-user-select: none;\n  -ms-user-select: none;\n  user-select: none;\n  border-bottom-left-radius: 0.3rem;\n  border-bottom-right-radius: 0.3rem;\n}\n\n.react-datepicker__year-option:hover,\r\n.react-datepicker__month-option:hover,\r\n.react-datepicker__month-year-option:hover {\n  background-color: #ccc;\n}\n\n.react-datepicker__year-option:hover\r\n    .react-datepicker__navigation--years-upcoming,\r\n.react-datepicker__month-option:hover\r\n    .react-datepicker__navigation--years-upcoming,\r\n.react-datepicker__month-year-option:hover\r\n    .react-datepicker__navigation--years-upcoming {\n  border-bottom-color: #b3b3b3;\n}\n\n.react-datepicker__year-option:hover\r\n    .react-datepicker__navigation--years-previous,\r\n.react-datepicker__month-option:hover\r\n    .react-datepicker__navigation--years-previous,\r\n.react-datepicker__month-year-option:hover\r\n    .react-datepicker__navigation--years-previous {\n  border-top-color: #b3b3b3;\n}\n\n.react-datepicker__year-option--selected,\r\n.react-datepicker__month-option--selected,\r\n.react-datepicker__month-year-option--selected {\n  position: absolute;\n  left: 15px;\n}\n\n.react-datepicker__close-icon {\n  cursor: pointer;\n  background-color: transparent;\n  border: 0;\n  outline: 0;\n  padding: 0px 6px 0px 0px;\n  position: absolute;\n  top: 0;\n  right: 0;\n  height: 100%;\n  display: table-cell;\n  vertical-align: middle;\n}\n\n.react-datepicker__close-icon::after {\n  cursor: pointer;\n  background-color: #216ba5;\n  color: #fff;\n  border-radius: 50%;\n  height: 16px;\n  width: 16px;\n  padding: 2px;\n  font-size: 12px;\n  line-height: 1;\n  text-align: center;\n  display: table-cell;\n  vertical-align: middle;\n  content: \"\\D7\";\n}\n\n.react-datepicker__today-button {\n  background: #f0f0f0;\n  border-top: 1px solid #aeaeae;\n  cursor: pointer;\n  text-align: center;\n  font-weight: bold;\n  padding: 5px 0;\n  clear: left;\n}\n\n.react-datepicker__portal {\n  position: fixed;\n  width: 100vw;\n  height: 100vh;\n  background-color: rgba(0, 0, 0, 0.8);\n  left: 0;\n  top: 0;\n  justify-content: center;\n  align-items: center;\n  display: flex;\n  z-index: 2147483647;\n}\n\n.react-datepicker__portal .react-datepicker__day-name,\r\n.react-datepicker__portal .react-datepicker__day,\r\n.react-datepicker__portal .react-datepicker__time-name {\n  width: 3rem;\n  line-height: 3rem;\n}\n\n@media (max-width: 400px), (max-height: 550px) {\n  .react-datepicker__portal .react-datepicker__day-name,\r\n    .react-datepicker__portal .react-datepicker__day,\r\n    .react-datepicker__portal .react-datepicker__time-name {\n    width: 2rem;\n    line-height: 2rem;\n  }\n}\n\n.react-datepicker__portal .react-datepicker__current-month,\r\n.react-datepicker__portal .react-datepicker-time__header {\n  font-size: 1.44rem;\n}\n\n.react-datepicker__portal .react-datepicker__navigation {\n  border: 0.81rem solid transparent;\n}\n\n.react-datepicker__portal .react-datepicker__navigation--previous {\n  border-right-color: #ccc;\n}\n\n.react-datepicker__portal .react-datepicker__navigation--previous:hover {\n  border-right-color: #b3b3b3;\n}\n\n.react-datepicker__portal .react-datepicker__navigation--previous--disabled,\r\n.react-datepicker__portal\r\n    .react-datepicker__navigation--previous--disabled:hover {\n  border-right-color: #e6e6e6;\n  cursor: default;\n}\n\n.react-datepicker__portal .react-datepicker__navigation--next {\n  border-left-color: #ccc;\n}\n\n.react-datepicker__portal .react-datepicker__navigation--next:hover {\n  border-left-color: #b3b3b3;\n}\n\n.react-datepicker__portal .react-datepicker__navigation--next--disabled,\r\n.react-datepicker__portal .react-datepicker__navigation--next--disabled:hover {\n  border-left-color: #e6e6e6;\n  cursor: default;\n}\n\n.react-datepicker__input-container input {\n  border: 1px solid #ccc;\n  color: black;\n  font-size: small;\n  cursor: pointer;\n  background-color: white;\n}\r\n", ""]);

// exports


/***/ }),

/***/ "./node_modules/css-loader/index.js?!./node_modules/postcss-loader/src/index.js?!./resources/js/helpers/styles/dropdown.css":
/*!**********************************************************************************************************************************!*\
  !*** ./node_modules/css-loader??ref--6-1!./node_modules/postcss-loader/src??ref--6-2!./resources/js/helpers/styles/dropdown.css ***!
  \**********************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(/*! ../../../../node_modules/css-loader/lib/css-base.js */ "./node_modules/css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ".Dropdown-root {\n  position: relative;\n  width: 20rem;\n}\n\n.Dropdown-control {\n  position: relative;\n  overflow: hidden;\n  background-color: white;\n  border: 1px solid #ccc;\n  box-sizing: border-box;\n  color: black;\n  font-size: small;\n  cursor: default;\n  outline: none;\n  padding: 4px 52px 4px 10px;\n  transition: all 200ms ease;\n}\n\n.Dropdown-control:hover {\n  box-shadow: 0 1px 0 rgba(0, 0, 0, 0.06);\n}\n\n.Dropdown-arrow {\n  border-color: gray transparent transparent;\n  border-style: solid;\n  border-width: 4px 4px 0;\n  content: \" \";\n  display: block;\n  height: 0;\n  margin-top: -ceil(2.5);\n  position: absolute;\n  right: 10px;\n  top: 11px;\n  width: 0;\n}\n\n.is-open .Dropdown-arrow {\n  border-color: transparent transparent gray;\n  border-width: 0 4px 4px;\n}\n\n.Dropdown-menu {\n  background-color: white;\n  border: 1px solid #ccc;\n  box-shadow: 0 1px 0 rgba(0, 0, 0, 0.06);\n  box-sizing: border-box;\n  margin-top: -1px;\n  max-height: 200px;\n  overflow-y: auto;\n  position: absolute;\n  top: 100%;\n  width: 100%;\n  z-index: 1000;\n  -webkit-overflow-scrolling: touch;\n}\n\n.Dropdown-menu .Dropdown-group > .Dropdown-title {\n  padding: 8px 10px;\n  color: black;\n  font-weight: bold;\n  text-transform: capitalize;\n}\n\n.Dropdown-option {\n  box-sizing: border-box;\n  color: black;\n  cursor: pointer;\n  display: block;\n  padding: 8px 10px;\n}\n\n.Dropdown-option:last-child {\n  border-bottom-right-radius: 2px;\n  border-bottom-left-radius: 2px;\n}\n\n.Dropdown-option:hover {\n  background-color: rgb(138, 141, 141);\n  color: black;\n}\n\n.Dropdown-option.is-selected {\n  background-color: rgb(128, 120, 141);\n  color: black;\n}\n\n.Dropdown-noresults {\n  box-sizing: border-box;\n  color: black;\n  cursor: default;\n  display: block;\n  padding: 8px 10px;\n}\r\n", ""]);

// exports


/***/ }),

/***/ "./node_modules/css-loader/index.js?!./node_modules/postcss-loader/src/index.js?!./resources/js/helpers/styles/toast.css":
/*!*******************************************************************************************************************************!*\
  !*** ./node_modules/css-loader??ref--6-1!./node_modules/postcss-loader/src??ref--6-2!./resources/js/helpers/styles/toast.css ***!
  \*******************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(/*! ../../../../node_modules/css-loader/lib/css-base.js */ "./node_modules/css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ".Toastify__toast-container {\n  z-index: 9999;\n  -webkit-transform: translate3d(0, 0, 9999px);\n  position: fixed;\n  padding: 4px;\n  width: 320px;\n  box-sizing: border-box;\n  color: #fff;\n}\n\n.Toastify__toast-container--top-left {\n  top: 1em;\n  left: 1em;\n}\n\n.Toastify__toast-container--top-center {\n  top: 1em;\n  left: 50%;\n  transform: translateX(-50%);\n}\n\n.Toastify__toast-container--top-right {\n  top: 1em;\n  right: 1em;\n}\n\n.Toastify__toast-container--bottom-left {\n  bottom: 1em;\n  left: 1em;\n}\n\n.Toastify__toast-container--bottom-center {\n  bottom: 1em;\n  left: 50%;\n  transform: translateX(-50%);\n}\n\n.Toastify__toast-container--bottom-right {\n  bottom: 1em;\n  right: 1em;\n}\n\n@media only screen and (max-width: 480px) {\n  .Toastify__toast-container {\n    width: 100vw;\n    padding: 0;\n    left: 0;\n    margin: 0;\n  }\n\n  .Toastify__toast-container--top-left,\r\n    .Toastify__toast-container--top-center,\r\n    .Toastify__toast-container--top-right {\n    top: 0;\n    transform: translateX(0);\n  }\n\n  .Toastify__toast-container--bottom-left,\r\n    .Toastify__toast-container--bottom-center,\r\n    .Toastify__toast-container--bottom-right {\n    bottom: 0;\n    transform: translateX(0);\n  }\n\n  .Toastify__toast-container--rtl {\n    right: 0;\n    left: initial;\n  }\n}\n\n.Toastify__toast {\n  position: relative;\n  min-height: 64px;\n  box-sizing: border-box;\n  margin-bottom: 1rem;\n  padding: 8px;\n  border-radius: 1px;\n  box-shadow: 0 1px 10px 0 rgba(0, 0, 0, 0.1),\r\n        0 2px 15px 0 rgba(0, 0, 0, 0.05);\n  display: flex;\n  justify-content: space-between;\n  max-height: 800px;\n  overflow: hidden;\n  cursor: pointer;\n  direction: ltr;\n}\n\n.Toastify__toast--rtl {\n  direction: rtl;\n}\n\n.Toastify__toast--dark {\n  background: #121212;\n  color: #fff;\n}\n\n.Toastify__toast--default {\n  background: #fff;\n  color: #aaa;\n}\n\n.Toastify__toast--info {\n  background: #3498db;\n}\n\n.Toastify__toast--success {\n  background: #07bc0c;\n}\n\n.Toastify__toast--warning {\n  background: #f1c40f;\n}\n\n.Toastify__toast--error {\n  background: #e74c3c;\n}\n\n.Toastify__toast-body {\n  margin: auto 0;\n  flex: 1 1 auto;\n}\n\n@media only screen and (max-width: 480px) {\n  .Toastify__toast {\n    margin-bottom: 0;\n  }\n}\n\n.Toastify__close-button {\n  color: #fff;\n  background: transparent;\n  outline: none;\n  border: none;\n  padding: 0;\n  cursor: pointer;\n  opacity: 0.7;\n  transition: 0.3s ease;\n  align-self: flex-start;\n}\n\n.Toastify__close-button--default {\n  color: #000;\n  opacity: 0.3;\n}\n\n.Toastify__close-button > svg {\n  fill: currentColor;\n  height: 16px;\n  width: 14px;\n}\n\n.Toastify__close-button:hover,\r\n.Toastify__close-button:focus {\n  opacity: 1;\n}\n\n@-webkit-keyframes Toastify__trackProgress {\n  0% {\n    transform: scaleX(1);\n  }\n\n  100% {\n    transform: scaleX(0);\n  }\n}\n\n@keyframes Toastify__trackProgress {\n  0% {\n    transform: scaleX(1);\n  }\n\n  100% {\n    transform: scaleX(0);\n  }\n}\n\n.Toastify__progress-bar {\n  position: absolute;\n  bottom: 0;\n  left: 0;\n  width: 100%;\n  height: 5px;\n  z-index: 9999;\n  opacity: 0.7;\n  background-color: rgba(255, 255, 255, 0.7);\n  transform-origin: left;\n}\n\n.Toastify__progress-bar--animated {\n  -webkit-animation: Toastify__trackProgress linear 1 forwards;\n          animation: Toastify__trackProgress linear 1 forwards;\n}\n\n.Toastify__progress-bar--controlled {\n  transition: transform 0.2s;\n}\n\n.Toastify__progress-bar--rtl {\n  right: 0;\n  left: initial;\n  transform-origin: right;\n}\n\n.Toastify__progress-bar--default {\n  background: linear-gradient(\r\n        to right,\r\n        #4cd964,\r\n        #5ac8fa,\r\n        #007aff,\r\n        #34aadc,\r\n        #5856d6,\r\n        #ff2d55\r\n    );\n}\n\n.Toastify__progress-bar--dark {\n  background: #bb86fc;\n}\n\n@-webkit-keyframes Toastify__bounceInRight {\n  from,\r\n    60%,\r\n    75%,\r\n    90%,\r\n    to {\n    -webkit-animation-timing-function: cubic-bezier(0.215, 0.61, 0.355, 1);\n            animation-timing-function: cubic-bezier(0.215, 0.61, 0.355, 1);\n  }\n\n  from {\n    opacity: 0;\n    transform: translate3d(3000px, 0, 0);\n  }\n\n  60% {\n    opacity: 1;\n    transform: translate3d(-25px, 0, 0);\n  }\n\n  75% {\n    transform: translate3d(10px, 0, 0);\n  }\n\n  90% {\n    transform: translate3d(-5px, 0, 0);\n  }\n\n  to {\n    transform: none;\n  }\n}\n\n@keyframes Toastify__bounceInRight {\n  from,\r\n    60%,\r\n    75%,\r\n    90%,\r\n    to {\n    -webkit-animation-timing-function: cubic-bezier(0.215, 0.61, 0.355, 1);\n            animation-timing-function: cubic-bezier(0.215, 0.61, 0.355, 1);\n  }\n\n  from {\n    opacity: 0;\n    transform: translate3d(3000px, 0, 0);\n  }\n\n  60% {\n    opacity: 1;\n    transform: translate3d(-25px, 0, 0);\n  }\n\n  75% {\n    transform: translate3d(10px, 0, 0);\n  }\n\n  90% {\n    transform: translate3d(-5px, 0, 0);\n  }\n\n  to {\n    transform: none;\n  }\n}\n\n@-webkit-keyframes Toastify__bounceOutRight {\n  20% {\n    opacity: 1;\n    transform: translate3d(-20px, 0, 0);\n  }\n\n  to {\n    opacity: 0;\n    transform: translate3d(2000px, 0, 0);\n  }\n}\n\n@keyframes Toastify__bounceOutRight {\n  20% {\n    opacity: 1;\n    transform: translate3d(-20px, 0, 0);\n  }\n\n  to {\n    opacity: 0;\n    transform: translate3d(2000px, 0, 0);\n  }\n}\n\n@-webkit-keyframes Toastify__bounceInLeft {\n  from,\r\n    60%,\r\n    75%,\r\n    90%,\r\n    to {\n    -webkit-animation-timing-function: cubic-bezier(0.215, 0.61, 0.355, 1);\n            animation-timing-function: cubic-bezier(0.215, 0.61, 0.355, 1);\n  }\n\n  0% {\n    opacity: 0;\n    transform: translate3d(-3000px, 0, 0);\n  }\n\n  60% {\n    opacity: 1;\n    transform: translate3d(25px, 0, 0);\n  }\n\n  75% {\n    transform: translate3d(-10px, 0, 0);\n  }\n\n  90% {\n    transform: translate3d(5px, 0, 0);\n  }\n\n  to {\n    transform: none;\n  }\n}\n\n@keyframes Toastify__bounceInLeft {\n  from,\r\n    60%,\r\n    75%,\r\n    90%,\r\n    to {\n    -webkit-animation-timing-function: cubic-bezier(0.215, 0.61, 0.355, 1);\n            animation-timing-function: cubic-bezier(0.215, 0.61, 0.355, 1);\n  }\n\n  0% {\n    opacity: 0;\n    transform: translate3d(-3000px, 0, 0);\n  }\n\n  60% {\n    opacity: 1;\n    transform: translate3d(25px, 0, 0);\n  }\n\n  75% {\n    transform: translate3d(-10px, 0, 0);\n  }\n\n  90% {\n    transform: translate3d(5px, 0, 0);\n  }\n\n  to {\n    transform: none;\n  }\n}\n\n@-webkit-keyframes Toastify__bounceOutLeft {\n  20% {\n    opacity: 1;\n    transform: translate3d(20px, 0, 0);\n  }\n\n  to {\n    opacity: 0;\n    transform: translate3d(-2000px, 0, 0);\n  }\n}\n\n@keyframes Toastify__bounceOutLeft {\n  20% {\n    opacity: 1;\n    transform: translate3d(20px, 0, 0);\n  }\n\n  to {\n    opacity: 0;\n    transform: translate3d(-2000px, 0, 0);\n  }\n}\n\n@-webkit-keyframes Toastify__bounceInUp {\n  from,\r\n    60%,\r\n    75%,\r\n    90%,\r\n    to {\n    -webkit-animation-timing-function: cubic-bezier(0.215, 0.61, 0.355, 1);\n            animation-timing-function: cubic-bezier(0.215, 0.61, 0.355, 1);\n  }\n\n  from {\n    opacity: 0;\n    transform: translate3d(0, 3000px, 0);\n  }\n\n  60% {\n    opacity: 1;\n    transform: translate3d(0, -20px, 0);\n  }\n\n  75% {\n    transform: translate3d(0, 10px, 0);\n  }\n\n  90% {\n    transform: translate3d(0, -5px, 0);\n  }\n\n  to {\n    transform: translate3d(0, 0, 0);\n  }\n}\n\n@keyframes Toastify__bounceInUp {\n  from,\r\n    60%,\r\n    75%,\r\n    90%,\r\n    to {\n    -webkit-animation-timing-function: cubic-bezier(0.215, 0.61, 0.355, 1);\n            animation-timing-function: cubic-bezier(0.215, 0.61, 0.355, 1);\n  }\n\n  from {\n    opacity: 0;\n    transform: translate3d(0, 3000px, 0);\n  }\n\n  60% {\n    opacity: 1;\n    transform: translate3d(0, -20px, 0);\n  }\n\n  75% {\n    transform: translate3d(0, 10px, 0);\n  }\n\n  90% {\n    transform: translate3d(0, -5px, 0);\n  }\n\n  to {\n    transform: translate3d(0, 0, 0);\n  }\n}\n\n@-webkit-keyframes Toastify__bounceOutUp {\n  20% {\n    transform: translate3d(0, -10px, 0);\n  }\n\n  40%,\r\n    45% {\n    opacity: 1;\n    transform: translate3d(0, 20px, 0);\n  }\n\n  to {\n    opacity: 0;\n    transform: translate3d(0, -2000px, 0);\n  }\n}\n\n@keyframes Toastify__bounceOutUp {\n  20% {\n    transform: translate3d(0, -10px, 0);\n  }\n\n  40%,\r\n    45% {\n    opacity: 1;\n    transform: translate3d(0, 20px, 0);\n  }\n\n  to {\n    opacity: 0;\n    transform: translate3d(0, -2000px, 0);\n  }\n}\n\n@-webkit-keyframes Toastify__bounceInDown {\n  from,\r\n    60%,\r\n    75%,\r\n    90%,\r\n    to {\n    -webkit-animation-timing-function: cubic-bezier(0.215, 0.61, 0.355, 1);\n            animation-timing-function: cubic-bezier(0.215, 0.61, 0.355, 1);\n  }\n\n  0% {\n    opacity: 0;\n    transform: translate3d(0, -3000px, 0);\n  }\n\n  60% {\n    opacity: 1;\n    transform: translate3d(0, 25px, 0);\n  }\n\n  75% {\n    transform: translate3d(0, -10px, 0);\n  }\n\n  90% {\n    transform: translate3d(0, 5px, 0);\n  }\n\n  to {\n    transform: none;\n  }\n}\n\n@keyframes Toastify__bounceInDown {\n  from,\r\n    60%,\r\n    75%,\r\n    90%,\r\n    to {\n    -webkit-animation-timing-function: cubic-bezier(0.215, 0.61, 0.355, 1);\n            animation-timing-function: cubic-bezier(0.215, 0.61, 0.355, 1);\n  }\n\n  0% {\n    opacity: 0;\n    transform: translate3d(0, -3000px, 0);\n  }\n\n  60% {\n    opacity: 1;\n    transform: translate3d(0, 25px, 0);\n  }\n\n  75% {\n    transform: translate3d(0, -10px, 0);\n  }\n\n  90% {\n    transform: translate3d(0, 5px, 0);\n  }\n\n  to {\n    transform: none;\n  }\n}\n\n@-webkit-keyframes Toastify__bounceOutDown {\n  20% {\n    transform: translate3d(0, 10px, 0);\n  }\n\n  40%,\r\n    45% {\n    opacity: 1;\n    transform: translate3d(0, -20px, 0);\n  }\n\n  to {\n    opacity: 0;\n    transform: translate3d(0, 2000px, 0);\n  }\n}\n\n@keyframes Toastify__bounceOutDown {\n  20% {\n    transform: translate3d(0, 10px, 0);\n  }\n\n  40%,\r\n    45% {\n    opacity: 1;\n    transform: translate3d(0, -20px, 0);\n  }\n\n  to {\n    opacity: 0;\n    transform: translate3d(0, 2000px, 0);\n  }\n}\n\n.Toastify__bounce-enter--top-left,\r\n.Toastify__bounce-enter--bottom-left {\n  -webkit-animation-name: Toastify__bounceInLeft;\n          animation-name: Toastify__bounceInLeft;\n}\n\n.Toastify__bounce-enter--top-right,\r\n.Toastify__bounce-enter--bottom-right {\n  -webkit-animation-name: Toastify__bounceInRight;\n          animation-name: Toastify__bounceInRight;\n}\n\n.Toastify__bounce-enter--top-center {\n  -webkit-animation-name: Toastify__bounceInDown;\n          animation-name: Toastify__bounceInDown;\n}\n\n.Toastify__bounce-enter--bottom-center {\n  -webkit-animation-name: Toastify__bounceInUp;\n          animation-name: Toastify__bounceInUp;\n}\n\n.Toastify__bounce-exit--top-left,\r\n.Toastify__bounce-exit--bottom-left {\n  -webkit-animation-name: Toastify__bounceOutLeft;\n          animation-name: Toastify__bounceOutLeft;\n}\n\n.Toastify__bounce-exit--top-right,\r\n.Toastify__bounce-exit--bottom-right {\n  -webkit-animation-name: Toastify__bounceOutRight;\n          animation-name: Toastify__bounceOutRight;\n}\n\n.Toastify__bounce-exit--top-center {\n  -webkit-animation-name: Toastify__bounceOutUp;\n          animation-name: Toastify__bounceOutUp;\n}\n\n.Toastify__bounce-exit--bottom-center {\n  -webkit-animation-name: Toastify__bounceOutDown;\n          animation-name: Toastify__bounceOutDown;\n}\n\n@-webkit-keyframes Toastify__zoomIn {\n  from {\n    opacity: 0;\n    transform: scale3d(0.3, 0.3, 0.3);\n  }\n\n  50% {\n    opacity: 1;\n  }\n}\n\n@keyframes Toastify__zoomIn {\n  from {\n    opacity: 0;\n    transform: scale3d(0.3, 0.3, 0.3);\n  }\n\n  50% {\n    opacity: 1;\n  }\n}\n\n@-webkit-keyframes Toastify__zoomOut {\n  from {\n    opacity: 1;\n  }\n\n  50% {\n    opacity: 0;\n    transform: scale3d(0.3, 0.3, 0.3);\n  }\n\n  to {\n    opacity: 0;\n  }\n}\n\n@keyframes Toastify__zoomOut {\n  from {\n    opacity: 1;\n  }\n\n  50% {\n    opacity: 0;\n    transform: scale3d(0.3, 0.3, 0.3);\n  }\n\n  to {\n    opacity: 0;\n  }\n}\n\n.Toastify__zoom-enter {\n  -webkit-animation-name: Toastify__zoomIn;\n          animation-name: Toastify__zoomIn;\n}\n\n.Toastify__zoom-exit {\n  -webkit-animation-name: Toastify__zoomOut;\n          animation-name: Toastify__zoomOut;\n}\n\n@-webkit-keyframes Toastify__flipIn {\n  from {\n    transform: perspective(400px) rotate3d(1, 0, 0, 90deg);\n    -webkit-animation-timing-function: ease-in;\n            animation-timing-function: ease-in;\n    opacity: 0;\n  }\n\n  40% {\n    transform: perspective(400px) rotate3d(1, 0, 0, -20deg);\n    -webkit-animation-timing-function: ease-in;\n            animation-timing-function: ease-in;\n  }\n\n  60% {\n    transform: perspective(400px) rotate3d(1, 0, 0, 10deg);\n    opacity: 1;\n  }\n\n  80% {\n    transform: perspective(400px) rotate3d(1, 0, 0, -5deg);\n  }\n\n  to {\n    transform: perspective(400px);\n  }\n}\n\n@keyframes Toastify__flipIn {\n  from {\n    transform: perspective(400px) rotate3d(1, 0, 0, 90deg);\n    -webkit-animation-timing-function: ease-in;\n            animation-timing-function: ease-in;\n    opacity: 0;\n  }\n\n  40% {\n    transform: perspective(400px) rotate3d(1, 0, 0, -20deg);\n    -webkit-animation-timing-function: ease-in;\n            animation-timing-function: ease-in;\n  }\n\n  60% {\n    transform: perspective(400px) rotate3d(1, 0, 0, 10deg);\n    opacity: 1;\n  }\n\n  80% {\n    transform: perspective(400px) rotate3d(1, 0, 0, -5deg);\n  }\n\n  to {\n    transform: perspective(400px);\n  }\n}\n\n@-webkit-keyframes Toastify__flipOut {\n  from {\n    transform: perspective(400px);\n  }\n\n  30% {\n    transform: perspective(400px) rotate3d(1, 0, 0, -20deg);\n    opacity: 1;\n  }\n\n  to {\n    transform: perspective(400px) rotate3d(1, 0, 0, 90deg);\n    opacity: 0;\n  }\n}\n\n@keyframes Toastify__flipOut {\n  from {\n    transform: perspective(400px);\n  }\n\n  30% {\n    transform: perspective(400px) rotate3d(1, 0, 0, -20deg);\n    opacity: 1;\n  }\n\n  to {\n    transform: perspective(400px) rotate3d(1, 0, 0, 90deg);\n    opacity: 0;\n  }\n}\n\n.Toastify__flip-enter {\n  -webkit-animation-name: Toastify__flipIn;\n          animation-name: Toastify__flipIn;\n}\n\n.Toastify__flip-exit {\n  -webkit-animation-name: Toastify__flipOut;\n          animation-name: Toastify__flipOut;\n}\n\n@-webkit-keyframes Toastify__slideInRight {\n  from {\n    transform: translate3d(110%, 0, 0);\n    visibility: visible;\n  }\n\n  to {\n    transform: translate3d(0, 0, 0);\n  }\n}\n\n@keyframes Toastify__slideInRight {\n  from {\n    transform: translate3d(110%, 0, 0);\n    visibility: visible;\n  }\n\n  to {\n    transform: translate3d(0, 0, 0);\n  }\n}\n\n@-webkit-keyframes Toastify__slideInLeft {\n  from {\n    transform: translate3d(-110%, 0, 0);\n    visibility: visible;\n  }\n\n  to {\n    transform: translate3d(0, 0, 0);\n  }\n}\n\n@keyframes Toastify__slideInLeft {\n  from {\n    transform: translate3d(-110%, 0, 0);\n    visibility: visible;\n  }\n\n  to {\n    transform: translate3d(0, 0, 0);\n  }\n}\n\n@-webkit-keyframes Toastify__slideInUp {\n  from {\n    transform: translate3d(0, 110%, 0);\n    visibility: visible;\n  }\n\n  to {\n    transform: translate3d(0, 0, 0);\n  }\n}\n\n@keyframes Toastify__slideInUp {\n  from {\n    transform: translate3d(0, 110%, 0);\n    visibility: visible;\n  }\n\n  to {\n    transform: translate3d(0, 0, 0);\n  }\n}\n\n@-webkit-keyframes Toastify__slideInDown {\n  from {\n    transform: translate3d(0, -110%, 0);\n    visibility: visible;\n  }\n\n  to {\n    transform: translate3d(0, 0, 0);\n  }\n}\n\n@keyframes Toastify__slideInDown {\n  from {\n    transform: translate3d(0, -110%, 0);\n    visibility: visible;\n  }\n\n  to {\n    transform: translate3d(0, 0, 0);\n  }\n}\n\n@-webkit-keyframes Toastify__slideOutRight {\n  from {\n    transform: translate3d(0, 0, 0);\n  }\n\n  to {\n    visibility: hidden;\n    transform: translate3d(110%, 0, 0);\n  }\n}\n\n@keyframes Toastify__slideOutRight {\n  from {\n    transform: translate3d(0, 0, 0);\n  }\n\n  to {\n    visibility: hidden;\n    transform: translate3d(110%, 0, 0);\n  }\n}\n\n@-webkit-keyframes Toastify__slideOutLeft {\n  from {\n    transform: translate3d(0, 0, 0);\n  }\n\n  to {\n    visibility: hidden;\n    transform: translate3d(-110%, 0, 0);\n  }\n}\n\n@keyframes Toastify__slideOutLeft {\n  from {\n    transform: translate3d(0, 0, 0);\n  }\n\n  to {\n    visibility: hidden;\n    transform: translate3d(-110%, 0, 0);\n  }\n}\n\n@-webkit-keyframes Toastify__slideOutDown {\n  from {\n    transform: translate3d(0, 0, 0);\n  }\n\n  to {\n    visibility: hidden;\n    transform: translate3d(0, 500px, 0);\n  }\n}\n\n@keyframes Toastify__slideOutDown {\n  from {\n    transform: translate3d(0, 0, 0);\n  }\n\n  to {\n    visibility: hidden;\n    transform: translate3d(0, 500px, 0);\n  }\n}\n\n@-webkit-keyframes Toastify__slideOutUp {\n  from {\n    transform: translate3d(0, 0, 0);\n  }\n\n  to {\n    visibility: hidden;\n    transform: translate3d(0, -500px, 0);\n  }\n}\n\n@keyframes Toastify__slideOutUp {\n  from {\n    transform: translate3d(0, 0, 0);\n  }\n\n  to {\n    visibility: hidden;\n    transform: translate3d(0, -500px, 0);\n  }\n}\n\n.Toastify__slide-enter--top-left,\r\n.Toastify__slide-enter--bottom-left {\n  -webkit-animation-name: Toastify__slideInLeft;\n          animation-name: Toastify__slideInLeft;\n}\n\n.Toastify__slide-enter--top-right,\r\n.Toastify__slide-enter--bottom-right {\n  -webkit-animation-name: Toastify__slideInRight;\n          animation-name: Toastify__slideInRight;\n}\n\n.Toastify__slide-enter--top-center {\n  -webkit-animation-name: Toastify__slideInDown;\n          animation-name: Toastify__slideInDown;\n}\n\n.Toastify__slide-enter--bottom-center {\n  -webkit-animation-name: Toastify__slideInUp;\n          animation-name: Toastify__slideInUp;\n}\n\n.Toastify__slide-exit--top-left,\r\n.Toastify__slide-exit--bottom-left {\n  -webkit-animation-name: Toastify__slideOutLeft;\n          animation-name: Toastify__slideOutLeft;\n}\n\n.Toastify__slide-exit--top-right,\r\n.Toastify__slide-exit--bottom-right {\n  -webkit-animation-name: Toastify__slideOutRight;\n          animation-name: Toastify__slideOutRight;\n}\n\n.Toastify__slide-exit--top-center {\n  -webkit-animation-name: Toastify__slideOutUp;\n          animation-name: Toastify__slideOutUp;\n}\n\n.Toastify__slide-exit--bottom-center {\n  -webkit-animation-name: Toastify__slideOutDown;\n          animation-name: Toastify__slideOutDown;\n}\r\n", ""]);

// exports


/***/ }),

/***/ "./node_modules/moment/locale sync recursive ^\\.\\/.*$":
/*!**************************************************!*\
  !*** ./node_modules/moment/locale sync ^\.\/.*$ ***!
  \**************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"./af": "./node_modules/moment/locale/af.js",
	"./af.js": "./node_modules/moment/locale/af.js",
	"./ar": "./node_modules/moment/locale/ar.js",
	"./ar-dz": "./node_modules/moment/locale/ar-dz.js",
	"./ar-dz.js": "./node_modules/moment/locale/ar-dz.js",
	"./ar-kw": "./node_modules/moment/locale/ar-kw.js",
	"./ar-kw.js": "./node_modules/moment/locale/ar-kw.js",
	"./ar-ly": "./node_modules/moment/locale/ar-ly.js",
	"./ar-ly.js": "./node_modules/moment/locale/ar-ly.js",
	"./ar-ma": "./node_modules/moment/locale/ar-ma.js",
	"./ar-ma.js": "./node_modules/moment/locale/ar-ma.js",
	"./ar-sa": "./node_modules/moment/locale/ar-sa.js",
	"./ar-sa.js": "./node_modules/moment/locale/ar-sa.js",
	"./ar-tn": "./node_modules/moment/locale/ar-tn.js",
	"./ar-tn.js": "./node_modules/moment/locale/ar-tn.js",
	"./ar.js": "./node_modules/moment/locale/ar.js",
	"./az": "./node_modules/moment/locale/az.js",
	"./az.js": "./node_modules/moment/locale/az.js",
	"./be": "./node_modules/moment/locale/be.js",
	"./be.js": "./node_modules/moment/locale/be.js",
	"./bg": "./node_modules/moment/locale/bg.js",
	"./bg.js": "./node_modules/moment/locale/bg.js",
	"./bm": "./node_modules/moment/locale/bm.js",
	"./bm.js": "./node_modules/moment/locale/bm.js",
	"./bn": "./node_modules/moment/locale/bn.js",
	"./bn.js": "./node_modules/moment/locale/bn.js",
	"./bo": "./node_modules/moment/locale/bo.js",
	"./bo.js": "./node_modules/moment/locale/bo.js",
	"./br": "./node_modules/moment/locale/br.js",
	"./br.js": "./node_modules/moment/locale/br.js",
	"./bs": "./node_modules/moment/locale/bs.js",
	"./bs.js": "./node_modules/moment/locale/bs.js",
	"./ca": "./node_modules/moment/locale/ca.js",
	"./ca.js": "./node_modules/moment/locale/ca.js",
	"./cs": "./node_modules/moment/locale/cs.js",
	"./cs.js": "./node_modules/moment/locale/cs.js",
	"./cv": "./node_modules/moment/locale/cv.js",
	"./cv.js": "./node_modules/moment/locale/cv.js",
	"./cy": "./node_modules/moment/locale/cy.js",
	"./cy.js": "./node_modules/moment/locale/cy.js",
	"./da": "./node_modules/moment/locale/da.js",
	"./da.js": "./node_modules/moment/locale/da.js",
	"./de": "./node_modules/moment/locale/de.js",
	"./de-at": "./node_modules/moment/locale/de-at.js",
	"./de-at.js": "./node_modules/moment/locale/de-at.js",
	"./de-ch": "./node_modules/moment/locale/de-ch.js",
	"./de-ch.js": "./node_modules/moment/locale/de-ch.js",
	"./de.js": "./node_modules/moment/locale/de.js",
	"./dv": "./node_modules/moment/locale/dv.js",
	"./dv.js": "./node_modules/moment/locale/dv.js",
	"./el": "./node_modules/moment/locale/el.js",
	"./el.js": "./node_modules/moment/locale/el.js",
	"./en-au": "./node_modules/moment/locale/en-au.js",
	"./en-au.js": "./node_modules/moment/locale/en-au.js",
	"./en-ca": "./node_modules/moment/locale/en-ca.js",
	"./en-ca.js": "./node_modules/moment/locale/en-ca.js",
	"./en-gb": "./node_modules/moment/locale/en-gb.js",
	"./en-gb.js": "./node_modules/moment/locale/en-gb.js",
	"./en-ie": "./node_modules/moment/locale/en-ie.js",
	"./en-ie.js": "./node_modules/moment/locale/en-ie.js",
	"./en-il": "./node_modules/moment/locale/en-il.js",
	"./en-il.js": "./node_modules/moment/locale/en-il.js",
	"./en-in": "./node_modules/moment/locale/en-in.js",
	"./en-in.js": "./node_modules/moment/locale/en-in.js",
	"./en-nz": "./node_modules/moment/locale/en-nz.js",
	"./en-nz.js": "./node_modules/moment/locale/en-nz.js",
	"./en-sg": "./node_modules/moment/locale/en-sg.js",
	"./en-sg.js": "./node_modules/moment/locale/en-sg.js",
	"./eo": "./node_modules/moment/locale/eo.js",
	"./eo.js": "./node_modules/moment/locale/eo.js",
	"./es": "./node_modules/moment/locale/es.js",
	"./es-do": "./node_modules/moment/locale/es-do.js",
	"./es-do.js": "./node_modules/moment/locale/es-do.js",
	"./es-us": "./node_modules/moment/locale/es-us.js",
	"./es-us.js": "./node_modules/moment/locale/es-us.js",
	"./es.js": "./node_modules/moment/locale/es.js",
	"./et": "./node_modules/moment/locale/et.js",
	"./et.js": "./node_modules/moment/locale/et.js",
	"./eu": "./node_modules/moment/locale/eu.js",
	"./eu.js": "./node_modules/moment/locale/eu.js",
	"./fa": "./node_modules/moment/locale/fa.js",
	"./fa.js": "./node_modules/moment/locale/fa.js",
	"./fi": "./node_modules/moment/locale/fi.js",
	"./fi.js": "./node_modules/moment/locale/fi.js",
	"./fil": "./node_modules/moment/locale/fil.js",
	"./fil.js": "./node_modules/moment/locale/fil.js",
	"./fo": "./node_modules/moment/locale/fo.js",
	"./fo.js": "./node_modules/moment/locale/fo.js",
	"./fr": "./node_modules/moment/locale/fr.js",
	"./fr-ca": "./node_modules/moment/locale/fr-ca.js",
	"./fr-ca.js": "./node_modules/moment/locale/fr-ca.js",
	"./fr-ch": "./node_modules/moment/locale/fr-ch.js",
	"./fr-ch.js": "./node_modules/moment/locale/fr-ch.js",
	"./fr.js": "./node_modules/moment/locale/fr.js",
	"./fy": "./node_modules/moment/locale/fy.js",
	"./fy.js": "./node_modules/moment/locale/fy.js",
	"./ga": "./node_modules/moment/locale/ga.js",
	"./ga.js": "./node_modules/moment/locale/ga.js",
	"./gd": "./node_modules/moment/locale/gd.js",
	"./gd.js": "./node_modules/moment/locale/gd.js",
	"./gl": "./node_modules/moment/locale/gl.js",
	"./gl.js": "./node_modules/moment/locale/gl.js",
	"./gom-deva": "./node_modules/moment/locale/gom-deva.js",
	"./gom-deva.js": "./node_modules/moment/locale/gom-deva.js",
	"./gom-latn": "./node_modules/moment/locale/gom-latn.js",
	"./gom-latn.js": "./node_modules/moment/locale/gom-latn.js",
	"./gu": "./node_modules/moment/locale/gu.js",
	"./gu.js": "./node_modules/moment/locale/gu.js",
	"./he": "./node_modules/moment/locale/he.js",
	"./he.js": "./node_modules/moment/locale/he.js",
	"./hi": "./node_modules/moment/locale/hi.js",
	"./hi.js": "./node_modules/moment/locale/hi.js",
	"./hr": "./node_modules/moment/locale/hr.js",
	"./hr.js": "./node_modules/moment/locale/hr.js",
	"./hu": "./node_modules/moment/locale/hu.js",
	"./hu.js": "./node_modules/moment/locale/hu.js",
	"./hy-am": "./node_modules/moment/locale/hy-am.js",
	"./hy-am.js": "./node_modules/moment/locale/hy-am.js",
	"./id": "./node_modules/moment/locale/id.js",
	"./id.js": "./node_modules/moment/locale/id.js",
	"./is": "./node_modules/moment/locale/is.js",
	"./is.js": "./node_modules/moment/locale/is.js",
	"./it": "./node_modules/moment/locale/it.js",
	"./it-ch": "./node_modules/moment/locale/it-ch.js",
	"./it-ch.js": "./node_modules/moment/locale/it-ch.js",
	"./it.js": "./node_modules/moment/locale/it.js",
	"./ja": "./node_modules/moment/locale/ja.js",
	"./ja.js": "./node_modules/moment/locale/ja.js",
	"./jv": "./node_modules/moment/locale/jv.js",
	"./jv.js": "./node_modules/moment/locale/jv.js",
	"./ka": "./node_modules/moment/locale/ka.js",
	"./ka.js": "./node_modules/moment/locale/ka.js",
	"./kk": "./node_modules/moment/locale/kk.js",
	"./kk.js": "./node_modules/moment/locale/kk.js",
	"./km": "./node_modules/moment/locale/km.js",
	"./km.js": "./node_modules/moment/locale/km.js",
	"./kn": "./node_modules/moment/locale/kn.js",
	"./kn.js": "./node_modules/moment/locale/kn.js",
	"./ko": "./node_modules/moment/locale/ko.js",
	"./ko.js": "./node_modules/moment/locale/ko.js",
	"./ku": "./node_modules/moment/locale/ku.js",
	"./ku.js": "./node_modules/moment/locale/ku.js",
	"./ky": "./node_modules/moment/locale/ky.js",
	"./ky.js": "./node_modules/moment/locale/ky.js",
	"./lb": "./node_modules/moment/locale/lb.js",
	"./lb.js": "./node_modules/moment/locale/lb.js",
	"./lo": "./node_modules/moment/locale/lo.js",
	"./lo.js": "./node_modules/moment/locale/lo.js",
	"./lt": "./node_modules/moment/locale/lt.js",
	"./lt.js": "./node_modules/moment/locale/lt.js",
	"./lv": "./node_modules/moment/locale/lv.js",
	"./lv.js": "./node_modules/moment/locale/lv.js",
	"./me": "./node_modules/moment/locale/me.js",
	"./me.js": "./node_modules/moment/locale/me.js",
	"./mi": "./node_modules/moment/locale/mi.js",
	"./mi.js": "./node_modules/moment/locale/mi.js",
	"./mk": "./node_modules/moment/locale/mk.js",
	"./mk.js": "./node_modules/moment/locale/mk.js",
	"./ml": "./node_modules/moment/locale/ml.js",
	"./ml.js": "./node_modules/moment/locale/ml.js",
	"./mn": "./node_modules/moment/locale/mn.js",
	"./mn.js": "./node_modules/moment/locale/mn.js",
	"./mr": "./node_modules/moment/locale/mr.js",
	"./mr.js": "./node_modules/moment/locale/mr.js",
	"./ms": "./node_modules/moment/locale/ms.js",
	"./ms-my": "./node_modules/moment/locale/ms-my.js",
	"./ms-my.js": "./node_modules/moment/locale/ms-my.js",
	"./ms.js": "./node_modules/moment/locale/ms.js",
	"./mt": "./node_modules/moment/locale/mt.js",
	"./mt.js": "./node_modules/moment/locale/mt.js",
	"./my": "./node_modules/moment/locale/my.js",
	"./my.js": "./node_modules/moment/locale/my.js",
	"./nb": "./node_modules/moment/locale/nb.js",
	"./nb.js": "./node_modules/moment/locale/nb.js",
	"./ne": "./node_modules/moment/locale/ne.js",
	"./ne.js": "./node_modules/moment/locale/ne.js",
	"./nl": "./node_modules/moment/locale/nl.js",
	"./nl-be": "./node_modules/moment/locale/nl-be.js",
	"./nl-be.js": "./node_modules/moment/locale/nl-be.js",
	"./nl.js": "./node_modules/moment/locale/nl.js",
	"./nn": "./node_modules/moment/locale/nn.js",
	"./nn.js": "./node_modules/moment/locale/nn.js",
	"./oc-lnc": "./node_modules/moment/locale/oc-lnc.js",
	"./oc-lnc.js": "./node_modules/moment/locale/oc-lnc.js",
	"./pa-in": "./node_modules/moment/locale/pa-in.js",
	"./pa-in.js": "./node_modules/moment/locale/pa-in.js",
	"./pl": "./node_modules/moment/locale/pl.js",
	"./pl.js": "./node_modules/moment/locale/pl.js",
	"./pt": "./node_modules/moment/locale/pt.js",
	"./pt-br": "./node_modules/moment/locale/pt-br.js",
	"./pt-br.js": "./node_modules/moment/locale/pt-br.js",
	"./pt.js": "./node_modules/moment/locale/pt.js",
	"./ro": "./node_modules/moment/locale/ro.js",
	"./ro.js": "./node_modules/moment/locale/ro.js",
	"./ru": "./node_modules/moment/locale/ru.js",
	"./ru.js": "./node_modules/moment/locale/ru.js",
	"./sd": "./node_modules/moment/locale/sd.js",
	"./sd.js": "./node_modules/moment/locale/sd.js",
	"./se": "./node_modules/moment/locale/se.js",
	"./se.js": "./node_modules/moment/locale/se.js",
	"./si": "./node_modules/moment/locale/si.js",
	"./si.js": "./node_modules/moment/locale/si.js",
	"./sk": "./node_modules/moment/locale/sk.js",
	"./sk.js": "./node_modules/moment/locale/sk.js",
	"./sl": "./node_modules/moment/locale/sl.js",
	"./sl.js": "./node_modules/moment/locale/sl.js",
	"./sq": "./node_modules/moment/locale/sq.js",
	"./sq.js": "./node_modules/moment/locale/sq.js",
	"./sr": "./node_modules/moment/locale/sr.js",
	"./sr-cyrl": "./node_modules/moment/locale/sr-cyrl.js",
	"./sr-cyrl.js": "./node_modules/moment/locale/sr-cyrl.js",
	"./sr.js": "./node_modules/moment/locale/sr.js",
	"./ss": "./node_modules/moment/locale/ss.js",
	"./ss.js": "./node_modules/moment/locale/ss.js",
	"./sv": "./node_modules/moment/locale/sv.js",
	"./sv.js": "./node_modules/moment/locale/sv.js",
	"./sw": "./node_modules/moment/locale/sw.js",
	"./sw.js": "./node_modules/moment/locale/sw.js",
	"./ta": "./node_modules/moment/locale/ta.js",
	"./ta.js": "./node_modules/moment/locale/ta.js",
	"./te": "./node_modules/moment/locale/te.js",
	"./te.js": "./node_modules/moment/locale/te.js",
	"./tet": "./node_modules/moment/locale/tet.js",
	"./tet.js": "./node_modules/moment/locale/tet.js",
	"./tg": "./node_modules/moment/locale/tg.js",
	"./tg.js": "./node_modules/moment/locale/tg.js",
	"./th": "./node_modules/moment/locale/th.js",
	"./th.js": "./node_modules/moment/locale/th.js",
	"./tl-ph": "./node_modules/moment/locale/tl-ph.js",
	"./tl-ph.js": "./node_modules/moment/locale/tl-ph.js",
	"./tlh": "./node_modules/moment/locale/tlh.js",
	"./tlh.js": "./node_modules/moment/locale/tlh.js",
	"./tr": "./node_modules/moment/locale/tr.js",
	"./tr.js": "./node_modules/moment/locale/tr.js",
	"./tzl": "./node_modules/moment/locale/tzl.js",
	"./tzl.js": "./node_modules/moment/locale/tzl.js",
	"./tzm": "./node_modules/moment/locale/tzm.js",
	"./tzm-latn": "./node_modules/moment/locale/tzm-latn.js",
	"./tzm-latn.js": "./node_modules/moment/locale/tzm-latn.js",
	"./tzm.js": "./node_modules/moment/locale/tzm.js",
	"./ug-cn": "./node_modules/moment/locale/ug-cn.js",
	"./ug-cn.js": "./node_modules/moment/locale/ug-cn.js",
	"./uk": "./node_modules/moment/locale/uk.js",
	"./uk.js": "./node_modules/moment/locale/uk.js",
	"./ur": "./node_modules/moment/locale/ur.js",
	"./ur.js": "./node_modules/moment/locale/ur.js",
	"./uz": "./node_modules/moment/locale/uz.js",
	"./uz-latn": "./node_modules/moment/locale/uz-latn.js",
	"./uz-latn.js": "./node_modules/moment/locale/uz-latn.js",
	"./uz.js": "./node_modules/moment/locale/uz.js",
	"./vi": "./node_modules/moment/locale/vi.js",
	"./vi.js": "./node_modules/moment/locale/vi.js",
	"./x-pseudo": "./node_modules/moment/locale/x-pseudo.js",
	"./x-pseudo.js": "./node_modules/moment/locale/x-pseudo.js",
	"./yo": "./node_modules/moment/locale/yo.js",
	"./yo.js": "./node_modules/moment/locale/yo.js",
	"./zh-cn": "./node_modules/moment/locale/zh-cn.js",
	"./zh-cn.js": "./node_modules/moment/locale/zh-cn.js",
	"./zh-hk": "./node_modules/moment/locale/zh-hk.js",
	"./zh-hk.js": "./node_modules/moment/locale/zh-hk.js",
	"./zh-mo": "./node_modules/moment/locale/zh-mo.js",
	"./zh-mo.js": "./node_modules/moment/locale/zh-mo.js",
	"./zh-tw": "./node_modules/moment/locale/zh-tw.js",
	"./zh-tw.js": "./node_modules/moment/locale/zh-tw.js"
};


function webpackContext(req) {
	var id = webpackContextResolve(req);
	return __webpack_require__(id);
}
function webpackContextResolve(req) {
	if(!__webpack_require__.o(map, req)) {
		var e = new Error("Cannot find module '" + req + "'");
		e.code = 'MODULE_NOT_FOUND';
		throw e;
	}
	return map[req];
}
webpackContext.keys = function webpackContextKeys() {
	return Object.keys(map);
};
webpackContext.resolve = webpackContextResolve;
module.exports = webpackContext;
webpackContext.id = "./node_modules/moment/locale sync recursive ^\\.\\/.*$";

/***/ }),

/***/ "./resources/js/admin/components/EditProjectResources.js":
/*!***************************************************************!*\
  !*** ./resources/js/admin/components/EditProjectResources.js ***!
  \***************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime/regenerator */ "./node_modules/@babel/runtime/regenerator/index.js");
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _fortawesome_react_fontawesome__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @fortawesome/react-fontawesome */ "./node_modules/@fortawesome/react-fontawesome/index.es.js");
/* harmony import */ var _states_userGlobal__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../states/userGlobal */ "./resources/js/states/userGlobal.js");
/* harmony import */ var _helpers_resourceHelper_index__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../helpers/resourceHelper/index */ "./resources/js/helpers/resourceHelper/index.js");
/* harmony import */ var _components_base__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../components/_base */ "./resources/js/components/_base/index.js");
/* harmony import */ var _components_base__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(_components_base__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var _components_resources_ResourcesData__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../components/resources/ResourcesData */ "./resources/js/components/resources/ResourcesData.js");
/* harmony import */ var _components_resources_LandResourcesData__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../components/resources/LandResourcesData */ "./resources/js/components/resources/LandResourcesData.js");
/* harmony import */ var _data_index__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../../data/index */ "./resources/js/data/index.js");
/* harmony import */ var _components_base_alerts_sweetAlert__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../../components/_base/alerts/sweetAlert */ "./resources/js/components/_base/alerts/sweetAlert.js");


function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _unsupportedIterableToArray(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

function _iterableToArrayLimit(arr, i) { if (typeof Symbol === "undefined" || !(Symbol.iterator in Object(arr))) return; var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }











var EditProjectResources = function EditProjectResources(_ref) {
  var _userState$projectDat;

  var toggleFetch = _ref.toggleFetch;

  var _UserGlobal = Object(_states_userGlobal__WEBPACK_IMPORTED_MODULE_3__["default"])(),
      _UserGlobal2 = _slicedToArray(_UserGlobal, 2),
      userState = _UserGlobal2[0],
      userActions = _UserGlobal2[1];

  var _useState = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])((_userState$projectDat = userState.projectData.resources) !== null && _userState$projectDat !== void 0 ? _userState$projectDat : []),
      _useState2 = _slicedToArray(_useState, 2),
      projectResources = _useState2[0],
      setProjectResources = _useState2[1];

  var _useState3 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])([]),
      _useState4 = _slicedToArray(_useState3, 2),
      resourcesHolder = _useState4[0],
      setResourcesHolder = _useState4[1];

  var _useState5 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(false),
      _useState6 = _slicedToArray(_useState5, 2),
      loading = _useState6[0],
      setLoading = _useState6[1];

  var resourceForm = Object(react__WEBPACK_IMPORTED_MODULE_1__["useRef"])(null);

  var _useState7 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(userState.projectData.builder_profile_link ? "text" : "file"),
      _useState8 = _slicedToArray(_useState7, 2),
      builderProfile = _useState8[0],
      setBuilderProfile = _useState8[1];

  var _useState9 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(""),
      _useState10 = _slicedToArray(_useState9, 2),
      builderProfileLink = _useState10[0],
      setBuilderProfileLink = _useState10[1];

  var _useState11 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(userState.projectData.architect_profile_link ? "text" : "file"),
      _useState12 = _slicedToArray(_useState11, 2),
      arcProfile = _useState12[0],
      setArcProfile = _useState12[1];

  var _useState13 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(""),
      _useState14 = _slicedToArray(_useState13, 2),
      arcProfileLink = _useState14[0],
      setArcProfileLink = _useState14[1];

  var _useState15 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(userState.projectData.video_link ? userState.projectData.video_link : ""),
      _useState16 = _slicedToArray(_useState15, 2),
      projectVideo = _useState16[0],
      setProjectVideo = _useState16[1];

  var _useState17 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(userState.projectData.developer_profile_link ? "text" : "file"),
      _useState18 = _slicedToArray(_useState17, 2),
      devProfile = _useState18[0],
      setDevProfile = _useState18[1];

  var _useState19 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(""),
      _useState20 = _slicedToArray(_useState19, 2),
      devProfileLink = _useState20[0],
      setDevProfileLink = _useState20[1];

  Object(react__WEBPACK_IMPORTED_MODULE_1__["useEffect"])(function () {
    if (userState.projectData.architect_profile_link) {
      setArcProfileLink(userState.projectData.architect_profile_link);
    }

    if (userState.projectData.builder_profile_link) {
      setBuilderProfileLink(userState.projectData.builder_profile_link);
    }

    if (userState.projectData.developer_profile_link) {
      setDevProfileLink(userState.projectData.developer_profile_link);
    }
  }, []);

  var handleSubmit = function handleSubmit(e) {
    e.preventDefault();
    saveProjectResources();
  };

  var saveProjectResources = /*#__PURE__*/function () {
    var _ref2 = _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee() {
      var formData, url;
      return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee$(_context) {
        while (1) {
          switch (_context.prev = _context.next) {
            case 0:
              formData = new FormData(resourceForm.current);
              projectResources.forEach(function (resource, key) {
                if (arcProfile === "text" && resource.name === "architect_profile") {
                  return;
                }

                if (builderProfile === "text" && resource.name === "builder_profile") {
                  return;
                }

                if (devProfile === "text" && resource.name === "developer_profile") {
                  return;
                }

                formData.append("resources[".concat(key, "][file]"), resource.file);
                formData.append("resources[".concat(key, "][name]"), resource.name);
              });

              if (devProfileLink) {
                formData.append("devProfileLink", _helpers_resourceHelper_index__WEBPACK_IMPORTED_MODULE_4__["resources"].appendHttpsToResource(devProfileLink));
              }

              if (arcProfileLink) {
                formData.append("arcProfileLink", _helpers_resourceHelper_index__WEBPACK_IMPORTED_MODULE_4__["resources"].appendHttpsToResource(arcProfileLink));
              }

              if (builderProfileLink) {
                formData.append("builderProfileLink", _helpers_resourceHelper_index__WEBPACK_IMPORTED_MODULE_4__["resources"].appendHttpsToResource(builderProfileLink));
              }

              setLoading(true);
              formData.append("id", userState.projectData.id);
              formData.append("name", userState.projectData.name);
              formData.append("projectVideo", projectVideo);
              url = "/api/project-resource/update";
              _context.next = 12;
              return Object(_data_index__WEBPACK_IMPORTED_MODULE_8__["saveUpdateResources"])({
                formData: formData,
                url: url
              }).then(function (r) {
                if (r.status === 200) {
                  toggleFetch();
                  setLoading(false);
                  Object(_components_base_alerts_sweetAlert__WEBPACK_IMPORTED_MODULE_9__["sweetAlert"])("success", "Project resources successfully updated.");
                  userActions.setState({
                    projectData: r.data
                  });
                  userActions.setState({
                    propertiesData: r.data.properties
                  });
                }
              })["catch"](function (err) {
                setLoading(false);
              }).then(function () {});

            case 12:
            case "end":
              return _context.stop();
          }
        }
      }, _callee);
    }));

    return function saveProjectResources() {
      return _ref2.apply(this, arguments);
    };
  }();

  var handleFileChange = function handleFileChange(e, resource) {
    e.preventDefault();
    var c = projectResources.filter(function (file) {
      if (file["name"] !== resource) {
        return file;
      }
    });

    if (e.target.files.length > 0) {
      setProjectResources(c.concat(_helpers_resourceHelper_index__WEBPACK_IMPORTED_MODULE_4__["resources"].trimFilename(e.target.files[0], resource)));
      e.target.value = null;
    }
  };

  var handleDelete = function handleDelete(e, resource) {
    e.preventDefault();
    setProjectResources(projectResources.filter(function (file) {
      return file["name"] !== resource;
    }));
    setResourcesHolder(resourcesHolder.filter(function (file) {
      return file["name"] !== resource;
    }));
    e.target.value = null;
  };

  var getFileName = function getFileName(resource) {
    var filename = "";
    projectResources.forEach(function (file) {
      if (file["name"] === resource) {
        if (typeof file["file"].name == "string") {
          filename = file["file"]["name"];
        } else {
          var first = file["file"].split("/".concat(resource, "/"))[1];
          filename = first.split("?")[0];
        }
      }
    });
    return filename;
  };

  var changeInputType = function changeInputType(rowName, checked) {
    if (rowName === "Builder Profile") {
      if (checked) {
        setBuilderProfile("text");
      } else {
        setBuilderProfileLink("");
        setBuilderProfile("file");
      }
    }

    if (rowName === "Architect Profile") {
      if (checked) {
        setArcProfile("text");
      } else {
        setArcProfileLink("");
        setArcProfile("file");
      }
    }

    if (rowName === "Developer Profile") {
      if (checked) {
        setDevProfile("text");
      } else {
        setDevProfileLink("");
        setDevProfile("file");
      }
    }
  };

  var setCheckedState = function setCheckedState(row) {
    var retval = false;

    if (row.label === "Builder Profile") {
      if (builderProfile === "text") {
        retval = true;
      }
    }

    if (row.label === "Architect Profile") {
      if (arcProfile === "text") {
        retval = true;
      }
    }

    if (row.label === "Developer Profile") {
      if (devProfile === "text") {
        retval = true;
      }
    }

    return retval;
  };

  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "bg-white rounded-lg py-5 lg:px-8 px-4 lg:m5 my-5"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("form", {
    ref: resourceForm,
    onSubmit: handleSubmit
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "overflow-x-auto"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("table", {
    className: "w-full"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("thead", {
    className: "w-full"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("tr", null, _helpers_resourceHelper_index__WEBPACK_IMPORTED_MODULE_4__["tableHeaders"].map(function (header, key) {
    return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("th", {
      key: key,
      className: " ".concat(header.style, " border-b font-bold lg:px-4 px-1 py-2 text-palette-gray py-2 ").concat(header.width, " ")
    }, header.label);
  }))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("tbody", {
    className: "w-full"
  }, userState.projectData && userState.projectData.sub_property_id == "4" ? /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components_resources_LandResourcesData__WEBPACK_IMPORTED_MODULE_7__["default"], {
    devProfile: devProfile,
    devProfileLink: devProfileLink,
    getFileName: getFileName,
    setCheckedState: setCheckedState,
    changeInputType: changeInputType,
    handleFileChange: handleFileChange,
    handleDelete: handleDelete,
    setProjectVideo: setProjectVideo,
    projectVideoDefaultValue: projectVideo,
    setDevProfileLink: setDevProfileLink
  }) : /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components_resources_ResourcesData__WEBPACK_IMPORTED_MODULE_6__["default"], {
    builderProfile: builderProfile,
    devProfile: devProfile,
    devProfileLink: devProfileLink,
    arcProfile: arcProfile,
    getFileName: getFileName,
    setCheckedState: setCheckedState,
    changeInputType: changeInputType,
    handleFileChange: handleFileChange,
    handleDelete: handleDelete,
    setProjectVideo: setProjectVideo,
    projectVideoDefaultValue: projectVideo,
    setDevProfileLink: setDevProfileLink
  })))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "flex justify-center lg:justify-end my-5"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components_base__WEBPACK_IMPORTED_MODULE_5__["Button"], {
    className: "block button button-primary rounded-full",
    disabled: loading
  }, loading && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_fortawesome_react_fontawesome__WEBPACK_IMPORTED_MODULE_2__["FontAwesomeIcon"], {
    icon: ["fas", "spinner"],
    className: "fa-spin mr-2"
  }), "Continue"))));
};

/* harmony default export */ __webpack_exports__["default"] = (EditProjectResources);

/***/ }),

/***/ "./resources/js/components sync .*\\.(js)$":
/*!*************************************************************!*\
  !*** ./resources/js/components sync nonrecursive .*\.(js)$ ***!
  \*************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"./AddLandProperty.js": "./resources/js/components/AddLandProperty.js",
	"./AddProject.js": "./resources/js/components/AddProject.js",
	"./AddProjectResources.js": "./resources/js/components/AddProjectResources.js",
	"./AddPropertyModal.js": "./resources/js/components/AddPropertyModal.js",
	"./AddingSteps.js": "./resources/js/components/AddingSteps.js",
	"./AdditionalInfos.js": "./resources/js/components/AdditionalInfos.js",
	"./AgencyAgreementPart.js": "./resources/js/components/AgencyAgreementPart.js",
	"./AgentDetailsSection.js": "./resources/js/components/AgentDetailsSection.js",
	"./BookInspection.js": "./resources/js/components/BookInspection.js",
	"./BuyingProgress.js": "./resources/js/components/BuyingProgress.js",
	"./ContactForm.js": "./resources/js/components/ContactForm.js",
	"./Deposit.js": "./resources/js/components/Deposit.js",
	"./DeveloperPropertyManager.js": "./resources/js/components/DeveloperPropertyManager.js",
	"./EditLandPropertyModal.js": "./resources/js/components/EditLandPropertyModal.js",
	"./EditProjectPart.js": "./resources/js/components/EditProjectPart.js",
	"./EditPropertyModal.js": "./resources/js/components/EditPropertyModal.js",
	"./EditSteps.js": "./resources/js/components/EditSteps.js",
	"./EstimatedCompletionDate.js": "./resources/js/components/EstimatedCompletionDate.js",
	"./ForgetPassword.js": "./resources/js/components/ForgetPassword.js",
	"./GoogleMaps.js": "./resources/js/components/GoogleMaps.js",
	"./LandProjectSize.js": "./resources/js/components/LandProjectSize.js",
	"./LoaderComponent.js": "./resources/js/components/LoaderComponent.js",
	"./MediaPlayer.js": "./resources/js/components/MediaPlayer.js",
	"./MediaPlayerContainer.js": "./resources/js/components/MediaPlayerContainer.js",
	"./MemberCount.js": "./resources/js/components/MemberCount.js",
	"./Message.js": "./resources/js/components/Message.js",
	"./OutgoingCosts.js": "./resources/js/components/OutgoingCosts.js",
	"./PaypalButton.js": "./resources/js/components/PaypalButton.js",
	"./PlayButton.js": "./resources/js/components/PlayButton.js",
	"./PurchaserDetails.js": "./resources/js/components/PurchaserDetails.js",
	"./ReadyForOccupancy.js": "./resources/js/components/ReadyForOccupancy.js",
	"./ResourcesComponent.js": "./resources/js/components/ResourcesComponent.js",
	"./SalesAdviceModal.js": "./resources/js/components/SalesAdviceModal.js",
	"./SalesAdvicePropertyDetails.js": "./resources/js/components/SalesAdvicePropertyDetails.js",
	"./ScrollArrow.js": "./resources/js/components/ScrollArrow.js",
	"./ScrollToBottomArrow.js": "./resources/js/components/ScrollToBottomArrow.js",
	"./SecureDeal.js": "./resources/js/components/SecureDeal.js",
	"./SellerApply.js": "./resources/js/components/SellerApply.js",
	"./SequenceAddingModal.js": "./resources/js/components/SequenceAddingModal.js",
	"./SequenceEditingModal.js": "./resources/js/components/SequenceEditingModal.js",
	"./SignIn.js": "./resources/js/components/SignIn.js",
	"./SignUp.js": "./resources/js/components/SignUp.js",
	"./SocialMedia.js": "./resources/js/components/SocialMedia.js",
	"./SocialMediaShareModal.js": "./resources/js/components/SocialMediaShareModal.js",
	"./SolicitorDetails.js": "./resources/js/components/SolicitorDetails.js",
	"./TermsAndConditionsModal.js": "./resources/js/components/TermsAndConditionsModal.js",
	"./TotalSize.js": "./resources/js/components/TotalSize.js",
	"./index.js": "./resources/js/components/index.js"
};


function webpackContext(req) {
	var id = webpackContextResolve(req);
	return __webpack_require__(id);
}
function webpackContextResolve(req) {
	if(!__webpack_require__.o(map, req)) {
		var e = new Error("Cannot find module '" + req + "'");
		e.code = 'MODULE_NOT_FOUND';
		throw e;
	}
	return map[req];
}
webpackContext.keys = function webpackContextKeys() {
	return Object.keys(map);
};
webpackContext.resolve = webpackContextResolve;
module.exports = webpackContext;
webpackContext.id = "./resources/js/components sync .*\\.(js)$";

/***/ }),

/***/ "./resources/js/components/AddLandProperty.js":
/*!****************************************************!*\
  !*** ./resources/js/components/AddLandProperty.js ***!
  \****************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react_dropdown__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react-dropdown */ "./node_modules/react-dropdown/dist/index.js");
/* harmony import */ var react_dropdown__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react_dropdown__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _helpers_styles_dropdown_css__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../helpers/styles/dropdown.css */ "./resources/js/helpers/styles/dropdown.css");
/* harmony import */ var _helpers_styles_dropdown_css__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(_helpers_styles_dropdown_css__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _base__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./_base */ "./resources/js/components/_base/index.js");
/* harmony import */ var _base__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(_base__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var draft_js__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! draft-js */ "./node_modules/draft-js/lib/Draft.js");
/* harmony import */ var draft_js__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(draft_js__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var draft_convert__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! draft-convert */ "./node_modules/draft-convert/esm/index.js");
/* harmony import */ var react_currency_format__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! react-currency-format */ "./node_modules/react-currency-format/lib/currency-format.js");
/* harmony import */ var react_currency_format__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(react_currency_format__WEBPACK_IMPORTED_MODULE_6__);
/* harmony import */ var _states_userGlobal__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../states/userGlobal */ "./resources/js/states/userGlobal.js");
/* harmony import */ var _helpers_styles_toast_css__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../helpers/styles/toast.css */ "./resources/js/helpers/styles/toast.css");
/* harmony import */ var _helpers_styles_toast_css__WEBPACK_IMPORTED_MODULE_8___default = /*#__PURE__*/__webpack_require__.n(_helpers_styles_toast_css__WEBPACK_IMPORTED_MODULE_8__);
/* harmony import */ var _fortawesome_react_fontawesome__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @fortawesome/react-fontawesome */ "./node_modules/@fortawesome/react-fontawesome/index.es.js");
/* harmony import */ var _fortawesome_free_solid_svg_icons__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @fortawesome/free-solid-svg-icons */ "./node_modules/@fortawesome/free-solid-svg-icons/index.es.js");
/* harmony import */ var lodash_isEmpty__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! lodash/isEmpty */ "./node_modules/lodash/isEmpty.js");
/* harmony import */ var lodash_isEmpty__WEBPACK_IMPORTED_MODULE_11___default = /*#__PURE__*/__webpack_require__.n(lodash_isEmpty__WEBPACK_IMPORTED_MODULE_11__);
/* harmony import */ var _components_base_alerts_sweetAlert__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ../components/_base/alerts/sweetAlert */ "./resources/js/components/_base/alerts/sweetAlert.js");
/* harmony import */ var _helpers_propertyHelper_propertyHelper__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ../helpers/propertyHelper/propertyHelper */ "./resources/js/helpers/propertyHelper/propertyHelper.js");
/* harmony import */ var _helpers_fileHelper_fileHelper__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ../helpers/fileHelper/fileHelper */ "./resources/js/helpers/fileHelper/fileHelper.js");
function _toConsumableArray(arr) { return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _unsupportedIterableToArray(arr) || _nonIterableSpread(); }

function _nonIterableSpread() { throw new TypeError("Invalid attempt to spread non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _iterableToArray(iter) { if (typeof Symbol !== "undefined" && Symbol.iterator in Object(iter)) return Array.from(iter); }

function _arrayWithoutHoles(arr) { if (Array.isArray(arr)) return _arrayLikeToArray(arr); }

function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _unsupportedIterableToArray(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

function _iterableToArrayLimit(arr, i) { if (typeof Symbol === "undefined" || !(Symbol.iterator in Object(arr))) return; var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }

















/* harmony default export */ __webpack_exports__["default"] = (function (_ref) {
  var projectName = _ref.projectName,
      projectId = _ref.projectId,
      toggleAddPropertyModal = _ref.toggleAddPropertyModal,
      addToggleFetch = _ref.addToggleFetch;

  var _UserGlobal = Object(_states_userGlobal__WEBPACK_IMPORTED_MODULE_7__["default"])(),
      _UserGlobal2 = _slicedToArray(_UserGlobal, 2),
      userState = _UserGlobal2[0],
      userActions = _UserGlobal2[1];

  var _useState = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])([]),
      _useState2 = _slicedToArray(_useState, 2),
      featuredImages = _useState2[0],
      setFeaturedImages = _useState2[1];

  var _useState3 = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])("4"),
      _useState4 = _slicedToArray(_useState3, 2),
      propertyType = _useState4[0],
      setPropertyType = _useState4[1];

  var _useState5 = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])([]),
      _useState6 = _slicedToArray(_useState5, 2),
      errors = _useState6[0],
      setErrors = _useState6[1];

  var _useState7 = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])(function () {
    return draft_js__WEBPACK_IMPORTED_MODULE_4__["EditorState"].createEmpty();
  }),
      _useState8 = _slicedToArray(_useState7, 2),
      editorState = _useState8[0],
      setEditorState = _useState8[1];

  var _useState9 = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])([]),
      _useState10 = _slicedToArray(_useState9, 2),
      floorPlan = _useState10[0],
      setFLoorPlan = _useState10[1];

  var _useState11 = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])(""),
      _useState12 = _slicedToArray(_useState11, 2),
      filename = _useState12[0],
      setFilename = _useState12[1];

  var _useState13 = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])(""),
      _useState14 = _slicedToArray(_useState13, 2),
      prevFilename = _useState14[0],
      setPrevFilename = _useState14[1];

  var _useState15 = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])([]),
      _useState16 = _slicedToArray(_useState15, 2),
      previewImage = _useState16[0],
      setPreviewImage = _useState16[1];

  var _useState17 = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])([]),
      _useState18 = _slicedToArray(_useState17, 2),
      fmPlaceHolder = _useState18[0],
      setFmPlaceHolder = _useState18[1];

  var _useState19 = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])(false),
      _useState20 = _slicedToArray(_useState19, 2),
      limitError = _useState20[0],
      setLimitError = _useState20[1];

  var addPropertyForm = Object(react__WEBPACK_IMPORTED_MODULE_0__["useRef"])(null);

  var _useState21 = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])(false),
      _useState22 = _slicedToArray(_useState21, 2),
      loading = _useState22[0],
      setLoading = _useState22[1];

  var previewImageRef = Object(react__WEBPACK_IMPORTED_MODULE_0__["useRef"])(null);
  var floorPlanRef = Object(react__WEBPACK_IMPORTED_MODULE_0__["useRef"])(null);
  Object(react__WEBPACK_IMPORTED_MODULE_0__["useEffect"])(function () {
    var phs = [];

    for (var index = 0; index < 6 - featuredImages.length; index++) {
      var ph = {
        classes: "cursor-pointer m-2 inline-flex bg-transparent text-palette-gray"
      };
      phs.push(ph);
    }

    setFmPlaceHolder(phs);
  }, [featuredImages.length]);

  var handleDeletePrevFile = function handleDeletePrevFile(e) {
    e.preventDefault();
    setPreviewImage([]);
    previewImageRef.current.type = "";
    previewImageRef.current.type = "file";
    setPrevFilename("");
    e.target.value = null;
  };

  var handlePreviewImageDisplay = function handlePreviewImageDisplay(e) {
    e.preventDefault();

    if (e.target.files.length > 1) {
      Object(_components_base_alerts_sweetAlert__WEBPACK_IMPORTED_MODULE_12__["sweetAlert"])("error", "Multiple upload is not allowed!");
      return;
    }

    if (!Object(_helpers_fileHelper_fileHelper__WEBPACK_IMPORTED_MODULE_14__["imageFileTypeFilter"])(e.target.files[0]["name"])) {
      Object(_components_base_alerts_sweetAlert__WEBPACK_IMPORTED_MODULE_12__["sweetAlert"])("error", "Filetype is not supported!");
      e.target.value = null;
      return;
    }

    setPreviewImage(previewImage.concat(_toConsumableArray(_toConsumableArray(e.target.files).map(function (file) {
      var newFilename = Object(_helpers_fileHelper_fileHelper__WEBPACK_IMPORTED_MODULE_14__["sanitizeFilename"])(file.name);
      var newFileObj = new File([file], newFilename);
      setPrevFilename(newFilename);
      return {
        image: newFileObj,
        preview: URL.createObjectURL(newFileObj)
      };
    }))));
  };

  var saveProperty = function saveProperty() {
    var _userState$projectDat;

    setLoading(true);
    var formData = new FormData(addPropertyForm.current);
    formData.append("role", "project_developer");

    if (floorPlan && !lodash_isEmpty__WEBPACK_IMPORTED_MODULE_11___default()(floorPlan)) {
      formData.append("floorPlan[file]", floorPlan[0]);
    }

    if (previewImage.length) {
      formData.append("preview_img[".concat(0, "]"), previewImage[0].image);
    } else {
      formData.append("preview_img[".concat(0, "]"), "");
    } // formData.append("projectName", projectName);


    formData.append("projectId", (_userState$projectDat = userState.projectData.id) !== null && _userState$projectDat !== void 0 ? _userState$projectDat : projectId); // formData.append("ownershipType", ownershipType);
    // formData.append("subPropertyType", subPropertyType);

    formData.append("propertyType", propertyType); // formData.append("water", water);
    // formData.append("council", council);
    // formData.append("strata", strata);
    // formData.append("state", state);

    formData.append("description", editorState.getCurrentContent().getPlainText());
    formData.append("descriptionHTML", Object(draft_convert__WEBPACK_IMPORTED_MODULE_5__["convertToHTML"])(editorState.getCurrentContent()));
    featuredImages.map(function (image, key) {
      formData.append("featuredImages[".concat(key, "]"), image.image);
    });
    var url = "/api/property/";
    axios({
      method: "post",
      url: url,
      data: formData,
      name: "project",
      headers: {
        "content-type": "multipart/form-data"
      }
    }).then(function (retVal) {
      if (retVal.status === 200) {
        Object(_components_base_alerts_sweetAlert__WEBPACK_IMPORTED_MODULE_12__["sweetAlert"])("success", "Property successfully added.");
        setLoading(false);
        var project = userState.projectData;
        project.properties = retVal.data;
        userActions.setState({
          projectData: project
        });
        addToggleFetch();
        toggleAddPropertyModal();
      }
    })["catch"](function (error) {
      if (error.response.data.errors) {
        Object(_components_base_alerts_sweetAlert__WEBPACK_IMPORTED_MODULE_12__["sweetAlert"])("error", "Some field are missing.");
      }

      setLoading(false);
      setErrors(error.response.data.errors);
    }).then(function () {});
  };

  var handleSubmit = function handleSubmit(e) {
    e.preventDefault();
    setLimitError(false);
    saveProperty(); // window.location.href = window.location.href;
  };

  var handleDrop = function handleDrop(e) {
    e.preventDefault();
    e.stopPropagation();
    var fmCount = featuredImages.length;
    var onCount = e.dataTransfer.files.length;

    if (fmCount + onCount > 6) {
      e.target.value = null;
      setLimitError(true);
      return;
    } else {
      setLimitError(false);
    }

    setFeaturedImages(featuredImages.concat(_toConsumableArray(_toConsumableArray(e.dataTransfer.files).map(function (file) {
      var newFilename = Object(_helpers_fileHelper_fileHelper__WEBPACK_IMPORTED_MODULE_14__["sanitizeFilename"])(file.name);
      var newFileObj = new File([file], newFilename);
      return {
        image: newFileObj,
        preview: URL.createObjectURL(newFileObj)
      };
    }))));
    e.target.value = null;
  };

  var handleChange = function handleChange(e) {
    var fmCount = featuredImages.length;
    var onCount = e.target.files.length;

    if (fmCount + onCount > 6) {
      setLimitError(true);
      e.target.value = null;
      return;
    } else {
      setLimitError(false);
    }

    setFeaturedImages(featuredImages.concat(_toConsumableArray(_toConsumableArray(e.target.files).map(function (file) {
      return {
        image: file,
        preview: URL.createObjectURL(file)
      };
    }))));
    e.target.value = null;
  };

  var handleRemove = function handleRemove(e, i) {
    e.preventDefault();
    e.stopPropagation();
    setFeaturedImages(_toConsumableArray(featuredImages.filter(function (image, k) {
      if (i !== k) {
        return image;
      }
    })));

    if (featuredImages.length > 6) {
      setLimitError(true);
    } else {
      setLimitError(false);
    }
  };

  var handleFloorPlanChange = function handleFloorPlanChange(e) {
    e.preventDefault();

    if (!Object(_helpers_fileHelper_fileHelper__WEBPACK_IMPORTED_MODULE_14__["PDFFileTypeFilter"])(e.target.files[0]["name"])) {
      Object(_components_base_alerts_sweetAlert__WEBPACK_IMPORTED_MODULE_12__["sweetAlert"])("error", "Filetype is not supported!");
      e.target.value = null;
      return;
    }

    setFLoorPlan(Object(_helpers_propertyHelper_propertyHelper__WEBPACK_IMPORTED_MODULE_13__["floorPlanHelper"])(e.target.files));
    var newFilename = Object(_helpers_fileHelper_fileHelper__WEBPACK_IMPORTED_MODULE_14__["sanitizeFilename"])(e.target.files[0]["name"]);
    setFilename(newFilename); // e.target.value = null;
  };

  var handleDeleteFile = function handleDeleteFile(e) {
    e.preventDefault();
    setFLoorPlan({});
    floorPlanRef.current.type = "";
    floorPlanRef.current.type = "file";
    setFilename("");
    e.target.value = null;
  };

  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_base__WEBPACK_IMPORTED_MODULE_3__["Modal"], {
    show: true,
    title: "Add Land Property",
    disableBackdropClick: true,
    maxWidth: "md",
    topOff: true,
    onClose: function onClose() {
      return toggleAddPropertyModal();
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "bg-white rounded-lg py-5 px-8 m-5"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("form", {
    ref: addPropertyForm,
    onSubmit: handleSubmit
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "text-black"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "py-3 flex justify-start "
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "flex justify-center items-center"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("label", {
    className: "font-semibold w-24"
  }, "Property Type"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_dropdown__WEBPACK_IMPORTED_MODULE_1___default.a, {
    id: "property_type",
    value: propertyType,
    options: _helpers_propertyHelper_propertyHelper__WEBPACK_IMPORTED_MODULE_13__["property_types"],
    onChange: function onChange(option) {
      setPropertyType(option.value);
    },
    placeholder: "Please select..." // name="property_type"
    ,
    className: "w-48 ml-4",
    disabled: true
  }), errors["propertyType"] && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
    className: "px-1 text-red-400 tracking-widest  text-xs "
  }, errors["propertyType"][0]))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "ml-10 flex-1 flex justify-end items-center"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("label", {
    className: "font-semibold w-32"
  }, "Lot Number"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_base__WEBPACK_IMPORTED_MODULE_3__["TextInput"], {
    className: "border-gray-400 border pl-2 py-1",
    border: false,
    name: "unit_name",
    width: "w-24 ml-4"
  }), errors["unit_name"] && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
    className: "px-1 text-red-400 tracking-widest  text-xs "
  }, errors["unit_name"][0]))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "ml-4 flex-1 flex justify-end items-center"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("label", {
    className: "font-semibold w-12"
  }, "Size"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_base__WEBPACK_IMPORTED_MODULE_3__["TextInput"], {
    className: "border-gray-400 border pl-2 py-1",
    border: false,
    name: "size",
    width: "w-24"
  }), errors["size"] && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
    className: "px-1 text-red-400 tracking-widest  text-xs "
  }, errors["size"][0])))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "flex justify-start my-3"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "flex items-center justify-start py-3"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("label", {
    className: "w-12"
  }, "Price"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "relative"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_currency_format__WEBPACK_IMPORTED_MODULE_6___default.a, {
    allowNegative: false,
    className: "border text-sm border-gray-400 px-2 py-1 w-48",
    thousandSeparator: true,
    name: "price",
    id: "price",
    prefix: "$"
  })), errors["price"] && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
    className: "absolute px-1 text-red-400 tracking-widest text-xs "
  }, errors["price"][0]))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "py-3 flex justify-start items-center ml-10"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("label", {
    className: ""
  }, "Frontage"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "relative"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "ml-4 w-24"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_base__WEBPACK_IMPORTED_MODULE_3__["TextInput"], {
    className: "pl-2 text-sm border-gray-400 w-32 border py-1",
    border: false // type={`number`}
    ,
    name: "frontage"
  })), errors["frontage"] && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
    className: "absolute px-1 text-red-400 tracking-widest  text-xs "
  }, errors["frontage"][0]))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "py-3 flex justify-start items-center ml-8"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("label", {
    className: ""
  }, "Width"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "relative"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "ml-4 w-24"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_base__WEBPACK_IMPORTED_MODULE_3__["TextInput"], {
    className: "pl-2 text-sm border-gray-400 w-32 border py-1",
    border: false // type={`number`}
    ,
    name: "width"
  })), errors["width"] && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
    className: "absolute px-1 text-red-400 tracking-widest  text-xs "
  }, errors["width"][0]))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "py-3 flex justify-start items-center ml-8"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("label", {
    className: ""
  }, "Depth "), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "relative"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "ml-4 w-24"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_base__WEBPACK_IMPORTED_MODULE_3__["TextInput"], {
    className: "pl-2 text-sm border-gray-400 w-32 border py-1 mr-1",
    border: false // type={`number`}
    ,
    name: "depth"
  })), errors["depth"] && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
    className: "absolute px-1 text-red-400 tracking-widest  text-xs "
  }, errors["depth"][0])))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "py-3 "
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("label", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "w-full text-sm bg-white text-black "
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_base__WEBPACK_IMPORTED_MODULE_3__["RichTextEditor"], {
    title: "Property Description",
    richEditorState: editorState,
    richEditorStateContent: function richEditorStateContent(content) {
      return setEditorState(content);
    },
    characterLimit: 2000
  }))), errors["description"] && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
    className: "px-1 text-red-400 tracking-widest  text-xs "
  }, errors["description"][0])), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "py-3"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("h1", {
    className: "font-semibold"
  }, " ", "Featured Images (min. 3 images)", " "), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("label", {
    className: "button ",
    htmlFor: "upload-photos"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: ""
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "w-full flex flex-wrap  cursor-pointer",
    style: {
      border: "2px dashed #ccc",
      borderRadius: "20px",
      padding: "20px",
      textAlign: "center",
      marginBottom: "0px"
    },
    onDrop: function onDrop(e) {
      return handleDrop(e);
    },
    onDragOver: function onDragOver(e) {
      e.preventDefault();
      e.stopPropagation();
    },
    onDragEnter: function onDragEnter(e) {
      e.preventDefault();
      e.stopPropagation();
    },
    onDragLeave: function onDragLeave(e) {
      e.preventDefault();
      e.stopPropagation();
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("input", {
    type: "file",
    id: "upload-photos",
    accept: "image/*",
    multiple: true,
    className: "hidden",
    name: "image[]",
    onChange: function onChange(e) {
      return handleChange(e);
    }
  }), featuredImages.map(function (image, index) {
    return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      key: index,
      className: "rounded-sm border m-2 relative",
      style: {
        backgroundImage: "url('".concat(image.preview, "')"),
        backgroundPositionX: "center",
        width: "6.9em",
        height: "8.9rem",
        backgroundSize: "cover"
      }
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_base__WEBPACK_IMPORTED_MODULE_3__["Tooltip"], {
      title: "Remove this item"
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
      onClick: function onClick(e) {
        return handleRemove(e, index);
      },
      className: "absolute bottom-0 right-0 p-2 -mb-2 "
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_fortawesome_react_fontawesome__WEBPACK_IMPORTED_MODULE_9__["FontAwesomeIcon"], {
      className: "text-red-500 shadow-xl hover:font-bold hover:text-red-700",
      icon: _fortawesome_free_solid_svg_icons__WEBPACK_IMPORTED_MODULE_10__["faTimes"]
    }))));
  }), fmPlaceHolder.map(function (p, i) {
    return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      key: i,
      className: "".concat(p.classes, " cursor-pointer flex justify-center items-center"),
      style: {
        width: "6.9rem",
        height: "8.9rem",
        border: "2px solid #ccc"
      }
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_fortawesome_react_fontawesome__WEBPACK_IMPORTED_MODULE_9__["FontAwesomeIcon"], {
      className: "text-xl",
      icon: _fortawesome_free_solid_svg_icons__WEBPACK_IMPORTED_MODULE_10__["faCamera"]
    }));
  })), limitError && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
    className: "px-1 text-red-400 tracking-widest  text-xs "
  }, "Please limit your photos to six items only."))), errors["featuredImages"] && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
    className: "px-1 text-red-400 tracking-widest  text-xs "
  }, errors["featuredImages"][0])), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "flex flex-row"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "w-1/2 ml-2"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("h1", {
    className: "font-semibold"
  }, "Rich Text Preview "), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "flex flex-col justify-between py-3"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "flex"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("label", {
    className: "w-full"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "items-center cursor-pointer w-full p-3 rounded-lg flex",
    style: {
      border: "2px dashed #ccc",
      borderRadius: "20px"
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("input", {
    id: "rtp",
    ref: previewImageRef,
    type: "file" // name="prev_image"
    ,
    onChange: function onChange(e) {
      return handlePreviewImageDisplay(e);
    },
    className: "hidden border text-sm border-gray-400 px-2 py-1 w-full py-1"
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("label", {
    htmlFor: "rtp",
    className: " cursor-pointer hover:text-palette-blue-light ".concat(prevFilename ? "text-palette-blue-light" : "")
  }, prevFilename || "Select File"), prevFilename && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "text-red-500 text-lg cursor-pointer ml-3"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_fortawesome_react_fontawesome__WEBPACK_IMPORTED_MODULE_9__["FontAwesomeIcon"], {
    onClick: function onClick(e) {
      return handleDeletePrevFile(e);
    },
    icon: _fortawesome_free_solid_svg_icons__WEBPACK_IMPORTED_MODULE_10__["faTimes"]
  })))))))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "flex justify-end my-5"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_base__WEBPACK_IMPORTED_MODULE_3__["Button"], {
    className: "font-bold rounded-full",
    disabled: loading
  }, loading && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_fortawesome_react_fontawesome__WEBPACK_IMPORTED_MODULE_9__["FontAwesomeIcon"], {
    icon: _fortawesome_free_solid_svg_icons__WEBPACK_IMPORTED_MODULE_10__["faSpinner"],
    className: "fa-spin mr-2"
  }), "Save"))))));
});

/***/ }),

/***/ "./resources/js/components/AddProject.js":
/*!***********************************************!*\
  !*** ./resources/js/components/AddProject.js ***!
  \***********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime/regenerator */ "./node_modules/@babel/runtime/regenerator/index.js");
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var react_dropdown__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! react-dropdown */ "./node_modules/react-dropdown/dist/index.js");
/* harmony import */ var react_dropdown__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(react_dropdown__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _helpers_styles_dropdown_css__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../helpers/styles/dropdown.css */ "./resources/js/helpers/styles/dropdown.css");
/* harmony import */ var _helpers_styles_dropdown_css__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(_helpers_styles_dropdown_css__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! moment */ "./node_modules/moment/moment.js");
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(moment__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var _base__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./_base */ "./resources/js/components/_base/index.js");
/* harmony import */ var _base__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(_base__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var draft_js__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! draft-js */ "./node_modules/draft-js/lib/Draft.js");
/* harmony import */ var draft_js__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(draft_js__WEBPACK_IMPORTED_MODULE_6__);
/* harmony import */ var react_datepicker__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! react-datepicker */ "./node_modules/react-datepicker/dist/react-datepicker.min.js");
/* harmony import */ var react_datepicker__WEBPACK_IMPORTED_MODULE_7___default = /*#__PURE__*/__webpack_require__.n(react_datepicker__WEBPACK_IMPORTED_MODULE_7__);
/* harmony import */ var _helpers_styles_datepicker_css__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../helpers/styles/datepicker.css */ "./resources/js/helpers/styles/datepicker.css");
/* harmony import */ var _helpers_styles_datepicker_css__WEBPACK_IMPORTED_MODULE_8___default = /*#__PURE__*/__webpack_require__.n(_helpers_styles_datepicker_css__WEBPACK_IMPORTED_MODULE_8__);
/* harmony import */ var draft_convert__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! draft-convert */ "./node_modules/draft-convert/esm/index.js");
/* harmony import */ var _helpers_propertyHelper_propertyHelper__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ../helpers/propertyHelper/propertyHelper */ "./resources/js/helpers/propertyHelper/propertyHelper.js");
/* harmony import */ var _fortawesome_react_fontawesome__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! @fortawesome/react-fontawesome */ "./node_modules/@fortawesome/react-fontawesome/index.es.js");
/* harmony import */ var _fortawesome_free_regular_svg_icons__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! @fortawesome/free-regular-svg-icons */ "./node_modules/@fortawesome/free-regular-svg-icons/index.es.js");
/* harmony import */ var _fortawesome_free_solid_svg_icons__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! @fortawesome/free-solid-svg-icons */ "./node_modules/@fortawesome/free-solid-svg-icons/index.es.js");
/* harmony import */ var _states_userGlobal__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ../states/userGlobal */ "./resources/js/states/userGlobal.js");
/* harmony import */ var lodash_isEmpty__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! lodash/isEmpty */ "./node_modules/lodash/isEmpty.js");
/* harmony import */ var lodash_isEmpty__WEBPACK_IMPORTED_MODULE_15___default = /*#__PURE__*/__webpack_require__.n(lodash_isEmpty__WEBPACK_IMPORTED_MODULE_15__);
/* harmony import */ var _components_base_alerts_sweetAlert__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! ../components/_base/alerts/sweetAlert */ "./resources/js/components/_base/alerts/sweetAlert.js");
/* harmony import */ var _data_index__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! ../data/index */ "./resources/js/data/index.js");
/* harmony import */ var _helpers_mapCoordinateHelper__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(/*! ../helpers/mapCoordinateHelper */ "./resources/js/helpers/mapCoordinateHelper.js");
/* harmony import */ var _helpers_fileHelper_fileHelper__WEBPACK_IMPORTED_MODULE_19__ = __webpack_require__(/*! ../helpers/fileHelper/fileHelper */ "./resources/js/helpers/fileHelper/fileHelper.js");


function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

function _toConsumableArray(arr) { return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _unsupportedIterableToArray(arr) || _nonIterableSpread(); }

function _nonIterableSpread() { throw new TypeError("Invalid attempt to spread non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _iterableToArray(iter) { if (typeof Symbol !== "undefined" && Symbol.iterator in Object(iter)) return Array.from(iter); }

function _arrayWithoutHoles(arr) { if (Array.isArray(arr)) return _arrayLikeToArray(arr); }

function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _unsupportedIterableToArray(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

function _iterableToArrayLimit(arr, i) { if (typeof Symbol === "undefined" || !(Symbol.iterator in Object(arr))) return; var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }






















/* harmony default export */ __webpack_exports__["default"] = (function (_ref) {
  var toggleFetch = _ref.toggleFetch;

  var _UserGlobal = Object(_states_userGlobal__WEBPACK_IMPORTED_MODULE_14__["default"])(),
      _UserGlobal2 = _slicedToArray(_UserGlobal, 2),
      userState = _UserGlobal2[0],
      userActions = _UserGlobal2[1];

  var _useState = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(userState.user),
      _useState2 = _slicedToArray(_useState, 2),
      currUser = _useState2[0],
      setCurrUser = _useState2[1];

  var _useState3 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(new Date(Date.now())),
      _useState4 = _slicedToArray(_useState3, 2),
      date = _useState4[0],
      setDate = _useState4[1];

  var _useState5 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])([]),
      _useState6 = _slicedToArray(_useState5, 2),
      featuredImages = _useState6[0],
      setFeaturedImages = _useState6[1];

  var _useState7 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(""),
      _useState8 = _slicedToArray(_useState7, 2),
      projectState = _useState8[0],
      setProjectState = _useState8[1];

  var _useState9 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(""),
      _useState10 = _slicedToArray(_useState9, 2),
      displaySuiteState = _useState10[0],
      setDisplaySuiteState = _useState10[1];

  var _useState11 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(""),
      _useState12 = _slicedToArray(_useState11, 2),
      propertyType = _useState12[0],
      setPropertyType = _useState12[1];

  var _useState13 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])([]),
      _useState14 = _slicedToArray(_useState13, 2),
      errors = _useState14[0],
      setErrors = _useState14[1];

  var _useState15 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(function () {
    return draft_js__WEBPACK_IMPORTED_MODULE_6__["EditorState"].createEmpty();
  }),
      _useState16 = _slicedToArray(_useState15, 2),
      editorState = _useState16[0],
      setEditorState = _useState16[1];

  var _useState17 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])("Australia"),
      _useState18 = _slicedToArray(_useState17, 2),
      projectCountry = _useState18[0],
      setProjectCountry = _useState18[1];

  var _useState19 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])("Australia"),
      _useState20 = _slicedToArray(_useState19, 2),
      displaySuiteCountry = _useState20[0],
      setDisplaySuiteCountry = _useState20[1];

  var _useState21 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])([]),
      _useState22 = _slicedToArray(_useState21, 2),
      fmPlaceHolder = _useState22[0],
      setFmPlaceHolder = _useState22[1];

  var _useState23 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(false),
      _useState24 = _slicedToArray(_useState23, 2),
      limitError = _useState24[0],
      setLimitError = _useState24[1];

  var addProjectForm = Object(react__WEBPACK_IMPORTED_MODULE_1__["useRef"])(null);

  var _useState25 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(""),
      _useState26 = _slicedToArray(_useState25, 2),
      developerValue = _useState26[0],
      setDeveloperValue = _useState26[1];

  var countries = [{
    label: "Australia",
    value: "Australia"
  }];

  var _useState27 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(userState.projectData),
      _useState28 = _slicedToArray(_useState27, 2),
      project = _useState28[0],
      setProject = _useState28[1];

  var _useState29 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(false),
      _useState30 = _slicedToArray(_useState29, 2),
      sameAddress = _useState30[0],
      setSame = _useState30[1];

  var _useState31 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(false),
      _useState32 = _slicedToArray(_useState31, 2),
      loading = _useState32[0],
      setLoading = _useState32[1];

  var _useState33 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])([]),
      _useState34 = _slicedToArray(_useState33, 2),
      projectDevelopers = _useState34[0],
      setProjectDevelopers = _useState34[1];

  var _useState35 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(userState.projectData.deposit ? userState.projectData.deposit.toString() : null),
      _useState36 = _slicedToArray(_useState35, 2),
      deposit = _useState36[0],
      setDeposit = _useState36[1];

  var _useState37 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(false),
      _useState38 = _slicedToArray(_useState37, 2),
      completedNow = _useState38[0],
      setCompletedNow = _useState38[1];

  var _useState39 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(false),
      _useState40 = _slicedToArray(_useState39, 2),
      isProjectLand = _useState40[0],
      setIsProjectLand = _useState40[1];

  Object(react__WEBPACK_IMPORTED_MODULE_1__["useEffect"])(function () {
    var url = "/api/user";
    Object(_data_index__WEBPACK_IMPORTED_MODULE_17__["developers"])(url).then(function (result) {
      setProjectDevelopers(_toConsumableArray(result.data.map(function (developer) {
        return {
          label: "".concat(developer.first_name, " ").concat(developer.last_name),
          value: String(developer.id)
        };
      })));
    })["catch"](function (error) {
      setLoading(false);
      setErrors(error.response.data.errors);
    });
  }, []);
  Object(react__WEBPACK_IMPORTED_MODULE_1__["useEffect"])(function () {
    var phs = [];

    for (var index = 0; index < 12 - featuredImages.length; index++) {
      var ph = {
        classes: "cursor-pointer m-2 inline-flex bg-transparent text-palette-gray"
      };
      phs.push(ph);
    }

    setFmPlaceHolder(phs);
  }, [featuredImages.length]);
  Object(react__WEBPACK_IMPORTED_MODULE_1__["useEffect"])(function () {
    setEditorState(function () {
      return draft_js__WEBPACK_IMPORTED_MODULE_6__["EditorState"].push(editorState, Object(draft_convert__WEBPACK_IMPORTED_MODULE_9__["convertFromHTML"])(userState.projectData.description ? userState.projectData.description : ""));
    });
  }, []);

  var handleSubmit = function handleSubmit(e) {
    e.preventDefault();
    setLimitError(false);
    saveProject();
  };

  var saveProject = /*#__PURE__*/function () {
    var _ref2 = _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee() {
      var formData, url;
      return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee$(_context) {
        while (1) {
          switch (_context.prev = _context.next) {
            case 0:
              setLoading(true);
              formData = new FormData(addProjectForm.current);
              formData.append("propertyType", propertyType);
              formData.append("project_address_state", projectState);
              formData.append("project_address_country", projectCountry);
              formData.append("display_suite_address_state", displaySuiteState);
              formData.append("display_suite_address_country", displaySuiteCountry);
              formData.append("isCompleted", completedNow);
              formData.append("deposit", deposit);
              formData.append("description", editorState.getCurrentContent().getPlainText());
              formData.append("descriptionHTML", Object(draft_convert__WEBPACK_IMPORTED_MODULE_9__["convertToHTML"])(editorState.getCurrentContent()));
              featuredImages.map(function (image, key) {
                formData.append("featuredImages[".concat(key, "]"), image.image);
              });
              formData.append("developer", developerValue);
              formData.append("time_limit[optionVal]", 0);
              formData.append("time_limit[option]", 0);
              formData.append("time_limit[createdAt]", 0);
              formData.append("same_address", sameAddress);
              formData.append("isPropertyTypeLand", isProjectLand);
              url = "/api/deal/";
              _context.next = 21;
              return saveDeal({
                formData: formData,
                url: url
              });

            case 21:
            case "end":
              return _context.stop();
          }
        }
      }, _callee);
    }));

    return function saveProject() {
      return _ref2.apply(this, arguments);
    };
  }();

  var getDealCoordinates = /*#__PURE__*/function () {
    var _ref4 = _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee2(_ref3) {
      var formData, address, suburb, addressParam, data, _address, _suburb, _addressParam, _data;

      return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee2$(_context2) {
        while (1) {
          switch (_context2.prev = _context2.next) {
            case 0:
              formData = _ref3.formData;
              address = formData.get("project_address_line_1");
              suburb = formData.get("project_address_suburb");
              addressParam = address.concat(" ", suburb).concat(" ", projectState).concat(" ", projectCountry);
              _context2.next = 6;
              return Object(_helpers_mapCoordinateHelper__WEBPACK_IMPORTED_MODULE_18__["getMapCoordinates"])(addressParam);

            case 6:
              data = _context2.sent;
              formData.append("long", data["long"]);
              formData.append("lat", data.lat);

              if (sameAddress) {
                _context2.next = 18;
                break;
              }

              _address = formData.get("display_suite_address_line_1");
              _suburb = formData.get("display_suite_address_suburb");
              _addressParam = _address.concat(" ", _suburb).concat(" ", displaySuiteState).concat(" ", displaySuiteCountry);
              _context2.next = 15;
              return Object(_helpers_mapCoordinateHelper__WEBPACK_IMPORTED_MODULE_18__["getMapCoordinates"])(_addressParam);

            case 15:
              _data = _context2.sent;
              formData.append("display_suite_long", _data["long"]);
              formData.append("display_suite_lat", _data.lat);

            case 18:
            case "end":
              return _context2.stop();
          }
        }
      }, _callee2);
    }));

    return function getDealCoordinates(_x) {
      return _ref4.apply(this, arguments);
    };
  }();

  var saveDeal = /*#__PURE__*/function () {
    var _ref6 = _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee3(_ref5) {
      var formData, url;
      return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee3$(_context3) {
        while (1) {
          switch (_context3.prev = _context3.next) {
            case 0:
              formData = _ref5.formData, url = _ref5.url;
              _context3.next = 3;
              return getDealCoordinates({
                formData: formData
              });

            case 3:
              _context3.next = 5;
              return Object(_data_index__WEBPACK_IMPORTED_MODULE_17__["saveUpdateDeal"])({
                formData: formData,
                url: url
              }).then(function (r) {
                if (r.status === 200) {
                  Object(_components_base_alerts_sweetAlert__WEBPACK_IMPORTED_MODULE_16__["sweetAlert"])("success", "Project successfully saved.");
                  setLoading(false);
                  toggleFetch();
                  userActions.setState({
                    projectData: r.data,
                    currentProjStep: 2
                  });
                }
              })["catch"](function (error) {
                if (error.response.data.errors) {
                  Object(_components_base_alerts_sweetAlert__WEBPACK_IMPORTED_MODULE_16__["sweetAlert"])("error", "Some fields are missing!");
                }

                setLoading(false);
                setErrors(error.response.data.errors);
              }).then(function () {});

            case 5:
            case "end":
              return _context3.stop();
          }
        }
      }, _callee3);
    }));

    return function saveDeal(_x2) {
      return _ref6.apply(this, arguments);
    };
  }();

  var handleDrop = function handleDrop(e) {
    e.preventDefault();
    e.stopPropagation();
    var fmCount = featuredImages.length;
    var onCount = e.dataTransfer.files.length;

    if (fmCount + onCount > 12) {
      e.target.value = null;
      setLimitError(true);
      return;
    } else {
      setLimitError(false);
    }

    setFeaturedImages(featuredImages.concat(_toConsumableArray(_toConsumableArray(e.dataTransfer.files).map(function (file) {
      var newFilename = Object(_helpers_fileHelper_fileHelper__WEBPACK_IMPORTED_MODULE_19__["sanitizeFilename"])(file.name);
      var newFileObj = new File([file], newFilename);
      return {
        image: newFileObj,
        preview: URL.createObjectURL(newFileObj)
      };
    }))));
    e.target.value = null;
  };

  var handleChange = function handleChange(e) {
    var fmCount = featuredImages.length;
    var onCount = e.target.files.length;

    if (fmCount + onCount > 12) {
      setLimitError(true);
      e.target.value = null;
      return;
    } else {
      setLimitError(false);
    }

    setFeaturedImages(featuredImages.concat(_toConsumableArray(_toConsumableArray(e.target.files).map(function (file) {
      var newFilename = Object(_helpers_fileHelper_fileHelper__WEBPACK_IMPORTED_MODULE_19__["sanitizeFilename"])(file.name);
      var newFileObj = new File([file], newFilename);
      return {
        image: newFileObj,
        preview: URL.createObjectURL(newFileObj)
      };
    }))));
    e.target.value = null;
  };

  var handleRemove = function handleRemove(e, i) {
    e.preventDefault();
    e.stopPropagation();
    setFeaturedImages(_toConsumableArray(featuredImages.filter(function (image, k) {
      if (i !== k) {
        return image;
      }
    })));

    if (featuredImages.length > 12) {
      setLimitError(true);
    } else {
      setLimitError(false);
    }
  };

  var compareDate = function compareDate(selectedDate) {
    var date = moment__WEBPACK_IMPORTED_MODULE_4___default()(selectedDate, "DD-MM-YYYY"); //Date format

    var currDate = moment__WEBPACK_IMPORTED_MODULE_4___default()();
    var duration = date.diff(currDate, "hours");

    if (duration < 0) {
      return false;
    }

    return true;
  };

  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "bg-white rounded-lg py-5 px-8 m-5"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("form", {
    ref: addProjectForm,
    onSubmit: handleSubmit
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "text-black"
  }, currUser && currUser.user_role === "admin" && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "flex items-center"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("label", {
    className: "block py-3 font-semibold w-40"
  }, "Project Developer"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "relative"
  }, !lodash_isEmpty__WEBPACK_IMPORTED_MODULE_15___default()(projectDevelopers) ? /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(react_dropdown__WEBPACK_IMPORTED_MODULE_2___default.a, {
    id: "project_developer",
    value: developerValue,
    options: projectDevelopers,
    onChange: function onChange(option) {
      setDeveloperValue(option.value);
    },
    placeholder: "Please select...",
    name: "project_developer"
  }) : /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
    className: "block p-3"
  }, "Loading developer's list...")), errors["developer"] && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
    className: "absolute px-1 text-red-400 tracking-widest  text-xs "
  }, errors["developer"][0])), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "py-3 flex items-center "
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("label", {
    className: "font-semibold w-1/5",
    htmlFor: "name"
  }, "Project Name"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "flex-1 relative"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_base__WEBPACK_IMPORTED_MODULE_5__["TextInput"], {
    id: "name",
    className: "border-gray-400 border text-sm pl-2 py-1 ",
    border: false,
    name: "name"
  }), errors["name"] && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
    className: " absolute px-1 text-red-400 tracking-widest  text-xs "
  }, errors["name"][0]))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "py-3"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("label", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "w-full text-sm bg-white text-black "
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_base__WEBPACK_IMPORTED_MODULE_5__["RichTextEditor"], {
    title: "Project Description",
    richEditorState: editorState,
    richEditorStateContent: function richEditorStateContent(content) {
      return setEditorState(content);
    },
    characterLimit: 2000
  }))), errors["description"] && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
    className: "px-1 text-red-400 tracking-widest  text-xs "
  }, errors["description"][0])), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "flex justify-between"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "flex flex-start items-center"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("label", {
    className: "block py-3 w-32"
  }, "Project Type"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "relative "
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(react_dropdown__WEBPACK_IMPORTED_MODULE_2___default.a, {
    id: "property_type",
    value: propertyType,
    options: _helpers_propertyHelper_propertyHelper__WEBPACK_IMPORTED_MODULE_10__["property_types"],
    onChange: function onChange(option) {
      if (option.label === "Land") {
        setIsProjectLand(true);
      } else {
        setIsProjectLand(false);
      }

      setPropertyType(option.value);
    },
    placeholder: "Please select...",
    name: "property_type"
  }), errors["propertyType"] && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
    className: "absolute px-1 text-red-400 tracking-widest  text-xs "
  }, errors["propertyType"][0]))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "px-10 flex flex-start items-center "
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("label", {
    className: "block py-3 pr-10"
  }, "Deposit"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "relative flex items-center w-48"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(react_dropdown__WEBPACK_IMPORTED_MODULE_2___default.a, {
    id: "deposit",
    value: deposit,
    options: [{
      label: "5",
      value: "5"
    }, {
      label: "10",
      value: "10"
    }],
    onChange: function onChange(option) {
      setDeposit(option.value);
    },
    placeholder: "Please select...",
    name: "deposit"
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
    className: "font-extrabold pl-3"
  }, " % "), errors["deposit"] && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
    className: "absolute px-1 text-red-400 tracking-widest  text-xs "
  }, errors["deposit"][0])))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("h1", {
    className: "font-semibold mt-5"
  }, "Project Address"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "w-full"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "py-3 flex w-full items-center"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("label", {
    className: "w-32"
  }, "Address Line 1"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: " flex-1 relative"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_base__WEBPACK_IMPORTED_MODULE_5__["TextInput"], {
    className: " border-gray-400 border pl-2 py-1",
    border: false,
    name: "project_address_line_1"
  }), errors["project_address_line_1"] && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
    className: "absolute px-1 text-red-400 tracking-widest  text-xs "
  }, errors["project_address_line_1"][0]))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "py-3 flex justify-between"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "flex items-center"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("label", {
    className: "w-32 block py-3"
  }, "Suburb "), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    style: {
      width: "320px"
    },
    className: "flex-1 relative"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_base__WEBPACK_IMPORTED_MODULE_5__["TextInput"], {
    className: " border-gray-400 border py-1 pl-2",
    border: false,
    name: "project_address_suburb"
  }), errors["project_address_suburb"] && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
    className: "absolute px-1 text-red-400 tracking-widest  text-xs "
  }, errors["project_address_suburb"][0]))), !isProjectLand && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "ml-5 flex items-center"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("label", {
    className: "w-32 block py-3"
  }, "City "), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "relative"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_base__WEBPACK_IMPORTED_MODULE_5__["TextInput"], {
    className: " border-gray-400 border pl-2 py-1",
    border: false,
    name: "project_address_city"
  }), errors["project_address_city"] && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
    className: "absolute px-1 text-red-400 tracking-widest  text-xs "
  }, errors["project_address_city"][0])))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "py-3 flex justify-between"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "py-3 flex items-center"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("label", {
    className: "w-32 block py-3"
  }, "State "), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "relative"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(react_dropdown__WEBPACK_IMPORTED_MODULE_2___default.a, {
    id: "project_address_state",
    value: projectState,
    options: _helpers_propertyHelper_propertyHelper__WEBPACK_IMPORTED_MODULE_10__["states"],
    onChange: function onChange(option) {
      setProjectState(option.value);
    },
    placeholder: "Select state",
    name: "project_address_state"
  }), errors["project_address_state"] && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
    className: "absolute px-1 text-red-400 tracking-widest  text-xs "
  }, errors["project_address_state"][0]))), !isProjectLand && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "ml-5 flex items-center"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("label", {
    className: "w-32 block py-3"
  }, "Postcode/ZIP "), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "relative"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_base__WEBPACK_IMPORTED_MODULE_5__["TextInput"], {
    className: " border-gray-400 border pl-2 py-1",
    border: false,
    name: "project_address_zip"
  }), errors["project_address_zip"] && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
    className: "absolute px-1 text-red-400 tracking-widest  text-xs "
  }, errors["project_address_zip"][0])))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "py-3 flex items-center"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("label", {
    className: "w-32 block py-3"
  }, "Country "), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "relative"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(react_dropdown__WEBPACK_IMPORTED_MODULE_2___default.a, {
    id: "project_address_country",
    value: projectCountry,
    options: countries,
    onChange: function onChange(option) {
      setProjectCountry(option.value);
    },
    name: "project_address_country"
  }), errors["project_address_country"] && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
    className: "absolute px-1 text-red-400 tracking-widest  text-xs "
  }, errors["project_address_country"][0])))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "flex justify-between mt-5 items-center"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("h1", {
    className: "font-semibold"
  }, " ", isProjectLand ? "Office" : "Display Suite", " Address"), " ", /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "p-1 flex items-center"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("input", {
    type: "checkbox",
    name: "same_address_check",
    id: "same_address",
    onChange: function onChange(e) {
      setSame(e.target.checked);
    }
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
    className: "p-1"
  }, isProjectLand ? "Office" : "Display Suite", " Address is same as Project Address"))), !sameAddress && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "py-3 flex w-full items-center"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("label", {
    className: "w-32"
  }, "Address Line 1"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "flex-1 relative"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_base__WEBPACK_IMPORTED_MODULE_5__["TextInput"], {
    className: " border-gray-400 border flex-1 pl-2 py-1",
    border: false,
    name: "display_suite_address_line_1"
  }), errors["display_suite_address_line_1"] && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
    className: "absolute px-1 text-red-400 tracking-widest  text-xs "
  }, errors["display_suite_address_line_1"][0]))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "py-3 flex justify-between"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "flex items-center"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("label", {
    className: "w-32 block py-3"
  }, "Suburb "), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    style: {
      width: "320px"
    },
    className: "flex-1 relative"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_base__WEBPACK_IMPORTED_MODULE_5__["TextInput"], {
    className: " border-gray-400 border py-1 pl-2",
    border: false,
    name: "display_suite_address_suburb"
  }), errors["display_suite_address_suburb"] && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
    className: "absolute px-1 text-red-400 tracking-widest  text-xs "
  }, errors["display_suite_address_suburb"][0]))), !isProjectLand && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "ml-5 flex items-center"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("label", {
    className: "w-32 block py-3"
  }, "City "), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "relative"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_base__WEBPACK_IMPORTED_MODULE_5__["TextInput"], {
    className: " border-gray-400 border pl-2 py-1",
    border: false,
    name: "display_suite_address_city"
  }), errors["display_suite_address_city"] && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
    className: "absolute px-1 text-red-400 tracking-widest  text-xs "
  }, errors["display_suite_address_city"][0])))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "py-3 flex justify-between"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "py-3 flex items-center"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("label", {
    className: "w-32 block py-3"
  }, "State "), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "relative"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(react_dropdown__WEBPACK_IMPORTED_MODULE_2___default.a, {
    id: "display_suite_address_state",
    value: displaySuiteState,
    options: _helpers_propertyHelper_propertyHelper__WEBPACK_IMPORTED_MODULE_10__["states"],
    onChange: function onChange(option) {
      setDisplaySuiteState(option.value);
    },
    placeholder: "Select state",
    name: "display_suite_address_state"
  }), errors["display_suite_address_state"] && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
    className: "absolute px-1 text-red-400 tracking-widest  text-xs "
  }, errors["display_suite_address_state"][0]))), !isProjectLand && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "ml-5 flex items-center"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("label", {
    className: "w-32 block py-3"
  }, "Postcode/ZIP "), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "relative"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_base__WEBPACK_IMPORTED_MODULE_5__["TextInput"], {
    className: " border-gray-400 border pl-2 py-1",
    border: false,
    name: "display_suite_address_zip"
  }), errors["display_suite_address_zip"] && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
    className: "absolute px-1 text-red-400 tracking-widest  text-xs "
  }, errors["display_suite_address_zip"][0])))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "py-3 flex items-center"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("label", {
    className: "w-32 block py-3"
  }, "Country "), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "relative"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(react_dropdown__WEBPACK_IMPORTED_MODULE_2___default.a, {
    id: "display_suite_address_country",
    value: displaySuiteCountry,
    options: countries,
    onChange: function onChange(option) {
      setDisplaySuiteCountry(option.value);
    },
    name: "display_suite_address_country"
  }), errors["display_suite_address_country"] && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
    className: "absolute px-1 text-red-400 tracking-widest  text-xs "
  }, errors["display_suite_address_country"][0])))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "py-3 items-center"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("label", {
    className: "cursor-pointer"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("h1", {
    className: "font-semibold mb-4"
  }, "Estimated Completion/", isProjectLand ? "Land Registration" : "Settlement"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_fortawesome_react_fontawesome__WEBPACK_IMPORTED_MODULE_11__["FontAwesomeIcon"], {
    icon: _fortawesome_free_regular_svg_icons__WEBPACK_IMPORTED_MODULE_12__["faCalendarCheck"]
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(react_datepicker__WEBPACK_IMPORTED_MODULE_7___default.a, {
    id: "proposed_settlement",
    name: "proposed_settlement",
    className: "mx-2 py-1 px-2",
    showMonthDropdown: true,
    showYearDropdown: true,
    dateFormat: "PP",
    selected: date,
    disabled: completedNow,
    onSelect: function onSelect(date) {
      if (!compareDate(date)) {
        Object(_components_base_alerts_sweetAlert__WEBPACK_IMPORTED_MODULE_16__["sweetAlert"])("error", "Please select future date!");
      } else {
        setDate(date);
      }
    },
    onChange: function onChange(date) {
      if (!compareDate(date)) {
        Object(_components_base_alerts_sweetAlert__WEBPACK_IMPORTED_MODULE_16__["sweetAlert"])("error", "Please select future date!");
      } else {
        setDate(date);
      }
    }
  })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "p-1 flex items-center mt-3"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("input", {
    type: "checkbox",
    id: "completed_now",
    onChange: function onChange(e) {
      setCompletedNow(e.target.checked);
    }
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
    className: "p-1"
  }, "Completed Now"))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "py-3"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("h1", {
    className: "font-semibold"
  }, " Featured Images (min. 3 images) "), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("label", {
    className: "button ",
    htmlFor: "upload-photos"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: ""
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "w-full flex flex-wrap  cursor-pointer",
    style: {
      border: "2px dashed #ccc",
      borderRadius: "20px",
      padding: "20px",
      textAlign: "center",
      marginBottom: "0px"
    },
    onDrop: function onDrop(e) {
      return handleDrop(e);
    },
    onDragOver: function onDragOver(e) {
      e.preventDefault();
      e.stopPropagation();
    },
    onDragEnter: function onDragEnter(e) {
      e.preventDefault();
      e.stopPropagation();
    },
    onDragLeave: function onDragLeave(e) {
      e.preventDefault();
      e.stopPropagation();
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("input", {
    type: "file",
    id: "upload-photos",
    accept: "image/*",
    multiple: true,
    className: "hidden",
    name: "image[]",
    onChange: function onChange(e) {
      return handleChange(e);
    }
  }), featuredImages.map(function (image, index) {
    return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
      key: index,
      className: "rounded-sm border m-2 relative",
      style: {
        backgroundImage: "url('".concat(image.preview, "')"),
        backgroundPositionX: "center",
        width: "6.9em",
        height: "8.9rem",
        backgroundSize: "cover"
      }
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
      className: "absolute bg-palette-blue-light font-bold leading-none ml-1 mt-1 rounded-full text-white w-1/4"
    }, index + 1), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_base__WEBPACK_IMPORTED_MODULE_5__["Tooltip"], {
      title: "Remove this item"
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
      onClick: function onClick(e) {
        return handleRemove(e, index);
      },
      className: "absolute bottom-0 right-0 p-2 -mb-2 "
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_fortawesome_react_fontawesome__WEBPACK_IMPORTED_MODULE_11__["FontAwesomeIcon"], {
      className: "text-red-500 shadow-xl hover:font-bold hover:text-red-700",
      icon: _fortawesome_free_solid_svg_icons__WEBPACK_IMPORTED_MODULE_13__["faTimes"]
    }))));
  }), fmPlaceHolder.map(function (p, index) {
    return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
      key: index,
      className: "".concat(p.classes, " relative cursor-pointer flex justify-center items-center"),
      style: {
        width: "6.9rem",
        height: "8.9rem",
        border: "2px solid #ccc"
      }
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
      className: "absolute font-bold leading-none w-1/4"
    }, index + featuredImages.length + 1));
  })), limitError && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
    className: "px-1 text-red-400 tracking-widest  text-xs "
  }, "Please limit your photos to twelve(12) items only."))), errors["featuredImages"] && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
    className: "px-1 text-red-400 tracking-widest  text-xs "
  }, errors["featuredImages"][0])), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "flex justify-end my-10"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_base__WEBPACK_IMPORTED_MODULE_5__["Button"], {
    className: "font-bold rounded-full",
    disabled: loading
  }, loading && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_fortawesome_react_fontawesome__WEBPACK_IMPORTED_MODULE_11__["FontAwesomeIcon"], {
    icon: _fortawesome_free_solid_svg_icons__WEBPACK_IMPORTED_MODULE_13__["faSpinner"],
    className: "fa-spin mr-2"
  }), "Continue")))));
});

/***/ }),

/***/ "./resources/js/components/AddProjectResources.js":
/*!********************************************************!*\
  !*** ./resources/js/components/AddProjectResources.js ***!
  \********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime/regenerator */ "./node_modules/@babel/runtime/regenerator/index.js");
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _fortawesome_react_fontawesome__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @fortawesome/react-fontawesome */ "./node_modules/@fortawesome/react-fontawesome/index.es.js");
/* harmony import */ var _states_userGlobal__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../states/userGlobal */ "./resources/js/states/userGlobal.js");
/* harmony import */ var _data_index__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../data/index */ "./resources/js/data/index.js");
/* harmony import */ var _helpers_resourceHelper_index__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../helpers/resourceHelper/index */ "./resources/js/helpers/resourceHelper/index.js");
/* harmony import */ var _base__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./_base */ "./resources/js/components/_base/index.js");
/* harmony import */ var _base__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(_base__WEBPACK_IMPORTED_MODULE_6__);
/* harmony import */ var _fortawesome_free_solid_svg_icons__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @fortawesome/free-solid-svg-icons */ "./node_modules/@fortawesome/free-solid-svg-icons/index.es.js");
/* harmony import */ var _components_base_alerts_sweetAlert__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../components/_base/alerts/sweetAlert */ "./resources/js/components/_base/alerts/sweetAlert.js");
/* harmony import */ var _resources_ResourcesData__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./resources/ResourcesData */ "./resources/js/components/resources/ResourcesData.js");
/* harmony import */ var _resources_LandResourcesData__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./resources/LandResourcesData */ "./resources/js/components/resources/LandResourcesData.js");


function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _unsupportedIterableToArray(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

function _iterableToArrayLimit(arr, i) { if (typeof Symbol === "undefined" || !(Symbol.iterator in Object(arr))) return; var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }












var AddProjectResources = function AddProjectResources(_ref) {
  var toggleFetch = _ref.toggleFetch;

  var _UserGlobal = Object(_states_userGlobal__WEBPACK_IMPORTED_MODULE_3__["default"])(),
      _UserGlobal2 = _slicedToArray(_UserGlobal, 2),
      userState = _UserGlobal2[0],
      userActions = _UserGlobal2[1];

  var _useState = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])([]),
      _useState2 = _slicedToArray(_useState, 2),
      projectResources = _useState2[0],
      setProjectResources = _useState2[1];

  var resourceForm = Object(react__WEBPACK_IMPORTED_MODULE_1__["useRef"])(null);

  var _useState3 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(false),
      _useState4 = _slicedToArray(_useState3, 2),
      loading = _useState4[0],
      setLoading = _useState4[1];

  var _useState5 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])("file"),
      _useState6 = _slicedToArray(_useState5, 2),
      builderProfile = _useState6[0],
      setBuilderProfile = _useState6[1];

  var _useState7 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(""),
      _useState8 = _slicedToArray(_useState7, 2),
      builderProfileLink = _useState8[0],
      setBuilderProfileLink = _useState8[1];

  var _useState9 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])("file"),
      _useState10 = _slicedToArray(_useState9, 2),
      arcProfile = _useState10[0],
      setArcProfile = _useState10[1];

  var _useState11 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(""),
      _useState12 = _slicedToArray(_useState11, 2),
      arcProfileLink = _useState12[0],
      setArcProfileLink = _useState12[1];

  var _useState13 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])("file"),
      _useState14 = _slicedToArray(_useState13, 2),
      devProfile = _useState14[0],
      setDevProfile = _useState14[1];

  var _useState15 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(""),
      _useState16 = _slicedToArray(_useState15, 2),
      devProfileLink = _useState16[0],
      setDevProfileLink = _useState16[1];

  var _useState17 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(""),
      _useState18 = _slicedToArray(_useState17, 2),
      projectVideo = _useState18[0],
      setProjectVideo = _useState18[1];

  var handleSubmit = /*#__PURE__*/function () {
    var _ref2 = _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee(e) {
      return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee$(_context) {
        while (1) {
          switch (_context.prev = _context.next) {
            case 0:
              e.preventDefault();
              saveResources();

            case 2:
            case "end":
              return _context.stop();
          }
        }
      }, _callee);
    }));

    return function handleSubmit(_x) {
      return _ref2.apply(this, arguments);
    };
  }();

  var saveResources = /*#__PURE__*/function () {
    var _ref3 = _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee2() {
      var formData, url;
      return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee2$(_context2) {
        while (1) {
          switch (_context2.prev = _context2.next) {
            case 0:
              formData = new FormData(resourceForm.current);
              projectResources.forEach(function (resource, key) {
                if (arcProfile === "text" && resource.name === "architect_profile") {
                  return;
                }

                if (builderProfile === "text" && resource.name === "builder_profile") {
                  return;
                }

                if (devProfile === "text" && resource.name === "developer_profile") {
                  return;
                }

                formData.append("resources[".concat(key, "][file]"), resource.file);
                formData.append("resources[".concat(key, "][name]"), resource.name);
              });

              if (devProfileLink) {
                formData.append("devProfileLink", _helpers_resourceHelper_index__WEBPACK_IMPORTED_MODULE_5__["resources"].appendHttpsToResource(devProfileLink));
              }

              if (arcProfileLink) {
                formData.append("arcProfileLink", _helpers_resourceHelper_index__WEBPACK_IMPORTED_MODULE_5__["resources"].appendHttpsToResource(arcProfileLink));
              }

              if (builderProfileLink) {
                formData.append("builderProfileLink", _helpers_resourceHelper_index__WEBPACK_IMPORTED_MODULE_5__["resources"].appendHttpsToResource(builderProfileLink));
              }

              setLoading(true);
              formData.append("id", userState.projectData.id);
              formData.append("name", userState.projectData.name);
              formData.append("projectVideo", projectVideo);
              url = "/api/project-resource/upload";
              _context2.next = 12;
              return Object(_data_index__WEBPACK_IMPORTED_MODULE_4__["saveUpdateResources"])({
                formData: formData,
                url: url
              }).then(function (r) {
                if (r.status === 200) {
                  userActions.setState({
                    projectData: r.data
                  });
                  userActions.setState({
                    currentProjStep: 3
                  });
                }

                toggleFetch();
                setLoading(false);
                Object(_components_base_alerts_sweetAlert__WEBPACK_IMPORTED_MODULE_8__["sweetAlert"])("success", "Project resources successfully saved.");
              })["catch"](function (err) {
                setLoading(false);
              }).then(function () {});

            case 12:
            case "end":
              return _context2.stop();
          }
        }
      }, _callee2);
    }));

    return function saveResources() {
      return _ref3.apply(this, arguments);
    };
  }();

  var handleFileChange = function handleFileChange(e, resource) {
    e.preventDefault();
    var c = projectResources.filter(function (file) {
      if (file["name"] !== resource) {
        return file;
      }
    });

    if (e.target.files.length > 0) {
      setProjectResources(c.concat(_helpers_resourceHelper_index__WEBPACK_IMPORTED_MODULE_5__["resources"].trimFilename(e.target.files[0], resource)));
      e.target.value = null;
    }
  };

  var handleDelete = function handleDelete(e, resource) {
    e.preventDefault();
    setProjectResources(projectResources.filter(function (file) {
      if (file["name"] !== resource) {
        return file;
      }
    }));
    e.target.value = null;
  };

  var getFileName = function getFileName(resource) {
    var filename = "";
    projectResources.forEach(function (file) {
      if (file["name"] === resource) {
        filename = file["file"]["name"];
      }
    });
    return filename;
  };

  var changeInputType = function changeInputType(rowName, checked) {
    if (rowName === "Builder Profile") {
      if (checked) {
        setBuilderProfile("text");
      } else {
        setBuilderProfileLink("");
        setBuilderProfile("file");
      }
    }

    if (rowName === "Developer Profile") {
      if (checked) {
        setDevProfile("text");
      } else {
        setDevProfileLink("");
        setDevProfile("file");
      }
    }

    if (rowName === "Architect Profile") {
      if (checked) {
        setArcProfile("text");
      } else {
        setArcProfileLink("");
        setArcProfile("file");
      }
    }
  };

  var setCheckedState = function setCheckedState(row) {
    var retval = false;

    if (row.label === "Builder Profile") {
      if (builderProfile === "text") {
        retval = true;
      }
    }

    if (row.label === "Developer Profile") {
      if (devProfile === "text") {
        retval = true;
      }
    }

    if (row.label === "Architect Profile") {
      if (arcProfile === "text") {
        retval = true;
      }
    }

    return retval;
  };

  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "bg-white rounded-lg py-5 px-8 m-5"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("form", {
    ref: resourceForm,
    onSubmit: handleSubmit
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("table", {
    className: "w-full"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("thead", {
    className: "w-full"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("tr", null, _helpers_resourceHelper_index__WEBPACK_IMPORTED_MODULE_5__["tableHeaders"].map(function (header, key) {
    return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("th", {
      key: key,
      className: " ".concat(header.style, " border-b font-bold px-4 py-2 text-palette-gray px-4 py-2 ").concat(header.width, " ")
    }, header.label);
  }))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("tbody", {
    className: "w-full "
  }, userState.projectData && userState.projectData.sub_property_id == "4" ? /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_resources_LandResourcesData__WEBPACK_IMPORTED_MODULE_10__["default"], {
    devProfile: devProfile,
    getFileName: getFileName,
    setCheckedState: setCheckedState,
    changeInputType: changeInputType,
    handleFileChange: handleFileChange,
    handleDelete: handleDelete,
    setProjectVideo: setProjectVideo,
    setDevProfileLink: setDevProfileLink
  }) : /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_resources_ResourcesData__WEBPACK_IMPORTED_MODULE_9__["default"], {
    builderProfile: builderProfile,
    devProfile: devProfile,
    arcProfile: arcProfile,
    getFileName: getFileName,
    setCheckedState: setCheckedState,
    changeInputType: changeInputType,
    handleFileChange: handleFileChange,
    handleDelete: handleDelete,
    setProjectVideo: setProjectVideo,
    setDevProfileLink: setDevProfileLink
  }))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "flex justify-end mt-5"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_base__WEBPACK_IMPORTED_MODULE_6__["Button"], {
    className: "font-bold rounded-full",
    disabled: loading
  }, loading && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_fortawesome_react_fontawesome__WEBPACK_IMPORTED_MODULE_2__["FontAwesomeIcon"], {
    icon: _fortawesome_free_solid_svg_icons__WEBPACK_IMPORTED_MODULE_7__["faSpinner"],
    className: "fa-spin mr-2"
  }), "Save & Continue"))));
};

/* harmony default export */ __webpack_exports__["default"] = (AddProjectResources);

/***/ }),

/***/ "./resources/js/components/AddPropertyModal.js":
/*!*****************************************************!*\
  !*** ./resources/js/components/AddPropertyModal.js ***!
  \*****************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react_dropdown__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react-dropdown */ "./node_modules/react-dropdown/dist/index.js");
/* harmony import */ var react_dropdown__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react_dropdown__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _helpers_styles_dropdown_css__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../helpers/styles/dropdown.css */ "./resources/js/helpers/styles/dropdown.css");
/* harmony import */ var _helpers_styles_dropdown_css__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(_helpers_styles_dropdown_css__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _base__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./_base */ "./resources/js/components/_base/index.js");
/* harmony import */ var _base__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(_base__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var draft_js__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! draft-js */ "./node_modules/draft-js/lib/Draft.js");
/* harmony import */ var draft_js__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(draft_js__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var _helpers_styles_datepicker_css__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../helpers/styles/datepicker.css */ "./resources/js/helpers/styles/datepicker.css");
/* harmony import */ var _helpers_styles_datepicker_css__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(_helpers_styles_datepicker_css__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var draft_convert__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! draft-convert */ "./node_modules/draft-convert/esm/index.js");
/* harmony import */ var react_currency_format__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! react-currency-format */ "./node_modules/react-currency-format/lib/currency-format.js");
/* harmony import */ var react_currency_format__WEBPACK_IMPORTED_MODULE_7___default = /*#__PURE__*/__webpack_require__.n(react_currency_format__WEBPACK_IMPORTED_MODULE_7__);
/* harmony import */ var _fortawesome_react_fontawesome__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @fortawesome/react-fontawesome */ "./node_modules/@fortawesome/react-fontawesome/index.es.js");
/* harmony import */ var _fortawesome_free_solid_svg_icons__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @fortawesome/free-solid-svg-icons */ "./node_modules/@fortawesome/free-solid-svg-icons/index.es.js");
/* harmony import */ var _states_userGlobal__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ../states/userGlobal */ "./resources/js/states/userGlobal.js");
/* harmony import */ var _material_ui_icons__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! @material-ui/icons */ "./node_modules/@material-ui/icons/esm/index.js");
/* harmony import */ var lodash_isEmpty__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! lodash/isEmpty */ "./node_modules/lodash/isEmpty.js");
/* harmony import */ var lodash_isEmpty__WEBPACK_IMPORTED_MODULE_12___default = /*#__PURE__*/__webpack_require__.n(lodash_isEmpty__WEBPACK_IMPORTED_MODULE_12__);
/* harmony import */ var _base_alerts_sweetAlert__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ./_base/alerts/sweetAlert */ "./resources/js/components/_base/alerts/sweetAlert.js");
/* harmony import */ var _data_index__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ../data/index */ "./resources/js/data/index.js");
/* harmony import */ var _helpers_resourceHelper_index__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! ../helpers/resourceHelper/index */ "./resources/js/helpers/resourceHelper/index.js");
/* harmony import */ var _helpers_propertyHelper_propertyHelper__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! ../helpers/propertyHelper/propertyHelper */ "./resources/js/helpers/propertyHelper/propertyHelper.js");
/* harmony import */ var _helpers_fileHelper_fileHelper__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! ../helpers/fileHelper/fileHelper */ "./resources/js/helpers/fileHelper/fileHelper.js");
function _toConsumableArray(arr) { return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _unsupportedIterableToArray(arr) || _nonIterableSpread(); }

function _nonIterableSpread() { throw new TypeError("Invalid attempt to spread non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _iterableToArray(iter) { if (typeof Symbol !== "undefined" && Symbol.iterator in Object(iter)) return Array.from(iter); }

function _arrayWithoutHoles(arr) { if (Array.isArray(arr)) return _arrayLikeToArray(arr); }

function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _unsupportedIterableToArray(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

function _iterableToArrayLimit(arr, i) { if (typeof Symbol === "undefined" || !(Symbol.iterator in Object(arr))) return; var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }




















/* harmony default export */ __webpack_exports__["default"] = (function (_ref) {
  var toggleAddPropertyModal = _ref.toggleAddPropertyModal,
      projectName = _ref.projectName,
      projectId = _ref.projectId,
      addToggleFetch = _ref.addToggleFetch;

  var _UserGlobal = Object(_states_userGlobal__WEBPACK_IMPORTED_MODULE_10__["default"])(),
      _UserGlobal2 = _slicedToArray(_UserGlobal, 2),
      userState = _UserGlobal2[0],
      userActions = _UserGlobal2[1];

  var _useState = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])(false),
      _useState2 = _slicedToArray(_useState, 2),
      loading = _useState2[0],
      setLoading = _useState2[1];

  var _useState3 = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])([]),
      _useState4 = _slicedToArray(_useState3, 2),
      featuredImages = _useState4[0],
      setFeaturedImages = _useState4[1];

  var _useState5 = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])(""),
      _useState6 = _slicedToArray(_useState5, 2),
      state = _useState6[0],
      setState = _useState6[1];

  var _useState7 = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])(""),
      _useState8 = _slicedToArray(_useState7, 2),
      propertyType = _useState8[0],
      setPropertyType = _useState8[1];

  var _useState9 = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])(""),
      _useState10 = _slicedToArray(_useState9, 2),
      subPropertyType = _useState10[0],
      setSubPropertyType = _useState10[1];

  var _useState11 = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])(""),
      _useState12 = _slicedToArray(_useState11, 2),
      ownershipType = _useState12[0],
      setOwnershipType = _useState12[1];

  var _useState13 = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])([]),
      _useState14 = _slicedToArray(_useState13, 2),
      errors = _useState14[0],
      setErrors = _useState14[1];

  var _useState15 = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])(function () {
    return draft_js__WEBPACK_IMPORTED_MODULE_4__["EditorState"].createEmpty();
  }),
      _useState16 = _slicedToArray(_useState15, 2),
      editorState = _useState16[0],
      setEditorState = _useState16[1];

  var _useState17 = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])([]),
      _useState18 = _slicedToArray(_useState17, 2),
      floorPlan = _useState18[0],
      setFLoorPlan = _useState18[1];

  var _useState19 = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])(""),
      _useState20 = _slicedToArray(_useState19, 2),
      filename = _useState20[0],
      setFilename = _useState20[1];

  var _useState21 = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])(false),
      _useState22 = _slicedToArray(_useState21, 2),
      limitError = _useState22[0],
      setLimitError = _useState22[1];

  var _useState23 = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])([]),
      _useState24 = _slicedToArray(_useState23, 2),
      fmPlaceHolder = _useState24[0],
      setFmPlaceHolder = _useState24[1];

  var _useState25 = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])(""),
      _useState26 = _slicedToArray(_useState25, 2),
      strata = _useState26[0],
      setStrata = _useState26[1];

  var _useState27 = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])(""),
      _useState28 = _slicedToArray(_useState27, 2),
      water = _useState28[0],
      setWater = _useState28[1];

  var _useState29 = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])(""),
      _useState30 = _slicedToArray(_useState29, 2),
      council = _useState30[0],
      setCouncil = _useState30[1];

  var _useState31 = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])(_helpers_propertyHelper_propertyHelper__WEBPACK_IMPORTED_MODULE_16__["costDueDates"]),
      _useState32 = _slicedToArray(_useState31, 2),
      dates = _useState32[0],
      setDates = _useState32[1];

  var floorPlanRef = Object(react__WEBPACK_IMPORTED_MODULE_0__["useRef"])(null); // const [projectId, setProjectId] = useState(userState.projectData.id);

  var addPropertyForm = Object(react__WEBPACK_IMPORTED_MODULE_0__["useRef"])(null);
  Object(react__WEBPACK_IMPORTED_MODULE_0__["useEffect"])(function () {
    var phs = [];

    for (var index = 0; index < 6 - featuredImages.length; index++) {
      var ph = {
        classes: "cursor-pointer m-2 inline-flex bg-transparent text-palette-gray"
      };
      phs.push(ph);
    }

    setFmPlaceHolder(phs);
  }, [featuredImages.length]);

  var handleClose = function handleClose(e) {
    e.preventDefault();
    toggleAddPropertyModal();
  };

  var handleSubmit = function handleSubmit(e) {
    e.preventDefault();
    setLimitError(false);
    saveProperty();
  };

  var saveProperty = function saveProperty() {
    setLoading(true);
    var formData = new FormData(addPropertyForm.current);
    formData.append("role", "project_developer");

    if (floorPlan && !lodash_isEmpty__WEBPACK_IMPORTED_MODULE_12___default()(floorPlan)) {
      formData.append("floorPlan[file]", floorPlan[0]);
    }

    formData.append("projectId", projectId);
    formData.append("propertyType", propertyType);
    formData.append("description", editorState.getCurrentContent().getPlainText());
    formData.append("descriptionHTML", Object(draft_convert__WEBPACK_IMPORTED_MODULE_6__["convertToHTML"])(editorState.getCurrentContent()));
    formData.append("water", water);
    formData.append("council", council);
    formData.append("strata", strata);
    featuredImages.map(function (image, key) {
      formData.append("featuredImages[".concat(key, "]"), image.image);
    });
    var url = "/api/property/";
    Object(_data_index__WEBPACK_IMPORTED_MODULE_14__["saveUpdateProperty"])({
      formData: formData,
      url: url
    }).then(function (retVal) {
      if (retVal.status === 200) {
        var project = userState.projectData;
        project.properties = retVal.data;
        userActions.setState({
          projectData: project
        });
        setLoading(false);
        Object(_base_alerts_sweetAlert__WEBPACK_IMPORTED_MODULE_13__["sweetAlert"])("success", "Property successfully added.");
        addToggleFetch();
        toggleAddPropertyModal();
      }
    })["catch"](function (error) {
      if (error.response.data.errors) {
        Object(_base_alerts_sweetAlert__WEBPACK_IMPORTED_MODULE_13__["sweetAlert"])("error", "Some field are missing.");
      }

      setLoading(false);
      setErrors(error.response.data.errors);
    }).then(function () {});
  };

  var handleDrop = function handleDrop(e) {
    e.preventDefault();
    e.stopPropagation();
    var fmCount = featuredImages.length;
    var onCount = e.dataTransfer.files.length;

    if (fmCount + onCount > 6) {
      e.target.value = null;
      setLimitError(true);
      return;
    } else {
      setLimitError(false);
    }

    setFeaturedImages(featuredImages.concat(_toConsumableArray(_toConsumableArray(e.dataTransfer.files).map(function (file) {
      var newFilename = Object(_helpers_fileHelper_fileHelper__WEBPACK_IMPORTED_MODULE_17__["sanitizeFilename"])(file.name);
      var newFileObj = new File([file], newFilename);
      return {
        image: newFileObj,
        preview: URL.createObjectURL(newFileObj)
      };
    }))));
    e.target.value = null;
  };

  var handleChange = function handleChange(e) {
    var fmCount = featuredImages.length;
    var onCount = e.target.files.length;

    if (fmCount + onCount > 6) {
      setLimitError(true);
      e.target.value = null;
      return;
    } else {
      setLimitError(false);
    }

    setFeaturedImages(featuredImages.concat(_toConsumableArray(_toConsumableArray(e.target.files).map(function (file) {
      var newFilename = file.name.replace(/\s+/g, "");
      var newFileObj = new File([file], newFilename);
      return {
        image: newFileObj,
        preview: URL.createObjectURL(newFileObj)
      };
    }))));
    e.target.value = null;
  };

  var handleRemove = function handleRemove(e, i) {
    e.preventDefault();
    e.stopPropagation();
    setFeaturedImages(_toConsumableArray(featuredImages.filter(function (image, k) {
      if (i !== k) {
        return image;
      }
    })));

    if (featuredImages.length > 6) {
      setLimitError(true);
    } else {
      setLimitError(false);
    }
  };

  var handleFloorPlanChange = function handleFloorPlanChange(e) {
    e.preventDefault();

    if (!Object(_helpers_fileHelper_fileHelper__WEBPACK_IMPORTED_MODULE_17__["PDFFileTypeFilter"])(e.target.files[0]["name"])) {
      Object(_base_alerts_sweetAlert__WEBPACK_IMPORTED_MODULE_13__["sweetAlert"])("error", "Filetype is not supported!");
      e.target.value = null;
      return;
    }

    setFLoorPlan(Object(_helpers_propertyHelper_propertyHelper__WEBPACK_IMPORTED_MODULE_16__["floorPlanHelper"])(e.target.files));
    var newFilename = Object(_helpers_fileHelper_fileHelper__WEBPACK_IMPORTED_MODULE_17__["sanitizeFilename"])(e.target.files[0]["name"]);
    setFilename(newFilename);
    e.target.value = null;
  };

  var handleDeleteFile = function handleDeleteFile(e) {
    e.preventDefault();
    setFLoorPlan([]);
    floorPlanRef.current.type = "";
    floorPlanRef.current.type = "file";
    setFilename("");
    e.target.value = null;
  };

  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_base__WEBPACK_IMPORTED_MODULE_3__["Modal"], {
    show: true,
    maxWidth: "md",
    title: "Add Property",
    onClose: function onClose() {
      return toggleAddPropertyModal();
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("form", {
    ref: addPropertyForm,
    className: "bg-white rounded-lg py-5 px-8 m-5",
    onSubmit: handleSubmit
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "text-black"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "py-3 flex justify-start "
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "flex justify-center items-center"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("label", {
    className: "font-semibold w-24"
  }, "Property Type"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_dropdown__WEBPACK_IMPORTED_MODULE_1___default.a, {
    id: "property_type",
    value: propertyType,
    options: _helpers_propertyHelper_propertyHelper__WEBPACK_IMPORTED_MODULE_16__["property_types"],
    onChange: function onChange(option) {
      setPropertyType(option.value);
    },
    placeholder: "Please select..." // name="property_type"
    ,
    className: "w-48"
  }), errors["propertyType"] && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
    className: "px-1 text-red-400 tracking-widest  text-xs "
  }, errors["propertyType"][0]))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "ml-10 flex-1 flex justify-end items-center"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("label", {
    className: "font-semibold w-32"
  }, "Property Number"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_base__WEBPACK_IMPORTED_MODULE_3__["TextInput"], {
    className: "border-gray-400 border pl-2 py-1",
    border: false,
    name: "unit_name",
    width: "w-24"
  }), errors["unit_name"] && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
    className: "px-1 text-red-400 tracking-widest text-xs "
  }, errors["unit_name"][0]))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "ml-4 flex-1 flex justify-end items-center"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("label", {
    className: "font-semibold w-24 mr-4"
  }, "Lot Number"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_base__WEBPACK_IMPORTED_MODULE_3__["TextInput"], {
    className: "border-gray-400 border pl-2 py-1",
    border: false,
    name: "unit_no",
    width: "w-24"
  }), errors["unit_no"] && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
    className: "px-1 text-red-400 tracking-widest  text-xs "
  }, errors["unit_no"][0])))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "flex justify-start my-3"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "flex items-center justify-start py-3"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("label", {
    className: "w-12"
  }, "Price"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "relative"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_currency_format__WEBPACK_IMPORTED_MODULE_7___default.a, {
    allowNegative: false,
    className: "border text-sm border-gray-400 px-2 py-1 w-48",
    thousandSeparator: true,
    name: "price",
    id: "price",
    prefix: "$"
  })), errors["price"] && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
    className: "absolute px-1 text-red-400 tracking-widest text-xxs "
  }, errors["price"][0]))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "py-3 flex items-center ml-12"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("label", {
    title: "Number of bedrooms",
    className: "cursor-pointer mr-4"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_material_ui_icons__WEBPACK_IMPORTED_MODULE_11__["LocalHotel"], null)), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "flex flex-col w-24"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_base__WEBPACK_IMPORTED_MODULE_3__["TextInput"], {
    className: "pl-2 text-sm border-gray-400 w-32 border py-1",
    border: false,
    type: "number",
    name: "number_of_bedrooms"
  }), errors["number_of_bedrooms"] && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
    className: "px-1 text-red-400 tracking-widest  text-xxs "
  }, errors["number_of_bedrooms"][0]))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "py-3 flex items-center ml-16"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("label", {
    title: "Number of bathrooms",
    className: "cursor-pointer mr-4"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_material_ui_icons__WEBPACK_IMPORTED_MODULE_11__["Bathtub"], null)), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "flex flex-col w-24"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_base__WEBPACK_IMPORTED_MODULE_3__["TextInput"], {
    className: "pl-2 text-sm border-gray-400 w-32 border py-1",
    border: false,
    type: "number",
    name: "number_of_bathrooms"
  }), errors["number_of_bathrooms"] && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
    className: "px-1 text-red-400 tracking-widest  text-xxs "
  }, errors["number_of_bathrooms"][0]))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "py-3 flex items-center ml-12"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("label", {
    title: "Number of garages",
    className: "cursor-pointer mr-4 "
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_material_ui_icons__WEBPACK_IMPORTED_MODULE_11__["DirectionsCar"], null)), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "flex flex-col w-24"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_base__WEBPACK_IMPORTED_MODULE_3__["TextInput"], {
    className: "pl-2 text-sm border-gray-400 w-32 border py-1",
    border: false,
    type: "number",
    name: "number_of_garages",
    min: "1"
  }), errors["number_of_garages"] && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
    className: "px-1 text-red-400 tracking-widest  text-xxs "
  }, errors["number_of_garages"][0])))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "flex"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "py-3 flex justify-start items-center"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("label", {
    className: "w-24"
  }, "Internal SQM"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "relative"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "ml-4 w-32"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_base__WEBPACK_IMPORTED_MODULE_3__["TextInput"], {
    className: "pl-2 text-sm border-gray-400 w-32 border py-1",
    border: false,
    type: "number",
    name: "internal_size"
  })), errors["internal_size"] && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
    className: "absolute px-1 text-red-400 tracking-widest  text-xxs "
  }, errors["internal_size"][0]))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "py-3 flex justify-start items-center mx-10"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("label", {
    className: "w-24"
  }, "External SQM"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "relative"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "ml-4 w-32"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_base__WEBPACK_IMPORTED_MODULE_3__["TextInput"], {
    className: "pl-2 text-sm border-gray-400 w-32 border py-1",
    border: false,
    type: "number",
    name: "external_size"
  })), errors["external_size"] && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
    className: "absolute px-1 text-red-400 tracking-widest  text-xxs "
  }, errors["external_size"][0]))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "py-3 flex justify-start items-center ml-3"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("label", {
    className: "w-24"
  }, "Parking SQM"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "relative"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "ml-4 w-32"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_base__WEBPACK_IMPORTED_MODULE_3__["TextInput"], {
    className: "pl-2 text-sm border-gray-400 w-32 border py-1 mr-1",
    border: false,
    type: "number",
    name: "parking_size"
  })), errors["parking_size"] && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
    className: "absolute px-1 text-red-400 tracking-widest  text-xxs "
  }, errors["parking_size"][0])))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "flex justify-center my-3"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "py-3 flex flex-1 items-center"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("label", {
    title: "Strata",
    className: "cursor-pointer mr-4 ".concat(errors["strata"] ? "mb-4" : "mb-0")
  }, "Strata"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "flex ml-2"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_currency_format__WEBPACK_IMPORTED_MODULE_7___default.a, {
    allowNegative: false,
    className: "border text-sm border-gray-400 px-2 py-1 w-32",
    thousandSeparator: true,
    name: "strata_cost",
    id: "strata_cost",
    prefix: "$"
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_dropdown__WEBPACK_IMPORTED_MODULE_1___default.a, {
    id: "strata",
    value: strata,
    options: dates,
    onChange: function onChange(option) {
      setStrata(option.value);
    },
    placeholder: "Please select...",
    name: "strata",
    className: "w-48 ml-2"
  })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", null, errors["strata"] && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
    className: "px-1 text-red-400 tracking-widest  text-xxs "
  }, errors["strata"][0]), errors["strata_cost"] && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
    className: "px-1 text-red-400 tracking-widest  text-xxs "
  }, errors["strata_cost"][0])))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "flex flex-1 items-center justify-end py-3"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("label", {
    title: "Water",
    className: "cursor-pointer mr-4 ".concat(errors["water"] ? "mb-4" : "mb-0")
  }, "Water"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "flex ml-2"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_currency_format__WEBPACK_IMPORTED_MODULE_7___default.a, {
    allowNegative: false,
    className: "border text-sm border-gray-400 px-2 py-1 w-32",
    thousandSeparator: true,
    name: "water_cost",
    id: "water_cost",
    prefix: "$"
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_dropdown__WEBPACK_IMPORTED_MODULE_1___default.a, {
    id: "water",
    value: water,
    options: dates,
    onChange: function onChange(option) {
      setWater(option.value);
    },
    placeholder: "Please select...",
    name: "water",
    className: "w-48 ml-2"
  })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", null, errors["water"] && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
    className: "px-1 text-red-400 tracking-widest  text-xxs "
  }, errors["water"][0]), errors["water_cost"] && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
    className: "px-1 text-red-400 tracking-widest  text-xxs "
  }, errors["water_cost"][0]))))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "flex justify-center my-3"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "py-3 flex flex-1 items-center"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("label", {
    title: "Strata",
    className: "cursor-pointer mr-3 ".concat(errors["council"] ? "mb-4" : "mb-0")
  }, "Council"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "flex ml-1"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_currency_format__WEBPACK_IMPORTED_MODULE_7___default.a, {
    allowNegative: false,
    className: "border text-sm border-gray-400 px-2 py-1 w-32",
    thousandSeparator: true,
    name: "council_cost",
    id: "council_cost",
    prefix: "$"
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_dropdown__WEBPACK_IMPORTED_MODULE_1___default.a, {
    id: "council",
    value: council,
    options: dates,
    onChange: function onChange(option) {
      setCouncil(option.value);
    },
    placeholder: "Please select...",
    name: "council",
    className: "w-48 ml-2"
  })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", null, errors["council"] && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
    className: "px-1 text-red-400 tracking-widest  text-xxs "
  }, errors["council"][0]), errors["council_cost"] && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
    className: "px-1 text-red-400 tracking-widest  text-xxs "
  }, errors["council_cost"][0]))))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "py-3"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("label", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "w-full text-sm bg-white text-black "
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_base__WEBPACK_IMPORTED_MODULE_3__["RichTextEditor"], {
    key: "addProperty",
    title: "Property Description",
    richEditorState: editorState,
    richEditorStateContent: function richEditorStateContent(content) {
      return setEditorState(content);
    },
    characterLimit: 2000
  }))), errors["description"] && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
    className: "px-1 text-red-400 tracking-widest  text-xs "
  }, errors["description"][0])), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "py-3"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("h1", {
    className: "font-semibold"
  }, " Featured Images (min. 3 images) "), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("label", {
    className: "button ",
    htmlFor: "upload-photos"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: ""
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "w-full flex flex-wrap  cursor-pointer",
    style: {
      border: "2px dashed #ccc",
      borderRadius: "20px",
      padding: "20px",
      textAlign: "center",
      marginBottom: "0px"
    },
    onDrop: function onDrop(e) {
      return handleDrop(e);
    },
    onDragOver: function onDragOver(e) {
      e.preventDefault();
      e.stopPropagation();
    },
    onDragEnter: function onDragEnter(e) {
      e.preventDefault();
      e.stopPropagation();
    },
    onDragLeave: function onDragLeave(e) {
      e.preventDefault();
      e.stopPropagation();
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("input", {
    type: "file",
    id: "upload-photos",
    accept: "image/*",
    multiple: true,
    className: "hidden",
    name: "image[]",
    onChange: function onChange(e) {
      return handleChange(e);
    }
  }), featuredImages.map(function (image, index) {
    return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      key: index,
      className: "rounded-sm border m-2 relative",
      style: {
        backgroundImage: "url('".concat(image.preview, "')"),
        backgroundPositionX: "center",
        width: "6.9em",
        height: "8.9rem",
        backgroundSize: "cover"
      }
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_base__WEBPACK_IMPORTED_MODULE_3__["Tooltip"], {
      title: "Remove this item"
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
      onClick: function onClick(e) {
        return handleRemove(e, index);
      },
      className: "absolute bottom-0 right-0 p-2 -mb-2 "
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_fortawesome_react_fontawesome__WEBPACK_IMPORTED_MODULE_8__["FontAwesomeIcon"], {
      className: "text-red-500 shadow-xl hover:font-bold hover:text-red-700",
      icon: _fortawesome_free_solid_svg_icons__WEBPACK_IMPORTED_MODULE_9__["faTimes"]
    }))));
  }), fmPlaceHolder.map(function (p, i) {
    return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      key: i,
      className: "".concat(p.classes, " cursor-pointer flex justify-center items-center"),
      style: {
        width: "6.9rem",
        height: "8.9rem",
        border: "2px solid #ccc"
      }
    }, " ", /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_fortawesome_react_fontawesome__WEBPACK_IMPORTED_MODULE_8__["FontAwesomeIcon"], {
      className: "text-xl",
      icon: _fortawesome_free_solid_svg_icons__WEBPACK_IMPORTED_MODULE_9__["faCamera"]
    }), " ");
  })), limitError && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
    className: "px-1 text-red-400 tracking-widest  text-xs "
  }, "Please limit your photos to six items only."))), errors["featuredImages"] && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
    className: "px-1 text-red-400 tracking-widest  text-xs "
  }, errors["featuredImages"][0])), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "flex flex-col mb-10"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "flex-1"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("h1", {
    className: "font-semibold"
  }, "Floor Plan "), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "py-3 flex flex-col"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "flex"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("label", {
    className: "w-full"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "cursor-pointer w-full p-3 rounded-lg flex",
    style: {
      border: "2px dashed #ccc",
      borderRadius: "20px"
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("input", {
    id: "floorPlan",
    ref: floorPlanRef,
    type: "file",
    name: "floor_plan",
    onChange: function onChange(e) {
      return handleFloorPlanChange(e);
    },
    className: "hidden border text-sm border-gray-400 px-2 py-1 w-full py-1"
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("label", {
    htmlFor: "floorPlan",
    className: " cursor-pointer hover:text-palette-blue-light ".concat(filename ? "text-palette-blue-light" : "")
  }, filename || "Select File"), filename && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "cursor-pointer ml-4 mr-8 text-lg text-red-500"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_fortawesome_react_fontawesome__WEBPACK_IMPORTED_MODULE_8__["FontAwesomeIcon"], {
    onClick: function onClick(e) {
      return handleDeleteFile(e);
    },
    icon: _fortawesome_free_solid_svg_icons__WEBPACK_IMPORTED_MODULE_9__["faTimes"]
  })))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "flex items-center justify-end w-1/2"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_base__WEBPACK_IMPORTED_MODULE_3__["Button"], {
    className: "font-bold rounded-full",
    disabled: loading
  }, loading && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_fortawesome_react_fontawesome__WEBPACK_IMPORTED_MODULE_8__["FontAwesomeIcon"], {
    icon: _fortawesome_free_solid_svg_icons__WEBPACK_IMPORTED_MODULE_9__["faSpinner"],
    className: "fa-spin mr-2"
  }), "Save")))))))));
});

/***/ }),

/***/ "./resources/js/components/AddingSteps.js":
/*!************************************************!*\
  !*** ./resources/js/components/AddingSteps.js ***!
  \************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _states_userGlobal__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../states/userGlobal */ "./resources/js/states/userGlobal.js");
function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _unsupportedIterableToArray(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

function _iterableToArrayLimit(arr, i) { if (typeof Symbol === "undefined" || !(Symbol.iterator in Object(arr))) return; var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }




var AddingSteps = function AddingSteps() {
  var _UserGlobal = Object(_states_userGlobal__WEBPACK_IMPORTED_MODULE_1__["default"])(),
      _UserGlobal2 = _slicedToArray(_UserGlobal, 2),
      userState = _UserGlobal2[0],
      userActions = _UserGlobal2[1];

  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "flex justify-between text-palette-blue-light px-10"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("h1", {
    className: "text-center mb-2"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
    className: "border ".concat(userState.currentProjStep === 1 ? "border-palette-blue-light" : "border-gray-600 text-gray-600", "  rounded-full py-1"),
    style: {
      fontSize: "1.2rem",
      paddingLeft: "0.85rem",
      paddingRight: "0.85rem"
    }
  }, "1")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("h4", {
    className: "font-semibold tracking-wider  ".concat(userState.currentProjStep === 1 ? "border-palette-blue-light" : "text-gray-600", "  ")
  }, "Project Details")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "flex-1 border-b border-white mx-6 mb-2 border-palette-gray"
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("h1", {
    className: "text-center mb-2"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
    className: "border ".concat(userState.currentProjStep === 2 ? "border-palette-blue-light" : "border-gray-600 text-gray-600", " rounded-full py-1 px-3"),
    style: {
      fontSize: "1.2rem"
    }
  }, "2")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("h4", {
    className: "font-semibold tracking-wider  ".concat(userState.currentProjStep === 2 ? "border-palette-blue-light" : "text-gray-600", "  ")
  }, "Project Resources")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "flex-1 border-b border-white mx-6 mb-2 border-palette-gray"
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("h1", {
    className: "text-center mb-2"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
    className: "border ".concat(userState.currentProjStep === 3 ? "border-palette-blue-light" : "border-gray-600 text-gray-600", " rounded-full py-1 px-3"),
    style: {
      fontSize: "1.2rem"
    }
  }, "3")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("h4", {
    className: "font-semibold tracking-wider  ".concat(userState.currentProjStep === 3 ? "border-palette-blue-light" : "text-gray-600", " ")
  }, "Project Properties")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "flex-1 border-b border-white mx-6 mb-2 border-palette-gray"
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("h1", {
    className: "text-center mb-2"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
    className: "border ".concat(userState.currentProjStep === 4 ? "border-palette-blue-light" : "border-gray-600 text-gray-600", " rounded-full py-1 px-3"),
    style: {
      fontSize: "1.2rem"
    }
  }, "4")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("h4", {
    className: "font-semibold tracking-wider  ".concat(userState.currentProjStep === 4 ? "border-palette-blue-light" : "text-gray-600", " ")
  }, "Agency Agreement")));
};

/* harmony default export */ __webpack_exports__["default"] = (AddingSteps);

/***/ }),

/***/ "./resources/js/components/AdditionalInfos.js":
/*!****************************************************!*\
  !*** ./resources/js/components/AdditionalInfos.js ***!
  \****************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _base__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./_base */ "./resources/js/components/_base/index.js");
/* harmony import */ var _base__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_base__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var lodash_isEmpty__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! lodash/isEmpty */ "./node_modules/lodash/isEmpty.js");
/* harmony import */ var lodash_isEmpty__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(lodash_isEmpty__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _helpers_salesAdviceHelper_salesAdviceHelper__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../helpers/salesAdviceHelper/salesAdviceHelper */ "./resources/js/helpers/salesAdviceHelper/salesAdviceHelper.js");





var AdditionalInfos = function AdditionalInfos(_ref) {
  var additionalInfoErrors = _ref.additionalInfoErrors,
      additionalInfoForm = _ref.additionalInfoForm,
      _ref$width = _ref.width,
      width = _ref$width === void 0 ? "w-full" : _ref$width;
  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("section", {
    className: "relative mt-6 rounded"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "mx-auto",
    style: {
      maxWidth: 1366
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "flex justify-center mt-4 mb-8 ".concat(width, " lg:pr-10")
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "border lg:mt-0 mt-8 px-6 rounded w-full"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("h1", {
    className: "font-bold py-6 text-base"
  }, "Additional Information"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("form", {
    ref: additionalInfoForm
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_base__WEBPACK_IMPORTED_MODULE_1__["Form"], {
    errors: additionalInfoErrors,
    formFields: _helpers_salesAdviceHelper_salesAdviceHelper__WEBPACK_IMPORTED_MODULE_3__["addtionalInfoFields"]
  }))))));
};

/* harmony default export */ __webpack_exports__["default"] = (AdditionalInfos);

/***/ }),

/***/ "./resources/js/components/AgencyAgreementPart.js":
/*!********************************************************!*\
  !*** ./resources/js/components/AgencyAgreementPart.js ***!
  \********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);


var AgencyAgreementPart = function AgencyAgreementPart(_ref) {
  var toggleSEM = _ref.toggleSEM,
      toggleFetch = _ref.toggleFetch;
  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "relative bg-white py-5 px-5 flex flex-col items-center m-5 rounded-lg"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
    src: "/assets/images/undraw_fill_forms_yltj.svg",
    height: "250px",
    width: "420px",
    style: {
      marginTop: '24px'
    }
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("h4", {
    className: "font-bold lg:w-3/4 w-full px-1 py-3 rounded-t text-2xl text-center"
  }, "Your project will be approved by a GroupBuyer Admin Agent prior to being displayed online. Should your project not meet the requirements of GroupBuyer, a team member will be in contact with you. Click the 'View Agency Agreement' button to learn more about our terms and conditions."), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("a", {
    href: "/assets/pdf/group-buyer-agency-agreement-2020-10-08-draft.pdf",
    target: "_blank",
    className: "border border-gray-900 button py-3 flex items-center justify-around mr-2 mt-2 mb-2"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
    className: "text-base text-gray-900"
  }, "View Agency Agreement")));
};

/* harmony default export */ __webpack_exports__["default"] = (AgencyAgreementPart);

/***/ }),

/***/ "./resources/js/components/AgentDetailsSection.js":
/*!********************************************************!*\
  !*** ./resources/js/components/AgentDetailsSection.js ***!
  \********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _GoogleMaps__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./GoogleMaps */ "./resources/js/components/GoogleMaps.js");
/* harmony import */ var _components_base_alerts_sweetAlert__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../components/_base/alerts/sweetAlert */ "./resources/js/components/_base/alerts/sweetAlert.js");
/* harmony import */ var _states_userGlobal__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../states/userGlobal */ "./resources/js/states/userGlobal.js");
function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _unsupportedIterableToArray(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

function _iterableToArrayLimit(arr, i) { if (typeof Symbol === "undefined" || !(Symbol.iterator in Object(arr))) return; var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }





var mapKey = "".concat("AIzaSyB2angcqMN5xDkZHAzghZ5hlg-QldC02ww");

var AgentDetailsSection = function AgentDetailsSection(_ref) {
  var project = _ref.project,
      isProjectLand = _ref.isProjectLand;

  var _useState = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])(""),
      _useState2 = _slicedToArray(_useState, 2),
      origin = _useState2[0],
      setOrigin = _useState2[1];

  var _useState3 = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])("".concat(project.lat, ",").concat(project["long"])),
      _useState4 = _slicedToArray(_useState3, 2),
      destination = _useState4[0],
      setDestination = _useState4[1];

  var _useState5 = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])("".concat(project.display_suite_lat, ", ").concat(project.display_suite_long)),
      _useState6 = _slicedToArray(_useState5, 2),
      destinationDisPlaySuite = _useState6[0],
      setDestinationDisPlaySuite = _useState6[1];

  var _UserGlobal = Object(_states_userGlobal__WEBPACK_IMPORTED_MODULE_3__["default"])(),
      _UserGlobal2 = _slicedToArray(_UserGlobal, 2),
      userState = _UserGlobal2[0],
      seState = _UserGlobal2[1];

  Object(react__WEBPACK_IMPORTED_MODULE_0__["useEffect"])(function () {
    getCurrentLocation();
  }, []);

  var getDirection = function getDirection(destination) {
    return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: "font-semibold mt-5 text-palette-purple hover:text-palette-violet transition-all duration-300"
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("a", {
      href: "https://www.google.com/maps/dir/?api=1&origin=".concat(origin, "&destination=").concat(destination, "&travelmode=driving"),
      target: "#"
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
      className: "cursor-pointer"
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("svg", {
      className: "inline feather-icon h-4 mr-2 w-4"
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("use", {
      xlinkHref: "/assets/svg/feather-sprite.svg#map-pin"
    })), "Get Directions")));
  };

  var getCurrentLocation = function getCurrentLocation() {
    if (navigator.geolocation) {
      navigator.geolocation.getCurrentPosition(function (position) {
        var currPosition = "".concat(position.coords.latitude, ",").concat(position.coords.longitude); // let currPosition = `-37.831387, 145.04581`

        setOrigin(currPosition);
      }, function () {//handle error
      });
    } else {
      // Browser doesn't support Geolocation
      Object(_components_base_alerts_sweetAlert__WEBPACK_IMPORTED_MODULE_2__["sweetAlert"])("error", "Browser doesn't support Geolocation, please update your browser!");
    }
  };

  var projectAdress = function projectAdress(project, isProjectLand) {
    return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: "lg:flex lg:flex-1 lg:items-center lg:text-left text-base text-center"
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: "lg:flex-1 my-6 lg:my-0"
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: "font-black lg:mt-0 mb-4 mt-6"
    }, isProjectLand ? "OFFICE ADDRESS" : "PROJECT ADDRESS"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
      className: "font-normal text-base"
    }, project.address ? project.address.line_1 + ", " : ""), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("br", null), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
      className: "font-normal text-base"
    }, project.address ? project.address.suburb + ", " : ""), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
      className: "font-normal text-base"
    }, project.address ? project.address.state + ", " : ""), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
      className: "font-normal text-base"
    }, project.address ? project.address.postal : ""), getDirection(destination)), !project.is_completed && !isProjectLand && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: "lg:flex-1 my-6 lg:my-0"
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: "font-black mb-4"
    }, "DISPLAY SUITE ADDRESS"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
      className: "font-normal text-base"
    }, project.display_suite_address ? project.display_suite_address.line_1 + ", " : ""), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("br", null), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
      className: "font-normal text-base"
    }, project.display_suite_address ? project.display_suite_address.suburb + ", " : ""), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
      className: "font-normal text-base"
    }, project.display_suite_address ? project.display_suite_address.state + ", " : ""), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
      className: "font-normal text-base"
    }, project.display_suite_address ? project.display_suite_address.postal : ""), getDirection(destinationDisPlaySuite)));
  };

  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("section", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "py-6",
    style: {
      background: "rgba(177, 173, 173, 0.21)"
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "mx-auto justify-center lg:flex lg:flex-row",
    style: {
      maxWidth: 1366
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "lg:flex lg:flex-1 text-center text-gray-700"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "flex flex-col items-center lg:flex-1 lg:flex-row"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "lg:-mt-20 lg:flex lg:flex-1 lg:flex-col lg:items-center"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "mt-2 text-center"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
    src: "/assets/images/_directors/luke-hayes.jpg",
    className: "mx-auto rounded-full",
    style: {
      width: 120
    }
  })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "text-center mt-2 text-center"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "text-base text-gray-900 mt-2 font-bold"
  }, project && project.agent_name), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "border rounded my-1 border-gray-700 font-normal text-base text-center text-gray-900 inline-block"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("a", {
    className: "px-4",
    href: "tel:".concat(project.agent_phone)
  }, project && project.agent_phone)), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "font-bold text-base text-gray-900 truncate w-48 hover:text-palette-purple duration-300 transition-all"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("a", {
    href: "mailto:".concat(project.agent_email)
  }, project && project.agent_email)))))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "lg:flex lg:flex-1 lg:items-center"
  }, projectAdress(project, isProjectLand)))), project && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_GoogleMaps__WEBPACK_IMPORTED_MODULE_1__["default"], {
    mapProps: project
  }));
};

/* harmony default export */ __webpack_exports__["default"] = (AgentDetailsSection);

/***/ }),

/***/ "./resources/js/components/BookInspection.js":
/*!***************************************************!*\
  !*** ./resources/js/components/BookInspection.js ***!
  \***************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime/regenerator */ "./node_modules/@babel/runtime/regenerator/index.js");
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _base__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./_base */ "./resources/js/components/_base/index.js");
/* harmony import */ var _base__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(_base__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _material_ui_lab_Alert__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @material-ui/lab/Alert */ "./node_modules/@material-ui/lab/esm/Alert/index.js");
/* harmony import */ var _material_ui_core_Snackbar__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @material-ui/core/Snackbar */ "./node_modules/@material-ui/core/esm/Snackbar/index.js");
/* harmony import */ var _fortawesome_react_fontawesome__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @fortawesome/react-fontawesome */ "./node_modules/@fortawesome/react-fontawesome/index.es.js");
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! axios */ "./node_modules/axios/index.js");
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(axios__WEBPACK_IMPORTED_MODULE_6__);
/* harmony import */ var _states_userGlobal__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../states/userGlobal */ "./resources/js/states/userGlobal.js");
/* harmony import */ var _data_propertiesData_propertiesData__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../data/propertiesData/propertiesData */ "./resources/js/data/propertiesData/propertiesData.js");
/* harmony import */ var _base_alerts_sweetAlert__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./_base/alerts/sweetAlert */ "./resources/js/components/_base/alerts/sweetAlert.js");


function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _unsupportedIterableToArray(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

function _iterableToArrayLimit(arr, i) { if (typeof Symbol === "undefined" || !(Symbol.iterator in Object(arr))) return; var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }

function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }












function Alert(props) {
  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_material_ui_lab_Alert__WEBPACK_IMPORTED_MODULE_3__["default"], _extends({
    elevation: 6,
    variant: "filled"
  }, props));
}

var BookInspection = function BookInspection(_ref) {
  var _React$createElement;

  var show = _ref.show,
      toogleShowBookingModal = _ref.toogleShowBookingModal,
      _ref$backgroundColor = _ref.backgroundColor,
      backgroundColor = _ref$backgroundColor === void 0 ? "#ffffff" : _ref$backgroundColor,
      _ref$propertyId = _ref.propertyId,
      propertyId = _ref$propertyId === void 0 ? null : _ref$propertyId,
      dealId = _ref.dealId;

  var _UserGlobal = Object(_states_userGlobal__WEBPACK_IMPORTED_MODULE_7__["default"])(),
      _UserGlobal2 = _slicedToArray(_UserGlobal, 2),
      userState = _UserGlobal2[0],
      userAction = _UserGlobal2[1];

  var _useState = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(false),
      _useState2 = _slicedToArray(_useState, 2),
      loading = _useState2[0],
      setLoading = _useState2[1];

  var _useState3 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(false),
      _useState4 = _slicedToArray(_useState3, 2),
      showAlert = _useState4[0],
      setShowAlert = _useState4[1];

  var _useState5 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])({
    name: "",
    phone: "",
    email: "",
    message: "",
    propertyId: 0,
    dealId: 0
  }),
      _useState6 = _slicedToArray(_useState5, 2),
      emailData = _useState6[0],
      setEmailData = _useState6[1];

  var _useState7 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])({}),
      _useState8 = _slicedToArray(_useState7, 2),
      errors = _useState8[0],
      setErrors = _useState8[1];

  var handleSubmit = /*#__PURE__*/function () {
    var _ref2 = _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee(e) {
      var errors, formData;
      return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee$(_context) {
        while (1) {
          switch (_context.prev = _context.next) {
            case 0:
              e.preventDefault();
              errors = {};
              formData = _objectSpread({}, emailData);

              if (formData.phone) {
                formData.phone = "+61" + emailData.phone;
              }

              if (propertyId) {
                formData.propertyId = propertyId;
              }

              formData.dealId = dealId;
              setLoading(true);
              Object(_data_propertiesData_propertiesData__WEBPACK_IMPORTED_MODULE_8__["requestBookingInspection"])({
                formData: formData
              }).then(function (res) {
                if (res.status == 200) {
                  setLoading(false);
                  Object(_base_alerts_sweetAlert__WEBPACK_IMPORTED_MODULE_9__["sweetAlert"])("success", res.data.message);
                }
              })["catch"](function (err) {
                setLoading(false);
                setErrors(err.response.data.errors);
              });

            case 8:
            case "end":
              return _context.stop();
          }
        }
      }, _callee);
    }));

    return function handleSubmit(_x) {
      return _ref2.apply(this, arguments);
    };
  }();

  var changeHandler = function changeHandler(key, value) {
    var data = Object.assign({}, emailData);
    data[key] = value;
    setEmailData(data);
  };

  var handleClose = function handleClose(event, reason) {
    if (reason === "clickaway") {
      return;
    }

    setShowAlert(false);
  };

  Object(react__WEBPACK_IMPORTED_MODULE_1__["useEffect"])(function () {
    if (userState.user) {
      var phone = userState.user.phone ? userState.user.phone.replace("+61", "") : "";
      setEmailData({
        name: userState.user.first_name + " " + userState.user.last_name,
        phone: phone,
        email: userState.user.email,
        message: ""
      });
    }
  }, []);
  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_base__WEBPACK_IMPORTED_MODULE_2__["Modal"], {
    show: show,
    maxWidth: "sm",
    title: "Book Inspection",
    onClose: function onClose() {
      return toogleShowBookingModal();
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "bg-white lg:m-6 rounded"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("form", {
    className: "lg:w-full",
    onSubmit: handleSubmit
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "lg:px-8 px-2 py-2 pt-6 pb-10 rounded-lg",
    style: {
      background: backgroundColor
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_base__WEBPACK_IMPORTED_MODULE_2__["TextInput"], {
    className: "text-base border-b-2 border-gray-400",
    type: "text",
    placeholder: "Name",
    prefix: "user",
    errors: errors.name,
    value: emailData.name,
    onChange: function onChange(e) {
      return changeHandler("name", e.target.value);
    }
  }), errors.name && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
    className: "ml-10 text-xs text-red-500"
  }, errors.name[0]), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_base__WEBPACK_IMPORTED_MODULE_2__["TextInput"], {
    className: "text-base border-b-2 border-gray-400",
    type: "text",
    placeholder: "Email",
    prefix: "mail",
    errors: errors.email,
    value: emailData.email,
    onChange: function onChange(e) {
      return changeHandler("email", e.target.value);
    }
  }), errors.email && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
    className: "ml-10 text-xs text-red-500"
  }, errors.email[0]), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_base__WEBPACK_IMPORTED_MODULE_2__["TextInput"], (_React$createElement = {
    className: "text-base border-b-2 border-gray-400",
    type: "text",
    placeholder: "Phone (+61)",
    prefix: "phone",
    errors: errors.phone,
    value: emailData.phone,
    maxLength: "10"
  }, _defineProperty(_React$createElement, "type", "number"), _defineProperty(_React$createElement, "onChange", function onChange(e) {
    return changeHandler("phone", e.target.value);
  }), _React$createElement)), errors.phone && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
    className: "ml-10 text-xs text-red-500"
  }, errors.phone[0]), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_base__WEBPACK_IMPORTED_MODULE_2__["TextArea"], {
    className: "text-base border-2 p-2 h-40 border-gray-400 rounded",
    placeholder: "Message",
    prefix: "message-square",
    errors: errors.message,
    value: emailData.message,
    onChange: function onChange(e) {
      return changeHandler("message", e.target.value);
    }
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "flex justify-end"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_base__WEBPACK_IMPORTED_MODULE_2__["Button"], {
    className: "py-2 mt-4 block mr-2 rounded",
    disabled: loading
  }, loading && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_fortawesome_react_fontawesome__WEBPACK_IMPORTED_MODULE_5__["FontAwesomeIcon"], {
    icon: ["fas", "spinner"],
    className: "fa-spin mr-2"
  }), "Submit"))))));
};

/* harmony default export */ __webpack_exports__["default"] = (BookInspection);

/***/ }),

/***/ "./resources/js/components/BuyingProgress.js":
/*!***************************************************!*\
  !*** ./resources/js/components/BuyingProgress.js ***!
  \***************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react_device_detect__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react-device-detect */ "./node_modules/react-device-detect/main.js");
/* harmony import */ var react_device_detect__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react_device_detect__WEBPACK_IMPORTED_MODULE_1__);



var BuyingProgress = function BuyingProgress(_ref) {
  var _ref$current = _ref.current,
      current = _ref$current === void 0 ? 1 : _ref$current,
      _ref$project_id = _ref.project_id,
      project_id = _ref$project_id === void 0 ? 0 : _ref$project_id,
      isPreview = _ref.isPreview;
  var steps = [{
    name: "Select project",
    step: 1
  }, {
    name: "Select property",
    step: 2
  }, {
    name: "Secure deal",
    step: 3
  }, {
    name: "Agent contact",
    step: 4
  }];

  var handleBackToSearch = function handleBackToSearch() {
    if (current == 3) {
      window.location = "/weekly-deals/".concat(project_id, "/").concat(isPreview);
    }

    if (current == 2) {
      localStorage.setItem("__use_projects_filters", true);
      window.location = "/weekly-deals";
    }
  };

  var renderProgress = function renderProgress() {
    return steps.map(function (step, key) {
      return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("li", {
        key: key,
        className: "\n          ".concat(key == 0 && react_device_detect__WEBPACK_IMPORTED_MODULE_1__["isMobile"] ? "lg:pl-4 pl-2" : "pl-0", "\n            relative pb-2 pt-4 lg:pt-6 w-1/4 lg:w-auto"),
        style: {
          minWidth: react_device_detect__WEBPACK_IMPORTED_MODULE_1__["isMobile"] ? "unset" : 190,
          color: "#68889B",
          borderColor: "#B1ADAD"
        }
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: "flex flex-row items-center justify-center text-xxs"
      }, step.step < current && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: "bg-palette-purple flex h-10 items-center justify-center rounded-full w-10"
      }, step.step > 1 && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: "absolute bg-palette-purple  h-1 items-center justify-center left-0 w-2/5"
      }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
        className: "text-base font-bold text-white"
      }, step.step), step.step < 4 && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: "absolute bg-palette-purple   h-1 items-center justify-center right-0 w-2/5"
      })), step.step >= current && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: "bg-gray-500 flex h-10 items-center justify-center rounded-full w-10"
      }, step.step > 1 && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: "absolute bg-gray-500 h-1 items-center justify-center left-0 w-2/5"
      }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
        className: "text-base font-bold text-white"
      }, step.step), step.step < 4 && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: "absolute bg-gray-500  h-1 items-center justify-center right-0 w-2/5"
      }))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: "font-bold lg:font-normal text-xs lg:text-base text-center ".concat(react_device_detect__WEBPACK_IMPORTED_MODULE_1__["isMobile"] ? "truncate" : "")
      }, step.name));
    });
  };

  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    style: {
      background: "#f5f5f5"
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "mx-auto",
    style: {
      maxWidth: 1366,
      position: "relative"
    }
  }, !react_device_detect__WEBPACK_IMPORTED_MODULE_1__["isMobile"] && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "absolute lg:px-16 mt-6 ".concat(current == 1 || current == 5 ? "hidden" : "")
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
    className: "hover:underline absolute cursor-pointer duration-300 font-bold hover:text-palette-blue inline-flex items-center pr-3 text-sm text-base text-palette-blue-light transition-all z-10",
    onClick: handleBackToSearch
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("svg", {
    className: "feather-icon h-4 w-4 mr-1 inline"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("use", {
    xlinkHref: "/assets/svg/feather-sprite.svg#chevron-left"
  })), "Back"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "absolute border-gray-200 border-t w-full",
    style: {
      top: 10
    }
  })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("ul", {
    className: "mx-auto flex",
    style: {
      width: react_device_detect__WEBPACK_IMPORTED_MODULE_1__["isMobile"] ? "auto" : "fit-content"
    }
  }, renderProgress())));
};

/* harmony default export */ __webpack_exports__["default"] = (BuyingProgress);

/***/ }),

/***/ "./resources/js/components/ContactForm.js":
/*!************************************************!*\
  !*** ./resources/js/components/ContactForm.js ***!
  \************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _base__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./_base */ "./resources/js/components/_base/index.js");
/* harmony import */ var _base__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_base__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _helpers_bootstrap__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../helpers/bootstrap */ "./resources/js/helpers/bootstrap.js");
/* harmony import */ var _fortawesome_react_fontawesome__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @fortawesome/react-fontawesome */ "./node_modules/@fortawesome/react-fontawesome/index.es.js");
/* harmony import */ var _material_ui_core_Snackbar__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @material-ui/core/Snackbar */ "./node_modules/@material-ui/core/esm/Snackbar/index.js");
/* harmony import */ var _material_ui_lab_Alert__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @material-ui/lab/Alert */ "./node_modules/@material-ui/lab/esm/Alert/index.js");
function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }








function Alert(props) {
  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_material_ui_lab_Alert__WEBPACK_IMPORTED_MODULE_5__["default"], _extends({
    elevation: 6,
    variant: "filled"
  }, props));
}

var ContactForm = function ContactForm(_ref) {
  var _ref$backgroundColor = _ref.backgroundColor,
      backgroundColor = _ref$backgroundColor === void 0 ? "#000033" : _ref$backgroundColor;
  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("form", {
    method: "POST",
    action: "https://groupbuyer.activehosted.com/proc.php",
    id: "_form_3_",
    style: {
      background: backgroundColor
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "py-2 pt-6 pb-10 rounded-lg"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("input", {
    type: "hidden",
    name: "u",
    value: "3"
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("input", {
    type: "hidden",
    name: "f",
    value: "3"
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("input", {
    type: "hidden",
    name: "s"
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("input", {
    type: "hidden",
    name: "c",
    value: "0"
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("input", {
    type: "hidden",
    name: "m",
    value: "0"
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("input", {
    type: "hidden",
    name: "act",
    value: "sub"
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("input", {
    type: "hidden",
    name: "v",
    value: "2"
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "_form-content"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "_form_element _x66775207 _full_width "
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("label", {
    className: "_form-label font-bold font-sans"
  }, "Name"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "_field-wrapper"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("input", {
    type: "text",
    name: "fullname",
    placeholder: "Name"
  }))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "_form_element _x28906445 _full_width "
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("label", {
    className: "_form-label font-bold font-sans"
  }, "Email *"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "_field-wrapper"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("input", {
    type: "text",
    pattern: "[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\\.[a-zA-Z0-9-]{2,4})+$",
    name: "email",
    placeholder: "Email",
    required: true
  }))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "_form_element _x30740924 _full_width "
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("label", {
    className: "_form-label font-bold font-sans"
  }, "Phone"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "_field-wrapper"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("input", {
    type: "text",
    name: "phone",
    placeholder: "Phone number"
  }))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "_form_element _field52 _full_width "
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("label", {
    className: "_form-label font-bold font-sans"
  }, "Message *"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "_field-wrapper"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("textarea", {
    name: "field[52]",
    placeholder: "",
    style: {
      height: 102
    },
    required: true
  }))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "_button-wrapper _full_width mt-6"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("button", {
    id: "_form_3_submit",
    className: "_submit w-full",
    type: "submit"
  }, "Submit")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "_clear-element"
  })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "_form-thank-you",
    style: {
      display: "none"
    }
  })));
};

/* harmony default export */ __webpack_exports__["default"] = (ContactForm);

/***/ }),

/***/ "./resources/js/components/Deposit.js":
/*!********************************************!*\
  !*** ./resources/js/components/Deposit.js ***!
  \********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);


var Deposit = function Deposit(_ref) {
  var deposit = _ref.deposit;
  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "flex h-8 items-center justify-center mb-2 mb-6 ml-1 mt-6 px-2 rounded-lg shadow-lg xs:ml-0 mt-2 xs:w-2/3",
    style: {
      backgroundColor: "rgba(104,136,155,0.2)"
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
    className: "text-sm text-center font-bold text-black "
  }, "".concat(deposit, "% deposit required")));
};

/* harmony default export */ __webpack_exports__["default"] = (Deposit);

/***/ }),

/***/ "./resources/js/components/DeveloperPropertyManager.js":
/*!*************************************************************!*\
  !*** ./resources/js/components/DeveloperPropertyManager.js ***!
  \*************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _EditPropertyModal__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./EditPropertyModal */ "./resources/js/components/EditPropertyModal.js");
/* harmony import */ var _AddPropertyModal__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./AddPropertyModal */ "./resources/js/components/AddPropertyModal.js");
/* harmony import */ var _EditLandPropertyModal__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./EditLandPropertyModal */ "./resources/js/components/EditLandPropertyModal.js");
/* harmony import */ var _AddLandProperty__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./AddLandProperty */ "./resources/js/components/AddLandProperty.js");
/* harmony import */ var _states_userGlobal__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../states/userGlobal */ "./resources/js/states/userGlobal.js");
/* harmony import */ var _fortawesome_react_fontawesome__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @fortawesome/react-fontawesome */ "./node_modules/@fortawesome/react-fontawesome/index.es.js");
/* harmony import */ var _fortawesome_free_solid_svg_icons__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @fortawesome/free-solid-svg-icons */ "./node_modules/@fortawesome/free-solid-svg-icons/index.es.js");
/* harmony import */ var _base_alerts_BaseAlert__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./_base/alerts/BaseAlert */ "./resources/js/components/_base/alerts/BaseAlert.js");
/* harmony import */ var _base__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./_base */ "./resources/js/components/_base/index.js");
/* harmony import */ var _base__WEBPACK_IMPORTED_MODULE_9___default = /*#__PURE__*/__webpack_require__.n(_base__WEBPACK_IMPORTED_MODULE_9__);
/* harmony import */ var lodash_isEmpty__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! lodash/isEmpty */ "./node_modules/lodash/isEmpty.js");
/* harmony import */ var lodash_isEmpty__WEBPACK_IMPORTED_MODULE_10___default = /*#__PURE__*/__webpack_require__.n(lodash_isEmpty__WEBPACK_IMPORTED_MODULE_10__);
/* harmony import */ var _helpers_stateAbbreviation__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ../helpers/stateAbbreviation */ "./resources/js/helpers/stateAbbreviation.js");
function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _unsupportedIterableToArray(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

function _iterableToArrayLimit(arr, i) { if (typeof Symbol === "undefined" || !(Symbol.iterator in Object(arr))) return; var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }














var DeveloperPropertyManager = function DeveloperPropertyManager(_ref) {
  var toggleFetch = _ref.toggleFetch,
      toggleCloseModal = _ref.toggleCloseModal,
      toggleAgreementView = _ref.toggleAgreementView;

  var _UserGlobal = Object(_states_userGlobal__WEBPACK_IMPORTED_MODULE_5__["default"])(),
      _UserGlobal2 = _slicedToArray(_UserGlobal, 2),
      userState = _UserGlobal2[0],
      userActions = _UserGlobal2[1];

  var _useState = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])([]),
      _useState2 = _slicedToArray(_useState, 2),
      properties = _useState2[0],
      setProperties = _useState2[1];

  var _useState3 = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])([]),
      _useState4 = _slicedToArray(_useState3, 2),
      property = _useState4[0],
      setProperty = _useState4[1];

  var _useState5 = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])(false),
      _useState6 = _slicedToArray(_useState5, 2),
      toggleEditProperty = _useState6[0],
      setToggleEditProperty = _useState6[1];

  var _useState7 = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])(false),
      _useState8 = _slicedToArray(_useState7, 2),
      toggleAddProperty = _useState8[0],
      setToggleAddProperty = _useState8[1];

  var _useState9 = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])(false),
      _useState10 = _slicedToArray(_useState9, 2),
      toogleLandProperty = _useState10[0],
      setToogleLandProperty = _useState10[1];

  var _useState11 = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])(false),
      _useState12 = _slicedToArray(_useState11, 2),
      toogleEditLandProperty = _useState12[0],
      setToogleEditLandProperty = _useState12[1];

  var _useState13 = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])(0),
      _useState14 = _slicedToArray(_useState13, 2),
      count = _useState14[0],
      setCount = _useState14[1];

  var isProjectLand = userState.projectData.deal_type_id == 4 ? true : false;
  Object(react__WEBPACK_IMPORTED_MODULE_0__["useEffect"])(function () {
    storeProperties();
    userActions.setState({
      propertiesData: userState.projectData.properties
    });
  }, [userState.projectData.properties]);

  var storeProperties = function storeProperties() {
    if (userState.projectData.properties.length == 5) {
      setProperties(userState.projectData.properties);
      setCount();
      return;
    }

    var arr1 = [];

    for (var index = 0; index < userState.projectData.properties.length; index++) {
      var element = userState.projectData.properties[index];
      arr1.push(element);
    }

    var result = [];

    if (arr1.length < 5) {
      var arr2 = [];

      for (var _index = 0; _index < 5 - arr1.length; _index++) {
        arr2.push({});
      }

      result = [].concat(arr1, arr2);
    }

    setProperties(result);
  };

  var nextStep = function nextStep() {
    userActions.setState({
      currentProjStep: 4
    });
    toggleAgreementView();
  };

  var _toggleEditPropertyModal = function toggleEditPropertyModal() {
    if (!isProjectLand) {
      setToggleEditProperty(!toggleEditProperty);
    } else {
      setToogleEditLandProperty(!toogleEditLandProperty);
    }
  };

  var _toggleAddPropertyModal = function toggleAddPropertyModal() {
    if (!isProjectLand) {
      setToggleAddProperty(!toggleAddProperty);
    } else {
      setToogleLandProperty(!toogleLandProperty);
    }
  }; // const toggleAddPropertyModal = () => {
  //   setToggleAP(!toggleAP);
  // };


  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "relative bg-white py-5 my-5 lg:m-5 rounded-lg"
  }, toggleEditProperty && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_EditPropertyModal__WEBPACK_IMPORTED_MODULE_1__["default"], {
    project: userState.projectData,
    projectId: userState.projectData.id,
    property: property,
    toggleEditPropertyModal: function toggleEditPropertyModal() {
      return _toggleEditPropertyModal();
    },
    editToggleFetch: toggleFetch
  }), toggleAddProperty && userState.projectData.properties.length < 5 && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_AddPropertyModal__WEBPACK_IMPORTED_MODULE_2__["default"], {
    project: userState.projectData,
    projectId: userState.projectData.id,
    toggleAddPropertyModal: function toggleAddPropertyModal() {
      return _toggleAddPropertyModal();
    },
    addToggleFetch: toggleFetch
  }), toogleEditLandProperty && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_EditLandPropertyModal__WEBPACK_IMPORTED_MODULE_3__["default"], {
    projectName: userState.projectData.name,
    projectId: userState.projectData.id,
    property: property,
    toggleEditPropertyModal: function toggleEditPropertyModal() {
      return _toggleEditPropertyModal();
    },
    editToggleFetch: toggleFetch
  }), toogleLandProperty && userState.projectData.properties.length < 5 && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_AddLandProperty__WEBPACK_IMPORTED_MODULE_4__["default"], {
    projectName: userState.projectData.name,
    projectId: userState.projectData.id,
    toggleAddPropertyModal: function toggleAddPropertyModal() {
      return _toggleAddPropertyModal();
    },
    addToggleFetch: toggleFetch
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "flex flex-wrap justify-around"
  }, properties.map(function (property, key) {
    if (!lodash_isEmpty__WEBPACK_IMPORTED_MODULE_10___default()(property)) {
      return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(PropertyDetails, {
        key: key,
        pId: key,
        property: property,
        projectName: userState.projectData.deal_type,
        setProperty: setProperty,
        toggleEditPropertyModal: _toggleEditPropertyModal,
        toggleAddPropertyModal: _toggleAddPropertyModal,
        isLand: userState.projectData.sub_property_id == 4 ? true : false
      });
    } else {
      return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(EmptyProperties, {
        pId: key,
        key: key,
        toggleAddPropertyModal: _toggleAddPropertyModal
      });
    }
  })), userState.projectData.properties.length == 5 && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "flex flex-1 px-3"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_base_alerts_BaseAlert__WEBPACK_IMPORTED_MODULE_8__["default"], {
    message: "You have reached the maximum 5 properties per project. Once all 5 properties are sold you can upload another 5 properties.",
    icon: "check",
    type: "success",
    iconHeight: "h-5",
    iconWidth: "w-6",
    className: "w-full"
  })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "flex flex-1 items-center justify-end px-3 mt-2"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_base__WEBPACK_IMPORTED_MODULE_9__["Button"], {
    className: "button button-primary font-bold h-10 ml-10 mt-1 rounded rounded-full bg-blue-500",
    onClick: function onClick() {
      return nextStep();
    },
    disabled: userState.projectData.properties.length < 5
  }, "Next Step"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_base__WEBPACK_IMPORTED_MODULE_9__["Button"], {
    className: "button button-primary font-bold h-10 ml-5 mt-1 rounded rounded-full",
    onClick: function onClick() {
      return toggleCloseModal();
    }
  }, "Save & Close")));
};

var EmptyProperties = function EmptyProperties(_ref2) {
  var pId = _ref2.pId,
      toggleAddPropertyModal = _ref2.toggleAddPropertyModal;

  var addProperty = function addProperty(e) {
    e.preventDefault();
    toggleAddPropertyModal();
  };

  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    key: pId,
    className: "mb-4 px-3  w-full"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "block bg-white rounded-md p-0",
    style: {
      boxShadow: "0 5px 28px 4px rgba(0,0,0,0.1)"
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    onClick: function onClick(e) {
      return addProperty(e);
    },
    className: "cursor-pointer border border-gray-400 flex h-48 items-center justify-center relative rounded"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
    className: "text-2xl text-gray-600"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_fortawesome_react_fontawesome__WEBPACK_IMPORTED_MODULE_6__["FontAwesomeIcon"], {
    icon: _fortawesome_free_solid_svg_icons__WEBPACK_IMPORTED_MODULE_7__["faPlus"],
    className: "fa-plus mr-2"
  }), "Add Property"))));
};

var parseAddress = function parseAddress(strAddress) {
  if (typeof strAddress === "string") {
    return JSON.parse(strAddress);
  } else {
    return strAddress;
  }
};

var PropertyDetails = function PropertyDetails(_ref3) {
  var pId = _ref3.pId,
      property = _ref3.property,
      projectName = _ref3.projectName,
      setProperty = _ref3.setProperty,
      toggleEditPropertyModal = _ref3.toggleEditPropertyModal,
      isLand = _ref3.isLand;
  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    key: pId,
    className: "mb-4 px-3  w-full"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "block bg-white rounded-md p-0",
    style: {
      boxShadow: "0 5px 28px 4px rgba(0,0,0,0.1)"
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "flex items-center relative"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
    src: property.featured_images[0],
    className: "w-1/3 h-48 p-2 pr-4"
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "flex flex-1 flex-col relative"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "lg:text-2xl text-base mb-2 font-bold w-56 leading-none"
  }, projectName, " ", property.unit_name), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "lg:mb-2 mr-4 flex items-center text-base"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("svg", {
    className: "feather-icon text-palette-purple mr-1"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("use", {
    xlinkHref: "/assets/svg/feather-sprite.svg#map-pin"
  })), parseAddress(property.address).suburb + ", " + parseAddress(property.address).state), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "mr-4 flex items-center"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "text-base  ",
    dangerouslySetInnerHTML: {
      __html: Object(_helpers_stateAbbreviation__WEBPACK_IMPORTED_MODULE_11__["shortIntro"])(property.description)
    }
  })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "flex flex-1 justify-start"
  }, !isLand && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react__WEBPACK_IMPORTED_MODULE_0___default.a.Fragment, null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
    className: "mr-3 text-base"
  }, property.no_of_bedrooms, " ", /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_fortawesome_react_fontawesome__WEBPACK_IMPORTED_MODULE_6__["FontAwesomeIcon"], {
    icon: ["fas", "bed"]
  })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
    className: "mr-3 text-base"
  }, property.no_of_bathrooms, " ", /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_fortawesome_react_fontawesome__WEBPACK_IMPORTED_MODULE_6__["FontAwesomeIcon"], {
    icon: ["fas", "bath"]
  })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
    className: "text-base"
  }, property.no_of_garages, " ", /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_fortawesome_react_fontawesome__WEBPACK_IMPORTED_MODULE_6__["FontAwesomeIcon"], {
    icon: ["fas", "car"]
  }))), isLand && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react__WEBPACK_IMPORTED_MODULE_0___default.a.Fragment, null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
    className: "mr-3 text-base"
  }, "Size: ", /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
    className: 'font-bold'
  }, property.size)), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
    className: "mr-3 text-base"
  }, "Frontage: ", /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
    className: 'font-bold'
  }, property.frontage)), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
    className: "mr-3 text-base"
  }, "Width: ", /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
    className: 'font-bold'
  }, property.width)), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
    className: "mr-3 text-base"
  }, "Depth: ", /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
    className: 'font-bold'
  }, " ", property.depth))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "flex flex-1 justify-end px-3"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("button", {
    className: "text-base mr-4",
    onClick: function onClick() {
      setProperty(property);
      toggleEditPropertyModal();
    }
  }, "Edit\xA0", /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_fortawesome_react_fontawesome__WEBPACK_IMPORTED_MODULE_6__["FontAwesomeIcon"], {
    icon: ["fas", "edit"]
  }))))))));
};

/* harmony default export */ __webpack_exports__["default"] = (DeveloperPropertyManager);

/***/ }),

/***/ "./resources/js/components/EditLandPropertyModal.js":
/*!**********************************************************!*\
  !*** ./resources/js/components/EditLandPropertyModal.js ***!
  \**********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime/regenerator */ "./node_modules/@babel/runtime/regenerator/index.js");
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var react_dropdown__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! react-dropdown */ "./node_modules/react-dropdown/dist/index.js");
/* harmony import */ var react_dropdown__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(react_dropdown__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _helpers_styles_dropdown_css__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../helpers/styles/dropdown.css */ "./resources/js/helpers/styles/dropdown.css");
/* harmony import */ var _helpers_styles_dropdown_css__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(_helpers_styles_dropdown_css__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var _base__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./_base */ "./resources/js/components/_base/index.js");
/* harmony import */ var _base__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(_base__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var draft_js__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! draft-js */ "./node_modules/draft-js/lib/Draft.js");
/* harmony import */ var draft_js__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(draft_js__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var _helpers_styles_datepicker_css__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../helpers/styles/datepicker.css */ "./resources/js/helpers/styles/datepicker.css");
/* harmony import */ var _helpers_styles_datepicker_css__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(_helpers_styles_datepicker_css__WEBPACK_IMPORTED_MODULE_6__);
/* harmony import */ var draft_convert__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! draft-convert */ "./node_modules/draft-convert/esm/index.js");
/* harmony import */ var react_currency_format__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! react-currency-format */ "./node_modules/react-currency-format/lib/currency-format.js");
/* harmony import */ var react_currency_format__WEBPACK_IMPORTED_MODULE_8___default = /*#__PURE__*/__webpack_require__.n(react_currency_format__WEBPACK_IMPORTED_MODULE_8__);
/* harmony import */ var _fortawesome_react_fontawesome__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @fortawesome/react-fontawesome */ "./node_modules/@fortawesome/react-fontawesome/index.es.js");
/* harmony import */ var _fortawesome_free_solid_svg_icons__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @fortawesome/free-solid-svg-icons */ "./node_modules/@fortawesome/free-solid-svg-icons/index.es.js");
/* harmony import */ var _states_userGlobal__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ../states/userGlobal */ "./resources/js/states/userGlobal.js");
/* harmony import */ var lodash_isEmpty__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! lodash/isEmpty */ "./node_modules/lodash/isEmpty.js");
/* harmony import */ var lodash_isEmpty__WEBPACK_IMPORTED_MODULE_12___default = /*#__PURE__*/__webpack_require__.n(lodash_isEmpty__WEBPACK_IMPORTED_MODULE_12__);
/* harmony import */ var _components_base_alerts_sweetAlert__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ../components/_base/alerts/sweetAlert */ "./resources/js/components/_base/alerts/sweetAlert.js");
/* harmony import */ var _helpers_propertyHelper_propertyHelper__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ../helpers/propertyHelper/propertyHelper */ "./resources/js/helpers/propertyHelper/propertyHelper.js");
/* harmony import */ var _data_index__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! ../data/index */ "./resources/js/data/index.js");
/* harmony import */ var _helpers_fileHelper_fileHelper__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! ../helpers/fileHelper/fileHelper */ "./resources/js/helpers/fileHelper/fileHelper.js");
/* harmony import */ var _helpers_resourceHelper_index__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! ../helpers/resourceHelper/index */ "./resources/js/helpers/resourceHelper/index.js");


function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

function _toConsumableArray(arr) { return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _unsupportedIterableToArray(arr) || _nonIterableSpread(); }

function _nonIterableSpread() { throw new TypeError("Invalid attempt to spread non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _iterableToArray(iter) { if (typeof Symbol !== "undefined" && Symbol.iterator in Object(iter)) return Array.from(iter); }

function _arrayWithoutHoles(arr) { if (Array.isArray(arr)) return _arrayLikeToArray(arr); }

function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _unsupportedIterableToArray(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

function _iterableToArrayLimit(arr, i) { if (typeof Symbol === "undefined" || !(Symbol.iterator in Object(arr))) return; var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }



















/* harmony default export */ __webpack_exports__["default"] = (function (_ref) {
  var projectName = _ref.projectName,
      projectId = _ref.projectId,
      property = _ref.property,
      toggleEditPropertyModal = _ref.toggleEditPropertyModal,
      editToggleFetch = _ref.editToggleFetch;

  var _UserGlobal = Object(_states_userGlobal__WEBPACK_IMPORTED_MODULE_11__["default"])(),
      _UserGlobal2 = _slicedToArray(_UserGlobal, 2),
      userState = _UserGlobal2[0],
      userActions = _UserGlobal2[1];

  var _useState = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(_toConsumableArray(property.featured_images.map(function (image) {
    return {
      image: image,
      preview: image
    };
  }))),
      _useState2 = _slicedToArray(_useState, 2),
      featuredImages = _useState2[0],
      setFeaturedImages = _useState2[1];

  var previewImageRef = Object(react__WEBPACK_IMPORTED_MODULE_1__["useRef"])(null);
  var floorPlanRef = Object(react__WEBPACK_IMPORTED_MODULE_1__["useRef"])(null);

  var _useState3 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(""),
      _useState4 = _slicedToArray(_useState3, 2),
      state = _useState4[0],
      setState = _useState4[1];

  var _useState5 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(String(property.property_type_id)),
      _useState6 = _slicedToArray(_useState5, 2),
      propertyType = _useState6[0],
      setPropertyType = _useState6[1];

  var _useState7 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(false),
      _useState8 = _slicedToArray(_useState7, 2),
      loading = _useState8[0],
      setLoading = _useState8[1];

  var _useState9 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(""),
      _useState10 = _slicedToArray(_useState9, 2),
      subPropertyType = _useState10[0],
      setSubPropertyType = _useState10[1];

  var _useState11 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(""),
      _useState12 = _slicedToArray(_useState11, 2),
      ownershipType = _useState12[0],
      setOwnershipType = _useState12[1];

  var _useState13 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])([]),
      _useState14 = _slicedToArray(_useState13, 2),
      errors = _useState14[0],
      setErrors = _useState14[1];

  var _useState15 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(function () {
    return draft_js__WEBPACK_IMPORTED_MODULE_5__["EditorState"].createEmpty();
  }),
      _useState16 = _slicedToArray(_useState15, 2),
      editorState = _useState16[0],
      setEditorState = _useState16[1];

  var _useState17 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])({}),
      _useState18 = _slicedToArray(_useState17, 2),
      floorPlan = _useState18[0],
      setFLoorPlan = _useState18[1];

  var _useState19 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(""),
      _useState20 = _slicedToArray(_useState19, 2),
      filename = _useState20[0],
      setFilename = _useState20[1];

  var _useState21 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(""),
      _useState22 = _slicedToArray(_useState21, 2),
      prevFilename = _useState22[0],
      setPrevFilename = _useState22[1];

  var _useState23 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])([]),
      _useState24 = _slicedToArray(_useState23, 2),
      previewImage = _useState24[0],
      setPreviewImage = _useState24[1];

  var _useState25 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(""),
      _useState26 = _slicedToArray(_useState25, 2),
      propertyPrice = _useState26[0],
      setPropertyPrice = _useState26[1];

  var _useState27 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(false),
      _useState28 = _slicedToArray(_useState27, 2),
      limitError = _useState28[0],
      setLimitError = _useState28[1];

  var _useState29 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])([]),
      _useState30 = _slicedToArray(_useState29, 2),
      fmPlaceHolder = _useState30[0],
      setFmPlaceHolder = _useState30[1];

  var _useState31 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(_helpers_propertyHelper_propertyHelper__WEBPACK_IMPORTED_MODULE_14__["costDueDates"]),
      _useState32 = _slicedToArray(_useState31, 2),
      dates = _useState32[0],
      setDates = _useState32[1];

  var addPropertyForm = Object(react__WEBPACK_IMPORTED_MODULE_1__["useRef"])(null);
  Object(react__WEBPACK_IMPORTED_MODULE_1__["useEffect"])(function () {
    var phs = [];

    for (var index = 0; index < 6 - featuredImages.length; index++) {
      var ph = {
        classes: "cursor-pointer m-2 inline-flex bg-transparent text-palette-gray"
      };
      phs.push(ph);
    }

    setFmPlaceHolder(phs);
  }, [featuredImages.length]);
  Object(react__WEBPACK_IMPORTED_MODULE_1__["useEffect"])(function () {
    setEditorState(function () {
      return draft_js__WEBPACK_IMPORTED_MODULE_5__["EditorState"].push(editorState, Object(draft_convert__WEBPACK_IMPORTED_MODULE_7__["convertFromHTML"])(property.description));
    });
  }, []);
  Object(react__WEBPACK_IMPORTED_MODULE_1__["useEffect"])(function () {
    if (property.rich_preview_image) {
      var img = [];
      img.push({
        image: property.rich_preview_image,
        preview: property.rich_preview_image
      });
      var newFilename = property.rich_preview_image.split("/imgPreview/")[1];
      setPrevFilename(newFilename);
      setPreviewImage(img);
    }

    if (property.price) {
      setPropertyPrice(property.price);
    }
  }, []);

  var handleSubmit = function handleSubmit(e) {
    e.preventDefault();
    setLimitError(false);
    saveProperty();
  };

  var saveProperty = /*#__PURE__*/function () {
    var _ref2 = _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee() {
      var formData, url;
      return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee$(_context) {
        while (1) {
          switch (_context.prev = _context.next) {
            case 0:
              setLoading(true);
              formData = new FormData(addPropertyForm.current);
              formData.append("role", "project_developer");

              if (floorPlan && !lodash_isEmpty__WEBPACK_IMPORTED_MODULE_12___default()(floorPlan)) {
                formData.append("floorPlan[file]", floorPlan.file);
                formData.append("floorPlan[name]", "floorplan");
              }

              formData.append("projectId", projectId);
              formData.append("propertyType", propertyType);
              formData.append("description", editorState.getCurrentContent().getPlainText());
              formData.append("descriptionHTML", Object(draft_convert__WEBPACK_IMPORTED_MODULE_7__["convertToHTML"])(editorState.getCurrentContent()));
              featuredImages.forEach(function (image, key) {
                formData.append("featuredImages[".concat(key, "]"), image.image);
              });

              if (previewImage.length) {
                formData.append("preview_img[".concat(0, "]"), previewImage[0].image);
              } else {
                formData.append("preview_img[".concat(0, "]"), "");
              }

              formData.append("_method", "PATCH");
              url = "/api/property/".concat(property.id);
              _context.next = 14;
              return Object(_data_index__WEBPACK_IMPORTED_MODULE_15__["saveUpdateProperty"])({
                formData: formData,
                url: url
              }).then(function (retVal) {
                if (retVal.status === 200) {
                  var project = userState.projectData;
                  project.properties = retVal.data;
                  userActions.setState({
                    projectData: project
                  });
                  setLoading(false);
                  Object(_components_base_alerts_sweetAlert__WEBPACK_IMPORTED_MODULE_13__["sweetAlert"])("success", "Property successfully updated.");
                  editToggleFetch();
                  toggleEditPropertyModal();
                }
              })["catch"](function (error) {
                setErrors(error.response.data.errors);
                setLoading(false);
              }).then(function () {});

            case 14:
            case "end":
              return _context.stop();
          }
        }
      }, _callee);
    }));

    return function saveProperty() {
      return _ref2.apply(this, arguments);
    };
  }();

  var handlePreviewImageDisplay = function handlePreviewImageDisplay(e) {
    e.preventDefault();

    if (e.target.files.length > 1) {
      Object(_components_base_alerts_sweetAlert__WEBPACK_IMPORTED_MODULE_13__["sweetAlert"])("error", "Multiple upload is not allowed!");
      return;
    }

    if (!Object(_helpers_fileHelper_fileHelper__WEBPACK_IMPORTED_MODULE_16__["imageFileTypeFilter"])(e.target.files[0]["name"])) {
      Object(_components_base_alerts_sweetAlert__WEBPACK_IMPORTED_MODULE_13__["sweetAlert"])("error", "Filetype is not supported!");
      e.target.value = null;
      return;
    }

    var imgArr = [];
    setPreviewImage(imgArr.concat(_toConsumableArray(_toConsumableArray(e.target.files).map(function (file) {
      var newFilename = Object(_helpers_fileHelper_fileHelper__WEBPACK_IMPORTED_MODULE_16__["sanitizeFilename"])(file.name);
      var newFileObj = new File([file], newFilename);
      setPrevFilename(newFilename);
      return {
        image: newFileObj,
        preview: URL.createObjectURL(newFileObj)
      };
    }))));
  };

  var handleDrop = function handleDrop(e) {
    e.preventDefault();
    e.stopPropagation();
    var fmCount = featuredImages.length;
    var onCount = e.dataTransfer.files.length;

    if (fmCount + onCount > 6) {
      e.target.value = null;
      setLimitError(true);
      return;
    } else {
      setLimitError(false);
    }

    setFeaturedImages(featuredImages.concat(_toConsumableArray(_toConsumableArray(e.dataTransfer.files).map(function (file) {
      var newFilename = Object(_helpers_fileHelper_fileHelper__WEBPACK_IMPORTED_MODULE_16__["sanitizeFilename"])(file.name);
      var newFileObj = new File([file], newFilename);
      return {
        image: newFileObj,
        preview: URL.createObjectURL(newFileObj)
      };
    }))));
    e.target.value = null;
  };

  var handleChange = function handleChange(e) {
    var fmCount = featuredImages.length;
    var onCount = e.target.files.length;

    if (fmCount + onCount > 6) {
      setLimitError(true);
      e.target.value = null;
      return;
    } else {
      setLimitError(false);
    }

    setFeaturedImages(featuredImages.concat(_toConsumableArray(_toConsumableArray(e.target.files).map(function (file) {
      var newFilename = Object(_helpers_fileHelper_fileHelper__WEBPACK_IMPORTED_MODULE_16__["sanitizeFilename"])(file.name);
      var newFileObj = new File([file], newFilename);
      return {
        image: newFileObj,
        preview: URL.createObjectURL(newFileObj)
      };
    }))));
    e.target.value = null;
  };

  var handleRemove = function handleRemove(e, i) {
    e.preventDefault();
    e.stopPropagation();
    setFeaturedImages(_toConsumableArray(featuredImages.filter(function (image, k) {
      if (i !== k) {
        return image;
      }
    })));

    if (featuredImages.length > 6) {
      setLimitError(true);
    } else {
      setLimitError(false);
    }
  };

  var handleFloorPlanChange = function handleFloorPlanChange(e) {
    e.preventDefault();

    if (!Object(_helpers_fileHelper_fileHelper__WEBPACK_IMPORTED_MODULE_16__["PDFFileTypeFilter"])(e.target.files[0]["name"])) {
      Object(_components_base_alerts_sweetAlert__WEBPACK_IMPORTED_MODULE_13__["sweetAlert"])("error", "Filetype is not supported!");
      e.target.value = null;
      return;
    }

    setFLoorPlan({
      file: _helpers_resourceHelper_index__WEBPACK_IMPORTED_MODULE_17__["resources"].trimFloorPlan(e.target.files[0]),
      name: "floor_plan"
    });
    var newFilename = Object(_helpers_fileHelper_fileHelper__WEBPACK_IMPORTED_MODULE_16__["sanitizeFilename"])(e.target.files[0]["name"]);
    setFilename(newFilename); // e.target.value = null;
  };

  var handleDeleteFile = function handleDeleteFile(e) {
    e.preventDefault();
    setFLoorPlan({});
    floorPlanRef.current.type = "";
    floorPlanRef.current.type = "file";
    setFilename("");
    e.target.value = null;
  };

  var handleDeletePrevFile = function handleDeletePrevFile(e) {
    e.preventDefault();
    setPreviewImage([]);
    previewImageRef.current.type = "";
    previewImageRef.current.type = "file";
    setPrevFilename("");
    e.target.value = null;
  };

  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_base__WEBPACK_IMPORTED_MODULE_4__["Modal"], {
    show: true,
    maxWidth: "md",
    title: "Edit Land Property",
    onClose: function onClose() {
      return toggleEditPropertyModal();
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("form", {
    ref: addPropertyForm,
    className: "bg-white rounded-lg py-5 px-8 m-5",
    onSubmit: handleSubmit
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "text-black "
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "py-3 flex justify-start "
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "flex justify-center items-center"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("label", {
    className: "font-semibold w-24"
  }, "Property Type"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(react_dropdown__WEBPACK_IMPORTED_MODULE_2___default.a, {
    id: "property_type",
    value: propertyType,
    options: _helpers_propertyHelper_propertyHelper__WEBPACK_IMPORTED_MODULE_14__["property_types"],
    onChange: function onChange(option) {
      setPropertyType(option.value);
    },
    placeholder: "Please select...",
    name: "property_type",
    className: "w-48 ml-4",
    disabled: true
  }), errors["propertyType"] && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
    className: "px-1 text-red-400 tracking-widest  text-xs "
  }, errors["propertyType"][0]))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "ml-10 flex-1 flex justify-end items-center"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("label", {
    className: "font-semibold w-32"
  }, "Lot Number"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_base__WEBPACK_IMPORTED_MODULE_4__["TextInput"], {
    defaultValue: property.unit_name,
    className: "border-gray-400 border pl-2 py-1",
    border: false,
    name: "unit_name",
    width: "w-24 ml-4"
  }), errors["unit_name"] && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
    className: "px-1 text-red-400 tracking-widest  text-xs "
  }, errors["unit_name"][0]))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "ml-4 flex-1 flex justify-end items-center"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("label", {
    className: "font-semibold w-12"
  }, "Size"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_base__WEBPACK_IMPORTED_MODULE_4__["TextInput"], {
    className: "border-gray-400 border pl-2 py-1",
    border: false,
    name: "size",
    defaultValue: property.size,
    width: "w-24"
  }), errors["size"] && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
    className: "px-1 text-red-400 tracking-widest  text-xs "
  }, errors["size"][0])))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "flex justify-start my-3"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "flex items-center justify-start py-3"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("label", {
    className: "w-12"
  }, "Price"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "relative"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(react_currency_format__WEBPACK_IMPORTED_MODULE_8___default.a, {
    allowNegative: false,
    className: "border text-sm border-gray-400 px-2 py-1 w-48 ".concat(property.is_property_approved ? "bg-gray-300 cursor-not-allowed" : ""),
    thousandSeparator: true,
    value: propertyPrice,
    onValueChange: function onValueChange(e) {
      setPropertyPrice(e.value);
    },
    name: "price",
    id: "price",
    prefix: "$",
    readOnly: property.is_property_approved
  })), errors["price"] && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
    className: "absolute px-1 text-red-400 tracking-widest text-xs "
  }, errors["price"][0]))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "py-3 flex justify-start items-center ml-10"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("label", {
    className: ""
  }, "Frontage"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "relative"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "ml-4 w-24"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_base__WEBPACK_IMPORTED_MODULE_4__["TextInput"], {
    className: "pl-2 text-sm border-gray-400 w-32 border py-1",
    border: false // type={`number`}
    ,
    name: "frontage",
    defaultValue: property.frontage
  })), errors["frontage"] && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
    className: "absolute px-1 text-red-400 tracking-widest  text-xs "
  }, errors["frontage"][0]))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "py-3 flex justify-start items-center ml-8"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("label", {
    className: ""
  }, "Width"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "relative"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "ml-4 w-24"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_base__WEBPACK_IMPORTED_MODULE_4__["TextInput"], {
    className: "pl-2 text-sm border-gray-400 w-32 border py-1",
    border: false // type={`number`}
    ,
    name: "width",
    defaultValue: property.width
  })), errors["width"] && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
    className: "absolute px-1 text-red-400 tracking-widest  text-xs "
  }, errors["width"][0]))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "py-3 flex justify-start items-center ml-8"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("label", {
    className: ""
  }, "Depth "), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "relative"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "ml-4 w-24"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_base__WEBPACK_IMPORTED_MODULE_4__["TextInput"], {
    className: "pl-2 text-sm border-gray-400 w-32 border py-1 mr-1",
    border: false // type={`number`}
    ,
    name: "depth",
    defaultValue: property.depth
  })), errors["depth"] && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
    className: "absolute px-1 text-red-400 tracking-widest  text-xs "
  }, errors["depth"][0])))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "py-3"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "w-full text-sm bg-white text-black "
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_base__WEBPACK_IMPORTED_MODULE_4__["RichTextEditor"], {
    key: property.id,
    title: "Property Description",
    richEditorState: editorState,
    richEditorStateContent: function richEditorStateContent(content) {
      return setEditorState(content);
    },
    characterLimit: 2000
  })), errors["description"] && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
    className: "px-1 text-red-400 tracking-widest  text-xs "
  }, errors["description"][0])), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "py-3"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("h1", {
    className: "font-semibold"
  }, " Featured Images (min. 3 images) "), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("label", {
    className: "button ",
    htmlFor: "upload-photos"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: ""
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "w-full flex flex-wrap  cursor-pointer",
    style: {
      border: "2px dashed #ccc",
      borderRadius: "20px",
      padding: "20px",
      textAlign: "center",
      marginBottom: "0px"
    },
    onDrop: function onDrop(e) {
      return handleDrop(e);
    },
    onDragOver: function onDragOver(e) {
      e.preventDefault();
      e.stopPropagation();
    },
    onDragEnter: function onDragEnter(e) {
      e.preventDefault();
      e.stopPropagation();
    },
    onDragLeave: function onDragLeave(e) {
      e.preventDefault();
      e.stopPropagation();
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("input", {
    type: "file",
    id: "upload-photos",
    accept: "image/*",
    multiple: true,
    className: "hidden",
    name: "image[]",
    onChange: function onChange(e) {
      return handleChange(e);
    }
  }), featuredImages.map(function (image, index) {
    return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
      key: index,
      className: "rounded-sm border m-2 relative",
      style: {
        backgroundImage: "url('".concat(image.preview, "')"),
        backgroundPositionX: "center",
        width: "6.9em",
        height: "8.9rem",
        backgroundSize: "cover"
      }
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_base__WEBPACK_IMPORTED_MODULE_4__["Tooltip"], {
      title: "Remove this item"
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
      onClick: function onClick(e) {
        return handleRemove(e, index);
      },
      className: "absolute bottom-0 right-0 p-2 -mb-2 "
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_fortawesome_react_fontawesome__WEBPACK_IMPORTED_MODULE_9__["FontAwesomeIcon"], {
      className: "text-red-500 shadow-xl hover:font-bold hover:text-red-700",
      icon: _fortawesome_free_solid_svg_icons__WEBPACK_IMPORTED_MODULE_10__["faTimes"]
    }))));
  }), fmPlaceHolder.map(function (p, i) {
    return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
      key: i,
      className: "".concat(p.classes, " cursor-pointer flex justify-center items-center"),
      style: {
        width: "6.9rem",
        height: "8.9rem",
        border: "2px solid #ccc"
      }
    }, " ", /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_fortawesome_react_fontawesome__WEBPACK_IMPORTED_MODULE_9__["FontAwesomeIcon"], {
      className: "text-xl",
      icon: _fortawesome_free_solid_svg_icons__WEBPACK_IMPORTED_MODULE_10__["faCamera"]
    }), " ");
  })), limitError && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
    className: "px-1 text-red-400 tracking-widest  text-xs "
  }, "Please limit your photos to six items only."))), errors["featuredImages"] && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
    className: "px-1 text-red-400 tracking-widest  text-xs "
  }, errors["featuredImages"][0])), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "flex flex-row"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "w-1/2 ml-2"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("h1", {
    className: "font-semibold"
  }, "Rich Text Preview "), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "flex flex-col justify-between py-3"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "flex"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("label", {
    className: "w-full"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "items-center cursor-pointer w-full p-3 rounded-lg flex",
    style: {
      border: "2px dashed #ccc",
      borderRadius: "20px"
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("input", {
    id: "rtp",
    ref: previewImageRef,
    type: "file",
    name: "prev_image",
    onChange: function onChange(e) {
      return handlePreviewImageDisplay(e);
    },
    className: "hidden border text-sm border-gray-400 px-2 py-1 w-full py-1"
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("label", {
    htmlFor: "rtp",
    className: " cursor-pointer hover:text-palette-blue-light ".concat(prevFilename ? "text-palette-blue-light" : "")
  }, prevFilename || "Select File"), prevFilename && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "text-red-500 text-lg cursor-pointer ml-3"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_fortawesome_react_fontawesome__WEBPACK_IMPORTED_MODULE_9__["FontAwesomeIcon"], {
    onClick: function onClick(e) {
      return handleDeletePrevFile(e);
    },
    icon: _fortawesome_free_solid_svg_icons__WEBPACK_IMPORTED_MODULE_10__["faTimes"]
  })))))))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "flex justify-end my-5"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_base__WEBPACK_IMPORTED_MODULE_4__["Button"], {
    className: "font-bold rounded-full",
    disabled: loading
  }, loading && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_fortawesome_react_fontawesome__WEBPACK_IMPORTED_MODULE_9__["FontAwesomeIcon"], {
    icon: _fortawesome_free_solid_svg_icons__WEBPACK_IMPORTED_MODULE_10__["faSpinner"],
    className: "fa-spin mr-2"
  }), "Update")))));
});

/***/ }),

/***/ "./resources/js/components/EditProjectPart.js":
/*!****************************************************!*\
  !*** ./resources/js/components/EditProjectPart.js ***!
  \****************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime/regenerator */ "./node_modules/@babel/runtime/regenerator/index.js");
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var react_dropdown__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! react-dropdown */ "./node_modules/react-dropdown/dist/index.js");
/* harmony import */ var react_dropdown__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(react_dropdown__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _helpers_styles_dropdown_css__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../helpers/styles/dropdown.css */ "./resources/js/helpers/styles/dropdown.css");
/* harmony import */ var _helpers_styles_dropdown_css__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(_helpers_styles_dropdown_css__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! moment */ "./node_modules/moment/moment.js");
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(moment__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var _components_base_alerts_sweetAlert__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../components/_base/alerts/sweetAlert */ "./resources/js/components/_base/alerts/sweetAlert.js");
/* harmony import */ var react_device_detect__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! react-device-detect */ "./node_modules/react-device-detect/main.js");
/* harmony import */ var react_device_detect__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(react_device_detect__WEBPACK_IMPORTED_MODULE_6__);
/* harmony import */ var _helpers_fileHelper_fileHelper__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../helpers/fileHelper/fileHelper */ "./resources/js/helpers/fileHelper/fileHelper.js");
/* harmony import */ var _services_auth__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../services/auth */ "./resources/js/services/auth.js");
/* harmony import */ var _base__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./_base */ "./resources/js/components/_base/index.js");
/* harmony import */ var _base__WEBPACK_IMPORTED_MODULE_9___default = /*#__PURE__*/__webpack_require__.n(_base__WEBPACK_IMPORTED_MODULE_9__);
/* harmony import */ var draft_js__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! draft-js */ "./node_modules/draft-js/lib/Draft.js");
/* harmony import */ var draft_js__WEBPACK_IMPORTED_MODULE_10___default = /*#__PURE__*/__webpack_require__.n(draft_js__WEBPACK_IMPORTED_MODULE_10__);
/* harmony import */ var react_datepicker__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! react-datepicker */ "./node_modules/react-datepicker/dist/react-datepicker.min.js");
/* harmony import */ var react_datepicker__WEBPACK_IMPORTED_MODULE_11___default = /*#__PURE__*/__webpack_require__.n(react_datepicker__WEBPACK_IMPORTED_MODULE_11__);
/* harmony import */ var _helpers_styles_datepicker_css__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ../helpers/styles/datepicker.css */ "./resources/js/helpers/styles/datepicker.css");
/* harmony import */ var _helpers_styles_datepicker_css__WEBPACK_IMPORTED_MODULE_12___default = /*#__PURE__*/__webpack_require__.n(_helpers_styles_datepicker_css__WEBPACK_IMPORTED_MODULE_12__);
/* harmony import */ var draft_convert__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! draft-convert */ "./node_modules/draft-convert/esm/index.js");
/* harmony import */ var _helpers_propertyHelper_propertyHelper__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ../helpers/propertyHelper/propertyHelper */ "./resources/js/helpers/propertyHelper/propertyHelper.js");
/* harmony import */ var _fortawesome_react_fontawesome__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! @fortawesome/react-fontawesome */ "./node_modules/@fortawesome/react-fontawesome/index.es.js");
/* harmony import */ var _fortawesome_free_solid_svg_icons__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! @fortawesome/free-solid-svg-icons */ "./node_modules/@fortawesome/free-solid-svg-icons/index.es.js");
/* harmony import */ var _fortawesome_free_regular_svg_icons__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! @fortawesome/free-regular-svg-icons */ "./node_modules/@fortawesome/free-regular-svg-icons/index.es.js");
/* harmony import */ var _states_userGlobal__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(/*! ../states/userGlobal */ "./resources/js/states/userGlobal.js");
/* harmony import */ var _data_index__WEBPACK_IMPORTED_MODULE_19__ = __webpack_require__(/*! ../data/index */ "./resources/js/data/index.js");
/* harmony import */ var lodash_isEmpty__WEBPACK_IMPORTED_MODULE_20__ = __webpack_require__(/*! lodash/isEmpty */ "./node_modules/lodash/isEmpty.js");
/* harmony import */ var lodash_isEmpty__WEBPACK_IMPORTED_MODULE_20___default = /*#__PURE__*/__webpack_require__.n(lodash_isEmpty__WEBPACK_IMPORTED_MODULE_20__);
/* harmony import */ var _helpers_mapCoordinateHelper__WEBPACK_IMPORTED_MODULE_21__ = __webpack_require__(/*! ../helpers/mapCoordinateHelper */ "./resources/js/helpers/mapCoordinateHelper.js");


function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

function _toConsumableArray(arr) { return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _unsupportedIterableToArray(arr) || _nonIterableSpread(); }

function _nonIterableSpread() { throw new TypeError("Invalid attempt to spread non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _iterableToArray(iter) { if (typeof Symbol !== "undefined" && Symbol.iterator in Object(iter)) return Array.from(iter); }

function _arrayWithoutHoles(arr) { if (Array.isArray(arr)) return _arrayLikeToArray(arr); }

function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _unsupportedIterableToArray(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

function _iterableToArrayLimit(arr, i) { if (typeof Symbol === "undefined" || !(Symbol.iterator in Object(arr))) return; var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }

























var EditProjectPart = function EditProjectPart(_ref) {
  var toggleFetch = _ref.toggleFetch;

  var _UserGlobal = Object(_states_userGlobal__WEBPACK_IMPORTED_MODULE_18__["default"])(),
      _UserGlobal2 = _slicedToArray(_UserGlobal, 2),
      userState = _UserGlobal2[0],
      userActions = _UserGlobal2[1];

  var _useState = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(userState.user),
      _useState2 = _slicedToArray(_useState, 2),
      currUser = _useState2[0],
      setCurrUser = _useState2[1];

  var _useState3 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(function () {
    return draft_js__WEBPACK_IMPORTED_MODULE_10__["EditorState"].createEmpty();
  }),
      _useState4 = _slicedToArray(_useState3, 2),
      editorState = _useState4[0],
      setEditorState = _useState4[1];

  var _useState5 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(function () {
    return draft_js__WEBPACK_IMPORTED_MODULE_10__["EditorState"].createEmpty();
  }),
      _useState6 = _slicedToArray(_useState5, 2),
      editorStateBackup = _useState6[0],
      setEditorStateBackup = _useState6[1];

  var _useState7 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(false),
      _useState8 = _slicedToArray(_useState7, 2),
      loading = _useState8[0],
      setLoading = _useState8[1];

  var data = userState.projectData.expires_at ? JSON.parse(userState.projectData.expires_at) : null;

  var _useState9 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(userState.projectData.proposed_settlement ? new Date(moment__WEBPACK_IMPORTED_MODULE_4___default()(userState.projectData.proposed_settlement).format("YYYY-MM-DD")) : new Date(Date.now("YYYY-MM-DD"))),
      _useState10 = _slicedToArray(_useState9, 2),
      proposed_settlement = _useState10[0],
      setDate = _useState10[1];

  var _useState11 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(userState.projectData.proposed_settlement ? new Date(moment__WEBPACK_IMPORTED_MODULE_4___default()(userState.projectData.proposed_settlement).format("YYYY-MM-DD")) : new Date(Date.now("YYYY-MM-DD"))),
      _useState12 = _slicedToArray(_useState11, 2),
      default_proposed_settlement = _useState12[0],
      setDefaultDate = _useState12[1];

  var _useState13 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(data && data.option ? parseInt(data.option) : 0),
      _useState14 = _slicedToArray(_useState13, 2),
      timeLimitOptionValue = _useState14[0],
      setTimeLimitOptionValue = _useState14[1];

  var _useState15 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(data && data.optionVal ? parseInt(data.optionVal) : 0),
      _useState16 = _slicedToArray(_useState15, 2),
      timeLimitValue = _useState16[0],
      setTimeLimitValue = _useState16[1];

  var _useState17 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(_toConsumableArray(userState.projectData.featured_images.map(function (image) {
    return {
      image: image,
      preview: image
    };
  }))),
      _useState18 = _slicedToArray(_useState17, 2),
      featuredImages = _useState18[0],
      setFeaturedImages = _useState18[1];

  var _useState19 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])([]),
      _useState20 = _slicedToArray(_useState19, 2),
      fmPlaceHolder = _useState20[0],
      setFmPlaceHolder = _useState20[1];

  var _useState21 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(""),
      _useState22 = _slicedToArray(_useState21, 2),
      developerValue = _useState22[0],
      setDeveloperValue = _useState22[1];

  var _useState23 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(""),
      _useState24 = _slicedToArray(_useState23, 2),
      developerSelectValue = _useState24[0],
      setDeveloperSelectValue = _useState24[1];

  var _useState25 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(userState.projectData.address.state),
      _useState26 = _slicedToArray(_useState25, 2),
      projectState = _useState26[0],
      setProjectState = _useState26[1];

  var _useState27 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(!lodash_isEmpty__WEBPACK_IMPORTED_MODULE_20___default()(userState.projectData.display_suite_address) && userState.projectData.display_suite_address.state ? userState.projectData.display_suite_address.state : ""),
      _useState28 = _slicedToArray(_useState27, 2),
      displaySuiteState = _useState28[0],
      setDisplaySuiteState = _useState28[1];

  var _useState29 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(String(userState.projectData.sub_property_id)),
      _useState30 = _slicedToArray(_useState29, 2),
      propertyType = _useState30[0],
      setPropertyType = _useState30[1];

  var _useState31 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])([]),
      _useState32 = _slicedToArray(_useState31, 2),
      errors = _useState32[0],
      setErrors = _useState32[1];

  var _useState33 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(userState.projectData.description),
      _useState34 = _slicedToArray(_useState33, 2),
      desc = _useState34[0],
      setDesc = _useState34[1];

  var _useState35 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])("Australia"),
      _useState36 = _slicedToArray(_useState35, 2),
      projectCountry = _useState36[0],
      setProjectCountry = _useState36[1];

  var _useState37 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])("Australia"),
      _useState38 = _slicedToArray(_useState37, 2),
      displaySuiteCountry = _useState38[0],
      setDisplaySuiteCountry = _useState38[1];

  var addProjectForm = Object(react__WEBPACK_IMPORTED_MODULE_1__["useRef"])(null);
  var countries = [{
    label: "Australia",
    value: "Australia"
  }];

  var _useState39 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(userState.projectData),
      _useState40 = _slicedToArray(_useState39, 2),
      project = _useState40[0],
      setProject = _useState40[1];

  var _useState41 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(userState.projectData.is_address_same == 1 ? true : false),
      _useState42 = _slicedToArray(_useState41, 2),
      sameAddress = _useState42[0],
      setSame = _useState42[1];

  var _useState43 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(false),
      _useState44 = _slicedToArray(_useState43, 2),
      limitError = _useState44[0],
      setLimitError = _useState44[1];

  var _useState45 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])([]),
      _useState46 = _slicedToArray(_useState45, 2),
      projectDevelopers = _useState46[0],
      setProjectDevelopers = _useState46[1];

  var _useState47 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(userState.projectData.deposit ? userState.projectData.deposit.toString() : null),
      _useState48 = _slicedToArray(_useState47, 2),
      deposit = _useState48[0],
      setDeposit = _useState48[1];

  var _useState49 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(userState.projectData.is_completed),
      _useState50 = _slicedToArray(_useState49, 2),
      completedNow = _useState50[0],
      setCompletedNow = _useState50[1];

  var _useState51 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(userState.projectData && userState.projectData.sub_property_id == 4),
      _useState52 = _slicedToArray(_useState51, 2),
      isProjectLand = _useState52[0],
      setIsProjectLand = _useState52[1];

  Object(react__WEBPACK_IMPORTED_MODULE_1__["useEffect"])(function () {
    var phs = [];

    for (var index = 0; index < 12 - featuredImages.length; index++) {
      var ph = {
        classes: "cursor-pointer m-2 inline-flex bg-transparent text-palette-gray"
      };
      phs.push(ph);
    }

    setFmPlaceHolder(phs);
  }, [featuredImages.length]);
  Object(react__WEBPACK_IMPORTED_MODULE_1__["useEffect"])(function () {
    var url = "/api/user";
    Object(_data_index__WEBPACK_IMPORTED_MODULE_19__["developers"])(url).then(function (result) {
      setProjectDevelopers(_toConsumableArray(result.data.map(function (developer) {
        return {
          label: "".concat(developer.first_name, " ").concat(developer.last_name),
          value: String(developer.id)
        };
      })));
    })["catch"](function (error) {
      setLoading(false);
      setErrors(error.response.data.errors);
    });
  }, []);
  Object(react__WEBPACK_IMPORTED_MODULE_1__["useEffect"])(function () {
    setEditorState(function () {
      return draft_js__WEBPACK_IMPORTED_MODULE_10__["EditorState"].push(editorState, Object(draft_convert__WEBPACK_IMPORTED_MODULE_13__["convertFromHTML"])(userState.projectData.description));
    });
    setEditorStateBackup(function () {
      return draft_js__WEBPACK_IMPORTED_MODULE_10__["EditorState"].push(editorStateBackup, Object(draft_convert__WEBPACK_IMPORTED_MODULE_13__["convertFromHTML"])(userState.projectData.description));
    });
  }, []);
  Object(react__WEBPACK_IMPORTED_MODULE_1__["useEffect"])(function () {
    var currentProjectDeveloper = projectDevelopers.filter(function (val, i) {
      return parseInt(val.value) === userState.projectData.user_id;
    });
    setDeveloperValue(userState.projectData.user_id);
    setDeveloperSelectValue(currentProjectDeveloper[0]);
  }, [projectDevelopers]);

  var getDealCoordinates = /*#__PURE__*/function () {
    var _ref3 = _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee(_ref2) {
      var formData, url, address, suburb, addressParam, data, _address, _suburb, _addressParam, _data;

      return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee$(_context) {
        while (1) {
          switch (_context.prev = _context.next) {
            case 0:
              formData = _ref2.formData, url = _ref2.url;
              address = formData.get("project_address_line_1");
              suburb = formData.get("project_address_suburb");
              addressParam = address.concat(" ", suburb).concat(" ", projectState).concat(" ", projectCountry);
              _context.next = 6;
              return Object(_helpers_mapCoordinateHelper__WEBPACK_IMPORTED_MODULE_21__["getMapCoordinates"])(addressParam);

            case 6:
              data = _context.sent;
              formData.append("long", data["long"]);
              formData.append("lat", data.lat);

              if (sameAddress) {
                _context.next = 18;
                break;
              }

              _address = formData.get("display_suite_address_line_1");
              _suburb = formData.get("display_suite_address_suburb");
              _addressParam = _address.concat(" ", _suburb).concat(" ", displaySuiteState).concat(" ", displaySuiteCountry);
              _context.next = 15;
              return Object(_helpers_mapCoordinateHelper__WEBPACK_IMPORTED_MODULE_21__["getMapCoordinates"])(_addressParam);

            case 15:
              _data = _context.sent;
              formData.append("display_suite_long", _data["long"]);
              formData.append("display_suite_lat", _data.lat);

            case 18:
            case "end":
              return _context.stop();
          }
        }
      }, _callee);
    }));

    return function getDealCoordinates(_x) {
      return _ref3.apply(this, arguments);
    };
  }();

  var saveDeal = /*#__PURE__*/function () {
    var _ref5 = _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee2(_ref4) {
      var formData, url;
      return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee2$(_context2) {
        while (1) {
          switch (_context2.prev = _context2.next) {
            case 0:
              formData = _ref4.formData, url = _ref4.url;
              _context2.next = 3;
              return getDealCoordinates({
                formData: formData
              });

            case 3:
              _context2.next = 5;
              return Object(_data_index__WEBPACK_IMPORTED_MODULE_19__["saveUpdateDeal"])({
                formData: formData,
                url: url
              }).then(function (r) {
                if (r.status === 200) {
                  setLoading(false);
                  Object(_components_base_alerts_sweetAlert__WEBPACK_IMPORTED_MODULE_5__["sweetAlert"])("success", "Project successfully updated!");
                  setDefaultDate(proposed_settlement);
                  toggleFetch();
                }
              })["catch"](function (error) {
                if (error.response.data.errors) {
                  Object(_components_base_alerts_sweetAlert__WEBPACK_IMPORTED_MODULE_5__["sweetAlert"])("error", "Some fields are missing");
                }

                setLoading(false);
                setErrors(error.response.data.errors);
              }).then(function () {});

            case 5:
            case "end":
              return _context2.stop();
          }
        }
      }, _callee2);
    }));

    return function saveDeal(_x2) {
      return _ref5.apply(this, arguments);
    };
  }();

  var saveProject = function saveProject() {
    setLoading(true);
    var formData = new FormData(addProjectForm.current);
    var newDate = new Date(moment__WEBPACK_IMPORTED_MODULE_4___default()(userState.projectData.created_at).format("YYYY-MM-DD"));
    formData.append("role", "project_developer");
    formData.append("propertyType", propertyType);
    formData.append("project_address_state", projectState);
    formData.append("project_address_country", projectCountry);
    formData.append("display_suite_address_state", displaySuiteState);
    formData.append("display_suite_address_country", displaySuiteCountry);
    formData.append("deposit", deposit);
    formData.append("developer", developerValue || currUser.id);
    formData.append("time_limit[optionVal]", timeLimitValue || 0);
    formData.append("time_limit[option]", timeLimitOptionValue || 0);
    formData.append("time_limit[createdAt]", newDate);
    formData.append("same_address", sameAddress);
    formData.append("isCompleted", completedNow);
    formData.append("proposed_settlement", proposed_settlement);
    formData.append("isPropertyTypeLand", isProjectLand);
    formData.append("description", editorState.getCurrentContent().getPlainText());
    formData.append("descriptionHTML", Object(draft_convert__WEBPACK_IMPORTED_MODULE_13__["convertToHTML"])(editorState.getCurrentContent()));
    featuredImages.forEach(function (image, key) {
      formData.append("featuredImages[".concat(key, "]"), image.image);
    });
    formData.append("_method", "PATCH");
    var url = "/api/deal/".concat(userState.projectData.id);
    saveDeal({
      formData: formData,
      url: url
    });
  };

  var handleSubmit = function handleSubmit(e) {
    e.preventDefault();
    setLimitError(false);
    saveProject();
  };

  var handleDrop = function handleDrop(e) {
    e.preventDefault();
    e.stopPropagation();
    var fmCount = featuredImages.length;
    var onCount = e.dataTransfer.files.length;

    if (fmCount + onCount > 12) {
      e.target.value = null;
      setLimitError(true);
      return;
    } else {
      setLimitError(false);
    }

    setFeaturedImages(featuredImages.concat(_toConsumableArray(_toConsumableArray(e.dataTransfer.files).map(function (file) {
      var newFilename = Object(_helpers_fileHelper_fileHelper__WEBPACK_IMPORTED_MODULE_7__["sanitizeFilename"])(file.name);
      var newFileObj = new File([file], newFilename);
      return {
        image: newFileObj,
        preview: URL.createObjectURL(newFileObj)
      };
    }))));
    e.target.value = null;
  };

  var handleChange = function handleChange(e) {
    var fmCount = featuredImages.length;
    var onCount = e.target.files.length;

    if (fmCount + onCount > 12) {
      setLimitError(true);
      e.target.value = null;
      return;
    } else {
      setLimitError(false);
    }

    setFeaturedImages(featuredImages.concat(_toConsumableArray(_toConsumableArray(e.target.files).map(function (file) {
      var newFilename = Object(_helpers_fileHelper_fileHelper__WEBPACK_IMPORTED_MODULE_7__["sanitizeFilename"])(file.name);
      var newFileObj = new File([file], newFilename);
      return {
        image: newFileObj,
        preview: URL.createObjectURL(newFileObj)
      };
    }))));
    e.target.value = null;
  };

  var handleRemove = function handleRemove(e, i) {
    e.preventDefault();
    e.stopPropagation();
    setFeaturedImages(_toConsumableArray(featuredImages.filter(function (image, k) {
      if (i !== k) {
        return image;
      }
    })));

    if (featuredImages.length > 12) {
      setLimitError(true);
    } else {
      setLimitError(false);
    }
  };

  var compareDate = function compareDate(selectedDate) {
    var date = moment__WEBPACK_IMPORTED_MODULE_4___default()(selectedDate, "DD-MM-YYYY"); //Date format

    var defaultDate = moment__WEBPACK_IMPORTED_MODULE_4___default()(default_proposed_settlement, "DD-MM-YYYY");
    var durationDays = date.diff(defaultDate, "days");

    if (durationDays < 0) {
      return false;
    }

    return true;
  };

  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "bg-white rounded-lg py-5 px-8 ".concat(react_device_detect__WEBPACK_IMPORTED_MODULE_6__["isMobile"] ? "my-5" : "m-5")
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("form", {
    ref: addProjectForm,
    onSubmit: handleSubmit
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "text-black"
  }, Object(_services_auth__WEBPACK_IMPORTED_MODULE_8__["userRole"])() === "admin" && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "flex ".concat(react_device_detect__WEBPACK_IMPORTED_MODULE_6__["isMobile"] ? "flex flex-col" : "items-center")
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("label", {
    className: "block py-3 font-semibold w-40"
  }, "Project Developer"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "relative"
  }, !lodash_isEmpty__WEBPACK_IMPORTED_MODULE_20___default()(projectDevelopers) ? /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(react_dropdown__WEBPACK_IMPORTED_MODULE_2___default.a, {
    className: "".concat(react_device_detect__WEBPACK_IMPORTED_MODULE_6__["isMobile"] ? "w-full pointer-events-none cursor-not-allowed" : "pointer-events-none cursor-not-allowed"),
    id: "project_developer",
    value: developerSelectValue,
    options: projectDevelopers,
    onChange: function onChange(option) {
      setDeveloperValue(option.value);
    },
    placeholder: "Please select...",
    name: "project_developer"
  }) : /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_fortawesome_react_fontawesome__WEBPACK_IMPORTED_MODULE_15__["FontAwesomeIcon"], {
    icon: _fortawesome_free_solid_svg_icons__WEBPACK_IMPORTED_MODULE_16__["faSpinner"],
    className: "fa-spin mr-2"
  })), errors["developer"] && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
    className: "absolute px-1 text-red-400 tracking-widest text-xs "
  }, errors["developer"][0])), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "py-3 flex  ".concat(react_device_detect__WEBPACK_IMPORTED_MODULE_6__["isMobile"] ? "flex flex-col" : "items-center", " ")
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("label", {
    className: "font-semibold ".concat(react_device_detect__WEBPACK_IMPORTED_MODULE_6__["isMobile"] ? "py-3" : "w-1/5"),
    htmlFor: "name"
  }, "Project Name:"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "flex-1 relative"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_base__WEBPACK_IMPORTED_MODULE_9__["TextInput"], {
    id: "name",
    defaultValue: userState.projectData.name,
    className: "border-gray-400 border py-1 text-sm pl-2",
    border: false,
    name: "name"
  }), errors["name"] && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
    className: "absolute px-1 text-red-400 tracking-widest  text-xs "
  }, errors["name"][0]))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "py-3"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("label", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "w-full text-sm bg-white text-black "
  }, editorStateBackup.getCurrentContent().getPlainText() && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_base__WEBPACK_IMPORTED_MODULE_9__["RichTextEditor"], {
    title: "Project Description",
    richEditorState: editorState,
    richEditorStateContent: function richEditorStateContent(content) {
      return setEditorState(content);
    },
    characterLimit: 2000
  }))), errors["description"] && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
    className: "px-1 text-red-400 tracking-widest  text-xs "
  }, errors["description"][0])), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "flex justify-between"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "flex flex-start items-center"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("label", {
    className: "block py-3 w-32"
  }, "Project Type"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "relative"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(react_dropdown__WEBPACK_IMPORTED_MODULE_2___default.a, {
    id: "property_type",
    value: propertyType,
    options: _helpers_propertyHelper_propertyHelper__WEBPACK_IMPORTED_MODULE_14__["property_types"],
    onChange: function onChange(option) {
      if (option.label === "Land") {
        setIsProjectLand(true);
      } else {
        setIsProjectLand(false);
      }

      setPropertyType(option.value);
    },
    placeholder: "Please select...",
    name: "property_type"
  }), errors["propertyType"] && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
    className: "absolute px-1 text-red-400 tracking-widest  text-xs "
  }, errors["propertyType"][0]))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "px-10 flex flex-start items-center "
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("label", {
    className: "block py-3 pr-10"
  }, "Deposit"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "relative flex items-center w-48"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(react_dropdown__WEBPACK_IMPORTED_MODULE_2___default.a, {
    id: "deposit",
    value: deposit,
    options: [{
      label: "5",
      value: "5"
    }, {
      label: "10",
      value: "10"
    }],
    onChange: function onChange(option) {
      setDeposit(option.value);
    },
    placeholder: "Please select...",
    name: "deposit"
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
    className: "font-extrabold pl-3"
  }, " % "), errors["deposit"] && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
    className: "absolute px-1 text-red-400 tracking-widest  text-xs "
  }, errors["deposit"][0])))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("h1", {
    className: "font-semibold mt-5"
  }, "Project Address"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "w-full"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "py-3 flex w-full ".concat(react_device_detect__WEBPACK_IMPORTED_MODULE_6__["isMobile"] ? "flex-col" : "items-center")
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("label", {
    className: "".concat(react_device_detect__WEBPACK_IMPORTED_MODULE_6__["isMobile"] ? "w-full pb-3" : "w-32")
  }, "Address Line 1"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "flex-1 relative ".concat(react_device_detect__WEBPACK_IMPORTED_MODULE_6__["isMobile"] ? "w-full" : "w-32")
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_base__WEBPACK_IMPORTED_MODULE_9__["TextInput"], {
    defaultValue: userState.projectData.address.line_1,
    className: " border-gray-400 border py-1 pl-2",
    border: false,
    name: "project_address_line_1"
  }), errors["project_address_line_1"] && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
    className: "absolute px-1 text-red-400 tracking-widest  text-xs "
  }, errors["project_address_line_1"][0]))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "flex justify-between ".concat(react_device_detect__WEBPACK_IMPORTED_MODULE_6__["isMobile"] ? "flex-col" : "py-3")
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "flex ".concat(react_device_detect__WEBPACK_IMPORTED_MODULE_6__["isMobile"] ? "flex-col" : "items-center")
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("label", {
    className: "".concat(react_device_detect__WEBPACK_IMPORTED_MODULE_6__["isMobile"] ? "w-full" : "w-32", " block py-3")
  }, "Suburb", " "), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    style: {
      width: "320px"
    },
    className: "flex-1 relative ".concat(react_device_detect__WEBPACK_IMPORTED_MODULE_6__["isMobile"] ? "w-full" : "w-40")
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_base__WEBPACK_IMPORTED_MODULE_9__["TextInput"], {
    className: " border-gray-400 border py-1 pl-2",
    border: false,
    name: "project_address_suburb",
    defaultValue: userState.projectData.address.suburb
  }), errors["project_address_suburb"] && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
    className: "absolute px-1 text-red-400 tracking-widest  text-xs "
  }, errors["project_address_suburb"][0]))), !isProjectLand && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "py-3 flex ".concat(react_device_detect__WEBPACK_IMPORTED_MODULE_6__["isMobile"] ? "flex-col" : "ml-5 items-center ")
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("label", {
    className: "block py-3 ".concat(react_device_detect__WEBPACK_IMPORTED_MODULE_6__["isMobile"] ? "w-full" : "w-32")
  }, "City", " "), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "relative ".concat(react_device_detect__WEBPACK_IMPORTED_MODULE_6__["isMobile"] ? "w-full" : "")
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_base__WEBPACK_IMPORTED_MODULE_9__["TextInput"], {
    className: " border-gray-400 border pl-2 py-1",
    border: false,
    name: "project_address_city",
    defaultValue: userState.projectData.address.city
  }), errors["project_address_city"] && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
    className: "absolute px-1 text-red-400 tracking-widest  text-xs "
  }, errors["project_address_city"][0])))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "flex justify-between ".concat(react_device_detect__WEBPACK_IMPORTED_MODULE_6__["isMobile"] ? "flex-col" : "py-3")
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "flex ".concat(react_device_detect__WEBPACK_IMPORTED_MODULE_6__["isMobile"] ? "flex-col" : "items-center")
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("label", {
    className: "".concat(react_device_detect__WEBPACK_IMPORTED_MODULE_6__["isMobile"] ? "w-full" : "w-32", " block py-3")
  }, "State", " "), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "relative"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(react_dropdown__WEBPACK_IMPORTED_MODULE_2___default.a, {
    className: "".concat(react_device_detect__WEBPACK_IMPORTED_MODULE_6__["isMobile"] ? "w-full" : ""),
    id: "project_address_state",
    value: projectState,
    options: _helpers_propertyHelper_propertyHelper__WEBPACK_IMPORTED_MODULE_14__["states"],
    onChange: function onChange(option) {
      setProjectState(option.value);
    },
    placeholder: "Select state",
    name: "project_address_state"
  }), errors["project_address_state"] && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
    className: "absolute px-1 text-red-400 tracking-widest  text-xs "
  }, errors["project_address_state"][0]))), !isProjectLand && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "flex items-center ".concat(react_device_detect__WEBPACK_IMPORTED_MODULE_6__["isMobile"] ? "flex-col py-3" : "ml-5")
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("label", {
    className: "block py-3 ".concat(react_device_detect__WEBPACK_IMPORTED_MODULE_6__["isMobile"] ? "w-full" : "w-32")
  }, "Postcode/ZIP", " "), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "relative ".concat(react_device_detect__WEBPACK_IMPORTED_MODULE_6__["isMobile"] ? "w-full" : "")
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_base__WEBPACK_IMPORTED_MODULE_9__["TextInput"], {
    className: " border-gray-400 border pl-2 py-1",
    border: false,
    name: "project_address_zip",
    defaultValue: userState.projectData.address.postal
  }), errors["project_address_zip"] && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
    className: "absolute px-1 text-red-400 tracking-widest  text-xs "
  }, errors["project_address_zip"][0])))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: " flex ".concat(react_device_detect__WEBPACK_IMPORTED_MODULE_6__["isMobile"] ? "flex-col" : "py-3")
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("label", {
    className: "".concat(react_device_detect__WEBPACK_IMPORTED_MODULE_6__["isMobile"] ? "w-full" : "w-32", " block py-3")
  }, "Country", " "), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "relative"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(react_dropdown__WEBPACK_IMPORTED_MODULE_2___default.a, {
    className: "".concat(react_device_detect__WEBPACK_IMPORTED_MODULE_6__["isMobile"] ? "w-full" : ""),
    id: "project_address_country",
    value: projectCountry,
    options: countries,
    onChange: function onChange(option) {
      setProjectCountry(option.value);
    },
    name: "project_address_country"
  }), errors["project_address_country"] && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
    className: "absolute px-1 text-red-400 tracking-widest  text-xs "
  }, errors["project_address_country"][0])))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "flex justify-between mt-5 ".concat(react_device_detect__WEBPACK_IMPORTED_MODULE_6__["isMobile"] ? "flex-col" : "")
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("h1", {
    className: "font-semibold"
  }, isProjectLand ? "Office" : "Display Suite", " Address"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "flex p-1 items-center ".concat(react_device_detect__WEBPACK_IMPORTED_MODULE_6__["isMobile"] ? "" : "justify-between", " ")
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("input", {
    type: "checkbox",
    name: "same_address_check",
    id: "same_address",
    onChange: function onChange(e) {
      setSame(e.target.checked);
    },
    checked: sameAddress
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
    className: "p-1"
  }, isProjectLand ? "Office" : "Display Suite", " Address is same as Project Address"))), !sameAddress && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "py-3 flex w-full ".concat(react_device_detect__WEBPACK_IMPORTED_MODULE_6__["isMobile"] ? "flex-col" : "items-center")
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("label", {
    className: "".concat(react_device_detect__WEBPACK_IMPORTED_MODULE_6__["isMobile"] ? "w-full pb-3" : "w-32")
  }, "Address Line 1"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "flex-1 relative ".concat(react_device_detect__WEBPACK_IMPORTED_MODULE_6__["isMobile"] ? "w-full" : "w-32")
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_base__WEBPACK_IMPORTED_MODULE_9__["TextInput"], {
    defaultValue: userState.projectData.display_suite_address.line_1,
    className: " border-gray-400 border py-1 flex-1 pl-2",
    border: false,
    name: "display_suite_address_line_1"
  }), errors["display_suite_address_line_1"] && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
    className: "absolute px-1 text-red-400 tracking-widest  text-xs "
  }, errors["display_suite_address_line_1"][0]))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "flex justify-between ".concat(react_device_detect__WEBPACK_IMPORTED_MODULE_6__["isMobile"] ? "flex-col" : "py-3")
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "flex ".concat(react_device_detect__WEBPACK_IMPORTED_MODULE_6__["isMobile"] ? "flex-col" : "items-center")
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("label", {
    className: "".concat(react_device_detect__WEBPACK_IMPORTED_MODULE_6__["isMobile"] ? "w-full" : "w-32", " block py-3")
  }, "Suburb", " "), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    style: {
      width: "320px"
    },
    className: "flex-1 relative ".concat(react_device_detect__WEBPACK_IMPORTED_MODULE_6__["isMobile"] ? "w-full" : "w-40")
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_base__WEBPACK_IMPORTED_MODULE_9__["TextInput"], {
    className: " border-gray-400 border py-1 pl-2",
    border: false,
    name: "display_suite_address_suburb",
    defaultValue: userState.projectData.display_suite_address.suburb
  }), errors["display_suite_address_suburb"] && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
    className: "absolute px-1 text-red-400 tracking-widest  text-xs "
  }, errors["display_suite_address_suburb"][0]))), !isProjectLand && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "py-3 flex ".concat(react_device_detect__WEBPACK_IMPORTED_MODULE_6__["isMobile"] ? "flex-col" : "ml-5 items-center")
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("label", {
    className: "block py-3 ".concat(react_device_detect__WEBPACK_IMPORTED_MODULE_6__["isMobile"] ? "w-full" : "w-32")
  }, "City", " "), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "relative ".concat(react_device_detect__WEBPACK_IMPORTED_MODULE_6__["isMobile"] ? "w-full" : "")
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_base__WEBPACK_IMPORTED_MODULE_9__["TextInput"], {
    className: " border-gray-400 border pl-2 py-1",
    border: false,
    name: "display_suite_address_city",
    defaultValue: userState.projectData.display_suite_address.city
  }), errors["display_suite_address_city"] && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
    className: "absolute px-1 text-red-400 tracking-widest  text-xs "
  }, errors["display_suite_address_city"][0])))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "flex justify-between ".concat(react_device_detect__WEBPACK_IMPORTED_MODULE_6__["isMobile"] ? "flex-col" : "py-3")
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "flex ".concat(react_device_detect__WEBPACK_IMPORTED_MODULE_6__["isMobile"] ? "flex-col" : "items-center")
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("label", {
    className: "".concat(react_device_detect__WEBPACK_IMPORTED_MODULE_6__["isMobile"] ? "w-full" : "w-32", " block py-3")
  }, "State", " "), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "relative"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(react_dropdown__WEBPACK_IMPORTED_MODULE_2___default.a, {
    className: "".concat(react_device_detect__WEBPACK_IMPORTED_MODULE_6__["isMobile"] ? "w-full" : ""),
    id: "display_suite_address_state",
    value: displaySuiteState,
    options: _helpers_propertyHelper_propertyHelper__WEBPACK_IMPORTED_MODULE_14__["states"],
    onChange: function onChange(option) {
      return setDisplaySuiteState(option.value);
    },
    placeholder: "Select state",
    name: "display_suite_address_state"
  }), errors["display_suite_address_state"] && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
    className: "absolute px-1 text-red-400 tracking-widest  text-xs "
  }, errors["display_suite_address_state"][0]))), !isProjectLand && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "flex ".concat(react_device_detect__WEBPACK_IMPORTED_MODULE_6__["isMobile"] ? "flex-col py-3" : "ml-5 items-center")
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("label", {
    className: "block py-3 ".concat(react_device_detect__WEBPACK_IMPORTED_MODULE_6__["isMobile"] ? "w-full" : "w-32")
  }, "Postcode/ZIP", " "), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "relative ".concat(react_device_detect__WEBPACK_IMPORTED_MODULE_6__["isMobile"] ? "w-full" : "")
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_base__WEBPACK_IMPORTED_MODULE_9__["TextInput"], {
    className: " border-gray-400 border pl-2 py-1",
    border: false,
    name: "display_suite_address_zip",
    defaultValue: userState.projectData.display_suite_address.postal
  }), errors["display_suite_address_zip"] && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
    className: "absolute px-1 text-red-400 tracking-widest  text-xs "
  }, errors["display_suite_address_zip"][0])))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: " flex ".concat(react_device_detect__WEBPACK_IMPORTED_MODULE_6__["isMobile"] ? "flex-col" : "py-3")
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("label", {
    className: "".concat(react_device_detect__WEBPACK_IMPORTED_MODULE_6__["isMobile"] ? "w-full" : "w-32", " block py-3")
  }, "Country", " "), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "relative"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(react_dropdown__WEBPACK_IMPORTED_MODULE_2___default.a, {
    className: "".concat(react_device_detect__WEBPACK_IMPORTED_MODULE_6__["isMobile"] ? "w-full" : ""),
    id: "display_suite_address_country",
    value: displaySuiteCountry,
    options: countries,
    onChange: function onChange(option) {
      setDisplaySuiteCountry(option.value);
    },
    name: "display_suite_address_country"
  }), errors["display_suite_address_country"] && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
    className: "absolute px-1 text-red-400 tracking-widest  text-xs "
  }, errors["display_suite_address_country"][0])))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: " ".concat(react_device_detect__WEBPACK_IMPORTED_MODULE_6__["isMobile"] ? "py-8" : "py-3")
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("label", {
    className: "cursor-pointer"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("h1", {
    className: "font-semibold mb-4"
  }, "Estimated Completion/", isProjectLand ? "Land Registration" : "Settlement"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_fortawesome_react_fontawesome__WEBPACK_IMPORTED_MODULE_15__["FontAwesomeIcon"], {
    className: "mr-2",
    icon: _fortawesome_free_regular_svg_icons__WEBPACK_IMPORTED_MODULE_17__["faCalendarCheck"]
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(react_datepicker__WEBPACK_IMPORTED_MODULE_11___default.a, {
    className: "px-2 py-1",
    id: "proposed_settlement" // name={`proposed_settlement`}
    ,
    showMonthDropdown: true,
    showYearDropdown: true,
    dateFormat: "PP",
    selected: proposed_settlement,
    disabled: completedNow,
    onSelect: function onSelect(date) {
      if (!compareDate(date)) {
        Object(_components_base_alerts_sweetAlert__WEBPACK_IMPORTED_MODULE_5__["sweetAlert"])("error", "Please select future date!");
      } else {
        setDate(date);
      }
    },
    onChange: function onChange(date) {
      if (!compareDate(date)) {
        Object(_components_base_alerts_sweetAlert__WEBPACK_IMPORTED_MODULE_5__["sweetAlert"])("error", "Please select future date!");
      } else {
        setDate(date);
      }
    }
  })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "p-1 flex items-center mt-3"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("input", {
    type: "checkbox",
    id: "completed_now",
    checked: completedNow,
    onChange: function onChange(e) {
      setCompletedNow(e.target.checked);
    }
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
    className: "p-1"
  }, "Completed Now"))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "py-3"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("h1", {
    className: "font-semibold"
  }, " Featured Images (min. 3 images) "), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("label", {
    className: "button ",
    htmlFor: "upload-photos"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: ""
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    // cursor-pointer flex flex-col flex-wrap h-full overflow-auto w-full
    className: "w-full flex flex-wrap  cursor-pointer",
    style: {
      border: "2px dashed #ccc",
      borderRadius: "10px",
      padding: "20px",
      textAlign: "center",
      marginBottom: "0px"
    },
    onDrop: function onDrop(e) {
      return handleDrop(e);
    },
    onDragOver: function onDragOver(e) {
      e.preventDefault();
      e.stopPropagation();
    },
    onDragEnter: function onDragEnter(e) {
      e.preventDefault();
      e.stopPropagation();
    },
    onDragLeave: function onDragLeave(e) {
      e.preventDefault();
      e.stopPropagation();
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("input", {
    type: "file",
    id: "upload-photos",
    accept: "image/*",
    multiple: true,
    className: "hidden",
    name: "image[]",
    onChange: function onChange(e) {
      return handleChange(e);
    }
  }), featuredImages.map(function (image, index) {
    return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
      key: index,
      className: "rounded-sm border m-2 relative",
      style: {
        backgroundImage: "url('".concat(image.preview, "')"),
        backgroundPositionX: "center",
        width: "6.9em",
        height: "8.9rem",
        backgroundSize: "cover"
      }
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
      className: "absolute bg-palette-blue-light font-bold leading-none ml-1 mt-1 rounded-full text-white w-1/4"
    }, index + 1), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_base__WEBPACK_IMPORTED_MODULE_9__["Tooltip"], {
      title: "Remove this item"
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
      onClick: function onClick(e) {
        return handleRemove(e, index);
      },
      className: "absolute bottom-0 right-0 p-2 -mb-2 "
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_fortawesome_react_fontawesome__WEBPACK_IMPORTED_MODULE_15__["FontAwesomeIcon"], {
      className: "text-red-500 shadow-xl hover:font-bold hover:text-red-700",
      icon: _fortawesome_free_solid_svg_icons__WEBPACK_IMPORTED_MODULE_16__["faTimes"]
    }))));
  }), fmPlaceHolder.map(function (p, index) {
    return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
      key: index,
      className: "".concat(p.classes, " cursor-pointer flex justify-center items-center"),
      style: {
        width: "6.9rem",
        height: "8.9rem",
        border: "2px solid #ccc"
      }
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
      className: "absolute font-bold leading-none w-1/4"
    }, index + featuredImages.length + 1));
  })), limitError && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
    className: "px-1 text-red-400 tracking-widest  text-xs "
  }, "Please limit your photos to twelve(12) items only."))), errors["featuredImages"] && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
    className: "px-1 text-red-400 tracking-widest  text-xs "
  }, errors["featuredImages"][0])), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "flex justify-end my-10"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_base__WEBPACK_IMPORTED_MODULE_9__["Button"], {
    className: "font-bold rounded-full",
    disabled: loading
  }, loading && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_fortawesome_react_fontawesome__WEBPACK_IMPORTED_MODULE_15__["FontAwesomeIcon"], {
    icon: _fortawesome_free_solid_svg_icons__WEBPACK_IMPORTED_MODULE_16__["faSpinner"],
    className: "fa-spin mr-2"
  }), "Continue")))));
};

/* harmony default export */ __webpack_exports__["default"] = (EditProjectPart);

/***/ }),

/***/ "./resources/js/components/EditPropertyModal.js":
/*!******************************************************!*\
  !*** ./resources/js/components/EditPropertyModal.js ***!
  \******************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime/regenerator */ "./node_modules/@babel/runtime/regenerator/index.js");
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var react_dropdown__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! react-dropdown */ "./node_modules/react-dropdown/dist/index.js");
/* harmony import */ var react_dropdown__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(react_dropdown__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _helpers_styles_dropdown_css__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../helpers/styles/dropdown.css */ "./resources/js/helpers/styles/dropdown.css");
/* harmony import */ var _helpers_styles_dropdown_css__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(_helpers_styles_dropdown_css__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var _base__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./_base */ "./resources/js/components/_base/index.js");
/* harmony import */ var _base__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(_base__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var draft_js__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! draft-js */ "./node_modules/draft-js/lib/Draft.js");
/* harmony import */ var draft_js__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(draft_js__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var react_datepicker__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! react-datepicker */ "./node_modules/react-datepicker/dist/react-datepicker.min.js");
/* harmony import */ var react_datepicker__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(react_datepicker__WEBPACK_IMPORTED_MODULE_6__);
/* harmony import */ var _helpers_styles_datepicker_css__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../helpers/styles/datepicker.css */ "./resources/js/helpers/styles/datepicker.css");
/* harmony import */ var _helpers_styles_datepicker_css__WEBPACK_IMPORTED_MODULE_7___default = /*#__PURE__*/__webpack_require__.n(_helpers_styles_datepicker_css__WEBPACK_IMPORTED_MODULE_7__);
/* harmony import */ var draft_convert__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! draft-convert */ "./node_modules/draft-convert/esm/index.js");
/* harmony import */ var react_currency_format__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! react-currency-format */ "./node_modules/react-currency-format/lib/currency-format.js");
/* harmony import */ var react_currency_format__WEBPACK_IMPORTED_MODULE_9___default = /*#__PURE__*/__webpack_require__.n(react_currency_format__WEBPACK_IMPORTED_MODULE_9__);
/* harmony import */ var _fortawesome_react_fontawesome__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @fortawesome/react-fontawesome */ "./node_modules/@fortawesome/react-fontawesome/index.es.js");
/* harmony import */ var _fortawesome_free_solid_svg_icons__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! @fortawesome/free-solid-svg-icons */ "./node_modules/@fortawesome/free-solid-svg-icons/index.es.js");
/* harmony import */ var _states_userGlobal__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ../states/userGlobal */ "./resources/js/states/userGlobal.js");
/* harmony import */ var _material_ui_icons__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! @material-ui/icons */ "./node_modules/@material-ui/icons/esm/index.js");
/* harmony import */ var lodash_isEmpty__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! lodash/isEmpty */ "./node_modules/lodash/isEmpty.js");
/* harmony import */ var lodash_isEmpty__WEBPACK_IMPORTED_MODULE_14___default = /*#__PURE__*/__webpack_require__.n(lodash_isEmpty__WEBPACK_IMPORTED_MODULE_14__);
/* harmony import */ var _helpers_resourceHelper_index__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! ../helpers/resourceHelper/index */ "./resources/js/helpers/resourceHelper/index.js");
/* harmony import */ var _base_alerts_sweetAlert__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! ./_base/alerts/sweetAlert */ "./resources/js/components/_base/alerts/sweetAlert.js");
/* harmony import */ var _helpers_propertyHelper_propertyHelper__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! ../helpers/propertyHelper/propertyHelper */ "./resources/js/helpers/propertyHelper/propertyHelper.js");
/* harmony import */ var _data_index__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(/*! ../data/index */ "./resources/js/data/index.js");
/* harmony import */ var _base_alerts_BaseAlert__WEBPACK_IMPORTED_MODULE_19__ = __webpack_require__(/*! ./_base/alerts/BaseAlert */ "./resources/js/components/_base/alerts/BaseAlert.js");
/* harmony import */ var _helpers_fileHelper_fileHelper__WEBPACK_IMPORTED_MODULE_20__ = __webpack_require__(/*! ../helpers/fileHelper/fileHelper */ "./resources/js/helpers/fileHelper/fileHelper.js");


function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

function _toConsumableArray(arr) { return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _unsupportedIterableToArray(arr) || _nonIterableSpread(); }

function _nonIterableSpread() { throw new TypeError("Invalid attempt to spread non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _iterableToArray(iter) { if (typeof Symbol !== "undefined" && Symbol.iterator in Object(iter)) return Array.from(iter); }

function _arrayWithoutHoles(arr) { if (Array.isArray(arr)) return _arrayLikeToArray(arr); }

function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _unsupportedIterableToArray(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

function _iterableToArrayLimit(arr, i) { if (typeof Symbol === "undefined" || !(Symbol.iterator in Object(arr))) return; var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }






















/* harmony default export */ __webpack_exports__["default"] = (function (_ref) {
  var project = _ref.project,
      projectId = _ref.projectId,
      property = _ref.property,
      toggleEditPropertyModal = _ref.toggleEditPropertyModal,
      editToggleFetch = _ref.editToggleFetch;

  var _UserGlobal = Object(_states_userGlobal__WEBPACK_IMPORTED_MODULE_12__["default"])(),
      _UserGlobal2 = _slicedToArray(_UserGlobal, 2),
      userState = _UserGlobal2[0],
      userActions = _UserGlobal2[1];

  var _useState = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(_toConsumableArray(property.featured_images.map(function (image) {
    return {
      image: image,
      preview: image
    };
  }))),
      _useState2 = _slicedToArray(_useState, 2),
      featuredImages = _useState2[0],
      setFeaturedImages = _useState2[1];

  var _useState3 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(""),
      _useState4 = _slicedToArray(_useState3, 2),
      state = _useState4[0],
      setState = _useState4[1];

  var _useState5 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(String(property.property_type_id)),
      _useState6 = _slicedToArray(_useState5, 2),
      propertyType = _useState6[0],
      setPropertyType = _useState6[1];

  var _useState7 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(false),
      _useState8 = _slicedToArray(_useState7, 2),
      loading = _useState8[0],
      setLoading = _useState8[1];

  var _useState9 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(""),
      _useState10 = _slicedToArray(_useState9, 2),
      subPropertyType = _useState10[0],
      setSubPropertyType = _useState10[1];

  var _useState11 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(""),
      _useState12 = _slicedToArray(_useState11, 2),
      ownershipType = _useState12[0],
      setOwnershipType = _useState12[1];

  var _useState13 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(""),
      _useState14 = _slicedToArray(_useState13, 2),
      propertyPrice = _useState14[0],
      setPropertyPrice = _useState14[1];

  var _useState15 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])([]),
      _useState16 = _slicedToArray(_useState15, 2),
      errors = _useState16[0],
      setErrors = _useState16[1];

  var _useState17 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(function () {
    return draft_js__WEBPACK_IMPORTED_MODULE_5__["EditorState"].createEmpty();
  }),
      _useState18 = _slicedToArray(_useState17, 2),
      editorState = _useState18[0],
      setEditorState = _useState18[1];

  var _useState19 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])({}),
      _useState20 = _slicedToArray(_useState19, 2),
      floorPlan = _useState20[0],
      setFLoorPlan = _useState20[1];

  var _useState21 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(""),
      _useState22 = _slicedToArray(_useState21, 2),
      filename = _useState22[0],
      setFilename = _useState22[1];

  var _useState23 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(false),
      _useState24 = _slicedToArray(_useState23, 2),
      limitError = _useState24[0],
      setLimitError = _useState24[1];

  var _useState25 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])([]),
      _useState26 = _slicedToArray(_useState25, 2),
      fmPlaceHolder = _useState26[0],
      setFmPlaceHolder = _useState26[1];

  var _useState27 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(_helpers_propertyHelper_propertyHelper__WEBPACK_IMPORTED_MODULE_17__["costDueDates"]),
      _useState28 = _slicedToArray(_useState27, 2),
      dates = _useState28[0],
      setDates = _useState28[1];

  var _useState29 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(property.strata_cost || ""),
      _useState30 = _slicedToArray(_useState29, 2),
      strataCost = _useState30[0],
      setStrataCost = _useState30[1];

  var _useState31 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(property.water_cost || ""),
      _useState32 = _slicedToArray(_useState31, 2),
      waterCost = _useState32[0],
      setWaterCost = _useState32[1];

  var _useState33 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(property.council_cost || ""),
      _useState34 = _slicedToArray(_useState33, 2),
      councilCost = _useState34[0],
      setCouncilCost = _useState34[1];

  var _useState35 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(property.strata || ""),
      _useState36 = _slicedToArray(_useState35, 2),
      strata = _useState36[0],
      setStrata = _useState36[1];

  var _useState37 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(property.water || ""),
      _useState38 = _slicedToArray(_useState37, 2),
      water = _useState38[0],
      setWater = _useState38[1];

  var _useState39 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(property.council || ""),
      _useState40 = _slicedToArray(_useState39, 2),
      council = _useState40[0],
      setCouncil = _useState40[1];

  var addPropertyForm = Object(react__WEBPACK_IMPORTED_MODULE_1__["useRef"])(null);
  var floorPlanRef = Object(react__WEBPACK_IMPORTED_MODULE_1__["useRef"])(null);
  Object(react__WEBPACK_IMPORTED_MODULE_1__["useEffect"])(function () {
    var phs = [];

    for (var index = 0; index < 6 - featuredImages.length; index++) {
      var ph = {
        classes: "cursor-pointer m-2 inline-flex bg-transparent text-palette-gray"
      };
      phs.push(ph);
    }

    setFmPlaceHolder(phs);
  }, [featuredImages.length]);
  Object(react__WEBPACK_IMPORTED_MODULE_1__["useEffect"])(function () {
    setEditorState(function () {
      return draft_js__WEBPACK_IMPORTED_MODULE_5__["EditorState"].push(editorState, Object(draft_convert__WEBPACK_IMPORTED_MODULE_8__["convertFromHTML"])(property.description));
    });
  }, []);
  Object(react__WEBPACK_IMPORTED_MODULE_1__["useEffect"])(function () {
    if (property.floor_plan && property.floor_plan.file) {
      var first = property.floor_plan.file.split("/floorplan/")[1];
      var _filename = first.split("?")[0];
      setFilename(_filename);
      setFLoorPlan(property.floor_plan);
    }

    if (property.price) {
      setPropertyPrice(property.price);
    }
  }, []);

  var handleSubmit = function handleSubmit(e) {
    e.preventDefault();
    setLimitError(false);
    saveProperty();
  };

  var saveProperty = /*#__PURE__*/function () {
    var _ref2 = _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee() {
      var formData, url;
      return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee$(_context) {
        while (1) {
          switch (_context.prev = _context.next) {
            case 0:
              setLoading(true);
              formData = new FormData(addPropertyForm.current);
              formData.append("role", "project_developer");

              if (floorPlan && !lodash_isEmpty__WEBPACK_IMPORTED_MODULE_14___default()(floorPlan)) {
                formData.append("floorPlan[file]", floorPlan.file);
                formData.append("floorPlan[name]", "floorplan");
              }

              formData.append("projectId", projectId);
              formData.append("water", water);
              formData.append("council", council);
              formData.append("strata", strata);
              formData.append("propertyType", propertyType);
              formData.append("description", editorState.getCurrentContent().getPlainText());
              formData.append("descriptionHTML", Object(draft_convert__WEBPACK_IMPORTED_MODULE_8__["convertToHTML"])(editorState.getCurrentContent()));
              featuredImages.forEach(function (image, key) {
                formData.append("featuredImages[".concat(key, "]"), image.image);
              });
              formData.append("_method", "PATCH");
              url = "/api/property/".concat(property.id);
              _context.next = 16;
              return Object(_data_index__WEBPACK_IMPORTED_MODULE_18__["saveUpdateProperty"])({
                formData: formData,
                url: url
              }).then(function (retVal) {
                if (retVal.status === 200) {
                  var _project = userState.projectData;
                  _project.properties = retVal.data;
                  userActions.setState({
                    projectData: _project
                  });
                  setLoading(false);
                  Object(_base_alerts_sweetAlert__WEBPACK_IMPORTED_MODULE_16__["sweetAlert"])("success", "Property successfully updated.");
                  editToggleFetch();
                  toggleEditPropertyModal();
                }
              })["catch"](function (error) {
                setErrors(error.response.data.errors);
                setLoading(false);
              }).then(function () {});

            case 16:
            case "end":
              return _context.stop();
          }
        }
      }, _callee);
    }));

    return function saveProperty() {
      return _ref2.apply(this, arguments);
    };
  }();

  var handleDrop = function handleDrop(e) {
    e.preventDefault();
    e.stopPropagation();
    var fmCount = featuredImages.length;
    var onCount = e.dataTransfer.files.length;

    if (fmCount + onCount > 6) {
      e.target.value = null;
      setLimitError(true);
      return;
    } else {
      setLimitError(false);
    }

    setFeaturedImages(featuredImages.concat(_toConsumableArray(_toConsumableArray(e.dataTransfer.files).map(function (file) {
      var newFilename = Object(_helpers_fileHelper_fileHelper__WEBPACK_IMPORTED_MODULE_20__["sanitizeFilename"])(file.name);
      var newFileObj = new File([file], newFilename);
      return {
        image: newFileObj,
        preview: URL.createObjectURL(newFileObj)
      };
    }))));
    e.target.value = null;
  };

  var handleChange = function handleChange(e) {
    var fmCount = featuredImages.length;
    var onCount = e.target.files.length;

    if (fmCount + onCount > 6) {
      setLimitError(true);
      e.target.value = null;
      return;
    } else {
      setLimitError(false);
    }

    setFeaturedImages(featuredImages.concat(_toConsumableArray(_toConsumableArray(e.target.files).map(function (file) {
      var newFilename = file.name.replace(/\s+/g, "");
      var newFileObj = new File([file], newFilename);
      return {
        image: newFileObj,
        preview: URL.createObjectURL(newFileObj)
      };
    }))));
    e.target.value = null;
  };

  var handleRemove = function handleRemove(e, i) {
    e.preventDefault();
    e.stopPropagation();
    setFeaturedImages(_toConsumableArray(featuredImages.filter(function (image, k) {
      if (i !== k) {
        return image;
      }
    })));

    if (featuredImages.length > 6) {
      setLimitError(true);
    } else {
      setLimitError(false);
    }
  };

  var handleFloorPlanChange = function handleFloorPlanChange(e) {
    e.preventDefault();

    if (!Object(_helpers_fileHelper_fileHelper__WEBPACK_IMPORTED_MODULE_20__["PDFFileTypeFilter"])(e.target.files[0]["name"])) {
      Object(_base_alerts_sweetAlert__WEBPACK_IMPORTED_MODULE_16__["sweetAlert"])("error", "Filetype is not supported!");
      e.target.value = null;
      return;
    }

    setFLoorPlan({
      file: _helpers_resourceHelper_index__WEBPACK_IMPORTED_MODULE_15__["resources"].trimFloorPlan(e.target.files[0]),
      name: "floor_plan"
    });
    var newFilename = Object(_helpers_fileHelper_fileHelper__WEBPACK_IMPORTED_MODULE_20__["sanitizeFilename"])(e.target.files[0]["name"]);
    setFilename(newFilename);
    e.target.value = null;
  };

  var handleDeleteFile = function handleDeleteFile(e) {
    e.preventDefault();
    setFLoorPlan({});
    floorPlanRef.current.type = "";
    floorPlanRef.current.type = "file";
    setFilename("");
    e.target.value = null;
  };

  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_base__WEBPACK_IMPORTED_MODULE_4__["Modal"], {
    show: true,
    maxWidth: "md",
    title: "Edit Property",
    onClose: function onClose() {
      return toggleEditPropertyModal();
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("form", {
    ref: addPropertyForm,
    className: "bg-white rounded-lg py-5 px-8 m-5",
    onSubmit: handleSubmit
  }, property.is_property_approved && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_base_alerts_BaseAlert__WEBPACK_IMPORTED_MODULE_19__["default"], {
    message: "This property has already been approved. If you wish to update the price please contact the admin.",
    icon: "alert-triangle",
    type: "success",
    className: "w-full"
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "text-black"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "py-3 flex justify-start "
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "flex justify-center items-center"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("label", {
    className: "font-semibold w-24"
  }, "Property Type"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(react_dropdown__WEBPACK_IMPORTED_MODULE_2___default.a, {
    id: "property_type",
    value: propertyType,
    options: _helpers_propertyHelper_propertyHelper__WEBPACK_IMPORTED_MODULE_17__["property_types"],
    onChange: function onChange(option) {
      setPropertyType(option.value);
    },
    placeholder: "Please select..." // name="property_type"
    ,
    className: "w-48"
  }), errors["propertyType"] && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
    className: "px-1 text-red-400 tracking-widest  text-xs "
  }, errors["propertyType"][0]))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "flex flex-1 items-center justify-end ml-10"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("label", {
    className: "font-semibold w-32"
  }, "Property Number"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_base__WEBPACK_IMPORTED_MODULE_4__["TextInput"], {
    defaultValue: property.unit_name,
    className: "border-gray-400 border pl-2 py-1 w-24",
    border: false,
    name: "unit_name",
    width: "w-24"
  }), errors["unit_name"] && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
    className: "px-1 text-red-400 tracking-widest  text-xs "
  }, errors["unit_name"][0]))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "flex flex-1 items-center justify-end ml-4"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("label", {
    className: "font-semibold w-24"
  }, "Lot Number"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_base__WEBPACK_IMPORTED_MODULE_4__["TextInput"], {
    defaultValue: property.unit_no,
    className: "border-gray-400 border pl-2 py-1 w-24",
    border: false,
    name: "unit_no",
    width: "w-24"
  }), errors["unit_no"] && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
    className: "px-1 text-red-400 tracking-widest  text-xs "
  }, errors["unit_no"][0])))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "flex justify-start my-3"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "flex items-center justify-start py-3"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("label", {
    className: "w-12"
  }, "Price"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "relative"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(react_currency_format__WEBPACK_IMPORTED_MODULE_9___default.a, {
    allowNegative: false,
    className: "border text-sm border-gray-400 px-2 py-1 w-48 ".concat(property.is_property_approved ? "bg-gray-300 cursor-not-allowed" : ""),
    thousandSeparator: true,
    value: propertyPrice,
    onValueChange: function onValueChange(e) {
      setPropertyPrice(e.value);
    },
    name: "price",
    id: "price",
    prefix: "$",
    readOnly: property.is_property_approved
  })), errors["price"] && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
    className: "absolute px-1 text-red-400 tracking-widest text-xxs "
  }, errors["price"][0]))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "py-3 flex items-center ml-12"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("label", {
    title: "Number of bedrooms",
    className: "cursor-pointer mr-4"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_material_ui_icons__WEBPACK_IMPORTED_MODULE_13__["LocalHotel"], null)), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "flex flex-col w-24"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_base__WEBPACK_IMPORTED_MODULE_4__["TextInput"], {
    className: "pl-2 text-sm border-gray-400 w-32 border py-1",
    border: false,
    type: "number",
    name: "number_of_bedrooms",
    defaultValue: property.no_of_bedrooms
  }), errors["number_of_bedrooms"] && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
    className: "px-1 text-red-400 tracking-widest  text-xxs "
  }, errors["number_of_bedrooms"][0]))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "py-3 flex items-center ml-16"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("label", {
    title: "Number of bathrooms",
    className: "cursor-pointer mr-4"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_material_ui_icons__WEBPACK_IMPORTED_MODULE_13__["Bathtub"], null)), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "flex flex-col w-24"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_base__WEBPACK_IMPORTED_MODULE_4__["TextInput"], {
    className: "pl-2 text-sm border-gray-400 w-32 border py-1",
    border: false,
    type: "number",
    name: "number_of_bathrooms",
    defaultValue: property.no_of_bathrooms
  }), errors["number_of_bathrooms"] && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
    className: "px-1 text-red-400 tracking-widest  text-xxs "
  }, errors["number_of_bathrooms"][0]))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "py-3 flex items-center ml-12"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("label", {
    title: "Number of garages",
    className: "cursor-pointer mr-4 "
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_material_ui_icons__WEBPACK_IMPORTED_MODULE_13__["DirectionsCar"], null)), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "flex flex-col w-24"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_base__WEBPACK_IMPORTED_MODULE_4__["TextInput"], {
    className: "pl-2 text-sm border-gray-400 w-32 border py-1",
    border: false,
    type: "number",
    name: "number_of_garages",
    min: "1",
    defaultValue: property.no_of_garages
  }), errors["number_of_garages"] && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
    className: "px-1 text-red-400 tracking-widest  text-xxs "
  }, errors["number_of_garages"][0])))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "flex"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "py-3 flex justify-start items-center"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("label", {
    className: "w-24"
  }, "Internal SQM"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "relative"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "ml-4 w-32"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_base__WEBPACK_IMPORTED_MODULE_4__["TextInput"], {
    className: "pl-2 text-sm border-gray-400 w-32 border py-1",
    border: false,
    type: "number",
    name: "internal_size",
    defaultValue: property.internal_size
  })), errors["internal_size"] && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
    className: "absolute px-1 text-red-400 tracking-widest  text-xxs "
  }, errors["internal_size"][0]))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "py-3 flex justify-start items-center mx-10"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("label", {
    className: "w-24"
  }, "External SQM"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "relative"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "ml-4 w-32"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_base__WEBPACK_IMPORTED_MODULE_4__["TextInput"], {
    className: "pl-2 text-sm border-gray-400 w-32 border py-1",
    border: false,
    type: "number",
    name: "external_size",
    defaultValue: property.external_size
  })), errors["external_size"] && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
    className: "absolute px-1 text-red-400 tracking-widest  text-xxs "
  }, errors["external_size"][0]))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "py-3 flex justify-start items-center ml-3"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("label", {
    className: "w-24"
  }, "Parking SQM"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "relative"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "ml-4 w-32"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_base__WEBPACK_IMPORTED_MODULE_4__["TextInput"], {
    className: "pl-2 text-sm border-gray-400 w-32 border py-1 mr-1",
    border: false,
    type: "number",
    name: "parking_size",
    defaultValue: property.parking_size
  })), errors["parking_size"] && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
    className: "absolute px-1 text-red-400 tracking-widest  text-xxs "
  }, errors["parking_size"][0])))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "flex justify-center my-3"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "py-3 flex flex-1 items-center"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("label", {
    title: "Strata",
    className: "cursor-pointer mr-4 ".concat(errors["strata"] ? "mb-4" : "mb-0")
  }, "Strata"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "flex ml-2"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(react_currency_format__WEBPACK_IMPORTED_MODULE_9___default.a, {
    allowNegative: false,
    className: "border text-sm border-gray-400 px-2 py-1 w-32",
    thousandSeparator: true,
    name: "strata_cost",
    id: "strata_cost",
    prefix: "$",
    value: strataCost,
    onValueChange: function onValueChange(e) {
      setStrataCost(e.value);
    }
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(react_dropdown__WEBPACK_IMPORTED_MODULE_2___default.a, {
    id: "strata",
    value: strata,
    options: dates,
    onChange: function onChange(option) {
      setStrata(option.value);
    },
    placeholder: "Please select...",
    name: "strata",
    className: "w-48 ml-2"
  })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", null, errors["strata"] && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
    className: "px-1 text-red-400 tracking-widest  text-xxs "
  }, errors["strata"][0]), errors["strata_cost"] && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
    className: "px-1 text-red-400 tracking-widest  text-xxs "
  }, errors["strata_cost"][0])))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "flex flex-1 items-center justify-end py-3"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("label", {
    title: "Water",
    className: "cursor-pointer mr-4 ".concat(errors["water"] ? "mb-4" : "mb-0")
  }, "Water"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "flex ml-2"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(react_currency_format__WEBPACK_IMPORTED_MODULE_9___default.a, {
    allowNegative: false,
    className: "border text-sm border-gray-400 px-2 py-1 w-32",
    thousandSeparator: true,
    name: "water_cost",
    id: "water_cost",
    prefix: "$",
    value: waterCost,
    onValueChange: function onValueChange(e) {
      setWaterCost(e.value);
    }
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(react_dropdown__WEBPACK_IMPORTED_MODULE_2___default.a, {
    id: "water",
    value: water,
    options: dates,
    onChange: function onChange(option) {
      setWater(option.value);
    },
    placeholder: "Please select...",
    name: "water",
    className: "w-48 ml-2"
  })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", null, errors["water"] && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
    className: "px-1 text-red-400 tracking-widest  text-xxs "
  }, errors["water"][0]), errors["water_cost"] && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
    className: "px-1 text-red-400 tracking-widest  text-xxs "
  }, errors["water_cost"][0]))))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "flex justify-center my-3"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "py-3 flex flex-1 items-center"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("label", {
    title: "Council",
    className: "cursor-pointer mr-3 ".concat(errors["council"] ? "mb-4" : "mb-0")
  }, "Council"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "flex ml-1"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(react_currency_format__WEBPACK_IMPORTED_MODULE_9___default.a, {
    allowNegative: false,
    className: "border text-sm border-gray-400 px-2 py-1 w-32",
    thousandSeparator: true,
    name: "council_cost",
    id: "council_cost",
    prefix: "$",
    value: councilCost,
    onValueChange: function onValueChange(e) {
      setCouncilCost(e.value);
    }
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(react_dropdown__WEBPACK_IMPORTED_MODULE_2___default.a, {
    id: "council",
    value: council,
    options: dates,
    onChange: function onChange(option) {
      setCouncil(option.value);
    },
    placeholder: "Please select...",
    name: "council",
    className: "w-48 ml-2"
  })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", null, errors["council"] && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
    className: "px-1 text-red-400 tracking-widest  text-xxs "
  }, errors["council"][0]), errors["council_cost"] && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
    className: "px-1 text-red-400 tracking-widest  text-xxs "
  }, errors["council_cost"][0]))))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "py-3"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "w-full text-sm bg-white text-black "
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_base__WEBPACK_IMPORTED_MODULE_4__["RichTextEditor"], {
    key: property.id,
    title: "Property Description",
    richEditorState: editorState,
    richEditorStateContent: function richEditorStateContent(content) {
      return setEditorState(content);
    },
    characterLimit: 2000
  })), errors["description"] && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
    className: "px-1 text-red-400 tracking-widest  text-xs "
  }, errors["description"][0])), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "py-3"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("h1", {
    className: "font-semibold"
  }, " Featured Images (min. 3 images) "), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("label", {
    className: "button ",
    htmlFor: "upload-photos"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: ""
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "w-full flex flex-wrap  cursor-pointer",
    style: {
      border: "2px dashed #ccc",
      borderRadius: "20px",
      padding: "20px",
      textAlign: "center",
      marginBottom: "0px"
    },
    onDrop: function onDrop(e) {
      return handleDrop(e);
    },
    onDragOver: function onDragOver(e) {
      e.preventDefault();
      e.stopPropagation();
    },
    onDragEnter: function onDragEnter(e) {
      e.preventDefault();
      e.stopPropagation();
    },
    onDragLeave: function onDragLeave(e) {
      e.preventDefault();
      e.stopPropagation();
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("input", {
    type: "file",
    id: "upload-photos",
    accept: "image/*",
    multiple: true,
    className: "hidden",
    name: "image[]",
    onChange: function onChange(e) {
      return handleChange(e);
    }
  }), featuredImages.map(function (image, index) {
    return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
      key: index,
      className: "rounded-sm border m-2 relative",
      style: {
        backgroundImage: "url('".concat(image.preview, "')"),
        backgroundPositionX: "center",
        width: "6.9em",
        height: "8.9rem",
        backgroundSize: "cover"
      }
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_base__WEBPACK_IMPORTED_MODULE_4__["Tooltip"], {
      title: "Remove this item"
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
      onClick: function onClick(e) {
        return handleRemove(e, index);
      },
      className: "absolute bottom-0 right-0 p-2 -mb-2 "
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_fortawesome_react_fontawesome__WEBPACK_IMPORTED_MODULE_10__["FontAwesomeIcon"], {
      className: "text-red-500 shadow-xl hover:font-bold hover:text-red-700",
      icon: _fortawesome_free_solid_svg_icons__WEBPACK_IMPORTED_MODULE_11__["faTimes"]
    }))));
  }), fmPlaceHolder.map(function (p, i) {
    return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
      key: i,
      className: "".concat(p.classes, " cursor-pointer flex justify-center items-center"),
      style: {
        width: "6.9rem",
        height: "8.9rem",
        border: "2px solid #ccc"
      }
    }, " ", /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_fortawesome_react_fontawesome__WEBPACK_IMPORTED_MODULE_10__["FontAwesomeIcon"], {
      className: "text-xl",
      icon: _fortawesome_free_solid_svg_icons__WEBPACK_IMPORTED_MODULE_11__["faCamera"]
    }), " ");
  })), limitError && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
    className: "px-1 text-red-400 tracking-widest  text-xs "
  }, "Please limit your photos to six items only."))), errors["featuredImages"] && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
    className: "px-1 text-red-400 tracking-widest  text-xs "
  }, errors["featuredImages"][0])), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "flex flex-col mb-10"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "flex-1"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("h1", {
    className: "font-semibold"
  }, "Floor Plan "), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "py-3 flex flex-col"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "flex"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("label", {
    className: "w-full"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "cursor-pointer w-full p-3 rounded-lg flex",
    style: {
      border: "2px dashed #ccc",
      borderRadius: "20px"
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("input", {
    id: "floorPlan",
    ref: floorPlanRef,
    type: "file",
    name: "floor_plan",
    onChange: function onChange(e) {
      return handleFloorPlanChange(e);
    },
    className: "hidden border text-sm border-gray-400 px-2 py-1 w-full py-1"
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("label", {
    htmlFor: "floorPlan",
    className: " cursor-pointer hover:text-palette-blue-light ".concat(filename ? "text-palette-blue-light" : "")
  }, filename || "Select File"), filename && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "cursor-pointer ml-4 mr-8 text-lg text-red-500"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_fortawesome_react_fontawesome__WEBPACK_IMPORTED_MODULE_10__["FontAwesomeIcon"], {
    onClick: function onClick(e) {
      return handleDeleteFile(e);
    },
    icon: _fortawesome_free_solid_svg_icons__WEBPACK_IMPORTED_MODULE_11__["faTimes"]
  })))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "flex items-center justify-end w-1/2"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_base__WEBPACK_IMPORTED_MODULE_4__["Button"], {
    className: "font-bold rounded-full",
    disabled: loading
  }, loading && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_fortawesome_react_fontawesome__WEBPACK_IMPORTED_MODULE_10__["FontAwesomeIcon"], {
    icon: _fortawesome_free_solid_svg_icons__WEBPACK_IMPORTED_MODULE_11__["faSpinner"],
    className: "fa-spin mr-2"
  }), "Save")))))))));
});

/***/ }),

/***/ "./resources/js/components/EditSteps.js":
/*!**********************************************!*\
  !*** ./resources/js/components/EditSteps.js ***!
  \**********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _states_userGlobal__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../states/userGlobal */ "./resources/js/states/userGlobal.js");
/* harmony import */ var react_device_detect__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! react-device-detect */ "./node_modules/react-device-detect/main.js");
/* harmony import */ var react_device_detect__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(react_device_detect__WEBPACK_IMPORTED_MODULE_2__);
function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _unsupportedIterableToArray(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

function _iterableToArrayLimit(arr, i) { if (typeof Symbol === "undefined" || !(Symbol.iterator in Object(arr))) return; var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }





var EditSteps = function EditSteps(_ref) {
  var activeProjectView = _ref.activeProjectView,
      activeResourcesView = _ref.activeResourcesView,
      activePropertyView = _ref.activePropertyView,
      activeAgreementView = _ref.activeAgreementView,
      toggleProjectView = _ref.toggleProjectView,
      toggleResourcesView = _ref.toggleResourcesView,
      togglePropertyView = _ref.togglePropertyView,
      toggleAgreementView = _ref.toggleAgreementView;

  var _UserGlobal = Object(_states_userGlobal__WEBPACK_IMPORTED_MODULE_1__["default"])(),
      _UserGlobal2 = _slicedToArray(_UserGlobal, 2),
      userState = _UserGlobal2[0],
      userActions = _UserGlobal2[1];

  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "flex justify-between text-palette-blue-light lg:px-10"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "cursor-pointer ".concat(react_device_detect__WEBPACK_IMPORTED_MODULE_2__["isMobile"] ? "flex flex-col items-center justify-center text-center" : ""),
    onClick: function onClick() {
      return toggleProjectView();
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("h1", {
    className: "text-center mb-2"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
    className: "border ".concat(activeProjectView ? "border-palette-blue-light" : "border-gray-600 text-gray-600", "  rounded-full py-1"),
    style: {
      fontSize: "1.2rem",
      paddingLeft: "0.85rem",
      paddingRight: "0.85rem"
    }
  }, "1")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("h4", {
    className: "font-semibold tracking-wider  ".concat(activeProjectView ? "border-palette-blue-light" : "text-gray-600", "  ")
  }, "Project Details")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "flex-1 border-b border-white mx-6 mb-2 border-palette-gray"
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "cursor-pointer ".concat(react_device_detect__WEBPACK_IMPORTED_MODULE_2__["isMobile"] ? "flex flex-col items-center justify-center text-center" : ""),
    onClick: function onClick() {
      return toggleResourcesView();
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("h1", {
    className: "text-center mb-2"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
    className: "border ".concat(activeResourcesView ? "border-palette-blue-light" : "border-gray-600 text-gray-600", " rounded-full py-1 px-3"),
    style: {
      fontSize: "1.2rem"
    }
  }, "2")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("h4", {
    className: "font-semibold tracking-wider  ".concat(activeResourcesView ? "border-palette-blue-light" : "text-gray-600", "  ")
  }, "Project Resources")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "flex-1 border-b border-white mx-6 mb-2 border-palette-gray"
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "cursor-pointer ".concat(react_device_detect__WEBPACK_IMPORTED_MODULE_2__["isMobile"] ? "flex flex-col items-center justify-center text-center" : ""),
    onClick: function onClick() {
      return togglePropertyView();
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("h1", {
    className: "text-center mb-2"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
    className: "border ".concat(activePropertyView ? "border-palette-blue-light" : "border-gray-600 text-gray-600", " rounded-full py-1 px-3"),
    style: {
      fontSize: "1.2rem"
    }
  }, "3")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("h4", {
    className: "font-semibold tracking-wider  ".concat(activePropertyView ? "border-palette-blue-light" : "text-gray-600", " ")
  }, "Project Properties")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "flex-1 border-b border-white mx-6 mb-2 border-palette-gray"
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "cursor-pointer ".concat(react_device_detect__WEBPACK_IMPORTED_MODULE_2__["isMobile"] ? "flex flex-col items-center justify-center text-center" : ""),
    onClick: function onClick() {
      return toggleAgreementView();
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("h1", {
    className: "text-center mb-2"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
    className: "border ".concat(activeAgreementView ? "border-palette-blue-light" : "border-gray-600 text-gray-600", " rounded-full py-1 px-3"),
    style: {
      fontSize: "1.2rem"
    }
  }, "4")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("h4", {
    className: "font-semibold tracking-wider  ".concat(activeAgreementView ? "border-palette-blue-light" : "text-gray-600", " ")
  }, "Agency Agreement")));
};

/* harmony default export */ __webpack_exports__["default"] = (EditSteps);

/***/ }),

/***/ "./resources/js/components/EstimatedCompletionDate.js":
/*!************************************************************!*\
  !*** ./resources/js/components/EstimatedCompletionDate.js ***!
  \************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! moment */ "./node_modules/moment/moment.js");
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(moment__WEBPACK_IMPORTED_MODULE_1__);



var EstimatedCompletionDate = function EstimatedCompletionDate(_ref) {
  var loading = _ref.loading,
      project = _ref.project;
  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "flex font-bold h-8 items-center justify-center mb-2 mr-1 mr-2 mt-6 rounded-lg shadow-lg text-sm xs:w-2/3",
    style: {
      backgroundColor: "rgb(255, 0, 255, 0.1)"
    }
  }, "Est. completion:\xA0", !loading && project && moment__WEBPACK_IMPORTED_MODULE_1___default()(project.proposed_settlement).format("MMM YYYY"));
};

/* harmony default export */ __webpack_exports__["default"] = (EstimatedCompletionDate);

/***/ }),

/***/ "./resources/js/components/ForgetPassword.js":
/*!***************************************************!*\
  !*** ./resources/js/components/ForgetPassword.js ***!
  \***************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _layouts_users_UserLayout__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../layouts/users/UserLayout */ "./resources/js/layouts/users/UserLayout.js");
/* harmony import */ var _base__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./_base */ "./resources/js/components/_base/index.js");
/* harmony import */ var _base__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(_base__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _fortawesome_react_fontawesome__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @fortawesome/react-fontawesome */ "./node_modules/@fortawesome/react-fontawesome/index.es.js");
function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _unsupportedIterableToArray(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

function _iterableToArrayLimit(arr, i) { if (typeof Symbol === "undefined" || !(Symbol.iterator in Object(arr))) return; var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }






var ForgetPassword = function ForgetPassword() {
  var _useState = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])({}),
      _useState2 = _slicedToArray(_useState, 2),
      errors = _useState2[0],
      setErrors = _useState2[1];

  var handleSubmit = function handleSubmit() {};

  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_layouts_users_UserLayout__WEBPACK_IMPORTED_MODULE_1__["default"], null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("section", {
    className: "text-white bg-palette-blue-dark",
    style: {
      marginTop: -130,
      paddingTop: 160
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "mx-auto",
    style: {
      maxWidth: 1366
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("h1", {
    className: "font-bold leading-10 pb-20 pt-24 text-4xl"
  }, "Get in touch."), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_base__WEBPACK_IMPORTED_MODULE_2__["TextInput"], {
    className: "text-base",
    placeholder: "Password",
    name: "password",
    prefix: "mail",
    errors: errors.password
  }), errors.password && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
    className: "text-red-500 text-xs"
  }, errors.password[0]), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_base__WEBPACK_IMPORTED_MODULE_2__["Button"], {
    className: "px-20",
    disabled: loading
  }, loading && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_fortawesome_react_fontawesome__WEBPACK_IMPORTED_MODULE_3__["FontAwesomeIcon"], {
    icon: ["fas", "spinner"],
    className: "fa-spin mr-2"
  }), "Log in"))));
};

/* harmony default export */ __webpack_exports__["default"] = (ForgetPassword);

/***/ }),

/***/ "./resources/js/components/GoogleMaps.js":
/*!***********************************************!*\
  !*** ./resources/js/components/GoogleMaps.js ***!
  \***********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var lodash_isEmpty__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! lodash/isEmpty */ "./node_modules/lodash/isEmpty.js");
/* harmony import */ var lodash_isEmpty__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(lodash_isEmpty__WEBPACK_IMPORTED_MODULE_1__);
function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _unsupportedIterableToArray(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

function _iterableToArrayLimit(arr, i) { if (typeof Symbol === "undefined" || !(Symbol.iterator in Object(arr))) return; var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }



var mapKey = "".concat("AIzaSyB2angcqMN5xDkZHAzghZ5hlg-QldC02ww");

var GoogleMaps = function GoogleMaps(mapProps) {
  var googleMapRef = Object(react__WEBPACK_IMPORTED_MODULE_0__["useRef"])();

  var _useState = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])(16),
      _useState2 = _slicedToArray(_useState, 2),
      zoom = _useState2[0],
      setZoom = _useState2[1];

  var _useState3 = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])(null),
      _useState4 = _slicedToArray(_useState3, 2),
      map = _useState4[0],
      setMap = _useState4[1];

  var dealAddress = "";
  var dealName = "";
  var img = "";
  var panPath = [];
  var panQueue = [];
  var STEPS = 200;
  Object(react__WEBPACK_IMPORTED_MODULE_0__["useEffect"])(function () {
    if (!lodash_isEmpty__WEBPACK_IMPORTED_MODULE_1___default()(mapProps)) {
      getImg();
      appendStrings();
      appendMap();
    }
  }, []);

  var getImg = function getImg() {
    if (mapProps.mapProps && mapProps.mapProps.featured_images.length > 0) {
      img = mapProps.mapProps.featured_images[0];
    }
  };

  var appendMap = function appendMap() {
    var googleMapScript = document.createElement("script");
    googleMapScript.src = "https://maps.googleapis.com/maps/api/js?key=".concat(mapKey);
    googleMapScript.async = true;
    window.document.body.appendChild(googleMapScript);
    googleMapScript.addEventListener("load", function () {
      getLatLng();
    });
  };

  var appendStrings = function appendStrings() {
    if (mapProps.mapProps && mapProps.mapProps.address) {
      if (mapProps.mapProps.address.line_1) {
        dealAddress = dealAddress.concat("", mapProps.mapProps.address.line_1 + ", ");
      }

      if (mapProps.mapProps.address.city) {
        dealAddress = dealAddress.concat(mapProps.mapProps.address.city + ", ");
      }

      if (mapProps.mapProps.address.state) {
        dealAddress = dealAddress.concat(mapProps.mapProps.address.state);
      }

      if (mapProps.mapProps.address.postal) {
        dealAddress = dealAddress.concat(", ", mapProps.mapProps.address.postal);
      }
    }

    if (mapProps.mapProps && mapProps.mapProps.name) {
      dealName = mapProps.mapProps.name;
    }
  };

  var getLatLng = function getLatLng() {
    var latlng = new google.maps.LatLng(parseFloat(mapProps.mapProps.lat), parseFloat(mapProps.mapProps["long"]));
    var mapOptions = {
      zoom: zoom,
      center: latlng,
      gestureHandling: "cooperative"
    };
    var map = new google.maps.Map(googleMapRef.current, mapOptions);
    var contentString = '<div id="root-content">' + '<img  src="' + img + '" class="h-32 w-auto" />' + '<div class="flex flex-col mt-2">' + '<h2 class="font-black">' + dealName + "</h2> <p>" + dealAddress + "</p>" + "</div></div>";
    var infowindow = new google.maps.InfoWindow({
      content: contentString
    });
    var marker = new google.maps.Marker({
      position: latlng
    });
    marker.setMap(map);
    marker.addListener("click", function () {
      infowindow.getMap() ? infowindow.close() : infowindow.open(map, marker);
    });
    map.addListener("zoom_changed", function () {
      panToLocation(parseFloat(mapProps.mapProps.lat), parseFloat(mapProps.mapProps["long"]), map);
    }); // map.setCenter(marker.getPosition());
  };

  var panToLocation = function panToLocation(newLat, newLng, map) {
    if (panPath.length > 0) {
      // We are already panning...queue this up for next move
      panQueue.push([newLat, newLng]);
    } else {
      // Lets compute the points we'll use
      panPath.push("LAZY SYNCRONIZED LOCK"); // make length non-zero - 'release' this before calling setTimeout

      var curLat = map.getCenter().lat();
      var curLng = map.getCenter().lng();
      var dLat = (newLat - curLat) / STEPS;
      var dLng = (newLng - curLng) / STEPS;

      for (var i = 0; i < STEPS; i++) {
        panPath.push([curLat + dLat * i, curLng + dLng * i]);
      }

      panPath.push([newLat, newLng]);
      panPath.shift(); // LAZY SYNCRONIZED LOCK

      setTimeout(doPan(map), 700);
    }
  };

  var doPan = function doPan(map) {
    var next = panPath.shift();

    if (next != null) {
      // Continue our current pan action
      map.panTo(new google.maps.LatLng(next[0], next[1]));
      setTimeout(doPan(map), 700);
    } else {
      // We are finished with this pan - check if there are any queue'd up locations to pan to
      var queued = panQueue.shift();

      if (queued != null) {
        panToLocation(queued[0], queued[1]);
      } else {// map.setZoom(zoom);
      }
    }
  };

  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    id: "google-map",
    ref: googleMapRef,
    style: {
      width: "100%",
      height: "100vh"
    }
  });
};

/* harmony default export */ __webpack_exports__["default"] = (GoogleMaps);

/***/ }),

/***/ "./resources/js/components/LandProjectSize.js":
/*!****************************************************!*\
  !*** ./resources/js/components/LandProjectSize.js ***!
  \****************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);


var LandProjectSize = function LandProjectSize(_ref) {
  var apartment = _ref.apartment;
  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "mt-10 border-gray-500 border-b-2 pb-8"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
    className: "text-base font-bold"
  }, "Size"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "flex flex-col lg:flex-row items-center mt-4"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "flex-1 font-semibold text-base text-gray-700"
  }, "Size: ".concat(apartment.size ? apartment.size : 0, "sqm")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "flex-1 font-semibold text-base text-gray-700"
  }, "Frontage: ".concat(apartment.frontage ? apartment.frontage : 0, "sqm")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "flex-1 font-semibold text-base text-gray-700"
  }, "Width: ".concat(apartment.width ? apartment.width : 0, "sqm")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "flex-1 font-semibold text-base text-gray-700"
  }, "Depth: ".concat(apartment.depth ? apartment.depth : 0, "sqm"))));
};

/* harmony default export */ __webpack_exports__["default"] = (LandProjectSize);

/***/ }),

/***/ "./resources/js/components/MediaPlayer.js":
/*!************************************************!*\
  !*** ./resources/js/components/MediaPlayer.js ***!
  \************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react_player__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react-player */ "./node_modules/react-player/lib/ReactPlayer.js");
/* harmony import */ var react_player__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react_player__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var react_device_detect__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! react-device-detect */ "./node_modules/react-device-detect/main.js");
/* harmony import */ var react_device_detect__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(react_device_detect__WEBPACK_IMPORTED_MODULE_2__);




var MediaPlayer = function MediaPlayer(_ref) {
  var url = _ref.url,
      _ref$className = _ref.className,
      className = _ref$className === void 0 ? "" : _ref$className,
      height = _ref.height;
  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_player__WEBPACK_IMPORTED_MODULE_1___default.a, {
    className: "".concat(className),
    url: url,
    width: "100%",
    height: height
  });
};

/* harmony default export */ __webpack_exports__["default"] = (MediaPlayer);

/***/ }),

/***/ "./resources/js/components/MediaPlayerContainer.js":
/*!*********************************************************!*\
  !*** ./resources/js/components/MediaPlayerContainer.js ***!
  \*********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _base__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./_base */ "./resources/js/components/_base/index.js");
/* harmony import */ var _base__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_base__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _MediaPlayer__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./MediaPlayer */ "./resources/js/components/MediaPlayer.js");




var MediaPlayerContainer = function MediaPlayerContainer(_ref) {
  var toogleShowPlayerModal = _ref.toogleShowPlayerModal,
      url = _ref.url,
      show = _ref.show;
  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_base__WEBPACK_IMPORTED_MODULE_1__["Modal"], {
    show: show,
    maxWidth: "md",
    title: "Project video",
    onClose: function onClose() {
      return toogleShowPlayerModal();
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "bg-white m-10 p-4 rounded"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_MediaPlayer__WEBPACK_IMPORTED_MODULE_2__["default"], {
    url: url
  })));
};

/* harmony default export */ __webpack_exports__["default"] = (MediaPlayerContainer);

/***/ }),

/***/ "./resources/js/components/MemberCount.js":
/*!************************************************!*\
  !*** ./resources/js/components/MemberCount.js ***!
  \************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _states_userGlobal__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../states/userGlobal */ "./resources/js/states/userGlobal.js");
function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _unsupportedIterableToArray(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

function _iterableToArrayLimit(arr, i) { if (typeof Symbol === "undefined" || !(Symbol.iterator in Object(arr))) return; var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }




var MemberCount = function MemberCount(_ref) {
  var append = _ref.append;

  var _UserGlobal = Object(_states_userGlobal__WEBPACK_IMPORTED_MODULE_1__["default"])(),
      _UserGlobal2 = _slicedToArray(_UserGlobal, 2),
      userState = _UserGlobal2[0],
      userAction = _UserGlobal2[1];

  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "".concat(append, "   border flex font-bold h-8 items-center justify-center pt-3 px-4 rounded-full text-base text-palette-teal w-auto")
  }, "Members:\xA0", userState.userCount ? userState.userCount : 0);
};

/* harmony default export */ __webpack_exports__["default"] = (MemberCount);

/***/ }),

/***/ "./resources/js/components/Message.js":
/*!********************************************!*\
  !*** ./resources/js/components/Message.js ***!
  \********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);


var Message = function Message() {
  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "lg:mt-10 lg:px-64 py-10 rounded"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("h1", {
    className: "font-semibold mb-4 text-base text-center text-gray-500"
  }, "A message to our members."), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("p", {
    className: "lg:px-24 text-base text-gray-500"
  }, "We apologise for the temporary disruption to our listed projects. We\u2019re excited to return very soon with more of Australia's biggest discounts. If you're not already a member, sign up for free today. Thank you for your patience during this time."));
};

/* harmony default export */ __webpack_exports__["default"] = (Message);

/***/ }),

/***/ "./resources/js/components/OutgoingCosts.js":
/*!**************************************************!*\
  !*** ./resources/js/components/OutgoingCosts.js ***!
  \**************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react_currency_format__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react-currency-format */ "./node_modules/react-currency-format/lib/currency-format.js");
/* harmony import */ var react_currency_format__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react_currency_format__WEBPACK_IMPORTED_MODULE_1__);



var OutgoingCosts = function OutgoingCosts(_ref) {
  var apartment = _ref.apartment;
  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "mt-10 border-gray-500 border-b-2 pb-8"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
    className: "text-base font-bold"
  }, "Outgoing Costs:"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "flex flex-col lg:flex-row items-center mt-4"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "flex-1 font-semibold text-base text-gray-700"
  }, "Strata:\xA0", /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_currency_format__WEBPACK_IMPORTED_MODULE_1___default.a, {
    value: apartment.strata_cost ? apartment.strata_cost : 0,
    displayType: "text",
    thousandSeparator: true,
    prefix: "$"
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", null, " ".concat(apartment.strata === "Annual" ? "p/a" : "p/q"))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "flex-1 font-semibold text-base text-gray-700"
  }, "Water:\xA0", /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_currency_format__WEBPACK_IMPORTED_MODULE_1___default.a, {
    value: apartment.water_cost ? apartment.water_cost : 0,
    displayType: "text",
    thousandSeparator: true,
    prefix: "$"
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", null, " ".concat(apartment.water === "Annual" ? "p/a" : "p/q"))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "flex-1 font-semibold text-base text-gray-700"
  }, "Council:\xA0", /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_currency_format__WEBPACK_IMPORTED_MODULE_1___default.a, {
    value: apartment.council_cost ? apartment.council_cost : 0,
    displayType: "text",
    thousandSeparator: true,
    prefix: "$"
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", null, " ".concat(apartment.council === "Annual" ? "p/a" : "p/q")))));
};

/* harmony default export */ __webpack_exports__["default"] = (OutgoingCosts);

/***/ }),

/***/ "./resources/js/components/PaypalButton.js":
/*!*************************************************!*\
  !*** ./resources/js/components/PaypalButton.js ***!
  \*************************************************/
/*! exports provided: PaypalButton, PaypalDummyButton */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PaypalButton", function() { return PaypalButton; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PaypalDummyButton", function() { return PaypalDummyButton; });
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime/regenerator */ "./node_modules/@babel/runtime/regenerator/index.js");
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var lodash_isEmpty__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! lodash/isEmpty */ "./node_modules/lodash/isEmpty.js");
/* harmony import */ var lodash_isEmpty__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(lodash_isEmpty__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _services_auth__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../services/auth */ "./resources/js/services/auth.js");


function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _unsupportedIterableToArray(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

function _iterableToArrayLimit(arr, i) { if (typeof Symbol === "undefined" || !(Symbol.iterator in Object(arr))) return; var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }




var paypalClientId = "AdLMgT5h3U8UbbVl8tiZbUio1n3DNFmRmQG8diFSiP1zpTzDyxjGHunqJA1u9cjW4jHeomn3vB9RupwP";

var appendScriptToBody = function appendScriptToBody(loadPaypalComponent) {
  var script = document.createElement("script");
  script.src = "https://www.paypal.com/sdk/js?client-id=".concat(paypalClientId, "&currency=AUD&components=buttons"); // "https://www.paypal.com/sdk/js?client-id=SB_CLIENT_ID";

  script.addEventListener("load", loadPaypalComponent);
  document.body.appendChild(script);
};

var smartButtonStyle = function smartButtonStyle() {
  return {
    layout: "vertical",
    color: "black",
    shape: "rect",
    label: "paypal",
    size: "medium" //tagline: true

  };
};

var PaypalButton = function PaypalButton(_ref) {
  var property_id = _ref.property_id,
      project_id = _ref.project_id,
      setLoading = _ref.setLoading,
      setOpen = _ref.setOpen;

  var _useState = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(false),
      _useState2 = _slicedToArray(_useState, 2),
      paypalLoaded = _useState2[0],
      setPaypalLoaded = _useState2[1];

  var _useState3 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(false),
      _useState4 = _slicedToArray(_useState3, 2),
      showCancelBtn = _useState4[0],
      setShowCancelBtn = _useState4[1];

  var form = Object(react__WEBPACK_IMPORTED_MODULE_1__["createRef"])();

  var handleCallBack = /*#__PURE__*/function () {
    var _ref2 = _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee(details) {
      var formData, _yield$axios$post, data;

      return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee$(_context) {
        while (1) {
          switch (_context.prev = _context.next) {
            case 0:
              formData = new FormData();
              formData.append("property_id", property_id);

              if (!(!lodash_isEmpty__WEBPACK_IMPORTED_MODULE_2___default()(details) && details.status === "COMPLETED")) {
                _context.next = 13;
                break;
              }

              formData.append("transaction_id", details.id);
              formData.append("paypal_payer_id", details.payer.payer_id);
              formData.append("amount", 1000);
              formData.append("status", details.status);
              _context.next = 9;
              return axios.post("/api/paypal-payment", formData, {
                headers: {
                  Authorization: "Bearer " + Object(_services_auth__WEBPACK_IMPORTED_MODULE_3__["isLoggedIn"])()
                }
              });

            case 9:
              _yield$axios$post = _context.sent;
              data = _yield$axios$post.data;
              setLoading(false);
              window.location = "/weekly-deals/".concat(project_id, "/apartment/").concat(property_id, "/confirm");

            case 13:
            case "end":
              return _context.stop();
          }
        }
      }, _callee);
    }));

    return function handleCallBack(_x) {
      return _ref2.apply(this, arguments);
    };
  }();

  Object(react__WEBPACK_IMPORTED_MODULE_1__["useEffect"])(function () {
    appendScriptToBody(loadPaypalComponent);
  }, []);

  var smartBtnConfig = function smartBtnConfig() {
    return {
      intent: "Secure Deal Fee",
      currency_code: "AUD",
      style: smartButtonStyle(),
      onButtonReady: function onButtonReady() {
        setPaypalLoaded(!paypalLoaded);
      },
      onClick: function onClick() {
        setOpen(false);
      },
      createOrder: function createOrder(data, actions) {
        setShowCancelBtn(true);
        return actions.order.create({
          purchase_units: [{
            description: "Secure Deal Fee",
            amount: {
              currency_code: "AUD",
              value: 1000
            }
          }]
        });
      },
      onApprove: function () {
        var _onApprove = _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee2(data, actions) {
          return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee2$(_context2) {
            while (1) {
              switch (_context2.prev = _context2.next) {
                case 0:
                  _context2.next = 2;
                  return actions.order.capture().then(function (details) {
                    handleCallBack(details);
                  });

                case 2:
                  return _context2.abrupt("return", _context2.sent);

                case 3:
                case "end":
                  return _context2.stop();
              }
            }
          }, _callee2);
        }));

        function onApprove(_x2, _x3) {
          return _onApprove.apply(this, arguments);
        }

        return onApprove;
      }(),
      onError: function onError(err) {}
    };
  };

  var loadPaypalComponent = function loadPaypalComponent() {
    setPaypalLoaded(!paypalLoaded);
    window.paypal.Buttons(smartBtnConfig()).render("#paypal-button-container");
  };

  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    id: "paypal-button-container",
    className: "rounded-lg mx-3",
    style: {
      height: 160
    }
  }, showCancelBtn &&
  /*#__PURE__*/
  // style={{ backgroundColor: 'rgb(0, 112, 186)' }}
  // style={{ color: 'rgb(255, 255, 255)' }}
  react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "cursor-pointer flex h-12 items-center justify-center mb-4 mx-16 rounded rounded-full"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("button", {
    onClick: function onClick() {
      {
        setShowCancelBtn(false);
        appendScriptToBody(loadPaypalComponent);
      }
    },
    className: "font-bold hover:underline text-gray-700 text-lg",
    style: {
      color: "rgb(255, 255, 255)"
    }
  }, "Cancel")));
};
var PaypalDummyButton = function PaypalDummyButton(_ref3) {
  var agree = _ref3.agree,
      swal = _ref3.swal;

  var _useState5 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(false),
      _useState6 = _slicedToArray(_useState5, 2),
      paypalLoaded = _useState6[0],
      setPaypalLoaded = _useState6[1];

  var smartBtnConfig = function smartBtnConfig() {
    return {
      style: smartButtonStyle(),
      onButtonReady: function onButtonReady() {
        setPaypalLoaded(!paypalLoaded);
      },
      onInit: function onInit(data, actions) {
        // Disable the buttons
        actions.disable();
      },
      onClick: function onClick() {
        if (!agree) {
          swal.fire({
            type: "error",
            title: "Please agree with our Terms & Conditions.",
            showConfirmButton: false,
            timer: 3000
          });
        }
      }
    };
  };

  var loadPaypalComponent = function loadPaypalComponent() {
    setPaypalLoaded(!paypalLoaded);
    window.paypal.Buttons(smartBtnConfig()).render("#paypal-dummy-button-container");
  };

  Object(react__WEBPACK_IMPORTED_MODULE_1__["useEffect"])(function () {
    appendScriptToBody(loadPaypalComponent);
  }, []);
  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    id: "paypal-dummy-button-container",
    className: "rounded-lg mx-3"
  });
};

/***/ }),

/***/ "./resources/js/components/PlayButton.js":
/*!***********************************************!*\
  !*** ./resources/js/components/PlayButton.js ***!
  \***********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _fortawesome_react_fontawesome__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @fortawesome/react-fontawesome */ "./node_modules/@fortawesome/react-fontawesome/index.es.js");
function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

function _objectWithoutProperties(source, excluded) { if (source == null) return {}; var target = _objectWithoutPropertiesLoose(source, excluded); var key, i; if (Object.getOwnPropertySymbols) { var sourceSymbolKeys = Object.getOwnPropertySymbols(source); for (i = 0; i < sourceSymbolKeys.length; i++) { key = sourceSymbolKeys[i]; if (excluded.indexOf(key) >= 0) continue; if (!Object.prototype.propertyIsEnumerable.call(source, key)) continue; target[key] = source[key]; } } return target; }

function _objectWithoutPropertiesLoose(source, excluded) { if (source == null) return {}; var target = {}; var sourceKeys = Object.keys(source); var key, i; for (i = 0; i < sourceKeys.length; i++) { key = sourceKeys[i]; if (excluded.indexOf(key) >= 0) continue; target[key] = source[key]; } return target; }




var PlayButton = function PlayButton(_ref) {
  var onClick = _ref.onClick,
      className = _ref.className,
      _ref$size = _ref.size,
      size = _ref$size === void 0 ? "2x" : _ref$size,
      rest = _objectWithoutProperties(_ref, ["onClick", "className", "size"]);

  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("button", _extends({
    className: "\n        w-24 h-24 border-2 rounded-full mx-auto flex items-center justify-center cursor-pointer transform hover:scale-110 transition-all duration-300\n        ".concat(className, "\n      "),
    style: {
      background: "linear-gradient(rgba(109, 109, 109, 0.5), rgba(51, 51, 51, 0.8))"
    },
    onClick: onClick
  }, rest), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_fortawesome_react_fontawesome__WEBPACK_IMPORTED_MODULE_1__["FontAwesomeIcon"], {
    icon: "play",
    size: size
  }));
};

/* harmony default export */ __webpack_exports__["default"] = (PlayButton);

/***/ }),

/***/ "./resources/js/components/PurchaserDetails.js":
/*!*****************************************************!*\
  !*** ./resources/js/components/PurchaserDetails.js ***!
  \*****************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime/regenerator */ "./node_modules/@babel/runtime/regenerator/index.js");
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _layouts_users_UserLayout__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../layouts/users/UserLayout */ "./resources/js/layouts/users/UserLayout.js");
/* harmony import */ var _states_userGlobal__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../states/userGlobal */ "./resources/js/states/userGlobal.js");
/* harmony import */ var _helpers_bootstrap__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../helpers/bootstrap */ "./resources/js/helpers/bootstrap.js");
/* harmony import */ var _base__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./_base */ "./resources/js/components/_base/index.js");
/* harmony import */ var _base__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(_base__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var _fortawesome_react_fontawesome__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @fortawesome/react-fontawesome */ "./node_modules/@fortawesome/react-fontawesome/index.es.js");
/* harmony import */ var _material_ui_core_Collapse__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @material-ui/core/Collapse */ "./node_modules/@material-ui/core/esm/Collapse/index.js");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! sweetalert2 */ "./node_modules/sweetalert2/dist/sweetalert2.all.js");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_8___default = /*#__PURE__*/__webpack_require__.n(sweetalert2__WEBPACK_IMPORTED_MODULE_8__);
/* harmony import */ var react_select__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! react-select */ "./node_modules/react-select/dist/react-select.browser.esm.js");
/* harmony import */ var _helpers_countries__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ../helpers/countries */ "./resources/js/helpers/countries.js");
/* harmony import */ var _helpers_propertyHelper_propertyHelper__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ../helpers/propertyHelper/propertyHelper */ "./resources/js/helpers/propertyHelper/propertyHelper.js");
/* harmony import */ var uuid__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! uuid */ "./node_modules/uuid/dist/esm-browser/index.js");
/* harmony import */ var lodash_isEmpty__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! lodash/isEmpty */ "./node_modules/lodash/isEmpty.js");
/* harmony import */ var lodash_isEmpty__WEBPACK_IMPORTED_MODULE_13___default = /*#__PURE__*/__webpack_require__.n(lodash_isEmpty__WEBPACK_IMPORTED_MODULE_13__);
/* harmony import */ var hookrouter__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! hookrouter */ "./node_modules/hookrouter/dist/index.js");
/* harmony import */ var hookrouter__WEBPACK_IMPORTED_MODULE_14___default = /*#__PURE__*/__webpack_require__.n(hookrouter__WEBPACK_IMPORTED_MODULE_14__);
/* harmony import */ var react_device_detect__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! react-device-detect */ "./node_modules/react-device-detect/main.js");
/* harmony import */ var react_device_detect__WEBPACK_IMPORTED_MODULE_15___default = /*#__PURE__*/__webpack_require__.n(react_device_detect__WEBPACK_IMPORTED_MODULE_15__);
/* harmony import */ var _components_base_alerts_sweetAlert__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! ../components/_base/alerts/sweetAlert */ "./resources/js/components/_base/alerts/sweetAlert.js");
/* harmony import */ var _helpers_numberInput__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! ../helpers/numberInput */ "./resources/js/helpers/numberInput.js");


function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

function _toConsumableArray(arr) { return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _unsupportedIterableToArray(arr) || _nonIterableSpread(); }

function _nonIterableSpread() { throw new TypeError("Invalid attempt to spread non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _iterableToArray(iter) { if (typeof Symbol !== "undefined" && Symbol.iterator in Object(iter)) return Array.from(iter); }

function _arrayWithoutHoles(arr) { if (Array.isArray(arr)) return _arrayLikeToArray(arr); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }



















var PurchaserDetails = function PurchaserDetails(_ref) {
  var purchasers = _ref.purchasers,
      setPurchasers = _ref.setPurchasers,
      errors = _ref.errors,
      loading = _ref.loading,
      _ref$showHeader = _ref.showHeader,
      showHeader = _ref$showHeader === void 0 ? true : _ref$showHeader,
      _ref$showLink = _ref.showLink,
      showLink = _ref$showLink === void 0 ? true : _ref$showLink,
      _ref$width = _ref.width,
      width = _ref$width === void 0 ? "w-full" : _ref$width;
  var defaultPurchaser = {
    uid: Object(uuid__WEBPACK_IMPORTED_MODULE_12__["v4"])(),
    id: "",
    title: "",
    first_name: "",
    last_name: "",
    suburb: "",
    address_line_1: "",
    postcode: "",
    state: "",
    country: "",
    phone: "",
    alternate_phone: "",
    email: ""
  };

  var handleAddPeople = function handleAddPeople() {
    if (purchasers.length == 4) return Object(_components_base_alerts_sweetAlert__WEBPACK_IMPORTED_MODULE_16__["sweetAlert"])("error", "You can only add a maximum of 4 person.");
    setPurchasers([].concat(_toConsumableArray(purchasers), [defaultPurchaser]));
  };

  Object(react__WEBPACK_IMPORTED_MODULE_1__["useEffect"])(function () {}, [purchasers]);

  var deletePerson = /*#__PURE__*/function () {
    var _ref2 = _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee(id) {
      return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee$(_context) {
        while (1) {
          switch (_context.prev = _context.next) {
            case 0:
              _context.next = 2;
              return sweetalert2__WEBPACK_IMPORTED_MODULE_8___default.a.fire({
                title: "Are you sure you want to delete the person?",
                text: "The person will be deleted permanently.",
                type: "warning",
                showCancelButton: true,
                confirmButtonText: "Yes!",
                cancelButtonText: "No"
              }).then(function (result) {
                if (result.value) {
                  _helpers_bootstrap__WEBPACK_IMPORTED_MODULE_4__["axios"]["delete"]("/api/sales-advice/".concat(id)).then(function (res) {
                    if (res.status == 200) {
                      Object(_components_base_alerts_sweetAlert__WEBPACK_IMPORTED_MODULE_16__["sweetAlert"])("success", "The person details has been deleted.");
                    }
                  });
                }
              });

            case 2:
            case "end":
              return _context.stop();
          }
        }
      }, _callee);
    }));

    return function deletePerson(_x) {
      return _ref2.apply(this, arguments);
    };
  }();

  var handleRemovePeople = function handleRemovePeople(person) {
    if (purchasers.length == 1 && person.id == null) return;

    if (purchasers.length == 1 && person.id) {
      deletePerson(person.id);
      setPurchasers.apply(void 0, [defaultPurchaser]);
    } else {
      var result = [];

      if (person.id) {
        deletePerson(person.id);
        result = purchasers.filter(function (p) {
          return p.id != person.id;
        });
      } else {
        result = purchasers.filter(function (p) {
          return p.uid != person.uid;
        });
      }

      setPurchasers(result);
    }
  };

  var handleChange = function handleChange(index, fieldName, type, e) {
    var arr = purchasers;

    for (var i = 0; i < arr.length; i++) {
      if (i == index && type === "select") {
        arr[i][fieldName] = e.value;
      }

      if (i == index && type === "text") {
        arr[i][fieldName] = e.target.value;
      }
    }

    setPurchasers(_toConsumableArray(arr));
  };

  var titles = [{
    label: "Please select",
    value: ""
  }, {
    value: "Mr.",
    label: "Mr."
  }, {
    value: "Ms.",
    label: "Ms."
  }, {
    value: "Mrs.",
    label: "Mrs."
  }];

  var renderPurchasers = function renderPurchasers() {
    return purchasers.map(function (data, index) {
      return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
        className: "flex justify-center ".concat(index > 0 ? "mt-4" : "mt-0", " mb-8 ").concat(width, " lg:pr-10"),
        key: index
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
        className: "border lg:mt-0 mt-8 px-6 rounded w-full"
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
        className: "flex mb-5 mt-8"
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
        className: "font-bold text-base"
      }, "Purchaser(s) details"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
        className: "flex-1  text-base text-right"
      }, "NB: Full name/s required for contract purposes")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("h1", {
        className: "font-bold pb-6 text-base"
      }, "Person ", index + 1), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("form", {
        key: data.uid
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
        className: "mb-5"
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
        className: "mb-3 text-base flex"
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("label", {
        className: "mr-3 w-32 mt-2 capitalize pr-3"
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
        className: "font-bold"
      }, "Title"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
        className: "text-red-500"
      }, " *")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(react_select__WEBPACK_IMPORTED_MODULE_9__["default"], {
        isOptionSelected: true,
        name: "title",
        classNamePrefix: "input-select",
        onChange: function onChange(e) {
          return handleChange(index, "title", "select", e);
        },
        defaultValue: titles.find(function (t) {
          return t.value === data.title;
        }) || {
          label: "Please select",
          value: ""
        },
        className: "w-full",
        placeholder: "Select Title",
        options: titles
      })), errors.length > 0 && errors[index].title && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
        className: "ml-32 text-red-500 text-xs"
      }, errors[index].title)), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
        className: "mb-5"
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
        className: "mb-3 text-base flex"
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("label", {
        className: "mr-3 w-32 mt-2 capitalize pr-3"
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
        className: "font-bold"
      }, "First Name"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
        className: "text-red-500"
      }, " *")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_base__WEBPACK_IMPORTED_MODULE_5__["TextInput"], {
        type: "text",
        step: "any",
        className: "w-full rounded",
        name: "first_name",
        autoComplete: "off",
        placeholder: "Enter First Name",
        border: false,
        onChange: function onChange(e) {
          return handleChange(index, "first_name", "text", e);
        },
        appearance: true,
        defaultValue: data.first_name || ""
      })), errors.length > 0 && errors[index].first_name && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
        className: "ml-32 text-red-500 text-xs"
      }, errors[index].first_name)), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
        className: "mb-5"
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
        className: "mb-3 text-base flex"
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("label", {
        className: "mr-3 w-32 mt-2 capitalize pr-3"
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
        className: "font-bold"
      }, "Last Name"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
        className: "text-red-500"
      }, " *")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_base__WEBPACK_IMPORTED_MODULE_5__["TextInput"], {
        type: "text",
        step: "any",
        className: "w-full rounded",
        name: "last_name",
        autoComplete: "off",
        placeholder: "Enter Last Name",
        border: false,
        appearance: true,
        onChange: function onChange(e) {
          return handleChange(index, "last_name", "text", e);
        },
        defaultValue: data.last_name || ""
      })), errors.length > 0 && errors[index].last_name && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
        className: "ml-32 text-red-500 text-xs"
      }, errors[index].last_name)), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
        className: "mb-5"
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
        className: "mb-3 text-base flex"
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("label", {
        className: "mr-3 w-32 mt-2 capitalize pr-3"
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
        className: "font-bold"
      }, "Address"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
        className: "text-red-500"
      }, " *")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_base__WEBPACK_IMPORTED_MODULE_5__["TextInput"], {
        type: "text",
        step: "any",
        className: "w-full rounded",
        name: "address_line_1",
        autoComplete: "off",
        placeholder: "Enter Address Line 1",
        border: false,
        appearance: true,
        onChange: function onChange(e) {
          return handleChange(index, "address_line_1", "text", e);
        },
        defaultValue: data.address_line_1 || ""
      })), errors.length > 0 && errors[index].address_line_1 && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
        className: "ml-32 text-red-500 text-xs"
      }, errors[index].address_line_1)), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
        className: "mb-5"
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
        className: "mb-3 text-base flex"
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("label", {
        className: "mr-3 w-32 mt-2 capitalize pr-3"
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
        className: "font-bold"
      }, "Suburb"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
        className: "text-red-500"
      }, " *")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_base__WEBPACK_IMPORTED_MODULE_5__["TextInput"], {
        type: "text",
        step: "any",
        className: "w-full rounded",
        name: "suburb",
        autoComplete: "off",
        placeholder: "Enter Suburb",
        border: false,
        appearance: true,
        onChange: function onChange(e) {
          return handleChange(index, "suburb", "text", e);
        },
        defaultValue: data.suburb || ""
      })), errors.length > 0 && errors[index].suburb && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
        className: "ml-32 text-red-500 text-xs"
      }, errors[index].suburb)), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
        className: "mb-5"
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
        className: "mb-3 text-base flex"
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("label", {
        className: "mr-3 w-32 mt-2 capitalize pr-3"
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
        className: "font-bold"
      }, "Postcode"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
        className: "text-red-500"
      }, " *")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_base__WEBPACK_IMPORTED_MODULE_5__["TextInput"], {
        type: "text",
        step: "any",
        className: "w-full rounded",
        name: "postcode",
        autoComplete: "off",
        placeholder: "Enter Postcode",
        border: false,
        appearance: true,
        onChange: function onChange(e) {
          return handleChange(index, "postcode", "text", e);
        },
        defaultValue: data.postcode || ""
      })), errors.length > 0 && errors[index].postcode && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
        className: "ml-32 text-red-500 text-xs"
      }, errors[index].postcode)), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
        className: "mb-5"
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
        className: "mb-3 text-base flex"
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("label", {
        className: "mr-3 w-32 mt-2 capitalize pr-3"
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
        className: "font-bold"
      }, "State"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
        className: "text-red-500"
      }, " *")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(react_select__WEBPACK_IMPORTED_MODULE_9__["default"], {
        isOptionSelected: true,
        name: "state",
        classNamePrefix: "input-select",
        onChange: function onChange(e) {
          return handleChange(index, "state", "select", e);
        },
        defaultValue: _helpers_propertyHelper_propertyHelper__WEBPACK_IMPORTED_MODULE_11__["states"].find(function (t) {
          return t.value === data.state;
        }) || {
          label: "Please select",
          value: ""
        },
        className: "w-full",
        placeholder: "Select State",
        options: _helpers_propertyHelper_propertyHelper__WEBPACK_IMPORTED_MODULE_11__["states"]
      })), errors.length > 0 && errors[index].state && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
        className: "ml-32 text-red-500 text-xs"
      }, errors[index].state)), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
        className: "mb-5"
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
        className: "mb-3 text-base flex"
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("label", {
        className: "mr-3 w-32 mt-2 capitalize pr-3"
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
        className: "font-bold"
      }, "Country"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
        className: "text-red-500"
      }, " *")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(react_select__WEBPACK_IMPORTED_MODULE_9__["default"], {
        isOptionSelected: true,
        name: "country",
        classNamePrefix: "input-select",
        onChange: function onChange(e) {
          return handleChange(index, "country", "select", e);
        },
        defaultValue: _helpers_countries__WEBPACK_IMPORTED_MODULE_10__["countries"].find(function (c) {
          return c.value === data.country;
        }) || {
          label: "Please select",
          value: ""
        },
        className: "w-full",
        placeholder: "Select Country",
        options: _helpers_countries__WEBPACK_IMPORTED_MODULE_10__["countries"]
      })), errors.length > 0 && errors[index].country && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
        className: "ml-32 text-red-500 text-xs"
      }, errors[index].country)), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
        className: "mb-5"
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
        className: "mb-3 text-base flex"
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("label", {
        className: "mr-3 w-32 mt-2 capitalize pr-3"
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
        className: "font-bold"
      }, "Phone"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
        className: "text-red-500"
      }, " *")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
        className: "ml-2 px-4 bg-gray-200 flex items-center justify-center rounded-l border-l border-t border-b border-gray-400"
      }, "+61"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_base__WEBPACK_IMPORTED_MODULE_5__["TextInput"], {
        type: "number",
        step: "any",
        className: "w-full rounded border-l-0 rounded-l-none" // name={`phone`}
        // value={data && data.phone}
        ,
        defaultValue: data.phone || "",
        autoComplete: "off",
        placeholder: "Enter Phone",
        border: false,
        appearance: true,
        maxLength: 9,
        minLength: 9,
        onChange: function onChange(e) {
          return handleChange(index, "phone", "text", e);
        },
        onKeyDown: function onKeyDown(evt) {
          return (evt.key === "e" || evt.key === "." || evt.key === "-") && evt.preventDefault();
        }
      })), errors.length > 0 && errors[index].phone && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
        className: "ml-32 text-red-500 text-xs"
      }, errors[index].phone)), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
        className: "mb-5"
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
        className: "mb-3 text-base flex"
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("label", {
        className: "mr-3 w-32 mt-2 capitalize pr-3"
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
        className: "font-bold"
      }, "Alternate")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
        className: "ml-2 px-4 bg-gray-200 flex items-center justify-center rounded-l border-l border-t border-b border-gray-400"
      }, "+61"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_base__WEBPACK_IMPORTED_MODULE_5__["TextInput"], {
        type: "number",
        step: "any",
        className: "w-full rounded border-l-0 rounded-l-none",
        name: "alternate_phone",
        autoComplete: "off",
        placeholder: "Enter Alternate Phone",
        border: false,
        appearance: true,
        maxLength: 9,
        minLength: 9,
        onChange: function onChange(e) {
          return handleChange(index, "alternate_phone", "text", e);
        },
        defaultValue: data.alternate_phone,
        onKeyDown: function onKeyDown(evt) {
          return (evt.key === "e" || evt.key === "." || evt.key === "-") && evt.preventDefault();
        }
      })), errors.length > 0 && errors[index].alternate_phone && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
        className: "ml-32 text-red-500 text-xs"
      }, errors[index].alternate_phone)), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
        className: "mb-5"
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
        className: "mb-3 text-base flex"
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("label", {
        className: "mr-3 w-32 mt-2 capitalize pr-3"
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
        className: "font-bold"
      }, "Email"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
        className: "text-red-500"
      }, " *")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_base__WEBPACK_IMPORTED_MODULE_5__["TextInput"], {
        type: "text",
        step: "any",
        className: "w-full rounded",
        name: "email",
        autoComplete: "off",
        placeholder: "Enter Email",
        border: false,
        appearance: true,
        onChange: function onChange(e) {
          return handleChange(index, "email", "text", e);
        },
        defaultValue: data.email || ""
      })), errors.length > 0 && errors[index].email && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
        className: "ml-32 text-red-500 text-xs"
      }, errors[index].email))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
        className: "flex ml-20 mb-6 justify-end"
      }, purchasers.length > 1 && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_base__WEBPACK_IMPORTED_MODULE_5__["Button"], {
        className: "bg-red-600 button button-primary font-bold mr-2 rounded-full h-10",
        disabled: loading,
        onClick: function onClick() {
          return handleRemovePeople(data);
        }
      }, "Remove"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_base__WEBPACK_IMPORTED_MODULE_5__["Button"], {
        className: "font-bold rounded-full bg-blue-400 h-10",
        disabled: loading,
        onClick: function onClick() {
          return handleAddPeople();
        }
      }, "Add"))));
    });
  };

  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("section", {
    className: "relative mt-6 rounded"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "mx-auto",
    style: {
      maxWidth: 1366
    }
  }, showHeader && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("h1", {
    className: "font-bold leading-tight pt-10 text-3xl mb-4"
  }, "Sales Advice"), showLink ? /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("p", {
    className: "text-base"
  }, "Please complete the below sales advice information.", /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("br", null), " You can complete this later under", /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("a", {
    className: "hover:text-palette-blue text-palette-blue-light",
    href: "/profile/secured-deals"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
    className: "font-bold"
  }, "\xA0'Secured deals'."))) : /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("p", {
    className: "text-base"
  }, "Please complete the below sales advice information."), renderPurchasers()));
};

/* harmony default export */ __webpack_exports__["default"] = (PurchaserDetails);

/***/ }),

/***/ "./resources/js/components/ReadyForOccupancy.js":
/*!******************************************************!*\
  !*** ./resources/js/components/ReadyForOccupancy.js ***!
  \******************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);


var ReadyForOccupancy = function ReadyForOccupancy() {
  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "flex font-bold h-8 items-center justify-center mb-2 mr-1 mr-2 mt-6 rounded-lg shadow-lg text-sm xs:w-2/3",
    style: {
      backgroundColor: "rgba(125, 196, 61, 0.2)"
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("svg", {
    className: "feather-icon h-5 rounded-full p-1 text-white w-5",
    style: {
      backgroundColor: "rgb(137, 176, 54)"
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("use", {
    xlinkHref: "/assets/svg/feather-sprite.svg#check"
  })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
    className: "font-bold pl-2 text-gray-900 text-sm"
  }, "Ready for Occupancy"));
};

/* harmony default export */ __webpack_exports__["default"] = (ReadyForOccupancy);

/***/ }),

/***/ "./resources/js/components/ResourcesComponent.js":
/*!*******************************************************!*\
  !*** ./resources/js/components/ResourcesComponent.js ***!
  \*******************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _components_base_alerts_sweetAlert__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../components/_base/alerts/sweetAlert */ "./resources/js/components/_base/alerts/sweetAlert.js");
/* harmony import */ var _helpers_resourceHelper_index__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../helpers/resourceHelper/index */ "./resources/js/helpers/resourceHelper/index.js");
/* harmony import */ var _MediaPlayerContainer__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./MediaPlayerContainer */ "./resources/js/components/MediaPlayerContainer.js");
/* harmony import */ var react_device_detect__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! react-device-detect */ "./node_modules/react-device-detect/main.js");
/* harmony import */ var react_device_detect__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(react_device_detect__WEBPACK_IMPORTED_MODULE_4__);
function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _unsupportedIterableToArray(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

function _iterableToArrayLimit(arr, i) { if (typeof Symbol === "undefined" || !(Symbol.iterator in Object(arr))) return; var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }







var ResourcesComponent = function ResourcesComponent(_ref) {
  var project = _ref.project,
      _ref$floorPlan = _ref.floorPlan,
      floorPlan = _ref$floorPlan === void 0 ? null : _ref$floorPlan;

  var _useState = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])([]),
      _useState2 = _slicedToArray(_useState, 2),
      resourceData = _useState2[0],
      setResourceData = _useState2[1];

  var _useState3 = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])(false),
      _useState4 = _slicedToArray(_useState3, 2),
      toogleShowPlayer = _useState4[0],
      setToogleShowPlayerModal = _useState4[1];

  Object(react__WEBPACK_IMPORTED_MODULE_0__["useEffect"])(function () {
    mapURLLinks();
  }, [resourceData]);

  var mapURLLinks = function mapURLLinks() {
    var resourceArr = project.resources;

    if (project.architect_profile_link) {
      resourceArr.push({
        file: project.architect_profile_link,
        name: "architect_profile"
      });
    }

    if (project.developer_profile_link) {
      resourceArr.push({
        file: project.developer_profile_link,
        name: "developer_profile"
      });
    }

    if (project.builder_profile_link) {
      resourceArr.push({
        file: project.builder_profile_link,
        name: "builder_profile"
      });
    }

    if (project.video_link) {
      resourceArr.push({
        file: project.video_link,
        name: "project_video"
      });
    }

    if (floorPlan) {
      resourceArr.push(floorPlan);
    }

    setResourceData(resourceArr);
  };

  var toogleShowPlayerModal = function toogleShowPlayerModal() {
    setToogleShowPlayerModal(!toogleShowPlayer);
  };

  var renderResources = function renderResources() {
    var dataArr = _helpers_resourceHelper_index__WEBPACK_IMPORTED_MODULE_2__["resources"].resourcesArr(resourceData);
    return dataArr.map(function (resource, key) {
      return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("li", {
        key: key,
        className: "mb-3"
      }, resource.name && resource.link && resource.name !== "project_video" && linkContainer(resource), resource.name === "project_video" && resource.link && videoContainer(resource));
    });
  };

  var linkContainer = function linkContainer(resource) {
    return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("a", {
      href: resource.link.file,
      target: "_blank",
      className: "bg-gray-200 block font-bold px-3 py-1 leading-relaxed text-base rounded text-palette-blue-light ".concat(react_device_detect__WEBPACK_IMPORTED_MODULE_4__["isMobile"] ? "text-center" : "text-left")
    }, resource.label);
  };

  var videoContainer = function videoContainer(resource) {
    return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("a", {
      onClick: function onClick(e) {
        e.preventDefault(), toogleShowPlayerModal();
      },
      target: "_blank",
      className: "bg-gray-200 block cursor-pointer font-bold leading-relaxed px-3 py-1 rounded text-base text-palette-blue-light ".concat(react_device_detect__WEBPACK_IMPORTED_MODULE_4__["isMobile"] ? "text-center" : "text-left")
    }, resource.label);
  };

  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", null, project.video_link && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_MediaPlayerContainer__WEBPACK_IMPORTED_MODULE_3__["default"], {
    url: project.video_link,
    toogleShowPlayerModal: toogleShowPlayerModal,
    show: toogleShowPlayer,
    height: "75%"
  }), renderResources());
};

/* harmony default export */ __webpack_exports__["default"] = (ResourcesComponent);

/***/ }),

/***/ "./resources/js/components/SalesAdviceModal.js":
/*!*****************************************************!*\
  !*** ./resources/js/components/SalesAdviceModal.js ***!
  \*****************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _material_ui_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @material-ui/core */ "./node_modules/@material-ui/core/esm/index.js");
/* harmony import */ var uuid__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! uuid */ "./node_modules/uuid/dist/esm-browser/index.js");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! sweetalert2 */ "./node_modules/sweetalert2/dist/sweetalert2.all.js");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(sweetalert2__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var _services_auth__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../services/auth */ "./resources/js/services/auth.js");
/* harmony import */ var _PurchaserDetails__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./PurchaserDetails */ "./resources/js/components/PurchaserDetails.js");
/* harmony import */ var _SalesAdvicePropertyDetails__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./SalesAdvicePropertyDetails */ "./resources/js/components/SalesAdvicePropertyDetails.js");
/* harmony import */ var _AdditionalInfos__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./AdditionalInfos */ "./resources/js/components/AdditionalInfos.js");
/* harmony import */ var _SolicitorDetails__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./SolicitorDetails */ "./resources/js/components/SolicitorDetails.js");
/* harmony import */ var _helpers_salesAdviceHelper_salesAdviceHelper__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../helpers/salesAdviceHelper/salesAdviceHelper */ "./resources/js/helpers/salesAdviceHelper/salesAdviceHelper.js");
/* harmony import */ var lodash_isEmpty__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! lodash/isEmpty */ "./node_modules/lodash/isEmpty.js");
/* harmony import */ var lodash_isEmpty__WEBPACK_IMPORTED_MODULE_10___default = /*#__PURE__*/__webpack_require__.n(lodash_isEmpty__WEBPACK_IMPORTED_MODULE_10__);
/* harmony import */ var lodash_fromPairs__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! lodash/fromPairs */ "./node_modules/lodash/fromPairs.js");
/* harmony import */ var lodash_fromPairs__WEBPACK_IMPORTED_MODULE_11___default = /*#__PURE__*/__webpack_require__.n(lodash_fromPairs__WEBPACK_IMPORTED_MODULE_11__);
/* harmony import */ var _helpers_pricingHelper_discountHelper__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ../helpers/pricingHelper/discountHelper */ "./resources/js/helpers/pricingHelper/discountHelper.js");
/* harmony import */ var _material_ui_icons_Close__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! @material-ui/icons/Close */ "./node_modules/@material-ui/icons/Close.js");
/* harmony import */ var _material_ui_icons_Close__WEBPACK_IMPORTED_MODULE_13___default = /*#__PURE__*/__webpack_require__.n(_material_ui_icons_Close__WEBPACK_IMPORTED_MODULE_13__);
/* harmony import */ var _states_userGlobal__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ../states/userGlobal */ "./resources/js/states/userGlobal.js");
/* harmony import */ var react_device_detect__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! react-device-detect */ "./node_modules/react-device-detect/main.js");
/* harmony import */ var react_device_detect__WEBPACK_IMPORTED_MODULE_15___default = /*#__PURE__*/__webpack_require__.n(react_device_detect__WEBPACK_IMPORTED_MODULE_15__);
/* harmony import */ var _base__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! ./_base */ "./resources/js/components/_base/index.js");
/* harmony import */ var _base__WEBPACK_IMPORTED_MODULE_16___default = /*#__PURE__*/__webpack_require__.n(_base__WEBPACK_IMPORTED_MODULE_16__);
/* harmony import */ var _fortawesome_react_fontawesome__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! @fortawesome/react-fontawesome */ "./node_modules/@fortawesome/react-fontawesome/index.es.js");
/* harmony import */ var _base_alerts_sweetAlert__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(/*! ./_base/alerts/sweetAlert */ "./resources/js/components/_base/alerts/sweetAlert.js");
function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _unsupportedIterableToArray(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

function _iterableToArrayLimit(arr, i) { if (typeof Symbol === "undefined" || !(Symbol.iterator in Object(arr))) return; var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }





















var SalesAdviceModal = function SalesAdviceModal(_ref) {
  var _React$createElement;

  var _onClose = _ref.onClose,
      purchasersParam = _ref.purchasersParam,
      _ref$show = _ref.show,
      show = _ref$show === void 0 ? false : _ref$show,
      apartment = _ref.apartment,
      deal = _ref.deal,
      _ref$allowOutsideInpu = _ref.allowOutsideInput,
      allowOutsideInput = _ref$allowOutsideInpu === void 0 ? false : _ref$allowOutsideInpu,
      _ref$maxWidth = _ref.maxWidth,
      maxWidth = _ref$maxWidth === void 0 ? "md" : _ref$maxWidth;

  var _useState = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])(false),
      _useState2 = _slicedToArray(_useState, 2),
      loading = _useState2[0],
      setLoading = _useState2[1];

  var _useState3 = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])([]),
      _useState4 = _slicedToArray(_useState3, 2),
      purchaserErrors = _useState4[0],
      setPurchaserErrors = _useState4[1];

  var _useState5 = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])({}),
      _useState6 = _slicedToArray(_useState5, 2),
      solicitorErrors = _useState6[0],
      setSolicitorErrors = _useState6[1];

  var _useState7 = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])({}),
      _useState8 = _slicedToArray(_useState7, 2),
      additionalInfoErrors = _useState8[0],
      setAddtionalInfoErrors = _useState8[1];

  var _useState9 = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])(purchasersParam.length > 0 ? purchasersParam : _helpers_salesAdviceHelper_salesAdviceHelper__WEBPACK_IMPORTED_MODULE_9__["defaultPurchasers"]),
      _useState10 = _slicedToArray(_useState9, 2),
      purchasers = _useState10[0],
      setPurchasers = _useState10[1];

  var solicitorForm = Object(react__WEBPACK_IMPORTED_MODULE_0__["createRef"])();
  var additionalInfoForm = Object(react__WEBPACK_IMPORTED_MODULE_0__["createRef"])();
  Object(react__WEBPACK_IMPORTED_MODULE_0__["useEffect"])(function () {}, []);

  var mapDeal = function mapDeal() {
    return new Object({
      name: deal.deal_name,
      address: {
        line_1: deal.property_address.line_1,
        suburbs: deal.property_address.suburbs,
        state: deal.property_address.state,
        postal: deal.property_address.postal
      }
    });
  };

  var handleSubmit = function handleSubmit(isSubmit) {
    var solicitorFormData = new FormData(solicitorForm.current);
    var additionalInfoFormData = new FormData(additionalInfoForm.current);
    setLoading(true);
    var purchaserArr = [];
    var solicitorAddress = {
      suburb: solicitorFormData.get("suburb"),
      state: solicitorFormData.get("state"),
      country: solicitorFormData.get("country"),
      postcode: solicitorFormData.get("postcode")
    };
    solicitorFormData.append("propertyId", apartment.id);
    solicitorFormData.append("address", JSON.stringify(solicitorAddress));
    var phone = solicitorFormData.get("phone") ? "+61" + solicitorFormData.get("phone") : "";
    solicitorFormData.set("phone", phone);
    purchasers.forEach(function (purchaser) {
      var data = {};
      data.address = {
        address_line_1: purchaser.address_line_1,
        suburb: purchaser.suburb,
        country: purchaser.country,
        state: purchaser.state,
        postcode: purchaser.postcode
      };
      data.address_line_1 = purchaser.address_line_1;
      data.suburb = purchaser.suburb;
      data.country = purchaser.country;
      data.state = purchaser.state;
      data.title = purchaser.title;
      data.postcode = purchaser.postcode;
      data.phone = purchaser["phone"] ? "+61" + purchaser["phone"] : "";
      data.alternate_phone = purchaser["alternate_phone"] ? "+61" + purchaser["alternate_phone"] : "";
      data.email = purchaser["email"];
      data.first_name = purchaser["first_name"];
      data.last_name = purchaser["last_name"];
      data.propertyId = purchaser["propertyId"];
      data.id = purchaser["id"];
      purchaserArr.push(data);
    });
    var param = {
      propertyId: apartment.id,
      purchasers: purchaserArr,
      solicitor: Object(lodash_fromPairs__WEBPACK_IMPORTED_MODULE_11__["fromPairs"])(Array.from(solicitorFormData.entries())),
      additionalInfo: Object(lodash_fromPairs__WEBPACK_IMPORTED_MODULE_11__["fromPairs"])(Array.from(additionalInfoFormData.entries())),
      isSubmit: isSubmit
    };

    try {
      axios.post("/api/sales-advice", param).then(function (res) {
        setLoading(false);

        if (res.status === 200 && !res.data.errorCode) {
          setPurchasers(Object(_helpers_salesAdviceHelper_salesAdviceHelper__WEBPACK_IMPORTED_MODULE_9__["purchasersMapper"])(res.data[0].original.purchasers));
          setSolicitorErrors([]);
          setPurchaserErrors([]);
          Object(_base_alerts_sweetAlert__WEBPACK_IMPORTED_MODULE_18__["sweetAlert"])("success", "Sales advice successfully saved.");
        }

        if (res.data.errorCode === 422) {
          setLoading(false);

          if (!Object(lodash_isEmpty__WEBPACK_IMPORTED_MODULE_10__["isEmpty"])(res.data.solicitorValidator)) {
            setSolicitorErrors(res.data.solicitorValidator);
          }

          if (!Object(lodash_isEmpty__WEBPACK_IMPORTED_MODULE_10__["isEmpty"])(res.data.purchaserValidator)) {
            setPurchaserErrors(res.data.purchaserValidator);
          }
        }
      });
    } catch (error) {
      setLoading(false);
    }
  };

  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_material_ui_core__WEBPACK_IMPORTED_MODULE_1__["Dialog"] // TransitionComponent={Transition}
  , (_React$createElement = {
    transitionDuration: 400,
    maxWidth: "sm",
    keepMounted: true,
    disableEnforceFocus: allowOutsideInput,
    fullScreen: react_device_detect__WEBPACK_IMPORTED_MODULE_15__["isMobile"] ? true : false,
    open: show,
    onClose: function onClose() {
      return _onClose();
    },
    fullWidth: false
  }, _defineProperty(_React$createElement, "maxWidth", react_device_detect__WEBPACK_IMPORTED_MODULE_15__["isMobile"] ? false : maxWidth), _defineProperty(_React$createElement, "scroll", "paper"), _defineProperty(_React$createElement, "PaperProps", {
    style: styles.paperProps
  }), _React$createElement), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_material_ui_core__WEBPACK_IMPORTED_MODULE_1__["DialogTitle"], null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "text-gray-700 font-bold text-4xl lg:text-4xl mt-10 lg:px-2 leading-none"
  }, "Sales Advice"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_material_ui_core__WEBPACK_IMPORTED_MODULE_1__["IconButton"], {
    className: "p-3 text-gray-700",
    onClick: function onClick() {
      return _onClose();
    },
    style: {
      right: 12,
      top: 12,
      position: "absolute"
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_material_ui_icons_Close__WEBPACK_IMPORTED_MODULE_13___default.a, null))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_material_ui_core__WEBPACK_IMPORTED_MODULE_1__["DialogContent"], null, purchasers.length > 0 && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_PurchaserDetails__WEBPACK_IMPORTED_MODULE_5__["default"], {
    showHeader: false,
    setPurchasers: setPurchasers,
    purchasers: purchasers,
    errors: purchaserErrors,
    loading: loading,
    showLink: false
  }), apartment && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_SalesAdvicePropertyDetails__WEBPACK_IMPORTED_MODULE_6__["default"], {
    project: mapDeal(),
    apartment: apartment,
    deal: deal
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_SolicitorDetails__WEBPACK_IMPORTED_MODULE_8__["default"], {
    solicitorErrors: solicitorErrors,
    solicitorForm: solicitorForm
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_AdditionalInfos__WEBPACK_IMPORTED_MODULE_7__["default"], {
    additionalInfoErrors: additionalInfoErrors,
    additionalInfoForm: additionalInfoForm
  })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "flex justify-center lg:justify-end mb-10 mr-6 mt-8"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_base__WEBPACK_IMPORTED_MODULE_16__["Button"], {
    onClick: function onClick() {
      return handleSubmit(false);
    },
    className: "".concat(loading ? "bg-gray-500" : "bg-palette-blue-light", " text-white mr-2 block button button-primary rounded-full"),
    disabled: loading
  }, loading && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_fortawesome_react_fontawesome__WEBPACK_IMPORTED_MODULE_17__["FontAwesomeIcon"], {
    icon: ["fas", "spinner"],
    className: "fa-spin mr-2"
  }), "Save"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_base__WEBPACK_IMPORTED_MODULE_16__["Button"], {
    onClick: _onClose,
    className: "bg-red-600 button button-primary font-bold mr-2 rounded-full h-10",
    disabled: loading
  }, "Cancel"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_base__WEBPACK_IMPORTED_MODULE_16__["Button"], {
    onClick: function onClick() {
      return handleSubmit(true);
    },
    className: "block button button-primary rounded-full",
    disabled: loading
  }, loading && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_fortawesome_react_fontawesome__WEBPACK_IMPORTED_MODULE_17__["FontAwesomeIcon"], {
    icon: ["fas", "spinner"],
    className: "fa-spin mr-2"
  }), "Submit")));
};

var styles = {
  paperProps: {
    backgroundColor: "#fff"
  }
};
/* harmony default export */ __webpack_exports__["default"] = (SalesAdviceModal);

/***/ }),

/***/ "./resources/js/components/SalesAdvicePropertyDetails.js":
/*!***************************************************************!*\
  !*** ./resources/js/components/SalesAdvicePropertyDetails.js ***!
  \***************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var lodash_fromPairs__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! lodash/fromPairs */ "./node_modules/lodash/fromPairs.js");
/* harmony import */ var lodash_fromPairs__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(lodash_fromPairs__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var react_currency_format__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! react-currency-format */ "./node_modules/react-currency-format/lib/currency-format.js");
/* harmony import */ var react_currency_format__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(react_currency_format__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _helpers_pricingHelper_discountHelper__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../helpers/pricingHelper/discountHelper */ "./resources/js/helpers/pricingHelper/discountHelper.js");





var SalesAdvicePropertyDetails = function SalesAdvicePropertyDetails(_ref) {
  var project = _ref.project,
      apartment = _ref.apartment,
      deal = _ref.deal;
  Object(react__WEBPACK_IMPORTED_MODULE_0__["useEffect"])(function () {}, []);
  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("section", {
    className: "relative mt-6 rounded"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "mx-auto",
    style: {
      maxWidth: 1366
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "flex justify-center mt-4 mb-8 lg:w-9/12 lg:pr-10"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "border lg:mt-0 mt-8 px-6 rounded w-full"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("h1", {
    className: "font-bold py-6 text-base"
  }, "Property Details"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "pb-4 flex ml-6"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
    className: "text-base font-bold mr-10 "
  }, "Apartment/Lot:"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "flex flex-col"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
    className: "text-base"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
    className: "font-bold text-base"
  }, "Unit ", apartment && apartment.unit_name, ", ", project.name, ",\xA0"), !Object(lodash_fromPairs__WEBPACK_IMPORTED_MODULE_1__["isEmpty"])(project) && project.address.line_1 ? project.address.line_1 + ", " : "", !Object(lodash_fromPairs__WEBPACK_IMPORTED_MODULE_1__["isEmpty"])(project) && project.address.line_2 ? project.address.line_2 + ", " : "", !Object(lodash_fromPairs__WEBPACK_IMPORTED_MODULE_1__["isEmpty"])(project) && project.address.suburbs ? project.address.suburbs + ", " : "", !Object(lodash_fromPairs__WEBPACK_IMPORTED_MODULE_1__["isEmpty"])(project) && project.address.state ? project.address.state + " " : "", !Object(lodash_fromPairs__WEBPACK_IMPORTED_MODULE_1__["isEmpty"])(project) && project.address.postal ? project.address.postal : ""))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "mb-8"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
    className: "text-base font-bold mr-10 ml-6 "
  }, "Purchase price:"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
    className: "font-bold text-base text-red-700"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_currency_format__WEBPACK_IMPORTED_MODULE_2___default.a, {
    value: deal.property_price - deal.property_price * (deal.discount / 100),
    displayType: "text",
    thousandSeparator: true,
    prefix: "$"
  })))))));
};

/* harmony default export */ __webpack_exports__["default"] = (SalesAdvicePropertyDetails);

/***/ }),

/***/ "./resources/js/components/ScrollArrow.js":
/*!************************************************!*\
  !*** ./resources/js/components/ScrollArrow.js ***!
  \************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _unsupportedIterableToArray(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

function _iterableToArrayLimit(arr, i) { if (typeof Symbol === "undefined" || !(Symbol.iterator in Object(arr))) return; var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }



var ScrollArrow = function ScrollArrow(_ref) {
  var className = _ref.className,
      top = _ref.top,
      icon = _ref.icon;

  var _useState = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])(false),
      _useState2 = _slicedToArray(_useState, 2),
      showScroll = _useState2[0],
      setShowScroll = _useState2[1];

  var checkScrollDirection = function checkScrollDirection() {
    if (top) {
      if (!showScroll && window.pageYOffset > 400) {
        setShowScroll(true);
      } else if (showScroll && window.pageYOffset <= 400) {
        setShowScroll(false);
      }
    } else {
      if (!showScroll && window.pageYOffset < 400) {
        setShowScroll(true);
      } else if (showScroll && window.pageYOffset >= 400) {
        setShowScroll(false);
      }
    }
  };

  var scrollDirection = function scrollDirection() {
    if (top) {
      window.scrollTo({
        top: 0,
        behavior: 'smooth'
      });
    } else {
      window.scrollTo({
        left: 0,
        top: document.documentElement.scrollHeight - document.documentElement.clientHeight,
        behavior: 'smooth'
      });
    }
  };

  window.addEventListener('scroll', checkScrollDirection);
  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: className,
    onClick: scrollDirection
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("svg", {
    className: "feather-icon h-6 text-white w-6"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("use", {
    xlinkHref: "/assets/svg/feather-sprite.svg#".concat(icon)
  })));
};

/* harmony default export */ __webpack_exports__["default"] = (ScrollArrow);

/***/ }),

/***/ "./resources/js/components/ScrollToBottomArrow.js":
/*!********************************************************!*\
  !*** ./resources/js/components/ScrollToBottomArrow.js ***!
  \********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _unsupportedIterableToArray(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

function _iterableToArrayLimit(arr, i) { if (typeof Symbol === "undefined" || !(Symbol.iterator in Object(arr))) return; var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }



var ScrollToBottomArrow = function ScrollToBottomArrow(_ref) {
  var className = _ref.className,
      _ref$top = _ref.top,
      top = _ref$top === void 0 ? true : _ref$top,
      icon = _ref.icon;

  var _useState = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])(false),
      _useState2 = _slicedToArray(_useState, 2),
      showScroll = _useState2[0],
      setShowScroll = _useState2[1];

  var checkScrollBottom = function checkScrollBottom() {
    if (!showScroll && window.pageYOffset < 400) {
      setShowScroll(true);
    } else if (showScroll && window.pageYOffset >= 400) {
      setShowScroll(false);
    }
  };

  var scrollBottom = function scrollBottom() {
    window.scrollTo({
      left: 0,
      top: document.documentElement.scrollHeight - document.documentElement.clientHeight,
      behavior: 'smooth'
    });
  };

  window.addEventListener('scroll', checkScrollBottom);
  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: className,
    onClick: scrollBottom
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("svg", {
    className: "feather-icon h-6 text-white w-6"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("use", {
    xlinkHref: "/assets/svg/feather-sprite.svg#".concat(icon)
  })));
};

/* harmony default export */ __webpack_exports__["default"] = (ScrollToBottomArrow);

/***/ }),

/***/ "./resources/js/components/SecureDeal.js":
/*!***********************************************!*\
  !*** ./resources/js/components/SecureDeal.js ***!
  \***********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime/regenerator */ "./node_modules/@babel/runtime/regenerator/index.js");
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _states_userGlobal__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../states/userGlobal */ "./resources/js/states/userGlobal.js");
/* harmony import */ var _base__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./_base */ "./resources/js/components/_base/index.js");
/* harmony import */ var _base__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(_base__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var _services_auth__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../services/auth */ "./resources/js/services/auth.js");
/* harmony import */ var _fortawesome_react_fontawesome__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @fortawesome/react-fontawesome */ "./node_modules/@fortawesome/react-fontawesome/index.es.js");
/* harmony import */ var pretty_checkbox_react__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! pretty-checkbox-react */ "./node_modules/pretty-checkbox-react/dist/pretty-checkbox-react.umd.min.js");
/* harmony import */ var pretty_checkbox_react__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(pretty_checkbox_react__WEBPACK_IMPORTED_MODULE_6__);
/* harmony import */ var react_currency_format__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! react-currency-format */ "./node_modules/react-currency-format/lib/currency-format.js");
/* harmony import */ var react_currency_format__WEBPACK_IMPORTED_MODULE_7___default = /*#__PURE__*/__webpack_require__.n(react_currency_format__WEBPACK_IMPORTED_MODULE_7__);
/* harmony import */ var lodash_isEmpty__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! lodash/isEmpty */ "./node_modules/lodash/isEmpty.js");
/* harmony import */ var lodash_isEmpty__WEBPACK_IMPORTED_MODULE_8___default = /*#__PURE__*/__webpack_require__.n(lodash_isEmpty__WEBPACK_IMPORTED_MODULE_8__);
/* harmony import */ var _components_base_alerts_sweetAlert__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../components/_base/alerts/sweetAlert */ "./resources/js/components/_base/alerts/sweetAlert.js");
/* harmony import */ var _payment_PaymentConfirmationModal__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./payment/PaymentConfirmationModal */ "./resources/js/components/payment/PaymentConfirmationModal.js");
/* harmony import */ var _helpers_deviceSizeHelper__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ../helpers/deviceSizeHelper */ "./resources/js/helpers/deviceSizeHelper.js");


function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _unsupportedIterableToArray(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

function _iterableToArrayLimit(arr, i) { if (typeof Symbol === "undefined" || !(Symbol.iterator in Object(arr))) return; var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }













var CommBankIntegration = function CommBankIntegration(_ref) {
  var property = _ref.property,
      project = _ref.project,
      handleClose = _ref.handleClose;

  var _useState = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])({}),
      _useState2 = _slicedToArray(_useState, 2),
      errors = _useState2[0],
      setErrors = _useState2[1];

  var _useState3 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(false),
      _useState4 = _slicedToArray(_useState3, 2),
      loading = _useState4[0],
      setLoading = _useState4[1];

  var _UserGlobal = Object(_states_userGlobal__WEBPACK_IMPORTED_MODULE_2__["default"])(),
      _UserGlobal2 = _slicedToArray(_UserGlobal, 2),
      userState = _UserGlobal2[0],
      userAction = _UserGlobal2[1];

  var _useState5 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])({}),
      _useState6 = _slicedToArray(_useState5, 2),
      cardNumber = _useState6[0],
      setCardNumber = _useState6[1];

  var _useState7 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])({}),
      _useState8 = _slicedToArray(_useState7, 2),
      cardCvc = _useState8[0],
      setCardCvc = _useState8[1];

  var _useState9 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])({}),
      _useState10 = _slicedToArray(_useState9, 2),
      cardExpiryYear = _useState10[0],
      setCardExpiryYear = _useState10[1];

  var _useState11 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])({}),
      _useState12 = _slicedToArray(_useState11, 2),
      cardExpiryMonth = _useState12[0],
      setCardExpiryMonth = _useState12[1];

  var _useState13 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(false),
      _useState14 = _slicedToArray(_useState13, 2),
      agree = _useState14[0],
      setAgree = _useState14[1];

  var _useState15 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(false),
      _useState16 = _slicedToArray(_useState15, 2),
      toggleOpen = _useState16[0],
      setToggleOpen = _useState16[1];

  var _useState17 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])({}),
      _useState18 = _slicedToArray(_useState17, 2),
      tokenData = _useState18[0],
      setTokenData = _useState18[1];

  var _useState19 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(""),
      _useState20 = _slicedToArray(_useState19, 2),
      nameOnCard = _useState20[0],
      setNameOnCard = _useState20[1];

  var _useState21 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(null),
      _useState22 = _slicedToArray(_useState21, 2),
      formData = _useState22[0],
      setFormData = _useState22[1];

  var form = Object(react__WEBPACK_IMPORTED_MODULE_1__["createRef"])();

  var createPayment = /*#__PURE__*/function () {
    var _ref2 = _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee(formData) {
      var errors, data;
      return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee$(_context) {
        while (1) {
          switch (_context.prev = _context.next) {
            case 0:
              errors = {};
              _context.prev = 1;
              _context.next = 4;
              return axios.post("/api/create-payment", formData).then(function (res) {
                if (res.status === 200 && res.data.status === "DECLINED" && res.data.statusCode == 400) {
                  if (res.data.errors) {
                    setErrors(res.data.errors);
                  } else {
                    var msg = res.data.declineReason.replace(/_/g, " ");
                    Object(_components_base_alerts_sweetAlert__WEBPACK_IMPORTED_MODULE_9__["sweetAlert"])("error", msg);
                  }

                  setLoading(false);
                }

                if (res.status == 200 && res.data.statusCode == 200 && res.data.status === "APPROVED") {
                  window.location = "/weekly-deals/".concat(project.id, "/apartment/").concat(property.id, "/", false, "/confirm");
                }
              });

            case 4:
              _context.next = 14;
              break;

            case 6:
              _context.prev = 6;
              _context.t0 = _context["catch"](1);
              Object(_components_base_alerts_sweetAlert__WEBPACK_IMPORTED_MODULE_9__["sweetAlert"])("error", "Please check your card details.");
              data = _context.t0.response.data;
              errors = data.errors;
              setLoading(false);
              setErrors(errors || {});
              return _context.abrupt("return");

            case 14:
            case "end":
              return _context.stop();
          }
        }
      }, _callee, null, [[1, 6]]);
    }));

    return function createPayment(_x) {
      return _ref2.apply(this, arguments);
    };
  }();

  var handleSubmit = /*#__PURE__*/function () {
    var _ref3 = _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee2(e) {
      var errors, formData, data;
      return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee2$(_context2) {
        while (1) {
          switch (_context2.prev = _context2.next) {
            case 0:
              e.preventDefault();
              errors = {};
              formData = new FormData(form.current);
              formData.append("property_id", property.id);
              formData.append("expMonth", cardExpiryMonth);
              formData.append("expYear", cardExpiryYear);
              formData.append("cardNumber", cardNumber);
              setFormData(formData);
              setLoading(true);

              if (agree) {
                _context2.next = 13;
                break;
              }

              Object(_components_base_alerts_sweetAlert__WEBPACK_IMPORTED_MODULE_9__["sweetAlert"])("error", "Please agree with our Terms & Conditions.");
              setLoading(false);
              return _context2.abrupt("return");

            case 13:
              _context2.prev = 13;
              _context2.next = 16;
              return axios.post("/api/generate-payment-token", formData, {
                headers: {
                  Authorization: "Bearer " + Object(_services_auth__WEBPACK_IMPORTED_MODULE_4__["isLoggedIn"])()
                }
              }).then(function (res) {
                setLoading(false);

                if (res.status === 200) {
                  if (!lodash_isEmpty__WEBPACK_IMPORTED_MODULE_8___default()(res.data.card) && res.data.card.secure3DData && res.data.card.secure3DData.isEnrolled) {
                    setToggleOpen(!toggleOpen);
                    setTokenData(res);
                  }

                  if (!lodash_isEmpty__WEBPACK_IMPORTED_MODULE_8___default()(res.data.card) && res.data.card.secure3DData && !res.data.card.secure3DData.isEnrolled) {
                    createPayment(formData);
                  }
                }

                if (res.status === 200 && res.data.status === "DECLINED" && res.data.statusCode == 400 || res.data.statusCode == 405) {
                  setLoading(false);

                  if (res.data.statusCode != 405) {
                    setErrors(res.data.errors);
                  } else {
                    Object(_components_base_alerts_sweetAlert__WEBPACK_IMPORTED_MODULE_9__["sweetAlert"])("error", res.data.errors.message);
                  }
                }
              });

            case 16:
              _context2.next = 25;
              break;

            case 18:
              _context2.prev = 18;
              _context2.t0 = _context2["catch"](13);
              data = _context2.t0.response.data;
              errors = data.errors;
              setLoading(false);
              setErrors(errors || {});
              return _context2.abrupt("return");

            case 25:
            case "end":
              return _context2.stop();
          }
        }
      }, _callee2, null, [[13, 18]]);
    }));

    return function handleSubmit(_x2) {
      return _ref3.apply(this, arguments);
    };
  }();

  var limit = function limit(val, max) {
    if (val.length === 1 && val[0] > max[0]) {
      val = "0" + val;
    }

    if (val.length === 2) {
      if (Number(val) === 0) {
        val = "01"; //this can happen when user paste number
      } else if (val > max) {
        val = max;
      }
    }

    return val;
  };

  var setExpiration = function setExpiration(value) {
    var month = limit(value.substring(0, 2), "12");
    var year = value.substring(3, 5);
    setCardExpiryMonth(parseInt(month));
    setCardExpiryYear(parseInt(year));
  };

  var sanitizeCardNumber = function sanitizeCardNumber(e) {
    var val = e.replace(/\s/g, "");
    setCardNumber(parseInt(val));
  };

  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(react__WEBPACK_IMPORTED_MODULE_1___default.a.Fragment, null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("form", {
    ref: form,
    className: "text-white pb-6",
    onSubmit: handleSubmit
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "bg-gray-200 flex mt-10 rounded-lg flex-col"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "bg-white lg:p-8 mb-8 mt-4 mx-3 p-4 pb-6 rounded-lg"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "text-xl font-bold text-black mb-2"
  }, "Payment Details"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "border-b-2 px-2 py-3 flex items-center w-full"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("svg", {
    className: "feather-icon mr-3 h-6 w-6 text-gray-600 ".concat(errors["nameOnCard"] ? "text-red-500" : "")
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("use", {
    xlinkHref: "/assets/svg/feather-sprite.svg#user"
  })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("input", {
    className: "appearance-none bg-transparent text-black border-0 font-hairline leading-relaxed text-base w-full",
    name: "nameOnCard",
    placeholder: "Name on card",
    onChange: function onChange(e) {
      setNameOnCard(e.target.value);
      setErrors({});
    }
  })), errors["nameOnCard"] && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
    className: "text-red-500 mt-2"
  }, errors["nameOnCard"]), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "flex flex-1 items-start"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "flex-1"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "border-b-2 px-2 py-3 flex items-center w-full"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("svg", {
    className: "feather-icon mr-3 h-6 w-6 text-gray-600 ".concat(errors["cardNumber"] ? "text-red-500" : "")
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("use", {
    xlinkHref: "/assets/svg/feather-sprite.svg#credit-card"
  })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(react_currency_format__WEBPACK_IMPORTED_MODULE_7___default.a, {
    placeholder: "Card number",
    name: "cardNumber",
    format: "#### #### #### ####",
    className: "text-gray-600 text-base w-full",
    onChange: function onChange(e) {
      sanitizeCardNumber(e.target.value);
      setErrors({});
    }
  })), errors["cardNumber"] && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
    className: "text-red-500 mt-2"
  }, errors["cardNumber"]))), window.innerWidth <= _helpers_deviceSizeHelper__WEBPACK_IMPORTED_MODULE_11__["baseSmall"] && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "flex-1"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "border-b-2 px-2 py-3 flex items-center w-full"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("svg", {
    className: "feather-icon h-6 mr-3 text-gray-600 w-6 ".concat(errors["expMonth"] || errors["expYear"] ? "text-red-500" : "")
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("use", {
    xlinkHref: "/assets/svg/feather-sprite.svg#calendar"
  })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(react_currency_format__WEBPACK_IMPORTED_MODULE_7___default.a, {
    format: "##/##",
    placeholder: "MM/YY",
    className: "text-gray-600 text-base",
    onChange: function onChange(e) {
      setExpiration(e.target.value);
      setErrors({});
    }
  })), (errors["expYear"] || errors["expMonth"]) && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
    className: "text-red-500 mt-2"
  }, "The expiry month and year is required.")), window.innerWidth <= _helpers_deviceSizeHelper__WEBPACK_IMPORTED_MODULE_11__["baseSmall"] && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "flex-1"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "border-b-2 px-2 py-3 flex items-center w-full"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("svg", {
    className: "feather-icon mr-3 h-6 w-6 text-gray-600 ".concat(errors["cvc"] ? "text-red-500" : "")
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("use", {
    xlinkHref: "/assets/svg/feather-sprite.svg#key"
  })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(react_currency_format__WEBPACK_IMPORTED_MODULE_7___default.a, {
    format: "###",
    placeholder: "CVC",
    className: "text-gray-600 text-base",
    name: "cvc",
    onChange: function onChange(e) {
      setCardCvc(e.target.value);
      setErrors({});
    }
  })), errors["cvc"] && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
    className: "text-red-500 mt-2"
  }, errors["cvc"])), window.innerWidth > _helpers_deviceSizeHelper__WEBPACK_IMPORTED_MODULE_11__["baseSmall"] && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "flex flex-1 items-start"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "flex-1 mr-2"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "border-b-2 px-2 py-3 flex items-center w-full"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("svg", {
    className: "feather-icon h-6 mr-3 text-gray-600 w-6 ".concat(errors["expMonth"] || errors["expYear"] ? "text-red-500" : "")
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("use", {
    xlinkHref: "/assets/svg/feather-sprite.svg#calendar"
  })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(react_currency_format__WEBPACK_IMPORTED_MODULE_7___default.a, {
    format: "##/##",
    placeholder: "MM/YY",
    className: "text-gray-600 text-base",
    onChange: function onChange(e) {
      setExpiration(e.target.value);
      setErrors({});
    }
  })), (errors["expYear"] || errors["expMonth"]) && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
    className: "text-red-500 mt-2"
  }, "The expiry month and year is required.")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "flex-1"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "border-b-2 px-2 py-3 flex items-center w-full"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("svg", {
    className: "feather-icon mr-3 h-6 w-6 text-gray-600 ".concat(errors["cvc"] ? "text-red-500" : "")
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("use", {
    xlinkHref: "/assets/svg/feather-sprite.svg#key"
  })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(react_currency_format__WEBPACK_IMPORTED_MODULE_7___default.a, {
    format: "###",
    placeholder: "CVC",
    className: "text-gray-600 text-base",
    name: "cvc",
    onChange: function onChange(e) {
      setCardCvc(e.target.value);
      setErrors({});
    }
  })), errors["cvc"] && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
    className: "text-red-500 mt-2"
  }, errors["cvc"]))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "flex flex-col mb-4 mt-5"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "flex flex-1 flex-row"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "flex-1"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(pretty_checkbox_react__WEBPACK_IMPORTED_MODULE_6__["Checkbox"], {
    className: "lg:text-base remember_me mr-0 text-gray-800 font-bold",
    shape: "round",
    color: "primary",
    value: agree,
    onChange: function onChange() {
      setAgree(!agree);
      setErrors({});
    }
  }, "I agree to the\xA0"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("a", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
    className: "cursor-pointer font-bold hover:text-blue-600 lg:text-base text-gray-800",
    onClick: function onClick() {
      return userAction.setState({
        showTermsAndConditions: true
      });
    }
  }, "Terms & Conditions")))), errors["agree"] && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
    className: "ml-6 mt-2 text-red-500"
  }, errors["agree"])), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "flex items-center mt-8"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_base__WEBPACK_IMPORTED_MODULE_3__["Button"], {
    disabled: loading
  }, loading && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_fortawesome_react_fontawesome__WEBPACK_IMPORTED_MODULE_5__["FontAwesomeIcon"], {
    icon: ["fas", "spinner"],
    className: "fa-spin mr-2"
  }), loading ? "Saving..." : window.innerWidth >= _helpers_deviceSizeHelper__WEBPACK_IMPORTED_MODULE_11__["baseSmall"] ? "Submit Payment" : "Submit"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "ml-6 mt-3"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
    className: "text-gray-800 text-base font-bold italic"
  }, "Powered by:", /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("img", {
    className: "-mt-3",
    src: "/assets/images/commbank_logo.png",
    style: {
      width: 200
    }
  }))))))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_payment_PaymentConfirmationModal__WEBPACK_IMPORTED_MODULE_10__["default"], {
    show: toggleOpen,
    data: tokenData,
    handleClose: setToggleOpen,
    formData: formData,
    project: project,
    property: property
  }));
};

var SecureDeal = function SecureDeal(_ref4) {
  var show = _ref4.show,
      project = _ref4.project,
      apartment = _ref4.apartment,
      _handleClose = _ref4.handleClose;

  var _UserGlobal3 = Object(_states_userGlobal__WEBPACK_IMPORTED_MODULE_2__["default"])(),
      _UserGlobal4 = _slicedToArray(_UserGlobal3, 1),
      userState = _UserGlobal4[0];

  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_base__WEBPACK_IMPORTED_MODULE_3__["Modal"], {
    show: show,
    title: "Secure this deal",
    maxWidth: "sm",
    onClose: function onClose() {
      return _handleClose();
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "lg:px-2"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("p", {
    className: "text-base text-white font-bold mb-8"
  }, userState.user && userState.user.first_name, ", secure this deal today for only $1,000 and it's fully refunded at exchange of contracts."), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "bg-gray-200 flex flex-col px-4 lg:px-8 py-4 rounded-lg"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "flex flex-row"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("h3", {
    className: "font-bold text-2xl lg:text-4xl"
  }, "Apartment ", apartment.unit_name)), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", null)), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "my-2"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "text-base lg:text-lg font-bold"
  }, project.address && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(react__WEBPACK_IMPORTED_MODULE_1___default.a.Fragment, null, project.address.line_1 ? project.address.line_1 + ", " : "", project.address.line_2 ? project.address.line_2 + ", " : "", project.address.suburbs ? project.address.suburbs + ", " : "", project.address.state ? project.address.state : "")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "font-bold text-base flex items-center"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
    className: "mr-4"
  }, apartment.no_of_bedrooms, " ", /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_fortawesome_react_fontawesome__WEBPACK_IMPORTED_MODULE_5__["FontAwesomeIcon"], {
    icon: ["fas", "bed"]
  })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
    className: "mr-4"
  }, apartment.no_of_bathrooms, " ", /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_fortawesome_react_fontawesome__WEBPACK_IMPORTED_MODULE_5__["FontAwesomeIcon"], {
    icon: ["fas", "bath"]
  })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
    className: "mr-4"
  }, apartment.no_of_garages, " ", /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_fortawesome_react_fontawesome__WEBPACK_IMPORTED_MODULE_5__["FontAwesomeIcon"], {
    icon: ["fas", "car"]
  })))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("hr", {
    className: "my-2 border-b  opacity-50",
    style: {
      borderColor: "#bdbdbd"
    }
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "flex"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "flex-1"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
    className: "font-semibold line-through text-lg lg:text-2xl"
  }, "WAS:", " ", /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(react_currency_format__WEBPACK_IMPORTED_MODULE_7___default.a, {
    value: apartment.price || 0,
    displayType: "text",
    thousandSeparator: true,
    prefix: "$"
  }))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "flex flex-1 justify-end"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
    className: "font-semibold text-lg lg:text-2xl text-red-700"
  }, "SAVE:", " ", /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(react_currency_format__WEBPACK_IMPORTED_MODULE_7___default.a, {
    value: apartment.price * project.discount / 100,
    displayType: "text",
    thousandSeparator: true,
    prefix: "$"
  })))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "flex mt-2"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "flex-1"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
    className: "text-base lg:text-lg font-bold"
  }, "Price")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "flex-1 text-right"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
    className: "text-base lg:text-lg font-bold"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(react_currency_format__WEBPACK_IMPORTED_MODULE_7___default.a, {
    value: apartment.price - apartment.price * project.discount / 100,
    displayType: "text",
    thousandSeparator: true,
    prefix: "$"
  })))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "flex mt-1"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "flex-1"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "flex font-bold"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("p", {
    className: "text-base lg:text-lg mb-0"
  }, "GroupBuyer \"secure deal\" fee", " "), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_base__WEBPACK_IMPORTED_MODULE_3__["Tooltip"], {
    title: "Please refer to our FAQ's and Terms & Conditions",
    placement: "top"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("svg", {
    className: "border-b-0 feather-icon h-5 ml-2 mt-1 w-5",
    style: {
      color: "#BD00F2"
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("use", {
    xlinkHref: "/assets/svg/feather-sprite.svg#info"
  })))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
    className: "text-xs lg:text-sm opacity-50"
  }, "Refunded at exchange of contracts")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "text-right"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("p", {
    className: "text-base lg:text-lg font-bold"
  }, "$1,000"))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("hr", {
    className: "mt-4 mb-2 border-b opacity-50",
    style: {
      borderColor: "#bdbdbd"
    }
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "flex"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
    className: "flex-1 font-semibold lg:text-2xl text-xl"
  }, "Total to pay now"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
    className: "lg:text-2xl text-xl font-semibold text-right"
  }, "$1,000"))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(CommBankIntegration, {
    property: apartment,
    project: project,
    handleClose: function handleClose() {
      return _handleClose();
    }
  })));
};

/* harmony default export */ __webpack_exports__["default"] = (SecureDeal);

/***/ }),

/***/ "./resources/js/components/SellerApply.js":
/*!************************************************!*\
  !*** ./resources/js/components/SellerApply.js ***!
  \************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime/regenerator */ "./node_modules/@babel/runtime/regenerator/index.js");
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _states_userGlobal__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../states/userGlobal */ "./resources/js/states/userGlobal.js");
/* harmony import */ var _base__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./_base */ "./resources/js/components/_base/index.js");
/* harmony import */ var _base__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(_base__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var _helpers_bootstrap__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../helpers/bootstrap */ "./resources/js/helpers/bootstrap.js");
/* harmony import */ var _fortawesome_react_fontawesome__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @fortawesome/react-fontawesome */ "./node_modules/@fortawesome/react-fontawesome/index.es.js");
/* harmony import */ var _material_ui_core_Snackbar__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @material-ui/core/Snackbar */ "./node_modules/@material-ui/core/esm/Snackbar/index.js");
/* harmony import */ var _material_ui_lab_Alert__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @material-ui/lab/Alert */ "./node_modules/@material-ui/lab/esm/Alert/index.js");


function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _unsupportedIterableToArray(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

function _iterableToArrayLimit(arr, i) { if (typeof Symbol === "undefined" || !(Symbol.iterator in Object(arr))) return; var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }

function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }









function Alert(props) {
  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_material_ui_lab_Alert__WEBPACK_IMPORTED_MODULE_7__["default"], _extends({
    elevation: 6,
    variant: "filled"
  }, props));
}

var SellerApply = function SellerApply(_ref) {
  var handleClose = _ref.handleClose;

  var _UserGlobal = Object(_states_userGlobal__WEBPACK_IMPORTED_MODULE_2__["default"])(),
      _UserGlobal2 = _slicedToArray(_UserGlobal, 2),
      userState = _UserGlobal2[0],
      userAction = _UserGlobal2[1];

  var _useState = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])({}),
      _useState2 = _slicedToArray(_useState, 2),
      errors = _useState2[0],
      setErrors = _useState2[1];

  var _useState3 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(false),
      _useState4 = _slicedToArray(_useState3, 2),
      loading = _useState4[0],
      setLoading = _useState4[1];

  var _useState5 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(false),
      _useState6 = _slicedToArray(_useState5, 2),
      showAlert = _useState6[0],
      setShowAlert = _useState6[1];

  var _useState7 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])({
    first_name: "",
    last_name: "",
    company: "",
    location: "",
    phone: "",
    email: "",
    message: ""
  }),
      _useState8 = _slicedToArray(_useState7, 2),
      emailData = _useState8[0],
      setEmailData = _useState8[1];

  var handleSubmit = /*#__PURE__*/function () {
    var _ref2 = _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee(e) {
      var errors, formData, data;
      return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee$(_context) {
        while (1) {
          switch (_context.prev = _context.next) {
            case 0:
              e.preventDefault();
              errors = {};
              formData = _objectSpread({}, emailData);
              formData.phone = "+61" + formData.phone;
              setLoading(true);
              _context.prev = 5;
              _context.next = 8;
              return _helpers_bootstrap__WEBPACK_IMPORTED_MODULE_4__["axios"].post("/api/send-apply-now", formData);

            case 8:
              setLoading(false);
              setShowAlert(true);
              setEmailData({
                name: "",
                phone: "",
                email: "",
                message: ""
              });
              handleClose();
              _context.next = 20;
              break;

            case 14:
              _context.prev = 14;
              _context.t0 = _context["catch"](5);
              data = _context.t0.response.data;
              errors = data.errors;
              setLoading(false);
              setShowAlert(false);

            case 20:
              setErrors(errors || {});

            case 21:
            case "end":
              return _context.stop();
          }
        }
      }, _callee, null, [[5, 14]]);
    }));

    return function handleSubmit(_x) {
      return _ref2.apply(this, arguments);
    };
  }();

  var changeHandler = function changeHandler(key, value) {
    var data = Object.assign({}, emailData);
    data[key] = value;
    setEmailData(data);
  };

  var handleAlertClose = function handleAlertClose(event, reason) {
    if (reason === "clickaway") {
      return;
    }

    setShowAlert(false);
  };

  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(react__WEBPACK_IMPORTED_MODULE_1___default.a.Fragment, null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_base__WEBPACK_IMPORTED_MODULE_3__["Modal"], {
    show: userState.showSellerApply,
    title: "Enquire",
    onClose: function onClose() {
      return handleClose();
    },
    transition: "grow"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("form", {
    className: "flex-row p-6 text-white pb-16",
    onSubmit: handleSubmit
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "flex flex-col items-center lg:flex-row"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "w-full"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_base__WEBPACK_IMPORTED_MODULE_3__["TextInput"], {
    type: "text",
    placeholder: "First Name",
    name: "first_name",
    prefix: "user",
    errors: errors.first_name,
    value: emailData.first_name,
    onChange: function onChange(e) {
      return changeHandler("first_name", e.target.value);
    }
  }), errors.first_name && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
    className: "text-red-500 text-xs"
  }, errors.first_name[0])), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "hidden lg:block w-12"
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "w-full"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_base__WEBPACK_IMPORTED_MODULE_3__["TextInput"], {
    type: "text",
    placeholder: "Last Name",
    name: "last_name",
    prefix: "user",
    errors: errors.last_name,
    value: emailData.last_name,
    onChange: function onChange(e) {
      return changeHandler("last_name", e.target.value);
    }
  }), errors.last_name && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
    className: "text-red-500 text-xs"
  }, errors.last_name[0]))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_base__WEBPACK_IMPORTED_MODULE_3__["TextInput"], {
    type: "text",
    placeholder: "Company Name",
    name: "company",
    prefix: "home",
    errors: errors.company,
    value: emailData.company,
    onChange: function onChange(e) {
      return changeHandler("company", e.target.value);
    }
  }), errors.company && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
    className: "text-red-500 text-xs"
  }, errors.company[0]), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_base__WEBPACK_IMPORTED_MODULE_3__["TextInput"], {
    type: "text",
    placeholder: "Mobile",
    name: "phone",
    prefix: "phone-call",
    value: emailData.phone,
    errors: errors.phone,
    onChange: function onChange(e) {
      return changeHandler("phone", e.target.value);
    }
  }), errors.phone && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
    className: "text-red-500 text-xs"
  }, errors.phone[0]), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_base__WEBPACK_IMPORTED_MODULE_3__["TextInput"], {
    type: "text",
    placeholder: "Email",
    name: "email",
    prefix: "mail",
    value: emailData.email,
    errors: errors.email,
    onChange: function onChange(e) {
      return changeHandler("email", e.target.value);
    }
  }), errors.email && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
    className: "text-red-500 text-xs"
  }, errors.email[0]), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_base__WEBPACK_IMPORTED_MODULE_3__["TextInput"], {
    type: "text",
    placeholder: "Location",
    name: "location",
    prefix: "map-pin",
    errors: errors.location,
    value: emailData.location,
    onChange: function onChange(e) {
      return changeHandler("location", e.target.value);
    }
  }), errors.location && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
    className: "text-red-500 text-xs"
  }, errors.location[0]), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_base__WEBPACK_IMPORTED_MODULE_3__["TextArea"], {
    placeholder: "Message",
    name: "message",
    prefix: "message-square",
    value: emailData.message,
    onChange: function onChange(e) {
      return changeHandler("message", e.target.value);
    } // errors={errors.message}
    // value={emailData.message}
    // onChange={e => changeHandler('message', e.target.value)}

  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("input", {
    type: "hidden",
    name: "role",
    value: "project_developer"
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "flex flex-col items-center justify-between lg:flex-row lg:mt-0 mt-16"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_base__WEBPACK_IMPORTED_MODULE_3__["Button"], {
    className: "py-2 mt-12 block",
    disabled: loading
  }, loading && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_fortawesome_react_fontawesome__WEBPACK_IMPORTED_MODULE_5__["FontAwesomeIcon"], {
    icon: ["fas", "spinner"],
    className: "fa-spin mr-2"
  }), "Send Enquiry")))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_material_ui_core_Snackbar__WEBPACK_IMPORTED_MODULE_6__["default"], {
    anchorOrigin: {
      vertical: "bottom",
      horizontal: "right"
    },
    open: showAlert,
    autoHideDuration: 6000,
    onClose: handleAlertClose
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(Alert, {
    severity: "success",
    onClose: handleAlertClose
  }, "Thank you for your message, we will contact you shortly!")));
};

/* harmony default export */ __webpack_exports__["default"] = (SellerApply);

/***/ }),

/***/ "./resources/js/components/SequenceAddingModal.js":
/*!********************************************************!*\
  !*** ./resources/js/components/SequenceAddingModal.js ***!
  \********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _AddingSteps__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./AddingSteps */ "./resources/js/components/AddingSteps.js");
/* harmony import */ var _AddProject__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./AddProject */ "./resources/js/components/AddProject.js");
/* harmony import */ var _AddProjectResources__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./AddProjectResources */ "./resources/js/components/AddProjectResources.js");
/* harmony import */ var _components_base__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../components/_base */ "./resources/js/components/_base/index.js");
/* harmony import */ var _components_base__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(_components_base__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var _states_userGlobal__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../states/userGlobal */ "./resources/js/states/userGlobal.js");
/* harmony import */ var _DeveloperPropertyManager__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./DeveloperPropertyManager */ "./resources/js/components/DeveloperPropertyManager.js");
/* harmony import */ var _AgencyAgreementPart__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./AgencyAgreementPart */ "./resources/js/components/AgencyAgreementPart.js");
function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _unsupportedIterableToArray(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

function _iterableToArrayLimit(arr, i) { if (typeof Symbol === "undefined" || !(Symbol.iterator in Object(arr))) return; var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }










var SequenceAddingModal = function SequenceAddingModal(_ref) {
  var toggleSequenceAddingModal = _ref.toggleSequenceAddingModal,
      addToggleFetch = _ref.addToggleFetch;

  var _UserGlobal = Object(_states_userGlobal__WEBPACK_IMPORTED_MODULE_5__["default"])(),
      _UserGlobal2 = _slicedToArray(_UserGlobal, 2),
      userState = _UserGlobal2[0],
      userActions = _UserGlobal2[1];

  Object(react__WEBPACK_IMPORTED_MODULE_0__["useEffect"])(function () {
    userActions.setState({
      currentProjStep: 1
    });
    userActions.setState({
      projectData: []
    });
  }, []);
  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_base__WEBPACK_IMPORTED_MODULE_4__["Modal"], {
    show: true,
    disableBackdropClick: true,
    topOff: true,
    maxWidth: "md",
    title: "Add Project",
    onClose: function onClose() {
      return toggleSequenceAddingModal();
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_AddingSteps__WEBPACK_IMPORTED_MODULE_1__["default"], null), userState.currentProjStep === 1 && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_AddProject__WEBPACK_IMPORTED_MODULE_2__["default"], {
    toggleFetch: addToggleFetch
  }), userState.currentProjStep === 2 && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_AddProjectResources__WEBPACK_IMPORTED_MODULE_3__["default"], {
    toggleFetch: addToggleFetch
  }), userState.currentProjStep === 3 && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_DeveloperPropertyManager__WEBPACK_IMPORTED_MODULE_6__["default"], {
    toggleFetch: addToggleFetch,
    toggleCloseModal: toggleSequenceAddingModal
  }), userState.currentProjStep === 4 && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_AgencyAgreementPart__WEBPACK_IMPORTED_MODULE_7__["default"], null));
};

/* harmony default export */ __webpack_exports__["default"] = (SequenceAddingModal);

/***/ }),

/***/ "./resources/js/components/SequenceEditingModal.js":
/*!*********************************************************!*\
  !*** ./resources/js/components/SequenceEditingModal.js ***!
  \*********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _EditSteps__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./EditSteps */ "./resources/js/components/EditSteps.js");
/* harmony import */ var _EditProjectPart__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./EditProjectPart */ "./resources/js/components/EditProjectPart.js");
/* harmony import */ var _AgencyAgreementPart__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./AgencyAgreementPart */ "./resources/js/components/AgencyAgreementPart.js");
/* harmony import */ var _admin_components_EditProjectResources__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../admin/components/EditProjectResources */ "./resources/js/admin/components/EditProjectResources.js");
/* harmony import */ var _DeveloperPropertyManager__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./DeveloperPropertyManager */ "./resources/js/components/DeveloperPropertyManager.js");
/* harmony import */ var _components_base__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../components/_base */ "./resources/js/components/_base/index.js");
/* harmony import */ var _components_base__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(_components_base__WEBPACK_IMPORTED_MODULE_6__);
/* harmony import */ var _states_userGlobal__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../states/userGlobal */ "./resources/js/states/userGlobal.js");
function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _unsupportedIterableToArray(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

function _iterableToArrayLimit(arr, i) { if (typeof Symbol === "undefined" || !(Symbol.iterator in Object(arr))) return; var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }










var SequenceEditingModal = function SequenceEditingModal(_ref) {
  var project = _ref.project,
      toggleSequenceEditModal = _ref.toggleSequenceEditModal,
      editToggleFetch = _ref.editToggleFetch;

  var _UserGlobal = Object(_states_userGlobal__WEBPACK_IMPORTED_MODULE_7__["default"])(),
      _UserGlobal2 = _slicedToArray(_UserGlobal, 2),
      userState = _UserGlobal2[0],
      userActions = _UserGlobal2[1];

  var _useState = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])(true),
      _useState2 = _slicedToArray(_useState, 2),
      projectView = _useState2[0],
      setProjectView = _useState2[1];

  var _useState3 = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])(false),
      _useState4 = _slicedToArray(_useState3, 2),
      resourcesView = _useState4[0],
      setResourcesView = _useState4[1];

  var _useState5 = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])(false),
      _useState6 = _slicedToArray(_useState5, 2),
      propertyView = _useState6[0],
      setPropertyView = _useState6[1];

  var _useState7 = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])(false),
      _useState8 = _slicedToArray(_useState7, 2),
      agreementView = _useState8[0],
      setAgreementView = _useState8[1];

  Object(react__WEBPACK_IMPORTED_MODULE_0__["useEffect"])(function () {
    userActions.setState({
      projectData: project
    });
    userActions.setState({
      currentProjStep: 1
    });
  }, []);

  var toggleProjV = function toggleProjV() {
    setProjectView(true);
    setResourcesView(false);
    setPropertyView(false);
    setAgreementView(false);
  };

  var toggleResV = function toggleResV() {
    setResourcesView(true);
    setPropertyView(false);
    setProjectView(false);
    setAgreementView(false);
  };

  var togglePropView = function togglePropView() {
    setPropertyView(true);
    setProjectView(false);
    setResourcesView(false);
    setAgreementView(false);
  };

  var toggleAgreeView = function toggleAgreeView() {
    setPropertyView(false);
    setProjectView(false);
    setResourcesView(false);
    setAgreementView(true);
  };

  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_base__WEBPACK_IMPORTED_MODULE_6__["Modal"], {
    show: true,
    disableBackdropClick: true,
    topOff: true,
    maxWidth: "md",
    title: "Edit Project",
    onClose: function onClose() {
      return toggleSequenceEditModal();
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_EditSteps__WEBPACK_IMPORTED_MODULE_1__["default"], {
    activeProjectView: projectView,
    activeResourcesView: resourcesView,
    activePropertyView: propertyView,
    activeAgreementView: agreementView,
    toggleProjectView: function toggleProjectView() {
      return toggleProjV();
    },
    toggleResourcesView: function toggleResourcesView() {
      return toggleResV();
    },
    togglePropertyView: function togglePropertyView() {
      return togglePropView();
    },
    toggleAgreementView: function toggleAgreementView() {
      return toggleAgreeView();
    }
  }), projectView && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_EditProjectPart__WEBPACK_IMPORTED_MODULE_2__["default"], {
    toggleFetch: editToggleFetch
  }), resourcesView && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_admin_components_EditProjectResources__WEBPACK_IMPORTED_MODULE_4__["default"], {
    toggleFetch: editToggleFetch
  }), propertyView && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_DeveloperPropertyManager__WEBPACK_IMPORTED_MODULE_5__["default"], {
    toggleCloseModal: toggleSequenceEditModal,
    toggleFetch: editToggleFetch,
    toggleAgreementView: toggleAgreeView
  }), agreementView && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_AgencyAgreementPart__WEBPACK_IMPORTED_MODULE_3__["default"], {
    toggleFetch: editToggleFetch
  }));
};

/* harmony default export */ __webpack_exports__["default"] = (SequenceEditingModal);

/***/ }),

/***/ "./resources/js/components/SignIn.js":
/*!*******************************************!*\
  !*** ./resources/js/components/SignIn.js ***!
  \*******************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime/regenerator */ "./node_modules/@babel/runtime/regenerator/index.js");
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _states_userGlobal__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../states/userGlobal */ "./resources/js/states/userGlobal.js");
/* harmony import */ var _base__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./_base */ "./resources/js/components/_base/index.js");
/* harmony import */ var _base__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(_base__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var _services_auth__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../services/auth */ "./resources/js/services/auth.js");
/* harmony import */ var _helpers_bootstrap__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../helpers/bootstrap */ "./resources/js/helpers/bootstrap.js");
/* harmony import */ var _fortawesome_react_fontawesome__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @fortawesome/react-fontawesome */ "./node_modules/@fortawesome/react-fontawesome/index.es.js");
/* harmony import */ var pretty_checkbox_react__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! pretty-checkbox-react */ "./node_modules/pretty-checkbox-react/dist/pretty-checkbox-react.umd.min.js");
/* harmony import */ var pretty_checkbox_react__WEBPACK_IMPORTED_MODULE_7___default = /*#__PURE__*/__webpack_require__.n(pretty_checkbox_react__WEBPACK_IMPORTED_MODULE_7__);
/* harmony import */ var lodash_isEmpty__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! lodash/isEmpty */ "./node_modules/lodash/isEmpty.js");
/* harmony import */ var lodash_isEmpty__WEBPACK_IMPORTED_MODULE_8___default = /*#__PURE__*/__webpack_require__.n(lodash_isEmpty__WEBPACK_IMPORTED_MODULE_8__);
/* harmony import */ var react_facebook_login_dist_facebook_login_render_props__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! react-facebook-login/dist/facebook-login-render-props */ "./node_modules/react-facebook-login/dist/facebook-login-render-props.js");
/* harmony import */ var react_facebook_login_dist_facebook_login_render_props__WEBPACK_IMPORTED_MODULE_9___default = /*#__PURE__*/__webpack_require__.n(react_facebook_login_dist_facebook_login_render_props__WEBPACK_IMPORTED_MODULE_9__);
/* harmony import */ var react_google_login__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! react-google-login */ "./node_modules/react-google-login/dist/google-login.js");
/* harmony import */ var react_google_login__WEBPACK_IMPORTED_MODULE_10___default = /*#__PURE__*/__webpack_require__.n(react_google_login__WEBPACK_IMPORTED_MODULE_10__);
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! sweetalert2 */ "./node_modules/sweetalert2/dist/sweetalert2.all.js");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_11___default = /*#__PURE__*/__webpack_require__.n(sweetalert2__WEBPACK_IMPORTED_MODULE_11__);
/* harmony import */ var react_device_detect__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! react-device-detect */ "./node_modules/react-device-detect/main.js");
/* harmony import */ var react_device_detect__WEBPACK_IMPORTED_MODULE_12___default = /*#__PURE__*/__webpack_require__.n(react_device_detect__WEBPACK_IMPORTED_MODULE_12__);


function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _unsupportedIterableToArray(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

function _iterableToArrayLimit(arr, i) { if (typeof Symbol === "undefined" || !(Symbol.iterator in Object(arr))) return; var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }














var SignIn = function SignIn(_ref) {
  var handleClose = _ref.handleClose;

  var _UserGlobal = Object(_states_userGlobal__WEBPACK_IMPORTED_MODULE_2__["default"])(),
      _UserGlobal2 = _slicedToArray(_UserGlobal, 2),
      userState = _UserGlobal2[0],
      userAction = _UserGlobal2[1];

  var _useState = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(false),
      _useState2 = _slicedToArray(_useState, 2),
      showPassword = _useState2[0],
      setShowPassword = _useState2[1];

  var _useState3 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])({}),
      _useState4 = _slicedToArray(_useState3, 2),
      errors = _useState4[0],
      setErrors = _useState4[1];

  var _useState5 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(false),
      _useState6 = _slicedToArray(_useState5, 2),
      loading = _useState6[0],
      setLoading = _useState6[1];

  var form = Object(react__WEBPACK_IMPORTED_MODULE_1__["createRef"])();

  var handleShowPassword = function handleShowPassword() {
    setShowPassword(!showPassword);
  }; // const handleGoogleLogin = async e => {
  //   e.preventDefault();
  //   let url = window.location.origin;
  //   window.location.assign(`${url}/api/sign-in/google`);
  // };


  var handleSubmit = /*#__PURE__*/function () {
    var _ref2 = _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee(e) {
      var errors, formData, _yield$axios$post, data, _data;

      return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee$(_context) {
        while (1) {
          switch (_context.prev = _context.next) {
            case 0:
              e.preventDefault();
              errors = {};
              formData = new FormData(form.current);
              setLoading(true);
              _context.prev = 4;
              _context.next = 7;
              return _helpers_bootstrap__WEBPACK_IMPORTED_MODULE_5__["axios"].post("/api/login", formData);

            case 7:
              _yield$axios$post = _context.sent;
              data = _yield$axios$post.data;
              Object(_services_auth__WEBPACK_IMPORTED_MODULE_4__["setToken"])(data.access_token);
              setLoading(false);
              Object(_services_auth__WEBPACK_IMPORTED_MODULE_4__["setUserRole"])(data.user_role);
              userAction.setState({
                fetch: !userState.fetch
              });
              userAction.setState({
                userCount: userState.userCount + 1
              });
              handleClose();
              _context.next = 22;
              break;

            case 17:
              _context.prev = 17;
              _context.t0 = _context["catch"](4);
              _data = _context.t0.response.data;
              errors = _data.errors;
              setLoading(false);

            case 22:
              setErrors(errors || {});

            case 23:
            case "end":
              return _context.stop();
          }
        }
      }, _callee, null, [[4, 17]]);
    }));

    return function handleSubmit(_x) {
      return _ref2.apply(this, arguments);
    };
  }();

  var socialMediaLoginDataMapper = function socialMediaLoginDataMapper(response, isGoogle) {
    var formData = new FormData();
    var newName = [];

    if (!isGoogle) {
      newName = response.name.split(" ", 2);
      formData.append("facebook_id", response.userID);
    } else {
      formData.append("google_id", response.profileObj.googleId);
    }

    formData.append("first_name", !isGoogle ? newName[0] : response.profileObj.givenName);
    formData.append("last_name", !isGoogle ? newName[1] : response.profileObj.familyName);
    formData.append("email", !isGoogle ? response.email : response.profileObj.email);
    formData.append("avatar_path", !isGoogle ? response.picture.data.url : response.profileObj.imageUrl);
    formData.append("is_google", isGoogle);
    return formData;
  };

  var responseFacebook = function responseFacebook(response) {
    if (lodash_isEmpty__WEBPACK_IMPORTED_MODULE_8___default()(response.email)) {
      return sweetalert2__WEBPACK_IMPORTED_MODULE_11___default.a.fire({
        type: "error",
        title: "Please use facebook account with email.",
        showConfirmButton: false,
        timer: 3000
      });
    }

    userLogin(socialMediaLoginDataMapper(response, false));
  };

  var responseGoogle = function responseGoogle(response) {
    userLogin(socialMediaLoginDataMapper(response, true));
  };

  var userLogin = /*#__PURE__*/function () {
    var _ref3 = _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee2(formData) {
      var _yield$axios$post2, data;

      return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee2$(_context2) {
        while (1) {
          switch (_context2.prev = _context2.next) {
            case 0:
              _context2.prev = 0;
              _context2.next = 3;
              return _helpers_bootstrap__WEBPACK_IMPORTED_MODULE_5__["axios"].post("/api/social-media-login", formData);

            case 3:
              _yield$axios$post2 = _context2.sent;
              data = _yield$axios$post2.data;
              Object(_services_auth__WEBPACK_IMPORTED_MODULE_4__["setToken"])(data.access_token);

              if (react_device_detect__WEBPACK_IMPORTED_MODULE_12__["isMobile"]) {
                userAction.setState({
                  showSignUp: false
                });
              }

              userAction.setState({
                fetch: !userState.fetch
              });
              handleClose();
              _context2.next = 14;
              break;

            case 11:
              _context2.prev = 11;
              _context2.t0 = _context2["catch"](0);
              sweetalert2__WEBPACK_IMPORTED_MODULE_11___default.a.fire({
                type: "error",
                title: "Error on login, please try again!",
                showConfirmButton: false,
                timer: 3000
              });

            case 14:
            case "end":
              return _context2.stop();
          }
        }
      }, _callee2, null, [[0, 11]]);
    }));

    return function userLogin(_x2) {
      return _ref3.apply(this, arguments);
    };
  }();

  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_base__WEBPACK_IMPORTED_MODULE_3__["Modal"], {
    show: userState.showSignIn,
    title: "Log in",
    onClose: function onClose() {
      return handleClose();
    },
    transition: "grow"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("form", {
    ref: form,
    className: "flex-row lg:p-6 text-white",
    onSubmit: handleSubmit
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_base__WEBPACK_IMPORTED_MODULE_3__["TextInput"], {
    type: "text",
    placeholder: "Email",
    className: "text-base",
    name: "email",
    prefix: "mail",
    errors: errors.email
  }), errors.email && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
    className: "text-red-500 text-xs"
  }, errors.email[0]), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_base__WEBPACK_IMPORTED_MODULE_3__["TextInput"], {
    type: showPassword ? "text" : "password",
    className: "text-base",
    placeholder: "Password",
    name: "password",
    prefix: "lock",
    suffix: /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
      className: "cursor-pointer",
      onClick: function onClick() {
        return handleShowPassword();
      }
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("svg", {
      className: "feather-icon h-5 w-5 opacity-50"
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("use", {
      xlinkHref: "/assets/svg/feather-sprite.svg#".concat(showPassword ? "eye-off" : "eye")
    }))),
    errors: errors.password
  }), errors.password && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
    className: "text-red-500 text-xs"
  }, errors.password[0]), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "flex flex-col items-center justify-between lg:flex-row my-8"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(pretty_checkbox_react__WEBPACK_IMPORTED_MODULE_7__["Checkbox"], {
    className: "text-base remember_me lg:mb-0 mb-8",
    shape: "round",
    color: "primary",
    inputProps: {
      name: "remember_me"
    }
  }, "Remember password"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
    className: "cursor-pointer text-base hover:text-palette-teal transition-all duration-300"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("a", {
    href: "/request-password-reset",
    className: "cursor-pointer duration-300 font-bold hover:text-palette-blue-light hover:text-palette-teal inline-flex items-center pr-3 relative text-palette-blue-light text-sm transition-all z-10"
  }, "Forgot password?"))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "flex flex-col lg:flex-row items-center justify-between mt-16"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_base__WEBPACK_IMPORTED_MODULE_3__["Button"], {
    className: "px-20",
    disabled: loading
  }, loading && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_fortawesome_react_fontawesome__WEBPACK_IMPORTED_MODULE_6__["FontAwesomeIcon"], {
    icon: ["fas", "spinner"],
    className: "fa-spin mr-2"
  }), "Log in"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "text-gray-600 text-base mt-4 lg:mt-0"
  }, "Don't have an account?", /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
    className: "cursor-pointer text-palette-blue-light hover:text-palette-teal transition-all duration-300",
    onClick: function onClick() {
      handleClose();
      userAction.setState({
        showSignUp: true
      });
    }
  }, "\xA0Register now")))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("section", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "flex my-4 items-center"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "border-b-2 flex-1",
    style: {
      borderColor: "rgba(255,255,255,0.1)"
    }
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "bg-palette-blue-dark opacity-25 px-4 py-1 text-base text-white"
  }, "or"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "border-b-2 flex-1",
    style: {
      borderColor: "rgba(255,255,255,0.1)"
    }
  })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "flex flex-col items-center mt-8 mb-16"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(react_facebook_login_dist_facebook_login_render_props__WEBPACK_IMPORTED_MODULE_9___default.a, {
    appId: "186207692714514",
    autoLoad: false,
    fields: "name,email,picture",
    callback: responseFacebook,
    render: function render(renderProps) {
      return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("button", {
        onClick: renderProps.onClick,
        className: "font-bold items-center flex px-8 py-2 rounded-full text-base text-white mb-8",
        style: {
          background: "#4267b2"
        }
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_fortawesome_react_fontawesome__WEBPACK_IMPORTED_MODULE_6__["FontAwesomeIcon"], {
        icon: ["fab", "facebook-f"],
        size: "lg"
      }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
        className: "ml-3"
      }, "Continue with Facebook"));
    }
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(react_google_login__WEBPACK_IMPORTED_MODULE_10__["GoogleLogin"], {
    clientId: "970841385117-1r70ifjjdqq7fi5h0vnkvki1k5q0kjsp.apps.googleusercontent.com",
    autoLoad: false,
    render: function render(renderProps) {
      return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("button", {
        className: "bg-gray-100 font-bold rounded-full text-gray-800 px-8 py-2 items-center flex text-base",
        onClick: renderProps.onClick
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("img", {
        src: "/assets/images/google-logo.png"
      }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
        className: "lg:ml-6 lg:mr-6 ml-3 mr-3"
      }, "Sign up with Google"));
    },
    buttonText: "Log in",
    onSuccess: responseGoogle // onFailure={responseGoogle}
    ,
    cookiePolicy: "single_host_origin"
  }))));
};

/* harmony default export */ __webpack_exports__["default"] = (SignIn);

/***/ }),

/***/ "./resources/js/components/SignUp.js":
/*!*******************************************!*\
  !*** ./resources/js/components/SignUp.js ***!
  \*******************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime/regenerator */ "./node_modules/@babel/runtime/regenerator/index.js");
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _states_userGlobal__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../states/userGlobal */ "./resources/js/states/userGlobal.js");
/* harmony import */ var _base__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./_base */ "./resources/js/components/_base/index.js");
/* harmony import */ var _base__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(_base__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var _helpers_bootstrap__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../helpers/bootstrap */ "./resources/js/helpers/bootstrap.js");
/* harmony import */ var _services_auth__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../services/auth */ "./resources/js/services/auth.js");
/* harmony import */ var _fortawesome_react_fontawesome__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @fortawesome/react-fontawesome */ "./node_modules/@fortawesome/react-fontawesome/index.es.js");
/* harmony import */ var pretty_checkbox_react__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! pretty-checkbox-react */ "./node_modules/pretty-checkbox-react/dist/pretty-checkbox-react.umd.min.js");
/* harmony import */ var pretty_checkbox_react__WEBPACK_IMPORTED_MODULE_7___default = /*#__PURE__*/__webpack_require__.n(pretty_checkbox_react__WEBPACK_IMPORTED_MODULE_7__);
/* harmony import */ var _components_base_alerts_sweetAlert__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../components/_base/alerts/sweetAlert */ "./resources/js/components/_base/alerts/sweetAlert.js");
/* harmony import */ var react_facebook_login_dist_facebook_login_render_props__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! react-facebook-login/dist/facebook-login-render-props */ "./node_modules/react-facebook-login/dist/facebook-login-render-props.js");
/* harmony import */ var react_facebook_login_dist_facebook_login_render_props__WEBPACK_IMPORTED_MODULE_9___default = /*#__PURE__*/__webpack_require__.n(react_facebook_login_dist_facebook_login_render_props__WEBPACK_IMPORTED_MODULE_9__);
/* harmony import */ var react_google_login__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! react-google-login */ "./node_modules/react-google-login/dist/google-login.js");
/* harmony import */ var react_google_login__WEBPACK_IMPORTED_MODULE_10___default = /*#__PURE__*/__webpack_require__.n(react_google_login__WEBPACK_IMPORTED_MODULE_10__);
/* harmony import */ var react_device_detect__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! react-device-detect */ "./node_modules/react-device-detect/main.js");
/* harmony import */ var react_device_detect__WEBPACK_IMPORTED_MODULE_11___default = /*#__PURE__*/__webpack_require__.n(react_device_detect__WEBPACK_IMPORTED_MODULE_11__);
/* harmony import */ var lodash_isEmpty__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! lodash/isEmpty */ "./node_modules/lodash/isEmpty.js");
/* harmony import */ var lodash_isEmpty__WEBPACK_IMPORTED_MODULE_12___default = /*#__PURE__*/__webpack_require__.n(lodash_isEmpty__WEBPACK_IMPORTED_MODULE_12__);


function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _unsupportedIterableToArray(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

function _iterableToArrayLimit(arr, i) { if (typeof Symbol === "undefined" || !(Symbol.iterator in Object(arr))) return; var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }














var SignUp = function SignUp(_ref) {
  var handleClose = _ref.handleClose;

  var _UserGlobal = Object(_states_userGlobal__WEBPACK_IMPORTED_MODULE_2__["default"])(),
      _UserGlobal2 = _slicedToArray(_UserGlobal, 2),
      userState = _UserGlobal2[0],
      userAction = _UserGlobal2[1];

  var _useState = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(false),
      _useState2 = _slicedToArray(_useState, 2),
      showPassword = _useState2[0],
      setShowPassword = _useState2[1];

  var _useState3 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])({}),
      _useState4 = _slicedToArray(_useState3, 2),
      errors = _useState4[0],
      setErrors = _useState4[1];

  var _useState5 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(false),
      _useState6 = _slicedToArray(_useState5, 2),
      loading = _useState6[0],
      setLoading = _useState6[1];

  var form = Object(react__WEBPACK_IMPORTED_MODULE_1__["createRef"])();

  var handleShowPassword = function handleShowPassword() {
    setShowPassword(!showPassword);
  };

  var handleSubmit = /*#__PURE__*/function () {
    var _ref2 = _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee(e) {
      var errors, formData, _yield$axios$post, data, _data;

      return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee$(_context) {
        while (1) {
          switch (_context.prev = _context.next) {
            case 0:
              e.preventDefault();
              errors = {};
              formData = new FormData(form.current);
              setLoading(true);
              _context.prev = 4;
              _context.next = 7;
              return _helpers_bootstrap__WEBPACK_IMPORTED_MODULE_4__["axios"].post('/api/register', formData);

            case 7:
              _yield$axios$post = _context.sent;
              data = _yield$axios$post.data;
              Object(_services_auth__WEBPACK_IMPORTED_MODULE_5__["setToken"])(data.access_token);
              setLoading(false);
              userAction.setState({
                fetch: !userState.fetch
              });
              userAction.setState({
                userCount: userState.userCount + 1
              });
              handleClose();
              setTimeout(function () {
                Object(_components_base_alerts_sweetAlert__WEBPACK_IMPORTED_MODULE_8__["sweetAlert"])('success', 'Thank you for registering and welcome to GroupBuyer.');
              }, 2000);
              _context.next = 22;
              break;

            case 17:
              _context.prev = 17;
              _context.t0 = _context["catch"](4);
              _data = _context.t0.response.data;
              errors = _data.errors;
              setLoading(false);

            case 22:
              setErrors(errors || {});

            case 23:
            case "end":
              return _context.stop();
          }
        }
      }, _callee, null, [[4, 17]]);
    }));

    return function handleSubmit(_x) {
      return _ref2.apply(this, arguments);
    };
  }();

  var socialMediaLoginDataMapper = function socialMediaLoginDataMapper(response, isGoogle) {
    var formData = new FormData();
    var newName = [];

    if (!isGoogle) {
      newName = response.name.split(' ', 2);
      formData.append('facebook_id', response.userID);
    } else {
      formData.append('google_id', response.profileObj.googleId);
    }

    formData.append('first_name', !isGoogle ? newName[0] : response.profileObj.givenName);
    formData.append('last_name', !isGoogle ? newName[1] : response.profileObj.familyName);
    formData.append('email', !isGoogle ? response.email : response.profileObj.email);
    formData.append('avatar_path', !isGoogle ? response.picture.data.url : response.profileObj.imageUrl);
    formData.append('is_google', isGoogle);
    return formData;
  };

  var responseFacebook = function responseFacebook(response) {
    if (lodash_isEmpty__WEBPACK_IMPORTED_MODULE_12___default()(response.email)) {
      return Object(_components_base_alerts_sweetAlert__WEBPACK_IMPORTED_MODULE_8__["sweetAlert"])('error', 'Please use facebook account with email');
    }

    userLogin(socialMediaLoginDataMapper(response, false));
  };

  var responseGoogle = function responseGoogle(response) {
    userLogin(socialMediaLoginDataMapper(response, true));
  };

  var userLogin = /*#__PURE__*/function () {
    var _ref3 = _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee2(formData) {
      var _yield$axios$post2, data;

      return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee2$(_context2) {
        while (1) {
          switch (_context2.prev = _context2.next) {
            case 0:
              _context2.prev = 0;
              _context2.next = 3;
              return _helpers_bootstrap__WEBPACK_IMPORTED_MODULE_4__["axios"].post('/api/social-media-login', formData);

            case 3:
              _yield$axios$post2 = _context2.sent;
              data = _yield$axios$post2.data;
              Object(_services_auth__WEBPACK_IMPORTED_MODULE_5__["setToken"])(data.access_token);
              userAction.setState({
                userCount: userState.userCount + 1
              });

              if (react_device_detect__WEBPACK_IMPORTED_MODULE_11__["isMobile"]) {
                userAction.setState({
                  showSignUp: false
                });
              }

              userAction.setState({
                fetch: !userState.fetch
              });
              _context2.next = 14;
              break;

            case 11:
              _context2.prev = 11;
              _context2.t0 = _context2["catch"](0);
              Object(_components_base_alerts_sweetAlert__WEBPACK_IMPORTED_MODULE_8__["sweetAlert"])('error', 'Error on login, please try again!');

            case 14:
            case "end":
              return _context2.stop();
          }
        }
      }, _callee2, null, [[0, 11]]);
    }));

    return function userLogin(_x2) {
      return _ref3.apply(this, arguments);
    };
  }();

  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_base__WEBPACK_IMPORTED_MODULE_3__["Modal"], {
    show: userState.showSignUp,
    title: "Free to join",
    onClose: function onClose() {
      return handleClose();
    },
    transition: "grow"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("form", {
    ref: form,
    className: "flex-row lg:p-6 text-white",
    onSubmit: handleSubmit
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "flex flex-col lg:flex-row items-center"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "w-full"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_base__WEBPACK_IMPORTED_MODULE_3__["TextInput"], {
    className: "text-base",
    type: "text",
    placeholder: "First Name",
    name: "first_name",
    prefix: "user",
    errors: errors.first_name
  }), errors.first_name && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
    className: "text-red-500 text-xs"
  }, errors.first_name[0])), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "hidden lg:block w-12"
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "w-full"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_base__WEBPACK_IMPORTED_MODULE_3__["TextInput"], {
    type: "text",
    className: "text-base",
    placeholder: "Last Name",
    name: "last_name",
    prefix: "user",
    errors: errors.last_name
  }), errors.last_name && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
    className: "text-red-500 text-xs"
  }, errors.last_name[0]))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_base__WEBPACK_IMPORTED_MODULE_3__["TextInput"], {
    type: "text",
    className: "text-base",
    placeholder: "Email",
    name: "email",
    errors: errors.email,
    prefix: "mail"
  }), errors.email && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
    className: "text-red-500 text-xs"
  }, errors.email[0]), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_base__WEBPACK_IMPORTED_MODULE_3__["TextInput"], {
    type: showPassword ? 'text' : 'password',
    placeholder: "Password",
    className: "text-base",
    name: "password",
    prefix: "lock",
    suffix: /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
      className: "cursor-pointer",
      onClick: function onClick() {
        return handleShowPassword();
      }
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("svg", {
      className: "feather-icon h-5 w-5 opacity-50"
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("use", {
      xlinkHref: "/assets/svg/feather-sprite.svg#".concat(showPassword ? 'eye-off' : 'eye')
    }))),
    errors: errors.password
  }), errors.password && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
    className: "text-red-500 text-xs"
  }, errors.password[0]), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "flex flex-col lg:flex-row lg:items-center justify-between my-8 px-2"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(pretty_checkbox_react__WEBPACK_IMPORTED_MODULE_7__["Radio"], {
    className: "text-base remember_me",
    shape: "round",
    color: "primary",
    name: "user_type",
    value: "customer",
    inputProps: {
      defaultChecked: true
    }
  }, "I'm looking to buy"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(pretty_checkbox_react__WEBPACK_IMPORTED_MODULE_7__["Radio"], {
    className: "mt-4 lg:mt-0 text-base remember_me",
    shape: "round",
    color: "primary",
    name: "user_type",
    value: "project_developer"
  }, "I'm a developer/seller")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "flex flex-col lg:flex-row items-center justify-between mt-16"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_base__WEBPACK_IMPORTED_MODULE_3__["Button"], {
    className: "px-20",
    disabled: loading
  }, loading && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_fortawesome_react_fontawesome__WEBPACK_IMPORTED_MODULE_6__["FontAwesomeIcon"], {
    icon: ['fas', 'spinner'],
    className: "fa-spin mr-2"
  }), "Sign up"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "text-gray-600 text-base mt-4 lg:mt-0"
  }, "Already have an account?", /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
    className: "cursor-pointer text-palette-blue-light hover:text-palette-teal transition-all duration-300",
    onClick: function onClick() {
      handleClose();
      userAction.setState({
        showSignIn: true
      });
    }
  }, "\xA0Log in")))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("section", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "flex my-4 items-center"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "border-b-2 flex-1",
    style: {
      borderColor: 'rgba(255,255,255,0.1)'
    }
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "bg-palette-blue-dark opacity-25 px-4 py-1 text-base text-white"
  }, "or"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "border-b-2 flex-1",
    style: {
      borderColor: 'rgba(255,255,255,0.1)'
    }
  })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "flex flex-col items-center mt-8 mb-16"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(react_facebook_login_dist_facebook_login_render_props__WEBPACK_IMPORTED_MODULE_9___default.a, {
    appId: "186207692714514",
    autoLoad: false,
    fields: "name,email,picture",
    callback: responseFacebook,
    render: function render(renderProps) {
      return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("button", {
        onClick: renderProps.onClick,
        className: "font-bold items-center flex px-8 py-2 rounded-full text-base text-white mb-8",
        style: {
          background: '#4267b2'
        }
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_fortawesome_react_fontawesome__WEBPACK_IMPORTED_MODULE_6__["FontAwesomeIcon"], {
        icon: ['fab', 'facebook-f'],
        size: "lg"
      }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
        className: "ml-3"
      }, "Sign up with Facebook"));
    }
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(react_google_login__WEBPACK_IMPORTED_MODULE_10__["GoogleLogin"], {
    clientId: "970841385117-1r70ifjjdqq7fi5h0vnkvki1k5q0kjsp.apps.googleusercontent.com",
    autoLoad: false,
    render: function render(renderProps) {
      return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("button", {
        className: "bg-gray-100 font-bold rounded-full text-gray-800 px-8 py-2 items-center flex text-base",
        onClick: renderProps.onClick
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("img", {
        src: "/assets/images/google-logo.png"
      }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
        className: "lg:ml-6 lg:mr-6 ml-3 mr-3"
      }, "Sign up with Google"));
    },
    buttonText: "Log in",
    onSuccess: responseGoogle // onFailure={responseGoogle}
    ,
    cookiePolicy: 'single_host_origin'
  }))));
};

/* harmony default export */ __webpack_exports__["default"] = (SignUp);

/***/ }),

/***/ "./resources/js/components/SocialMedia.js":
/*!************************************************!*\
  !*** ./resources/js/components/SocialMedia.js ***!
  \************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _fortawesome_react_fontawesome__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @fortawesome/react-fontawesome */ "./node_modules/@fortawesome/react-fontawesome/index.es.js");



var SocialMedia = function SocialMedia(_ref) {
  var _ref$variant = _ref.variant,
      variant = _ref$variant === void 0 ? "primary" : _ref$variant,
      _ref$size = _ref.size,
      size = _ref$size === void 0 ? "lg" : _ref$size,
      className = _ref.className;
  var socialMediaArr = [{
    icon: /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_fortawesome_react_fontawesome__WEBPACK_IMPORTED_MODULE_1__["FontAwesomeIcon"], {
      icon: ["fab", "facebook-f"],
      size: size
    }),
    color: "#3B5999",
    linkPath: 'https://www.facebook.com/GroupBuyer.com.au/'
  }, {
    icon: /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_fortawesome_react_fontawesome__WEBPACK_IMPORTED_MODULE_1__["FontAwesomeIcon"], {
      icon: ["fab", "instagram"],
      size: size
    }),
    color: "#019EFE",
    linkPath: 'https://www.instagram.com/groupbuyerau '
  }, {
    icon: /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_fortawesome_react_fontawesome__WEBPACK_IMPORTED_MODULE_1__["FontAwesomeIcon"], {
      icon: ["fab", "linkedin-in"],
      size: size
    }),
    color: "#0077B5",
    linkPath: 'https://www.linkedin.com/company/19168781/'
  }];

  var socialMediaLinks = function socialMediaLinks(variant) {
    return socialMediaArr.map(function (scMedia, index) {
      return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: "text-center mx-2 my-1",
        key: index
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("a", {
        href: scMedia.linkPath,
        target: "_blank",
        className: "\n              ".concat(variant !== "primary" ? "w-8 h-8" : "w-12 h-12", "\n              rounded-full block block items-center justify-center flex hover:bg-palette-teal hover:text-white\n            "),
        style: {
          background: variant !== "primary" ? "#151E48" : scMedia.color
        }
      }, scMedia.icon));
    });
  };

  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "flex flex-row flex-wrap items-center ".concat(className)
  }, socialMediaLinks(variant));
};

/* harmony default export */ __webpack_exports__["default"] = (SocialMedia);

/***/ }),

/***/ "./resources/js/components/SocialMediaShareModal.js":
/*!**********************************************************!*\
  !*** ./resources/js/components/SocialMediaShareModal.js ***!
  \**********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime/regenerator */ "./node_modules/@babel/runtime/regenerator/index.js");
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _base__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./_base */ "./resources/js/components/_base/index.js");
/* harmony import */ var _base__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(_base__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var react_share__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! react-share */ "./node_modules/react-share/es/index.js");
/* harmony import */ var _states_userGlobal__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../states/userGlobal */ "./resources/js/states/userGlobal.js");
/* harmony import */ var _fortawesome_react_fontawesome__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @fortawesome/react-fontawesome */ "./node_modules/@fortawesome/react-fontawesome/index.es.js");
/* harmony import */ var _components_base_alerts_sweetAlert__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../components/_base/alerts/sweetAlert */ "./resources/js/components/_base/alerts/sweetAlert.js");


function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _unsupportedIterableToArray(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

function _iterableToArrayLimit(arr, i) { if (typeof Symbol === "undefined" || !(Symbol.iterator in Object(arr))) return; var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }









var SocialMediaShareModal = function SocialMediaShareModal() {
  var fbApplicationId = "".concat("186207692714514");

  var _UserGlobal = Object(_states_userGlobal__WEBPACK_IMPORTED_MODULE_4__["default"])(),
      _UserGlobal2 = _slicedToArray(_UserGlobal, 2),
      userState = _UserGlobal2[0],
      userAction = _UserGlobal2[1];

  var handleClose = function handleClose() {
    userAction.setState({
      showSocialMedia: false
    });
  };

  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_base__WEBPACK_IMPORTED_MODULE_2__["Modal"], {
    show: userState.showSocialMedia,
    title: "Share this deal",
    maxWidth: "sm",
    onClose: function onClose() {
      return handleClose();
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "flex flex-col items-center justify-center"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(react_share__WEBPACK_IMPORTED_MODULE_3__["FacebookShareButton"], {
    url: window.location.href,
    className: "mt-6"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "w-64 font-bold items-center flex px-8 py-2 rounded-full text-base text-white mb-8",
    style: {
      background: "rgb(59, 89, 153)"
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_fortawesome_react_fontawesome__WEBPACK_IMPORTED_MODULE_5__["FontAwesomeIcon"], {
    icon: ["fab", "facebook-f"],
    size: "lg"
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
    className: "ml-3"
  }, "Share on Facebook"))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(react_share__WEBPACK_IMPORTED_MODULE_3__["LinkedinShareButton"], {
    url: window.location.href,
    className: "mt-2"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "w-64 font-bold items-center flex px-8 py-2 rounded-full text-base text-white mb-8",
    style: {
      background: "rgb(0, 119, 181)"
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_fortawesome_react_fontawesome__WEBPACK_IMPORTED_MODULE_5__["FontAwesomeIcon"], {
    icon: ["fab", "linkedin"],
    size: "lg"
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
    className: "ml-3"
  }, "Share on Linkedin"))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(react_share__WEBPACK_IMPORTED_MODULE_3__["TwitterShareButton"], {
    url: window.location.href,
    className: "mt-2"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", _defineProperty({
    style: {
      backgroundColor: "#0e76a8"
    },
    className: "bg-blue-400 w-64 font-bold items-center flex px-8 py-2 rounded-full text-base text-white mb-8"
  }, "style", {
    background: "#4267b2"
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_fortawesome_react_fontawesome__WEBPACK_IMPORTED_MODULE_5__["FontAwesomeIcon"], {
    icon: ["fab", "twitter"],
    size: "lg"
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
    className: "ml-3"
  }, "Share on Twitter"))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(react_share__WEBPACK_IMPORTED_MODULE_3__["EmailShareButton"], {
    subject: "GroupBuyer",
    url: window.location.href,
    className: "mt-2",
    target: "_blank"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", _defineProperty({
    style: {
      backgroundColor: "#0e76a8"
    },
    className: "w-64 font-bold items-center flex px-8 py-2 rounded-full text-base text-white mb-8"
  }, "style", {
    background: "#1565C0"
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_fortawesome_react_fontawesome__WEBPACK_IMPORTED_MODULE_5__["FontAwesomeIcon"], {
    icon: ["fas", "envelope"],
    size: "lg"
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
    className: "ml-3"
  }, "Share on Email"))), ShareViaSMS(), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(react_share__WEBPACK_IMPORTED_MODULE_3__["FacebookMessengerShareButton"], {
    url: window.location.href,
    redirectUri: window.location.href,
    appId: "186207692714514",
    to: "",
    className: "mt-2"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", _defineProperty({
    style: {
      backgroundColor: "#0e76a8"
    },
    className: "w-64 font-bold items-center flex px-8 py-2 rounded-full text-base text-white mb-8"
  }, "style", {
    background: "#1565C0"
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_fortawesome_react_fontawesome__WEBPACK_IMPORTED_MODULE_5__["FontAwesomeIcon"], {
    icon: ["fab", "facebook-messenger"],
    size: "lg"
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
    className: "ml-3"
  }, "Share on Messenger"))), CopyLink()));
};

var ShareViaSMS = function ShareViaSMS() {
  var _useState = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(""),
      _useState2 = _slicedToArray(_useState, 2),
      targetNumber = _useState2[0],
      setTargetNumber = _useState2[1];

  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", _defineProperty({
    style: {
      backgroundColor: "#0e76a8"
    },
    className: "cursor-pointer bg-blue-900 w-64 font-bold items-center flex px-8 py-2 rounded-full text-base text-white mb-8"
  }, "style", {
    background: "#1565C0"
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_fortawesome_react_fontawesome__WEBPACK_IMPORTED_MODULE_5__["FontAwesomeIcon"], {
    icon: ["fas", "comment-alt"],
    size: "lg"
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("a", {
    href: "sms:".concat(targetNumber, "?&body=").concat(window.location.href),
    className: "smsLink ml-3"
  }, "Share link via SMS"));
};

var CopyLink = function CopyLink() {
  var _React$createElement5;

  var copyToClipBoard = /*#__PURE__*/function () {
    var _ref = _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee() {
      return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee$(_context) {
        while (1) {
          switch (_context.prev = _context.next) {
            case 0:
              _context.prev = 0;
              _context.next = 3;
              return navigator.clipboard.writeText(window.location.href);

            case 3:
              Object(_components_base_alerts_sweetAlert__WEBPACK_IMPORTED_MODULE_6__["sweetAlert"])("success", "Copied!");
              _context.next = 9;
              break;

            case 6:
              _context.prev = 6;
              _context.t0 = _context["catch"](0);
              Object(_components_base_alerts_sweetAlert__WEBPACK_IMPORTED_MODULE_6__["sweetAlert"])("error", "Failed to copy!");

            case 9:
            case "end":
              return _context.stop();
          }
        }
      }, _callee, null, [[0, 6]]);
    }));

    return function copyToClipBoard() {
      return _ref.apply(this, arguments);
    };
  }();

  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", (_React$createElement5 = {
    style: {
      backgroundColor: "#0e76a8"
    },
    className: "cursor-pointer bg-blue-800 w-64 font-bold items-center flex px-8 py-2 rounded-full text-base text-white mb-8"
  }, _defineProperty(_React$createElement5, "style", {
    background: "#1565C0"
  }), _defineProperty(_React$createElement5, "onClick", function onClick() {
    return copyToClipBoard();
  }), _React$createElement5), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_fortawesome_react_fontawesome__WEBPACK_IMPORTED_MODULE_5__["FontAwesomeIcon"], {
    icon: ["fas", "copy"],
    size: "lg"
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
    className: "ml-3"
  }, "Copy Link"));
};

/* harmony default export */ __webpack_exports__["default"] = (SocialMediaShareModal);

/***/ }),

/***/ "./resources/js/components/SolicitorDetails.js":
/*!*****************************************************!*\
  !*** ./resources/js/components/SolicitorDetails.js ***!
  \*****************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _base__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./_base */ "./resources/js/components/_base/index.js");
/* harmony import */ var _base__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_base__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _helpers_salesAdviceHelper_salesAdviceHelper__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../helpers/salesAdviceHelper/salesAdviceHelper */ "./resources/js/helpers/salesAdviceHelper/salesAdviceHelper.js");




var SolicitorDetails = function SolicitorDetails(_ref) {
  var solicitorErrors = _ref.solicitorErrors,
      solicitorForm = _ref.solicitorForm,
      _ref$width = _ref.width,
      width = _ref$width === void 0 ? "w-full" : _ref$width;
  Object(react__WEBPACK_IMPORTED_MODULE_0__["useEffect"])(function () {}, []);
  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("section", {
    className: "relative mt-6 rounded"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "mx-auto",
    style: {
      maxWidth: 1366
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "flex justify-center mt-4 mb-8 ".concat(width, " lg:pr-10")
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "border lg:mt-0 mt-8 px-6 rounded w-full"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("h1", {
    className: "font-bold py-6 text-base"
  }, "Purchaser's solicitor details"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("form", {
    ref: solicitorForm
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_base__WEBPACK_IMPORTED_MODULE_1__["Form"], {
    errors: solicitorErrors,
    formFields: Object(_helpers_salesAdviceHelper_salesAdviceHelper__WEBPACK_IMPORTED_MODULE_2__["solicitorDetailsMapper"])({})
  }))))));
};

/* harmony default export */ __webpack_exports__["default"] = (SolicitorDetails);

/***/ }),

/***/ "./resources/js/components/TermsAndConditionsModal.js":
/*!************************************************************!*\
  !*** ./resources/js/components/TermsAndConditionsModal.js ***!
  \************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _base__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./_base */ "./resources/js/components/_base/index.js");
/* harmony import */ var _base__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_base__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _states_userGlobal__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../states/userGlobal */ "./resources/js/states/userGlobal.js");
function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _unsupportedIterableToArray(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

function _iterableToArrayLimit(arr, i) { if (typeof Symbol === "undefined" || !(Symbol.iterator in Object(arr))) return; var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }





var TermsAndConditionsModal = function TermsAndConditionsModal(_ref) {
  var handleClose = _ref.handleClose;

  // const TermsAndConditionModal = () => {
  var _UserGlobal = Object(_states_userGlobal__WEBPACK_IMPORTED_MODULE_2__["default"])(),
      _UserGlobal2 = _slicedToArray(_UserGlobal, 1),
      userState = _UserGlobal2[0];

  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_base__WEBPACK_IMPORTED_MODULE_1__["Modal"], {
    show: userState.showTermsAndConditions,
    title: "Terms and Conditions",
    onClose: function onClose() {
      return handleClose();
    },
    transition: "grow",
    maxWidth: "md"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("section", {
    className: "text-white bg-palette-blue-dark",
    style: {
      marginTop: -130,
      paddingTop: 160
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "mx-auto",
    style: {
      maxWidth: 1366
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "bg-palette-blue-dark mx-auto pb-10 lg:pt-16 px-4 text-white"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("h2", {
    className: "font-extrabold lg:text-4xl mb-4 text-2xl"
  }, "USER"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("p", null, "Group Buyer Pty Ltd ACN 641 556 470 (Group Buyer) has the right to operate the Group Buyer website (Website). These Terms and Conditions apply to your use of the Website. Your use of the Website including accessing or browsing the Website or uploading any material to the Website is deemed to be your acceptance of these Terms and Conditions. Please read these carefully and if you do not accept the Terms and Conditions set out below please exit the Website and refrain from using the Website."), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("p", null, "If you breach any provision of these Terms and Conditions your right to access the Website will be immediately revoked. By registering as a Group Buyer account member and using the Website you acknowledge that you are at least eighteen (18) years of age and you agree to be bound by these Terms and Conditions."), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("h4", {
    className: "text-2xl mb-3 mt-8 font-bold"
  }, "1. Intellectual Property"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("p", null, "Group Buyer uses the word \u2018Group Buyer\u2019 as a trade mark in Australia (Trade Mark). You are not permitted to use the Trade Mark or any other trademarks which belong or are licensed to Group Buyer or any of Group Buyer\u2019 Intellectual Property which includes but is not limited to copyright material, confidential information, designs and other content located on the Website without Group Buyer\u2019 prior written authorisation."), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("p", null, "The information, images and text on the Website may be subject to copyright and are the property of or otherwise property that is licensed to Group Buyer. Trade marks used on the Website to describe other companies and their products or services are trade marks which belong to or are licensed to those companies (Other Trade Marks). The Other Trade Marks are displayed on the Website to provide information to you about other products or services via the Website. You agree not to download, copy, reproduce or in any way use the Other Trade Marks without authorisation from the owner/s of the Other Trade Marks."), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("p", null, "You acknowledge that you are expressly prohibited from using the phrase \u2018Group Buyer\u2019 or any part of the website domain name without the written consent of Group Buyer. You agree that you are prohibited from reverse engineering the Website or otherwise attempting to copy the Website functionality or obtain from the Website information or data including but not limited to user\u2019s personal information."), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("h4", {
    className: "text-2xl mb-3 mt-8 font-bold"
  }, "2. Information and Liability"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("p", null, "You acknowledge that Group Buyer is not liable or responsible for any errors or omissions in the content of the Website. The information contained on the Website is provided for general information purposes only. Group Buyer makes no warranties as to the accuracy or completeness of the recommendations, information or material contained on the Website."), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("p", null, "You acknowledge that there are risks associated with using the internet and World Wide Web and to the fullest extent permitted by law, Group Buyer or its affiliates or subsidiaries are not liable for any direct or indirect damages or losses arising by way of your use or access to the Website."), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("p", null, "You acknowledge that Group Buyer makes no warranty that the Website will meet your user requirements or that the information contained on the Website or your use of the Website will be uninterrupted, timely, secure, error free, accurate, virus-free or compliant with legislation or regulations."), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("p", null, "You acknowledge and agree that Group Buyer have no liability of any kind either express or implied with respect to any claims arising in any way from your use of the Website, by virtue of the information provided or contained on the Website. The Website contains general statements regarding goods and/or services offered by other businesses however it is your responsibility to ensure the goods and/or services are the product you wish to purchase and must review the relevant product or service documents supplied by the relevant businesses."), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("p", null, "You acknowledge that Group Buyer may from time to time improve or make changes to the Website to upgrade security on the Website, update information on the Website or for any other reason in Group Buyer\u2019s sole discretion."), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("p", null, "You acknowledge that you are responsible for ensuring the protection of your electronic equipment when accessing or browsing the Website and unreservedly hold Group Buyer harmless in respect to damage sustained to your electronic equipment or losses incurred by you as a result of the Website."), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("h4", {
    className: "text-2xl mb-3 mt-8 font-bold"
  }, "3. Privacy"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("p", null, "You acknowledge that during your use of the Website you may provide or upload certain personal information, photos, images or written material to the Website. Group Buyer is committed to preserving the privacy of its users and will ensure that all personal information collected will be kept secure and in accordance with any applicable privacy laws. By using the Website, you agree to provide certain personal information necessary to utilise the Website to its full capability."), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("p", null, "Whilst Group Buyer agrees to ensure that all personal information is kept secure, you agree that Group Buyer may provide your personal information to other companies for the purpose of dealing with your property request. You acknowledge that Group Buyer will receive remuneration for providing your personal information to relevant third-party businesses."), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("p", null, "Group Buyer reserves the right to cooperate fully with any law enforcement authorities relating to any lawful request for the disclosure of personal information of its users. You acknowledge that you have read and agree to the terms of Group Buyer\u2019s", " ", /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("a", {
    href: "#"
  }, ".Privacy Policy")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("h4", {
    className: "text-2xl mb-3 mt-8 font-bold"
  }, "4. Use of Website"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("p", null, "You acknowledge that your use of the Website is undertaken at your own risk and you unreservedly indemnify Group Buyer from any responsibility in respect of information or content listed on the Website or any linked Websites not controlled by Group Buyer."), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("p", null, "You agree that you will not engage in any activity that is disruptive or is detrimental to the operation or function of the Website. You are prohibited from displaying or transmitting unlawful, threatening, defamatory, obscene, or profane material on the Website. Group Buyer may prevent you from accessing the Website if in Group Buyer\u2019 sole opinion you have violated or acted inconsistently with these Terms and Conditions."), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("h4", {
    className: "text-2xl mb-3 mt-8 font-bold"
  }, "5. Links and Advertisements"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("p", null, "You acknowledge that any links or advertisements containing links to external websites that are provided or located on the Website are for convenience only. Group Buyer does not endorse or make any warranty with respect to any external websites or to the accuracy or reliability of the links or advertisements contained on the Website."), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("p", null, "You acknowledge that items, resources or learning material which are advertised or displayed on the Website may not be available to purchase or download at the time you access the Website."), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("h4", {
    className: "text-2xl mb-3 mt-8 font-bold"
  }, "6. Security"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("p", null, "Group Buyer, from time to time, may use technology to improve security on the Website. You agree to cooperate with Group Buyer and you are responsible to check and read the amended Terms and Conditions."), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("h4", {
    className: "text-2xl mb-3 mt-8 font-bold"
  }, "7. Cookies and Viruses"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("p", null, "Group Buyer uses its best endeavours to ensure the use of the best technology to protect its users, but accepts no responsibility in relation to any damage to any electronic equipment used by you, including mobile devices, as a result of your use of the Website. You acknowledge that you are responsible for ensuring the protection of your electronic equipment and unreservedly indemnify Group Buyer from any responsibility in respect of damage to any electronic equipment as a result of your use of the Website."), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("h4", {
    className: "text-2xl mb-3 mt-8 font-bold"
  }, "8. Payment"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("p", null, "You acknowledge that you may be charged for the services you receive from using the Website. You agree to provide Group Buyer with at least one payment method supported by Group Buyer upon request from Group Buyer. You acknowledge that Group Buyer may vary the amount charged for a service at any time and at the sole discretion of Group Buyer."), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("h4", {
    className: "text-2xl mb-3 mt-8 font-bold"
  }, "9. General"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("p", null, "If any part of these Terms and Conditions are held to be illegal or unenforceable, you authorise Group Buyer to amend or sever that part of the Terms and Conditions to make that part legal and enforceable or to ensure that the remaining parts of the Terms and Conditions remain in full force and effect."), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("p", null, "You acknowledge that Group Buyer may vary these Terms and Conditions and you agree to be bound by that variation from that time when that variation is advised. You understand that it is your responsibility to regularly check these Terms and Conditions so that you are aware of any variations which apply. Your continued use of the Website constitutes your agreement to these Terms and Conditions and any variations to these Terms and Conditions."), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("h4", {
    className: "text-2xl mb-3 mt-8 font-bold"
  }, "10. Applicable Law"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("p", null, "You agree that the content of the Website is intended for Australian residents only and may not be available in your Country. The laws of the State of New South Wales will apply with respect to these Terms and Conditions and your use of the Website. You submit to the exclusive jurisdiction of the applicable Courts located in the State of New South Wales, Australia with respect to the Website and these Terms and Conditions.")))));
};

/* harmony default export */ __webpack_exports__["default"] = (TermsAndConditionsModal);

/***/ }),

/***/ "./resources/js/components/TotalSize.js":
/*!**********************************************!*\
  !*** ./resources/js/components/TotalSize.js ***!
  \**********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);


var TotalSize = function TotalSize(_ref) {
  var apartment = _ref.apartment;

  var getTotalSize = function getTotalSize(data) {
    if (data) {
      return data.internal_size + data.external_size + data.parking_size;
    }

    return 0;
  };

  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "mt-10 border-gray-500 border-b-2 pb-8"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
    className: "text-base font-bold"
  }, "Total Size: ", "".concat(getTotalSize(apartment), "sqm")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "flex flex-col lg:flex-row items-center mt-4"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "flex-1 font-semibold text-base text-gray-700"
  }, "Internal: ".concat(apartment.internal_size ? apartment.internal_size : 0, "sqm")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "flex-1 font-semibold text-base text-gray-700"
  }, "External: ".concat(apartment.external_size ? apartment.external_size : 0, "sqm")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "flex-1 font-semibold text-base text-gray-700"
  }, "Parking: ".concat(apartment.parking_size ? apartment.parking_size : 0, "sqm"))));
};

/* harmony default export */ __webpack_exports__["default"] = (TotalSize);

/***/ }),

/***/ "./resources/js/components/_base/alerts/BaseAlert.js":
/*!***********************************************************!*\
  !*** ./resources/js/components/_base/alerts/BaseAlert.js ***!
  \***********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _unsupportedIterableToArray(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

function _iterableToArrayLimit(arr, i) { if (typeof Symbol === "undefined" || !(Symbol.iterator in Object(arr))) return; var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }



var BaseAlert = function BaseAlert(_ref) {
  var message = _ref.message,
      icon = _ref.icon,
      type = _ref.type,
      className = _ref.className,
      _ref$iconHeight = _ref.iconHeight,
      iconHeight = _ref$iconHeight === void 0 ? "h-5" : _ref$iconHeight,
      _ref$iconWidth = _ref.iconWidth,
      iconWidth = _ref$iconWidth === void 0 ? "w-5" : _ref$iconWidth;

  var _useState = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])(""),
      _useState2 = _slicedToArray(_useState, 2),
      bgColor = _useState2[0],
      setBackgrounColor = _useState2[1];

  var _useState3 = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])(""),
      _useState4 = _slicedToArray(_useState3, 2),
      msgBGColor = _useState4[0],
      setMsgBGColor = _useState4[1];

  var _useState5 = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])(message),
      _useState6 = _slicedToArray(_useState5, 2),
      msgType = _useState6[0],
      setMsgType = _useState6[1];

  Object(react__WEBPACK_IMPORTED_MODULE_0__["useEffect"])(function () {
    assignValues();
  }, []);

  var assignValues = function assignValues() {
    switch (type) {
      case "success":
        setBackgrounColor("rgba(137, 176, 54, 0.3)");
        setMsgBGColor("rgb(137, 176, 54)");
        break;

      case "info":
        break;

      case "error":
        setBackgrounColor("rgba(216, 67, 21, 1)");
        setMsgBGColor("rgba(255, 255, 255, 1)");
        break;

      default:
        break;
    }
  };

  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "flex items-center mb-2 px-6 py-3 rounded ".concat(className),
    style: {
      backgroundColor: bgColor
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("svg", {
    className: "feather-icon ".concat(iconHeight, " ").concat(iconWidth, " lg:rounded-full text-white mr-4 p-1"),
    style: {
      backgroundColor: msgBGColor
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("use", {
    xlinkHref: "/assets/svg/feather-sprite.svg#".concat(icon)
  })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
    className: "text-base text-center",
    style: {
      color: msgBGColor
    }
  }, message));
};

/* harmony default export */ __webpack_exports__["default"] = (BaseAlert);

/***/ }),

/***/ "./resources/js/components/index.js":
/*!******************************************!*\
  !*** ./resources/js/components/index.js ***!
  \******************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var requireContext = __webpack_require__("./resources/js/components sync .*\\.(js)$");

var components = {};
requireContext.keys().forEach(function (fileName) {
  if (fileName === './index.js') return;
  var name = fileName.replace(/(\.\/|\.js)/g, '');
  components[name] = requireContext(fileName)["default"];
});
module.exports = components;

/***/ }),

/***/ "./resources/js/components/payment/PaymentConfirmationModal.js":
/*!*********************************************************************!*\
  !*** ./resources/js/components/payment/PaymentConfirmationModal.js ***!
  \*********************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime/regenerator */ "./node_modules/@babel/runtime/regenerator/index.js");
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _base__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../_base */ "./resources/js/components/_base/index.js");
/* harmony import */ var _base__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(_base__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var lodash_isEmpty__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! lodash/isEmpty */ "./node_modules/lodash/isEmpty.js");
/* harmony import */ var lodash_isEmpty__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(lodash_isEmpty__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var _services_auth__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../services/auth */ "./resources/js/services/auth.js");
/* harmony import */ var _components_base_alerts_sweetAlert__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../components/_base/alerts/sweetAlert */ "./resources/js/components/_base/alerts/sweetAlert.js");


function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _unsupportedIterableToArray(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

function _iterableToArrayLimit(arr, i) { if (typeof Symbol === "undefined" || !(Symbol.iterator in Object(arr))) return; var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }







var PaymentConfirmationModal = function PaymentConfirmationModal(_ref) {
  var show = _ref.show,
      handleClose = _ref.handleClose,
      data = _ref.data,
      formData = _ref.formData,
      project = _ref.project,
      property = _ref.property;
  Object(react__WEBPACK_IMPORTED_MODULE_1__["useEffect"])(function () {
    if (!lodash_isEmpty__WEBPACK_IMPORTED_MODULE_3___default()(data)) {
      loadIFrame();
    }
  }, [data]);
  var iframeNode = $("#secure3d-frame");

  var _useState = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])("https://www.simplify.com"),
      _useState2 = _slicedToArray(_useState, 2),
      simplifyDomain = _useState2[0],
      setSimplifyDomain = _useState2[1];

  var loadIFrame = function loadIFrame() {
    var newSecure3dForm = create3DSecureForm(data.data.card); // Step 2

    $(newSecure3dForm).insertAfter(iframeNode);
    iframeNode.removeClass("hidden");

    var process3dSecureCallback = function process3dSecureCallback(threeDsResponse) {
      window.removeEventListener("message", process3dSecureCallback);

      if (threeDsResponse.origin === simplifyDomain && JSON.parse(threeDsResponse.data)['secure3d']['authenticated']) {
        securedDealPayment();
      } else {
        handleClose();
      }
    };

    iframeNode.on("load", function () {
      window.addEventListener("message", process3dSecureCallback); // Step 3
    });
    newSecure3dForm.submit();
  };

  var securedDealPayment = /*#__PURE__*/function () {
    var _ref2 = _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee() {
      return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee$(_context) {
        while (1) {
          switch (_context.prev = _context.next) {
            case 0:
              formData.append("token", data.data.id);
              _context.next = 3;
              return axios.post("/api/secure-deal", formData, {
                headers: {
                  Authorization: "Bearer " + Object(_services_auth__WEBPACK_IMPORTED_MODULE_4__["isLoggedIn"])()
                }
              }).then(function (res) {
                if (res.status == 200 && res.data.statusCode == 400) {
                  var msg = res.data.declineReason.replace(/_/g, " ");
                  Object(_components_base_alerts_sweetAlert__WEBPACK_IMPORTED_MODULE_5__["sweetAlert"])("error", msg);
                  iframeNode.hide();
                  handleClose();
                }

                if (res.status == 200 && res.data.statusCode == 200 && res.data.status === "APPROVED") {
                  window.location = "/weekly-deals/".concat(project.id, "/apartment/").concat(property.id, "/", false, "/confirm");
                }
              });

            case 3:
              return _context.abrupt("return", _context.sent);

            case 4:
            case "end":
              return _context.stop();
          }
        }
      }, _callee);
    }));

    return function securedDealPayment() {
      return _ref2.apply(this, arguments);
    };
  }();

  var create3DSecureForm = function create3DSecureForm(data) {
    var secure3dData = data["secure3DData"];
    var secure3dForm = document.createElement("form");
    secure3dForm.setAttribute("method", "POST");
    secure3dForm.setAttribute("action", secure3dData.acsUrl);
    secure3dForm.setAttribute("target", "secure3d-frame");
    var merchantDetails = secure3dData.md;
    var paReq = secure3dData.paReq;
    var termUrl = secure3dData.termUrl;
    secure3dForm.append(createInput("PaReq", paReq));
    secure3dForm.append(createInput("TermUrl", termUrl));
    secure3dForm.append(createInput("MD", merchantDetails));
    return secure3dForm;
  };

  var createInput = function createInput(name, value) {
    var input = document.createElement("input");
    input.setAttribute("type", "hidden");
    input.setAttribute("name", name);
    input.setAttribute("value", value);
    return input;
  };

  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_base__WEBPACK_IMPORTED_MODULE_2__["Modal"], {
    show: show,
    title: "",
    maxWidth: "sm",
    onClose: function onClose() {
      return handleClose();
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "bg-gray-200 flex flex-col px-4 lg:px-8 py-4 rounded-lg mb-6"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "flex flex-col"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "flex justify-center item-center"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("h3", {
    className: "font-bold text-2xl lg:text-4xl"
  }, "Payment Authentication")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("iframe", {
    name: "secure3d-frame",
    id: "secure3d-frame",
    className: "hidden w-full",
    style: {
      height: 500
    }
  })))));
};

/* harmony default export */ __webpack_exports__["default"] = (PaymentConfirmationModal);

/***/ }),

/***/ "./resources/js/components/resources/LandResourcesData.js":
/*!****************************************************************!*\
  !*** ./resources/js/components/resources/LandResourcesData.js ***!
  \****************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _helpers_resourceHelper_index__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../helpers/resourceHelper/index */ "./resources/js/helpers/resourceHelper/index.js");
/* harmony import */ var _fortawesome_react_fontawesome__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @fortawesome/react-fontawesome */ "./node_modules/@fortawesome/react-fontawesome/index.es.js");
/* harmony import */ var _fortawesome_free_solid_svg_icons__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @fortawesome/free-solid-svg-icons */ "./node_modules/@fortawesome/free-solid-svg-icons/index.es.js");
/* harmony import */ var _base__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../_base */ "./resources/js/components/_base/index.js");
/* harmony import */ var _base__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(_base__WEBPACK_IMPORTED_MODULE_4__);






var LandResourcesData = function LandResourcesData(_ref) {
  var devProfile = _ref.devProfile,
      getFileName = _ref.getFileName,
      setCheckedState = _ref.setCheckedState,
      changeInputType = _ref.changeInputType,
      handleFileChange = _ref.handleFileChange,
      handleDelete = _ref.handleDelete,
      setProjectVideo = _ref.setProjectVideo,
      _ref$projectVideoDefa = _ref.projectVideoDefaultValue,
      projectVideoDefaultValue = _ref$projectVideoDefa === void 0 ? "" : _ref$projectVideoDefa,
      setDevProfileLink = _ref.setDevProfileLink,
      _ref$devProfileLink = _ref.devProfileLink,
      devProfileLink = _ref$devProfileLink === void 0 ? "" : _ref$devProfileLink;
  return _helpers_resourceHelper_index__WEBPACK_IMPORTED_MODULE_1__["landTableRows"].map(function (row, key) {
    return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react__WEBPACK_IMPORTED_MODULE_0___default.a.Fragment, {
      key: key
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("tr", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("td", {
      className: "pt-4 lg:px-4 w-1/2 ".concat(row.label === "Developer Profile" ? "flex w-full" : "w-1/2", " ")
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
      className: "block font-semibold  mb-4"
    }, row.label), row.label === "Developer Profile" && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: "ml-40"
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("input", {
      type: "checkbox",
      checked: setCheckedState(row),
      onChange: function onChange(e) {
        changeInputType(row.label, e.target.checked);
      }
    }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
      className: "p-1"
    }, "Site URL")))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("tr", null, row.label === "Developer Profile" && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("td", {
      className: "pb-4 lg:px-4 pr:4 pr-2 w-1/2 "
    }, devProfile === "file" ? /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("label", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: "py-2 px-4 cursor-pointer w-full ",
      style: {
        border: "2px dashed #ccc"
      }
    }, "Select File...", /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("input", {
      name: row.resource,
      className: "block hidden w-full",
      type: "file",
      onChange: function onChange(e) {
        handleFileChange(e, row.resource); // e.target.value = null;
      }
    }))) : /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_base__WEBPACK_IMPORTED_MODULE_4__["TextInput"], {
      className: "border-gray-400 border pl-2 py-1",
      border: false,
      width: "w-full",
      placeholder: "https://",
      onChange: function onChange(e) {
        setDevProfileLink(e.target.value);
      },
      defaultValue: devProfileLink
    })), row.label === "Project Video" && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("td", {
      className: "pb-4 lg:px-4 pr:4 pr-2 w-1/2 "
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_base__WEBPACK_IMPORTED_MODULE_4__["TextInput"], {
      className: "border-gray-400 border pl-2 py-1",
      border: false,
      width: "w-full",
      onChange: function onChange(e) {
        return setProjectVideo(e.target.value);
      },
      defaultValue: projectVideoDefaultValue
    })), row.label !== "Project Video" && row.label !== "Developer Profile" && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("td", {
      className: "pb-4 lg:px-4 pr:4 pr-2 w-1/2 "
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("label", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: "py-2 px-4 cursor-pointer w-full ",
      style: {
        border: "2px dashed #ccc"
      }
    }, "Select File...", /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("input", {
      name: row.resource,
      className: "block hidden w-full",
      type: "file",
      onChange: function onChange(e) {
        handleFileChange(e, row.resource); // e.target.value = null;
      }
    })))), row.label !== "Developer Profile" && row.label !== "Project Video" && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react__WEBPACK_IMPORTED_MODULE_0___default.a.Fragment, null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("td", {
      className: "pb-4 w-4/12 text-center "
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
      className: "pt-5 text-palette-blue-light"
    }, getFileName(row.resource))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("td", {
      className: "pb-4 w-1/12 text-center cursor-pointer"
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_fortawesome_react_fontawesome__WEBPACK_IMPORTED_MODULE_2__["FontAwesomeIcon"], {
      className: "text-lg text-red-500 hover:text-red-400",
      onClick: function onClick(e) {
        handleDelete(e, row.resource);
      },
      icon: _fortawesome_free_solid_svg_icons__WEBPACK_IMPORTED_MODULE_3__["faTimes"]
    }))), row.label === "Developer Profile" && devProfile === "file" && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react__WEBPACK_IMPORTED_MODULE_0___default.a.Fragment, null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("td", {
      className: "pb-4 w-4/12 text-center "
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
      className: "pt-5 text-palette-blue-light"
    }, getFileName(row.resource))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("td", {
      className: "pb-4 w-1/12 text-center cursor-pointer"
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_fortawesome_react_fontawesome__WEBPACK_IMPORTED_MODULE_2__["FontAwesomeIcon"], {
      className: "text-lg text-red-500 hover:text-red-400",
      onClick: function onClick(e) {
        handleDelete(e, row.resource);
      },
      icon: _fortawesome_free_solid_svg_icons__WEBPACK_IMPORTED_MODULE_3__["faTimes"]
    })))));
  });
};

/* harmony default export */ __webpack_exports__["default"] = (LandResourcesData);

/***/ }),

/***/ "./resources/js/components/resources/ResourcesData.js":
/*!************************************************************!*\
  !*** ./resources/js/components/resources/ResourcesData.js ***!
  \************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _helpers_resourceHelper_index__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../helpers/resourceHelper/index */ "./resources/js/helpers/resourceHelper/index.js");
/* harmony import */ var _fortawesome_react_fontawesome__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @fortawesome/react-fontawesome */ "./node_modules/@fortawesome/react-fontawesome/index.es.js");
/* harmony import */ var _fortawesome_free_solid_svg_icons__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @fortawesome/free-solid-svg-icons */ "./node_modules/@fortawesome/free-solid-svg-icons/index.es.js");
/* harmony import */ var _base__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../_base */ "./resources/js/components/_base/index.js");
/* harmony import */ var _base__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(_base__WEBPACK_IMPORTED_MODULE_4__);






var ResourcesData = function ResourcesData(_ref) {
  var builderProfile = _ref.builderProfile,
      devProfile = _ref.devProfile,
      arcProfile = _ref.arcProfile,
      handleDelete = _ref.handleDelete,
      handleFileChange = _ref.handleFileChange,
      changeInputType = _ref.changeInputType,
      setCheckedState = _ref.setCheckedState,
      getFileName = _ref.getFileName,
      setProjectVideo = _ref.setProjectVideo,
      _ref$projectVideoDefa = _ref.projectVideoDefaultValue,
      projectVideoDefaultValue = _ref$projectVideoDefa === void 0 ? "" : _ref$projectVideoDefa,
      setDevProfileLink = _ref.setDevProfileLink,
      _ref$devProfileLink = _ref.devProfileLink,
      devProfileLink = _ref$devProfileLink === void 0 ? "" : _ref$devProfileLink;
  return _helpers_resourceHelper_index__WEBPACK_IMPORTED_MODULE_1__["tableRows"].map(function (row, key) {
    return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react__WEBPACK_IMPORTED_MODULE_0___default.a.Fragment, {
      key: key
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("tr", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("td", {
      className: "pt-4 lg:px-4 w-1/2 ".concat(row.label === "Builder Profile" || row.label === "Architect Profile" || row.label === "Developer Profile" ? "flex w-full" : "w-1/2", " ")
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
      className: "block font-semibold  mb-4"
    }, row.label), (row.label === "Builder Profile" || row.label === "Architect Profile" || row.label === "Developer Profile") && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: "ml-40"
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("input", {
      className: "".concat(row.label === "Builder Profile" ? "ml-5" : "", "\n                          ").concat(row.label === "Architect Profile" ? "ml-3" : ""),
      type: "checkbox",
      checked: setCheckedState(row),
      onChange: function onChange(e) {
        changeInputType(row.label, e.target.checked);
      }
    }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
      className: "p-1"
    }, "Site URL")))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("tr", null, row.label === "Builder Profile" && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("td", {
      className: "pb-4 lg:px-4 pr:4 pr-2 w-1/2 "
    }, builderProfile === "file" ? /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("label", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: "py-2 px-4 cursor-pointer w-full ",
      style: {
        border: "2px dashed #ccc"
      }
    }, "Select File...", /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("input", {
      name: row.resource,
      className: "block hidden w-full",
      type: "file",
      onChange: function onChange(e) {
        handleFileChange(e, row.resource); // e.target.value = null;
      }
    }))) : /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_base__WEBPACK_IMPORTED_MODULE_4__["TextInput"], {
      className: "border-gray-400 border pl-2 py-1",
      border: false,
      width: "w-full",
      placeholder: "https://",
      onChange: function onChange(e) {
        setBuilderProfileLink(e.target.value);
      } // defaultValue={builderProfileLink}
      // name="builderProfileLink"

    })), row.label === "Developer Profile" && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("td", {
      className: "pb-4 lg:px-4 pr:4 pr-2 w-1/2 "
    }, devProfile === "file" ? /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("label", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: "py-2 px-4 cursor-pointer w-full ",
      style: {
        border: "2px dashed #ccc"
      }
    }, "Select File...", /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("input", {
      name: row.resource,
      className: "block hidden w-full",
      type: "file",
      onChange: function onChange(e) {
        handleFileChange(e, row.resource); // e.target.value = null;
      }
    }))) : /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_base__WEBPACK_IMPORTED_MODULE_4__["TextInput"], {
      className: "border-gray-400 border pl-2 py-1",
      border: false,
      width: "w-full",
      placeholder: "https://",
      onChange: function onChange(e) {
        setDevProfileLink(e.target.value);
      },
      defaultValue: devProfileLink
    })), row.label === "Architect Profile" && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("td", {
      className: "pb-4 lg:px-4 pr-4 pr-2 w-1/2 "
    }, arcProfile === "file" ? /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("label", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: "py-2 px-4 cursor-pointer w-full ",
      style: {
        border: "2px dashed #ccc"
      }
    }, "Select File...", /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("input", {
      name: row.resource,
      className: "block hidden w-full",
      type: "file",
      onChange: function onChange(e) {
        handleFileChange(e, row.resource); // e.target.value = null;
      }
    }))) : /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_base__WEBPACK_IMPORTED_MODULE_4__["TextInput"], {
      className: "border-gray-400 border pl-2 py-1",
      border: false,
      width: "w-full",
      placeholder: "https://",
      onChange: function onChange(e) {
        setArcProfileLink(e.target.value);
      }
    })), row.label === "Project Video" && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("td", {
      className: "pb-4 lg:px-4 pr:4 pr-2 w-1/2 "
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_base__WEBPACK_IMPORTED_MODULE_4__["TextInput"], {
      className: "border-gray-400 border pl-2 py-1",
      border: false,
      width: "w-full" // name="projectVideo"
      ,
      onChange: function onChange(e) {
        return setProjectVideo(e.target.value);
      },
      defaultValue: projectVideoDefaultValue
    })), row.label !== "Architect Profile" && row.label !== "Builder Profile" && row.label !== "Project Video" && row.label !== "Developer Profile" && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("td", {
      className: "pb-4 lg:px-4 pr:4 pr-2 w-1/2 "
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("label", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: "py-2 px-4 cursor-pointer w-full ",
      style: {
        border: "2px dashed #ccc"
      }
    }, "Select File...", /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("input", {
      name: row.resource,
      className: "block hidden w-full",
      type: "file",
      onChange: function onChange(e) {
        handleFileChange(e, row.resource); // e.target.value = null;
      }
    })))), row.label !== "Architect Profile" && row.label !== "Builder Profile" && row.label !== "Developer Profile" && row.label !== "Project Video" && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react__WEBPACK_IMPORTED_MODULE_0___default.a.Fragment, null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("td", {
      className: "pb-4 w-4/12 text-center "
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
      className: "pt-5 text-palette-blue-light"
    }, getFileName(row.resource))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("td", {
      className: "pb-4 w-1/12 text-center cursor-pointer"
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_fortawesome_react_fontawesome__WEBPACK_IMPORTED_MODULE_2__["FontAwesomeIcon"], {
      className: "text-lg text-red-500 hover:text-red-400",
      onClick: function onClick(e) {
        handleDelete(e, row.resource);
      },
      icon: _fortawesome_free_solid_svg_icons__WEBPACK_IMPORTED_MODULE_3__["faTimes"]
    }))), row.label === "Developer Profile" && devProfile === "file" && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react__WEBPACK_IMPORTED_MODULE_0___default.a.Fragment, null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("td", {
      className: "pb-4 w-4/12 text-center "
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
      className: "pt-5 text-palette-blue-light"
    }, getFileName(row.resource))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("td", {
      className: "pb-4 w-1/12 text-center cursor-pointer"
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_fortawesome_react_fontawesome__WEBPACK_IMPORTED_MODULE_2__["FontAwesomeIcon"], {
      className: "text-lg text-red-500 hover:text-red-400",
      onClick: function onClick(e) {
        handleDelete(e, row.resource);
      },
      icon: _fortawesome_free_solid_svg_icons__WEBPACK_IMPORTED_MODULE_3__["faTimes"]
    }))), row.label === "Architect Profile" && arcProfile === "file" && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react__WEBPACK_IMPORTED_MODULE_0___default.a.Fragment, null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("td", {
      className: "pb-4 w-4/12 text-center "
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
      className: "pt-5 text-palette-blue-light"
    }, getFileName(row.resource))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("td", {
      className: "pb-4 w-1/12 text-center cursor-pointer"
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_fortawesome_react_fontawesome__WEBPACK_IMPORTED_MODULE_2__["FontAwesomeIcon"], {
      className: "text-lg text-red-500 hover:text-red-400",
      onClick: function onClick(e) {
        handleDelete(e, row.resource);
      },
      icon: _fortawesome_free_solid_svg_icons__WEBPACK_IMPORTED_MODULE_3__["faTimes"]
    }))), row.label === "Builder Profile" && builderProfile === "file" && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react__WEBPACK_IMPORTED_MODULE_0___default.a.Fragment, null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("td", {
      className: "pb-4 w-4/12 text-center "
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
      className: "pt-5 text-palette-blue-light"
    }, getFileName(row.resource))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("td", {
      className: "pb-4 w-1/12 text-center cursor-pointer"
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_fortawesome_react_fontawesome__WEBPACK_IMPORTED_MODULE_2__["FontAwesomeIcon"], {
      className: "text-lg text-red-500 hover:text-red-400",
      onClick: function onClick(e) {
        handleDelete(e, row.resource);
      },
      icon: _fortawesome_free_solid_svg_icons__WEBPACK_IMPORTED_MODULE_3__["faTimes"]
    })))));
  });
};

/* harmony default export */ __webpack_exports__["default"] = (ResourcesData);

/***/ }),

/***/ "./resources/js/helpers/fileHelper/fileHelper.js":
/*!*******************************************************!*\
  !*** ./resources/js/helpers/fileHelper/fileHelper.js ***!
  \*******************************************************/
/*! exports provided: sanitizeFilename, imageFileTypeFilter, PDFFileTypeFilter */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "sanitizeFilename", function() { return sanitizeFilename; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "imageFileTypeFilter", function() { return imageFileTypeFilter; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PDFFileTypeFilter", function() { return PDFFileTypeFilter; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);

var sanitizeFilename = function sanitizeFilename(filename) {
  return filename.replace(/[^.a-zA-Z0-9]/g, "");
};
var imageFileTypeFilter = function imageFileTypeFilter(filename) {
  return filename.match(/\.(jpg|jpeg|png|gif)$/);
};
var PDFFileTypeFilter = function PDFFileTypeFilter(filename) {
  return filename.match(/\.(pdf)$/);
};

/***/ }),

/***/ "./resources/js/helpers/mapCoordinateHelper.js":
/*!*****************************************************!*\
  !*** ./resources/js/helpers/mapCoordinateHelper.js ***!
  \*****************************************************/
/*! exports provided: getMapCoordinates */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getMapCoordinates", function() { return getMapCoordinates; });
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime/regenerator */ "./node_modules/@babel/runtime/regenerator/index.js");
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var react_geocode__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! react-geocode */ "./node_modules/react-geocode/lib/index.js");
/* harmony import */ var react_geocode__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(react_geocode__WEBPACK_IMPORTED_MODULE_2__);


function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }



var getMapCoordinates = /*#__PURE__*/function () {
  var _ref = _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee(addressParam) {
    var coordinates, mapKey;
    return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            coordinates = {
              lat: "",
              "long": ""
            };
            mapKey = "".concat("AIzaSyB2angcqMN5xDkZHAzghZ5hlg-QldC02ww");
            react_geocode__WEBPACK_IMPORTED_MODULE_2___default.a.setApiKey(mapKey);
            _context.next = 5;
            return react_geocode__WEBPACK_IMPORTED_MODULE_2___default.a.fromAddress(addressParam).then(function (response) {
              var _response$results$0$g = response.results[0].geometry.location,
                  lat = _response$results$0$g.lat,
                  lng = _response$results$0$g.lng;
              coordinates.lat = lat;
              coordinates["long"] = lng;
            }, function (error) {});

          case 5:
            return _context.abrupt("return", coordinates);

          case 6:
          case "end":
            return _context.stop();
        }
      }
    }, _callee);
  }));

  return function getMapCoordinates(_x) {
    return _ref.apply(this, arguments);
  };
}();

/***/ }),

/***/ "./resources/js/helpers/pricingHelper/discountHelper.js":
/*!**************************************************************!*\
  !*** ./resources/js/helpers/pricingHelper/discountHelper.js ***!
  \**************************************************************/
/*! exports provided: discountedPrice, discountedPriceRange, prices */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "discountedPrice", function() { return discountedPrice; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "discountedPriceRange", function() { return discountedPriceRange; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "prices", function() { return prices; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react_currency_format__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react-currency-format */ "./node_modules/react-currency-format/lib/currency-format.js");
/* harmony import */ var react_currency_format__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react_currency_format__WEBPACK_IMPORTED_MODULE_1__);


var discountedPrice = function discountedPrice(discount_is_percentage, discount, property_price) {
  var newPrice = discount_is_percentage ? property_price - property_price * discount : property_price - discount;
  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_currency_format__WEBPACK_IMPORTED_MODULE_1___default.a, {
    value: newPrice,
    displayType: "text",
    thousandSeparator: true,
    prefix: "$"
  });
};
var discountedPriceRange = function discountedPriceRange(project, isMax) {
  var discountedPrice = 0;

  if (project.properties.length == 0) {
    return discountedPrice;
  }

  if (isMax) {
    var max = project.properties.reduce(function (a, b) {
      return b["price"] > a ? b["price"] : a;
    }, project.properties[0].price);
    discountedPrice = max - max * (project.discount / 100);
  } else {
    var min = project.properties.reduce(function (a, b) {
      return b["price"] < a ? b["price"] : a;
    }, project.properties[0].price);
    discountedPrice = min - min * (project.discount / 100);
  }

  return discountedPrice;
};
var prices = [50000, 100000, 150000, 200000, 250000, 300000, 350000, 400000, 450000, 500000, 550000, 600000, 650000, 700000, 750000, 800000, 850000, 900000, 950000, 1000000, 1100000, 1200000, 1300000, 1400000, 1500000, 1600000, 1700000, 1800000, 1900000, 2000000, 2100000, 2200000, 2300000, 2400000, 2500000, 2600000, 2700000, 2800000, 2900000, 3000000, 4000000, 5000000, 6000000, 7000000, 8000000, 9000000, 10000000, 11000000, 12000000, 13000000, 14000000, 15000000];

/***/ }),

/***/ "./resources/js/helpers/resourceHelper/index.js":
/*!******************************************************!*\
  !*** ./resources/js/helpers/resourceHelper/index.js ***!
  \******************************************************/
/*! exports provided: resources, tableRows, tableHeaders, landTableRows */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _resources__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./resources */ "./resources/js/helpers/resourceHelper/resources.js");
/* harmony reexport (module object) */ __webpack_require__.d(__webpack_exports__, "resources", function() { return _resources__WEBPACK_IMPORTED_MODULE_0__; });
/* harmony import */ var _tableHelper__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./tableHelper */ "./resources/js/helpers/resourceHelper/tableHelper.js");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "tableRows", function() { return _tableHelper__WEBPACK_IMPORTED_MODULE_1__["tableRows"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "tableHeaders", function() { return _tableHelper__WEBPACK_IMPORTED_MODULE_1__["tableHeaders"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "landTableRows", function() { return _tableHelper__WEBPACK_IMPORTED_MODULE_1__["landTableRows"]; });





/***/ }),

/***/ "./resources/js/helpers/resourceHelper/resources.js":
/*!**********************************************************!*\
  !*** ./resources/js/helpers/resourceHelper/resources.js ***!
  \**********************************************************/
/*! exports provided: resourcesArr, trimFilename, appendHttpsToResource, trimFloorPlan */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "resourcesArr", function() { return resourcesArr; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "trimFilename", function() { return trimFilename; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "appendHttpsToResource", function() { return appendHttpsToResource; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "trimFloorPlan", function() { return trimFloorPlan; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);


var mapResources = function mapResources(arr, name) {
  return arr.find(function (element) {
    return element.name === name;
  });
};

var resourcesArr = function resourcesArr(resourceArr) {
  return [{
    name: "project_brochure",
    label: "Project Brochure",
    link: resourceArr ? mapResources(resourceArr, "project_brochure") : null
  }, {
    name: "executive_summary",
    label: "Executive summary",
    link: resourceArr ? mapResources(resourceArr, "executive_summary") : null
  }, {
    name: "schedule_of_finishes",
    label: "Schedule of finishes",
    link: resourceArr ? mapResources(resourceArr, "schedule_of_finishes") : null
  }, {
    name: "developer_profile",
    label: "Developer profile",
    link: resourceArr ? mapResources(resourceArr, "developer_profile") : null
  }, {
    name: "builder_profile",
    label: "Builder profile",
    link: resourceArr ? mapResources(resourceArr, "builder_profile") : null
  }, {
    name: "architect_profile",
    label: "Architect profile",
    link: resourceArr ? mapResources(resourceArr, "architect_profile") : null
  }, {
    name: "project_video",
    label: "Project video",
    link: resourceArr ? mapResources(resourceArr, "project_video") : null
  }, {
    name: "floor_plates",
    label: "Floor plates",
    link: resourceArr ? mapResources(resourceArr, "floor_plates") : null
  }, {
    name: "floor_plan",
    label: "Floor plan",
    link: resourceArr ? mapResources(resourceArr, "floor_plan") : null
  }, {
    name: "master_contract",
    label: "Master contract",
    link: resourceArr ? mapResources(resourceArr, "master_contract") : null
  }, {
    name: "strata_plan",
    label: "Strata plan",
    link: resourceArr ? mapResources(resourceArr, "project_strata_plan") : null
  }, {
    name: "rental_estimate",
    label: "Rental estimate",
    link: resourceArr ? mapResources(resourceArr, "rental_estimate") : null
  }, {
    name: "depreciation_schedule",
    label: "Depreciation schedule",
    link: resourceArr ? mapResources(resourceArr, "depreciation_schedule") : null
  }, {
    name: "marketing_contract",
    label: "Marketing Contract",
    link: resourceArr ? mapResources(resourceArr, "marketing_contract") : null
  }, {
    name: "subdivision_plan",
    label: "DP/Subdivision Plan",
    link: resourceArr ? mapResources(resourceArr, "subdivision_plan") : null
  }, {
    name: "lot_plan",
    label: "Lot Plan",
    link: resourceArr ? mapResources(resourceArr, "lot_plan") : null
  }, {
    name: "draft_88b",
    label: "Draft 88b",
    link: resourceArr ? mapResources(resourceArr, "draft_88b") : null
  }, {
    name: "draft_contours",
    label: "Draft Contours",
    link: resourceArr ? mapResources(resourceArr, "draft_contours") : null
  }, {
    name: "draft_sewer_diagram",
    label: "Draft Sewer Diagram",
    link: resourceArr ? mapResources(resourceArr, "draft_sewer_diagram") : null
  }, {
    name: "build_envelope_plan",
    label: "Build Envelope Plan",
    link: resourceArr ? mapResources(resourceArr, "build_envelope_plan") : null
  }, {
    name: "estate_guidelines",
    label: "Estate Guidelines",
    link: resourceArr ? mapResources(resourceArr, "estate_guidelines") : null
  }, {
    name: "project_brochure",
    label: "Project Brochure",
    link: resourceArr ? mapResources(resourceArr, "project_brochure") : null
  }, {
    name: "location_map",
    label: "Location Map",
    link: resourceArr ? mapResources(resourceArr, "location_map") : null
  }, {
    name: "other",
    label: "Other",
    link: resourceArr ? mapResources(resourceArr, "other") : null
  }];
};
var trimFilename = function trimFilename(file, resource) {
  var selectedFile = file;
  var newFilename = selectedFile.name.replace(/[^.a-zA-Z0-9]/g, "");
  var newFileObj = new File([selectedFile], newFilename);
  return {
    name: resource,
    file: newFileObj
  };
};
var appendHttpsToResource = function appendHttpsToResource(link) {
  var baseString = "https://";
  return link.includes('https://') ? link : baseString.concat("", link);
};
var trimFloorPlan = function trimFloorPlan(file) {
  var selectedFile = file;
  var newFilename = selectedFile.name.replace(/[^.a-zA-Z0-9]/g, "");
  return new File([selectedFile], newFilename);
};

/***/ }),

/***/ "./resources/js/helpers/resourceHelper/tableHelper.js":
/*!************************************************************!*\
  !*** ./resources/js/helpers/resourceHelper/tableHelper.js ***!
  \************************************************************/
/*! exports provided: tableRows, landTableRows, tableHeaders */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "tableRows", function() { return tableRows; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "landTableRows", function() { return landTableRows; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "tableHeaders", function() { return tableHeaders; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);

var tableRows = [{
  label: "Executive Summary",
  resource: "executive_summary",
  fileName: "",
  icon: "times"
}, {
  label: "Schedule of Finishes",
  resource: "schedule_of_finishes",
  fileName: "",
  icon: "times"
}, {
  label: "Developer Profile",
  resource: "developer_profile",
  fileName: "",
  icon: "times"
}, {
  label: "Builder Profile",
  resource: "builder_profile",
  fileName: "",
  icon: "times"
}, {
  label: "Project Video",
  resource: "project_video",
  fileName: "",
  icon: "times"
}, {
  label: "Architect Profile",
  resource: "architect_profile",
  fileName: "",
  icon: "times"
}, {
  label: "Floor Plates",
  resource: "floor_plates",
  fileName: "",
  icon: "times"
}, {
  label: "Master Contract",
  resource: "master_contract",
  fileName: "",
  icon: "times"
}, {
  label: "Project Brochure",
  resource: "project_brochure",
  fileName: "",
  icon: "times"
}, {
  label: "Project Strata Plan",
  resource: "project_strata_plan",
  fileName: "",
  icon: "times"
}, {
  label: "Rental Estimate",
  resource: "rental_estimate",
  fileName: "",
  icon: "times"
}, {
  label: "Depreciation Schedule",
  resource: "depreciation_schedule",
  fileName: "",
  icon: "times"
}];
var landTableRows = [{
  label: "Developer Profile",
  resource: "developer_profile",
  fileName: "",
  icon: "times"
}, {
  label: "Marketing Contract",
  resource: "marketing_contract",
  fileName: "",
  icon: "times"
}, {
  label: "DP/Subdivision Plan",
  resource: "subdivision_plan",
  fileName: "",
  icon: "times"
}, {
  label: "Lot Plan",
  resource: "lot_plan",
  fileName: "",
  icon: "times"
}, {
  label: "Draft 88b",
  resource: "draft_88b",
  fileName: "",
  icon: "times"
}, {
  label: "Draft Contours",
  resource: "draft_contours",
  fileName: "",
  icon: "times"
}, {
  label: "Draft Sewer Diagram",
  resource: "draft_sewer_diagram",
  fileName: "",
  icon: "times"
}, {
  label: "Build Envelope Plan",
  resource: "build_envelope_plan",
  fileName: "",
  icon: "times"
}, {
  label: "Estate Guidelines",
  resource: "estate_guidelines",
  fileName: "",
  icon: "times"
}, {
  label: "Project Brochure",
  resource: "project_brochure",
  fileName: "",
  icon: "times"
}, {
  label: "Location Map",
  resource: "location_map",
  fileName: "",
  icon: "times"
}, {
  label: "Project Video",
  resource: "project_video",
  fileName: "",
  icon: "times"
}, {
  label: "Other",
  resource: "other",
  fileName: "",
  icon: "times"
}];
var tableHeaders = [{
  label: "Project Resources",
  style: "",
  width: "w-1/2"
}, {
  label: "Document Name",
  style: "text-center",
  width: "w-4/12"
}, {
  label: "Delete",
  style: "text-center",
  width: "w-1/12"
}];

/***/ }),

/***/ "./resources/js/helpers/salesAdviceHelper/salesAdviceHelper.js":
/*!*********************************************************************!*\
  !*** ./resources/js/helpers/salesAdviceHelper/salesAdviceHelper.js ***!
  \*********************************************************************/
/*! exports provided: formDataFieldsValidator, fieldValidator, addtionalInfoFields, defaultPurchasers, titles, solicitorDetailsFields, purchasersMapper, additionalInfoMapper, solicitorDetailsMapper */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "formDataFieldsValidator", function() { return formDataFieldsValidator; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "fieldValidator", function() { return fieldValidator; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "addtionalInfoFields", function() { return addtionalInfoFields; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "defaultPurchasers", function() { return defaultPurchasers; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "titles", function() { return titles; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "solicitorDetailsFields", function() { return solicitorDetailsFields; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "purchasersMapper", function() { return purchasersMapper; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "additionalInfoMapper", function() { return additionalInfoMapper; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "solicitorDetailsMapper", function() { return solicitorDetailsMapper; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _countries__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../countries */ "./resources/js/helpers/countries.js");
/* harmony import */ var _propertyHelper_propertyHelper__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../propertyHelper/propertyHelper */ "./resources/js/helpers/propertyHelper/propertyHelper.js");
/* harmony import */ var lodash_isEmpty__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! lodash/isEmpty */ "./node_modules/lodash/isEmpty.js");
/* harmony import */ var lodash_isEmpty__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(lodash_isEmpty__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var uuid__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! uuid */ "./node_modules/uuid/dist/esm-browser/index.js");
function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _unsupportedIterableToArray(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _iterableToArrayLimit(arr, i) { if (typeof Symbol === "undefined" || !(Symbol.iterator in Object(arr))) return; var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }

function _createForOfIteratorHelper(o) { if (typeof Symbol === "undefined" || o[Symbol.iterator] == null) { if (Array.isArray(o) || (o = _unsupportedIterableToArray(o))) { var i = 0; var F = function F() {}; return { s: F, n: function n() { if (i >= o.length) return { done: true }; return { done: false, value: o[i++] }; }, e: function e(_e2) { throw _e2; }, f: F }; } throw new TypeError("Invalid attempt to iterate non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); } var it, normalCompletion = true, didErr = false, err; return { s: function s() { it = o[Symbol.iterator](); }, n: function n() { var step = it.next(); normalCompletion = step.done; return step; }, e: function e(_e3) { didErr = true; err = _e3; }, f: function f() { try { if (!normalCompletion && it["return"] != null) it["return"](); } finally { if (didErr) throw err; } } }; }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }






var formDataFieldsValidator = function formDataFieldsValidator(formData) {
  var hasError = false;

  var _iterator = _createForOfIteratorHelper(formData.entries()),
      _step;

  try {
    for (_iterator.s(); !(_step = _iterator.n()).done;) {
      var f = _step.value;

      if (!f[1]) {
        hasError = true;
      }
    }
  } catch (err) {
    _iterator.e(err);
  } finally {
    _iterator.f();
  }

  return hasError;
};
var fieldValidator = function fieldValidator(paramObj) {
  var errors = [];

  if (Array.isArray(paramObj)) {
    paramObj.forEach(function (purchaser) {
      var err = {};
      Object.entries(purchaser).forEach(function (_ref) {
        var _ref2 = _slicedToArray(_ref, 2),
            key = _ref2[0],
            value = _ref2[1];

        if (key !== 'id' && key !== 'alternate_phone' && key !== 'address_line_1' && key !== 'first_name' && key !== 'last_name' && !value) {
          err[key] = "The ".concat(key, " is required!");
        }

        if (key === 'first_name' && !value) {
          err[key] = "The first name is required!";
        }

        if (key === 'address_line_1' && !value) {
          err[key] = "The address line 1 is required!";
        }

        if (key === 'last_name' && !value) {
          err[key] = "The last name is required!";
        }

        if (key == 'phone' && typeof parseInt(value) != 'number') {
          err[key] = "The ".concat(key, " accepts numeric input only!");
        }
      });

      if (!Object(lodash_isEmpty__WEBPACK_IMPORTED_MODULE_3__["isEmpty"])(err)) {
        err.uid = purchaser.uid;
        errors.push(err);
      }
    });
  }

  return errors;
};
var addtionalInfoFields = {
  heard_about_us: {
    label: 'How did you hear about us?',
    type: 'radio',
    classname: "ml-20 mt-2",
    value: "SocialMedia",
    options: {
      SocialMedia: 'Social Media',
      Google: 'Google',
      Friend: 'Friend'
    }
  },
  other: {
    label: 'Other'
  },
  purpose: {
    label: 'Purpose?',
    type: 'radio',
    classname: "ml-20 mt-2",
    value: "OwnerOccupier",
    options: {
      OwnerOccupier: 'Owner occupier',
      Investor: 'Investor'
    }
  },
  isFirstHomeBuyer: {
    label: 'First Home Buyer?',
    type: 'radio',
    classname: "ml-20 mt-2",
    value: "No",
    options: {
      Yes: 'Yes',
      No: 'No'
    }
  },
  isPreviousBoughtPlan: {
    label: 'Previously bought off the plan?',
    type: 'radio',
    classname: "ml-20 mt-2",
    value: "No",
    options: {
      Yes: 'Yes',
      No: 'No'
    }
  },
  isFIRB_required: {
    label: 'FIRB required?',
    type: 'radio',
    classname: "ml-20 mt-2",
    value: "No",
    options: {
      Yes: 'Yes',
      No: 'No'
    }
  },
  comment: {
    label: 'Comment',
    type: 'textarea',
    placeholder: ''
  }
};
var defaultPurchasers = [{
  uid: Object(uuid__WEBPACK_IMPORTED_MODULE_4__["v4"])(),
  id: '',
  title: '',
  first_name: '',
  last_name: '',
  suburb: '',
  address_line_1: '',
  postcode: '',
  state: '',
  country: '',
  phone: '',
  alternate_phone: '',
  email: ''
}];
var titles = [{
  value: 'Mr.',
  label: 'Mr.'
}, {
  value: 'Ms.',
  label: 'Ms.'
}, {
  value: 'Mrs.',
  label: 'Mrs.'
}];
var solicitorDetailsFields = {
  company: {
    label: 'Company',
    required: true
  },
  contact: {
    label: 'Contact',
    // prefix: '+61',
    // maxlength: '10',
    required: true
  },
  phone: {
    label: 'Phone',
    required: true,
    prefix: '+61',
    maxlength: '10',
    type: 'number'
  },
  email: {
    label: 'Email',
    required: true
  },
  poBox: {
    label: 'PO Box',
    required: true,
    placeholder: 'Enter Address/PO Box'
  },
  suburb: {
    label: 'Suburb',
    required: true
  },
  postcode: {
    label: 'Postcode',
    required: true,
    placeholder: 'Enter Postcode/ZIP'
  },
  state: {
    label: 'State',
    required: true,
    type: 'select',
    placeholder: 'Select State',
    options: _propertyHelper_propertyHelper__WEBPACK_IMPORTED_MODULE_2__["states"]
  },
  country: {
    label: 'Country',
    required: true,
    type: 'select',
    placeholder: 'Select Country',
    options: _countries__WEBPACK_IMPORTED_MODULE_1__["countries"]
  }
};
var purchasersMapper = function purchasersMapper(purchasersParam) {
  var newArr = [];

  if (purchasersParam.length > 0) {
    purchasersParam.map(function (p) {
      var address = !Object(lodash_isEmpty__WEBPACK_IMPORTED_MODULE_3__["isEmpty"])(p.address) ? JSON.parse(p.address) : "";
      var obj = new Object({
        uid: Object(uuid__WEBPACK_IMPORTED_MODULE_4__["v4"])(),
        id: p.id,
        title: p.title,
        first_name: p.first_name,
        last_name: p.last_name,
        suburb: address.suburb,
        address_line_1: address.address_line_1,
        postcode: address.postcode,
        state: address.state,
        country: address.country,
        phone: p.phone ? p.phone.replace("+61", "") : '',
        alternate_phone: p.alternate_phone ? p.alternate_phone.replace("+61", "") : '',
        email: p.email
      });
      newArr.push(obj);
    });
  }

  return newArr;
}; //

var additionalInfoMapper = function additionalInfoMapper(AdditionalInfoDetails) {
  if (Object(lodash_isEmpty__WEBPACK_IMPORTED_MODULE_3__["isEmpty"])(AdditionalInfoDetails)) return;
  Object.keys(addtionalInfoFields).forEach(function (key) {
    if (AdditionalInfoDetails.hasOwnProperty(key)) {
      addtionalInfoFields[key].value = AdditionalInfoDetails[key];
    }
  });
  return addtionalInfoFields;
};
var solicitorDetailsMapper = function solicitorDetailsMapper(solicitorDetailsParam) {
  if (!Object(lodash_isEmpty__WEBPACK_IMPORTED_MODULE_3__["isEmpty"])(solicitorDetailsParam)) {
    var address = JSON.parse(solicitorDetailsParam.address);
    Object.keys(solicitorDetailsFields).forEach(function (key) {
      if (key === 'suburb') {
        solicitorDetailsFields[key].value = address[key];
      } else if (key === 'state') {
        solicitorDetailsFields[key].value = address[key];
      } else if (key === 'country') {
        solicitorDetailsFields[key].value = address[key];
      } else if (key === 'postcode') {
        solicitorDetailsFields[key].value = address[key];
      } else if (key === 'phone') {
        solicitorDetailsFields[key].value = solicitorDetailsParam[key] ? solicitorDetailsParam[key].replace("+61", "") : '';
      } else {
        solicitorDetailsFields[key].value = solicitorDetailsParam[key];
      }
    });
  }

  return solicitorDetailsFields;
};

/***/ }),

/***/ "./resources/js/helpers/stateAbbreviation.js":
/*!***************************************************!*\
  !*** ./resources/js/helpers/stateAbbreviation.js ***!
  \***************************************************/
/*! exports provided: stateAbbreviation, shortIntro */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "stateAbbreviation", function() { return stateAbbreviation; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "shortIntro", function() { return shortIntro; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! lodash */ "./node_modules/lodash/lodash.js");
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(lodash__WEBPACK_IMPORTED_MODULE_1__);


var stateAbbreviation = function stateAbbreviation(state) {
  if (!state) return;
  state = state.toLowerCase();
  var stateAbbr;

  switch (state) {
    case 'new south wales':
      stateAbbr = 'NSW';
      break;

    case 'queensland':
      stateAbbr = 'QLD';
      break;

    case 'south australia':
      stateAbbr = 'SA';
      break;

    case 'tasmania':
      stateAbbr = 'TAS';
      break;

    case 'victoria':
      stateAbbr = 'VIC';
      break;

    case 'australian capital territory':
      stateAbbr = 'ACT';
      break;

    case 'northern territory':
      stateAbbr = 'NT';
      break;

    case 'wester australia':
      stateAbbr = 'WA';
      break;

    default:
      stateAbbr = state.toUpperCase();
      break;
  }

  return stateAbbr;
};
var shortIntro = function shortIntro(desc) {
  if (!desc) return;
  var initialStr = desc.split('</strong>')[0];

  var wordCount = _.size(_.words(initialStr));

  if (wordCount > 23) {
    return initialStr.split(' ').slice(0, 19).join(' ') + '...';
  } else {
    return initialStr;
  }
};

/***/ }),

/***/ "./resources/js/helpers/styles/datepicker.css":
/*!****************************************************!*\
  !*** ./resources/js/helpers/styles/datepicker.css ***!
  \****************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {


var content = __webpack_require__(/*! !../../../../node_modules/css-loader??ref--6-1!../../../../node_modules/postcss-loader/src??ref--6-2!./datepicker.css */ "./node_modules/css-loader/index.js?!./node_modules/postcss-loader/src/index.js?!./resources/js/helpers/styles/datepicker.css");

if(typeof content === 'string') content = [[module.i, content, '']];

var transform;
var insertInto;



var options = {"hmr":true}

options.transform = transform
options.insertInto = undefined;

var update = __webpack_require__(/*! ../../../../node_modules/style-loader/lib/addStyles.js */ "./node_modules/style-loader/lib/addStyles.js")(content, options);

if(content.locals) module.exports = content.locals;

if(false) {}

/***/ }),

/***/ "./resources/js/helpers/styles/dropdown.css":
/*!**************************************************!*\
  !*** ./resources/js/helpers/styles/dropdown.css ***!
  \**************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {


var content = __webpack_require__(/*! !../../../../node_modules/css-loader??ref--6-1!../../../../node_modules/postcss-loader/src??ref--6-2!./dropdown.css */ "./node_modules/css-loader/index.js?!./node_modules/postcss-loader/src/index.js?!./resources/js/helpers/styles/dropdown.css");

if(typeof content === 'string') content = [[module.i, content, '']];

var transform;
var insertInto;



var options = {"hmr":true}

options.transform = transform
options.insertInto = undefined;

var update = __webpack_require__(/*! ../../../../node_modules/style-loader/lib/addStyles.js */ "./node_modules/style-loader/lib/addStyles.js")(content, options);

if(content.locals) module.exports = content.locals;

if(false) {}

/***/ }),

/***/ "./resources/js/helpers/styles/toast.css":
/*!***********************************************!*\
  !*** ./resources/js/helpers/styles/toast.css ***!
  \***********************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {


var content = __webpack_require__(/*! !../../../../node_modules/css-loader??ref--6-1!../../../../node_modules/postcss-loader/src??ref--6-2!./toast.css */ "./node_modules/css-loader/index.js?!./node_modules/postcss-loader/src/index.js?!./resources/js/helpers/styles/toast.css");

if(typeof content === 'string') content = [[module.i, content, '']];

var transform;
var insertInto;



var options = {"hmr":true}

options.transform = transform
options.insertInto = undefined;

var update = __webpack_require__(/*! ../../../../node_modules/style-loader/lib/addStyles.js */ "./node_modules/style-loader/lib/addStyles.js")(content, options);

if(content.locals) module.exports = content.locals;

if(false) {}

/***/ }),

/***/ "./resources/js/layouts/users/Footer.js":
/*!**********************************************!*\
  !*** ./resources/js/layouts/users/Footer.js ***!
  \**********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _components__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../components */ "./resources/js/components/index.js");
/* harmony import */ var _components__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_components__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _states_userGlobal__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../states/userGlobal */ "./resources/js/states/userGlobal.js");
/* harmony import */ var react_device_detect__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! react-device-detect */ "./node_modules/react-device-detect/main.js");
/* harmony import */ var react_device_detect__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(react_device_detect__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! moment */ "./node_modules/moment/moment.js");
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(moment__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var _helpers_deviceSizeHelper__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../helpers/deviceSizeHelper */ "./resources/js/helpers/deviceSizeHelper.js");
function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _unsupportedIterableToArray(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

function _iterableToArrayLimit(arr, i) { if (typeof Symbol === "undefined" || !(Symbol.iterator in Object(arr))) return; var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }








var Footer = function Footer(_ref) {
  var topFooter = _ref.topFooter;

  var _UserGlobal = Object(_states_userGlobal__WEBPACK_IMPORTED_MODULE_2__["default"])(),
      _UserGlobal2 = _slicedToArray(_UserGlobal, 2),
      userState = _UserGlobal2[0],
      userAction = _UserGlobal2[1];

  var gBuyerContactDetail = {
    address: "Level 27, 20 Bond St, Sydney NSW Australia 2000",
    poBox: "1300 031 835",
    email: "info@groupbuyer.com.au"
  };
  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("footer", {
    name: "footer",
    className: "bg-palette-blue-dark"
  }, topFooter && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react__WEBPACK_IMPORTED_MODULE_0___default.a.Fragment, null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "mx-auto pt-4",
    style: {
      maxWidth: 1366
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "px-4 pt-16 pb-2 mx-auto text-white bg-palette-blue-dark lg:flex lg:px-16"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "flex-col lg:pr-16 lg:w-5/12"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("h2", {
    className: "mb-4 text-3xl font-bold text-center lg:text-left lg:text-5xl lg:mb-8"
  }, "Questions?"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("p", {
    className: "px-4 mb-8 text-lg text-center lg:px-0 lg:text-left"
  }, "Check out our FAQ page\xA0", /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("a", {
    href: "/frequently-asked-questions",
    className: "text-palette-teal underline"
  }, "here"), "\xA0 for all frequently asked questions or feel free to get in touch directly with one of our team members."), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("h2", {
    className: "mb-2 text-xl text-center lg:block lg:text-left"
  }, "Connect with us"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components__WEBPACK_IMPORTED_MODULE_1__["SocialMedia"], {
    className: "lg:flex flex-row mb-6 lg:justify-start justify-center",
    size: "2x"
  })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "text-lg lg:mb-0 lg:py-8"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "lg:text-left text-center"
  }, gBuyerContactDetail.address), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "font-bold flex flex-col lg:flex-row lg:text-left text-center"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", null, "P:\xA0", /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("a", {
    className: "text-blue-500 hover:text-blue-300",
    href: "tel:1300031835"
  }, gBuyerContactDetail.poBox)), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
    className: "hidden lg:inline"
  }, "\xA0\u2022\xA0"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", null, "E:\xA0", /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("a", {
    className: "text-blue-500 hover:text-blue-300",
    href: "mailto:info@groupbuyer.com.au"
  }, gBuyerContactDetail.email))))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "flex-col lg:text-left lg:w-7/12"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components__WEBPACK_IMPORTED_MODULE_1__["ContactForm"], {
    backgroundColor: react_device_detect__WEBPACK_IMPORTED_MODULE_3__["isMobile"] ? "transparent" : "#000024"
  }))))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    style: {
      maxWidth: 1366
    },
    className: "flex flex-col flex-wrap items-center justify-center lg:flex-row lg:px-16 mx-auto px-4 lg:py-16 text-gray-500"
  }, !react_device_detect__WEBPACK_IMPORTED_MODULE_3__["isMobile"] && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "flex items-center lg:inline-block lg:py-12 lg:w-1/4"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("a", {
    href: "/terms-and-conditions"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
    className: "mr-4 text-sm transition-all duration-300 cursor-pointer hover:text-palette-teal" // onClick={() => userAction.setState({ showTermsAndConditions: true })}

  }, "Terms & Conditions")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("a", {
    href: "/privacy-policy"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
    className: "text-sm transition-all duration-300 cursor-pointer hover:text-palette-teal" // onClick={() => userAction.setState({ showPrivacyPolicy: true })}

  }, "Privacy Policy"))), !react_device_detect__WEBPACK_IMPORTED_MODULE_3__["isMobile"] && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "flex flex-col flex-wrap items-center justify-center py-4 text-xs lg:py-0 lg:flex-row lg:w-1/2"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
    className: "text-sm"
  }, "\xA9 GroupBuyer ", moment__WEBPACK_IMPORTED_MODULE_4___default()().format("YYYY"), "\xA0|\xA0"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", null, "\xA0"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
    className: "text-sm"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", null, "Founded by ", /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("strong", null, "\xA0Ben Daniel\xA0")))), react_device_detect__WEBPACK_IMPORTED_MODULE_3__["isMobile"] && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react__WEBPACK_IMPORTED_MODULE_0___default.a.Fragment, null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "flex flex-col flex-wrap items-center justify-center py-4 text-xs lg:py-0 lg:flex-row lg:w-1/2"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "flex items-center lg:inline-block lg:py-12 lg:w-1/4 mb-1"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("a", {
    href: "/terms-and-conditions"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
    className: "mr-4 text-sm transition-all duration-300 cursor-pointer hover:text-palette-teal" // onClick={() => userAction.setState({ showTermsAndConditions: true })}

  }, "Terms & Conditions")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("a", {
    href: "/privacy-policy"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
    className: "text-sm transition-all duration-300 cursor-pointer hover:text-palette-teal" // onClick={() => userAction.setState({ showPrivacyPolicy: true })}

  }, "Privacy Policy"))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
    className: "text-sm mb-1"
  }, "\xA9 GroupBuyer ", moment__WEBPACK_IMPORTED_MODULE_4___default()().format("YYYY"), "\xA0|\xA0", /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", null, "Founded by ", /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("strong", null, "Ben Daniel"), "\xA0")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
    className: "mvFooterText"
  }))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "items-center hidden lg:inline-block lg:py-12 lg:w-1/4"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components__WEBPACK_IMPORTED_MODULE_1__["SocialMedia"], {
    className: "justify-end",
    variant: "secondary"
  }))));
};

/* harmony default export */ __webpack_exports__["default"] = (Footer);

/***/ }),

/***/ "./resources/js/layouts/users/Header.js":
/*!**********************************************!*\
  !*** ./resources/js/layouts/users/Header.js ***!
  \**********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime/regenerator */ "./node_modules/@babel/runtime/regenerator/index.js");
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var hookrouter__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! hookrouter */ "./node_modules/hookrouter/dist/index.js");
/* harmony import */ var hookrouter__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(hookrouter__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _states_userGlobal__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../states/userGlobal */ "./resources/js/states/userGlobal.js");
/* harmony import */ var _services_auth__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../services/auth */ "./resources/js/services/auth.js");
/* harmony import */ var _material_ui_core__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @material-ui/core */ "./node_modules/@material-ui/core/esm/index.js");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! sweetalert2 */ "./node_modules/sweetalert2/dist/sweetalert2.all.js");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(sweetalert2__WEBPACK_IMPORTED_MODULE_6__);
/* harmony import */ var react_device_detect__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! react-device-detect */ "./node_modules/react-device-detect/main.js");
/* harmony import */ var react_device_detect__WEBPACK_IMPORTED_MODULE_7___default = /*#__PURE__*/__webpack_require__.n(react_device_detect__WEBPACK_IMPORTED_MODULE_7__);
/* harmony import */ var _components_base__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../../components/_base */ "./resources/js/components/_base/index.js");
/* harmony import */ var _components_base__WEBPACK_IMPORTED_MODULE_8___default = /*#__PURE__*/__webpack_require__.n(_components_base__WEBPACK_IMPORTED_MODULE_8__);
/* harmony import */ var _components__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../../components */ "./resources/js/components/index.js");
/* harmony import */ var _components__WEBPACK_IMPORTED_MODULE_9___default = /*#__PURE__*/__webpack_require__.n(_components__WEBPACK_IMPORTED_MODULE_9__);
/* harmony import */ var react_currency_format__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! react-currency-format */ "./node_modules/react-currency-format/lib/currency-format.js");
/* harmony import */ var react_currency_format__WEBPACK_IMPORTED_MODULE_10___default = /*#__PURE__*/__webpack_require__.n(react_currency_format__WEBPACK_IMPORTED_MODULE_10__);


function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _unsupportedIterableToArray(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

function _iterableToArrayLimit(arr, i) { if (typeof Symbol === "undefined" || !(Symbol.iterator in Object(arr))) return; var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }

function _objectDestructuringEmpty(obj) { if (obj == null) throw new TypeError("Cannot destructure undefined"); }












var Header = function Header(_ref) {
  _objectDestructuringEmpty(_ref);

  var path = Object(hookrouter__WEBPACK_IMPORTED_MODULE_2__["usePath"])();
  var anchorRef = Object(react__WEBPACK_IMPORTED_MODULE_1__["useRef"])(null);

  var _UserGlobal = Object(_states_userGlobal__WEBPACK_IMPORTED_MODULE_3__["default"])(),
      _UserGlobal2 = _slicedToArray(_UserGlobal, 2),
      userState = _UserGlobal2[0],
      userAction = _UserGlobal2[1];

  var _useState = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(null),
      _useState2 = _slicedToArray(_useState, 2),
      popper = _useState2[0],
      setPopper = _useState2[1];

  var _useState3 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(false),
      _useState4 = _slicedToArray(_useState3, 2),
      showMenu = _useState4[0],
      setShowMenu = _useState4[1];

  var links = [{
    name: "FAQ",
    route: "/frequently-asked-questions",
    show: true
  }, {
    name: "How it works",
    route: "/how-it-works",
    show: true
  }, // {
  //   name: "About",
  //   route: "/about"
  // },
  {
    name: "Properties",
    route: "/weekly-deals",
    show: true
  }, {
    name: "Cars",
    route: "/cars-inventory",
    show: Object(_services_auth__WEBPACK_IMPORTED_MODULE_4__["isLoggedIn"])() && Object(_services_auth__WEBPACK_IMPORTED_MODULE_4__["userRole"])() === "admin" ? true : false
  }, // {
  //   name: "Selling",
  //   route: "/selling"
  // },
  {
    name: "Contact",
    route: "/contact",
    show: true
  }];

  var handleLogout = /*#__PURE__*/function () {
    var _ref2 = _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee2() {
      return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee2$(_context2) {
        while (1) {
          switch (_context2.prev = _context2.next) {
            case 0:
              if (!react_device_detect__WEBPACK_IMPORTED_MODULE_7__["isMobile"]) {
                _context2.next = 12;
                break;
              }

              _context2.prev = 1;
              _context2.next = 4;
              return axios.post("/api/logout", null, {
                headers: {
                  Authorization: "Bearer " + Object(_services_auth__WEBPACK_IMPORTED_MODULE_4__["isLoggedIn"])()
                }
              });

            case 4:
              Object(_services_auth__WEBPACK_IMPORTED_MODULE_4__["logout"])();
              setUserRole("");
              userAction.setState({
                fetch: !userState.fetch
              });
              _context2.next = 11;
              break;

            case 9:
              _context2.prev = 9;
              _context2.t0 = _context2["catch"](1);

            case 11:
              return _context2.abrupt("return");

            case 12:
              sweetalert2__WEBPACK_IMPORTED_MODULE_6___default.a.fire({
                title: "Confirmation",
                text: "Are you sure you want to sign out?",
                showConfirmButton: true,
                showCloseButton: true,
                showCancelButton: true,
                focusConfirm: false,
                confirmButtonColor: "#BD00F2",
                confirmButtonText: "Confirm"
              }).then( /*#__PURE__*/function () {
                var _ref3 = _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee(result) {
                  return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee$(_context) {
                    while (1) {
                      switch (_context.prev = _context.next) {
                        case 0:
                          if (!result.value) {
                            _context.next = 10;
                            break;
                          }

                          _context.prev = 1;
                          _context.next = 4;
                          return axios.post("/api/logout", null, {
                            headers: {
                              Authorization: "Bearer " + Object(_services_auth__WEBPACK_IMPORTED_MODULE_4__["isLoggedIn"])()
                            }
                          });

                        case 4:
                          Object(_services_auth__WEBPACK_IMPORTED_MODULE_4__["logout"])();
                          userAction.setState({
                            fetch: !userState.fetch
                          });
                          _context.next = 10;
                          break;

                        case 8:
                          _context.prev = 8;
                          _context.t0 = _context["catch"](1);

                        case 10:
                        case "end":
                          return _context.stop();
                      }
                    }
                  }, _callee, null, [[1, 8]]);
                }));

                return function (_x) {
                  return _ref3.apply(this, arguments);
                };
              }());

            case 13:
            case "end":
              return _context2.stop();
          }
        }
      }, _callee2, null, [[1, 9]]);
    }));

    return function handleLogout() {
      return _ref2.apply(this, arguments);
    };
  }();

  var navLinks = function navLinks() {
    return links.map(function (link, key) {
      return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("a", {
        key: key,
        href: link.route,
        className: "\n            ".concat(path !== "/" ? "opacity-50" : "opacity-100", "\n            ").concat(path.includes(link.route) ? "text-palette-teal opacity-100" : "text-white", " mb-6 lg:mb-0\n            hover:text-palette-teal px-3 tracking-wide ").concat(!react_device_detect__WEBPACK_IMPORTED_MODULE_7__["isMobile"] ? "text-base" : "text-2xl lg:text-base", " font-bold transition-all duration-300\n            ").concat(link.show ? "" : "hidden", "\n          ")
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", null, link.name));
    });
  };

  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("header", {
    style: {
      maxWidth: 1366
    },
    className: "relative z-50 flex items-center mx-auto justify-center lg:justify-between px-6 lg:px-4 lg:pt-10"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("a", {
    href: "/"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("img", {
    // src="/assets/images/group_buyer_logo_white.png"
    src: "/assets/svg/upload_logo_min_negative.svg",
    style: {
      height: 90
    }
  })), !react_device_detect__WEBPACK_IMPORTED_MODULE_7__["isMobile"] && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "border border-bac flex font-bold h-8 items-center justify-center ml-3 mt-1 px-4 rounded-full lg:text-base text-palette-teal w-auto"
  }, "Members:\xA0", /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(react_currency_format__WEBPACK_IMPORTED_MODULE_10___default.a, {
    value: userState.userCount,
    displayType: "text",
    thousandSeparator: true // prefix={`$`}

  })), !react_device_detect__WEBPACK_IMPORTED_MODULE_7__["isMobile"] ? /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "flex items-center text-white"
  }, navLinks(), !Object(_services_auth__WEBPACK_IMPORTED_MODULE_4__["isLoggedIn"])() ? /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
    onClick: function onClick() {
      return userAction.setState({
        showSignIn: true
      });
    },
    className: "cursor-pointer hover:text-palette-teal px-3 tracking-wide lg:text-base text-white font-bold transition-all duration-300"
  }, "Log in") : /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "flex items-center"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "border-l border-gray-500 mr-6 ml-3",
    style: {
      width: 1,
      height: 20
    }
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
    onMouseOver: function onMouseOver(e) {
      return setPopper(e.currentTarget);
    },
    onMouseLeave: function onMouseLeave() {
      return setPopper(null);
    }
  }, userState.user && userState.user.avatar_path ? /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
    className: "flex items-center cursor-pointer"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
    className: "tracking-wide lg:text-base font-bold"
  }, userState.user && userState.user.first_name), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_material_ui_core__WEBPACK_IMPORTED_MODULE_5__["Avatar"], {
    ref: anchorRef,
    src: "".concat(userState.user.avatar_path),
    className: "cursor-pointer ml-2"
  })) : /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
    className: "flex items-center cursor-pointer"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
    className: "tracking-wide lg:text-base font-bold"
  }, userState.user && userState.user.first_name), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("svg", {
    className: "feather-icon h-6 w-6 ml-2",
    ref: anchorRef
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("use", {
    xlinkHref: "/assets/svg/feather-sprite.svg#user"
  }))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_material_ui_core__WEBPACK_IMPORTED_MODULE_5__["Popper"], {
    open: Boolean(popper),
    anchorEl: popper,
    placement: "bottom-end",
    transition: true,
    disablePortal: true,
    className: "-mr-4 mt-3 menu-list"
  }, function (_ref4) {
    var TransitionProps = _ref4.TransitionProps;
    return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_material_ui_core__WEBPACK_IMPORTED_MODULE_5__["Grow"], _extends({}, TransitionProps, {
      style: {
        transformOrigin: "right top"
      }
    }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_material_ui_core__WEBPACK_IMPORTED_MODULE_5__["Paper"], {
      elevation: 4,
      style: {
        width: 360
      },
      className: "relative"
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
      className: "arrow absolute",
      style: {
        top: "-6px",
        right: "24px",
        borderWidth: "0 0.8em 0.8em 0.8em",
        borderColor: "transparent transparent white transparent"
      }
    }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_material_ui_core__WEBPACK_IMPORTED_MODULE_5__["MenuList"], null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
      className: "flex items-center px-4 pt-3 pb-4 mb-2 border-b"
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
      className: "flex flex-col"
    }, userState.user && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(react__WEBPACK_IMPORTED_MODULE_1___default.a.Fragment, null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
      className: "block lg:text-base font-bold text-xl leading-none"
    }, userState.user.first_name + " " + userState.user.last_name), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
      className: "lg:text-base block\n                                    "
    }, userState.user.email)))), (Object(_services_auth__WEBPACK_IMPORTED_MODULE_4__["userRole"])() === "admin" || Object(_services_auth__WEBPACK_IMPORTED_MODULE_4__["userRole"])() === "super_admin") && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_material_ui_core__WEBPACK_IMPORTED_MODULE_5__["MenuItem"], {
      onClick: function onClick() {
        return window.location = "/admin/projects";
      }
    }, "Admin"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_material_ui_core__WEBPACK_IMPORTED_MODULE_5__["MenuItem"], {
      onClick: function onClick() {
        return window.location = "/profile/account-settings";
      }
    }, "My Profile"), (Object(_services_auth__WEBPACK_IMPORTED_MODULE_4__["userRole"])() === "admin" || Object(_services_auth__WEBPACK_IMPORTED_MODULE_4__["userRole"])() === "super_admin" || Object(_services_auth__WEBPACK_IMPORTED_MODULE_4__["userRole"])() === "project_developer") && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_material_ui_core__WEBPACK_IMPORTED_MODULE_5__["MenuItem"], {
      onClick: function onClick() {
        return window.location = "/profile/manage-projects";
      }
    }, "Manage projects"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_material_ui_core__WEBPACK_IMPORTED_MODULE_5__["MenuItem"], {
      onClick: function onClick() {
        return window.location = "/profile/secured-deals";
      }
    }, "Secured deals"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_material_ui_core__WEBPACK_IMPORTED_MODULE_5__["MenuItem"], {
      onClick: function onClick() {
        return window.location = "/profile/wish-list";
      }
    }, "Wish list"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_material_ui_core__WEBPACK_IMPORTED_MODULE_5__["MenuItem"], {
      onClick: function onClick() {
        return handleLogout();
      }
    }, "Sign out"))));
  })))) : /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(react__WEBPACK_IMPORTED_MODULE_1___default.a.Fragment, null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("svg", {
    onClick: function onClick() {
      return setShowMenu(!showMenu);
    },
    className: "absolute cursor-pointer feather-icon h-8 mr-6 right-0 text-white w-8"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("use", {
    xlinkHref: "/assets/svg/feather-sprite.svg#menu"
  })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components_base__WEBPACK_IMPORTED_MODULE_8__["Modal"], {
    show: showMenu,
    onClose: function onClose() {
      return setShowMenu(!showMenu);
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "flex flex-col items-end justify-around\n                "
  }, navLinks(), !Object(_services_auth__WEBPACK_IMPORTED_MODULE_4__["isLoggedIn"])() ? /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
    onClick: function onClick() {
      return userAction.setState({
        showSignIn: true
      });
    },
    className: "cursor-pointer px-3 tracking-wide text-2xl lg:text-base text-white font-bold"
  }, "Sign in") : /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(react__WEBPACK_IMPORTED_MODULE_1___default.a.Fragment, null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("hr", {
    className: "block w-1/4 mb-6 border border-gray-300 opacity-50"
  }), (Object(_services_auth__WEBPACK_IMPORTED_MODULE_4__["userRole"])() === "admin" || Object(_services_auth__WEBPACK_IMPORTED_MODULE_4__["userRole"])() === "super_admin") && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("a", {
    href: "/admin/projects",
    className: "mb-6 cursor-pointer px-3 tracking-wide text-2xl lg:text-base text-white font-bold"
  }, "Admin"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("a", {
    href: "/profile/account-settings",
    className: "mb-6 cursor-pointer px-3 tracking-wide text-2xl lg:text-base text-white font-bold"
  }, "My Profile"), (Object(_services_auth__WEBPACK_IMPORTED_MODULE_4__["userRole"])() === "admin" || Object(_services_auth__WEBPACK_IMPORTED_MODULE_4__["userRole"])() === "super_admin" || Object(_services_auth__WEBPACK_IMPORTED_MODULE_4__["userRole"])() === "project_developer") && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("a", {
    href: "/profile/manage-projects",
    className: "mb-6 cursor-pointer px-3 tracking-wide text-2xl lg:text-base text-white font-bold"
  }, "Manage Project"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("a", {
    href: "/profile/secured-deals",
    className: "mb-6 cursor-pointer px-3 tracking-wide text-2xl lg:text-base text-white font-bold"
  }, "Secured deals"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("a", {
    href: "/profile/wish-list",
    className: "mb-6 cursor-pointer px-3 tracking-wide text-2xl lg:text-base text-white font-bold"
  }, "Wish list"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
    onClick: function onClick() {
      return handleLogout();
    },
    className: "mb-6 cursor-pointer px-3 tracking-wide text-2xl lg:text-base text-white font-bold"
  }, "Sign out")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
    className: "text-white"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components__WEBPACK_IMPORTED_MODULE_9__["SocialMedia"], {
    className: "flex flex-row mb-6 lg:justify-start justify-center mt-6",
    size: "2x"
  }))))));
};

/* harmony default export */ __webpack_exports__["default"] = (Header);

/***/ }),

/***/ "./resources/js/layouts/users/UserLayout.js":
/*!**************************************************!*\
  !*** ./resources/js/layouts/users/UserLayout.js ***!
  \**************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime/regenerator */ "./node_modules/@babel/runtime/regenerator/index.js");
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _Header__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./Header */ "./resources/js/layouts/users/Header.js");
/* harmony import */ var _Footer__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./Footer */ "./resources/js/layouts/users/Footer.js");
/* harmony import */ var _components__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../components */ "./resources/js/components/index.js");
/* harmony import */ var _components__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(_components__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var _services_auth__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../services/auth */ "./resources/js/services/auth.js");
/* harmony import */ var _helpers_bootstrap__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../helpers/bootstrap */ "./resources/js/helpers/bootstrap.js");
/* harmony import */ var _states_userGlobal__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../states/userGlobal */ "./resources/js/states/userGlobal.js");
/* harmony import */ var hookrouter__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! hookrouter */ "./node_modules/hookrouter/dist/index.js");
/* harmony import */ var hookrouter__WEBPACK_IMPORTED_MODULE_8___default = /*#__PURE__*/__webpack_require__.n(hookrouter__WEBPACK_IMPORTED_MODULE_8__);
/* harmony import */ var react_device_detect__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! react-device-detect */ "./node_modules/react-device-detect/main.js");
/* harmony import */ var react_device_detect__WEBPACK_IMPORTED_MODULE_9___default = /*#__PURE__*/__webpack_require__.n(react_device_detect__WEBPACK_IMPORTED_MODULE_9__);
/* harmony import */ var _data_dealsData_dealsData__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ../../data/dealsData/dealsData */ "./resources/js/data/dealsData/dealsData.js");


function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _unsupportedIterableToArray(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

function _iterableToArrayLimit(arr, i) { if (typeof Symbol === "undefined" || !(Symbol.iterator in Object(arr))) return; var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }












var UserLayout = function UserLayout(_ref) {
  var children = _ref.children,
      _ref$topFooter = _ref.topFooter,
      topFooter = _ref$topFooter === void 0 ? true : _ref$topFooter;

  var _UserGlobal = Object(_states_userGlobal__WEBPACK_IMPORTED_MODULE_7__["default"])(),
      _UserGlobal2 = _slicedToArray(_UserGlobal, 2),
      userState = _UserGlobal2[0],
      userAction = _UserGlobal2[1];

  var currentPath = Object(hookrouter__WEBPACK_IMPORTED_MODULE_8__["usePath"])();

  var userCount = function userCount() {
    Object(_data_dealsData_dealsData__WEBPACK_IMPORTED_MODULE_10__["getUserCount"])().then(function (res) {
      userAction.setState({
        userCount: res.data
      });
    });
  };

  var featuredDeals = function featuredDeals() {
    Object(_data_dealsData_dealsData__WEBPACK_IMPORTED_MODULE_10__["getFeaturedDeals"])().then(function (res) {
      userAction.setState({
        featuredDeals: res.data.data
      });
    });
  };

  Object(react__WEBPACK_IMPORTED_MODULE_1__["useEffect"])(function () {
    featuredDeals();
    userCount();
  }, []);
  Object(react__WEBPACK_IMPORTED_MODULE_1__["useEffect"])(function () {}, [currentPath]);
  Object(react__WEBPACK_IMPORTED_MODULE_1__["useEffect"])(function () {
    var handleResize = function handleResize() {
      userAction.setState({
        windowSize: window.screen.width
      });
    };

    window.addEventListener("resize", handleResize);
    return function () {
      return window.removeEventListener("resize", handleResize);
    };
  }, []);
  Object(react__WEBPACK_IMPORTED_MODULE_1__["useEffect"])(function () {
    var getUser = /*#__PURE__*/function () {
      var _ref2 = _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee() {
        var _yield$axios$get, data;

        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                if (Object(_services_auth__WEBPACK_IMPORTED_MODULE_5__["isLoggedIn"])()) {
                  _context.next = 4;
                  break;
                }

                userAction.setState({
                  user: null
                });
                _context.next = 9;
                break;

              case 4:
                _context.next = 6;
                return _helpers_bootstrap__WEBPACK_IMPORTED_MODULE_6__["axios"].get("/api/me", userState.user === null ? {
                  headers: {
                    Authorization: "Bearer " + Object(_services_auth__WEBPACK_IMPORTED_MODULE_5__["isLoggedIn"])()
                  }
                } : null);

              case 6:
                _yield$axios$get = _context.sent;
                data = _yield$axios$get.data;
                userAction.setState({
                  user: data.data
                });

              case 9:
              case "end":
                return _context.stop();
            }
          }
        }, _callee);
      }));

      return function getUser() {
        return _ref2.apply(this, arguments);
      };
    }();

    getUser();
  }, [userState.fetch]);

  var _handleClose = function handleClose() {
    userAction.setState({
      showSignIn: false,
      showSignUp: false,
      showSellerApply: false,
      showSecureDeal: false,
      showTermsAndConditions: false,
      showSalesAdvice: false
    });
  };

  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(react__WEBPACK_IMPORTED_MODULE_1___default.a.Fragment, null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_Header__WEBPACK_IMPORTED_MODULE_2__["default"], null), react_device_detect__WEBPACK_IMPORTED_MODULE_9__["isMobile"] && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components__WEBPACK_IMPORTED_MODULE_4__["MemberCount"], null), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("main", null, children), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_Footer__WEBPACK_IMPORTED_MODULE_3__["default"], {
    topFooter: topFooter
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components__WEBPACK_IMPORTED_MODULE_4__["ScrollArrow"], {
    className: "bg-palette-blue-light bottom-0 cursor-pointer duration-300 fixed hover:translate-y-1 ml-6 pb-2 px-2 py-1 left-0 rounded-t transform transition-all translate-y-2 z-50" // className={`bg-palette-blue-light bottom-0 cursor-pointer duration-300 fixed hover:translate-y-1 mr-6 pb-2 px-2 py-1 right-0 rounded-t transform transition-all translate-y-2 z-50`}
    ,
    top: true,
    icon: "chevron-up"
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components__WEBPACK_IMPORTED_MODULE_4__["SignIn"], {
    handleClose: function handleClose() {
      return _handleClose();
    }
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components__WEBPACK_IMPORTED_MODULE_4__["SignUp"], {
    handleClose: function handleClose() {
      return _handleClose();
    }
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components__WEBPACK_IMPORTED_MODULE_4__["SellerApply"], {
    handleClose: function handleClose() {
      return _handleClose();
    }
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components__WEBPACK_IMPORTED_MODULE_4__["TermsAndConditionsModal"], {
    handleClose: function handleClose() {
      return _handleClose();
    }
  }));
};

/* harmony default export */ __webpack_exports__["default"] = (UserLayout);

/***/ })

}]);