(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["Home"],{

/***/ "./node_modules/css-loader/index.js?!./node_modules/postcss-loader/src/index.js?!./resources/js/ui/Title/Title.module.css":
/*!********************************************************************************************************************************!*\
  !*** ./node_modules/css-loader??ref--5-1!./node_modules/postcss-loader/src??ref--5-2!./resources/js/ui/Title/Title.module.css ***!
  \********************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(/*! ../../../../node_modules/css-loader/lib/css-base.js */ "./node_modules/css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ".Container {\r\n    margin: 0 auto;\r\n    padding: 0;\r\n    text-transform: uppercase;\r\n    width: 90%;\r\n    text-align: center;\r\n    font-size: 3.5rem;\r\n  }", ""]);

// exports


/***/ }),

/***/ "./node_modules/css-loader/index.js?!./node_modules/postcss-loader/src/index.js?!./resources/js/ui/Wrapper/Wrapper.module.css":
/*!************************************************************************************************************************************!*\
  !*** ./node_modules/css-loader??ref--5-1!./node_modules/postcss-loader/src??ref--5-2!./resources/js/ui/Wrapper/Wrapper.module.css ***!
  \************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(/*! ../../../../node_modules/css-loader/lib/css-base.js */ "./node_modules/css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ".Container {\r\n    display: flex;\r\n    justify-content: center;\r\n    align-items: center;\r\n    flex-flow: column;\r\n    width: 100%;\r\n    height: 100%;\r\n    margin: 0;\r\n    padding: 0;\r\n    pointer-events: none;\r\n    background-color: rgba(0, 0, 0, 0.1)\r\n  }", ""]);

// exports


/***/ }),

/***/ "./resources/js/pages/Home.js":
/*!************************************!*\
  !*** ./resources/js/pages/Home.js ***!
  \************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _layouts_users_UserLayout__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../layouts/users/UserLayout */ "./resources/js/layouts/users/UserLayout.js");
/* harmony import */ var _states_userGlobal__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../states/userGlobal */ "./resources/js/states/userGlobal.js");
/* harmony import */ var _services_auth__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../services/auth */ "./resources/js/services/auth.js");
/* harmony import */ var _components_base__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../components/_base */ "./resources/js/components/_base/index.js");
/* harmony import */ var _components_base__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(_components_base__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var _components__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../components */ "./resources/js/components/index.js");
/* harmony import */ var _components__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(_components__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var react_device_detect__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! react-device-detect */ "./node_modules/react-device-detect/main.js");
/* harmony import */ var react_device_detect__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(react_device_detect__WEBPACK_IMPORTED_MODULE_6__);
/* harmony import */ var react_currency_format__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! react-currency-format */ "./node_modules/react-currency-format/lib/currency-format.js");
/* harmony import */ var react_currency_format__WEBPACK_IMPORTED_MODULE_7___default = /*#__PURE__*/__webpack_require__.n(react_currency_format__WEBPACK_IMPORTED_MODULE_7__);
/* harmony import */ var react_slick__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! react-slick */ "./node_modules/react-slick/lib/index.js");
/* harmony import */ var react_slick__WEBPACK_IMPORTED_MODULE_8___default = /*#__PURE__*/__webpack_require__.n(react_slick__WEBPACK_IMPORTED_MODULE_8__);
/* harmony import */ var react_player__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! react-player */ "./node_modules/react-player/lib/index.js");
/* harmony import */ var react_player__WEBPACK_IMPORTED_MODULE_9___default = /*#__PURE__*/__webpack_require__.n(react_player__WEBPACK_IMPORTED_MODULE_9__);
/* harmony import */ var _helpers_deviceSizeHelper__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ../helpers/deviceSizeHelper */ "./resources/js/helpers/deviceSizeHelper.js");
/* harmony import */ var _helpers_stateAbbreviation__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ../helpers/stateAbbreviation */ "./resources/js/helpers/stateAbbreviation.js");
/* harmony import */ var _data_dealsData_dealsData__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ../data/dealsData/dealsData */ "./resources/js/data/dealsData/dealsData.js");
/* harmony import */ var react_helmet__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! react-helmet */ "./node_modules/react-helmet/es/Helmet.js");
/* harmony import */ var _helpers_metaHelper_metaHelper__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ../helpers/metaHelper/metaHelper */ "./resources/js/helpers/metaHelper/metaHelper.js");
function _toConsumableArray(arr) { return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _unsupportedIterableToArray(arr) || _nonIterableSpread(); }

function _nonIterableSpread() { throw new TypeError("Invalid attempt to spread non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _iterableToArray(iter) { if (typeof Symbol !== "undefined" && Symbol.iterator in Object(iter)) return Array.from(iter); }

function _arrayWithoutHoles(arr) { if (Array.isArray(arr)) return _arrayLikeToArray(arr); }

function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _unsupportedIterableToArray(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

function _iterableToArrayLimit(arr, i) { if (typeof Symbol === "undefined" || !(Symbol.iterator in Object(arr))) return; var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }

















var Home = function Home() {
  var _UserGlobal = Object(_states_userGlobal__WEBPACK_IMPORTED_MODULE_2__["default"])(),
      _UserGlobal2 = _slicedToArray(_UserGlobal, 2),
      userState = _UserGlobal2[0],
      userAction = _UserGlobal2[1];

  var _useState = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])(false),
      _useState2 = _slicedToArray(_useState, 2),
      showAbout = _useState2[0],
      setShowAbout = _useState2[1];

  var _useState3 = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])(false),
      _useState4 = _slicedToArray(_useState3, 2),
      showWork = _useState4[0],
      setShowWork = _useState4[1];

  var handleClose = function handleClose() {
    setShowAbout(false);
    setShowWork(false);
  };

  Object(react__WEBPACK_IMPORTED_MODULE_0__["useEffect"])(function () {
    averageSavings();
  }, []);

  var handleScrollBottom = function handleScrollBottom() {
    window.scrollTo({
      top: document.body.scrollHeight,
      behavior: "smooth"
    });
  };

  var averageSavings = function averageSavings() {
    Object(_data_dealsData_dealsData__WEBPACK_IMPORTED_MODULE_12__["getAverageSavings"])().then(function (res) {
      userAction.setState({
        aveSavings: res.data
      });
    });
  };

  var handleSelectProject = function handleSelectProject(url) {
    // if (!isLoggedIn()) {
    //   userAction.setState({ showSignIn: true });
    //   return;
    // }
    window.location = url;
  };

  var buyerBenefits = function buyerBenefits() {
    var array = ["Biggest discounts online", "Australia's best projects", "Top-shelf developers", "Faster and easier buying process"];
    return array.map(function (item, i) {
      return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("li", {
        key: i,
        className: "flex flex-1 items-center mb-3"
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("svg", {
        className: "feather-icon h-6 w-6 mr-2",
        style: {
          color: "#ffffff"
        }
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("use", {
        xlinkHref: "/assets/svg/feather-sprite.svg#check"
      })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
        className: "flex-1 text-left"
      }, item));
    });
  };

  var listYourProject = function listYourProject() {
    var array = ["Upload properties for free", "No advertising or hidden costs", "Sell your properties around the clock", "Log in to see real-time sale activity", "Online support team"];
    return array.map(function (item, i) {
      return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("li", {
        key: i,
        className: "flex items-center"
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("svg", {
        className: "feather-icon h-6 w-6 mr-2",
        style: {
          color: "#ffffff"
        }
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("use", {
        xlinkHref: "/assets/svg/feather-sprite.svg#check"
      })), item);
    });
  };

  var renderFeatured = function renderFeatured() {
    var slides = userState.featuredDeals && userState.featuredDeals.length;

    var newFeatured = _toConsumableArray(userState.featuredDeals);

    if (!react_device_detect__WEBPACK_IMPORTED_MODULE_6__["isMobile"]) {
      if (slides > 1 && slides < 4) {
        userState.featuredDeals.map(function (featured) {
          newFeatured.push(featured);
        });
      }

      if (slides === 1) {
        userState.featuredDeals.map(function (featured) {
          newFeatured.push(featured);
          newFeatured.push(featured);
          newFeatured.push(featured);
        });
      }
    }

    return newFeatured.map(function (featured, key) {
      return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        key: key,
        className: "item px-1 lg:px-0 rounded-lg lg:rounded-none cursor-pointer",
        onClick: function onClick() {
          return handleSelectProject("/weekly-deals/".concat(featured.id, "/false"));
        } // handleSelectProject(`/weekly-deals`)

      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: "bg-cover bg-center relative text-white",
        style: {
          minHeight: 320,
          background: "linear-gradient(rgba(0, 0, 0, 0.1) 0%, rgba(0, 0, 0, 0.2) 80%, rgb(0, 0, 0) 100%), url(\"".concat(featured.featured_images[0], "\")")
        }
      }, featured.discount && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: "absolute top-0 left-0 mt-6"
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: "bg-red-700 flex items-center pr-5 pl-12"
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
        className: "font-bold text-2xl mr-3"
      }, "".concat(featured.discount, "%")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
        className: "font-bold text-base uppercase"
      }, "Discount"))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: "absolute bottom-0 font-bold leading-none left-0 mb-16 lg:mb-6 lg:ml-10 text-2xl lg:text-3xl w-full lg:w-64 px-10 lg:p-0 text-center lg:text-left"
      }, featured.name), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: "absolute bottom-0 right-0 mb-6 lg:mb-8 lg:mr-6 flex items-center justify-center text-xs font-bold w-full lg:w-auto"
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("svg", {
        className: "feather-icon text-palette-purple h-6 w-6 mr-1"
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("use", {
        xlinkHref: "/assets/svg/feather-sprite.svg#map-pin"
      })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
        className: "opacity-75"
      }, featured.address.suburb + ", " + Object(_helpers_stateAbbreviation__WEBPACK_IMPORTED_MODULE_11__["stateAbbreviation"])(featured.address.state)))));
    });
  };

  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_layouts_users_UserLayout__WEBPACK_IMPORTED_MODULE_1__["default"], null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_helmet__WEBPACK_IMPORTED_MODULE_13__["Helmet"], null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("meta", {
    name: "description",
    content: _helpers_metaHelper_metaHelper__WEBPACK_IMPORTED_MODULE_14__["metaHelper"].desc
  })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("section", {
    className: "text-white"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("section", {
    className: "text-center relative bg-cover pb-20",
    style: rowStyles.banner
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "flex flex-col items-center justify-center lg:h-full ".concat(react_device_detect__WEBPACK_IMPORTED_MODULE_6__["isMobile"] ? "lg:pt-60" : "lg:pt-0", "  pt-40 ")
  }, react_device_detect__WEBPACK_IMPORTED_MODULE_6__["isMobile"] && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components__WEBPACK_IMPORTED_MODULE_5__["MemberCount"], {
    append: "pb-3"
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "font-bold leading-tight test ".concat(react_device_detect__WEBPACK_IMPORTED_MODULE_6__["isMobile"] ? "pt-8" : "pt-0"),
    style: {
      fontSize: !react_device_detect__WEBPACK_IMPORTED_MODULE_6__["isMobile"] ? "2.5rem" : "1.75rem"
    }
  }, !react_device_detect__WEBPACK_IMPORTED_MODULE_6__["isMobile"] && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", null, "Brand new homes at ", /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("br", null), " significantly reduced prices.")), react_device_detect__WEBPACK_IMPORTED_MODULE_6__["isMobile"] && userState.windowSize <= _helpers_deviceSizeHelper__WEBPACK_IMPORTED_MODULE_10__["baseSmall"] && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", null, "Brand new homes at"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("br", null), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", null, "significantly reduced prices.")), react_device_detect__WEBPACK_IMPORTED_MODULE_6__["isMobile"] && userState.windowSize > _helpers_deviceSizeHelper__WEBPACK_IMPORTED_MODULE_10__["baseSmall"] && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", null, "Brand new homes at ", /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("br", null), " significantly reduced prices."))), !react_device_detect__WEBPACK_IMPORTED_MODULE_6__["isMobile"] && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "font-bold leading-tight lg:mt-3 lg:p-0 lg:text-2xl mt-6 px-6 text-xl"
  }, "Average saving of", " ", /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("b", {
    className: "text-palette-teal"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_currency_format__WEBPACK_IMPORTED_MODULE_7___default.a, {
    value: userState.aveSavings,
    displayType: "text",
    thousandSeparator: true,
    prefix: "$"
  })), " ", "across our listed properties."), react_device_detect__WEBPACK_IMPORTED_MODULE_6__["isMobile"] && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "font-bold leading-tight lg:mt-3 lg:p-0 lg:text-2xl mt-6 px-6 text-xl"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", null, "Average saving of", " ", /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("b", {
    className: "text-palette-teal"
  }, " ", /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_currency_format__WEBPACK_IMPORTED_MODULE_7___default.a, {
    value: userState.aveSavings,
    displayType: "text",
    thousandSeparator: true,
    prefix: "$"
  }))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("br", null), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", null, "across our listed properties.")), !Object(_services_auth__WEBPACK_IMPORTED_MODULE_3__["isLoggedIn"])() && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "mt-12 ".concat(userState.windowSize <= _helpers_deviceSizeHelper__WEBPACK_IMPORTED_MODULE_10__["small"] ? "flex flex-col" : "flex flex-row")
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_base__WEBPACK_IMPORTED_MODULE_4__["Button"], {
    onClick: function onClick() {
      return userAction.setState({
        showSignIn: true
      });
    },
    type: "secondary",
    className: "".concat(!react_device_detect__WEBPACK_IMPORTED_MODULE_6__["isMobile"] ? "mr-2" : "", " px-16 ").concat(react_device_detect__WEBPACK_IMPORTED_MODULE_6__["isMobile"] && userState.windowSize <= _helpers_deviceSizeHelper__WEBPACK_IMPORTED_MODULE_10__["small"] ? "mb-3" : "mr-2")
  }, "Log in"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_base__WEBPACK_IMPORTED_MODULE_4__["Button"], {
    onClick: function onClick() {
      return userAction.setState({
        showSignUp: true
      });
    },
    className: "".concat(!react_device_detect__WEBPACK_IMPORTED_MODULE_6__["isMobile"] ? "ml-2" : "", " px-16 ").concat(react_device_detect__WEBPACK_IMPORTED_MODULE_6__["isMobile"] && userState.windowSize <= _helpers_deviceSizeHelper__WEBPACK_IMPORTED_MODULE_10__["small"] ? "" : "ml-2")
  }, "Sign up")))), react_device_detect__WEBPACK_IMPORTED_MODULE_6__["isMobile"] && userState.featuredDeals.length >= 3 && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("section", {
    className: "text-center -mt-8",
    style: rowStyles.sliders
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("section", {
    className: "relative"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_slick__WEBPACK_IMPORTED_MODULE_8___default.a, settings, renderFeatured()))), !react_device_detect__WEBPACK_IMPORTED_MODULE_6__["isMobile"] && userState.featuredDeals.length >= 3 && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("section", {
    className: "absolute home-deals left-0 right-0 z-20 mx-auto",
    style: {
      padding: "75px 0px",
      overflow: "hidden",
      maxWidth: 1366,
      top: 530
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_slick__WEBPACK_IMPORTED_MODULE_8___default.a, settings, renderFeatured())), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("section", {
    className: "px-6 lg:p-0 relative z-10 bg-cover text-center flex flex-col items-center justify-center",
    style: rowStyles.about
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "mx-auto",
    style: {
      maxWidth: 1366
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components__WEBPACK_IMPORTED_MODULE_5__["PlayButton"], {
    className: "mb-12 lg:mt-40",
    onClick: function onClick() {
      return setShowAbout(!showAbout);
    }
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "font-bold leading-tight lg:text-5xl text-3xl"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", null, "What is GroupBuyer?")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "font-bold mt-6 lg:px-10 text-xl lg:text-2xl"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", null, "GroupBuyer is an Australian-first online platform that allows buyers to"), !react_device_detect__WEBPACK_IMPORTED_MODULE_6__["isMobile"] && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("br", null), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
    className: "text-palette-teal"
  }, " ", "purchase quality new properties at significantly reduced prices", " "), "from top-shelf developers."))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("section", {
    className: "px-6 lg:p-0 bg-cover text-center bg-blend-overlay bg-center flex flex-col items-center justify-center",
    style: rowStyles.howWorks
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "mx-auto",
    style: {
      maxWidth: 1366
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components__WEBPACK_IMPORTED_MODULE_5__["PlayButton"], {
    className: "mb-12",
    onClick: function onClick() {
      return setShowWork(!showWork);
    }
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "font-bold leading-tight lg:text-5xl text-3xl"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", null, "How does it work?")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "font-bold lg:text-center mt-6 lg:px-10 text-xl lg:text-2xl"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", null, "GroupBuyer provides the platform for buyers\xA0"), !react_device_detect__WEBPACK_IMPORTED_MODULE_6__["isMobile"] && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("br", null), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", null, "to join", /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
    className: "text-palette-teal"
  }, " 'buyer groups' "), "to gain a", /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
    className: "text-palette-teal"
  }, " bulk-buy discount "), "from a developer.")))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("section", {
    className: "bg-palette-blue-dark pb-12 pt-10 text-center"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "mx-auto",
    style: {
      maxWidth: 1366
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "font-bold leading-tight lg:text-5xl text-3xl"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", null, "Access our projects")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("p", {
    className: "lg:text-xl mb-2 py-3 text-base"
  }, "and see all the benefits with GroupBuyer."), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("a", {
    href: "/weekly-deals"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_base__WEBPACK_IMPORTED_MODULE_4__["Button"], {
    type: "secondary"
  }, "View all projects"))))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("section", {
    className: "bg-cover bg-center py-10",
    style: rowStyles.benefits
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    // className={`mx-auto h-auto`}
    className: "mx-auto h-full",
    style: {
      maxWidth: 1366
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    // className={`flex h-auto items-center justify-center lg:text-center mx-auto text-justify`}
    className: "flex h-full items-center justify-center lg:text-center mx-auto text-justify"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "text-center"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "font-bold leading-tight lg:text-5xl text-center mb-6 text-3xl"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", null, "Buyer Benefits")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("p", {
    className: "font-bold lg:px-64 lg:text-2xl text-lg mt-6 mx-6 lg:mx-0"
  }, "Why pay full price for property when you can purchase from the same developer right here at GroupBuyer?"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("ul", {
    className: "mb-3 my-3 text-lg mx-auto",
    style: {
      width: "fit-content"
    }
  }, buyerBenefits()), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "mt-8"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("a", {
    href: "/weekly-deals"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_base__WEBPACK_IMPORTED_MODULE_4__["Button"], {
    className: ""
  }, "View our projects"))))))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("section", {
    className: "bg-cover bg-center py-10",
    style: rowStyles.sell
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    // className={`mx-auto h-auto`}
    className: "mx-auto h-full",
    style: {
      maxWidth: 1366
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    // className={`flex h-auto items-center justify-center lg:text-center mx-auto text-justify`}
    className: "flex h-full items-center justify-center lg:text-center mx-auto text-justify"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "text-center"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "font-bold leading-tight lg:text-5xl text-center mb-6 text-3xl"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", null, "List your project")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("p", {
    className: "font-bold lg:px-64 lg:text-2xl text-lg mt-6 mx-6 lg:mx-0"
  }, "GroupBuyer partners with Australia's most successful and well-regarded project agents and property developers."), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("ul", {
    className: "leading-10 my-3 text-lg mx-auto",
    style: {
      width: "fit-content"
    }
  }, listYourProject()), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("p", {
    className: "text-lg mt-8"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("a", {
    href: "tel:1300031835",
    className: "text-palette-teal"
  }, "Call", " "), "or send us a", /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
    className: "text-palette-teal cursor-pointer",
    onClick: handleScrollBottom
  }, " ", "Message", " "), "to get in touch."), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "mt-8 flex justify-center"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_base__WEBPACK_IMPORTED_MODULE_4__["Button"], {
    onClick: handleScrollBottom // onClick={() =>
    //   userAction.setState({ showSellerApply: true })
    // }
    // className={`bg-palette-violet hover:bg-palette-blue-light`}

  }, "Enquire")))))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_base__WEBPACK_IMPORTED_MODULE_4__["Modal"], {
    show: showAbout,
    title: "What is GroupBuyer?",
    maxWidth: "md",
    onClose: function onClose() {
      return handleClose();
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_player__WEBPACK_IMPORTED_MODULE_9___default.a, {
    className: "mb-4",
    url: "https://www.youtube.com/watch?v=Vpxopv8gr8M",
    width: "100%",
    height: react_device_detect__WEBPACK_IMPORTED_MODULE_6__["isMobile"] ? "300px" : "520px"
  })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_base__WEBPACK_IMPORTED_MODULE_4__["Modal"], {
    show: showWork,
    title: "How does it work?",
    maxWidth: "md",
    onClose: function onClose() {
      return handleClose();
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_player__WEBPACK_IMPORTED_MODULE_9___default.a, {
    className: "mb-4" // url={`https://www.youtube.com/watch?v=m8kBcCIQvx8`}
    ,
    url: "https://www.youtube.com/watch?v=_2EC8cfrFBI",
    width: "100%",
    height: react_device_detect__WEBPACK_IMPORTED_MODULE_6__["isMobile"] ? "300px" : "520px"
  }))));
};

var settings = {
  dots: false,
  arrows: false,
  infinite: true,
  autoplay: true,
  autoplaySpeed: 5000,
  slidesToShow: 3,
  responsive: [{
    breakpoint: 1024,
    settings: {
      slidesToShow: 3,
      slidesToScroll: 3,
      infinite: true,
      dots: true
    }
  }, {
    breakpoint: 600,
    settings: {
      infinite: true,
      slidesToShow: 2,
      slidesToScroll: 2
    }
  }, {
    breakpoint: 480,
    settings: {
      infinite: true,
      centerMode: true,
      slidesToShow: 1,
      slidesToScroll: 1
    }
  }]
};
var rowStyles = {
  banner: {
    background: "linear-gradient(rgba(47, 54, 86, 0.4) 0%, rgba(47, 54, 86, 0.7) 85%, rgba(47, 54, 86, 1) 100%), url('/assets/images/landing_page_banner_1.jpg')",
    marginTop: react_device_detect__WEBPACK_IMPORTED_MODULE_6__["isMobile"] ? -162 : -130,
    height: react_device_detect__WEBPACK_IMPORTED_MODULE_6__["isMobile"] ? "auto" : 800,
    backgroundSize: "cover",
    backgroundPosition: "center",
    backgroundRepeat: "no-repeat"
  },
  sliders: {
    background: "linear-gradient(to top, rgba(255, 0, 255, 1) 0%, rgba(47, 54, 86, 1) 100%), url('/assets/images/landing_page_what_is_group_buyer.jpg')"
  },
  about: {
    background: "linear-gradient(to left, blue 0%, rgba(0, 0, 255, 0.5) 0%, rgba(255, 0, 255, 0.7) 100%), url('/assets/images/landing_page_what_is_group_buyer.jpg')",
    height: react_device_detect__WEBPACK_IMPORTED_MODULE_6__["isMobile"] ? 550 : 800,
    marginTop: "-5px"
  },
  howWorks: {
    background: "url('/assets/images/landing_page_how_it_works.jpg'), rgba(7, 14, 51, 0.8)",
    height: react_device_detect__WEBPACK_IMPORTED_MODULE_6__["isMobile"] ? 550 : 650
  },
  dealDescription: {
    background: "linear-gradient(to left, rgba(0, 159, 255, 0.9) 0%, rgba(34, 222, 222, 0.9) 100%), url('/assets/images/landing_page_deal_descriptions.jpg')",
    padding: "20px 60px 20px 30px"
  },
  benefits: {
    background: "linear-gradient(to left, rgb(30, 136, 225) 0%, rgba(30, 136, 255, 0.5) 0%), url('/assets/images/landing_page_buyer_benefits2.png')",
    // padding: "80px 0",
    height: react_device_detect__WEBPACK_IMPORTED_MODULE_6__["isMobile"] ? "auto" : 650,
    backgroundSize: "cover",
    backgroundPosition: "center",
    backgroundRepeat: "no-repeat"
  },
  sell: {
    background: "linear-gradient(to left, blue 0%, rgba(0, 0, 255, 0.5) 0%, rgba(255, 0, 255, 0.7) 100%), url('/assets/images/landing_page_sell_your_project.jpg')",
    // padding: "80px 0",
    height: react_device_detect__WEBPACK_IMPORTED_MODULE_6__["isMobile"] ? "auto" : 650
  }
};
/* harmony default export */ __webpack_exports__["default"] = (/*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.memo(Home));

/***/ }),

/***/ "./resources/js/pages/NewHome.js":
/*!***************************************!*\
  !*** ./resources/js/pages/NewHome.js ***!
  \***************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime/regenerator */ "./node_modules/@babel/runtime/regenerator/index.js");
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var hero_slider__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! hero-slider */ "./node_modules/hero-slider/dist/index.es.js");
/* harmony import */ var _fortawesome_react_fontawesome__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @fortawesome/react-fontawesome */ "./node_modules/@fortawesome/react-fontawesome/index.es.js");
/* harmony import */ var pretty_checkbox_react__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! pretty-checkbox-react */ "./node_modules/pretty-checkbox-react/dist/pretty-checkbox-react.umd.min.js");
/* harmony import */ var pretty_checkbox_react__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(pretty_checkbox_react__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var _ui_Wrapper_Wrapper__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../ui/Wrapper/Wrapper */ "./resources/js/ui/Wrapper/Wrapper.js");
/* harmony import */ var _ui_Title_Title__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../ui/Title/Title */ "./resources/js/ui/Title/Title.js");
/* harmony import */ var _material_ui_core_Button__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @material-ui/core/Button */ "./node_modules/@material-ui/core/esm/Button/index.js");
/* harmony import */ var _material_ui_core_Modal__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @material-ui/core/Modal */ "./node_modules/@material-ui/core/esm/Modal/index.js");
/* harmony import */ var _states_userGlobal__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../states/userGlobal */ "./resources/js/states/userGlobal.js");
/* harmony import */ var _helpers_bootstrap__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ../helpers/bootstrap */ "./resources/js/helpers/bootstrap.js");
/* harmony import */ var _services_auth__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ../services/auth */ "./resources/js/services/auth.js");
/* harmony import */ var _hooks_customHooks__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ../hooks/customHooks */ "./resources/js/hooks/customHooks.js");
/* harmony import */ var _components__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ../components */ "./resources/js/components/index.js");
/* harmony import */ var _components__WEBPACK_IMPORTED_MODULE_13___default = /*#__PURE__*/__webpack_require__.n(_components__WEBPACK_IMPORTED_MODULE_13__);
/* harmony import */ var _components_base_alerts_sweetAlert__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ../components/_base/alerts/sweetAlert */ "./resources/js/components/_base/alerts/sweetAlert.js");
/* harmony import */ var react_sticky__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! react-sticky */ "./node_modules/react-sticky/lib/index.js");
/* harmony import */ var react_sticky__WEBPACK_IMPORTED_MODULE_15___default = /*#__PURE__*/__webpack_require__.n(react_sticky__WEBPACK_IMPORTED_MODULE_15__);
/* harmony import */ var _layouts_users_UserLayout__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! ../layouts/users/UserLayout */ "./resources/js/layouts/users/UserLayout.js");
/* harmony import */ var react_helmet__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! react-helmet */ "./node_modules/react-helmet/es/Helmet.js");
/* harmony import */ var _helpers_metaHelper_metaHelper__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(/*! ../helpers/metaHelper/metaHelper */ "./resources/js/helpers/metaHelper/metaHelper.js");


function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _unsupportedIterableToArray(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

function _iterableToArrayLimit(arr, i) { if (typeof Symbol === "undefined" || !(Symbol.iterator in Object(arr))) return; var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }

 // JSX














 // Images

var carrousel_1 = "/assets/images/homepage_carrousel_1.jpg";
var carrousel_2 = "/assets/images/homepage_carrousel_2.jpg";
var carrousel_3 = "/assets/images/homepage_carrousel_3.jpg";
var carrousel_4 = "/assets/images/homepage_carrousel_4.jpg";
var carrousel_b_1 = "/assets/images/homepage_carrousel_b_1.jpg";
var carrousel_b_2 = "/assets/images/homepage_carrousel_b_2.jpg";
var carrousel_b_3 = "/assets/images/homepage_carrousel_b_3.jpg";
var carrousel_b_4 = "/assets/images/homepage_carrousel_b_4.jpg";
var chevron_right = "/assets/images/chevron-right.svg";
var triple_chevron_down_purple = "/assets/images/triple-chevron-down-purple.svg";
var easy_step_bg = "/assets/images/easy_steps_bg.jpg";
var commonwealth_bank = "/assets/svg/commbank.svg";




var NewHome = function NewHome() {
  var _UserGlobal = Object(_states_userGlobal__WEBPACK_IMPORTED_MODULE_9__["default"])(),
      _UserGlobal2 = _slicedToArray(_UserGlobal, 2),
      userState = _UserGlobal2[0],
      userAction = _UserGlobal2[1];

  var _useState = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(false),
      _useState2 = _slicedToArray(_useState, 2),
      modal = _useState2[0],
      setModal = _useState2[1];

  var _useState3 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(false),
      _useState4 = _slicedToArray(_useState3, 2),
      modalLogin = _useState4[0],
      setModalLogin = _useState4[1];

  var _useForm = Object(_hooks_customHooks__WEBPACK_IMPORTED_MODULE_12__["useForm"])({
    first_name: "",
    last_name: "",
    email: "",
    password: "",
    user_type: "customer"
  }),
      _useForm2 = _slicedToArray(_useForm, 2),
      state = _useForm2[0],
      formChange = _useForm2[1];

  var _useState5 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])({}),
      _useState6 = _slicedToArray(_useState5, 2),
      errors = _useState6[0],
      setErrors = _useState6[1];

  var _useState7 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(false),
      _useState8 = _slicedToArray(_useState7, 2),
      loading = _useState8[0],
      setLoading = _useState8[1];

  var form = /*#__PURE__*/Object(react__WEBPACK_IMPORTED_MODULE_1__["createRef"])();

  var handleLoginModal = function handleLoginModal() {
    setModal(false);
    setModalLogin(true);
  };

  var handleLoginModalToSignUp = function handleLoginModalToSignUp() {
    setModalLogin(false);
    setModal(true);
  };

  var handleOpenModal = function handleOpenModal() {
    setModal(true);
  };

  var handleCloseModal = function handleCloseModal() {
    setModal(false);
    setModalLogin(false);
  };

  var handleSubmit = /*#__PURE__*/function () {
    var _ref = _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee(e) {
      var errors, formData, _yield$axios$post, data, _data;

      return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee$(_context) {
        while (1) {
          switch (_context.prev = _context.next) {
            case 0:
              e.preventDefault();
              errors = {};
              formData = _objectSpread({}, state);
              console.log(formData);
              setLoading(true);
              _context.prev = 5;
              _context.next = 8;
              return _helpers_bootstrap__WEBPACK_IMPORTED_MODULE_10__["axios"].post("/api/register", formData);

            case 8:
              _yield$axios$post = _context.sent;
              data = _yield$axios$post.data;
              Object(_services_auth__WEBPACK_IMPORTED_MODULE_11__["setToken"])(data.access_token);
              setLoading(false);
              userAction.setState({
                fetch: !userState.fetch
              });
              userAction.setState({
                userCount: userState.userCount + 1
              });
              window.location.reload(); // handleClose();
              // setTimeout(() => {
              //   sweetAlert(
              //     "success",
              //     "Thank you for registering and welcome to GroupBuyer."
              //   );
              // }, 2000);

              _context.next = 22;
              break;

            case 17:
              _context.prev = 17;
              _context.t0 = _context["catch"](5);
              _data = _context.t0.response.data;
              errors = _data.errors;
              setLoading(false);

            case 22:
              setErrors(errors || {});

            case 23:
            case "end":
              return _context.stop();
          }
        }
      }, _callee, null, [[5, 17]]);
    }));

    return function handleSubmit(_x) {
      return _ref.apply(this, arguments);
    };
  }();

  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_layouts_users_UserLayout__WEBPACK_IMPORTED_MODULE_16__["default"], null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(react_helmet__WEBPACK_IMPORTED_MODULE_17__["Helmet"], null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("meta", {
    name: "description",
    content: _helpers_metaHelper_metaHelper__WEBPACK_IMPORTED_MODULE_18__["metaHelper"].desc
  })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("section", {
    id: "headPart"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(hero_slider__WEBPACK_IMPORTED_MODULE_2__["default"], {
    slidingAnimation: "left_to_right",
    orientation: "horizontal",
    initialSlide: 1,
    style: {// backgroundColor: "linear-gradient(to top, rgba(255, 0, 255, 1) 0%, rgba(47, 54, 86, 1) 100%)"
      // background: "transparent linear-gradient(180deg, #4705B4 0%, #171617 100%) 0% 0% no-repeat padding-box"
    },
    settings: {
      slidingDuration: 250,
      slidingDelay: 100,
      shouldAutoplay: false,
      shouldDisplayButtons: false,
      autoplayDuration: 5000,
      height: "120vh",
      backgroundColor: "blue"
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(hero_slider__WEBPACK_IMPORTED_MODULE_2__["OverlayContainer"], null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "absolute w-full h-full",
    style: {
      background: "linear-gradient(359deg, rgb(71 5 180) 0%, rgb(23, 22, 23) 80%, rgb(23, 22, 23) 100%) 0% 0% no-repeat padding-box padding-box transparent",
      opacity: 0.65,
      backgroundBlendMode: "multiply"
    }
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "flex flex-col relative z-10 items-center justify-center h-full"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
    className: "text-white text-center leading-none font-bold mb-8 xs:mt-32 xs:text-5xl main-banner-header",
    style: {
      fontSize: "5.5rem"
    }
  }, "Welcome to ", /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("br", null), "GroupBuyer"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
    className: "text-white text-center text-base leading-normal font-light px-8"
  }, "Together, we are stronger: Via our exceptional", " ", /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("b", {
    className: "text-palette-purple-main"
  }, "power of purchase"), ", we give access to ", /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("br", null), "exclusive opportunities to", " ", /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("b", {
    className: "text-palette-purple-main"
  }, "buy new homes"), " from trusted developers at", " ", /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("b", {
    className: "text-palette-purple-main"
  }, "unbeatable prices.")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("button", {
    className: "rounded-sm text-white border border-white text-base font-light py-2 text-center w-full max-w-xs mt-8 mb-4",
    onClick: handleOpenModal
  }, "Join us for free"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("button", {
    className: "rounded-sm text-white text-base font-bold py-2 text-center mb-6 w-full max-w-xs",
    style: {
      backgroundColor: "#E91AFC"
    }
  }, "Browse the properties"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("h2", {
    className: "mb-3 text-base font-light text-white xs:mt-16 lg:mt-8 learn-more-landing"
  }, "Learn more"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("a", {
    href: "#four-easy-step"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("img", {
    src: triple_chevron_down_purple,
    className: "w-6"
  })))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(hero_slider__WEBPACK_IMPORTED_MODULE_2__["Slide"], {
    background: {
      backgroundImage: carrousel_1,
      backgroundAttachment: "fixed",
      shouldLazyLoad: false
    }
  }))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components__WEBPACK_IMPORTED_MODULE_13__["TheMore"], {
    bgColor: "#4D0B98"
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components__WEBPACK_IMPORTED_MODULE_13__["PieceMeal"], {
    breakpoint: "md"
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("section", {
    id: "access_opportunities_section",
    className: "access_opportunities_section"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(hero_slider__WEBPACK_IMPORTED_MODULE_2__["default"], {
    slidingAnimation: "left_to_right",
    orientation: "horizontal",
    initialSlide: 1,
    style: {// backgroundColor: "linear-gradient(to top, rgba(255, 0, 255, 1) 0%, rgba(47, 54, 86, 1) 100%)"
      // background: "transparent linear-gradient(180deg, #4705B4 0%, #171617 100%) 0% 0% no-repeat padding-box"
    },
    className: "access_opportunities",
    settings: {
      slidingDuration: 250,
      slidingDelay: 100,
      shouldAutoplay: true,
      shouldDisplayButtons: true,
      autoplayDuration: 5000,
      height: "118vh",
      backgroundColor: "blue"
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(hero_slider__WEBPACK_IMPORTED_MODULE_2__["OverlayContainer"], null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "absolute w-full h-full",
    style: {
      background: "linear-gradient(359deg,#171617 0%, #171617 85%, #171617 100%) 0% 0% no-repeat padding-box padding-box black",
      opacity: 0.65
    }
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "flex flex-col relative z-10 items-center justify-center h-full"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
    className: "text-white text-center leading-none font-bold mb-8 xs:text-4xl",
    style: {
      fontSize: "6rem"
    }
  }, "Access to", " ", /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
    className: "text-palette-purple-main"
  }, "Exclusive"), " ", /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("br", null), "Opportunities"))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(hero_slider__WEBPACK_IMPORTED_MODULE_2__["Slide"], {
    background: {
      backgroundImage: carrousel_b_1,
      backgroundAttachment: "fixed",
      shouldLazyLoad: false
    } // style={{
    //   background: `linear-gradient(180deg, #4705B4 0%, #171617 100%), url('${carrousel_1}') 0% 0% no-repeat padding-box padding-box rgba(255, 255, 255, 0)`
    // }}

  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(hero_slider__WEBPACK_IMPORTED_MODULE_2__["Slide"], {
    background: {
      backgroundImage: carrousel_b_2,
      backgroundAttachment: "fixed",
      shouldLazyLoad: false
    }
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(hero_slider__WEBPACK_IMPORTED_MODULE_2__["Slide"], {
    background: {
      backgroundImage: carrousel_b_3,
      backgroundAttachment: "fixed",
      shouldLazyLoad: false
    }
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(hero_slider__WEBPACK_IMPORTED_MODULE_2__["Slide"], {
    background: {
      backgroundImage: carrousel_b_4,
      backgroundAttachment: "fixed",
      shouldLazyLoad: false
    }
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(hero_slider__WEBPACK_IMPORTED_MODULE_2__["Nav"], null))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("section", {
    id: "be-in-the-loop"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("form", {
    ref: form // className={`flex-row lg:p-6 text-white`}
    ,
    onSubmit: handleSubmit
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "flex xs:flex-col-reverse w-full"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "w-full lg:w-1/2",
    style: {
      backgroundColor: "#4D0B98"
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components__WEBPACK_IMPORTED_MODULE_13__["RegistrationForm"], {
    subHeaderColor: "text-white",
    headerColor: "text-palette-purple-main",
    handleLoginModal: handleLoginModal
  })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "w-full lg:w-1/2 flex flex-col"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "py-8 px-12 w-full"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("h3", {
    className: "mb-2 font-bold text-palette-violet-main-dark xs:mb-0"
  }, "BE IN THE KNOW"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("h2", {
    className: "text-palette-purple-main font-bold xs:text-3xl xs:mb-4 xs:leading-tight",
    style: {
      fontSize: "3rem"
    }
  }, "What is GroupBuyer?"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("p", {
    className: "font-light text-base xs:text-justify"
  }, "Exclusive properties from Australia\u2019s most trusted developers, at prices you won\u2019t find anywhere else. We believe that everyone deserves to own their own home, that's why we leverage our power of purchase to get you a good deal. Save your time and money by purchasing directly through our user friendly and stress-free digital platform.", " "), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("a", {
    className: "text-palette-purple-main text-base font-medium"
  }, "Learn More")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "py-8 pl-12 pr-24 w-full flex-1 xs:pt-10 xs:px-10 xs:pb-10",
    style: {
      backgroundColor: "#E91AFC"
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("h3", {
    className: "mb-2 font-bold text-palette-violet-main-dark"
  }, "MANAGE OPPORTUNITIES"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("h2", {
    className: "text-white font-bold xs:text-3xl",
    style: {
      fontSize: "2rem"
    }
  }, "Find a property"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "flex text-base mt-2 mb-4"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("input", {
    className: "rounded-sm placeholder-white py-2 px-4 mr-2 flex-1 xs:text-xs xs:px-3",
    placeholder: "Enter development location or name",
    style: {
      backgroundColor: "#FFFFFF5C"
    }
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("button", {
    className: "rounded-sm text-white text-base py-2 px-6",
    style: {
      backgroundColor: "#4D0B98"
    }
  }, "Search"))))))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("section", {
    id: "why-groupbuyer"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "flex flex-col pt-20 pb-32 px-32 xs:pt-10 xs:px-10 xs:pb-10 xs:bg-center",
    style: {
      background: "url('/assets/images/why_groupbuyer_1.jpg') no-repeat padding-box padding-box rgba(255, 255, 255, 0)",
      backgroundSize: "cover",
      backgroundPositionX: "right",
      backgroundPositionY: "bottom"
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
    className: "font-bold text-white text-6xl mb-12 text-center xs:text-4xl xs:leading-tight"
  }, "Why GroupBuyer?"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "md:flex lg:flex m-auto",
    style: {
      maxWidth: 1336
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "flex-1 p-6 xs:p-1"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "flex justify-between items-center text-white leading-tight font-bold text-3xl"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("p", null, "Power of ", /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("br", {
    className: "xs:hidden"
  }), "Community"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_fortawesome_react_fontawesome__WEBPACK_IMPORTED_MODULE_3__["FontAwesomeIcon"], {
    icon: ["fas", "plus"],
    size: "sm",
    className: "xs:hidden"
  })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("p", {
    className: "text-base font-light text-white xs:leading-tight"
  }, "GroupBuyer is a community of like-minded people, all sharing the passion for property ownership. We give you the opportunity to use our collective power to buy the home you deserve."), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("ol", {
    className: "why-groupbuyer-ol text-base text-white font-light xs:leading-tight"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("li", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("b", {
    className: "text-palette-purple-main font-bold text-2xl"
  }, "1."), " ", "Lorem ipsum dolor sit amet"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("li", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("b", {
    className: "text-palette-purple-main font-bold text-2xl"
  }, "2."), " ", "consectetur adipiscing elit"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("li", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("b", {
    className: "text-palette-purple-main font-bold text-2xl"
  }, "3."), " ", "ut turpis in velit ultricies"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("li", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("b", {
    className: "text-palette-purple-main font-bold text-2xl"
  }, "4."), " ", "Morbi viverra libero quis")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "text-center text-white pt-5 text-3xl"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_fortawesome_react_fontawesome__WEBPACK_IMPORTED_MODULE_3__["FontAwesomeIcon"], {
    icon: ["fas", "plus"],
    size: "sm",
    className: "lg:hidden md:hidden"
  }))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "flex-1 p-6 xs:p-1"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "flex justify-between items-center text-white leading-tight font-bold text-3xl"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("p", null, "Power of ", /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("br", {
    className: "xs:hidden"
  }), "Purchase"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_fortawesome_react_fontawesome__WEBPACK_IMPORTED_MODULE_3__["FontAwesomeIcon"], {
    icon: ["fas", "plus"],
    size: "sm",
    className: "xs:hidden"
  })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("p", {
    className: "text-base font-light text-white xs:leading-tight"
  }, "The GroupBuyer community gives us the power to negotiate unbeatable prices on a range of premium properties. The more we are, the more we negotiate, the more you save"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("ol", {
    className: "why-groupbuyer-ol text-base text-white font-light xs:leading-tight"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("li", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("b", {
    className: "text-palette-purple-main font-bold text-2xl"
  }, "1."), " ", "Lorem ipsum dolor sit amet"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("li", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("b", {
    className: "text-palette-purple-main font-bold text-2xl"
  }, "2."), " ", "consectetur adipiscing elit"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("li", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("b", {
    className: "text-palette-purple-main font-bold text-2xl"
  }, "3."), " ", "ut turpis in velit ultricies"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("li", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("b", {
    className: "text-palette-purple-main font-bold text-2xl"
  }, "4."), " ", "Morbi viverra libero quis")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "text-center text-white pt-5 text-3xl"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_fortawesome_react_fontawesome__WEBPACK_IMPORTED_MODULE_3__["FontAwesomeIcon"], {
    icon: ["fas", "plus"],
    size: "sm",
    className: "lg:hidden md:hidden"
  }))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "flex-1 p-6 xs:p-1"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "flex justify-between items-center text-white leading-tight font-bold text-3xl"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("p", null, "Power of ", /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("br", {
    className: "xs:hidden"
  }), "Knowledge"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_fortawesome_react_fontawesome__WEBPACK_IMPORTED_MODULE_3__["FontAwesomeIcon"], {
    icon: ["fas", "equals"],
    size: "sm",
    className: "xs:hidden"
  })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("p", {
    className: "text-base font-light text-white xs:leading-tight"
  }, "Years of experience, expertise and relationships with the country\u2019s most trusted developers and real estate agents, all at your disposal. Using our knowledge of the property market, we bring the best deals to you."), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("ol", {
    className: "why-groupbuyer-ol text-base text-white font-light xs:leading-tight"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("li", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("b", {
    className: "text-palette-purple-main font-bold text-2xl"
  }, "1."), " ", "Lorem ipsum dolor sit amet"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("li", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("b", {
    className: "text-palette-purple-main font-bold text-2xl"
  }, "2."), " ", "consectetur adipiscing elit"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("li", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("b", {
    className: "text-palette-purple-main font-bold text-2xl"
  }, "3."), " ", "ut turpis in velit ultricies"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("li", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("b", {
    className: "text-palette-purple-main font-bold text-2xl"
  }, "4."), " ", "Morbi viverra libero quis")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "text-center text-white pt-5 text-3xl"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_fortawesome_react_fontawesome__WEBPACK_IMPORTED_MODULE_3__["FontAwesomeIcon"], {
    icon: ["fas", "equals"],
    size: "sm",
    className: "lg:hidden md:hidden"
  }))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "flex-1 p-6 xs:p-1"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "flex justify-between items-center text-palette-purple-main leading-tight font-bold text-3xl"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("p", null, "The One Stop", /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("br", {
    className: "xs:hidden"
  }), "Shop Solution")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("p", {
    className: "text-base font-light text-white xs:leading-tight"
  }, "GroupBuyer is the destination for all things property. Want to know how the property market is changing? Are you just starting out in your property investment journey? Or simply need help with finding the best financers? GroupBuyer representatives are always available to help you."), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("ol", {
    className: "why-groupbuyer-ol text-base text-white font-light xs:leading-tight"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("li", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("b", {
    className: "text-palette-purple-main font-bold text-2xl"
  }, "1."), " ", "Lorem ipsum dolor sit amet"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("li", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("b", {
    className: "text-palette-purple-main font-bold text-2xl"
  }, "2."), " ", "consectetur adipiscing elit"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("li", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("b", {
    className: "text-palette-purple-main font-bold text-2xl"
  }, "3."), " ", "ut turpis in velit ultricies"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("li", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("b", {
    className: "text-palette-purple-main font-bold text-2xl"
  }, "4."), " ", "Morbi viverra libero quis")))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "flex justify-center mt-16  xs:hidden"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "flex flex-col items-center py-12 px-24 rounded-md xs:pt-10 xs:px-10 xs:pb-10",
    style: {
      background: "linear-gradient(45deg, rgb(255 255 255 / 30%), rgb(255 255 255 / 30%))"
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("p", {
    className: "text-4xl text-white"
  }, "Join us for free now"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("p", {
    className: "text-base text-white max-w-lg text-center font-light"
  }, "Join a community of people getting great deals on properties. The larger we are the better the deals become. Sign up to be notified of Australia\u2019s best property deals.", " "), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("img", {
    src: triple_chevron_down_purple,
    className: "w-16 my-4"
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("p", {
    className: "text-4xl text-white"
  }, "Access to deals"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("p", {
    className: "text-base text-white max-w-lg text-center font-light"
  }, "Gain access to Australia\u2019s most exclusive property deals, all from trusted developers at unbeatable prices. Explore all our available deals.", " "), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("img", {
    src: triple_chevron_down_purple,
    className: "w-16 my-4"
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("p", {
    className: "text-4xl text-white"
  }, "Secured your deals"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("p", {
    className: "text-base text-white max-w-lg text-center font-light mb-6"
  }, "Found an unbelievable deal for a property that you really like? GroupBuyer will make your life stress-free by supporting you through each step of the acquisition process. Secure the property deal with a deposit of only $1000. It is easy, completely secure and our team will be there to guide you throughout the process."), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "flex justify-center items-center mb-8"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("img", {
    src: commonwealth_bank,
    style: {
      width: "85%"
    },
    className: ""
  })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("button", {
    className: "rounded-sm text-white text-base font-bold py-2 text-center mb-6 w-full max-w-xs",
    style: {
      backgroundColor: '#E91AFC'
    }
  }, "Find your next deal"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("button", {
    onClick: handleOpenModal,
    className: "rounded-sm text-palette-purple-main border text-base font-light py-2 text-center w-full max-w-xs mb-8",
    style: {
      borderColor: '#E91AFC'
    }
  }, "Join now for free"))))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("section", {
    id: "join-us"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "flex flex-col items-center w-full xs:pt-10 xs:px-10 xs:pb-10 lg:hidden md:hidden",
    style: {
      backgroundColor: "#4D0B98"
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("p", {
    className: "text-3xl text-white"
  }, "Join us for free now"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("p", {
    className: "text-base text-white max-w-lg text-center font-light"
  }, "Join a community of people getting great deals on properties. The larger we are the better the deals become. Sign up to be notified of Australia\u2019s best property deals.", " "), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("img", {
    src: triple_chevron_down_purple,
    className: "w-16 my-4"
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("p", {
    className: "text-3xl text-white"
  }, "Access to deals"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("p", {
    className: "text-base text-white max-w-lg text-center font-light"
  }, "Gain access to Australia\u2019s most exclusive property deals, all from trusted developers at unbeatable prices. Explore all our available deals.", " "), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("img", {
    src: triple_chevron_down_purple,
    className: "w-16 my-4"
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("p", {
    className: "text-3xl text-white"
  }, "Secured your deals"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("p", {
    className: "text-base text-white max-w-lg text-center font-light mb-2"
  }, "Found an unbelievable deal for a property that you really like? GroupBuyer will make your life stress-free by supporting you through each step of the acquisition process. Secure the property deal with a deposit of only $1000. It is easy, completely secure and our team will be there to guide you throughout the process."), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "flex justify-center item-center px-8 my-8"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("img", {
    src: commonwealth_bank,
    className: "w-full"
  })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("button", {
    className: "rounded-sm text-white text-base font-bold py-2 text-center mb-6 w-full max-w-xs",
    style: {
      backgroundColor: '#E91AFC'
    }
  }, "Find your next deal"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("button", {
    className: "rounded-sm text-white border text-base font-light py-2 text-center w-full max-w-xs mb-8",
    style: {
      borderColor: 'white'
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("a", {
    href: "#be-in-the-loop"
  }, "Join now for free")))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components__WEBPACK_IMPORTED_MODULE_13__["CurrentPopularDeals"], null), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_material_ui_core_Modal__WEBPACK_IMPORTED_MODULE_8__["default"], {
    "aria-labelledby": "simple-modal-title",
    "aria-describedby": "simple-modal-description",
    open: modal // centered
    ,
    style: {
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'center'
    },
    onClose: handleCloseModal
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    style: {
      zoom: '0.85'
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components__WEBPACK_IMPORTED_MODULE_13__["RegistrationForm"], {
    containerBgColor: "#fff",
    containerRounded: true,
    displayCloseButton: true,
    rightPadded: false,
    subHeader: "JOIN US FOR FREE",
    textColorTheme: "dark",
    inputBgColor: "#dadada5c",
    headerTop: "Create",
    headerBottom: "account",
    handleCloseModal: handleCloseModal,
    subHeaderColor: "text-gray-900",
    headerColor: "text-palette-purple-main",
    opacity: "100%",
    handleLoginModal: handleLoginModal
  }))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_material_ui_core_Modal__WEBPACK_IMPORTED_MODULE_8__["default"], {
    "aria-labelledby": "simple-modal-title",
    "aria-describedby": "simple-modal-description",
    open: modalLogin,
    style: {
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'center'
    } // onClose={handleCloseModal}

  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    style: {
      zoom: '0.85'
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components__WEBPACK_IMPORTED_MODULE_13__["LoginForm"], {
    containerBgColor: "#fff",
    containerRounded: true,
    displayCloseButton: true,
    rightPadded: false,
    subHeader: "JOIN US FOR FREE",
    textColorTheme: "dark",
    inputBgColor: "#dadada5c",
    headerTop: "Login to",
    headerBottom: "account",
    handleCloseModal: handleCloseModal,
    hangleSignUpModal: handleLoginModalToSignUp
  }))));
};

/* harmony default export */ __webpack_exports__["default"] = (NewHome);

/***/ }),

/***/ "./resources/js/pages/SiteUnderConstruction.js":
/*!*****************************************************!*\
  !*** ./resources/js/pages/SiteUnderConstruction.js ***!
  \*****************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _layouts_users_UserLayout__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../layouts/users/UserLayout */ "./resources/js/layouts/users/UserLayout.js");
/* harmony import */ var _components__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../components */ "./resources/js/components/index.js");
/* harmony import */ var _components__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(_components__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _fortawesome_react_fontawesome__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @fortawesome/react-fontawesome */ "./node_modules/@fortawesome/react-fontawesome/index.es.js");
/* harmony import */ var react_device_detect__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! react-device-detect */ "./node_modules/react-device-detect/main.js");
/* harmony import */ var react_device_detect__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(react_device_detect__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var _material_ui_core_Modal__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @material-ui/core/Modal */ "./node_modules/@material-ui/core/esm/Modal/index.js");
function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _unsupportedIterableToArray(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

function _iterableToArrayLimit(arr, i) { if (typeof Symbol === "undefined" || !(Symbol.iterator in Object(arr))) return; var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }







var triple_chevron_down_purple = "/assets/images/triple-chevron-down-purple.svg";

var SiteUnderConstruction = function SiteUnderConstruction() {
  var _useState = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])(false),
      _useState2 = _slicedToArray(_useState, 2),
      modal = _useState2[0],
      setModal = _useState2[1];

  var _useState3 = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])(react_device_detect__WEBPACK_IMPORTED_MODULE_4__["isMobile"] ? true : false),
      _useState4 = _slicedToArray(_useState3, 2),
      showButton = _useState4[0],
      setShowButton = _useState4[1];

  Object(react__WEBPACK_IMPORTED_MODULE_0__["useEffect"])(function () {
    window.addEventListener('scroll', function (e) {
      var mobileJoinUsFooter = document.getElementById('footerBottom');

      if (e.target.scrollTop > e.target.scrollHeight - (e.target.clientHeight + 200)) {
        mobileJoinUsFooter.classList.remove('block');
        mobileJoinUsFooter.classList.add('hidden');
      } else {
        mobileJoinUsFooter.classList.remove('hidden');
        mobileJoinUsFooter.classList.add('block');
      }
    }, true);
  }, []);

  var handleCloseModal = function handleCloseModal() {
    setModal(false);
  };

  var handleOpenModal = function handleOpenModal() {
    console.log("clicked");
    setModal(true);
  };

  var scrollToBottom = function scrollToBottom() {
    var div = document.getElementById("be-in-the-loop");
    div.scrollIntoView({
      behavior: 'smooth'
    });
  };

  var handlePageScroll = function handlePageScroll(event) {
    console.log(event);
  };

  document.onscroll = function (event) {
    console.log(event);

    if (window.innerHeight + window.scrollY > document.body.clientHeight) {
      document.getElementById('footerBottom').style.display = 'none';
    }
  };

  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_layouts_users_UserLayout__WEBPACK_IMPORTED_MODULE_1__["default"], {
    onScroll: handlePageScroll
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("section", {
    style: {
      backgroundImage: "linear-gradient(rgba(0, 0, 0, 0.7),\n                        rgba(0, 0, 0, 0.7)), url('/assets/images/construction.png')",
      backgroundSize: "cover",
      backgroundPosition: "top",
      backgroundRepeat: "no-repeat"
    },
    className: "h-full items-end text-white flex w-full relative"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "banner-container -m-1 flex flex-col h-full items-center justify-center lg:mt-56 mb-20 mt-48 relative w-full z-10"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("h2", {
    className: "full-website-coming-soon font-extrabold font-light lg:mb-3 lg:text-lg lg:w-1/2 lg:w-full text-center text-palette-purple-main text-xs w-1/4"
  }, "FULL WEBSITE COMING SOON"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
    className: "banner-slogan font-bold leading-none lg:text-8xl mt-5 text-5xl text-center text-white"
  }, "Your success ", /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("br", null), "begins here!"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("p", {
    className: "under-construction-banner-message lg:my-0 lg:my-12 lg:px-24 lg:text-base lg:w-2/4 mb-0 my-10 px-12 text-center "
  }, "This June 30th, we are bringing to you Australia's first property e-commerce platform offering access to the most exclusive deals, all from trusted developers at unbeatable prices. Join the GroupBuyer community now and get VIP access to exciting benefits exclusive to early members."), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("button", {
    className: "font-light hidden lg:block max-w-xs mb-16 mt-4 py-2 rounded-sm text-base text-center text-white w-full",
    onClick: scrollToBottom,
    style: {
      backgroundColor: "#E91AFC"
    }
  }, "Join us for free"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("h2", {
    className: "learn-more mb-3 font-light text-white xs:mt-0 lg:mt-16 text-base md:block hidden"
  }, "Learn more"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("a", {
    href: "#how-it-works",
    className: "md:block hidden"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
    src: triple_chevron_down_purple,
    className: "w-6"
  })))), !react_device_detect__WEBPACK_IMPORTED_MODULE_4__["isMobile"] && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components__WEBPACK_IMPORTED_MODULE_2__["TheMore"], {
    bgColor: "#4D0B98",
    txtColor: "text-white"
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("section", {
    className: "h-full"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
    src: "/assets/images/test.jpg",
    className: "w-full"
  })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("section", {
    id: "how-it-works"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components__WEBPACK_IMPORTED_MODULE_2__["HowItWorksComponent"], null)), !react_device_detect__WEBPACK_IMPORTED_MODULE_4__["isMobile"] && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components__WEBPACK_IMPORTED_MODULE_2__["PieceMeal"], null), react_device_detect__WEBPACK_IMPORTED_MODULE_4__["isMobile"] && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components__WEBPACK_IMPORTED_MODULE_2__["TheMore"], {
    bgColor: "#ffffff",
    txtColor: "text-gray-900"
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("section", {
    id: "join-us-section"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "flex flex-col pt-12 pb-32 px-32 xs:pt-10 items-center xs:px-4 xs:pb-10 xs:bg-right",
    style: {
      backgroundImage: "linear-gradient(#3c0e73e6, #3c0e73e6),\n                         url(".concat(react_device_detect__WEBPACK_IMPORTED_MODULE_4__["isMobile"] ? '/assets/images/buyer_benefits.png' : '/assets/images/site_construciotn_join_us.jpg', ")"),
      backgroundSize: "cover",
      backgroundPositionX: "center",
      backgroundPositionY: "bottom"
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "md:flex lg:flex"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "flex-1 p-6 xs:p-1"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "flex flex-col items-center px-24 py-12 rounded-md xs:pb-10 xs:pt-2 xs:px-2"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("p", {
    className: "lg:text-7xl text-white text-4xl lg:text-5xl font-bold lg:text-center m-0 p-0 leading-10 whitespace-no-wrap"
  }, "Join us for free and"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("p", {
    className: "font-bold lg:text-5xl lg:text-7xl lg:text-center m-0 p-0 text-4xl text-center text-white whitespace-no-wrap"
  }, "stay in the loop"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("p", {
    className: "text-palette-purple-main xs:text-sm lg:text-lg text-center pb-8 mt-3"
  }, "A new and innovative platform to access properties from trusted developers."), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
    className: "flex font-light items-center justify-center leading-normal px-8 text-base text-center text-white"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("p", {
    className: "lg:mb-4 mb-5 text-justify"
  }, "Join a community of people getting great deals on properties. The larger we are the better the deals become. Sign up to be notified of Australia\u2019s best property deals."))))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    id: "be-in-the-loop",
    className: "md:flex lg:block hidden"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components__WEBPACK_IMPORTED_MODULE_2__["RegistrationForm"], {
    containerRounded: true,
    checkIconColor: "text-white",
    containerBgColor: "rgb(255, 255, 255, 0.2)"
  }))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    id: "footerBottom",
    className: "pt-6 pb-10 block fixed flex items-center justify-center lg:hidden w-full",
    style: {
      bottom: "0px",
      background: "linear-gradient(transparent, rgb(12 12 12 / 50%) 15%,rgb(12 12 12 / 70%) 40%, rgb(0 0 0 / 90%))"
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("button", {
    className: "font-light max-w-xs mt-4 py-2 rounded-sm text-base text-center text-white w-full",
    onClick: handleOpenModal,
    style: {
      backgroundColor: "#E91AFC"
    }
  }, "Join us for free")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_material_ui_core_Modal__WEBPACK_IMPORTED_MODULE_5__["default"], {
    "aria-labelledby": "simple-modal-title",
    "aria-describedby": "simple-modal-description",
    open: modal // centered
    ,
    style: {
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'center'
    },
    onClose: handleCloseModal
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    style: {
      zoom: '0.85'
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components__WEBPACK_IMPORTED_MODULE_2__["RegistrationForm"], {
    containerBgColor: "#fff",
    containerRounded: true,
    displayCloseButton: true,
    rightPadded: false,
    subHeader: "JOIN US FOR FREE",
    textColorTheme: "dark",
    inputBgColor: "#dadada5c",
    headerTop: "Create",
    headerBottom: "account",
    handleCloseModal: handleCloseModal,
    opacity: "100%",
    isPopup: true,
    subHeaderColor: "text-gray-900"
  })))));
};

/* harmony default export */ __webpack_exports__["default"] = (SiteUnderConstruction);

/***/ }),

/***/ "./resources/js/ui/Title/Title.js":
/*!****************************************!*\
  !*** ./resources/js/ui/Title/Title.js ***!
  \****************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js");
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(prop_types__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _Title_module_css__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./Title.module.css */ "./resources/js/ui/Title/Title.module.css");
/* harmony import */ var _Title_module_css__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(_Title_module_css__WEBPACK_IMPORTED_MODULE_2__);

 // CSS



var title = function title(props) {
  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("h1", {
    className: _Title_module_css__WEBPACK_IMPORTED_MODULE_2___default.a.Container
  }, props.children);
};

title.propTypes = {
  children: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.oneOfType([prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.string, prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.object])
};
/* harmony default export */ __webpack_exports__["default"] = (title);

/***/ }),

/***/ "./resources/js/ui/Title/Title.module.css":
/*!************************************************!*\
  !*** ./resources/js/ui/Title/Title.module.css ***!
  \************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {


var content = __webpack_require__(/*! !../../../../node_modules/css-loader??ref--5-1!../../../../node_modules/postcss-loader/src??ref--5-2!./Title.module.css */ "./node_modules/css-loader/index.js?!./node_modules/postcss-loader/src/index.js?!./resources/js/ui/Title/Title.module.css");

if(typeof content === 'string') content = [[module.i, content, '']];

var transform;
var insertInto;



var options = {"hmr":true}

options.transform = transform
options.insertInto = undefined;

var update = __webpack_require__(/*! ../../../../node_modules/style-loader/lib/addStyles.js */ "./node_modules/style-loader/lib/addStyles.js")(content, options);

if(content.locals) module.exports = content.locals;

if(false) {}

/***/ }),

/***/ "./resources/js/ui/Wrapper/Wrapper.js":
/*!********************************************!*\
  !*** ./resources/js/ui/Wrapper/Wrapper.js ***!
  \********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js");
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(prop_types__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _Wrapper_module_css__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./Wrapper.module.css */ "./resources/js/ui/Wrapper/Wrapper.module.css");
/* harmony import */ var _Wrapper_module_css__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(_Wrapper_module_css__WEBPACK_IMPORTED_MODULE_2__);

 // CSS



var wrapper = function wrapper(props) {
  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: _Wrapper_module_css__WEBPACK_IMPORTED_MODULE_2___default.a.Container
  }, props.children);
};

wrapper.propTypes = {
  children: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.oneOfType([prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.any, prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.object, prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.element])
};
/* harmony default export */ __webpack_exports__["default"] = (wrapper);

/***/ }),

/***/ "./resources/js/ui/Wrapper/Wrapper.module.css":
/*!****************************************************!*\
  !*** ./resources/js/ui/Wrapper/Wrapper.module.css ***!
  \****************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {


var content = __webpack_require__(/*! !../../../../node_modules/css-loader??ref--5-1!../../../../node_modules/postcss-loader/src??ref--5-2!./Wrapper.module.css */ "./node_modules/css-loader/index.js?!./node_modules/postcss-loader/src/index.js?!./resources/js/ui/Wrapper/Wrapper.module.css");

if(typeof content === 'string') content = [[module.i, content, '']];

var transform;
var insertInto;



var options = {"hmr":true}

options.transform = transform
options.insertInto = undefined;

var update = __webpack_require__(/*! ../../../../node_modules/style-loader/lib/addStyles.js */ "./node_modules/style-loader/lib/addStyles.js")(content, options);

if(content.locals) module.exports = content.locals;

if(false) {}

/***/ })

}]);