(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["SecuredDealsListPage"],{

/***/ "./resources/js/pages/SecuredDealsListPage.js":
/*!****************************************************!*\
  !*** ./resources/js/pages/SecuredDealsListPage.js ***!
  \****************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime/regenerator */ "./node_modules/@babel/runtime/regenerator/index.js");
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! lodash */ "./node_modules/lodash/lodash.js");
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(lodash__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _states_userGlobal__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../states/userGlobal */ "./resources/js/states/userGlobal.js");
/* harmony import */ var _layouts_users_UserLayout__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../layouts/users/UserLayout */ "./resources/js/layouts/users/UserLayout.js");
/* harmony import */ var _components_base__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../components/_base */ "./resources/js/components/_base/index.js");
/* harmony import */ var _components_base__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(_components_base__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var react_currency_format__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! react-currency-format */ "./node_modules/react-currency-format/lib/currency-format.js");
/* harmony import */ var react_currency_format__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(react_currency_format__WEBPACK_IMPORTED_MODULE_6__);
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! moment */ "./node_modules/moment/moment.js");
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_7___default = /*#__PURE__*/__webpack_require__.n(moment__WEBPACK_IMPORTED_MODULE_7__);
/* harmony import */ var react_device_detect__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! react-device-detect */ "./node_modules/react-device-detect/main.js");
/* harmony import */ var react_device_detect__WEBPACK_IMPORTED_MODULE_8___default = /*#__PURE__*/__webpack_require__.n(react_device_detect__WEBPACK_IMPORTED_MODULE_8__);
/* harmony import */ var _components__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../components */ "./resources/js/components/index.js");
/* harmony import */ var _components__WEBPACK_IMPORTED_MODULE_9___default = /*#__PURE__*/__webpack_require__.n(_components__WEBPACK_IMPORTED_MODULE_9__);
/* harmony import */ var _helpers_salesAdviceHelper_salesAdviceHelper__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ../helpers/salesAdviceHelper/salesAdviceHelper */ "./resources/js/helpers/salesAdviceHelper/salesAdviceHelper.js");
/* harmony import */ var _helpers_addressHelper__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ../helpers/addressHelper */ "./resources/js/helpers/addressHelper.js");
/* harmony import */ var react_helmet__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! react-helmet */ "./node_modules/react-helmet/es/Helmet.js");
/* harmony import */ var _helpers_metaHelper_metaHelper__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ../helpers/metaHelper/metaHelper */ "./resources/js/helpers/metaHelper/metaHelper.js");


function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _unsupportedIterableToArray(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

function _iterableToArrayLimit(arr, i) { if (typeof Symbol === "undefined" || !(Symbol.iterator in Object(arr))) return; var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }
















var SecuredDealsList = function SecuredDealsList(_ref) {
  var isAdmin = _ref.isAdmin;

  var _UserGlobal = Object(_states_userGlobal__WEBPACK_IMPORTED_MODULE_3__["default"])(),
      _UserGlobal2 = _slicedToArray(_UserGlobal, 2),
      userState = _UserGlobal2[0],
      userAction = _UserGlobal2[1];

  var _useState = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(false),
      _useState2 = _slicedToArray(_useState, 2),
      toggleFetch = _useState2[0],
      setToggleFetch = _useState2[1];

  var _useState3 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(""),
      _useState4 = _slicedToArray(_useState3, 2),
      keyword = _useState4[0],
      setKeyword = _useState4[1];

  var _useState5 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])([]),
      _useState6 = _slicedToArray(_useState5, 2),
      deals = _useState6[0],
      setDeals = _useState6[1];

  var _useState7 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])({}),
      _useState8 = _slicedToArray(_useState7, 2),
      selectedDeal = _useState8[0],
      setSelectedDeal = _useState8[1];

  var _useState9 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(true),
      _useState10 = _slicedToArray(_useState9, 2),
      loading = _useState10[0],
      setLoading = _useState10[1];

  var _useState11 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])({}),
      _useState12 = _slicedToArray(_useState11, 2),
      selectedDealProperty = _useState12[0],
      setSelectedDealProperty = _useState12[1];

  var _useState13 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])([]),
      _useState14 = _slicedToArray(_useState13, 2),
      purchasers = _useState14[0],
      setPurchasers = _useState14[1];

  var _useState15 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])({}),
      _useState16 = _slicedToArray(_useState15, 2),
      addtionalInfo = _useState16[0],
      setAdditionalInfo = _useState16[1];

  var _useState17 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])({}),
      _useState18 = _slicedToArray(_useState17, 2),
      solicitorDetails = _useState18[0],
      setSolicitorDetail = _useState18[1];

  var tableHeaders = [{
    label: "Project Name",
    width: 230
  }, {
    label: "Address",
    width: 150
  }, {
    label: "Developer Price",
    width: 150
  }, {
    label: "Discount",
    width: 60
  }, {
    label: "GroupBuyer Price",
    width: 150
  }, {
    label: "Savings",
    width: 150
  }, // {
  //   label: "Status",
  //   width: 150
  // },
  {
    label: "Properties Left",
    width: 120
  }, {
    label: "Secured Date",
    width: 150
  }, {
    label: "Proposed Settlement",
    width: 150
  }, {
    label: "Sales Advice",
    width: 30
  }, {
    label: "View Deal",
    width: 80
  }];

  var handleSalesAdviceModal = /*#__PURE__*/function () {
    var _ref2 = _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee(deal) {
      return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee$(_context) {
        while (1) {
          switch (_context.prev = _context.next) {
            case 0:
              setLoading(true);
              _context.next = 3;
              return axios.get("/api/property/".concat(deal.property_id)).then(function (res) {
                if (res.status == 200) {
                  setSelectedDealProperty(res.data);
                }
              });

            case 3:
              _context.next = 5;
              return axios.get("/api/sales-advice/".concat(deal.secured_deals_id)).then(function (res) {
                if (res.status == 200) {
                  setPurchasers(Object(_helpers_salesAdviceHelper_salesAdviceHelper__WEBPACK_IMPORTED_MODULE_10__["purchasersMapper"])(res.data.purchasers));
                  setAdditionalInfo(Object(_helpers_salesAdviceHelper_salesAdviceHelper__WEBPACK_IMPORTED_MODULE_10__["additionalInfoMapper"])(res.data.additionalDetails));
                  setSolicitorDetail(Object(_helpers_salesAdviceHelper_salesAdviceHelper__WEBPACK_IMPORTED_MODULE_10__["solicitorDetailsMapper"])(res.data.solicitorDetails));
                }
              });

            case 5:
              setSelectedDeal(deal);
              setLoading(false);
              userAction.setState({
                showSalesAdviceModal: true
              });

            case 8:
            case "end":
              return _context.stop();
          }
        }
      }, _callee);
    }));

    return function handleSalesAdviceModal(_x) {
      return _ref2.apply(this, arguments);
    };
  }();

  var _onClose = function onClose() {
    userAction.setState({
      showSalesAdviceModal: false
    });
  };

  var discountedPrice = function discountedPrice(deal) {
    if (Object(lodash__WEBPACK_IMPORTED_MODULE_2__["isEmpty"])(deal)) return;
    return deal.property_price - deal.property_price * (deal.discount / 100);
  };

  var renderSecuredDeals = function renderSecuredDeals() {
    return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(react__WEBPACK_IMPORTED_MODULE_1___default.a.Fragment, null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("tbody", null, deals && deals.map(function (deal, index) {
      var originalPrice = parseInt(deal.property_price || 0);
      var address = Object(_helpers_addressHelper__WEBPACK_IMPORTED_MODULE_11__["addressHelper"])(deal.deal_address);
      return !react_device_detect__WEBPACK_IMPORTED_MODULE_8__["isMobile"] ? /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("tr", {
        key: index,
        className: "hover:bg-gray-100 border-b text-base"
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("td", {
        className: "pl-2"
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
        className: "my-3"
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
        className: "font-bold text-palette-purple"
      }, deal.deal_name), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
        className: "opacity-50 text-sm"
      }, "Apartment " + deal.property_unit_name))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("td", {
        className: "pl-2"
      }, address), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("td", {
        className: "pl-4"
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(react_currency_format__WEBPACK_IMPORTED_MODULE_6___default.a, {
        value: originalPrice,
        displayType: "text",
        thousandSeparator: true,
        prefix: "$"
      })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("td", {
        className: "pl-4"
      }, "".concat(deal.discount ? deal.discount + "%" : "N/A")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("td", {
        className: "pl-4 font-semibold text-red-700"
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components_base__WEBPACK_IMPORTED_MODULE_5__["Tooltip"], {
        title: "Price discount: ".concat(deal.discount, "%")
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(react_currency_format__WEBPACK_IMPORTED_MODULE_6___default.a, {
        value: deal.property_price - deal.property_price * (deal.discount / 100),
        displayType: "text",
        thousandSeparator: true,
        prefix: "$"
      }))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("td", {
        className: "pl-2 text-palette-blue-light"
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(react_currency_format__WEBPACK_IMPORTED_MODULE_6___default.a, {
        value: deal.property_price * deal.discount / 100 || 0,
        displayType: "text",
        thousandSeparator: true,
        prefix: "$"
      })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("td", {
        className: "pl-12"
      }, deal.property_available), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("td", {
        className: "pl-2"
      }, moment__WEBPACK_IMPORTED_MODULE_7___default()(deal.created_at).format("DD-MM-YYYY")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("td", {
        className: "pl-8"
      }, moment__WEBPACK_IMPORTED_MODULE_7___default()(deal.proposed_settlement).format("DD-MM-YYYY")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("td", {
        className: " ".concat(userState.adminDrawerOpen ? "pl-2" : "pl-8")
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
        onClick: function onClick() {
          return handleSalesAdviceModal(deal);
        },
        className: "cursor-pointer bg-white border border-gray-400 duration-300 hover:bg-palette-purple hover:border-palette-purple hover:text-white inline-flex px-3 py-1 rounded transition-all"
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("svg", {
        className: "feather-icon h-5 w-5 opacity-50"
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("use", {
        xlinkHref: "/assets/svg/feather-sprite.svg#edit"
      })))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("td", {
        className: "lg:pl-4"
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("a", {
        href: "/weekly-deals/".concat(deal.deal_id, "/apartment/").concat(deal.property_id, "/false")
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
        className: "cursor-pointer bg-white border border-gray-400 duration-300 hover:bg-palette-purple hover:border-palette-purple hover:text-white inline-flex px-3 py-1 rounded transition-all"
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("svg", {
        className: "feather-icon h-5 w-5 opacity-50"
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("use", {
        xlinkHref: "/assets/svg/feather-sprite.svg#eye"
      })))))) : /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("tr", {
        key: index,
        className: "flex flex-col"
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("td", {
        className: "pl-2 rounded",
        style: {
          boxShadow: "0 5px 28px 4px rgba(0,0,0,0.1)"
        }
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
        className: "flex items-center justify-between"
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
        className: "flex items-center"
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("img", {
        src: "".concat(deal.project_images[0]),
        className: "rounded-full w-12 h-12 mr-3"
      }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
        className: "font-bold text-base text-palette-purple"
      }, deal.deal_name), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
        className: "opacity-50 text-sm"
      }, "Apartment " + deal.property_unit_name))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
        onClick: function onClick() {
          return handleSalesAdviceModal(deal);
        },
        className: "cursor-pointer bg-white border border-gray-400 duration-300 hover:bg-palette-purple hover:border-palette-purple hover:text-white inline-flex px-3 py-1 rounded transition-all"
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("svg", {
        className: "feather-icon h-5 w-5 opacity-50"
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("use", {
        xlinkHref: "/assets/svg/feather-sprite.svg#edit"
      }))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("a", {
        href: "/weekly-deals/".concat(deal.deal_id, "/apartment/").concat(deal.property_id, "/false")
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
        className: "cursor-pointer bg-white border border-gray-400 duration-300 hover:bg-palette-purple hover:border-palette-purple hover:text-white inline-flex px-3 py-1 rounded transition-all"
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("svg", {
        className: "feather-icon h-5 w-5 opacity-50"
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("use", {
        xlinkHref: "/assets/svg/feather-sprite.svg#eye"
      }))))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("hr", {
        className: "block my-2 border border-gray-200"
      }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", null, address), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
        className: "flex items-center justify-between mt-2"
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
        className: "uppercase font-bold"
      }, "Deal Price:", " "), discountedPrice(deal.discount_is_percentage, deal.discount, deal.property_price)), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
        className: "uppercase font-bold"
      }, "Savings: "), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(react_currency_format__WEBPACK_IMPORTED_MODULE_6___default.a, {
        value: deal.discount_is_percentage ? deal.property_price * deal.discount : deal.property_price - deal.discount || 0,
        displayType: "text",
        thousandSeparator: true,
        prefix: "$",
        className: "text-palette-blue-light"
      })))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("td", {
        className: "p-3"
      }));
    }), !react_device_detect__WEBPACK_IMPORTED_MODULE_8__["isMobile"] && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("tr", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("td", {
      colSpan: 6
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
      className: "text-right lg:mr-32 mt-3"
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
      className: "font-bold uppercase text-base mr-3 opacity-50"
    }, "Total savings:"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(react_currency_format__WEBPACK_IMPORTED_MODULE_6___default.a, {
      value: deals.reduce(function (a, b) {
        return b["property_price"] * (b["discount"] / 100) + a;
      }, 0),
      displayType: "text",
      thousandSeparator: true,
      prefix: "$",
      className: "text-lg font-bold text-palette-blue-light"
    }))))));
  };

  var renderHeaders = function renderHeaders() {
    return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("thead", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("tr", null, tableHeaders.map(function (th, index) {
      return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("th", {
        style: {
          width: th.width
        },
        className: "border-b font-bold text-palette-gray pl-2 text-left",
        key: index
      }, th.label);
    })));
  };

  var handleSearch = Object(lodash__WEBPACK_IMPORTED_MODULE_2__["debounce"])(function (value) {
    setKeyword(value);
  }, 800);

  var renderMainChildComponent = function renderMainChildComponent() {
    return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(react__WEBPACK_IMPORTED_MODULE_1___default.a.Fragment, null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("h1", {
      className: "font-bold leading-tight text-4xl pt-10 pb-8 lg:pb-16 text-center"
    }, "Secured deals"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
      className: "flex justify-center lg:justify-end mb-8"
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
      className: "w-64 relative "
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("svg", {
      className: "absolute feather-icon h-full left-0 mx-3 text-gray-500"
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("use", {
      xlinkHref: "/assets/svg/feather-sprite.svg#search"
    })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components_base__WEBPACK_IMPORTED_MODULE_5__["TextInput"], {
      className: "pl-8",
      type: "text",
      placeholder: "Search",
      border: false,
      appearance: true,
      onChange: function onChange(e) {
        return handleSearch(e.target.value);
      }
    }))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components_base__WEBPACK_IMPORTED_MODULE_5__["Table"], {
      query: "/api/secure-deal",
      toggleFetch: toggleFetch,
      keyword: keyword,
      getData: setDeals,
      content: renderSecuredDeals(),
      header: react_device_detect__WEBPACK_IMPORTED_MODULE_8__["isMobile"] ? null : renderHeaders()
    })) //   </div>
    // </section>
    ;
  };

  if (isAdmin) {
    return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
      className: "bg-white mt-10"
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
      className: "pb-16 px-6 lg:px-10 mx-auto"
    }, renderMainChildComponent(), !loading && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components__WEBPACK_IMPORTED_MODULE_9__["SalesAdviceModal"], {
      show: userState.showSalesAdviceModal,
      onClose: function onClose() {
        return _onClose();
      },
      deal: selectedDeal,
      apartment: selectedDealProperty,
      purchasersParam: purchasers
    })));
  } else {
    return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_layouts_users_UserLayout__WEBPACK_IMPORTED_MODULE_4__["default"], null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(react_helmet__WEBPACK_IMPORTED_MODULE_12__["Helmet"], null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("meta", {
      name: "description",
      content: _helpers_metaHelper_metaHelper__WEBPACK_IMPORTED_MODULE_13__["metaHelper"].desc
    })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("section", {
      className: "bg-palette-blue-dark",
      style: {
        marginTop: -130,
        paddingTop: 160
      }
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("section", {
      className: "relative bg-white"
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
      className: "pb-16 px-6 lg:px-20"
    }, renderMainChildComponent(), !loading && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components__WEBPACK_IMPORTED_MODULE_9__["SalesAdviceModal"], {
      show: userState.showSalesAdviceModal,
      onClose: function onClose() {
        return _onClose();
      },
      deal: selectedDeal,
      apartment: selectedDealProperty,
      purchasersParam: purchasers
    })))));
  }
};

/* harmony default export */ __webpack_exports__["default"] = (SecuredDealsList);

/***/ })

}]);