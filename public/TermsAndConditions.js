(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["TermsAndConditions"],{

/***/ "./resources/js/pages/TermsAndConditions.js":
/*!**************************************************!*\
  !*** ./resources/js/pages/TermsAndConditions.js ***!
  \**************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _layouts_users_UserLayout__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../layouts/users/UserLayout */ "./resources/js/layouts/users/UserLayout.js");
/* harmony import */ var react_helmet__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! react-helmet */ "./node_modules/react-helmet/es/Helmet.js");
/* harmony import */ var _helpers_metaHelper_metaHelper__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../helpers/metaHelper/metaHelper */ "./resources/js/helpers/metaHelper/metaHelper.js");
 // import { Modal } from "~/components/_base"
// import UserGlobal from "~/states/UserGlobal"



 // const TermsAndConditions = ({ handleClose }) => {

var TermsAndConditions = function TermsAndConditions() {
  // const [userState] = UserGlobal()
  return (
    /*#__PURE__*/
    // <Modal
    //   show={userState.showTermsAndConditions}
    //   title={`Terms and Conditions`}
    //   onClose={() => handleClose()}
    //   transition={`grow`}
    //   maxWidth={`md`}
    // >
    react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_layouts_users_UserLayout__WEBPACK_IMPORTED_MODULE_1__["default"], null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_helmet__WEBPACK_IMPORTED_MODULE_2__["Helmet"], null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("meta", {
      name: "description",
      content: _helpers_metaHelper_metaHelper__WEBPACK_IMPORTED_MODULE_3__["metaHelper"].desc
    })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("section", {
      className: "text-white bg-palette-blue-dark",
      style: {
        marginTop: -130,
        paddingTop: 160
      }
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: "mx-auto",
      style: {
        maxWidth: 1366
      }
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: "bg-palette-blue-dark lg:px-16 mx-auto pb-10 lg:pt-16 px-4 text-white"
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("h1", {
      className: "font-extrabold lg:text-5xl mb-5 text-3xl text-center"
    }, "Terms and Conditions"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("h2", {
      className: "font-extrabold lg:text-4xl mb-4 text-2xl text-center"
    }, "USER"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("p", {
      className: "text-base"
    }, "GroupBuyer Pty Ltd ACN 641 556 470 (GroupBuyer) has the right to operate the GroupBuyer website (Website). These Terms and Conditions apply to your use of the Website. Your use of the Website including accessing or browsing the Website or uploading any material to the Website is deemed to be your acceptance of these Terms and Conditions. Please read these carefully and if you do not accept the Terms and Conditions set out below please exit the Website and refrain from using the Website."), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("p", {
      className: "text-base"
    }, "If you breach any provision of these Terms and Conditions your right to access the Website will be immediately revoked. By registering as a GroupBuyer account member and using the Website you acknowledge that you are at least eighteen (18) years of age and you agree to be bound by these Terms and Conditions."), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("h4", {
      className: "text-2xl mb-3 mt-8 font-bold"
    }, "1. Intellectual Property"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("p", {
      className: "text-base"
    }, "GroupBuyer uses the word \u2018GroupBuyer\u2019 as a trade mark in Australia (Trade Mark). You are not permitted to use the Trade Mark or any other trademarks which belong or are licensed to GroupBuyer or any of GroupBuyer\u2019 Intellectual Property which includes but is not limited to copyright material, confidential information, designs and other content located on the Website without GroupBuyer\u2019 prior written authorisation."), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("p", {
      className: "text-base"
    }, "The information, images and text on the Website may be subject to copyright and are the property of or otherwise property that is licensed to GroupBuyer. Trade marks used on the Website to describe other companies and their products or services are trade marks which belong to or are licensed to those companies (Other Trade Marks). The Other Trade Marks are displayed on the Website to provide information to you about other products or services via the Website. You agree not to download, copy, reproduce or in any way use the Other Trade Marks without authorisation from the owner/s of the Other Trade Marks."), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("p", {
      className: "text-base"
    }, "You acknowledge that you are expressly prohibited from using the phrase \u2018GroupBuyer\u2019 or any part of the website domain name without the written consent of GroupBuyer. You agree that you are prohibited from reverse engineering the Website or otherwise attempting to copy the Website functionality or obtain from the Website information or data including but not limited to user\u2019s personal information."), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("h4", {
      className: "text-2xl mb-3 mt-8 font-bold"
    }, "2. Information and Liability"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("p", {
      className: "text-base"
    }, "You acknowledge that GroupBuyer is not liable or responsible for any errors or omissions in the content of the Website. The information contained on the Website is provided for general information purposes only. GroupBuyer makes no warranties as to the accuracy or completeness of the recommendations, information or material contained on the Website."), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("p", {
      className: "text-base"
    }, "You acknowledge that there are risks associated with using the internet and World Wide Web and to the fullest extent permitted by law, GroupBuyer or its affiliates or subsidiaries are not liable for any direct or indirect damages or losses arising by way of your use or access to the Website."), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("p", {
      className: "text-base"
    }, "You acknowledge that GroupBuyer makes no warranty that the Website will meet your user requirements or that the information contained on the Website or your use of the Website will be uninterrupted, timely, secure, error free, accurate, virus-free or compliant with legislation or regulations."), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("p", {
      className: "text-base"
    }, "You acknowledge and agree that GroupBuyer have no liability of any kind either express or implied with respect to any claims arising in any way from your use of the Website, by virtue of the information provided or contained on the Website. The Website contains general statements regarding goods and/or services offered by other businesses however it is your responsibility to ensure the goods and/or services are the product you wish to purchase and must review the relevant product or service documents supplied by the relevant businesses."), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("p", {
      className: "text-base"
    }, "You acknowledge that GroupBuyer may from time to time improve or make changes to the Website to upgrade security on the Website, update information on the Website or for any other reason in GroupBuyer\u2019s sole discretion."), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("p", {
      className: "text-base"
    }, "You acknowledge that you are responsible for ensuring the protection of your electronic equipment when accessing or browsing the Website and unreservedly hold GroupBuyer harmless in respect to damage sustained to your electronic equipment or losses incurred by you as a result of the Website."), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("h4", {
      className: "text-2xl mb-3 mt-8 font-bold"
    }, "3. Privacy"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("p", {
      className: "text-base"
    }, "You acknowledge that during your use of the Website you may provide or upload certain personal information, photos, images or written material to the Website. GroupBuyer is committed to preserving the privacy of its users and will ensure that all personal information collected will be kept secure and in accordance with any applicable privacy laws. By using the Website, you agree to provide certain personal information necessary to utilise the Website to its full capability."), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("p", {
      className: "text-base"
    }, "Whilst GroupBuyer agrees to ensure that all personal information is kept secure, you agree that GroupBuyer may provide your personal information to other companies for the purpose of dealing with your property request. You acknowledge that GroupBuyer will receive remuneration for providing your personal information to relevant third-party businesses."), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("p", {
      className: "text-base"
    }, "GroupBuyer reserves the right to cooperate fully with any law enforcement authorities relating to any lawful request for the disclosure of personal information of its users. You acknowledge that you have read and agree to the terms of GroupBuyer\u2019s", " ", /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("a", {
      href: "#"
    }, ".Privacy Policy")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("h4", {
      className: "text-2xl mb-3 mt-8 font-bold"
    }, "4. Use of Website"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("p", {
      className: "text-base"
    }, "You acknowledge that your use of the Website is undertaken at your own risk and you unreservedly indemnify GroupBuyer from any responsibility in respect of information or content listed on the Website or any linked Websites not controlled by GroupBuyer."), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("p", {
      className: "text-base"
    }, "You agree that you will not engage in any activity that is disruptive or is detrimental to the operation or function of the Website. You are prohibited from displaying or transmitting unlawful, threatening, defamatory, obscene, or profane material on the Website. GroupBuyer may prevent you from accessing the Website if in GroupBuyer\u2019 sole opinion you have violated or acted inconsistently with these Terms and Conditions."), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("h4", {
      className: "text-2xl mb-3 mt-8 font-bold"
    }, "5. Links and Advertisements"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("p", {
      className: "text-base"
    }, "You acknowledge that any links or advertisements containing links to external websites that are provided or located on the Website are for convenience only. GroupBuyer does not endorse or make any warranty with respect to any external websites or to the accuracy or reliability of the links or advertisements contained on the Website."), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("p", {
      className: "text-base"
    }, "You acknowledge that items, resources or learning material which are advertised or displayed on the Website may not be available to purchase or download at the time you access the Website."), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("h4", {
      className: "text-2xl mb-3 mt-8 font-bold"
    }, "6. Security"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("p", {
      className: "text-base"
    }, "GroupBuyer, from time to time, may use technology to improve security on the Website. You agree to cooperate with GroupBuyer and you are responsible to check and read the amended Terms and Conditions."), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("h4", {
      className: "text-2xl mb-3 mt-8 font-bold"
    }, "7. Cookies and Viruses"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("p", {
      className: "text-base"
    }, "GroupBuyer uses its best endeavours to ensure the use of the best technology to protect its users, but accepts no responsibility in relation to any damage to any electronic equipment used by you, including mobile devices, as a result of your use of the Website. You acknowledge that you are responsible for ensuring the protection of your electronic equipment and unreservedly indemnify GroupBuyer from any responsibility in respect of damage to any electronic equipment as a result of your use of the Website."), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("h4", {
      className: "text-2xl mb-3 mt-8 font-bold"
    }, "8. Payment"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("p", {
      className: "text-base"
    }, "You acknowledge that you may be charged for the services you receive from using the Website. You agree to provide GroupBuyer with at least one payment method supported by GroupBuyer upon request from GroupBuyer. You acknowledge that GroupBuyer may vary the amount charged for a service at any time and at the sole discretion of GroupBuyer."), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("h4", {
      className: "text-2xl mb-3 mt-8 font-bold"
    }, "9. Refund Policy"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("p", {
      className: "text-base"
    }, "Any User of the Website who successfully purchases a property deal via the Website is aware they are refunded in full at the time of unconditional exchange of contracts for that same payment amount. Should a User withdraw from unconditional exchange of contracts the User will forfeit this payment to GroupBuyer."), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("h4", {
      className: "text-2xl mb-3 mt-8 font-bold"
    }, "10. General"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("p", {
      className: "text-base"
    }, "If any part of these Terms and Conditions are held to be illegal or unenforceable, you authorise GroupBuyer to amend or sever that part of the Terms and Conditions to make that part legal and enforceable or to ensure that the remaining parts of the Terms and Conditions remain in full force and effect."), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("p", {
      className: "text-base"
    }, "You acknowledge that GroupBuyer may vary these Terms and Conditions and you agree to be bound by that variation from that time when that variation is advised. You understand that it is your responsibility to regularly check these Terms and Conditions so that you are aware of any variations which apply. Your continued use of the Website constitutes your agreement to these Terms and Conditions and any variations to these Terms and Conditions."), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("h4", {
      className: "text-2xl mb-3 mt-8 font-bold"
    }, "11. Applicable Law"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("p", {
      className: "text-base"
    }, "You agree that the content of the Website is intended for Australian residents only and may not be available in your Country. The laws of the State of New South Wales will apply with respect to these Terms and Conditions and your use of the Website. You submit to the exclusive jurisdiction of the applicable Courts located in the State of New South Wales, Australia with respect to the Website and these Terms and Conditions.")))))
  );
};

/* harmony default export */ __webpack_exports__["default"] = (TermsAndConditions);

/***/ })

}]);