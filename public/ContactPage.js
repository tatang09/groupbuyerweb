(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["ContactPage"],{

/***/ "./resources/js/pages/ContactPage.js":
/*!*******************************************!*\
  !*** ./resources/js/pages/ContactPage.js ***!
  \*******************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var hookrouter__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! hookrouter */ "./node_modules/hookrouter/dist/index.js");
/* harmony import */ var hookrouter__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(hookrouter__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _layouts_users_UserLayout__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../layouts/users/UserLayout */ "./resources/js/layouts/users/UserLayout.js");
/* harmony import */ var _components__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../components */ "./resources/js/components/index.js");
/* harmony import */ var _components__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(_components__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var react_helmet__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! react-helmet */ "./node_modules/react-helmet/es/Helmet.js");
/* harmony import */ var _helpers_metaHelper_metaHelper__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../helpers/metaHelper/metaHelper */ "./resources/js/helpers/metaHelper/metaHelper.js");







var ContactPage = function ContactPage() {
  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_layouts_users_UserLayout__WEBPACK_IMPORTED_MODULE_2__["default"], {
    topFooter: false
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_helmet__WEBPACK_IMPORTED_MODULE_4__["Helmet"], null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("meta", {
    name: "description",
    content: _helpers_metaHelper_metaHelper__WEBPACK_IMPORTED_MODULE_5__["metaHelper"].desc
  })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("section", {
    className: "text-white bg-palette-blue-dark",
    style: {
      marginTop: -130,
      paddingTop: 160
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("section", {
    className: "relative text-center bg-center bg-cover",
    style: rowStyles.contact
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "mx-auto",
    style: {
      maxWidth: 1366
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("h1", {
    className: "font-bold leading-10 pb-20 pt-24 text-4xl"
  }, "Get in touch."), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "lg:mx-auto mx-4 relative"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components__WEBPACK_IMPORTED_MODULE_3__["ContactForm"], {
    backgroundColor: "rgba(0, 25, 84, 0.90)"
  })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "flex flex-col items-center justify-center py-12"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components__WEBPACK_IMPORTED_MODULE_3__["SocialMedia"], {
    className: "mb-4",
    size: "2x"
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "font-semibold lg:font-bold text-2xl"
  }, "Phone", /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("a", {
    href: "tel:1300031835",
    className: "hover:text-palette-teal text-palette-blue-dark"
  }, "\xA01300 031 835")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "font-semibold lg:font-bold text-2xl"
  }, "Email", /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("a", {
    href: "mailto:info@groupbuyer.com.au",
    className: "hover:text-palette-teal text-palette-blue-dark"
  }, "\xA0info@groupbuyer.com.au")))))));
};

var rowStyles = {
  contact: {
    background: "linear-gradient(to left,rgb(30, 136, 225) 0%, rgb(30, 136, 225, 0.5) 0%), url('/assets/images/contact_page.jpg')"
  }
};
/* harmony default export */ __webpack_exports__["default"] = (ContactPage);

/***/ })

}]);