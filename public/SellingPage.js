(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["SellingPage"],{

/***/ "./resources/js/pages/SellingPage.js":
/*!*******************************************!*\
  !*** ./resources/js/pages/SellingPage.js ***!
  \*******************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _layouts_users_UserLayout__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../layouts/users/UserLayout */ "./resources/js/layouts/users/UserLayout.js");
/* harmony import */ var react_helmet__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! react-helmet */ "./node_modules/react-helmet/es/Helmet.js");
/* harmony import */ var _helpers_metaHelper_metaHelper__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../helpers/metaHelper/metaHelper */ "./resources/js/helpers/metaHelper/metaHelper.js");





var SellingPage = function SellingPage() {
  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_layouts_users_UserLayout__WEBPACK_IMPORTED_MODULE_1__["default"], null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_helmet__WEBPACK_IMPORTED_MODULE_2__["Helmet"], null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("meta", {
    name: "description",
    content: _helpers_metaHelper_metaHelper__WEBPACK_IMPORTED_MODULE_3__["metaHelper"].desc
  })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("section", {
    className: "text-white bg-palette-blue-dark pb-20",
    style: {
      marginTop: -125,
      paddingTop: 160
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "bg-cover bg-center relative",
    style: rowStyles.selling
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "mx-auto pb-16",
    style: {
      maxWidth: 1280
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "md:flex md:w-full pt-10 md:p-10 lg:pt-10"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "flex-1 m-2 px-4 py-2 text-gray-700"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("h3", {
    className: "font-bold leading-10 text-4xl text-gray-100 text-left"
  }, "With an over-supply of property on the market in Australia and so many competing developers Group Buyer is your solution to making bulk sales faster than ever before."), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("ul", {
    className: "font-hairline mt-10 pl-8 text-base text-white"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("li", {
    className: "list-disc p-2"
  }, "Provides a platform for bulk-sales"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("li", {
    className: "list-disc p-2"
  }, "Access to a larger audience"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("li", {
    className: "list-disc p-2"
  }, "Faster sales made 24/7"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("li", {
    className: "list-disc p-2"
  }, "No marketing costs"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("li", {
    className: "list-disc p-2"
  }, "Sell through projects faster")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("p", {
    className: "mt-6 font-hairline text-base text-white"
  }, "Apply to become a partner. We'll get back in touch with you to discuss your project requirements.")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "flex-1 text-gray-700 text-center"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
    src: "/assets/images/landing_page_sell_image.jpg"
  })))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "flex flex-col pl-16"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("p", {
    className: "font-hairline text-base text-white"
  }, "You can also", " ", /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("a", {
    href: "tel:1300"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
    className: "hover:text-gray-600 text-sub-500"
  }, "Call")), " ", "or Send us a", " ", /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("a", {
    href: ""
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
    className: "hover:text-gray-600 text-sub-500"
  }, "Message")), " ", "to get in touch."), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("button", {
    className: "border border-gray-100 rounded-full text-white w-1/6 h-12 hover:bg-yellow-200 hover:text-primary-900"
  }, "View Projects"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("h3", {
    className: "font-bold text-4xl text-white pt-6"
  }, "Our Partners"))))));
};

var rowStyles = {
  selling: {
    background: "linear-gradient(to left, blue 0%, rgba(0, 0, 255, 0.8) 0%, rgba(255, 0, 255, 0.8) 100%), url('/assets/images/landing_page_sell_projects.jpg')"
  }
};
/* harmony default export */ __webpack_exports__["default"] = (SellingPage);

/***/ })

}]);