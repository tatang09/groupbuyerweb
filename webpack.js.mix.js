const mix = require("laravel-mix");
require('laravel-mix-merge-manifest');
const tailwindcss = require("tailwindcss");

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.browserSync({
  watch: true,
  files: [
    "public/js/**/*",
    "public/css/**/*",
    "public/**/*.+(html|php)",
    "resources/views/**/*.php",
  ],
  open: false,
  browser: ["google chrome", "firefox", "safari"],
  reloadDelay: 1000,
  proxy: {
    target: "http://127.0.0.1:8080",
    ws: true,
  },
});

mix
  .react("resources/js/admin/app.js", "public/js/admin")
  .react("resources/js/app.js", "public/js")
  .extract()
  .copyDirectory("resources/assets", "public/assets")
  .babelConfig({
    plugins: ['@babel/plugin-syntax-dynamic-import'],
  })
  .mergeManifest()
  .webpackConfig({
    output: {
        chunkFilename: '[name].js?id=[chunkhash]',
    }
  });


  if (mix.inProduction()) {
    mix.version();
}