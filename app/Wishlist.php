<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Wishlist extends Model
{
    protected $guarded = [];

    protected $casts = [
        'property_address' => 'array',
        'property_images' => 'array',
        'deal_address' => 'array',
        'price' => 'float'
    ];
}
