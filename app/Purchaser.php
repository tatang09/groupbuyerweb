<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Purchaser extends Model
{
    protected $fillable = [
        'first_name',
        'last_name',
        'email',
        'phone',
        'title',
        'alternate_phone',
        'address',
        'secured_deal_id',
        'property_id'
    ];

}
