<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Incentive extends Model
{
    protected $guarded = [];

    protected $casts = [
        'discount_is_percentage' => 'boolean'
    ];
}
