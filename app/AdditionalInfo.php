<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AdditionalInfo extends Model
{
    protected $fillable = [
        'heard_about_us',
        'contact',
        'purpose',
        'isFirstHomeBuyer',
        'isPreviousBoughtPlan',
        'isFIRB_required',
        'property_id',
        'secured_deal_id',
        'comment',
    ];

    public $table = "additional_info";
}
