<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Developer;

class Bank extends Model
{
    protected $guarded = [];

    protected $fillable = [
        'name',
    ];

    public function developer()
    {
        return $this->belongsTo(Developer::class);
    }
}
