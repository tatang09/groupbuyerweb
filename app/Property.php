<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class Property extends Model
{
    protected $guarded = [];

    protected $casts = [
        'address' => 'array',
        'is_secured' => 'boolean',
        'featured_images' => 'array',
        'price' => 'float',
        'is_property_approved' =>'boolean',
        'property_type_id' => 'integer',
        'water_cost' => 'float',
        'council_cost' => 'float',
        'strata_cost' => 'float',
        'unit_no' => 'integer'
    ];

    public function deal()
    {
        return $this->belongsTo('App\Deal');
    }

    public function securedDeals()
    {
        return $this->hasOne('App\SecuredDeal');
    }

    public function type()
    {
        return $this->belongsTo('App\PropertyType', 'property_type_id', 'id');
    }

    public function getFeaturedImagesAttribute($paths)
    {
        $links = [];
        $new = explode(',', str_replace(['"', '[', ']', ' '], [''. '', ''], $paths));
        foreach($new as $key=>$path) {
            $link = Storage::disk(config('filesystems.cloud'))->temporaryUrl($new[(int)$key], now()->addDays(1));
             array_push($links, $link);
        }

        return $links;
    }

    public function getRichPreviewImageAttribute($path)
    {
        
        return isset($path) ? Storage::disk('s3_cloud_image_preview')->url($path) : null;
    }

    public function getFloorPlanAttribute($path)
    {
         $resourceFile = [];
         if(isset($path)){
            $link = Storage::disk(config('filesystems.cloud'))->temporaryUrl($path, now()->addDays(1));
            $resourceFile = [
                'file' => $link,
                'name' => 'floor_plan'
            ];
        }

        return  $resourceFile;
    }

    public function setDiscountedPriceAttribute($discount)
    {
        if($discount){
            $discountedPrice = $this->price - ($this->price * ($discount / 100));
            $this->attributes['discounted_price'] = $discountedPrice;
        }
    }
}
