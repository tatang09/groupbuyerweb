<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SecuredDeal extends Model
{
    protected $guarded = [];

    protected $casts = [
        'property_address' => 'array',
        'property_images' => 'array',
        'deal_address' => 'array',
        'price' => 'float'
    ];

    public function property()
    {
        return $this->belongsTo('App\Property');
    }
}
