<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Http\Repositories\SalesAdviceRepositoryHelper\SalesAdviceAdditionalDetails;

class AdditionalDetailsServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('additionaldetails', function () {
            return new SalesAdviceAdditionalDetails();
        });
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
