<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('App\Http\Interfaces\DealRepositoryInterface', 'App\Http\Repositories\DealRepository');
        $this->app->bind('App\Http\Interfaces\SecuredDealsRepositoryInterface', 'App\Http\Repositories\SecuredDealsRepository');
        $this->app->bind('App\Http\Interfaces\DeveloperRepositoryInterface', 'App\Http\Repositories\DeveloperRepository');
        $this->app->bind('App\Http\Interfaces\SalesAdviceRepositoryInterface', 'App\Http\Repositories\SalesAdviceRepository');
        $this->app->bind('App\Http\Interfaces\BankRepositoryInterface', 'App\Http\Repositories\BankRepository');
        $this->app->bind('App\Http\Interfaces\UserRepositoryInterface', 'App\Http\Repositories\UserRepository');
        $this->app->bind('App\Http\Interfaces\SalesAdviceRepositoryInterface', 'App\Http\Repositories\SalesAdviceRepository');
        $this->app->bind('App\Http\Interfaces\PropertyRepositoryInterface', 'App\Http\Repositories\PropertyRepository');
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
