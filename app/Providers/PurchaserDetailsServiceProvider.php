<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Http\Repositories\SalesAdviceRepositoryHelper\SalesAdvicePurchaserDetails;

class PurchaserDetailsServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('purchaserdetails', function () {
            return new SalesAdvicePurchaserDetails();
        });
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
