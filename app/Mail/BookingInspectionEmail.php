<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class BookingInspectionEmail extends Mailable
{
    use Queueable, SerializesModels;
    public $request;
    public $lotNumber;
    public $dealName;
    public $lotName;
    public $imageUrl;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($request,$property, $deal)
    {
        $this->request = $request;
        $this->lotName = $property ? "- Apartment " . $property->unit_name : "";
        $this->lotNumber = $property ? "Lot " . $property->unit_no : "";
        $this->dealName = $deal->name;
        $this->imageUrl = $property ? $property->featured_images[0] : $deal->featured_images[0];
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject('Booking Inspection')->view('emails.bookinginspection');
    }
}
