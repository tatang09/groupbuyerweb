<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use App\Traits\Pricing;

class UserSecureDealSuccess extends Mailable
{
    use Queueable, SerializesModels, Pricing;

    public $property;
    public $savings;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($property)
    {
        $this->property = $property;
        $this->savings = '$'.number_format($this->computeSavings($property->discount, $property->property_price), 2, ".", ",");
    }

     

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $subject = 'You have just secured Apartment ' . $this->property->property_unit_name . " Lot " . $this->property->property_lot_number . ' at ' . $this->property->deal_name . '!';

        return $this->subject($subject)
            ->view('emails.secured_deal');
    }
}
