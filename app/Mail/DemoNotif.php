<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class DemoNotif extends Mailable
{
    use Queueable, SerializesModels;
    public $name;
    public $title;
    public $email;
    public $company_name;
    public $phone;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($name, $email, $company_name, $phone, $title)
    {
        $this->name = $name;
        $this->phone = $phone;
        $this->email = $email;
        $this->company_name = $company_name;
        $this->title = $title;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject('Demo Notification')->view('emails.demonotif');
    }
}
