<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PaymentDetail extends Model
{
    protected $guarded = [];

    protected $fillable = [
         'secured_deal_id',
         'auth_code',
         'payment_id',
         'card_data',
         'transaction_data',
         'payment_status',
         'decline_reason',
         'name_on_card'
    ];

    public function secured_deal()
    {
        return $this->belongsTo('App\SecuredDeal');
    }
}
