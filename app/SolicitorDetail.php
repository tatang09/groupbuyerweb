<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SolicitorDetail extends Model
{
    protected $fillable = [
        'company',
        'contact',
        'email',
        'poBox',
        'address',
        'phone',
        'secured_deal_id',
        'property_id'
    ];
}
