<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class Developer extends Model
{
    protected $fillable = [
        'name',
        'email',
        'state',
        'phone',
        'country',
        'suburb',
        'address',
        'address_line_1',
        'user_id',
        'postcode',
        'bank_id',
        'bank_bsb',
        'bank_account_number',
        'bank_account_name',
        'company_logo_path',
        'company_url'
    ];

    public function getCompanyLogoPathAttribute($path)
    {
        return !empty($path) ?
            Storage::disk(config('filesystems.cloud'))->temporaryUrl($path, now()->addDays(1)) :
            "";
    }

    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
