<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Storage;
use Laravel\Passport\HasApiTokens;
use Laravel\Cashier\Billable;
use Spatie\Permission\Traits\HasRoles;
use App\Notifications\PasswordResetNotification;
use Illuminate\Support\Str;

class User extends Authenticatable
{
    use Notifiable;
    use HasRoles;
    use HasApiTokens;
    use Billable;

    protected $guard_name = 'api';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'first_name',
        'last_name',
        'email',
        'phone',
        'password',
        'avatar_path',
        'google_id',
        'facebook_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function developer()
    {
        return $this->hasOne('App\Developer');
    }

    public function getAvatarPathAttribute($path)
    {
      
        if(Str::contains($path, 'https'))
        {
            return $path;
        }     

        return !empty($path) ?
            Storage::disk(config('filesystems.cloud'))->temporaryUrl($path, now()->addDays(1)) :
            "";
    }

    public function sendPasswordResetNotification($token){
        $this->notify(new PasswordResetNotification($token));
    }
}
