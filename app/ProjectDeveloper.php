<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Deal;

class ProjectDeveloper extends User
{
    protected $table = "users";

    public function newQuery($excludeDeleted = true)
    {
        return parent::newQuery($excludeDeleted)->join('model_has_roles', function($join) {
            $join->on('model_has_roles.model_id', '=', 'users.id');
        })->where('model_has_roles.role_id', '=', '3');
    }

    public function developer()
    {
        return $this->hasOne('App\Developer', 'user_id', 'id');
    }

    public function deals()
    {
        return $this->hasMany('App\Deal','user_id', 'id');
    }  
}
