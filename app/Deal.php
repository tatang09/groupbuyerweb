<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class Deal extends Model
{
    //
    protected $guarded = [];

    protected $casts = [
        'address' => 'array',
        'featured_images' => 'array',
        'is_featured' => 'boolean',
        'is_weekly' => 'boolean',
        'user_id' => 'integer',
        'display_suite_address' => 'array',
        'resources' => 'array',
        'is_deal_approved' => 'boolean',
        'is_completed' => 'boolean',
        
    ];

    protected $fillable = [
        'user_id',
    ];

    public function properties()
    {
        return $this->hasMany('App\Property');
    }

    public function getFeaturedImagesAttribute($paths)
    {
        $links = [];
        $new = explode(',', str_replace(['"', '[', ']', ' '], [''. '', ''], $paths));
        foreach($new as $key=>$path) {
            $link = Storage::disk(config('filesystems.cloud'))->temporaryUrl($new[(int)$key], now()->addDays(1));
            array_push($links, $link);
        }

        return $links;
    }

    public function getRichPreviewImageAttribute($path)
    {
        return isset($path) ? Storage::disk('s3_cloud_image_preview')->url($path) : null;
    }

    public function getResourcesAttribute($paths)
    {
        $links = [];
        if(strlen($paths) > 2) {
            $new = explode(',', str_replace(['"', '[', ']', ' ', '\\'], [''. '', '', ''], $paths));
            // dd($new);
            foreach($new as $key=>$path) {
                $resourceKey = explode('/', $path)[3];
                $link = Storage::disk(config('filesystems.cloud'))->temporaryUrl($new[(int)$key], now()->addDays(1));
                $resourceFile = [
                    'file' => $link,
                    'name' => $resourceKey
                ];

                array_push($links, $resourceFile);
            }
        }

        return $links;
    }
}
