<?php

namespace App\Http\Traits;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Arr;
use Illuminate\Support\Str;
use Illuminate\Support\Collection;
use App\Deal;
use App\Property;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;

trait ActiveCampaign
{

    protected $campaignURL;
    protected $campaignKey;

    public function __construct(){
        $this->campaignURL = config('services.active_campaign.url');
        $this->campaignKey = config('services.active_campaign.key');
    }

    public function triggerFeaturedDealsEmail($request)
    {
        $baseParam = [
            'email' =>  request('email'),
            'first_name' =>  request('first_name'),
            'last_name' =>  request('last_name'),
            'p[2]' => 2,
            'status[2]' => 1
        ];

        // $featuredDeals = $this->fetchFeaturedDeals('');

        // $savingsQuery = $featuredDeals && $featuredDeals->discount ? $featuredDeals->discount / 100 : '- 0';

        // $properties = $featuredDeals ? $this->fetchPropertySavings($featuredDeals->id,  $savingsQuery) : null;

        // $paramArray= array_merge($baseParam, $this->featuredDealsCustomFields($featuredDeals, $properties, $request));

        $this->subscribeToContact($baseParam);

        // $this->subscribeToContact($paramArray);
    }

    public function triggerHowitWorksEmail($request, $key)
    {
        $baseParam = [
            'email' =>  request('email'),
            'first_name' =>  request('first_name'),
            'last_name' =>  request('last_name'),
            'p[2]' => 2,
            'status[2]' => 1
        ];

        // $featuredDeals = $this->fetchFeaturedDeals($key);

        // $paramArray = array_merge($baseParam, $this->howItWorksDealsCustomFields($featuredDeals));

        // return $this->subscribeToContact($paramArray);

        return $this->subscribeToContact($baseParam);

    }

    private function subscribeToContact($paramArray){

        return Http::asForm()->post($this->campaignURL . '/admin/api.php?api_action=contact_add&api_key='. $this->campaignKey . '&api_output=json', $paramArray);

    }

    private function subscribeToContactNew($request){

        $params = [
            'email' =>  request('email'),
            'first_name' =>  request('first_name'),
            'last_name' =>  request('last_name'),
            'p[2]' => 2,
            'status[2]' => 1
        ];

        return Http::asForm()->post($this->campaignURL . '/admin/api.php?api_action=contact_add&api_key='. $this->campaignKey . '&api_output=json', $params);

    }

    private function fetchFeaturedDeals($key)
    {
        $deals = Deal::where('is_featured', 1)
        ->select(
            'deals.id',
            'deals.name',
            'deals.address',
            'deals.featured_images',
            'deals.description',
            'deals.discount'
        );

        switch ($key) {
            case 'how-it-works':
                $deals = $deals->inRandomOrder()->take(3)->get();
                break;
            default:
                $deals = $deals->inRandomOrder()->first();
                break;
        }
       return $deals;
    }

    private function featuredDealsCustomFields($featuredDeals, $properties, $request)
    {
        return [
            'field[%FEATURED_ID%, 0]' => $featuredDeals ? $featuredDeals->id : null,
            'field[%USER_TYPE%, 0]' => $featuredDeals ? $featuredDeals->id : null,
            'field[%FEATURED_NAME%, 0]' => $featuredDeals ? $featuredDeals->name . " - " . $featuredDeals->address['city'] : null,
            'field[%FEATURED_DISCOUNT%, 0]' => $featuredDeals ? $featuredDeals->discount . "%" : null,
            'field[%FEATURED_SAVING%, 0]' => $featuredDeals && $properties ? '$' . number_format($properties->savings, 0, '.', ',') : null,
            'field[%FEATURED_DESCRIPTION%, 0]' => $featuredDeals ? strip_tags($featuredDeals->description) : null,
            'field[%FEATURED_AVAILABLE%, 0]' => $featuredDeals && $properties ? $properties->property_count . ' apartments in ' . $featuredDeals->address['city'] . '\'s ' . $featuredDeals->address['suburbs'] : null,
            'field[%FEATURED_IMAGE_1%, 0]' => $featuredDeals ?  $featuredDeals->featured_images[0] : null,
            'field[%FEATURED_IMAGE_2%, 0]' => $featuredDeals ?  $featuredDeals->featured_images[1] : null,
            'field[%FEATURED_IMAGE_3%, 0]' => $featuredDeals ?  $featuredDeals->featured_images[2] : null,
            'field[%CONTACT_TYPE%, 0]' => $request->user_type === 'project_developer' ? "Seller" : null,
            'field[%FEATURED_PRICE%, 0]' => $featuredDeals ?  $properties->price : "$0.00"
        ];
    }

    private function mapProperties($deal){

        $savingsQuery = $deal && $deal->discount ? $deal->discount / 100 : '- 0';

        return $deal ? $this->fetchPropertySavings($deal->id,  $savingsQuery) : null;
    }

    private function fetchPropertySavings($id, $savingsQuery)
    {
        return Property::where('deal_id', $id)
        ->select(DB::raw('price, no_of_bedrooms, count(*) as property_count'), DB::raw('MAX(price *' . $savingsQuery . ') as savings'))
        ->first();
    }

    private function howItWorksDealsCustomFields($featuredDeals)
    {
        $fieldsArray = [];
        foreach($featuredDeals as $key=>$deal)
        {
            $price =  $this->mapProperties($deal)->price - ($this->mapProperties($deal)->price * ($deal->discount / 100));

            if($key == 0){
                    $fieldsArray  = [
                        'field[%FEATURED_ID%, 0]' => $deal ? $deal->id : null,
                        'field[%USER_TYPE%, 0]' => $deal ? $deal->id : null,
                        'field[%FEATURED_NAME%, 0]' => $deal ? $deal->name . " - " . $deal->address['city'] : null,
                        'field[%FEATURED_DISCOUNT%, 0]' => $deal ? $deal->discount . "%" : null,
                        'field[%FEATURED_SAVING%, 0]' => $deal && $this->mapProperties($deal) ? '$' . number_format($this->mapProperties($deal)->savings, 0, '.', ',') : null,
                        'field[%FEATURED_DESCRIPTION%, 0]' => $deal ? strip_tags($deal->description) : null,
                        'field[%FEATURED_AVAILABLE%, 0]' => $deal && $this->mapProperties($deal) ? $this->mapProperties($deal)->property_count . ' apartments in ' . $deal->address['city'] . '\'s ' . $deal->address['suburbs'] : null,
                        'field[%FEATURED_IMAGE_1%, 0]' => $deal ?  $deal->featured_images[0] : null,
                        'field[%FEATURED_IMAGE_2%, 0]' => $deal ?  $deal->featured_images[1] : null,
                        'field[%FEATURED_IMAGE_3%, 0]' => $deal ?  $deal->featured_images[2] : null,
                        'field[%FEATURED_PRICE%, 0]' => $deal && $this->mapProperties($deal)  ? '$'. number_format($price, 0, '.', ',') : "$0.00",
                        'field[%FEATURED_BEDROOM_COUNT%, 0]' => $deal && $this->mapProperties($deal) ? $this->mapProperties($deal)->no_of_bedrooms : "0"
                    ];
            }else if($key == 1){
                    $arrayIndex1 = [
                        'field[%FEATURED_ID_1%, 0]' => $deal ? $deal->id : null,
                        'field[%USER_TYPE%, 0]' => $deal ? $deal->id : null,
                        'field[%FEATURED_NAME_1%, 0]' => $deal ? $deal->name . " - " . $deal->address['city'] : null,
                        'field[%FEATURED_DISCOUNT_1%, 0]' => $deal ? $deal->discount . "%" : null,
                        'field[%FEATURED_SAVING_1%, 0]' => $deal && $this->mapProperties($deal) ? '$' . number_format($this->mapProperties($deal)->savings, 0, '.', ',') : null,
                        'field[%FEATURED_DESCRIPTION_1%, 0]' => $deal ? strip_tags($deal->description) : null,
                        'field[%FEATURED_AVAILABLE_1%, 0]' => $deal && $this->mapProperties($deal) ? $this->mapProperties($deal)->property_count . ' apartments in ' . $deal->address['city'] . '\'s ' . $deal->address['suburbs'] : null,
                        'field[%FEATURED_IMAGE_21%, 0]' => $deal ?  $deal->featured_images[0] : null,
                        'field[%FEATURED_IMAGE_22%, 0]' => $deal ?  $deal->featured_images[1] : null,
                        'field[%FEATURED_IMAGE_23%, 0]' => $deal ?  $deal->featured_images[2] : null,
                        'field[%FEATURED_PRICE_1%, 0]' => $deal && $this->mapProperties($deal)  ?  '$'. number_format($price, 0, '.', ',') : "$0.00",
                        'field[%FEATURED_BEDROOM_COUNT_1%, 0]' => $deal && $this->mapProperties($deal) ?  $this->mapProperties($deal)->no_of_bedrooms : 0
                    ];
                    $fieldsArray = array_merge($fieldsArray, $arrayIndex1);
            }else if($key == 2){
                $arrayIndex2 = [
                        'field[%FEATURED_ID_2%, 0]' => $deal ? $deal->id : null,
                        'field[%USER_TYPE%, 0]' => $deal ? $deal->id : null,
                        'field[%FEATURED_NAME_2%, 0]' => $deal ? $deal->name . " - " . $deal->address['city'] : null,
                        'field[%FEATURED_DISCOUNT_2%, 0]' => $deal ? $deal->discount . "%" : null,
                        'field[%FEATURED_SAVING_2%, 0]' => $deal && $this->mapProperties($deal) ? '$' . number_format($this->mapProperties($deal)->savings, 0, '.', ',') : null,
                        'field[%FEATURED_DESCRIPTION_2%, 0]' => $deal ? strip_tags($deal->description) : null,
                        'field[%FEATURED_AVAILABLE_2%, 0]' => $deal && $this->mapProperties($deal) ? $this->mapProperties($deal)->property_count . ' apartments in ' . $deal->address['city'] . '\'s ' . $deal->address['suburbs'] : null,
                        'field[%FEATURED_IMAGE_31%, 0]' => $deal ?  $deal->featured_images[0] : null,
                        'field[%FEATURED_IMAGE_32%, 0]' => $deal ?  $deal->featured_images[1] : null,
                        'field[%FEATURED_IMAGE_33%, 0]' => $deal ?  $deal->featured_images[2] : null,
                        'field[%FEATURED_PRICE_2%, 0]' => $deal && $this->mapProperties($deal)  ? '$'. number_format($price, 0, '.', ',') : "$0.00",
                        'field[%FEATURED_BEDROOM_COUNT_2%, 0]' => $deal && $this->mapProperties($deal) ?  $this->mapProperties($deal)->no_of_bedrooms : 0
                    ];
                    $fieldsArray = array_merge($fieldsArray, $arrayIndex2);
            }
        }
        return  $fieldsArray;
    }
}
