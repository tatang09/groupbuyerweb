<?php

namespace App\Http\Traits;
use App\Http\Repositories\DealRepository;
use App\Deal;
use App\Property;
use Illuminate\Support\Facades\DB;
trait OpenGraph{

    public function getDealById($id)
    {
        return Deal::where('id', $id)->first();
    }

    public function getPropertyById($id)
    {

        return Property::select(
            'properties.id',
            'deals.name as dealName',
            'properties.unit_name as propName',
            'properties.featured_images',
            'properties.description',
            'properties.unit_no',
            'properties.rich_preview_image',
            'property_types.name as propertyTypeName'
            )
            ->join('deals','properties.deal_id','=','deals.id')
            ->join('property_types','properties.property_type_id','=','property_types.id')
            ->where('properties.id', $id)->first();

    }

    public function processSiteContent($id, $type)
    {
        if($type === 'project')
        {
            $resource = $this->getDealById($id);
        }
        else
        {
            $resource = $this->getPropertyById($id);
        }

        $description= "";

        $description = strip_tags($resource->description);
        $description = str_replace("&nbsp;", " ", $description);
        $title = $type === 'project' ? "GroupBuyer - " . $resource->name :  $resource->dealName . " - " . $resource->propertyTypeName . " " . $resource->propName;

        return ["name" => "GroupBuyer",
        "title" => $title,
        "description" => $description,
        "image" => $resource->rich_preview_image ? $resource->rich_preview_image : $resource->featured_images[0],
        "url" => url()->current(),
        ];
    }
}

