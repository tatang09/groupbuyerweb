<?php

namespace App\Http\Traits\EmailCampaigns;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Arr;
use Illuminate\Support\Str;
use Illuminate\Support\Collection;
use App\Deal;
use App\Property;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use App\User;
use App\SecuredDeal;
use App\Http\Repositories\PropertyRepository;
use Illuminate\Support\Facades\Mail;
use App\Mail\UserSecureDealSuccess;

trait SecuredDealEmail {

    protected $campaignURL;
    protected $campaignKey;
    protected $user;
    protected $propertyRepository;

    public function triggerSecuredDealSuccessEmail($request)
    {
       
        $user = auth()->user();
        $property = $this->getPropertyDetails($request);
 
        Mail::to($user->email)
        // ->cc(config('services.booking_inspection.default_cc'))
        ->send(new UserSecureDealSuccess($property));
        return ["message" => "Deal secured successfully."];

        //Active campaign
        // $campaignURL = config('services.active_campaign.url');
        // $campaignKey = config('services.active_campaign.key');
        // $user = auth()->user();

        // $property = $this->getPropertyDetails($request);

        // $savings = $property->property_price - ($property->property_price * ($property->discount / 100));
        // $savings = '$'.number_format($savings, 2, ".", ",");

        // $params = [
        //     'id' => $user->ac_contact_id, // This is the contact id, we should store then when registering the user. Should be replaced by something like $user->ac_contact_id
        //     'email' => $user->email, //Can be any email and will update the existing email in AC if a different email is passed (in case the user changed their email in the site)
        //     'field[%SECURED_DEAL_SAVINGS%, 0]' => $savings, //This field has been automatically added to the contact fields, just replace this with the actual discount amount
        //     'field[%SECURED_DEAL_PROJECT%, 0]' => $property->deal_name, //Same here, automatically added to contact fields just replace with the actual project name
        //     'field[%SECURED_DEAL_PROPERTY%, 0]' => 'Property ' . $property->property_unit_name //Same here, automatically added to contact fields just replace with the actual property name
        // ];

        // $contactUpdateResponse = Http::asForm()->post('https://groupbuyer.api-us1.com/admin/api.php?api_action=contact_edit&api_key='. $campaignKey .'&api_output=json&overwrite=0', $params);

        // $sendSecuredDealCampaignResponse = Http::asForm()->get('https://groupbuyer.api-us1.com/admin/api.php?api_action=campaign_send&api_key='. $campaignKey .'&api_output=json&email='.$user->email.'&campaignid=37&messageid=null&action=send');
    }

    private function getPropertyDetails($request)
    {
        return Property::where('properties.id', '=' , $request->property_id)
        ->join('deals','properties.deal_id','=','deals.id')
        ->select(
            'deals.name as deal_name',
            'deals.discount',
            'properties.unit_name as property_unit_name',
            'properties.unit_no as property_lot_number',
            'properties.price as property_price',
            'properties.address as property_address'
        )->first(); ;
    }
}
