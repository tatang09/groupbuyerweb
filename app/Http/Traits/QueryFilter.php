<?php

namespace App\Http\Traits;

use Auth;

trait QueryFilter
{
     public function queryFilterByUserType($attribute, $model)
     {
        $user = Auth::user();
        $role = $user->getRoleNames()[0];

        $idList = collect();

        if ($role == "admin" || $role == "super_admin") {
            $idList = $model::whereNotNull('user_id')
                ->groupBy($attribute)
                ->pluck($attribute);
        } else {
            $idList = $model::where('user_id', '=', auth()->user()->id)
                ->groupBy($attribute)
                ->pluck($attribute);
        }

        return $idList;
     }
}