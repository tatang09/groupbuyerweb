<?php

namespace App\Http\Traits;

trait Pricing
{
    public function computeSavings($discount, $originalPrice)
    {
       return $originalPrice * ($discount / 100);
    }

    public function computeDiscountedPrice($discount, $originalPrice)
    {
       return $originalPrice - ($originalPrice * ($discount / 100));
    }
}