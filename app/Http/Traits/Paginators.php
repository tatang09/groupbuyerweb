<?php

namespace App\Http\Traits;

use App\Wishlist;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Arr;
use Illuminate\Support\Str;
use Illuminate\Support\Collection;
use Auth;
 

trait Paginators
{
    private $sortBy = ['created_at' => 'desc'];
    private $dev = "project_developer";


    public function paginate($builder, $attributes = [])
    {
             
        $limit = request('limit', 10);
        $offset = request('offset', 0);
        $sort = request('sort', null);
        $order = request('order', null);
        $keyword = request('keyword', '');
        $currentPage = ($offset / $limit) + 1;
        $filters = json_decode(request('filters', '[]'));

        Paginator::currentPageResolver(function () use ($currentPage) {
            return $currentPage;
        });

        $builder->when($filters, function ($query) use ($filters) {
            foreach (Arr::wrap($filters) as $key => $filter) {
                $query->when($filter->key, function ($query) use ($filter) {
                    if (Str::contains($filter->key, ':')) {
                        [$relation, $field] = explode(':', $filter->key);

                        return $query->whereHas($relation, function ($query) use ($field, $filter) {
                            return $this->filterCondition($query, $filter, $field);
                        });
                    }
                    return $this->filterCondition($query, $filter);
                });
            }
        });

        // $builder->when($keyword && $attributes, function ($query) use ($attributes, $keyword) {
        //     foreach (Arr::wrap($attributes) as $key => $attribute) {
        //         $query->when($attribute, function ($query) use ($attribute, $keyword, $key) {
        //             $condition = $key ? 'orWhere' : 'where';
        //             if (!Str::contains($attribute, 'address')) {
        //                 if($attribute == "properties.unit_name" || $attribute == "properties.unit_no"){                            
        //                     return $query->{$condition}($attribute, '=', $keyword);
        //                 }                       
        //                 return $query->{$condition}($attribute, 'LIKE', "%{$keyword}%");
        //             } else {
        //                 $query = $query->{$condition}(function ($query) use ($attribute, $keyword, $key) {                           
        //                     return $query->whereRaw('LOWER(' . $attribute . ') like ?', '%' . strtolower($keyword) . '%');
        //                 });
        //                 return $query;
        //             }
        //         });
        //     }
        // });      
 
        $builder->when(!$sort, function ($query) {
            foreach (Arr::wrap($this->sortBy) as $sort => $order) {
                return $query->orderByRaw($sort . " IS NULL, " . $sort . " " . $order);
            }
        });

        if (request('columns')) {
            return $builder;
        }

        return $builder->when($sort, function ($query) use ($sort, $order) {
            return $query->orderByRaw($sort . " IS NULL, " . $sort . " " . $order);
        })->paginate($limit);
    }

    private function filterCondition($query, $filter, $key = null)
    {
        if ($filter->type === 'date') {
            $date = date($filter->startDate);
            // $to = date("{$filter->endDate} 23:59:59");
            $key = $key ?? $filter->key;
            return $query->whereDate(DB::raw("CAST({$key} AS DATETIME)"), $date);
        }

        switch ($filter->filter) {
            case 'contains':
                return $query->where(DB::raw($key ?? $filter->key), 'LIKE', "%{$filter->filterValue}%");
            case 'does not contains':
                return $query->where(DB::raw($key ?? $filter->key), 'NOT LIKE', "%{$filter->filterValue}%");
            case 'is not empty':
                return $query->whereNotNull(DB::raw($key ?? $filter->key));
            case 'is empty':
                return $query->whereNull(DB::raw($key ?? $filter->key));
            case 'is equal to':
                return $query->where(DB::raw($key ?? $filter->key), '=', "{$filter->filterValue}");
            case 'is between':
                return $query->whereBetween(DB::raw($key ?? $filter->key), [$filter->filterValue, $filter->filterValue2]);
            case 'is greater than':
                return $query->where(DB::raw($key ?? $filter->key), '>', "{$filter->filterValue}");
            case 'is less than':
                return $query->where(DB::raw($key ?? $filter->key), '<', "{$filter->filterValue}");
            case 'is':
                return $query->where(DB::raw($key ?? $filter->key), '=', "{$filter->filterValue}");
            case "isn't":
                return $query->where(DB::raw($key ?? $filter->key), '<>', "{$filter->filterValue}");
        }
    }
}
