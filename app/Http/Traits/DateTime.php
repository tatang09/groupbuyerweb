<?php

namespace App\Http\Traits;
use Carbon\Carbon;

trait DateTime{

    public function calculateDateSum($request, $created)
    {
 
        $currentDate = Carbon::now();

        if((int)$request->time_limit["optionVal"] == 0 || (int)$request->time_limit["option"] == 0)
        {
            return  $currentDate;
        }
      
        if($created)
        {
            $createdAt = Carbon::parse($created);
            $currentDate = new Carbon($createdAt);
        }

       switch ($request->time_limit["option"]) {
            case  "1":
                $currentDate->addHours($request->time_limit["optionVal"]);
                break;
            case  "2":
                $currentDate->addDays($request->time_limit["optionVal"]);
               break;
            case  "3":
                $currentDate->addWeeks($request->time_limit["optionVal"]);
               break;
            case  "4":
                $currentDate->addMonths($request->time_limit["optionVal"]);
               break;   
            case  "5":
                $currentDate->addYears($request->time_limit["optionVal"]);
            break;  
            default:
                // $currentDate->addMonths($request->time_limit["option"]);
               break;
       }
       
       return  $currentDate;
    }
}
   
 