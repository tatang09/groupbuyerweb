<?php

namespace App\Http\Controllers;

use App\User;
use App\Http\Traits\UploadTrait;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Collection;
use GuzzleHttp\Client;
use App\ProjectDeveloper;
use App\Http\Helpers\Facades\S3FileUpload;
use App\Http\Interfaces\UserRepositoryInterface;
use App\Http\Requests\UserRequest;

class UserController extends Controller
{
    use UploadTrait;
  
    private $userRepositoryInterface;
    
    public function __construct(UserRepositoryInterface $userRepositoryInterface)
    {
        $this->userRepositoryInterface = $userRepositoryInterface;   
    }

    public function index()
    {
        $project_developers = ProjectDeveloper::all();

        return response()->json($project_developers);
    }

    public function store(UserRequest $request)
    {
       $user = $this->userRepositoryInterface->create($request);   

       return response()->json($user, 200);
    }
 
    public function show(User $user)
    {
        return response()->json($user);
    }

    public function update(UserRequest $request, User $user)
    {
        $user = $this->userRepositoryInterface->update($request);

        return response()->json($user, 200); 
    }

    public function userCount(User $user)
    {
        // return $this->userRepositoryInterface->userCount($user);
    }
}
