<?php

namespace App\Http\Controllers;

use App\Http\Traits\Paginators;
use App\Http\Resources\PaginationCollection;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Helpers\Facades\S3FileUpload;
use App\Property;
use App\Deal;
use Auth;
use Image;
use App\Http\Interfaces\PropertyRepositoryInterface;
use App\Http\Requests\BookingInspectionRequest;

class PropertyController extends Controller
{
    use Paginators;

    private $propertyRepositoryInterface;
    
    public function __construct(PropertyRepositoryInterface $propertyRepositoryInterface)
    {
        $this->propertyRepositoryInterface = $propertyRepositoryInterface;

        $this->middleware('auth:api', ['only' => ['store', 'update']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
         
           $retval = $this->propertyRepositoryInterface->fetchProperties();

           $data = $this->paginate($retval["properties"], $retval["columnsSearch"]);

           return new  PaginationCollection($data);
 
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user = Auth::user();
        $role =$user->getRoleNames()[0];

        $rules = [
            'featuredImages' => 'required|array|min:3|max:6',
            'price' => 'required',
            'number_of_bedrooms' => 'required_if:propertyType,1|required_if:propertyType,2|required_if:propertyType,3|required_if:propertyType,5',
            'number_of_bathrooms' => 'required_if:propertyType,1|required_if:propertyType,2|required_if:propertyType,3|required_if:propertyType,5',
            'number_of_garages' => 'required_if:propertyType,1|required_if:propertyType,2|required_if:propertyType,3|required_if:propertyType,5',
            'size' => 'required_if:propertyType,4|numeric',
            'frontage' => 'required_if:propertyType,4|numeric',
            'width' => 'required_if:propertyType,4|numeric',
            'depth' => 'required_if:propertyType,4|numeric',
            'strata' => 'required_with:strata_cost',
            'water' => 'required_with:water_cost',
            'council' => 'required_with:council_cost',
            'floor_area' => 'filled',
            'unit_no' => 'required_if:propertyType,1|required_if:propertyType,2|required_if:propertyType,3|required_if:propertyType,5',
            'unit_name' => 'required',
            'description' => 'required',
            'propertyType' => 'required',
            'subPropertyType' => 'filled',
            'ownershipType' => 'filled',
            'line_1' => 'filled',
            'suburb' => 'filled',
            'city' => 'filled',
            'state' => 'filled',
            'project' => 'filled'
        ];

        $validationMessages = [
            'required' => '*Required',
            'required_without' => '*Required',
            'required_if' => '*Required',
        ];

        $this->validate($request, $rules, $validationMessages);

        $property = new Property;

        $deal = Deal::find($request->projectId);

        if($role == "admin") {
            if($request->project) {
                $property->deal_id = $request->project;
            } else {
                $property->deal_id = $request->projectId;
            }
        }
         else {
            $property->deal_id = $request->projectId;
        }

        $property->unit_name = $request->unit_name;
        $property->description = $request->descriptionHTML;
        $property->property_type_id = $request->propertyType;
        $property->sub_property_id = 2;
        $property->ownership_type_id = 1;

        $property->address = json_encode($deal->address);

        $property->price = $this->amountHelper($request->price);
        $property->setAttribute('discounted_price', $deal->discount);
        $property->no_of_bedrooms = $request->number_of_bedrooms;
        $property->no_of_bathrooms = $request->number_of_bathrooms;
        $property->no_of_garages = $request->number_of_garages;
        $property->internal_size = $request->internal_size;
        $property->unit_no = $request->unit_no;

        $property->water = $request->water;
        $property->strata = $request->strata;
        $property->council = $request->council;

        $property->water_cost = $this->amountHelper($request->water_cost);
        $property->strata_cost =  $this->amountHelper($request->strata_cost);
        $property->council_cost = $this->amountHelper($request->council_cost);

        $property->external_size = $request->external_size;
        $property->parking_size = $request->parking_size;

        $property->size = $request->size;
        $property->frontage = $request->frontage;
        $property->width = $request->width;
        $property->depth = $request->depth;

        $property->is_secured = false;

        $property->save();

        $property->featured_images =  S3FileUpload::images($request->featuredImages)
            ->project($deal->id)
            ->property($property->id)
            ->resize()
            ->propertiesPaths();

        if($request->has('preview_img') && $request->preview_img[0])
        {
            $img = S3FileUpload::preview_image($request->preview_img)
                    ->project($deal->id)
                    ->property($property->id)
                    ->resize()
                    ->ppPath(false);
                            
            $property->rich_preview_image = $img;
        }

        if(isset($request->floorPlan['file'])){
            $s3 = S3FileUpload::floorPlan($request->floorPlan)
            ->project($deal->id)
            ->property($property->id)
            ->fpPath();

            $property->floor_plan = (string)$s3 != '' ? $s3 : null;
        }

        $property->save();

        // $deal = Deal::find(2); //test
        $proj = Deal::find($deal->id);

        $recentProperties = $proj->properties()->with('type')->get();

        return response()->json($recentProperties, 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Property $property)
    {
        return response($property);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user = Auth::user();
        $role =$user->getRoleNames()[0];

        $rules = [
            'featuredImages' => 'required|array|min:3|max:6',
            'price' => 'required',
            'number_of_bedrooms' => 'required_if:propertyType,1|required_if:propertyType,2|required_if:propertyType,3|required_if:propertyType,5',
            'number_of_bathrooms' => 'required_if:propertyType,1|required_if:propertyType,2|required_if:propertyType,3|required_if:propertyType,5',
            'number_of_garages' => 'required_if:propertyType,1|required_if:propertyType,2|required_if:propertyType,3|required_if:propertyType,5',
            'strata' => 'required_with:strata_cost',
            'water' => 'required_with:water_cost',
            'council' => 'required_with:council_cost',
            'size' => 'required_if:propertyType,4|numeric',
            'frontage' => 'required_if:propertyType,4|numeric',
            'width' => 'required_if:propertyType,4|numeric',
            'depth' => 'required_if:propertyType,4|numeric',
            'floor_area' => 'filled',
            'floorPlan' =>'filled',
            'unit_name' => 'required',
            'unit_no' => 'required_if:propertyType,1|required_if:propertyType,2|required_if:propertyType,3|required_if:propertyType,5',
            'description' => 'required',
            'propertyType' => 'required',
            'subPropertyType' => 'filled',
            'ownershipType' => 'filled'
            // 'internal_size' => 'required'
        ];

        $validationMessages = [
            'required' => '*Required',
            'required_without' => '*Required',
            'required_if' => '*Required',
        ];

        $this->validate($request, $rules, $validationMessages);

        $property = Property::find($id);

        $deal = $property->deal()->first();

        $property->unit_name = $request->unit_name;
        $property->description = $request->descriptionHTML;
        $property->property_type_id = $request->propertyType;
        $property->sub_property_id = 1;
        $property->ownership_type_id = 1;

        $property->address = $deal->address;

        $property->featured_images =  S3FileUpload::images($request->featuredImages)
                                        ->project($deal->id)
                                        ->property($property->id)
                                        ->resize()
                                        ->propertiesPaths();

         if(isset($request->floorPlan['file'])){
            $property->floor_plan = S3FileUpload::floorPlan($request->floorPlan)
                                        ->project($deal->id)
                                        ->property($property->id)
                                        ->fpPath();
         }else{
            S3FileUpload::delete_fp($property->floor_plan);
            $property->floor_plan = null;
         }

         if($request->has('preview_img') && $request->preview_img[0])
         {
             $img = S3FileUpload::preview_image($request->preview_img)
                                 ->project($deal->id)
                                 ->property($property->id)
                                 ->resize()
                                 ->ppPath(false);
     
             $property->rich_preview_image = $img;
         }
         else
         {
             S3FileUpload::delete_existing_image($property->rich_preview_image);
             $property->rich_preview_image = null;
         }        

        $property->price = $this->amountHelper($request->price);
        $property->setAttribute('discounted_price', $deal->discount);
        $property->no_of_bedrooms = $request->number_of_bedrooms;
        $property->no_of_bathrooms = $request->number_of_bathrooms;
        $property->no_of_garages = $request->number_of_garages;
        $property->internal_size = $request->internal_size;
        $property->unit_no = $request->unit_no;

        $property->water = $request->water;
        $property->strata = $request->strata;
        $property->council = $request->council;

        $property->water_cost = $this->amountHelper($request->water_cost);
        $property->strata_cost =  $this->amountHelper($request->strata_cost);
        $property->council_cost = $this->amountHelper($request->council_cost);

        $property->external_size = $request->external_size;
        $property->parking_size = $request->parking_size;

        $property->size = $request->size;
        $property->frontage = $request->frontage;
        $property->width = $request->width;
        $property->depth = $request->depth;

        $property->is_secured = false;

        $property->save();

        $proj = Deal::find($deal->id);

        $recentProperties = $proj->properties()->with('type')->get();

        return response()->json($recentProperties, 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    private function amountHelper($amount)
    {
        $newVal = str_replace('$','',$amount);
        $newVal = str_replace(',','',$newVal);
        return $newVal;
    }

    public function calculateAverageSavings()
    {
        $aveSavings = $this->propertyRepositoryInterface->averageSavings();
        return response()->json($aveSavings, 200);
    }

    public function requestInspection(BookingInspectionRequest $request){
        $requestData = $this->propertyRepositoryInterface->requestBookingInspection($request);
        return response()->json($requestData, 200);
    }
}
