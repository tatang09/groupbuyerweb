<?php

namespace App\Http\Controllers;

use App\Deal;
use App\Http\Traits\Paginators;
use App\Http\Traits\QueryFilter;
use App\Http\Controllers\Controller;
use App\Http\Resources\PaginationCollection;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Auth;

class ProjectController extends Controller
{
    use Paginators;
    use QueryFilter;
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
         
        $dealsId = $this->queryFilterByUserType('id', new Deal);

        $sort = request('sort', 'deals.created_at');

        $order = request('order', 'desc');

        $deal = new Deal();

        $deals = Deal::with('properties')
            ->join('sub_property_types', 'deals.sub_property_id', '=', 'sub_property_types.id')
            ->join('agents', 'deals.agent_id', '=', 'agents.id')
            ->select(
                'deals.id',
                'deals.name',
                'deals.address',
                'deals.featured_images',
                'deals.description',
                'deals.expires_at',
                'deals.is_deal_approved',
                'deals.is_featured',
                'deals.is_weekly',
                'deals.deal_time_limit',
                'deals.sub_property_id',
                'deals.display_suite_address',
                'deals.resources',
                'deals.proposed_settlement',
                'deals.discount',
                'deals.deposit',
                'deals.user_id',
                'deals.is_completed',
                'deals.rich_preview_image',
                'deals.lat',
                'deals.long',
                'deals.builder_profile_link',
                'deals.developer_profile_link',
                'deals.video_link',
                'deals.architect_profile_link',
                'sub_property_types.name as deal_type',
                'sub_property_types.id as deal_type_id',
                'agents.name as agent_name',
                'agents.phone_number as agent_phone',
                'agents.email as agent_email'
            );

        if (request('all')) {
            return $deals->get()->toArray();
        }

        $this->sortBy = [$sort => $order];

        $columnsSearch = [];

        $keyword = request('keyword', '');

        if ($keyword) {
            $deals
                ->where(function ($query) use ($keyword, $dealsId) {
                    $query->where('deals.name', 'LIKE', "%{$keyword}%")
                        ->whereIn("deals.id", $dealsId);
                })
                ->orWhere(function ($query) use ($keyword, $dealsId) {
                    return $query->orWhereRaw('LOWER(deals.address) like ?', '%' . strtolower($keyword) . '%');
                })->whereIn("deals.id", $dealsId)
                ->orWhere(function ($query) use ($keyword, $dealsId) {
                    $query->where('sub_property_types.name', 'LIKE', "%{$keyword}%")
                        ->whereIn("deals.id", $dealsId);
                });
        } else {
            $deals->whereIn("deals.id", $dealsId);
        }


        $data = $this->paginate($deals, $columnsSearch, true);

        return new PaginationCollection($data);
    }
}
