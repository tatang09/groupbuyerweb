<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\ValidationException;
use App\User;
use App\Http\Traits\ActiveCampaign;

class LoginController extends Controller
{
    /**
     * Login user and create token
     *
     * @param  [string] email
     * @param  [string] password
     * @param  [boolean] remember_me
     * @return [string] access_token
     * @return [string] token_type
     * @return [string] expires_at
     * @return \Illuminate\Http\Response
     */

    use ActiveCampaign;

    public function login(Request $request)
    {
        $request->validate([
            'email' => 'required|string|email',
            'password' => 'required|string',
        ]);

        $credentials = request(['email', 'password']);

        if (!Auth::attempt($credentials)) {
            throw ValidationException::withMessages([
                'email' => [trans('auth.failed')],
            ]);
        }

        $user = $request->user();
        //   $user = auth()->guard('web')->user();
        $tokenResult = $user->createToken('Personal Access Token');


        $token = $tokenResult->token;

        if ($request->has('remember_me')) {
            $token->expires_at = Carbon::now()->addWeeks(1);
        }

        $token->save();

        return response()->json([
            'access_token' => $tokenResult->accessToken,
            'token_type' => 'Bearer',
            'expires_at' => Carbon::parse(
                $tokenResult->token->expires_at
            )->toDateTimeString(),
            'user_role' => $user->roles->first()->name
        ]);
    }

    public function logout(Request $request)
    {
        $request->user()->token()->revoke();

        return response()->json([
            'message' => 'Successfully logged out.'
        ]);
    }

    public function socialMediaLogin(Request $request)
    {
 
        $existingUser = User::where('email', $request->email)->first();

        $user = null;

        if (!$existingUser) {

            $user = User::create([
                'first_name' =>  $request->first_name,
                'last_name' => $request->last_name,
                'email' => $request->email,
                'avatar_path' => $request->avatar_path
            ]);

            if (filter_var(request('is_google'), FILTER_VALIDATE_BOOLEAN)) {
                $user->google_id = $request->google_id;
            }

            if (filter_var(request('facebook_id'), FILTER_VALIDATE_BOOLEAN)) {
                $user->facebook_id = $request->facebook_id;
            }

            $user->assignRole("customer");

            $subscriptionResponse = $this->subscribeToContactNew($request);

            $subscriptionData = json_decode($subscriptionResponse);

            $user->ac_contact_id = $subscriptionData->subscriber_id;

            $user->save();

            $tokenResult = $user->createToken('Personal Access Token');

        } else {

            if ($existingUser->getRoleNames())
            {
                $existingUser->assignRole("customer");
            }

            if (!$existingUser->avatar_path) {
                $existingUser->avatar_path = $request->avatar_path;
                $existingUser->save();
            }

            $tokenResult = $existingUser->createToken('Personal Access Token');
        }

        $token = $tokenResult->token;

        $token->save();

        return response()->json([
            'access_token' => $tokenResult->accessToken,
            'token_type' => 'Bearer',
            'expires_at' => Carbon::parse(
                $tokenResult->token->expires_at
            )->toDateTimeString(),
            'user_role' => $existingUser->roles->first()->name
        ]);
    }
}
