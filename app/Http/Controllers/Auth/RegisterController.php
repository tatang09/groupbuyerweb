<?php

namespace App\Http\Controllers\Auth;

 
use App\User;
use Carbon\Carbon;
use App\Http\Controllers\Controller; 
use Illuminate\Http\Request; 
use Illuminate\Support\Facades\Auth; 
use Illuminate\Validation\ValidationException;
use App\Mail\RegistrationNotif;
use Illuminate\Support\Facades\Mail;
// use App\Http\Traits\ActiveCampaign;

class RegisterController extends Controller
{
    // use ActiveCampaign;

    public function register(Request $request)
    {
       
        $request->validate([
            'first_name' => 'required|string',
            'last_name' => 'required|string',
            'email' => 'required|string|email|unique:users',
            // 'phone' => 'required|regex:/^(\+61)[0-9]{9}$/',
            // 'phone' => 'required|numeric',
            'password' => 'required|string',
            'user_type' => 'required',
        ]);

            $user = User::create([
                'first_name' => $request->first_name,
                'last_name' => $request->last_name,
                'email' => $request->email,
                // 'phone' => $request->phone,
                'password' => bcrypt($request->password),
                'receive_notice' => $request->receive_notice
            ]);

            $user->assignRole($request->user_type);

            $credentials = request(['email', 'password']);

            if(!Auth::attempt($credentials)) {
                throw ValidationException::withMessages([
                    'email' => [trans('auth.failed')],
                ]);
            }

            $tokenResult = $user->createToken('Personal Access Token');

            $token = $tokenResult->token;

            $token->save();

            // $this->triggerFeaturedDealsEmail($request);
            // $this->triggerHowitWorksEmail($request, 'how-it-works');

            // $subscriptionResponse = $this->subscribeToContactNew($request);

            // $subscriptionData = json_decode($subscriptionResponse);

            // $user->ac_contact_id = $subscriptionData->subscriber_id;

            $user->save();

            $this->registrationNotif($request);

            return response()->json([
                'access_token' => $tokenResult->accessToken,
                'token_type' => 'Bearer',
                'expires_at' => Carbon::parse(
                    $tokenResult->token->expires_at
                )->toDateTimeString()
            ]);
    }

    private function registrationNotif($request)
    {
        $name = $request->first_name . " " . $request->last_name;
        $email = $request->email;
        $userType = $request->user_type;
        Mail::to(config('services.registration_notif.receiver'))
        ->send(new RegistrationNotif($name, $email, $userType));
        return ["message" => "Email sent successfully."];
    }
}
