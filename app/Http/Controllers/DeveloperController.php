<?php

namespace App\Http\Controllers;

use App\Developer;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Interfaces\DeveloperRepositoryInterface;
use App\Http\Traits\Paginators;
use App\Http\Resources\PaginationCollection;
use App\Mail\DemoNotif;
use Illuminate\Support\Facades\Mail;

class DeveloperController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    use Paginators;
    
    protected $developerIRepository;
    
    public function __construct(DeveloperRepositoryInterface $developerIRepository)
    {
        $this->developerIRepository = $developerIRepository;
    }

    public function index()
    {
        $returnValue =  $this->developerIRepository->fetchDevelopers();

        $data = $this->paginate($returnValue["developers"], $returnValue["columnsSearch"]);

        return new PaginationCollection($data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Developer  $developer
     * @return \Illuminate\Http\Response
     */
    public function show($user_id)
    {
        $data = $this->developerIRepository->fetchDeveloperById($user_id);

        return response()->json($data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Developer  $developer
     * @return \Illuminate\Http\Response
     */
    public function edit(Developer $developer)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Developer  $developer
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
       //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Developer  $developer
     * @return \Illuminate\Http\Response
     */
    public function destroy(Developer $developer)
    {
        //
    }

    public function requestDemo(Request $request)
    {
        $request->validate([
            'first_name' => 'required|string',
            'last_name' => 'required|string',
            'email' => 'required|string',
            'phone' => 'required|regex:/^(\+61)[0-9]{9}$/',
            'phone' => 'required|numeric',
            'company_name' => 'required',
            'title' => 'required'
        ]);

        $requestData = $this->demoNotif($request);
        return response()->json($requestData, 200);
    }

    private function demoNotif($request)
    {
        $name = $request->first_name . " " . $request->last_name;
        $email = $request->email;
        $phone = $request->phone;
        $company_name = $request->company_name;
        $title = $request->title;
        Mail::to(config('services.registration_notif.receiver'))
        ->cc([config('services.requestdemo_notif.cc1'), config('services.requestdemo_notif.cc2')])
        ->send(new DemoNotif($name, $email, $company_name, $phone, $title));
        return ["message" => "Email sent successfully."]; 
    }
}
