<?php

namespace App\Http\Controllers;

use App\Deal;
use App\Property;
use App\Http\Traits\Paginators;
use App\Http\Controllers\Controller;
use App\Http\Resources\PaginationCollection;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\File;
use App\Http\Helpers\Facades\S3FileUpload;
use Illuminate\Database\Eloquent\Collection;
use App\Http\Traits\DateTime;
// use App\Http\Helpers\Facades\DateTimeSum;
use Auth;
use App\ProjectDeveloper;
use Image;

class DealController extends Controller
{
    use Paginators;
    use DateTime;

    public function __construct()
    {
        $this->middleware('auth:api', ['only' => ['store', 'update']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */


    public function index()
    {
 
        if (request()->intent == "listings") {

            $deal = Deal::select('deals.id', 'deals.user_id', 'deals.name', 'deals.is_deal_approved', 'deals.proposed_settlement', 'deals.sub_property_id')
                ->addSelect(DB::raw('(SELECT COUNT(*) FROM properties WHERE properties.deal_id = deals.id) as propery_count'))
                ->get();

            if (filter_var(request()->isProjectLand, FILTER_VALIDATE_BOOLEAN)) {
                $deal = $deal->where('sub_property_id', "4");
            } else {
                $deal = $deal->where('sub_property_id', '<>', "4");
            }

            $data = $deal->where('propery_count', '<', 5)->toArray();
            return response()->json($data, 200);
        }

        $sort = request('sort', 'deals.created_at');
        $order = request('order', 'desc');

        $deals = Deal::with('properties')
            ->join('sub_property_types', 'deals.sub_property_id', '=', 'sub_property_types.id')
            ->join('agents', 'deals.agent_id', '=', 'agents.id')
            ->select(
                'deals.id',
                'deals.name',
                'deals.address',
                'deals.featured_images',
                'deals.description',
                'deals.expires_at',
                'deals.is_deal_approved',
                'deals.is_featured',
                'deals.is_weekly',
                'deals.deal_time_limit',
                'deals.sub_property_id',
                'deals.display_suite_address',
                'deals.resources',
                'deals.proposed_settlement',
                'deals.land_registration',
                'deals.discount',
                'deals.deposit',
                'deals.user_id',
                'deals.is_completed',
                'deals.rich_preview_image',
                'deals.lat',
                'deals.long',
                'deals.builder_profile_link',
                'deals.developer_profile_link',
                'deals.video_link',
                'deals.architect_profile_link',
                'deals.is_address_same',
                'sub_property_types.name as deal_type',
                'sub_property_types.id as deal_type_id',
                'agents.name as agent_name',
                'agents.phone_number as agent_phone',
                'agents.email as agent_email'
            )
            ->addSelect(DB::raw('(SELECT SUM(properties.price) FROM properties WHERE properties.deal_id = deals.id) as properties_total_price'))
            ->when(request('projects'), function ($query) {
                // Filter for Project Types
                $query->when(request('types'), function ($query) {
                    $types = explode(',', request('types'));

                    if (request('types') == "All") {
                        $query->whereNotNull('sub_property_id');
                    } else {
                        $query->whereIn('sub_property_id', $types);
                    }
                }, function ($query) {
                    $query->whereNull('sub_property_id');
                })

                    // Filter for Project Locations
                    ->when(request('locations'), function ($query) {
                        if (request('locations') == "All") {
                            // $query->whereNotNull('deals.address->city');
                        } else {
                            $query->where(function ($queryLocation) {
                                $locations = explode(',', request('locations'));

                                foreach ($locations as $key => $location) {
                                    $condition = $key ? 'orWhere' : 'where';
                                    $queryLocation->{$condition}('deals.address->city', $location);
                                }
                            });
                        }
                    }, function ($query) {
                        $query->whereNull('deals.address->city');
                    })

                    // Filter for Price Range
                    ->when(request('max_price') || request('min_price'), function ($query) {

                        $minPrice = request('min_price');
                        $maxPrice = request('max_price');

                        $discounted = Property::whereBetween('discounted_price', [$minPrice, $maxPrice])
                            ->groupBy('deal_id')
                            ->pluck('deal_id');

                        $original = Property::whereBetween('price', [$minPrice, $maxPrice])
                            ->whereNull('discounted_price')
                            ->groupBy('deal_id')
                            ->pluck('deal_id');

                        $deal_ids = $discounted->merge($original)->unique();

                        $query->whereIn('deals.id', $deal_ids);
                    });
            });

        if (request('approved')) {
            $deals->where('is_deal_approved', '=', 1);
        }

        if (request('featured')) {
            $deals->where('is_featured', '=', 1);
        }

        if (request('weekly')) {
            return $deals->where('is_weekly', 1)->latest('deals.created_at')->first();
        }

        if (request('all')) {
            return $deals->get()->toArray();
        }

        $this->sortBy = [$sort => $order];

        $keyword = request('keyword', '');

        if ($keyword) {
            $deals
                ->where('deals.name', 'LIKE', "%{$keyword}%")
                ->orWhere('agents.name', 'LIKE', "%{$keyword}%")
                ->orWhere('sub_property_types.name', 'LIKE', "%{$keyword}%")
                ->orWhere(function ($query) use ($keyword) {
                    return $query->orWhereRaw('LOWER(deals.address) like ?', '%' . strtolower($keyword) . '%');
                });
        }

        $columnsSearch = [''];

        $data = $this->paginate($deals, $columnsSearch);

        return new PaginationCollection($data);
    }



    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $user = Auth::user();
        $role = $user->getRoleNames()[0];

        $rules = [
            'name' => 'required|unique:deals,name',
            'description' => 'required',
            'propertyType' => 'required',
            'project_address_line_1' => 'required',
            'project_address_suburb' => 'required',
            'project_address_state' => 'required',
            'project_address_zip' => 'required_if:propertyType,1|required_if:propertyType,2|required_if:propertyType,3|required_if:propertyType,5',
            'project_address_city' => 'required_if:propertyType,1|required_if:propertyType,2|required_if:propertyType,3|required_if:propertyType,5',
            'project_address_country' => "required",
            'display_suite_address_line_1' => 'required_without:same_address',
            'display_suite_address_suburb' => 'required_without:same_address',
            'display_suite_address_state' => 'required_without:same_address',
            'display_suite_address_city' => 'required_without:same_address',
            'display_suite_address_zip' => 'required_without:same_address',
            'display_suite_address_country' => 'required_without:same_address',
            'featuredImages' => 'required|array|min:3|max:12',
            // 'proposed_settlement' => 'required',
            // 'developer' => 'filled',
            'project_time_limit' => 'filled'
        ];

        $validationMessages = [
            'required' => '*Required',
            'required_without' => '*Required'
        ];

        $this->validate($request, $rules, $validationMessages);

        $deal = new Deal;

        if ($role == "admin") {
            $deal->user_id = $request->developer;
            $deal->deal_time_limit = Carbon::parse($request->project_time_limit)->format('Y-m-d H:i:s');
        }

        if ($role == "project_developer") {
            $deal->user_id = Auth::user()->id;
            $date = Carbon::now();
            $deal->deal_time_limit = $date->addDays(30);
        }

        $deal->name = $request->name;

        $deal->description = $request->descriptionHTML;

        $project_address = [
            'line_1' => $request->project_address_line_1,
            'city' => $request->project_address_city,
            'suburb' => $request->project_address_suburb,
            'state' => $request->project_address_state,
            'postal' => $request->project_address_zip,
            'country' => $request->project_address_country
        ];

        $deal->address = $project_address;

        $bool = filter_var($request->same_address, FILTER_VALIDATE_BOOLEAN);

        if ($bool) {
            $deal->display_suite_address = $project_address;
        } else {
            $display_suite_address = [
                'line_1' => $request->display_suite_address_line_1,
                'city' => $request->display_suite_address_city,
                'suburb' => $request->display_suite_address_suburb,
                'state' => $request->display_suite_address_state,
                'postal' => $request->display_suite_address_zip,
                'country' => $request->display_suite_address_country
            ];

            $deal->display_suite_lat = $request->display_suite_lat;
            $deal->display_suite_long = $request->display_suite_long;
            $deal->display_suite_address = $display_suite_address;
        }

        $deal->is_address_same = $bool;

        $deal->featured_images = S3FileUpload::images($request->featuredImages)
            ->project($deal->id)
            ->resize()
            ->projectPaths();

        if ($request->has('preview_img') && $request->preview_img[0]) {
            $img = S3FileUpload::preview_image($request->preview_img)
                ->project($deal->id)
                ->resize()
                ->ppPath(true);

            $deal->rich_preview_image = $img;
        }

        $deal->created_at = Carbon::now();

        if (filter_var($request->isPropertyTypeLand, FILTER_VALIDATE_BOOLEAN)) {
            $deal->land_registration = Carbon::parse($request->proposed_settlement)->format('Y-m-d H:i:s');
        } else {
            $deal->proposed_settlement = Carbon::parse($request->proposed_settlement)->format('Y-m-d H:i:s');
            // $deal->expires_at = Carbon::parse($request->proposed_settlement)->format('Y-m-d H:i:s');
            $arr = ['time_limit' => $this->calculateDateSum($request, null), 'option' => $request->time_limit['option'], 'optionVal' => $request->time_limit['optionVal']];
            $deal->expires_at = json_encode($arr);
        }

        // $deal->incentive_id = 1;
        $deal->sub_property_id = $request->propertyType;
        $deal->agent_id = 1;
        $deal->is_featured = false;
        $deal->is_weekly = false;
        $deal->lat = $request->lat;
        $deal->long = $request->long;
        $deal->deposit = $request->deposit;
        $deal->is_completed = filter_var($request->isCompleted, FILTER_VALIDATE_BOOLEAN);

        if ($request->project_discount) {
            // do calculation
            $deal->discount = $request->project_discount;
        }

        $deal->resources = [];

        $deal->save();

        return response()->json($deal, 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Deal  $deal
     * @return \Illuminate\Http\Response
     */
    public function show(Deal $deal)
    {
        if (request()->paginated) {
            $sort = request('sort', 'deals.created_at');
            $order = request('order', 'desc');

            $deal = Deal::with('properties')
                ->where('deals.id', '=', $deal->id)
                ->join('agents', 'deals.agent_id', '=', 'agents.id')
                ->select(
                    'deals.id',
                    'deals.user_id',
                    'deals.name',
                    'deals.address',
                    'deals.featured_images',
                    'deals.description',
                    'deals.discount',
                    'deals.deposit',
                    'deals.resources',
                    'deals.lat',
                    'deals.long',
                    'deals.display_suite_address',
                    'deals.display_suite_lat',
                    'deals.display_suite_long',
                    'deals.builder_profile_link',
                    'deals.developer_profile_link',
                    'deals.video_link',
                    'deals.architect_profile_link',
                    'deals.expires_at',
                    'deals.is_completed',
                    'deals.sub_property_id',
                    'deals.proposed_settlement',
                    'agents.name as agent_name',
                    'agents.email as agent_email',
                    'agents.phone_number as agent_phone'
                );

            if (request('all')) {
                return $deal->get()->toArray();
            }

            $this->sortBy = [$sort => $order];

            $columnsSearch = ['deals.name'];

            $data = $this->paginate($deal, $columnsSearch);

            return new PaginationCollection($data);
        }

        $data = Deal::with('properties')
            ->join('agents', 'deals.agent_id', '=', 'agents.id')
            ->select(
                'deals.id',
                'deals.user_id',
                'deals.name',
                'deals.address',
                'deals.featured_images',
                'deals.description',
                'deals.discount',
                'deals.deposit',
                'deals.resources',
                'deals.lat',
                'deals.long',
                'deals.expires_at',
                'deals.is_completed',
                'deals.builder_profile_link',
                'deals.video_link',
                'deals.architect_profile_link',
                'deals.developer_profile_link',
                'deals.proposed_settlement',
                'deals.display_suite_address',
                'deals.display_suite_lat',
                'deals.display_suite_long',
                'deals.sub_property_id',
                'agents.name as agent_name',
                'agents.email as agent_email',
                'agents.phone_number as agent_phone'
            )
            ->find($deal->id);

        return response()->json($data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $user = Auth::user();
        $role = $user->getRoleNames()[0];

        $rules = [
            'name' => 'required|unique:deals,name,' . $id,
            'description' => 'required',
            'propertyType' => 'required',
            'project_address_line_1' => 'required',
            'project_address_suburb' => 'required',
            'project_address_state' => 'required',
            // 'project_address_zip' => 'required',
            // 'project_address_city' => 'required',
            'project_address_country' => "required",
            'display_suite_address_line_1' => 'required_without:same_address',
            'display_suite_address_suburb' => 'required_without:same_address',
            'display_suite_address_state' => 'required_without:same_address',
            'display_suite_address_city' => 'required_without:same_address',
            'display_suite_address_zip' => 'required_without:same_address',
            'display_suite_address_country' => 'required_without:same_address',
            'featuredImages' => 'required|array|min:3|max:12',
            // 'proposed_settlement' => 'required',
            // 'developer' => 'filled',
            'project_time_limit' => 'filled',
            'project_discount' => 'filled'
        ];

        $validationMessages = [
            'required' => '*Required',
            'required_without' => '*Required'
        ];

        $this->validate($request, $rules, $validationMessages);

        $deal = Deal::find($id);

        if ($role == "admin" || $role == "super_admin") {
            $deal->discount = $request->project_discount;
            $deal->user_id = $request->developer;
            $deal->deal_time_limit = Carbon::parse($request->project_time_limit)->format('Y-m-d H:i:s');
        } else {
            $deal->user_id = Auth::user()->id;
        }

        $deal->name = $request->name;
        $deal->description = $request->descriptionHTML;

        $project_address = [
            'line_1' => $request->project_address_line_1,
            'suburb' => $request->project_address_suburb,
            'city' => $request->project_address_city,
            'state' => $request->project_address_state,
            'postal' => $request->project_address_zip,
            'country' => $request->project_address_country
        ];

        $deal->address = $project_address;

        $bool = filter_var($request->same_address, FILTER_VALIDATE_BOOLEAN);

        if ($bool) {
            $deal->display_suite_address = $project_address;
        } else {
            $display_suite_address = [
                'line_1' => $request->display_suite_address_line_1,
                'suburb' => $request->display_suite_address_suburb,
                'city' => $request->display_suite_address_city,
                'state' => $request->display_suite_address_state,
                'postal' => $request->display_suite_address_zip,
                'country' => $request->display_suite_address_country
            ];
            $deal->display_suite_long = $request->display_suite_long;
            $deal->display_suite_lat = $request->display_suite_lat;
            $deal->display_suite_address = $display_suite_address;
        }

        $deal->is_address_same = $bool;

        $deal->featured_images = S3FileUpload::images($request->featuredImages)
            ->project($deal->id)
            ->resize()
            ->projectPaths();

        if ($request->has('preview_img') && $request->preview_img[0]) {
            $img = S3FileUpload::preview_image($request->preview_img)
                ->project($deal->id)
                ->resize()
                ->ppPath(true);

            $deal->rich_preview_image = $img;
        } else {
            S3FileUpload::delete_existing_image($deal->rich_preview_image);
            $deal->rich_preview_image = null;
        }

        if (filter_var($request->isPropertyTypeLand, FILTER_VALIDATE_BOOLEAN)) {
       
            $dateString = preg_replace('/\(.*$/', '', $request->proposed_settlement);
            $deal->land_registration = Carbon::parse($dateString)->format('Y-m-d');
           
        } else {     

            $dateString = preg_replace('/\(.*$/', '', $request->proposed_settlement);
            $newDate = Carbon::parse($dateString)->format('Y-m-d H:i:s');
            $deal->proposed_settlement = $newDate;
         
            $arr = ['time_limit' => $this->calculateDateSum($request, null), 'option' => $request->time_limit['option'], 'optionVal' => $request->time_limit['optionVal']];
            $deal->expires_at = json_encode($arr);
        }


        $deal->sub_property_id = $request->propertyType;
        $deal->agent_id = 1;
        // $deal->is_featured = false;
        // $deal->is_weekly = false;
        $deal->lat = $request->lat;
        $deal->long = $request->long;
        $deal->deposit = $request->deposit;

        $deal->is_completed = filter_var($request->isCompleted, FILTER_VALIDATE_BOOLEAN);

        $deal->save();

        return response()->json($deal, 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
