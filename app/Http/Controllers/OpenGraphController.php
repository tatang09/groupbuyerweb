<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Http\Traits\OpenGraph;

class OpenGraphController extends Controller
{
    use OpenGraph;

    public function __construct()
    {

    }

    public function processProjectContent($id, $preview = false)
    {
        $content = $this->processSiteContent($id, 'project');
        return view('app', ['items' => $content]);
    }

    public function processPropertyContent($projectId, $propertyId, $preview = false)
    {
        $content = $this->processSiteContent($propertyId, 'property');
        return view('app', ['items' => $content]);
    }
}
