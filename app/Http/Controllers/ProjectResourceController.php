<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Storage;
use App\Http\Helpers\Facades\S3FileUpload;
use Illuminate\Http\Request;
use App\Deal;

class ProjectResourceController extends Controller
{
    public function store(Request $request)
    {
        $deal = $this->searchDealById($request);

        if($request["builderProfileLink"] && $request["builder_profile"] == null){
            $deal->builder_profile_link = $request["builderProfileLink"];
        }else{
            if( $deal->builder_profile_link != null){
                $deal->builder_profile_link = null;
            }
        } 

        if($request["arcProfileLink"] && $request["architect_profile"] == null){
            $deal->architect_profile_link = $request["arcProfileLink"];
        }else{
            if( $deal->architect_profile_link != null){
                $deal->architect_profile_link = null;
            }
        } 

        if($request["devProfileLink"] && $request["developer_profile"] == null){
            $deal->developer_profile_link = $request["devProfileLink"];
        }else{
            if( $deal->developer_profile_link != null){
                $deal->developer_profile_link = null;
            }
        } 

        if($request["projectVideo"]){
            $deal->video_link = $request["projectVideo"];
        }else{
            $deal->video_link = null;
        }   

        if(isset($request->resources)) {
            $deal->resources = S3FileUpload::projectResources($request->resources)->project($deal->id)->resourcesPaths();
        }

        $deal->save();

        $new = Deal::with('properties.type')->where('deals.id', '=', $request->id)->first();

        return response()->json($new, 200);
    }

    public function update(Request $request)
    {
       
        $deal = $this->searchDealById($request);

        if($request["builderProfileLink"] && $request["builder_profile"] == null){
            $deal->builder_profile_link = $request["builderProfileLink"];
        }else{
            if( $deal->builder_profile_link){
                $deal->builder_profile_link = null;
            }
        } 

        if($request["arcProfileLink"] && $request["architect_profile"] == null){
            $deal->architect_profile_link = $request["arcProfileLink"];
        }else{
            if( $deal->architect_profile_link){
                $deal->architect_profile_link = null;
            }
        } 

        if($request["devProfileLink"] && $request["developer_profile"] == null){
            $deal->developer_profile_link = $request["devProfileLink"];
          
        }else{
            if( $deal->developer_profile_link){
                $deal->developer_profile_link = null;
            }
        } 

        if($request["projectVideo"]){
            $deal->video_link = $request["projectVideo"];
        }

        if(isset($request->resources)) {
            $deal->resources = S3FileUpload::projectResources($request->resources)->project($deal->id)->resourcesPaths();
        }else{
            $deal->resources = null;
        }

        $deal->save();

        $new = Deal::with('properties.type')->where('deals.id', '=', $request->id)->first();

        return response()->json($new, 200);
    }

    public function delete(Request $request)
    {
        $deal = $this->searchDealById($request);

        if(!isset($request->resources)) {

        }

        $deal->save();

        $new = Deal::with('properties.type')->where('deals.id', '=', $request->id)->first();

        return response()->json($new, 200);
    }

    private function searchDealById(Request $request)
    {
        return Deal::find($request->id);
    }
}
