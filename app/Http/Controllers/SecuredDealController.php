<?php

namespace App\Http\Controllers;

use App\Deal;
use App\SecuredDeal;
use App\Property;
use App\PaypalPaymentDetail;
use App\Mail\UserSecureDealSuccess;
use App\Http\Traits\Paginators;
use App\Http\Resources\PaginationCollection;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use App\Http\Interfaces\SecuredDealsRepositoryInterface;
use App\Http\Requests\PaymentGatewayRequest;
 

class SecuredDealController extends Controller
{

    use Paginators;
    protected $securedDealsIRepository;

    public function __construct(SecuredDealsRepositoryInterface $securedDealsIRepository)
    {
        $this->securedDealsIRepository = $securedDealsIRepository;
    }

    public function index()
    {
        return $this->securedDealsIRepository->fetchSecuredDeals();
    }

    public function store(PaymentGatewayRequest $request)
    {
        $payment =  $this->securedDealsIRepository->saveSecuredDeal($request);
        return response()->json($payment, 200);
    }

    public function generateSecure3DPaymentToken(PaymentGatewayRequest $request)
    {
        $tokenResult = $this->securedDealsIRepository->generateSecure3DPaymentToken($request);
        return response()->json($tokenResult, 200);
    }

    public function initiatePayment(PaymentGatewayRequest $request)
    {
        $paymentResult = $this->securedDealsIRepository->initiateCardPayment($request);
        return response()->json($paymentResult, 200);
    }
}
