<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Interfaces\BankRepositoryInterface;
use Illuminate\Support\Facades\DB;

class BankController extends Controller
{
    private $bankRepositoryInterface;

    public function __construct(BankRepositoryInterface $bankRepositoryInterface)
    {
        $this->bankRepositoryInterface = $bankRepositoryInterface;
    }

    public function index()
    {
        $banks = $this->bankRepositoryInterface->all();
        return response()->json($banks, 200);
    }

    public function show($bankId)
    {
        $bank = $this->bankRepositoryInterface->findById($bankId);
        return response()->json($bank, 200);
    }

    public function update($request, $bankId)
    {
        $bank = $this->bankRepositoryInterface->update($request, $bankId);
        return response()->json($bank, 200);
    }

    public function destroy($bankId)
    {
        $bank = $this->bankRepositoryInterface->delete($bankId);
        return response()->json($bank, 200);
    }

    public function create($request)
    {
        $bank = $this->bankRepositoryInterface->save($request);
        return response()->json($bank, 200);
    }
}
