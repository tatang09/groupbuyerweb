<?php

namespace App\Http\Controllers;

use App\Wishlist;
use Illuminate\Http\Request;
use App\Property;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Http\Traits\Paginators;
use App\Http\Traits\QueryFilter;
use App\Http\Resources\PaginationCollection;
use Auth;

class WishlistController extends Controller
{
    use Paginators;
    use QueryFilter;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $propertyIds = $this->queryFilterByUserType('property_id', new Wishlist);

        $propertyList = new Property();

        $userWishLists = $propertyList->join('deals', 'properties.deal_id', '=', 'deals.id')
            ->select(
                'deals.id as deal_id',
                'deals.name as deal_name',
                'deals.address as deal_address',
                'deals.discount',
                'properties.id as property_id',
                'properties.unit_name as property_unit_name',
                'properties.unit_no as property_unit_no',
                'properties.price as property_price',
                'properties.address',
                'properties.featured_images',
                'properties.internal_size',
                'properties.external_size',
                'properties.parking_size',
                'properties.water',
                'properties.strata',
                'properties.council',
                'properties.council_cost',
                'properties.water_cost',
                'properties.strata_cost',
                DB::raw("(SELECT count(*) from properties WHERE properties.deal_id = deals.id AND properties.is_secured = 0) property_available")
            );

        $this->sortBy = ['deals.name' => 'asc'];

        $columnsSearch = [];

        $keyword = request('keyword', '');

        if ($keyword) {
            $userWishLists
                ->where(function ($query) use ($keyword, $propertyIds) {
                    $query->where('properties.unit_no', '=', $keyword);
                    $this->filterByPropertyIds($query, $propertyIds);
                })
                ->orWhere(function ($query) use ($keyword, $propertyIds) {
                    $query->where('properties.unit_name', '=', $keyword);
                    $this->filterByPropertyIds($query, $propertyIds);
                })
                ->orWhere(function ($query) use ($keyword, $propertyIds) {
                    $query->whereRaw('LOWER(deals.address) like ?', '%' . strtolower($keyword) . '%');
                    $this->filterByPropertyIds($query, $propertyIds);
                })
                ->orWhere(function ($query) use ($keyword, $propertyIds) {
                    $query->where('deals.name', 'LIKE', "%{$keyword}%");
                    $this->filterByPropertyIds($query, $propertyIds);
                });
        } else {
            $userWishLists->where(function ($query) use ($propertyIds) {
                $this->filterByPropertyIds($query, $propertyIds);
            });
        }

        $data = $this->paginate($userWishLists, $columnsSearch);

        return new PaginationCollection($data);
    }

    private function filterByPropertyIds($query, $propertyIds)
    {
        return $query->whereIn('properties.id', $propertyIds)
            ->where([['properties.is_secured', '=', '0'], ['properties.is_property_approved', '=', '1']]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $user = auth()->user();

        $wishList = DB::Table('wishlists')
            ->where('user_id', $user->id)
            ->where('deal_id', $request->deal_id)
            ->where('property_id', $request->property_id)
            ->get();

        if (count($wishList) == 0) {
            $wishList = Wishlist::create([
                'user_id' => auth()->user()->id,
                'property_id' => request('property_id'),
                'deal_id' => request('deal_id')
            ]);
        }

        return response($wishList);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Wishlist  $wishlist
     * @return \Illuminate\Http\Response
     */
    public function show(Wishlist $wishlist, $deal_id)
    {
        $user = auth()->user();

        $existingWishlist = Wishlist::where('user_id', $user->id)
            ->where('deal_id', $deal_id)
            ->get();

        return response()->json($existingWishlist);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Wishlist  $wishlist
     * @return \Illuminate\Http\Response
     */
    public function edit(Wishlist $wishlist)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Wishlist  $wishlist
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Wishlist $wishlist)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Wishlist  $wishlist
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        DB::Table('wishlists')
            ->where('id', $id)->delete();

        return response()->json([
            'message' => 'Item succesfully deleted from wishlist.'
        ]);
    }
}
