<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Http\Interfaces\SalesAdviceRepositoryInterface;

class SalesAdviceController extends Controller
{
    private $salesAdviceRepositoryInterface;

    public function __construct(SalesAdviceRepositoryInterface $salesAdviceRepositoryInterface)
    {
        $this->salesAdviceRepositoryInterface = $salesAdviceRepositoryInterface;
    }

    public function destroy($id)
    {
        return $this->salesAdviceRepositoryInterface->deletePurchaser($id);
    }

    public function show($securedDealId)
    {
        $additionalDetails = $this->salesAdviceRepositoryInterface->getAdditionalDetailsBySecuredDealId($securedDealId);

        $solicitorDetails = $this->salesAdviceRepositoryInterface->getSolicitorDetailsBySecuredDealId($securedDealId);

        $purchasers = $this->salesAdviceRepositoryInterface->getPurchasersBySecuredDealId($securedDealId);

        return response()->json([
            "additionalDetails" => $additionalDetails,
            "solicitorDetails" => $solicitorDetails,
            "purchasers" => $purchasers], 200);
    }

   public function store(Request $request)
   {
        if($request->isSubmit)
        {
            $purchaserErrorArray = $this->validatePurchaser($request->purchasers);

            if(count($purchaserErrorArray) > 0)
            {
                return response()->json([
                    'purchaserValidator' => $purchaserErrorArray,
                    'errorCode' => 422
                ]);
            }
            
            $solicitorValidator = $this->validateSolicitorDetails($request->solicitor);

            if ($solicitorValidator->fails())
            {
                return response()->json([
                    'solicitorValidator' => $solicitorValidator->errors(),
                    'errorCode' => 422
                ]);
            }

            return response()->json([$this->saveSalesAdvice($request), 200]);
        }
        else
        {
            return response()->json([$this->saveSalesAdvice($request), 200]);
        }
    }

    private function saveSalesAdvice($request)
    {
        return $this->salesAdviceRepositoryInterface->saveSalesAdvice($request);
    }

   private function validateSolicitorDetails($solicitorDetails)
   {
        return Validator::make($solicitorDetails, [
            'company' => 'required|string',
            'contact' => 'required|string',
            'email' => 'required|string|email',
            'phone' => 'required|numeric',            
            'state' => 'required|string',
            'country' => 'required|string',
            'postcode' => 'required|string',
            'suburb' => 'required|string',
            'poBox' => 'required|string',
        ]);
   }

   private function validatePurchaser($purchasers)
   {
        $purchaserArr = collect([]);

        foreach ($purchasers as $purchaser)
        {
            $validation = $this->purchaserValidation($purchaser);

            if($validation->fails())
            {
                $purchaserArr->push($validation->errors());
            }
        }

        return $purchaserArr;
   }

   private function purchaserValidation($purchaser)
   {
        return Validator::make($purchaser, [
                'first_name' => 'required|string',
                'last_name' => 'required|string',
                'email' => 'required|string|email',
                'phone' => 'required|numeric',     
                'alternate_phone' => 'sometimes|numeric',           
                'state' => 'required|string',
                'country' => 'required|string',
                'postcode' => 'required|string',
                'suburb' => 'required|string',
                'title' => 'required|string',
                'address_line_1' => 'required|string',
        ]);
   }
}
