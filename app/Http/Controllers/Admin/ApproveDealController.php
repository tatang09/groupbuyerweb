<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Deal;
use App\Property;
use App\Http\Interfaces\DealRepositoryInterface;

class ApproveDealController extends Controller
{
  
    private $dealIRepository;
    
    public function __construct(DealRepositoryInterface $dealIRepository)
    {
        $this->dealIRepository = $dealIRepository;
    }
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        $data = $this->dealIRepository->approvedDeal($request);

        return response()->json($data, 200);
    }

    public function featuredDeal(Request $request)
    {    
        $data = $this->dealIRepository->setBooleanFields($request);

        return response()->json($data, 200);
    }

    public function weeklyDeal(Request $request)
    {    
        $data = $this->dealIRepository->setBooleanFields($request);

        return response()->json($data, 200);
    }
}
