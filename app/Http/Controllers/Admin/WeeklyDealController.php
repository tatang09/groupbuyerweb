<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Deal;
use App\Http\Traits\DealsSetter;

class AdminWeeklyDealController extends Controller
{
    use DealsSetter;
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        $param =  ["dealId" => $request->projectId, "field" => $request->field, "value" => $request->input('value')] ;
        
        $dealData = $this->setDealBooleanFields($param);

        return response()->json($dealData, 200);
    }
}
