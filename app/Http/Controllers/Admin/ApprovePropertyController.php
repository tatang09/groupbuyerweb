<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Property;
use App\Deal;

class ApprovePropertyController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        $property = Property::find($request->propertyId);
        $property->update([
            'is_property_approved' => $request->value 
        ]);

        if (!$request->value)
        {
            $deal = Deal::find($property->deal_id);
            if($deal)
            {
                $deal->is_deal_approved = false;
                $deal->is_featured = false;
                $deal->is_weekly = false;
                $deal->save();
            }
        }

        return response()->json([], 200);
    }
}
