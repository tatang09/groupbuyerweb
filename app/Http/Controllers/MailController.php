<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use App\Mail\ContactForm;
use App\Mail\ApplyForm;

class MailController extends Controller
{
    public function contactFormEmail(Request $request)
    {
        $request->validate([
            'name' => 'required|string',
            'email' => 'required|string|email|unique:users',
            // 'phone' => 'required|regex:/^(\+61)[0-9]{9}$/',
            'phone' => 'phone:AU',
            'message' => 'required|string',
        ]);

        $email = config('app.email');

        Mail::to($email)->send(new ContactForm($request));
        return response()->json($request);
    }

    public function applyFormEmail(Request $request)
    {
        $request->validate([
            'first_name' => 'required|string',
            'last_name' => 'required|string',
            'company' => 'required|string',
            // 'phone' => 'required|regex:/^(\+61)[0-9]{9}$/',
            'phone' => 'phone:AU',
            'email' => 'required|string|email',
            'location' => 'required|string',
            // 'message' => 'sometimes|string',
        ]);

        $email = config('app.email');

        Mail::to($email)->send(new ApplyForm($request));
        return response()->json($request);
    }
}
