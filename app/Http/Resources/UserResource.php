<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class UserResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {

        return [
            'id' => $this->id,
            'email' => $this->email,
            'first_name' => $this->first_name,
            'last_name' => $this->last_name,
            'phone' => $this->phone,
            'avatar_path' => $this->avatar_path,
            'location' => $this->location,
            'max_price' => $this->max_price,
            'min_price' => $this->min_price,
            'best_describe' => $this->best_describe,
            'propertyType' => $this->propertyType,
            'user_role' => $this->roles->first()->name
        ];
    }
}
