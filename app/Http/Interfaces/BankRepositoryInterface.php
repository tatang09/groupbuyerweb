<?php

namespace App\Http\Interfaces;

interface BankRepositoryInterface
{
    public function findById($bankId);

    public function all();

    public function update($request, $bankId);

    public function delete($bankId);

    public function save($request);
}