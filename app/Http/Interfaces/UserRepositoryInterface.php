<?php

namespace App\Http\Interfaces;
use App\User;

interface UserRepositoryInterface
{
   
    public function create($request);

    public function update($request);

    public function userCount($user);
}