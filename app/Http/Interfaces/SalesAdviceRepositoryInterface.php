<?php

namespace App\Http\Interfaces;

interface SalesAdviceRepositoryInterface
{
    public function getPurchasersBySecuredDealId($securedDealId);
    
    public function deletePurchaser($id);

    public function saveSalesAdvice($request);

    public function getAdditionalDetailsBySecuredDealId($securedDealId);
 
    public function getSolicitorDetailsBySecuredDealId($securedDealId);
}