<?php


namespace App\Http\Interfaces;

interface PropertyRepositoryInterface
{
    public function averageSavings();

    public function requestBookingInspection($request);

    public function fetchProperties();

}
