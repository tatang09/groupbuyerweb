<?php

namespace App\Http\Interfaces;

interface DeveloperRepositoryInterface
{
    public function fetchDevelopers();

    // public function createDeveloper($request);

    // public function updateDeveloper($request);

    public function fetchDeveloperById($user_id);
}
