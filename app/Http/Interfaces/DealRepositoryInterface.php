<?php


namespace App\Http\Interfaces;

interface DealRepositoryInterface
{

    public function setBooleanFields($request);

    public function approvedDeal($request);

}
