<?php

namespace App\Http\Interfaces;

interface SecuredDealsRepositoryInterface
{

    public function fetchSecuredDeals();

    public function saveSecuredDeal($request);

    public function initiateCardPayment($request);

    public function generateSecure3DPaymentToken($request);
}

