<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class BookingInspectionRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|string',
            'email' => 'required|string|email',
            'phone' => 'required|numeric',
        ];
    }

    public function message()
    {
        return [
            'email.required' => 'The :attribute field is required!',
            'email.unique' => 'The :attribute has already been taken.',
            'name.required' => 'The :attribute field is required!',
            'phone.required' => 'The :attribute field is required!',
            'phone.numeric' => 'The :attribute only accepts numeric input!',
        ];
    }
}
