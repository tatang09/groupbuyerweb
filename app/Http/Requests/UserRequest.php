<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class UserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
     
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */

    public function rules()
    {
        $defaultRules = $this->defaultRules();

        if($this->getMethod() === "POST")
        {
            return $defaultRules += ['email' => 'required|email|unique:users',
                'password' => 'required|string'];

        }else{

             $params = $this->request->all();

            return $defaultRules += [
                'email' => 'sometimes|required|string|email|unique:users,email,' . $params["user_id"],
                'password' => 'sometimes|required|string|confirmed|min:6',
                'avatar_path' =>  'sometimes|image|mimes:jpeg,png,jpg|max:10240',
                'old_password' => 'required_with:password'];            
        }
    }

    private function defaultRules()
    {
        return [
            'first_name' => 'required|string',
            'last_name' => 'required|string',
            // 'phone' => 'required|regex:/^(\+61)[0-9]{9}$/',
            //'phone' => 'required|numeric',
            // 'password' => 'sometimes|required|string|confirmed|min:6',
            'user_type' => 'required',
        ];
    }

    public function messages()
    {
        return [
            'email.required' => 'The :attribute field is required!',
            'email.unique' => 'The :attribute has already been taken.',
            'first_name.required' => 'The :attribute field is required!',
            'last_name.required' => 'The :attribute field is required!',
            'password.required' => 'The :attribute field is required',
            'password.min:6' => 'The :attribute field is six(6) character required',
            'user_type.required' => 'The :attribute field is required',
        ];
    }
}
