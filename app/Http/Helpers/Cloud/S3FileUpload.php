<?php

namespace App\Http\Helpers\Cloud;


use Illuminate\Support\Facades\Storage;
use Illuminate\Http\File;
use App;
use Illuminate\Support\Str;

use Image;

class S3FileUpload
{
    protected $inputs = [];
    protected $project_name = "";
    protected $property_name ="";
    protected $project_resources = [];
    protected $floor_plan = [];
    protected $resize = false;
    protected $output = [];
    protected $avatar = "";
    protected $imgPreview = [];
    protected $imagePreviewPath = "";

    public function images($inputs = [])
    {
        $this->inputs = $inputs;

        return $this;
    }

    public function preview_image($imgPreview = [])
    {
        $this->imgPreview = $imgPreview;

        return $this;
    }

    public function project($project_name = "")
    {

        $this->project_name = strtolower(str_replace(' ', '-', trim($project_name)));

        return $this;
    }

    public function projectResources($resources = [])
    {
        $this->project_resources = $resources;

        return $this;
    }

    public function floorPlan($floor_plan = [])
    {
        $this->floor_plan = $floor_plan;

        return $this;
    }

    public function fpPath()
    {
        $this->output = $this->process_fp();

        return $this->process_fp();
    }

    public function process_fp()
    {
        $fp = '';
        if(!is_string($this->floor_plan['file'])) {

            $filename = time() . '_' . preg_replace("/[^.a-zA-Z0-9]/", "", $this->floor_plan['file']->getClientOriginalName());

            $fp ='projects/' . $this->project_name . '/properties/' . $this->property_name . '/floorplan/' .  $filename;

            Storage::disk(config('filesystems.cloud'))
                ->putFileAs('projects/' . $this->project_name . '/properties/'. $this->property_name . '/floorplan',
                  $this->floor_plan['file'] ,
                  $filename);
        } else {
            $var = $this->floor_plan['file'];
            if((string)$var != 'undefined'){
                $first = explode('/floorplan/', $this->floor_plan['file'])[1];
                $filename = explode('?', $first)[0];
                $fp ='projects/' . $this->project_name . '/properties/' . $this->property_name . '/floorplan/' .  $filename;
            }
        }

        return $fp;
    }

    public function delete_fp($path)
    {
        if($path){
            $resourceKey = $path['name'];

            $first = explode('/' . $resourceKey . '/', $path['file']);

            $second = explode('?', $first[0]);

            $filePath = explode('com/', $second[0]);

            Storage::disk('s3')->delete($filePath[1]);
        }
    }

    public function avatar($avatar)
    {
        if(!is_string($avatar)) {
            $filename = time() . '_' . $avatar->getClientOriginalName();
            $this->avatar = "avatars/" . $filename;
            Storage::disk(config('filesystems.cloud'))
                ->putFileAs('avatars/',
                    $avatar,
                    $filename);
        } else {
            $first = explode('/', $avatar)[1];
            $this->avatar = "avatars/" . explode('?', $first)[0];
        }

        return $this;
    }

    public function avatarPath()
    {
        return $this->avatar;
    }

    public function resourcesPaths()
    {
        $this->output = $this->process_resources();

        return $this->output;
    }


    public function resize()
    {
        $this->resize = true;

        return $this;
    }

    public function get()
    {
        return $this->output;
    }

    public function projectPaths()
    {
        $this->output = $this->process_input();

        return $this->output;
    }

    public function propertiesPaths()
    {
        $this->output = $this->process_properties_input();

        return $this->output;
    }

    public function property($property_name = "")
    {
        $this->property_name = strtolower(str_replace(' ', '-', trim($property_name)));

        return $this;
    }

    public function process_resources()
    {
        $raw = [];
        foreach($this->project_resources as $key => $resource) {
            $resourceKey = $resource['name'];
            $resourceFile = '';

            if(!is_string($resource['file'])) {

                $filename = time() . '_' . preg_replace("/[^.a-zA-Z0-9]/", "", $resource['file']->getClientOriginalName());

                $resourceFile = 'projects/' . $this->project_name . '/resources/' .  $resourceKey . '/' .  $filename;

                Storage::disk(config('filesystems.cloud'))
                    ->putFileAs('projects/' . $this->project_name . '/resources/' . $resourceKey,
                        $resource['file'],
                        $filename);
            } else {

                $first = explode('/' . $resourceKey . '/', $resource['file'])[1];
                $filename = explode('?',$first)[0];

                $resourceFile = 'projects/' . $this->project_name . '/resources/' . $resourceKey . '/' . $filename;
            }

            array_push($raw, $resourceFile);
        }

        return $raw;
    }

    public function delete_existing_image($filename)
    {
        Storage::disk('s3')->delete($filename);
    }

    public function process_properties_input()
    {
        $raw = [];
        foreach($this->inputs as $key => $input) {
            if(!is_string($input)) {
                $filename = time() . '_' . preg_replace("/[^.a-zA-Z0-9]/", "", $input->getClientOriginalName());

                $featuredImage = Image::make($input);
                if($this->resize) {
                    $final = $featuredImage->resize(1200, 1600, function($constraint){
                        $constraint->aspectRatio();
                        $constraint->upsize();
                    });
                    $final->save(storage_path('app/public/' . $filename));
                } else {
                    $featuredImage->save(storage_path('app/public/' . $filename));
                }

                array_push($raw, 'projects/' . $this->project_name . '/properties/' . $this->property_name . '/' .  $filename);

                Storage::disk(config('filesystems.cloud'))
                    ->putFileAs('projects/' . $this->project_name . '/properties/'. $this->property_name,
                      new File(storage_path('app/public/' . $filename))
                      , $filename);

                Storage::disk('public')->delete($filename);

            } else {
                $first = explode('/properties/', $input)[1];
                $second = explode('/', $first)[1];
                $filename = explode('?',$second)[0];

                array_push($raw, 'projects/' . $this->project_name . '/properties/' . $this->property_name . '/' .  $filename);
            }
        }
        return $raw;
    }

    public function process_input()
    {
        $raw = [];
        foreach($this->inputs as $key => $input) {
            if(!is_string($input)) {
                $filename = time() . '_' . preg_replace("/[^.a-zA-Z0-9]/", "", $input->getClientOriginalName());

                $featuredImage = Image::make($input);

                if($this->resize) {
                    $final = $featuredImage->resize(1200, 1600, function($constraint){
                        $constraint->aspectRatio();
                        $constraint->upsize();
                    });
                    $final->save(storage_path('app/public/' . $filename));
                } else {
                    $featuredImage->save(storage_path('app/public/' . $filename));
                }

                array_push($raw, 'projects/'. $this->project_name . '/featured-images/' .  $filename);

                Storage::disk(config('filesystems.cloud'))
                    ->putFileAs('projects/' . $this->project_name . '/featured-images', new File(storage_path('app/public/' . $filename)), $filename);

                Storage::disk('public')->delete($filename);
            } else  {

                $first = explode('/featured-images/', $input)[1];
                $filename = explode('?',$first)[0];

                array_push($raw, 'projects/'. $this->project_name . '/featured-images/' .  $filename);
            }
        }

        return $raw;
    }

    public function process_pp_project()
    {
       
        $pp = '';
        if(!is_string($this->imgPreview[0])) {

            $filename = time() . '_' . preg_replace("/[^.a-zA-Z0-9]/", "", $this->imgPreview[0]->getClientOriginalName());

            $pp = $this->setPreviewImagePath() . '/projects/' . $this->project_name . '/imgPreview/' .  $filename;

            Storage::disk('s3_cloud_image_preview')
                ->putFileAs($this->setPreviewImagePath() . '/projects/' . $this->project_name . '/imgPreview',
                $this->imgPreview[0] ,
                  $filename);
        } else {
           
            $var = $this->imgPreview[0];
            if((string)$var != 'undefined'){
                $first = explode('/imgPreview/', $this->imgPreview[0]);                
                $filename = explode('?', $first[0]);   
                if(Str::contains('$filename', config('s3_cloud_image_preview'))){
                    $filenameSuffix = explode(config('s3_cloud_image_preview'), $first[1]); 
                    $filename = config('s3_cloud_image_preview') . '/' . $filenameSuffix;
                }        
                $pp = $this->setPreviewImagePath() . '/projects/' . $this->project_name  . '/imgPreview/' .  $filename[0];
            }
        }

        return $pp;
    }

    public function process_pp_property()
    {
        $pp = '';
        
        if(!is_string($this->imgPreview[0])) {

            $filename = time() . '_' . preg_replace("/[^.a-zA-Z0-9]/", "", $this->imgPreview[0]->getClientOriginalName());

            $pp = $this->setPreviewImagePath() . '/projects/' . $this->project_name . '/properties/' . $this->property_name . '/imgPreview/' .  $filename;

            Storage::disk('s3_cloud_image_preview')
                ->putFileAs($this->setPreviewImagePath() . '/projects/' . $this->project_name . '/properties/'. $this->property_name . '/imgPreview',
                  $this->imgPreview[0] ,
                  $filename);
        } else {
            $var = $this->imgPreview[0];
            if((string)$var != 'undefined'){
                $first = explode('/imgPreview/', $this->imgPreview[0]);                
                $filename = explode('?', $first[0]);   
                if(Str::contains('$filename', config('s3_cloud_image_preview'))){
                    $filenameSuffix = explode(config('s3_cloud_image_preview'), $first[1]); 
                    $filename = config('s3_cloud_image_preview') . '/' . $filenameSuffix;
                }        
                $pp = $this->setPreviewImagePath() . '/projects/' . $this->project_name . '/properties/' . $this->property_name . '/imgPreview/' .  $filename[0];
            }
        }

        return $pp;
    }

    
    public function imgPreview($imgPreview = [])
    {
        $this->imgPreview = $imgPreview;

        return $this;
    }

    public function ppPath($isProject)
    {
        
        if($isProject){
            $this->output = $this->process_pp_project();
            return $this->output;
        }else{
            $this->output = $this->process_pp_property();
            return $this->output;
        }
    }

    private function setPreviewImagePath()
    {
        if(App::environment('production')) {

            return 'prd';

        }else if(App::environment('stg')){

            return 'stg';

        }else{

            return 'dev';
            
        } 
    }
}
