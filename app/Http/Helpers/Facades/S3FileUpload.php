<?php

namespace App\Http\Helpers\Facades;

use Illuminate\Support\Facades\Facade;

class S3FileUpload extends Facade 
{
    protected static function getFacadeAccessor()
    {
        return 's3fileupload';
    }
}