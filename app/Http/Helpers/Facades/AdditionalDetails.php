<?php

namespace App\Helpers\Facades;

use Illuminate\Support\Facades\Facade;

class AdditionalDetails extends Facade 
{
    protected static function getFacadeAccessor()
    {
        return 'additionaldetails';
    }
}