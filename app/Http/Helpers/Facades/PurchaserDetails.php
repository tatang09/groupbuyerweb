<?php

namespace App\Helpers\Facades;

use Illuminate\Support\Facades\Facade;

class PurchaserDetails extends Facade 
{
    protected static function getFacadeAccessor()
    {
        return 'purchaserdetails';
    }
}