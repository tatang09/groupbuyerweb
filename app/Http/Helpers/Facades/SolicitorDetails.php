<?php

namespace App\Helpers\Facades;

use Illuminate\Support\Facades\Facade;

class SolicitorDetails extends Facade 
{
    protected static function getFacadeAccessor()
    {
        return 'solicitordetails';
    }
}