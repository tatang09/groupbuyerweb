<?php

namespace App\Http\Repositories;

use Illuminate\Http\Request;
use App\Bank;
use Illuminate\Support\Facades\DB;
use App\Http\Interfaces\BankRepositoryInterface;

class BankRepository implements BankRepositoryInterface
{
    public function __construct()
    {
    }

    public function all()
    {
        return Bank::all();
    }

    public function findById($bankId)
    {
        return Bank::where('id', $bankId)->firstOrFail();
    }

    private function findByName($name)
    {
        return DB::table('banks')->where('name', $name)->first();
    }

    public function update($request, $bankId)
    {
        $bank = $this->findById($bankId);

        if($bank)
        {
            $bank->name = $request->name;
            $bank->save();
        }

        return $bank;
    }

    public function delete($bankId)
    {
        Bank::where('id', $bankId)->delete();
        return ["message" => "Bank deleted successfully"];
    }

    public function save($request)
    {
        if(!$request->new_bank_name)
        {
            return null;
        }
        else
        {
            $bank = $this->findByName($request->new_bank_name);

            if(!$bank)
            {
                $bank = Bank::create([
                    "name" => $request->new_bank_name,
                ]);
            }
        }
        return $bank;
    }
}