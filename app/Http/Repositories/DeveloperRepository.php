<?php

namespace App\Http\Repositories;

use App\Developer;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Interfaces\DeveloperRepositoryInterface;
use Auth;
use App\Http\Traits\Paginators;
use App\ProjectDeveloper;
use App\Http\Helpers\Facades\S3FileUpload;
use App\Bank;
use App\Http\Repositories\BankRepository;

class DeveloperRepository implements DeveloperRepositoryInterface {

    use Paginators;

    protected $developer;
    protected $user;
    protected $bank;
    protected $bankRepository;
     
    public function __construct(Developer $developer, User $user, Bank $bank, BankRepository $bankRepository){
        $this->user = $user;
        $this->developer = $developer;
        $this->bank = $bank;
        $this->bankRepository = $bankRepository;
    }

    public function fetchDevelopers()
    {
        $sort = request('sort', 'users.created_at');
        $order = request('order', 'desc');
        $developers = ProjectDeveloper::with('developer');

        if(request('all'))
        {
            return $developers->get()->toArray();
        }

        $this->sortBy = [$sort => $order];
        
        $columnsSearch = ['users.first_name','users.first_name', 'users.email', 'users.phone'];

        return ["developers" => $developers, "columnsSearch" => $columnsSearch];

    }

    public function createDeveloper($request, $userId)
    {
        $img_path = null;

        if($request->company_logo_path){
               
            $request->validate([
                'company_logo_path' =>  'sometimes|image|mimes:jpeg,png,jpg|max:10240'
            ]);
         
            $img_path = S3FileUpload::avatar($request->company_logo_path)->avatarPath();
        }

        Developer::create([
            'name' => $request->dev_name,
            'user_id' => $userId ? $userId : auth()->user()->id,
            'email' => $request->dev_email,
            'phone' => $request->dev_phone,
            'address_line_1' => $request->dev_address_line_1,
            'suburb' => $request->dev_suburb,
            'state' => $request->dev_state,
            'country' => $request->dev_country,
            'postcode' => $request->dev_postcode,
            'company_logo_path' => $img_path ? $img_path : null,
            'bank_bsb' => $request->bank_bsb,
            'bank_account_name' => $request->bank_account_name,
            'bank_account_number' => $request->bank_account_number,
            'bank_id' => $this->getBankId($request),
            'company_url' => $request->company_url
        ]);
    }

    public function updateDeveloper($request)
    {
        $developer = Developer::find($request->devId);

        $existing = $developer ? true : false;
 
        if($developer)
        {
            
            if($request->company_logo_path)
            {
               
                $request->validate([
                    'company_logo_path' =>  'sometimes|image|mimes:jpeg,png,jpg|max:10240'
                ]);
             
                $developer->company_logo_path = S3FileUpload::avatar(request('company_logo_path'))->avatarPath();
            }    
 
            $developer->name = $request->input('dev_name');
            $developer->email = $request->input('dev_email');
            $developer->phone = $request->input('dev_phone');
            $developer->address_line_1 = $request->input('dev_address_line_1');
            $developer->suburb = $request->input('dev_suburb');
            $developer->state = $request->input('dev_state');
            $developer->country = $request->input('dev_country');
            $developer->postcode = $request->input('dev_postcode');
            $developer->bank_bsb = $request->bank_bsb;
            $developer->bank_account_name = $request->bank_account_name;
            $developer->bank_account_number = $request->bank_account_number;
            $developer->bank_id = $this->getBankId($request);
            $developer->company_url = $request->company_url;

            $developer->save();

        } 
        return $existing;
    }

    public function fetchDeveloperById($user_id)
    {
        return Developer::where('user_id', $user_id)->first();
    }       

    private function getBankId($request)
    {
        $bankId = null;

         if($request->bank_id)
         {
             $bankId = $request->bank_id;
         }
         else
         {
            $bank = $this->bankRepository->save($request);
            
            if($bank)
            {
                $bankId = $bank->id;
            }
         }
         return $bankId;
    }
};