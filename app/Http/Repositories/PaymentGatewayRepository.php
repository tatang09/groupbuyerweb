<?php


namespace App\Http\Repositories;


use App\libraries\Simplify;
use App\libraries\Classes\CardToken\Simplify_CardToken;
use App\libraries\Classes\Payment\Simplify_Payment;
use App\libraries\Classes\Exceptions\Simplify_ApiException;
use App\libraries\Classes\Exceptions\Simplify_BadRequestException;
use App\libraries\Classes\Exceptions\Simplify_NotAllowedException;
use App;
use App\PaymentDetail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class PaymentGatewayRepository
{

    private $desc = "GroupBuyer Payment";


    public function __construct()
    {
    }

    private function amount()
    {
        return App::environment(['local', 'stg']) ?
            config('services.payment.testing_amount') :
            config('services.payment.production_amount');
    }

    private function checkEnvironment()
    {
        return App::environment(['local', 'stg']);
    }

    public function generate3dToken($request)
    {

        $cardData = $this->cardDetails($request);

        Simplify::$publicKey  = $this->checkEnvironment() ? config('services.commbank_keys.public_default') : config('services.commbank_keys.public_sandbox');
        Simplify::$privateKey  = $this->checkEnvironment() ? config('services.commbank_keys.private_default') : config('services.commbank_keys.private_sandbox');

        try {
            return Simplify_CardToken::createCardToken(array(
                'card' =>  $cardData,
                'secure3DRequestData' => array(
                    'amount' => $this->amount(),
                    'currency' => 'AUD',
                    "description" => $this->desc
                )
            ));
        } catch (Simplify_ApiException  $e) {

            return $this->processRequestError($e);
        }
    }

    private function cardDetails($request)
    {
        return [
            'expMonth' => $request->expMonth,
            'expYear' => $request->expYear,
            'cvc' => $request->cvc,
            'number' => $request->cardNumber,
        ];
    }

    public function createPayment($request)
    {

        Simplify::$publicKey  = $this->checkEnvironment() ? config('services.commbank_keys.public_default') : config('services.commbank_keys.public_sandbox');
        Simplify::$privateKey  = $this->checkEnvironment() ? config('services.commbank_keys.private_default') : config('services.commbank_keys.private_sandbox');

        try {
            return Simplify_Payment::createPayment(array(
                "token" => $request->token,
                "currency" => "AUD",
                'amount' => $this->amount(),
                "description" => $this->desc
            ));
        } catch (Simplify_ApiException  $e) {
            return $this->processRequestError($e);
        }
    }

    public function savePaymentDetails($payment, $securedDealId, $request)
    {
        $data = json_decode(json_encode($payment), true);

        return PaymentDetail::create([
            "secured_deal_id" => $securedDealId,
            "payment_id" => $data["id"] ? $data["id"] : null,
            "auth_code" => $data["paymentStatus"] == "APPROVED" ? $data["authCode"] : null,
            "card_data" => json_encode($data["card"]),
            "transaction_data" => json_encode($data["transactionData"]),
            "payment_status" => $data["paymentStatus"],
            "decline_reason" => $data["paymentStatus"] == "DECLINED" ? $data["declineReason"]  : null,
            "name_on_card" => $request->nameOnCard
        ]);
    }

    public function initiatePayment($request)
    {

        Simplify::$publicKey  = $this->checkEnvironment() ? config('services.commbank_keys.public_default') : config('services.commbank_keys.public_sandbox');
        Simplify::$privateKey  = $this->checkEnvironment() ? config('services.commbank_keys.private_default') : config('services.commbank_keys.private_sandbox');

        try {
            return Simplify_Payment::createPayment(array(
                'amount' => $this->amount(),
                'description' => 'Secure deal payment.',
                'currency' => 'AUD',
                'card' => $this->cardDetails($request)
            ));
        } catch (Simplify_ApiException  $e) {

            return $this->processRequestError($e);
        }
    }

    public function processRequestError($e)
    {

        $errorItems = collect([]);
        $errorField = "";
        $statusCode = 400;

        if ($e instanceof Simplify_BadRequestException && $e->hasFieldErrors()) {

            foreach ($e->getFieldErrors() as $fieldError) {

                $fieldName = $fieldError->getFieldName();
                $errorMessage = $fieldError->getMessage();
                // $errCode = $fieldError->getErrorCode();

                switch ($fieldName) {
                    case 'card.number':
                        $errorField = "cardNumber";
                        break;
                    case 'card.cvc':
                        $errorField = "cvc";
                        break;
                    case 'card.expMonth':
                        $errorField = "expMonth";
                        break;
                    default:
                        $errorField = "expYear";
                        break;
                }
            }
            $errorItems->push([$errorMessage]);

            $error = [$errorField => $errorItems[0]];
        }

        if ($e instanceof Simplify_NotAllowedException) {

            $statusCode = 405;

            $error = ["message" => "Operation not allowed."];
        }

        return ["status" => "DECLINED",  "statusCode" => $statusCode, "errors" => $error];
    }
}
