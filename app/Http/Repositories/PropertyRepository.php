<?php

namespace App\Http\Repositories;

use Illuminate\Http\Request;
use App\Property;
use Illuminate\Support\Facades\DB;
use App\Http\Interfaces\PropertyRepositoryInterface;
use App\Http\Repositories\DealRepository;
use App\Http\Repositories\UserRepository;
use App\Mail\BookingInspectionEmail;
use Illuminate\Support\Facades\Mail;

class PropertyRepository implements PropertyRepositoryInterface
{
    protected $dealsRepository;
    protected $userRepository;

    public function __construct(DealRepository $dealsRepository, UserRepository $userRepository)
    {
        $this->dealsRepository = $dealsRepository;

        $this->userRepository = $userRepository;
    }

    public function all()
    {
    }

    public function fetchProperties()
    {
        $sort = request('sort', 'properties.created_at');

        $order = request('order', 'desc');

        $propertyList = new Property();

        $properties = $propertyList->join('deals', 'properties.deal_id', '=', 'deals.id')
            ->select('properties.*', 'deals.name as deal_name', 'deals.discount');

        if (request('all')) {
            return $properties->get()->toArray();
        }

        $this->sortBy = [$sort => $order];

        // $columnsSearch = ['properties.unit_name','properties.address', 'properties.unit_no', 'deals.name'];

        $keyword = request('keyword', '');

        if ($keyword) {
            $properties->where('properties.unit_name', 'LIKE', "%{$keyword}%")
                ->orwhere('deals.name', 'LIKE', "%{$keyword}%")
                ->orWhere('properties.unit_no', 'LIKE', "%{$keyword}%")
                ->orWhere(function ($query) use ($keyword) {
                    return $query->orWhereRaw('LOWER(properties.address) like ?', '%' . strtolower($keyword) . '%');
                });
        }

        return ["properties" => $properties, "columnsSearch" => []];
    }


    public function findById($propertyId)
    {
        return Property::where('id', $propertyId)->first();
    }

    private function findByName($name)
    {
    }

    public function update($request, $propertyId)
    {
    }

    public function delete($propertyId)
    {
    }

    public function save($request)
    {
    }

    public function averageSavings()
    {
        $deals = $this->dealsRepository->getDealsWithDiscount();

        $aveSaving = 0;
        $totalSavings = 0;
        $propertyCount = 0;

        foreach ($deals as $key => $deal) {
            $propertyCount = $propertyCount + 1;
            $savings = $deal["price"]  * ($deal["discount"] / 100);
            $totalSavings += $savings;          
        }

        $aveSaving = $totalSavings / $propertyCount;
        $aveSaving = number_format((float)$aveSaving, 0, '.', '');
        return $aveSaving;
    }

    public function requestBookingInspection($request)
    {
        //Update user phone if none.
        $this->userRepository->updateUserPhone($request);

        //Get Project and Property
        $property = $this->findById($request["propertyId"]);
        $deal = $this->dealsRepository->getDealById($request);


        Mail::to(config('services.booking_inspection.default_receiver'))
            ->cc(config('services.booking_inspection.default_cc'))
            ->send(new BookingInspectionEmail($request, $property, $deal));
        return ["message" => "Email sent successfully."];
    }
}
