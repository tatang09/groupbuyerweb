<?php

namespace App\Http\Repositories;

use App;
use App\Deal;
use App\SecuredDeal;
use App\Property;
use App\PaypalPaymentDetail;
use App\Mail\UserSecureDealSuccess;
use App\Http\Traits\Paginators;
use App\Http\Resources\PaginationCollection;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Interfaces\SecuredDealsRepositoryInterface;
use Auth;
use Carbon\Carbon;
use App\Http\Repositories\PaymentGatewayRepository;
use App\libraries\Classes\Exceptions\Simplify_BadRequestException;
use App\Http\Traits\EmailCampaigns\SecuredDealEmail;
use App\Http\Traits\QueryFilter;

class SecuredDealsRepository extends PaymentGatewayRepository implements SecuredDealsRepositoryInterface
{

    use Paginators;
    use SecuredDealEmail;
    use QueryFilter;

    protected $deal;
    protected $property;

    public function __construct(Deal $deal)
    {
        $this->deal = $deal;
    }

    private function fetchProperty($request)
    {
        return Property::where('id', request('property_id'))->first();
    }

    private function fetchProject($request)
    {
        return Deal::where('deals.id', $this->fetchProperty($request)->deal_id)
            ->join('agents', 'deals.agent_id', '=', 'agents.id')
            ->select(
                'deals.name as deal_name',
                'deals.discount',
                'agents.name as agent_name',
                'agents.email',
                'agents.phone_number'
            )
            ->first();
    }

    public function fetchSecuredDeals()
    {
        $propertyIds = $this->queryFilterByUserType('property_id', new SecuredDeal);
 
        $propertyList = new Property();

        $secured_deals =  $propertyList
            ->join('secured_deals', 'properties.id', '=', 'secured_deals.property_id')
            ->join('deals', 'properties.deal_id', '=', 'deals.id')
            ->select(
                'secured_deals.id as secured_deals_id',
                'deals.id as deal_id',
                'deals.name as deal_name',
                'deals.address as deal_address',
                'deals.proposed_settlement',
                'deals.discount',
                'deals.featured_images as project_images',
                'properties.id as property_id',
                'properties.is_secured',
                'properties.is_property_approved',
                'properties.unit_name as property_unit_name',
                'properties.price as property_price',
                'properties.address as property_address',
                'secured_deals.created_at',
                DB::raw("(SELECT count(*) from properties WHERE properties.deal_id = deals.id AND properties.is_secured = 0) property_available")
            );

        $this->sortBy = ['secured_deals.created_at' => 'desc'];

        $columnsSearch = [];

        $keyword = request('keyword', '');

        if ($keyword) {
            $secured_deals
                ->where(function ($query) use ($keyword, $propertyIds) {
                    $query->where('deals.name', 'LIKE', "%{$keyword}%");
                    $this->filterByPropertyIds($query, $propertyIds);
                })
                ->orWhere('properties.unit_no', '=', $keyword)
                ->orWhere('properties.unit_name', '=', $keyword)
                ->orWhere(function ($query) use ($keyword, $propertyIds) {
                    $query->orWhereRaw('LOWER(deals.address) like ?', '%' . strtolower($keyword) . '%');
                    $this->filterByPropertyIds($query, $propertyIds);
                });
        } else {
            $secured_deals->where(function ($query) use ($propertyIds){
                $this->filterByPropertyIds($query, $propertyIds);
            });
        }

        $data = $this->paginate($secured_deals, $columnsSearch);

        return new PaginationCollection($data);
    }

    private function filterByPropertyIds($query, $propertyIds)
    {
        return $query->whereIn('properties.id', $propertyIds)
            ->where([['properties.is_secured', '=', '1'], ['properties.is_property_approved', '=', '1']]);
    }

    public function generateSecure3DPaymentToken($request)
    {
        return $this->generate3dToken($request);
    }

    public function initiateCardPayment($request)
    {

        $payment =  $this->initiatePayment($request);

        if ($payment && is_array($payment)) {
            if ($payment["status"] == 'DECLINED' && $payment["statusCode"] == 400) {
                return $payment;
            }
            return ["status" => "DECLINED", "statusCode" => 400, "declineReason" => "Please check card details."];
        }

        if ($payment && $payment->paymentStatus == 'DECLINED') {
            $this->savePaymentDetails($payment, 0, $request);
            return ["status" => "DECLINED", "statusCode" => 400, "declineReason" => $payment->declineReason];
        }

        if ($payment && $payment->paymentStatus == 'APPROVED') {
            return $this->saveDealData($payment, $request);
        }
    }

    private function saveDealData($payment, $request)
    {

        $user = auth()->user();

        $property = $this->fetchProperty($request);

        $securedDeal = SecuredDeal::create([
            'user_id' => $user->id,
            'property_id' => $request->property_id,
            'created_at' => Carbon::now()
        ]);

        tap($property)->update([
            'is_secured' => true
        ]);

        //COMMENT FOR STAGING TESTING
        $this->savePaymentDetails($payment, $securedDeal->id, $request);

        //trigger email
        $this->triggerSecuredDealSuccessEmail($request);

        return ["status" => "APPROVED", "statusCode" => 200];
    }

    public function saveSecuredDeal($request)
    {

        $payment = $this->createPayment($request);

        if ($payment instanceof Simplify_BadRequestException && $payment->hasFieldErrors()) {
            $errorItems = collect([]);

            foreach ($payment->getFieldErrors() as $fieldError) {
                $fieldName = $fieldError->getFieldName();
                $message = $fieldError->getMessage();
                $errCode = $fieldError->getErrorCode();

                $errorItems->push([
                    "fieldName" =>  $fieldName,
                    "errorMessage" => $message,
                    "errorCode" => $errCode
                ]);
            }
            return ["status" => "DECLINED", "statusCode" => 400, "error" => $errorItems];
        }

        if ($payment && $payment->paymentStatus == 'DECLINED') {
            $this->savePaymentDetails($payment, 0, $request);
            return ["status" => "DECLINED", "statusCode" => 400, "declineReason" => $payment->declineReason];
        }

        if ($payment && $payment->paymentStatus == 'APPROVED') {
            return $this->saveDealData($payment, $request);
        }
    }

    public function fetchSecuredDealByPropertyId($propertyId)
    {
        return SecuredDeal::where('property_id', $propertyId)->first();
    }
}
