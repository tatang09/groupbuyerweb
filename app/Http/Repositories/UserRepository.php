<?php

namespace App\Http\Repositories;

use App\Http\Interfaces\UserRepositoryInterface;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\ValidationException;
use App\User;
use Illuminate\Support\Facades\Http;
use App\Http\Traits\ActiveCampaign;
use App\Http\Helpers\Facades\S3FileUpload;
use App\ProjectDeveloper;
use GuzzleHttp\Client;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use App\Http\Repositories\DeveloperRepository;

class UserRepository implements UserRepositoryInterface
{

    use ActiveCampaign;

    private $developerRepository;

    public function __construct(DeveloperRepository $developerRepository)
    {
        $this->developerRepository = $developerRepository;
    }

    //login user
    public function login($request)
    {
        $credentials = request(['email', 'password']);

        if (!Auth::attempt($credentials)) {
            throw ValidationException::withMessages([
                'email' => [trans('auth.failed')],
            ]);
        }

        $user = $request->user();
        //   $user = auth()->guard('web')->user();
        $tokenResult = $user->createToken('Personal Access Token');
        $token = $tokenResult->token;

        if ($request->has('remember_me')) {
            $token->expires_at = Carbon::now()->addWeeks(1);
        }

        $token->save();

        return response()->json([
            'access_token' => $tokenResult->accessToken,
            'token_type' => 'Bearer',
            'expires_at' => Carbon::parse(
                $tokenResult->token->expires_at
            )->toDateTimeString()
        ]);
    }

    //logout user
    public function logout($request)
    {
        $request->user()->token()->revoke();

        return response()->json([
            'message' => 'Successfully logged out'
        ]);
    }

    //register user
    public function register($request)
    {
        $user = User::create([
            'first_name' => $request->first_name,
            'last_name' => $request->last_name,
            'email' => $request->email,
            // 'phone' => $request->phone,
            'password' => bcrypt($request->password)
        ]);

        $user->assignRole($request->user_type);

        $credentials = request(['email', 'password']);

        if (!Auth::attempt($credentials)) {
            throw ValidationException::withMessages([
                'email' => [trans('auth.failed')],
            ]);
        }

        $tokenResult = $user->createToken('Personal Access Token');
        $token = $tokenResult->token;
        $token->save();

        //trigger active campaign
        $this->triggerFeaturedDealsEmail($request);
        $this->triggerHowitWorksEmail($request, 'how-it-works');

        return response()->json([
            'access_token' => $tokenResult->accessToken,
            'token_type' => 'Bearer',
            'expires_at' => Carbon::parse(
                $tokenResult->token->expires_at
            )->toDateTimeString()
        ]);
    }

    //social media login
    public function socialMedialLogin($request)
    {
        $existingUser = User::where('email', $request->email)->first();

        if (!$existingUser) {

            $user = null;

            $user = User::create([
                'first_name' =>  $request->first_name,
                'last_name' => $request->last_name,
                'email' => $request->email,
                'facebook_id' => $request->facebook_id ? $request->facebook_id : null,
                'google_id' => $request->google_id ? $request->google_id : null,
                'avatar_path' => $request->avatar_path
            ]);

            $user->assignRole("customer");

            $tokenResult = $user->createToken('Personal Access Token');
        } else {

            $tokenResult = $existingUser->createToken('Personal Access Token');
        }

        $token = $tokenResult->token;

        $token->save();

        return [
            'access_token' => $tokenResult->accessToken,
            'token_type' => 'Bearer',
            'expires_at' => Carbon::parse(
                $tokenResult->token->expires_at
            )->toDateTimeString()
        ];
    }

    //create user
    public function create($request)
    {
        $user = User::create([
            'first_name' => $request->first_name,
            'last_name' => $request->last_name,
            'email' => $request->email,
            // 'phone' => $request->phone,
            'password' => bcrypt($request->password)
        ]);

        $this->developerRepository->createDeveloper($request, $user->id);

        $user->assignRole($request->user_type);

        return $user;
    }

    //user update
    public function update($request)
    {
        $rules = [
            'old_password' => 'required_if:password_confirmation,4|numeric',
        ];

        $userUpdateId = $request->user_id ? $request->user_id : auth()->user()->id;

        $user = User::find($userUpdateId);

        if (request('avatar_path')) {
            $user->avatar_path = S3FileUpload::avatar(request('avatar_path'))->avatarPath();
        }

        if (request('password')) {
            $user->password = bcrypt(request('password'));
        }

        $user->location = request('location');
        $user->propertyType = request('propertyType');
        $user->max_price = request('max_price');
        $user->min_price = request('min_price');
        $user->best_describe = request('best_describe');

        $existing = $this->developerRepository->updateDeveloper($request);

        if (!$existing) {

            $this->developerRepository->createDeveloper($request, $user->id);
        }

        $user = tap($user)->update(request()->except(['password', 'avatar_path']));

        return $user;
    }

    public function userCount($user)
    {
        $campaignURL = config('services.active_campaign.url');
        $campaignKey = config('services.active_campaign.key');

        $client = new Client();

        $data = $client->request('GET', $campaignURL . '/api/3/contacts?status=-1', [
            'headers' => [
                'Accept' => 'application/json',
                'Api-Token' => $campaignKey,
            ],
        ]);

        $userCount = json_decode($data->getBody()->getContents())->meta->total;

        return  number_format($userCount);
    }

    public function updateUserPhone($request)
    {
        $currentUser = auth()->user();
        if ($currentUser) {
            $user = User::where('id', $currentUser->id)->first();
            if (!$user->phone) {
                $user->phone = $request->phone;
                $user->save();
            }
        }
    }
}
