<?php

namespace App\Http\Repositories\SalesAdviceRepositoryHelper;

use Illuminate\Http\Request;
use App\SolicitorDetail;
use App\SecuredDeal;

class SalesAdviceSolicitorDetails
{
    public function __construct()
    {
    }

    public function saveSolicitorDetails($request, $securedDealId)
    {
        return SolicitorDetail::create([
         'secured_deal_id' => $securedDealId,
         'property_id' => $request->propertyId,
         'company' => $request["solicitor"]["company"],
         'contact' => $request["solicitor"]["contact"],
         'poBox' => $request["solicitor"]["poBox"],
         'address' => $request["solicitor"]["address"],
         'phone' => $request["solicitor"]["phone"],
         'email' => $request["solicitor"]["email"],
         ]);
    }

    public function updateSolicitorDetails($request, $solicitorDetailsData)
    {
        $solicitorDetailsData->company = $request["solicitor"]["company"]; 
        $solicitorDetailsData->contact = $request["solicitor"]["contact"]; 
        $solicitorDetailsData->poBox = $request["solicitor"]["poBox"]; 
        $solicitorDetailsData->address = $request["solicitor"]["address"]; 
        $solicitorDetailsData->phone = $request["solicitor"]["phone"]; 
        $solicitorDetailsData->email = $request["solicitor"]["email"];
            
        $solicitorDetailsData->save();
       
        return $solicitorDetailsData;
    }

    public function getSolicitorDetailsBySecuredDealId($securedDealId)
    {
        return SolicitorDetail::where('secured_deal_id', $securedDealId)->first();
    }
}