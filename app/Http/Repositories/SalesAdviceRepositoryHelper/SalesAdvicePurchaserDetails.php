<?php

namespace App\Http\Repositories\SalesAdviceRepositoryHelper;

use Illuminate\Http\Request;
use App\Purchaser;
use App\SecuredDeal;
use App\Http\Repositories\SecuredDealsRepository;
use Carbon\Carbon;

class SalesAdvicePurchaserDetails  
{
    public function saveUpdatePurchaser($data)
    {
        $purchaserArr = collect([]);

        foreach ($data["purchasers"] as $p)
        {

            $purchaser = new Purchaser();
            $purchaser->secured_deal_id = $data["securedDealId"];
            $purchaser->property_id = $data["propertyId"];
            $purchaser->title = $p["title"];
            $purchaser->first_name = $p["first_name"];
            $purchaser->last_name = $p["last_name"];
            $purchaser->address = json_encode($p["address"]);
            $purchaser->phone = $p["phone"];
            $purchaser->alternate_phone = $p["alternate_phone"];
            $purchaser->email = $p["email"];
            
            if($p["id"])
            {
                $this->updatePurchaser($p);
            }else{
                $purchaser->created_at = Carbon::now();
                $purchaser->updated_at = Carbon::now();
                $purchaserArr->push($purchaser);
            }
        }

        Purchaser::insert($purchaserArr->toArray());

        return $this->getPurchasersBySecuredDealId($data["securedDealId"]);
    }

    private function updatePurchaser($p)
    {
        $purchaser = $this->findPurchaserById($p["id"]);

        if($purchaser)
        {
            $purchaser->title = $p['title'];
            $purchaser->first_name = $p['first_name'];
            $purchaser->last_name = $p['last_name'];
            $purchaser->address = $p['address'];
            $purchaser->phone = $p['phone'];
            $purchaser->alternate_phone = $p['alternate_phone'];
            $purchaser->email=$p['email'];
            
            $purchaser->save();
        }
    }

    public function deletePurchaser($id)
    {
        $purchaser = $this->findPurchaserById($id);

        if($purchaser)
        {
            $purchaser->delete();
        }

        return $purchaser;
    }

    private function findPurchaserById($id)
    {
        return Purchaser::find($id);
    }

    public function getPurchasersBySecuredDealId($securedDealId)
    {
        return Purchaser::where('secured_deal_id', $securedDealId)
        ->get();
    }
}