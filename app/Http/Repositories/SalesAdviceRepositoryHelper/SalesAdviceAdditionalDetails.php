<?php

namespace App\Http\Repositories\SalesAdviceRepositoryHelper;

use Illuminate\Http\Request;
use App\AdditionalInfo;
use App\SecuredDeal;

class SalesAdviceAdditionalDetails
{
    public function __construct()
    {
    }

    public function saveAdditionalDetails($request, $securedDealId)
    {
        $heardAbout = $request["additionalInfo"]["other"] ? $request["additionalInfo"]["other"] : $request["additionalInfo"]["heard_about_us"];

        return AdditionalInfo::create([
            'secured_deal_id' => $securedDealId,
            'property_id' => $request->propertyId,
            'heard_about_us' => $heardAbout,
            'isFirstHomeBuyer' => $request["additionalInfo"]["isFirstHomeBuyer"],
            'isPreviousBoughtPlan' => $request["additionalInfo"]["isPreviousBoughtPlan"],
            'isFIRB_required' => $request["additionalInfo"]["isFIRB_required"],
            'other' => $request["additionalInfo"]["other"],
            'comment' => $request["additionalInfo"]["purpose"],
            'purpose' => $request["additionalInfo"]["purpose"]
            ]);
    }

    public function updateAdditionalDetails($request, $additionalDetailsData)
    {
        $heardAbout = $request["additionalInfo"]["other"] ? $request["additionalInfo"]["other"] : $request["additionalInfo"]["heard_about_us"];
        $additionalDetailsData->heard_about_us = $heardAbout;
        $additionalDetailsData->isFirstHomeBuyer = $request["additionalInfo"]["isFirstHomeBuyer"];
        $additionalDetailsData->isPreviousBoughtPlan = $request["additionalInfo"]["isPreviousBoughtPlan"];
        $additionalDetailsData->isFIRB_required = $request["additionalInfo"]["isFIRB_required"];
        $additionalDetailsData->other = $request["additionalInfo"]["other"];
        $additionalDetailsData->comment = $request["additionalInfo"]["purpose"];
        $additionalDetailsData->purpose = $request["additionalInfo"]["purpose"];
         
        $additionalDetailsData->save();
        
        return $additionalDetailsData;
    }

    public function getAdditionalDetailsBySecuredDealId($securedDealId)
    {
        return AdditionalInfo::where('secured_deal_id', $securedDealId)->first();
    }
}