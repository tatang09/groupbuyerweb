<?php

namespace App\Http\Repositories;

use App\Http\Interfaces\DealRepositoryInterface;
use App\Deal;
use App\Property;
use App\Http\Traits\ActiveCampaign;

class DealRepository implements DealRepositoryInterface
{

    use ActiveCampaign;

    protected $deal;
    
    protected $property;

    public function __construct(Deal $deal, Property $property)
    {
        $this->deal = $deal;

        $this->property = $property;
    }

    public function getDealById($request)
    {
        return $this->deal->where('id', $request->id)->first();
    }

    public function setBooleanFields($request)
    {
        $dealData = $this->getDealById($request);
        $dealData[$request->field] = $request->input('value');
        $dealData->save();

        if ($request->input('value') && $request->field === 'is_weekly') {
            //$this.triggerActiveCampaign($request);
        }

        return $dealData;
    }


    public function approvedDeal($request)
    {

        $properties = $this->property->where('deal_id', $request->projectId)->get();

        $returnValue = json_decode('{"success" : true, "message": ""}');

        $deal = $this->deal->where('id', $request->projectId)->first();

        if ($request->input('value') == true) {
            if (count($properties) < 5) {
                $returnValue->success = false;
                $returnValue->message = "Please upload minimum 5 properties.";
                return $returnValue;
            } else {
                foreach ($properties as $property) {
                    if (!$property->is_property_approved) {
                        $returnValue->success = false;
                        $returnValue->message = "Please approve all 5 properties for this project!";
                        break;
                    }
                }

                if (!$returnValue->success) {

                    return $returnValue;
                } else {

                    $deal->is_deal_approved = $request->input('value');

                    if (!$request->input('value')) {
                        $deal->is_featured = $request->input('value');
                    }

                    $deal->save();
                    $returnValue->message = $deal->is_deal_approved ? $deal->name . " deal has been approved successfully." : $deal->name . " deal has been disapproved successfully.";

                    return $returnValue;
                }
            }
        } else {
            $deal->is_deal_approved = $request->input('value');
            if (!$request->input('value')) {
                $deal->is_featured = $request->input('value');
            }
            $deal->save();
            $returnValue->message = $deal->is_deal_approved ? $deal->name . " deal has been approved successfully." : $deal->name . " deal has been disapproved successfully.";

            return $returnValue;
        }
    }

    public function getDealsWithDiscount()
    {
        $propertyList = new Property();
        return $propertyList->join('deals', 'properties.deal_id', '=', 'deals.id')
            ->select(
                'properties.price',
                'deals.discount'
            )
            ->where('deals.discount', '!=', null)
            ->where('deals.is_deal_approved', '=', true)
            ->get();
    }
}
