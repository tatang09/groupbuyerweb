<?php


namespace App\Http\Repositories;

use Illuminate\Http\Request;
use App\SecuredDeal;
use App\Purchaser;
use App\AddtionalInfo;
use App\SolicitorDetail;
use Illuminate\Support\Facades\DB;
use App\Http\Interfaces\SalesAdviceRepositoryInterface;
use App\Helpers\Facades\AdditionalDetails as AdditionalDetailsFacade;
use App\Helpers\Facades\SolicitorDetails as SolicitorDetailsFacade;
use App\Helpers\Facades\PurchaserDetails as PurchaserDetailsFacade;
use App\Http\Repositories\SecuredDealsRepository;

class SalesAdviceRepository extends SecuredDealsRepository implements SalesAdviceRepositoryInterface
{
    public function __construct()
    {
    }
 
    public function getPurchasersBySecuredDealId($securedDealId)
    {
        return PurchaserDetailsFacade::getPurchasersBySecuredDealId($securedDealId);
    }

    public function deletePurchaser($id)
    {
        $purchaser = PurchaserDetailsFacade::deletePurchaser($id);
        return response($purchaser);
    }

    public function saveSalesAdvice($request)
    {
        $securedDeal = $this->fetchSecuredDealByPropertyId($request["propertyId"]);

        $data = ["purchasers" => $request["purchasers"],
                 "propertyId" => $request["propertyId"],
                 "securedDealId" => $securedDeal->id];

        //save purchasers
        $purchaserDetailsArr = PurchaserDetailsFacade::saveUpdatePurchaser($data);

        //save solicitor
        $solicitorDetailsData = SolicitorDetailsFacade::getSolicitorDetailsBySecuredDealId($securedDeal->id);

        if($solicitorDetailsData)
        {
            $solicitorDetails = SolicitorDetailsFacade::updateSolicitorDetails($request,$solicitorDetailsData);
        }else{
            $solicitorDetails = SolicitorDetailsFacade::saveSolicitorDetails($request, $securedDeal->id);
        }

         //save additional info 
        $additionalDetailsData = AdditionalDetailsFacade::getAdditionalDetailsBySecuredDealId($securedDeal->id);

        if($additionalDetailsData)
        {
            $additionalDetails = AdditionalDetailsFacade::updateAdditionalDetails($request, $additionalDetailsData);
        }else{
            $additionalDetails = AdditionalDetailsFacade::saveAdditionalDetails($request, $securedDeal->id);
        }

        return response()->json(["solicitorDetails" => $solicitorDetails,
                "additionalDetails" => $additionalDetails,
                "purchasers" => $purchaserDetailsArr],
                 200);
    }

    public function updatePurchaserDetails($request, $id)
    {
        return PurchaserDetailsFacade::updatePurchaser($request, $id);
    }

    private function deletePurchaserDetails($id)
    {
        return PurchaserDetailsFacade::deletePurchaser($id);
    }

    public function updateAdditionalDetails($request, $id)
    {
        return AdditionalDetailsFacade::updateAdditionalInfo($request, $id);
    }

    public function getAdditionalDetailsBySecuredDealId($securedDealId)
    {
        return AdditionalDetailsFacade::getAdditionalDetailsBySecuredDealId($securedDealId);
    }

    public function updateSolicitorDetails($request, $id)
    {
        return SolicitorDetailsFacade::updateSolicitorDetails($request, $id);
    }

    public function getSolicitorDetailsBySecuredDealId($securedDealId)
    {
        return SolicitorDetailsFacade::getSolicitorDetailsBySecuredDealId($securedDealId);
    }
}