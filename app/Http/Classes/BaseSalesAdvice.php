<?php

namespace App\Classes;

use App\Purchaser;
use App\SecuredDeal;
use App\AddtionalInfo;
use App\SolicitorDetail;

abstract class BaseSalesAdvice{

    private $securedDeal;
    protected $solicitorDetails;
    protected $additionalInfo;
   
    protected $salesAdvice;

    public function __construct( SecuredDeal $securedDeal,
     
     AdditionalInfo $additionalInfo,
     SolicitorDetails $solicitorDetails){

        $this->securedDeal = $securedDeal;
      
        $this->additionalInfo = $additionalInfo;
        $this->solicitorDetails = $solicitorDetails;
    }

    public function fetchSecuredDeal($propertyId){
        $securedDeal = $this->securedDeal->where('property_id',  $propertyId)->first();
        return $securedDeal->id;
    }

    abstract protected function deleteSalesAdvice($id);
}