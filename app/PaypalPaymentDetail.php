<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PaypalPaymentDetail extends Model
{
    protected $guarded = [];

    protected $casts = [
        'amount' => 'float'
    ];

    public function secured_deal()
    {
        return $this->belongsTo('App\SecuredDeal');
    }
}
