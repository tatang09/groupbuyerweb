const defaultTheme = require("tailwindcss/defaultTheme");

module.exports = {
  important: true,
  purge: [],
  theme: {
    backgroundColor: theme => ({
      ...theme("colors"),
      gradient:
        "linear-gradient(to left, blue 0%, rgba(0, 0, 255, 0.8) 0%, rgba(255, 0, 255, 0.8) 100%)"
    }),
    extend: {
      colors: {
        transparent: "transparent",
        palette: {
          blue: "#0000FE",
          "blue-dark": "#000033",
          "blue-light": "#009EFF",
          teal: "#00FFFF",
          purple: "#BD00F2",
          violet: "#8C00E7",
          pink: "#FF00FE",
          gray: "#68899C",
          "gray-dark": "#363636",
          "gray-light": "#B1ADAC",
          green: "#00FF01",
          "purple-main": "#E91AFC",
          "violet-main-dark": "#4D0B98",
          "violet-main-light": "#674EA7",
          "black-main": "#211B24",
          "gray-main": "#707070",
          "light-gray": "#919191"
        }
      },
      screens: {
        xss: "280px",
        xs: {min:"320px", max:"425px"},
        'sm': '640px',
      // => @media (min-width: 640px) { ... }

      'md': '768px',
      // => @media (min-width: 768px) { ... }

      'lg': '1024px',
      // => @media (min-width: 1024px) { ... }

      'xl': '1280px',
      // => @media (min-width: 1280px) { ... }

      '2xl': '1536px',
      // => @media (min-width: 1536px) { ... }

      }
    },
    fontSize: {
      xxs: "0.6rem",
      xs: ".75rem",
      sm: ".875rem",
      tiny: ".875rem",
      base: "1rem",
      lg: "1.125rem",
      xl: "1.25rem",
      "2xl": "1.5rem",
      "3xl": "1.875rem",
      "4xl": "2.25rem",
      "5xl": "3rem",
      "6xl": "4rem",
      "7xl": "5rem",
      "8xl": "6rem"
    }
  },
  variants: {},
  plugins: []
};
